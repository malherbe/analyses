-- MySQL dump 10.13  Distrib 5.7.33, for Linux (x86_64)
--
-- Host: localhost    Database: malherbe
-- ------------------------------------------------------
-- Server version	5.7.33-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `analyse_HE_corpus`
--

DROP TABLE IF EXISTS `analyse_HE_corpus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_HE_corpus` (
  `NBR_AUTEURS` int(11) NOT NULL COMMENT 'nombre d''auteur',
  `NBR_RECUEILS_POESIES` int(11) NOT NULL COMMENT 'nombre de recueils de poésies',
  `NBR_RECUEILS_PIECES` int(11) NOT NULL COMMENT 'nombre de recueils d''une pièce de théâtre',
  `NBR_TEXTES` int(11) NOT NULL COMMENT 'nombre de textes',
  `NBR_POEMES` int(11) NOT NULL COMMENT 'nombre de poèmes',
  `NBR_PIECES` int(11) NOT NULL COMMENT 'nombre de pièces de théâtre',
  `NBR_VERS_POEMES` int(11) NOT NULL COMMENT 'nombre de vers dans des poèmes',
  `NBR_VERS_PIECES` int(11) NOT NULL COMMENT 'nombre de vers dans des pièces de théâtre',
  `NBR_VERS_TOTAL` int(11) NOT NULL COMMENT 'nombre total de vers',
  `NBR_MOTS` int(11) NOT NULL COMMENT 'nombre de mots',
  `DATE` text NOT NULL,
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table statistiques du corpus analysé';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `analyse_HE_corpus`
--

LOCK TABLES `analyse_HE_corpus` WRITE;
/*!40000 ALTER TABLE `analyse_HE_corpus` DISABLE KEYS */;
INSERT INTO `analyse_HE_corpus` VALUES (230,518,75,20568,20447,121,923070,136130,1059200,8134414,'octobre 2022','2022-10-14');
/*!40000 ALTER TABLE `analyse_HE_corpus` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-15  9:24:21
