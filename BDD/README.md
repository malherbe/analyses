
<div align="center">
<center><h3>Tables et requêtes pour la base de données</h3></center>
</div>

base de données MySQL ou MariadB : _malherbe_

▪  Le fichier **malherbe_analyse_structure.sql** permet de reconstituer la
structure des tables de la base de données.
Les différentes tables peuvent être 

▪ Le fichier **malherbe_analyse_structure+données.sql** permet de reconstituer
la totalité de la base de données ; les différentes tables contiennent les données
de l'ensemble des corpus.

▪ Liste des tables au format CSV et au format SQL :

- **analyse_CM_césures** :  Table des vers avec délimitation des sous-vers
- **analyse_CM_entraves** :  Table des vers entravés ou bloqués par une propriété PCMF+L
- **analyse_CM_PCMF+L** :  Table de la distribution des propriétés PCMF+L
- **analyse_CM_profils** :  Table des profils métriques
- **analyse_DI** :  Table des diérèses ordinaires et particulières et des synérèses particulières
- **analyse_ER_rimes** :  Table des rimes et des PGTC (PGTC = Plus Grande Terminaison Commune)
- **analyse_ER_stats** :  Table des valeurs calculées de la PGTC
- **analyse_ER_vers** :  Table analytique du contenu de la PGTC
- **analyse_HE_corpus** :  Table statistique du corpus analysé
- **analyse_HE_mots** :  Table des propriétés métriques et rimiques des mots du corpus analysé
- **analyse_HE_nbr_mots** :  Table du dénombrement des mots par poème
- **analyse_HE_stats** :  Table de données statistiques du corpus analysé
- **analyse_HE_vers** :  Table des vers
- **analyse_LG_nbr** :  Dénombrement des strophes et des vers par poème ou pièce de théâtre
- **analyse_LM_types** :  Table des longueurs métriques et des types métriques
- **analyse_LM_vers** :  Table des vers avec leur longueur métrique
- **analyse_NS_abs** :  Table des noyaux syllabiques (avant analyse métrique)
- **analyse_NS_met** :  Table des noyaux syllabiques (après analyse des diérèses et des 'e' instables)
- **analyse_PT_dis** :  Table des valeurs ponctuométriques selon la place métrique
- **analyse_PT_frq** :  Table des fréquences des signes de ponctuation selon les mètres
- **analyse_PT_moy** :  Table des moyennes ponctuométriques selon les mètres
- **analyse_PT_nbr** :  Table du dénombrement des signes de ponctuation selon la place métrique
- **analyse_PT_pct** :  Table des signes de ponctuation selon la place métrique
- **analyse_QR_poèmes** :  Table des valeurs calculées de la qualité des rimes par poème
- **analyse_QR_rimes** :  Table du calcul de la qualité des rimes
- **analyse_QR_types** :  Table des valeurs calculées de la qualité des rimes selon leur type
- **analyse_RI_rimes** :  Table des rimes
- **analyse_RI_vers** :  Table analytique du contenu de la rime vers par vers
- **analyse_ST** :  Table des formes globales et des types strophiques


▪ Le répertoire CSV contient les tables au format CSV : champs séparés par une
tabulation et sans guillemets de délimitation du texte des champs.

▪ Le répertoire SQL contient les requêtes au format .sql qui permettent de créer
les différentes tables de la base de données.

▪ Le répertoire PDF contient les fichiers de descriptions des tables et de leurs champs.
