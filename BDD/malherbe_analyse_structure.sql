-- MySQL dump 10.13  Distrib 5.7.33, for Linux (x86_64)
--
-- Host: localhost    Database: malherbe
-- ------------------------------------------------------
-- Server version	5.7.33-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `analyse_CM_césures`
--

DROP TABLE IF EXISTS `analyse_CM_césures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_CM_césures` (
  `ID` varchar(12) NOT NULL COMMENT 'identifiant du vers',
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `NUM_ABS` int(5) NOT NULL COMMENT 'numéro absolu du vers',
  `METRE` varchar(5) NOT NULL COMMENT 'mètre du vers',
  `LM` int(2) NOT NULL COMMENT 'longueur métrique du vers',
  `VERS` text NOT NULL COMMENT 'texte du vers',
  `VERS_REST` varchar(1) NOT NULL COMMENT 'r = vers restitué (vers réparti sur plusieurs lignes)',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  PRIMARY KEY (`ID`),
  KEY `ID_AUTEUR` (`ID_AUTEUR`),
  KEY `ID_RECUEIL` (`ID_RECUEIL`),
  KEY `ID_POEME` (`ID_POEME`),
  KEY `METRE` (`METRE`,`LM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table des vers et sous-vers';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_CM_entraves`
--

DROP TABLE IF EXISTS `analyse_CM_entraves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_CM_entraves` (
  `ID` varchar(12) NOT NULL COMMENT 'identifiant du vers',
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `NUM_REL` varchar(8) NOT NULL COMMENT 'numéro relatif du vers (strophe et vers)',
  `NUM_ABS` int(5) NOT NULL COMMENT 'numéro absolu du vers',
  `LM` int(2) NOT NULL COMMENT 'longueur métrique du vers',
  `PROFIL` varchar(5) NOT NULL COMMENT 'profil métrique du poème ou de la pièce',
  `METRE` varchar(5) NOT NULL COMMENT 'mètre du vers',
  `REGLE` varchar(20) NOT NULL COMMENT 'règle de l''algorithme utilisée pour la détermination du mètre',
  `PCMF` varchar(10) NOT NULL COMMENT 'propriété PCMF+L',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  KEY `PCMF` (`PCMF`),
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`,`ID_POEME`,`LM`,`METRE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des vers entravés ou bloqués par une propriété PCMF+L)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_CM_PCMF`
--

DROP TABLE IF EXISTS `analyse_CM_PCMF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_CM_PCMF` (
  `ID` varchar(12) NOT NULL COMMENT 'identifiant du vers',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `MODIF` varchar(10) NOT NULL COMMENT 'suppression d''une propriété PCMF+L non pertinente',
  `NUM_ABS` int(5) NOT NULL COMMENT 'numéro absolu du vers',
  `NUM_REL` varchar(8) NOT NULL COMMENT 'numéro relatif du vers (strophe et vers)',
  `LM` int(2) NOT NULL COMMENT 'longueur métrique du vers',
  `PROFIL` varchar(5) NOT NULL COMMENT 'profil métrique du poème ou de la pièce',
  `METRE` varchar(5) NOT NULL COMMENT 'mètre du vers',
  `VERS_REST` varchar(1) NOT NULL COMMENT 'r = vers restitué (vers réparti sur plusieurs lignes)',
  `VERS` text NOT NULL COMMENT 'texte du vers',
  `P1` varchar(10) NOT NULL COMMENT 'place 1',
  `P2` varchar(10) NOT NULL COMMENT 'place 2',
  `P3` varchar(10) NOT NULL COMMENT 'place 3',
  `P4` varchar(10) NOT NULL COMMENT 'place 4',
  `P5` varchar(10) NOT NULL COMMENT 'place 5',
  `P6` varchar(10) NOT NULL COMMENT 'place 6',
  `P7` varchar(10) NOT NULL COMMENT 'place 7',
  `P8` varchar(10) NOT NULL COMMENT 'place 8',
  `P9` varchar(10) NOT NULL COMMENT 'place 9',
  `P10` varchar(10) NOT NULL COMMENT 'place 10',
  `P11` varchar(10) NOT NULL COMMENT 'place 11',
  `P12` varchar(10) NOT NULL COMMENT 'place 12',
  `P13` varchar(10) NOT NULL COMMENT 'place 13',
  `P14` varchar(10) NOT NULL COMMENT 'place 14',
  `P15` varchar(10) NOT NULL COMMENT 'place 15',
  `P16` varchar(10) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `i_P1` (`P1`),
  KEY `i_P2` (`P2`),
  KEY `i_P3` (`P3`),
  KEY `i_P4` (`P4`),
  KEY `i_P5` (`P5`),
  KEY `i_P6` (`P6`),
  KEY `i_P7` (`P7`),
  KEY `i_P8` (`P8`),
  KEY `i_P9` (`P9`),
  KEY `i_P10` (`P10`),
  KEY `i_P11` (`P11`),
  KEY `i_P12` (`P12`),
  KEY `i_P13` (`P13`),
  KEY `i_P14` (`P14`),
  KEY `i_P15` (`P15`),
  KEY `i_P16` (`P16`),
  KEY `i_auteur` (`ID_AUTEUR`),
  KEY `i_recueil` (`ID_RECUEIL`),
  KEY `i_poeme` (`ID_POEME`),
  KEY `LM` (`LM`,`PROFIL`,`METRE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table de la distribution des propriétés PCMF+L';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_CM_profils`
--

DROP TABLE IF EXISTS `analyse_CM_profils`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_CM_profils` (
  `ID` varchar(15) NOT NULL,
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `LM` int(2) NOT NULL COMMENT 'longueur métrique du vers',
  `PROFIL` varchar(8) NOT NULL COMMENT 'profil métrique du poème ou de la pièce',
  `NBR` int(10) NOT NULL COMMENT 'nombre de vers du profil',
  `REPETITION` varchar(3) NOT NULL COMMENT 'r = le vers est une répétition',
  `REGLE` varchar(20) NOT NULL COMMENT 'règle de l''algorithme utilisée pour la détermination du profil',
  `DESCRIPTION` text NOT NULL COMMENT 'description succincte du profil métrique',
  `REMARQUES` text NOT NULL COMMENT 'remarque générée automatiquement à partir de l''algorithme',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  PRIMARY KEY (`ID`),
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`,`ID_POEME`,`LM`,`PROFIL`),
  KEY `LM` (`LM`,`PROFIL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des profils métriques';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_DI`
--

DROP TABLE IF EXISTS `analyse_DI`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_DI` (
  `ID` varchar(13) NOT NULL,
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `NUM_ABS` int(5) NOT NULL COMMENT 'numéro absolu du vers',
  `VERS` text NOT NULL COMMENT 'texte du vers',
  `VERS_REST` varchar(1) NOT NULL COMMENT 'r = vers restitué (vers réparti sur plusieurs lignes)',
  `MOT` varchar(50) NOT NULL COMMENT 'mot',
  `D1` varchar(5) NOT NULL COMMENT 'voyelle traitée en diérèse ou en synérèse',
  `D2` varchar(5) NOT NULL COMMENT 'voyelle suivante',
  `FILTRE` varchar(1) NOT NULL COMMENT 'd = diérèse, s = synérèse',
  `CAT` varchar(5) NOT NULL COMMENT 'catégorie du mot',
  `DICT` varchar(50) NOT NULL COMMENT 'type de traitement',
  `EXT` varchar(10) NOT NULL COMMENT 'portée de l''intervention',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`,`ID_POEME`,`FILTRE`,`CAT`),
  KEY `DICT` (`DICT`),
  KEY `MOT` (`MOT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table des diérèses ordinaires et particulières et des synérèses particulières';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_ER_rimes`
--

DROP TABLE IF EXISTS `analyse_ER_rimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_ER_rimes` (
  `ID` varchar(12) NOT NULL COMMENT 'identifiant de la rime',
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `ORIENTATION` varchar(1) NOT NULL COMMENT 'p = appariement progressif, r = appariement régressif',
  `NUM_RIME` int(5) NOT NULL COMMENT 'numéro de la rime',
  `APPARIEMENT` varchar(20) NOT NULL COMMENT 'numéro des vers appariés en rime',
  `GENRE` set('m','f','mf') NOT NULL COMMENT 'genre de la rime',
  `POIDS_PGTC` int(2) NOT NULL COMMENT 'poids de la PGTC',
  `SCHEMA_PGTC` varchar(50) NOT NULL COMMENT 'schéma de la PGTC',
  `PGTC` text NOT NULL COMMENT 'couple de mots avec délimitation de la PGTC',
  `REGLE` text NOT NULL COMMENT 'règle dans l''algorithme d''appariement des vers en rimes',
  `APPEL` varchar(100) NOT NULL COMMENT 'mot-rime en appel',
  `ECHO` varchar(100) NOT NULL COMMENT 'mot-rime en écho',
  `FR_SYLLABE` varchar(50) NOT NULL COMMENT 'franchissement d''une frontière de syllabe',
  `FR_MOT` varchar(50) NOT NULL COMMENT 'franchissement d''une frontière de mot',
  `REPETITION` varchar(1) NOT NULL COMMENT 'r = le vers est une répétition',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  PRIMARY KEY (`ID`),
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`,`ID_POEME`,`GENRE`,`POIDS_PGTC`,`SCHEMA_PGTC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des rimes et des PGTC';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_ER_stats`
--

DROP TABLE IF EXISTS `analyse_ER_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_ER_stats` (
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `MODE_PGTC` int(11) NOT NULL COMMENT 'mode de la PGTC',
  `MODE_EFFECTIF` text NOT NULL COMMENT 'nombre de rimes correspondant au mode',
  `MOY_PGTC` decimal(2,1) NOT NULL COMMENT 'moyenne de la PGTC',
  `ECART_PGTC` decimal(2,1) NOT NULL COMMENT 'écart-type de la PGTC',
  `MAX_PGTC` int(11) NOT NULL COMMENT 'valeur maximale de la PGTC',
  `EX_MAXIMUM` text NOT NULL COMMENT 'exemple de PGTC avec valeur maximale',
  `EX_MINIMUM` text NOT NULL COMMENT 'exemple de PGTC avec valeur minimale',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  PRIMARY KEY (`ID_POEME`),
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des valeurs calculées de la PGTC';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_ER_vers`
--

DROP TABLE IF EXISTS `analyse_ER_vers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_ER_vers` (
  `ID` varchar(12) NOT NULL COMMENT 'identifiant du vers',
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `NUM_VERS` int(11) NOT NULL COMMENT 'numéro absolu du vers',
  `VERS` text NOT NULL COMMENT 'texte du vers',
  `APPARIEMENT` varchar(20) NOT NULL COMMENT 'numéro des vers appariés en rime',
  `NUM_RIME` int(5) NOT NULL COMMENT 'numéro de la rime',
  `ORIENTATION` set('p','r') NOT NULL COMMENT 'p = appariement progressif, r = appariement régressif',
  `APPEL_ECHO` varchar(1) NOT NULL COMMENT 'a = appel e = écho',
  `GENRE` varchar(1) NOT NULL COMMENT 'genre de la rime',
  `LETTRE` varchar(1) NOT NULL COMMENT 'étiquette de la rime pour le schéma de rimes',
  `PHONEME` varchar(5) NOT NULL COMMENT 'phonème de la voyelle',
  `GRAPHEME` varchar(5) NOT NULL COMMENT 'graphème de la voyelle',
  `PART_PGTC` text NOT NULL COMMENT 'partie finale du vers correspondant à la PGTC',
  `POIDS_PGTC` int(11) NOT NULL COMMENT 'poids de la PGTC',
  `SCHEMA_PGTC` text NOT NULL COMMENT 'schéma de la PGTC',
  `TYPE_PGTC` text NOT NULL COMMENT 'type de la PGTC',
  `PGTC` text NOT NULL COMMENT 'délimitation de la PGTC',
  `REGLE` text NOT NULL COMMENT 'identifiant de la règle de calcul',
  `FR_SYLLABE` varchar(50) NOT NULL COMMENT 'franchissement d''une frontière de syllabe',
  `FR_MOT` varchar(50) DEFAULT NULL COMMENT 'franchissement d''une frontière de mot',
  `PGTC_MOT` varchar(20) NOT NULL COMMENT '[PGTC] = mot',
  `PGTC_CLITIQUE` varchar(20) NOT NULL COMMENT 'cl = la PGTC inclut un enclitique',
  `REPETITION` varchar(1) DEFAULT NULL COMMENT 'r = le vers est une répétition',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  PRIMARY KEY (`ID`),
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`,`GENRE`,`PGTC_MOT`,`PGTC_CLITIQUE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table analytique du contenu de la PGTC';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_HE_corpus`
--

DROP TABLE IF EXISTS `analyse_HE_corpus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_HE_corpus` (
  `NBR_AUTEURS` int(11) NOT NULL COMMENT 'nombre d''auteur',
  `NBR_RECUEILS_POESIES` int(11) NOT NULL COMMENT 'nombre de recueils de poésies',
  `NBR_RECUEILS_PIECES` int(11) NOT NULL COMMENT 'nombre de recueils d''une pièce de théâtre',
  `NBR_TEXTES` int(11) NOT NULL COMMENT 'nombre de textes',
  `NBR_POEMES` int(11) NOT NULL COMMENT 'nombre de poèmes',
  `NBR_PIECES` int(11) NOT NULL COMMENT 'nombre de pièces de théâtre',
  `NBR_VERS_POEMES` int(11) NOT NULL COMMENT 'nombre de vers dans des poèmes',
  `NBR_VERS_PIECES` int(11) NOT NULL COMMENT 'nombre de vers dans des pièces de théâtre',
  `NBR_VERS_TOTAL` int(11) NOT NULL COMMENT 'nombre total de vers',
  `NBR_MOTS` int(11) NOT NULL COMMENT 'nombre de mots',
  `DATE` text NOT NULL,
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table statistiques du corpus analysé';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_HE_mots`
--

DROP TABLE IF EXISTS `analyse_HE_mots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_HE_mots` (
  `ID` varchar(18) NOT NULL COMMENT 'identifiant du mot',
  `MOT` varchar(20) NOT NULL COMMENT 'mot',
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `PROFIL` varchar(5) NOT NULL COMMENT 'profil métrique du poème ou de la pièce',
  `TYPE` varchar(2) NOT NULL COMMENT 'type métrique : s = vers simple, c = vers complexes, m = monométrie, p = polymétrie',
  `NUM_ABS` int(5) NOT NULL COMMENT 'numéro absolu du vers',
  `NUM_REL` varchar(8) NOT NULL COMMENT 'numéro relatif du vers (strophe et vers)',
  `LM` int(2) NOT NULL COMMENT 'longueur métrique du vers',
  `METRE` varchar(5) NOT NULL COMMENT 'mètre du vers',
  `NUM_MOT` int(3) NOT NULL COMMENT 'numéro d''ordre du mot dans le vers',
  `VOYELLES` mediumtext NOT NULL COMMENT 'liste des voyelles du mot',
  `DVM` int(2) NOT NULL COMMENT 'place dans le vers de la dernière voyelle métrique',
  `TYPE_DVM` varchar(2) NOT NULL COMMENT 'type de la dernière voyelle métrique',
  `PCMF` text NOT NULL COMMENT 'propriété PCMF+L de la dernière voyelle métrique',
  `CESURE` varchar(1) NOT NULL COMMENT 'mot à la césure',
  `GENRE_RIME` varchar(1) NOT NULL COMMENT 'genre du vers',
  `APPEL_ECHO` varchar(1) NOT NULL COMMENT 'mot en appel ou mot en écho pour un mot à la rime',
  `LETTRE` varchar(1) NOT NULL COMMENT 'lettre du schéma de rimes',
  `QR_RIME` int(1) NOT NULL COMMENT 'qualité de la rime',
  `SCHEMA_PGTC` varchar(20) NOT NULL COMMENT 'schéma de la PGTC',
  `POIDS_PGTC` int(11) NOT NULL COMMENT 'poids de la PGTC',
  `PONCT` varchar(6) NOT NULL COMMENT 'signe de ponctuation placé après le mot',
  `ETAPE` int(2) NOT NULL COMMENT 'numéro d''étape du traitement',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  KEY `MOT` (`MOT`,`ID_AUTEUR`,`ID_RECUEIL`,`ID_POEME`,`PROFIL`,`LM`,`METRE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des propriétés métriques et rimiques des mots du corpus analysé';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_HE_nbr_mots`
--

DROP TABLE IF EXISTS `analyse_HE_nbr_mots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_HE_nbr_mots` (
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème',
  `NBR_MOTS` int(11) NOT NULL COMMENT 'nombre de mots',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  PRIMARY KEY (`ID_POEME`),
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table du dénombrement des mots par poème';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_HE_stats`
--

DROP TABLE IF EXISTS `analyse_HE_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_HE_stats` (
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `NBR_SECTIONS` int(5) NOT NULL COMMENT 'nombre de sections',
  `NBR_STROPHES` int(5) NOT NULL COMMENT 'nombre de strophes',
  `NBR_VERS` int(5) NOT NULL COMMENT 'nombre de vers',
  `NBR_ADD` int(5) NOT NULL COMMENT 'nombre d''élément <add>',
  `NBR_MOTS` int(5) NOT NULL COMMENT 'nombre de mots',
  `NBR_VOYELLES` int(5) NOT NULL COMMENT 'nombre de voyelles',
  `NBR_VS` int(5) NOT NULL COMMENT 'nombre de voyelles stables (vs)',
  `NBR_VS_DICT` int(5) NOT NULL COMMENT 'nombre de voyelles ayant fait l''objet d''une intervention',
  `NBR_VA` int(5) NOT NULL COMMENT 'nombre de voyelles ambigües (va)',
  `NBR_VV` int(5) NOT NULL COMMENT 'nombre de voyelles virtuelles (vv)',
  `NBR_VI` int(5) NOT NULL COMMENT 'nombre de voyelles instables (vi)',
  `NBR_EM` int(5) NOT NULL COMMENT 'nombre de "e" masculins (em)',
  `NBR_EF` int(5) NOT NULL COMMENT 'nombre de "e" féminins (ef)',
  `NBR_EE` int(5) NOT NULL COMMENT 'nombre de "e" élidés (ee)',
  `NBR_EI` int(5) NOT NULL COMMENT 'nombre de "e" ignorés (ei)',
  `NBR_EC` int(5) NOT NULL COMMENT 'nombre de "e" écartés (ec)',
  `NBR_DI` int(5) NOT NULL COMMENT 'nombre de diérèses',
  `NBR_DI_I` int(5) NOT NULL COMMENT 'nombre de diérèses en [i]',
  `NBR_DI_U` int(5) NOT NULL COMMENT 'nombre de diérèses en [u]',
  `NBR_DI_Y` int(5) NOT NULL COMMENT 'nombre de diérèses en [y]',
  `NBR_DI_DICT` int(5) NOT NULL COMMENT 'nombre de diérèses ayant fait l''objet d''une intervention',
  `NBR_DI_I_DICT` int(5) NOT NULL COMMENT 'nombre de diérèses en [i] ayant fait l''objet d''une intervention',
  `NBR_DI_U_DICT` int(5) NOT NULL COMMENT 'nombre de diérèses en [u] ayant fait l''objet d''une intervention',
  `NBR_DI_Y_DICT` int(5) NOT NULL COMMENT 'nombre de diérèses en [y] ayant fait l''objet d''une intervention',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  PRIMARY KEY (`ID_POEME`),
  UNIQUE KEY `ID_POEME` (`ID_POEME`),
  KEY `MOT` (`ID_AUTEUR`,`ID_RECUEIL`,`ID_POEME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table de données statistiques du corpus analysé';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_HE_vers`
--

DROP TABLE IF EXISTS `analyse_HE_vers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_HE_vers` (
  `ID` varchar(13) NOT NULL COMMENT 'identifiant du vers',
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) DEFAULT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) DEFAULT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) DEFAULT NULL COMMENT 'type de texte : recueil de poèmes ou pièce de théâtre',
  `NUM_ABS` int(5) NOT NULL COMMENT 'numéro absolu du vers',
  `VERS` text NOT NULL COMMENT 'texte du vers',
  `VERS_REST` varchar(1) NOT NULL COMMENT 'r = vers restitué (vers réparti sur plusieurs lignes)',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date de traitement',
  PRIMARY KEY (`ID`),
  KEY `index2` (`ID_POEME`),
  KEY `index4` (`ID_RECUEIL`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Table des vers';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_LG_nbr`
--

DROP TABLE IF EXISTS `analyse_LG_nbr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_LG_nbr` (
  `ID` varchar(12) NOT NULL COMMENT 'identifiant : poème, pièce ou strophe',
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `NBR_TOTAL` int(11) NOT NULL COMMENT 'nombre total de vers',
  `NBR_STROPHE` int(11) NOT NULL COMMENT 'nombre de strophes',
  `NUM_STROPHE` int(11) NOT NULL COMMENT 'numéro de la strophe',
  `NBR_VERS` int(11) NOT NULL COMMENT 'nombre de vers',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`,`ID_POEME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table du dénombrement des strophes et des vers';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_LM_types`
--

DROP TABLE IF EXISTS `analyse_LM_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_LM_types` (
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `TYPE` varchar(2) NOT NULL COMMENT 'type métrique : s = vers simple, c = vers complexes, m = monométrie, p = polymétrie',
  `LM` int(2) NOT NULL COMMENT 'longueur métrique',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  PRIMARY KEY (`ID_POEME`),
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`,`TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des longueurs métriques et des types métriques';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_LM_vers`
--

DROP TABLE IF EXISTS `analyse_LM_vers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_LM_vers` (
  `ID` varchar(12) NOT NULL COMMENT 'identifiant du vers',
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `NUM_ABS` int(5) unsigned NOT NULL COMMENT 'numéro absolu du vers',
  `NUM_REL` varchar(8) NOT NULL COMMENT 'numéro relatif du vers (strophe et vers)',
  `LM` int(2) NOT NULL COMMENT 'longueur métrique du vers',
  `VERS` text NOT NULL COMMENT 'texte du vers',
  `VERS_REST` varchar(1) NOT NULL COMMENT 'r = vers restitué (vers réparti sur plusieurs lignes)',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`,`ID_POEME`,`LM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des vers avec leur longueur métrique';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_NS_abs`
--

DROP TABLE IF EXISTS `analyse_NS_abs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_NS_abs` (
  `ID` varchar(12) NOT NULL COMMENT 'identifiant du vers',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `NUM_ABS` int(5) NOT NULL COMMENT 'numéro absolu du vers',
  `VERS` text NOT NULL COMMENT 'texte du vers',
  `VERS_REST` varchar(1) NOT NULL COMMENT 'r = vers restitué (vers réparti sur plusieurs lignes)',
  `P1` varchar(2) NOT NULL COMMENT 'place 1',
  `P2` varchar(2) NOT NULL COMMENT 'place 2',
  `P3` varchar(2) NOT NULL COMMENT 'place 3',
  `P4` varchar(2) NOT NULL COMMENT 'place 4',
  `P5` varchar(2) NOT NULL COMMENT 'place 5',
  `P6` varchar(2) NOT NULL COMMENT 'place 6',
  `P7` varchar(2) NOT NULL COMMENT 'place 7',
  `P8` varchar(2) NOT NULL COMMENT 'place 8',
  `P9` varchar(2) NOT NULL COMMENT 'place 9',
  `P10` varchar(2) NOT NULL COMMENT 'place 10',
  `P11` varchar(2) NOT NULL COMMENT 'place 11',
  `P12` varchar(2) NOT NULL COMMENT 'place 12',
  `P13` varchar(2) NOT NULL COMMENT 'place 13',
  `P14` varchar(2) NOT NULL COMMENT 'place 14',
  `P15` varchar(2) NOT NULL COMMENT 'place 15',
  `P16` varchar(2) NOT NULL COMMENT 'place 16',
  `P17` varchar(2) NOT NULL COMMENT 'place 17',
  `P18` varchar(2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des vers avec le type de leurs noyaux syllabiques (avant analyse métrique)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_NS_met`
--

DROP TABLE IF EXISTS `analyse_NS_met`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_NS_met` (
  `ID` varchar(10) NOT NULL COMMENT 'identifiant du vers',
  `DATE` date NOT NULL,
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `NUM_ABS` int(5) NOT NULL COMMENT 'numéro absolu du vers',
  `VERS` text NOT NULL COMMENT 'texte du vers',
  `VERS_REST` varchar(1) NOT NULL COMMENT 'r = vers restitué (vers réparti sur plusieurs lignes)',
  `P1` varchar(2) NOT NULL COMMENT 'place 1',
  `P2` varchar(2) NOT NULL COMMENT 'place 2',
  `P3` varchar(2) NOT NULL COMMENT 'place 3',
  `P4` varchar(2) NOT NULL COMMENT 'place 4',
  `P5` varchar(2) NOT NULL COMMENT 'place 5',
  `P6` varchar(2) NOT NULL COMMENT 'place 6',
  `P7` varchar(2) NOT NULL COMMENT 'place 7',
  `P8` varchar(2) NOT NULL COMMENT 'place 8',
  `P9` varchar(2) NOT NULL COMMENT 'place 9',
  `P10` varchar(2) NOT NULL COMMENT 'place 10',
  `P11` varchar(2) NOT NULL COMMENT 'place 11',
  `P12` varchar(2) NOT NULL COMMENT 'place 12',
  `P13` varchar(2) NOT NULL COMMENT 'place 13',
  `P14` varchar(2) NOT NULL COMMENT 'place 14',
  `P15` varchar(2) NOT NULL COMMENT 'place 15',
  `P16` varchar(2) NOT NULL COMMENT 'place 16',
  `P17` varchar(2) NOT NULL COMMENT 'place 17',
  `P18` varchar(2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des vers avec le type de leurs noyaux syllabiques (après analyse métrique)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_PT_dis`
--

DROP TABLE IF EXISTS `analyse_PT_dis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_PT_dis` (
  `ID` varchar(12) NOT NULL COMMENT 'identifiant du vers',
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `NUM_ABS` int(5) NOT NULL COMMENT 'numéro absolu du vers',
  `LM` int(2) NOT NULL COMMENT 'longueur métrique du vers',
  `METRE` varchar(5) NOT NULL COMMENT 'mètre du vers',
  `GENRE` varchar(1) NOT NULL COMMENT 'genre du vers',
  `MOY_VERS` decimal(10,2) NOT NULL COMMENT 'moyenne ponctuométrique du vers',
  `MOY_SOUS_VERS` varchar(20) NOT NULL COMMENT 'moyenne ponctuométrique des sous-vers',
  `INITIALE` varchar(5) NOT NULL COMMENT 'signe typographique en début de vers',
  `P1` varchar(2) NOT NULL COMMENT 'place 1',
  `P2` varchar(2) NOT NULL COMMENT 'place 2',
  `P3` varchar(2) NOT NULL COMMENT 'place 3',
  `P4` varchar(2) NOT NULL COMMENT 'place 4',
  `P5` varchar(2) NOT NULL COMMENT 'place 5',
  `P6` varchar(2) NOT NULL COMMENT 'place 6',
  `P7` varchar(2) NOT NULL COMMENT 'place 7',
  `P8` varchar(2) NOT NULL COMMENT 'place 8',
  `P9` varchar(2) NOT NULL COMMENT 'place 9',
  `P10` varchar(2) NOT NULL COMMENT 'place 10',
  `P11` varchar(2) NOT NULL COMMENT 'place 11',
  `P12` varchar(2) NOT NULL COMMENT 'place 12',
  `P13` varchar(2) NOT NULL COMMENT 'place 13',
  `P14` varchar(2) NOT NULL COMMENT 'place 14',
  `P15` varchar(2) NOT NULL COMMENT 'place 15',
  `P16` varchar(2) NOT NULL COMMENT 'place 16',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  PRIMARY KEY (`ID`),
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`,`ID_POEME`,`LM`,`METRE`,`GENRE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table de la ponctuométrie : distribution des valeurs selon la place métrique';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_PT_frq`
--

DROP TABLE IF EXISTS `analyse_PT_frq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_PT_frq` (
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `LM` int(2) NOT NULL COMMENT 'longueur métrique',
  `METRE` varchar(5) NOT NULL COMMENT 'mètre du vers',
  `NBR_PCT_TOTAL` int(4) NOT NULL COMMENT 'nombre total de signes de ponctuation dans le poème',
  `NBR_PCT_LM` int(4) NOT NULL COMMENT 'nombre total de signes de ponctuation de la longueur métrique',
  `NBR_PCT_MET` int(4) NOT NULL COMMENT 'nombre total de signes de ponctuation du mètre',
  `FILTRE` varchar(6) NOT NULL COMMENT 'total, mode, 9, 6 ou 3',
  `NBR_PCT_VERS` int(4) NOT NULL COMMENT 'nombre total de signes correspondant au FILTRE par vers',
  `NBR_PCT_SOUS_VERS` varchar(15) NOT NULL COMMENT 'nombre total de signes correspondant au FILTRE par sous-vers',
  `P1` int(4) NOT NULL COMMENT 'place 1',
  `P2` int(4) NOT NULL COMMENT 'place 2',
  `P3` int(4) NOT NULL COMMENT 'place 3',
  `P4` int(4) NOT NULL COMMENT 'place 4',
  `P5` int(4) NOT NULL COMMENT 'place 5',
  `P6` int(4) NOT NULL COMMENT 'place 6',
  `P7` int(4) NOT NULL COMMENT 'place 7',
  `P8` int(4) NOT NULL COMMENT 'place 8',
  `P9` int(4) NOT NULL COMMENT 'place 9',
  `P10` int(4) NOT NULL COMMENT 'place 10',
  `P11` int(4) NOT NULL COMMENT 'place 11',
  `P12` int(4) NOT NULL COMMENT 'place 12',
  `P13` int(4) NOT NULL COMMENT 'place 13',
  `P14` int(4) NOT NULL COMMENT 'place 14',
  `P15` int(4) NOT NULL COMMENT 'place 15',
  `P16` int(4) NOT NULL COMMENT 'place 16',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`,`ID_POEME`,`LM`,`METRE`,`FILTRE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table de la ponctuométrie : fréquences des signes de ponctuation selon la place métrique';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_PT_moy`
--

DROP TABLE IF EXISTS `analyse_PT_moy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_PT_moy` (
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `LM` int(2) NOT NULL COMMENT 'longueur métrique du vers',
  `METRE` varchar(5) NOT NULL COMMENT 'mètre',
  `NBR_VERS_TOTAL` varchar(4) NOT NULL COMMENT 'nombre total de vers du poème ou de la pièce',
  `NBR_VERS_LM` int(4) NOT NULL COMMENT 'nombre de vers de la longueur métrique',
  `NBR_VERS_MET` int(4) NOT NULL COMMENT 'nombre de vers du mètre',
  `MOY_VERS` decimal(10,2) NOT NULL COMMENT 'moyenne ponctuométrique du vers',
  `MOY_SOUS_VERS` varchar(15) NOT NULL COMMENT 'moyenne ponctuométrique des sous-vers',
  `P1` decimal(10,2) NOT NULL COMMENT 'place 1',
  `P2` decimal(10,2) NOT NULL COMMENT 'place 2',
  `P3` decimal(10,2) NOT NULL COMMENT 'place 3',
  `P4` decimal(10,2) NOT NULL COMMENT 'place 4',
  `P5` decimal(10,2) NOT NULL COMMENT 'place 5',
  `P6` decimal(10,2) NOT NULL COMMENT 'place 6',
  `P7` decimal(10,2) NOT NULL COMMENT 'place 7',
  `P8` decimal(10,2) NOT NULL COMMENT 'place 8',
  `P9` decimal(10,2) NOT NULL COMMENT 'place 9',
  `P10` decimal(10,2) NOT NULL COMMENT 'place 10',
  `P11` decimal(10,2) NOT NULL COMMENT 'place 11',
  `P12` decimal(10,2) NOT NULL COMMENT 'place 12',
  `P13` decimal(10,2) NOT NULL COMMENT 'place 13',
  `P14` decimal(10,2) NOT NULL COMMENT 'place 14',
  `P15` decimal(10,2) NOT NULL COMMENT 'place 15',
  `P16` decimal(10,2) NOT NULL COMMENT 'place 16',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`,`ID_POEME`,`LM`,`METRE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table de la ponctuométrie : moyennes des valeurs selon la place métrique';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_PT_nbr`
--

DROP TABLE IF EXISTS `analyse_PT_nbr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_PT_nbr` (
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `LM` int(2) NOT NULL COMMENT 'longueur métrique',
  `METRE` varchar(5) NOT NULL COMMENT 'mètre',
  `SIGNE_PCT` varchar(2) NOT NULL COMMENT 'signe de ponctuation',
  `NBR_TOTAL_PCT` int(5) NOT NULL COMMENT 'nombre total de signes selon SIGN_PCT',
  `P1` decimal(10,2) NOT NULL COMMENT 'place 1',
  `P2` decimal(10,2) NOT NULL COMMENT 'place 2',
  `P3` decimal(10,2) NOT NULL COMMENT 'place 3',
  `P4` decimal(10,2) NOT NULL COMMENT 'place 4',
  `P5` decimal(10,2) NOT NULL COMMENT 'place 5',
  `P6` decimal(10,2) NOT NULL COMMENT 'place 6',
  `P7` decimal(10,2) NOT NULL COMMENT 'place 7',
  `P8` decimal(10,2) NOT NULL COMMENT 'place 8',
  `P9` decimal(10,2) NOT NULL COMMENT 'place 9',
  `P10` decimal(10,2) NOT NULL COMMENT 'place 10',
  `P11` decimal(10,2) NOT NULL COMMENT 'place 11',
  `P12` decimal(10,2) NOT NULL COMMENT 'place 12',
  `P13` decimal(10,2) NOT NULL COMMENT 'place 13',
  `P14` decimal(10,2) NOT NULL COMMENT 'place 14',
  `P15` decimal(10,2) NOT NULL COMMENT 'place 15',
  `P16` decimal(10,2) NOT NULL COMMENT 'place 16',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`,`ID_POEME`,`LM`,`METRE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table de la ponctuométrie : dénombrement des signes de ponctuation selon la place métrique';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_PT_pct`
--

DROP TABLE IF EXISTS `analyse_PT_pct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_PT_pct` (
  `ID` varchar(12) NOT NULL COMMENT 'identifiant du vers',
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `NUM_ABS` int(5) NOT NULL COMMENT 'numéro absolu du vers',
  `LM` int(2) NOT NULL COMMENT 'longueur métrique du vers',
  `VERS_REST` varchar(1) NOT NULL COMMENT 'r = vers restitué (vers réparti sur plusieurs lignes)',
  `VERS` text NOT NULL COMMENT 'texte du vers',
  `P1` decimal(10,2) NOT NULL COMMENT 'place 1',
  `P2` decimal(10,2) NOT NULL COMMENT 'place 2',
  `P3` decimal(10,2) NOT NULL COMMENT 'place 3',
  `P4` decimal(10,2) NOT NULL COMMENT 'place 4',
  `P5` decimal(10,2) NOT NULL COMMENT 'place 5',
  `P6` decimal(10,2) NOT NULL COMMENT 'place 6',
  `P7` decimal(10,2) NOT NULL COMMENT 'place 7',
  `P8` decimal(10,2) NOT NULL COMMENT 'place 8',
  `P9` decimal(10,2) NOT NULL COMMENT 'place 9',
  `P10` decimal(10,2) NOT NULL COMMENT 'place 10',
  `P11` decimal(10,2) NOT NULL COMMENT 'place 11',
  `P12` decimal(10,2) NOT NULL COMMENT 'place 12',
  `P13` decimal(10,2) NOT NULL COMMENT 'place 13',
  `P14` decimal(10,2) NOT NULL COMMENT 'place 14',
  `P15` decimal(10,2) NOT NULL COMMENT 'place 15',
  `P16` decimal(10,2) NOT NULL COMMENT 'place 16',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`,`ID_POEME`,`LM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table de la ponctuométrie : distribution des signes de ponctuation selon la place métrique';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_QR_poèmes`
--

DROP TABLE IF EXISTS `analyse_QR_poèmes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_QR_poèmes` (
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `NBR_VERS` int(11) DEFAULT NULL COMMENT 'nombre total de vers du poème',
  `MODE_QR` int(11) NOT NULL COMMENT 'mode de l''indice QR de la rime',
  `EFF_MODE_QR` varchar(50) NOT NULL COMMENT 'effectif du mode_QR',
  `PCENT_MODE_QR` decimal(10,2) NOT NULL COMMENT 'pourcentage du mode_QR',
  `MAX_QR` varchar(3) NOT NULL COMMENT 'valeur maximale de l''indice QR',
  `MOY_QR` decimal(10,2) NOT NULL COMMENT 'moyenne de l''indice QR',
  `ECART_QR` decimal(10,2) NOT NULL COMMENT 'écart-type de l''indice QR',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`,`ID_POEME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des valeurs calculées de la "qualité" des rimes par poème';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_QR_rimes`
--

DROP TABLE IF EXISTS `analyse_QR_rimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_QR_rimes` (
  `ID` varchar(12) NOT NULL COMMENT 'identifiant de la rime',
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `NUM_INSERT` varchar(2) DEFAULT NULL COMMENT 'numéro du poème inséré dans une pièce de théâtre',
  `MODIF` varchar(1) DEFAULT NULL COMMENT 'f = rime forcée, o = rime orpheline, n = pas de rime',
  `NUM_RIME` int(5) NOT NULL COMMENT 'numéro de la rime',
  `GENRE` set('m','f','mf') NOT NULL COMMENT 'genre de la rime',
  `APPEL` varchar(50) NOT NULL COMMENT 'mot-rime en appel',
  `ECHO` varchar(50) NOT NULL COMMENT 'mot-rime en écho',
  `REGLE` varchar(7) NOT NULL COMMENT 'identifiant de la règle d''appariement',
  `TYPE_RIME` varchar(1) NOT NULL COMMENT 'type de la rime',
  `QR` int(11) DEFAULT NULL COMMENT 'indice QR de la rime',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  PRIMARY KEY (`ID`),
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`,`ID_POEME`,`MODIF`,`GENRE`,`TYPE_RIME`,`QR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table du calcul de la "qualité" des rimes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_QR_types`
--

DROP TABLE IF EXISTS `analyse_QR_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_QR_types` (
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `TYPE_RIME` varchar(1) DEFAULT NULL COMMENT 'type de rime',
  `EFF_TYPE` int(11) DEFAULT NULL COMMENT 'effectif correspondant au type',
  `PCENT_EFF_TYPE` decimal(10,2) NOT NULL COMMENT 'pourcentage des rimes du type',
  `QR` int(11) NOT NULL COMMENT 'indice QR de la rime',
  `QUOTIENT_QR` decimal(10,2) NOT NULL COMMENT 'QUOTIENT_QR = indice QR/nombre total de vers',
  `EXEMPLE_QR` varchar(100) NOT NULL COMMENT 'exemple de rime du type',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`,`ID_POEME`,`TYPE_RIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des valeurs calculées de la "qualité" des rimes selon leur type';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_RI_rimes`
--

DROP TABLE IF EXISTS `analyse_RI_rimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_RI_rimes` (
  `ID` varchar(12) NOT NULL COMMENT 'identifiant de la rime',
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `NUM_INSERT` int(3) DEFAULT NULL COMMENT 'numéro du poème inséré dans une pièce de théâtre',
  `DICT_RI` varchar(1) DEFAULT NULL COMMENT 'f = rime forcée, o = rime orpheline, n = pas de rime',
  `NUM_APPARIEMENT` varchar(20) NOT NULL COMMENT 'numéro d''appariement',
  `NUM_RIME` int(5) NOT NULL COMMENT 'numéro de la rime',
  `NUM_APPEL` int(11) NOT NULL COMMENT 'numéro du vers en appel',
  `NUM_ECHO` int(11) NOT NULL COMMENT 'numéro du vers en écho',
  `ORIENTATION` set('p','r') NOT NULL COMMENT 'p = appariement progressif, r = appariement régressif',
  `GENRE` set('m','f','mf') NOT NULL COMMENT 'genre de la rime',
  `APPEL` varchar(50) NOT NULL COMMENT 'mot-rime en appel',
  `ECHO` varchar(50) NOT NULL COMMENT 'mot-rime en écho',
  `TYPE_RIME` varchar(1) NOT NULL COMMENT 'type d''appariemment',
  `REGLE` varchar(7) NOT NULL COMMENT 'règle dans l''algorithme d''appariement des vers en rimes',
  `ETAPE_RI` int(11) NOT NULL COMMENT 'Numéro d''étape dans l''algorithme d''appariement des vers en rimes',
  `DISTANCE_VERS` int(11) DEFAULT NULL COMMENT 'distance entre l''appel et l''écho (nombre de vers)',
  `DISTANCE_RIMES` int(11) NOT NULL COMMENT 'nombre de rimes séparant l''appel de son écho',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  PRIMARY KEY (`ID`),
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`,`ID_POEME`,`DICT_RI`,`GENRE`,`TYPE_RIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des rimes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_RI_vers`
--

DROP TABLE IF EXISTS `analyse_RI_vers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_RI_vers` (
  `ID` varchar(12) NOT NULL COMMENT 'identifiant du vers',
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `NUM_INSERT` int(3) DEFAULT NULL COMMENT 'numéro du poème inséré dans une pièce de théâtre',
  `MODIF` int(3) DEFAULT NULL COMMENT 'numéro du vers (appel ou écho) en cas de rime forcée',
  `NUM_ABS` int(5) NOT NULL COMMENT 'numéro absolu du vers',
  `NUM_REL` varchar(8) NOT NULL COMMENT 'numéro relatif du vers (strophe et vers)',
  `VERS` text NOT NULL COMMENT 'texte du vers',
  `VERS_REST` varchar(1) NOT NULL COMMENT 'r = vers restitué (vers réparti sur plusieurs lignes)',
  `MOT` varchar(50) NOT NULL COMMENT 'mot à la rime (mot entre crochets = mot restitué)',
  `GENRE` varchar(1) NOT NULL COMMENT 'genre de la rime',
  `PHONEME` varchar(5) NOT NULL COMMENT 'phonème de la voyelle',
  `GRAPHEME` varchar(5) NOT NULL COMMENT 'graphème de la voyelle',
  `CONS_SUB` varchar(5) NOT NULL COMMENT 'consonne(s) subséquente(s)',
  `CONS_FIN` varchar(3) NOT NULL COMMENT 'consonne(s) finale(s)',
  `APPEL_ECHO` varchar(1) NOT NULL COMMENT 'a = appel e = écho',
  `NUM_TIMBRE` int(4) NOT NULL COMMENT 'numéro du timbre',
  `NUM_RIME_RED` int(5) NOT NULL COMMENT 'numéro du timbre réduit (après appariement)',
  `NUM_APPARIEMENT` varchar(20) NOT NULL COMMENT 'numéro d''appariement',
  `NUM_RIME` int(5) NOT NULL COMMENT 'numéro de la rime',
  `RIME` text COMMENT 'couple rimique (avec numéro absolu du vers)',
  `LETTRE` varchar(1) NOT NULL COMMENT 'étiquette de la rime dans le schéma de rimes)',
  `TYPE_RIME` varchar(1) NOT NULL COMMENT 'type d''appariement',
  `REGLE` varchar(20) DEFAULT NULL COMMENT 'règle dans l''algorithme d''appariement des vers en rimes',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  PRIMARY KEY (`ID`),
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`,`ID_POEME`,`MOT`,`GENRE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table analytique du contenu de la rime vers par vers';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analyse_ST`
--

DROP TABLE IF EXISTS `analyse_ST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyse_ST` (
  `ID` varchar(12) NOT NULL COMMENT 'identifiant du vers',
  `ID_AUTEUR` varchar(3) NOT NULL COMMENT 'identifiant de l''auteur',
  `ID_RECUEIL` varchar(6) NOT NULL COMMENT 'identifiant du recueil',
  `ID_POEME` varchar(7) NOT NULL COMMENT 'identifiant du poème ou de la pièce',
  `TYPE_TEXTE` varchar(5) NOT NULL COMMENT 'poème ou pièce',
  `NUM_INSERT` int(3) DEFAULT NULL COMMENT 'numéro du poème inséré dans une pièce de théâtre',
  `NUM_STROPHE` int(3) NOT NULL COMMENT 'numéro de la strophe',
  `FORME` varchar(100) NOT NULL COMMENT 'forme globale',
  `NBR_VERS` int(11) NOT NULL COMMENT 'nombre de vers dans la strophe',
  `TYPE_FORME` varchar(100) NOT NULL COMMENT 'type de la strophe (quantain) ou type de forme fixe',
  `DESCRIPTION` text NOT NULL COMMENT 'schéma de rimes pour une strophe et sous-type pour une forme fixe',
  `SCHEMA_COMPLET` text NOT NULL COMMENT 'schéma de rimes pour l''ensemble du poème ou de la pièce',
  `SCHEMA_REDUIT` text NOT NULL COMMENT 'schéma de rimes factorisé',
  `REGLE` varchar(50) NOT NULL COMMENT 'règle d''identification de la forme globale',
  `NUM_SCHEMA` int(3) NOT NULL COMMENT 'numéro du schéma de rimes dans la ressource des formes globales',
  `PROTOTYPE` text NOT NULL COMMENT 'exemple prototypique dans la ressource des formes globales',
  `DATE_TRAITEMENT` date NOT NULL COMMENT 'date du traitement',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  KEY `ID_AUTEUR` (`ID_AUTEUR`,`ID_RECUEIL`,`ID_POEME`,`FORME`,`TYPE_FORME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des formes globales et des types strophiques';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-15  9:27:26
