
<div align="center">
<center><h2>Relevés métriques</h2></center>
</div>

<div align="center">
<center><h3>au format CSV</h3></center>
</div>




##

Les tables ont été générées automatiquement par les programmes d’analyse métrique du projet.
Ces programmes sont disponibles sur le dépôt Gitlab :
https://git.unicaen.fr/malherbe/programmes

Les tables sont au format CSV : champs séparés par une tabulation (\t) et sans guillemets de
délimitation du texte dans les champs.
Chaque table est accompagnée d’un fichier descriptif qui explicite le codage et le contenu des champs.

Les relevés portent sur l'analyse :

- des noyaux syllabiques (répertoire NS)
- des diérèses (répertoire DI)
- de la longueur métrique des vers (répertoire LM)
- du profil métrique des poèmes et du mètre des vers (répertoire CM)
- de l'appariement des vers en rimes et du schéma rimique (répertoire RI)
- de la forme globale des poèmes (répertoire ST)
- de l'extension des rimes (répertoire ER)
- de la qualité des rimes (répertoire QR)
- de la ponctuométrie (répertoire PT)
- du dénombrement des strophes (répertoire LG)

Une table des vers reconstitués ainsi qu'une table de propriétés métriques rapportées
aux mots du corpus (répertoire HE).


Les différents relevés métriques sont proposés :

- pour la totalité du corpus
- pour un auteur
- pour un recueil de poésie
- pour un poème ou une pièce de théâtre

Exemples (analyse des diérèses):

- analyse_DI.csv (tout le corpus)
- analyse_DI_BAU.csv (Charles Baudelaire)
- analyse_DI_BAU_1.csv (Les Fleurs du mal)
- analyse_DI_BAU1.csv (Au Lecteur)

▪ Ce répertoire contient :


▪ Chaque sous-répertoire consacré à une étape du traitement automatique contient :
- un fichier pdf de description des champs

▪ ** liste des tables au format CSV**




