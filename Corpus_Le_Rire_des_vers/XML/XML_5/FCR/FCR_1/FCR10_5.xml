<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA MÉTEMPSYCOSE</title>
				<title type="sub">BÊTISE EN UN ACTE, MÊLÉE DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="COU" sort="1">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<author key="JAI" sort="2">
					<name>
						<forename>Ernest</forename>
						<surname>JAIME</surname>
					</name>
					<date from="1804" to="1884">1804-1884</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>282 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FCR_1</idno>	
				<availability status="free">
					<licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA MÉTEMPSYCOSE</title>
						<author>FRÉDÉRIC DE COURCY ET JAIME</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=c1RoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032819917.0x000001# ?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">7 FÉVRIER 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FCR10" modus="sp" lm_max="8">
	<head type="tune">AIR : Des Adieux aux Fillettes.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8"><w n="1.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rd</w> <w n="1.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r</w> <w n="1.4">l<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>rs</w> <w n="1.5" punct="vg:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>ci<seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="vg">er</seg>s</w>,</l>
		<l n="2" num="1.2" lm="8"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="2.2">Gr<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>cs</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="2.4">p<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg></w> <w n="2.5" punct="vg:8">c<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rn<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>ssi<seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="vg">er</seg>s</w>,</l>
		<l n="3" num="1.3" lm="8"><w n="3.1">Cr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="3.2">d</w>'<w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>r</w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="8">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
		<l n="4" num="1.4" lm="8"><w n="4.1">D</w>'<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>r</w> <w n="4.3">m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>s</w> <w n="4.4">l<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="4.5">p<seg phoneme="ɛ" type="vs" value="1" rule="409" place="5">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="4.8" punct="pe:8">br<seg phoneme="ɔ" type="vs" value="1" rule="438" place="8">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> ! <del reason="analysis" type="repetition" hand="KL">( bis)</del></l>
		<l n="5" num="1.5" lm="8"><w n="5.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2" punct="vg:2"><seg phoneme="ø" type="vs" value="1" rule="397" place="2" punct="vg">eu</seg>x</w>, <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>rd</w>'<w n="5.4" punct="vg:5">hu<seg phoneme="i" type="vs" value="1" rule="490" place="5" punct="vg">i</seg></w>, <w n="5.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>s</w></l>
		<l n="6" num="1.6" lm="8"><w n="6.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>d<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="6.2"><seg phoneme="y" type="vs" value="1" rule="452" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.3">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="6.4" punct="dp:8">ch<seg phoneme="o" type="vs" value="1" rule="443" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
		<l n="7" num="1.7" lm="8"><w n="7.1">S<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>t</w>-<w n="7.2" punct="vg:2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg></w>, <w n="7.3" punct="pe:4">h<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="pe">a</seg>s</w> ! <w n="7.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="7.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="7.6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>ge<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>s</w></l>
		<l n="8" num="1.8" lm="8"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="8.2">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="8.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rds</w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="8.5">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="8.6" punct="ps:8">p<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ge<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="ps">on</seg>s</w>…</l>
	</lg>
	<lg n="2">
	<head type="speaker">ENSEMBLE.</head>
		<l n="9" num="2.1" lm="6"><w n="9.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2" punct="pe:6">m<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rph<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></w> !</l>
		<l n="10" num="2.2" lm="6"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="10.3">m<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">em</seg>ps<seg phoneme="i" type="vs" value="1" rule="492" place="5">y</seg>c<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
		<l n="11" num="2.3" lm="2"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2" punct="vg:2">cr<seg phoneme="wa" type="vs" value="1" rule="422" place="2" punct="vg">oi</seg></w>,</l>
		<l n="12" num="2.4" lm="3"><w n="12.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg></w> <w n="12.2" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="3" punct="vg">oi</seg></w>,</l>
		<l n="13" num="2.5" lm="7"><w n="13.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="13.2">j</w>'<w n="13.3"><seg phoneme="i" type="vs" value="1" rule="496" place="2">y</seg></w> <w n="13.4">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>s</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.6">b<seg phoneme="ɔ" type="vs" value="1" rule="418" place="5">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.7" punct="pt:7">f<seg phoneme="wa" type="vs" value="1" rule="422" place="7" punct="pt">oi</seg></w>.</l>
	</lg>
	<lg n="3">
	<head type="speaker">ERNEST.</head>
		<l n="14" num="3.1" lm="8"><w n="14.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">vi<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="14.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rd</w> <w n="14.4">f<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>t</w> <w n="14.5" punct="vg:8">pr<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>c<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>r</w>,</l>
		<l n="15" num="3.2" lm="8"><w n="15.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">in</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="15.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="15.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>d</w> <w n="15.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>ct<seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>r</w>,</l>
		<l n="16" num="3.3" lm="8"><w n="16.1">C<seg phoneme="ɛ" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="16.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="16.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t</w> <w n="16.4">l</w>'<w n="16.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="16.7">s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="16.8" punct="vg:8">b<seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="17" num="3.4" lm="8"><w n="17.1">J<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>s</w> <w n="17.2">f<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>t</w> <w n="17.3">d<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>ct<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>r</w> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="17.5" punct="pv:8">S<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rb<seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ; <del reason="analysis" type="repetition" hand="KL">( bis)</del></l>
		<l n="18" num="3.5" lm="8"><w n="18.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">gr<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>s</w> <w n="18.3" punct="vg:5">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rr<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="189" place="5" punct="vg">e</seg>t</w>, <w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="18.5" punct="vg:8">n<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="vg">ou</seg>s</w>,</l>
		<l n="19" num="3.6" lm="8"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="19.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>v<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>c<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>t</w> <w n="19.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</w> <w n="19.5">c<seg phoneme="o" type="vs" value="1" rule="317" place="8">au</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
		<l n="20" num="3.7" lm="8"><w n="20.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="20.2">d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t</w> <w n="20.3">qu</w>'<w n="20.4"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>s</w> <w n="20.5">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="20.6">c<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>s</w></l>
		<l n="21" num="3.8" lm="8"><w n="21.1">F<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="21.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="21.3" punct="vg:5">p<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="21.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s</w> <w n="21.5" punct="ps:8"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="ps">ou</seg>x</w>…</l>
	</lg>
	<lg n="4">
		<head type="speaker">ENSEMBLE. </head>
		<l n="22" num="4.1" lm="6"><w n="22.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="22.2" punct="pe:6">m<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rph<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></w> !</l>
		<l n="23" num="4.2" lm="6"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="23.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="23.3" punct="vg:6">m<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">em</seg>ps<seg phoneme="i" type="vs" value="1" rule="492" place="5">y</seg>c<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>, <del reason="analysis" type="repetition" hand="RR">etc</del>.</l>
	</lg>
</div></body></text></TEI>