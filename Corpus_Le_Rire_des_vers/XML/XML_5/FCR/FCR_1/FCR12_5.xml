<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA MÉTEMPSYCOSE</title>
				<title type="sub">BÊTISE EN UN ACTE, MÊLÉE DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="COU" sort="1">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<author key="JAI" sort="2">
					<name>
						<forename>Ernest</forename>
						<surname>JAIME</surname>
					</name>
					<date from="1804" to="1884">1804-1884</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>282 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FCR_1</idno>	
				<availability status="free">
					<licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA MÉTEMPSYCOSE</title>
						<author>FRÉDÉRIC DE COURCY ET JAIME</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=c1RoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032819917.0x000001# ?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">7 FÉVRIER 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FCR12" modus="sm" lm_max="8">
	<head type="tune">AIR : Ah ! si Madame me voyait.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="1.2">pr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409" place="5">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="1.4" punct="vg:8">r<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="vg">oi</seg></w>,</l>
		<l n="2" num="1.2" lm="8"><w n="2.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="1">ain</seg>t</w> <w n="2.2">l<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4" punct="vg:8">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rc<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
		<l n="3" num="1.3" lm="8"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2" punct="vg:4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4" punct="vg:8">di<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
		<l n="4" num="1.4" lm="8"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>t</w> <w n="4.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>t</w> <w n="4.4">l</w>'<w n="4.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="352" place="7">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="vg">oi</seg></w>,</l>
		<l n="5" num="1.5" lm="8"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="5.3">li<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="5.4">r<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="5.5">l</w>'<w n="5.6" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="352" place="7">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pt">oi</seg></w>.</l>
		<l n="6" num="1.6" lm="8"><w n="6.1">C</w>'<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="6.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="2">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="6.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5">j<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>s</w> <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="6.7" punct="vg:8">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="7" num="1.7" lm="8"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">l</w>'<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r</w> <w n="7.4">s<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>t</w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="7.6" punct="vg:8">l<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="vg">oi</seg></w>,</l>
		<l n="8" num="1.8" lm="8"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">p<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="8.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="8.5">l</w>'<w n="8.6" punct="ps:8"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
		<l n="9" num="1.9" lm="8"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="9.2">pr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409" place="5">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="9.4" punct="pt:8">r<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pt">oi</seg></w>. <del reason="analysis" type="repetition" hand="KL">( bis.)</del></l>
	</lg>
	<lg n="2">
	<head type="speaker">JULES.</head>
		<l n="10" num="2.1" lm="8"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="10.2">pr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409" place="5">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="10.4" punct="vg:8">r<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="vg">oi</seg></w>,</l>
		<l n="11" num="2.2" lm="8"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rd</w>'<w n="11.2">hu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="11.4">v<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg></w> <w n="11.5" punct="vg:8">br<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="12" num="2.3" lm="8"><w n="12.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="12.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>ri<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="12.4">qu</w>'<w n="12.5"><seg phoneme="y" type="vs" value="1" rule="452" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="12.6" punct="vg:8">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="13" num="2.4" lm="8"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="13.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>t</w> <w n="13.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="13.6" punct="ps:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>l<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="ps">oi</seg></w>…</l>
		<l n="14" num="2.5" lm="8"><w n="14.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="14.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>t</w> <w n="14.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="14.4" punct="vg:5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="5" punct="vg">en</seg></w>, <w n="14.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="14.6" punct="vg:8">m<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="vg">oi</seg></w>,</l>
		<l n="15" num="2.6" lm="8"><w n="15.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="15.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="304" place="3">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="15.3" punct="vg:5">g<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="5" punct="vg">en</seg>s</w>, <w n="15.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r</w> <w n="15.5" punct="vg:8">s<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
		<l n="16" num="2.7" lm="8"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>b<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.3">s<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="16.4">b<seg phoneme="ɔ" type="vs" value="1" rule="418" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="16.5" punct="vg:8">f<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="vg">oi</seg></w>,</l>
		<l n="17" num="2.8" lm="8"><w n="17.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="5">o</seg>r</w> <w n="17.4" punct="pt:6">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6" punct="pt vg pt">e</seg>s</w>.,. <w n="17.5" punct="ps:8">s<seg phoneme="o" type="vs" value="1" rule="434" place="7">o</seg>tt<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg>s</w>…</l>
		<l n="18" num="2.9" lm="8"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="18.2">pr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409" place="5">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="18.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="18.4" punct="pt:8">r<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pt">oi</seg></w>. <del reason="analysis" type="repetition" hand="KL">(bis).</del></l>
	</lg>
</div></body></text></TEI>