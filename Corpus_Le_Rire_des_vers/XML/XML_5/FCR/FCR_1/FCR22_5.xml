<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA MÉTEMPSYCOSE</title>
				<title type="sub">BÊTISE EN UN ACTE, MÊLÉE DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="COU" sort="1">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<author key="JAI" sort="2">
					<name>
						<forename>Ernest</forename>
						<surname>JAIME</surname>
					</name>
					<date from="1804" to="1884">1804-1884</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>282 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FCR_1</idno>	
				<availability status="free">
					<licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA MÉTEMPSYCOSE</title>
						<author>FRÉDÉRIC DE COURCY ET JAIME</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=c1RoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032819917.0x000001# ?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">7 FÉVRIER 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ENSEMBLE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FCR22" modus="cp" lm_max="10">
			<lg n="1">
			<head type="speaker">CHRISTOPHE.</head>
				<l n="1" num="1.1" lm="8"><w n="1.1">D</w>' <w n="1.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r</w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="1.5">v<seg phoneme="wa" type="vs" value="1" rule="439" place="5">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>t</w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="1.7" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="317" place="8">au</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
				<l n="2" num="1.2" lm="10"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">v<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="2.6">s<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r</w> <w n="2.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8">e</seg>s</w> <w n="2.8" punct="pv:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="9">e</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="189" place="10" punct="pv">e</seg>ts</w> ;</l>
				<l n="3" num="1.3" lm="8"><w n="3.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="3.2">gr<seg phoneme="a" type="vs" value="1" rule="339" place="2">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="3.5" punct="vg:8">m<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">em</seg>ps<seg phoneme="i" type="vs" value="1" rule="492" place="7">y</seg>c<seg phoneme="o" type="vs" value="1" rule="443" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
				<l n="4" num="1.4" lm="9"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">n</w>' <w n="4.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>b<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg></w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="4.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="4.6">v<seg phoneme="o" type="vs" value="1" rule="437" place="7">o</seg>s</w> <w n="4.7" punct="pt:9">f<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="189" place="9" punct="pt">e</seg>ts</w>.</l>
			</lg>
		</div></body></text></TEI>