<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA MÉTEMPSYCOSE</title>
				<title type="sub">BÊTISE EN UN ACTE, MÊLÉE DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="COU" sort="1">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<author key="JAI" sort="2">
					<name>
						<forename>Ernest</forename>
						<surname>JAIME</surname>
					</name>
					<date from="1804" to="1884">1804-1884</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>282 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FCR_1</idno>	
				<availability status="free">
					<licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA MÉTEMPSYCOSE</title>
						<author>FRÉDÉRIC DE COURCY ET JAIME</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=c1RoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032819917.0x000001# ?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">7 FÉVRIER 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FCR6" modus="cp" lm_max="10">
	<head type="tune">AIR : Vaudeville du Baiser au Porteur.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="1.2">m</w>' <w n="1.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.4">qu</w>' <w n="1.5">c</w>'<w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="1.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="1.8">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="1.9" punct="vg:8"><seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>s<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="2" num="1.2" lm="8"><w n="2.1" punct="vg:1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="vg">a</seg>r</w>, <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg></w> <w n="2.3">m<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4">en</seg>t</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5">s</w>'<w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w></l>
		<l n="3" num="1.3" lm="8"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="3.3">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="4">en</seg>s</w> <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg></w> <w n="3.5" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="4" num="1.4" lm="8"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="4.3">b<seg phoneme="ɛ̃" type="vs" value="1" rule="220" place="3">en</seg></w> <w n="4.4">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>s</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w></l>
		<l n="5" num="1.5" lm="8"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="5.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">g<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>r</w> <w n="5.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="5.5" punct="vg:8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="vg">er</seg></w>,</l>
		<l n="6" num="1.6" lm="8"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w>' <w n="6.2">f<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="6.3" punct="vg:3">pr<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="6.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="6.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="6.7" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="7" num="1.7" lm="10"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">v<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>t</w>' <w n="7.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="7.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="5">en</seg></w> <w n="7.5">n</w>' <w n="7.6">p<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>t</w> <w n="7.7">v<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="7.8" punct="ps:10">d<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="ps">er</seg></w>…</l>
		<l n="8" num="1.8" lm="10"><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="8.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="8.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>c</w> <w n="8.4">b<seg phoneme="ɛ̃" type="vs" value="1" rule="220" place="4">en</seg></w> <w n="8.5">d</w>' <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="352" place="6">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="338" place="7">a</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w> <w n="8.8">d</w>'<w n="8.9" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
		<l n="9" num="1.9" lm="8"><w n="9.1">Pu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">l</w>'<w n="9.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="9.4">n</w>' <w n="9.5">p<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>t</w> <w n="9.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="9.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="9.8" punct="pt:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pt">er</seg></w>.</l>
	</lg> 
</div></body></text></TEI>