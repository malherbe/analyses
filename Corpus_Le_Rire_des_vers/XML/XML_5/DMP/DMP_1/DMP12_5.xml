<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L'HOMME QUI BAT SA FEMME</title>
				<title type="sub">TABLEAU POPULAIRE EN UN ACTE, MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DML" sort="1">
				  <name>
					<forename>Julien</forename>
					<nameLink>de</nameLink>
					<surname>MAILLAN</surname>
					<addname type="other">Julien</addname>
					<addname type="other">Julien de M.</addname>
				  </name>
				  <date from="1805" to="1851">1805-1851</date>
				</author>
				<author key="DNR" sort="2">
				  <name>
					<forename>Philippe-François</forename>
					<surname>PINEL</surname>
					<addname type="pen_name">DUMANOIR</addname>
					<addname type="other">Philippe Dumanoir</addname>
					<addname type="other">Philippe D.</addname>
					<addname type="other">Philippe D***</addname>
				  </name>
				  <date from="1806" to="1865">1806-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DMP_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
          <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L'HOMME QUI BAT SA FEMME</title>
						<author>JULIEN DE MALLIAN ET PHILIPPE DUMANOIR</author>
					</titleStmt>
					<publicationStmt>
						<publisher>INTERNET ARCHIVE</publisher>
						<idno type="URL">https ://archive.org/details/lhommequibatsafe00malluoft</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>University of Toronto Libraries</repository>
								<idno type="URL">https ://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991105986034006196</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">8 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DMP12" modus="cp" lm_max="10">
	<head type="tune">AIR de Turenne.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="10"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="1.2">n</w>'<w n="1.3">s</w>'<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t</w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.7">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="1.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w> <w n="1.9">gr<seg phoneme="i" type="vs" value="1" rule="466" place="9">i</seg>m<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
		<l n="2" num="1.2" lm="8"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <hi rend="ital"><w n="2.4">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s</w></hi> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="2.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <hi rend="ital"><w n="2.7" punct="dp:8">s<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="dp">i</seg></w> :</hi></l>
		<l n="3" num="1.3" lm="10"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">t</w>'<w n="3.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4" punct="vg">ai</seg></w>, <w n="3.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="3.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="3.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg></w> <w n="3.7"><seg phoneme="o" type="vs" value="1" rule="317" place="9">au</seg>d<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
		<l n="4" num="1.4" lm="10"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">j</w>'<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg></w> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.5">dr<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>t</w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.7">c<seg phoneme="o" type="vs" value="1" rule="434" place="6">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w> <w n="4.8" punct="pt:10"><seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pt">i</seg></w>.</l>
		<l n="5" num="1.5" lm="8"><w n="5.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">dr<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>t</w> <w n="5.3">m</w>'<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rti<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="5">en</seg>t</w> <w n="5.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="6">en</seg></w> <w n="5.6">p<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>t</w>-<w n="5.7" punct="dp:8"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
		<l n="6" num="1.6" lm="10"><w n="6.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="6.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="vg">ai</seg>s</w>, <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg></w> <w n="6.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">em</seg>ps</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.7">n<seg phoneme="o" type="vs" value="1" rule="437" place="8">o</seg>s</w> <w n="6.8" punct="dp:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="dp">ou</seg>rs</w> :</l>
		<l n="7" num="1.7" lm="10"><w n="7.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="7.2">r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="7.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="7.4">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="351" place="7">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="7.5" punct="ps:10">t<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="ps">ou</seg>rs</w>…</l>
	</lg>
	<lg n="2">
	<head type="speaker">PICHARD.</head>
		<l n="8" num="2.1" lm="8"><w n="8.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="8.2">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="8.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">n</w>'<w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg></w> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="8.7" punct="dp:6">d<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="dp">i</seg>t</w> : <w n="8.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="8.9" punct="pt:8">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
		<l n="9" num="2.2" lm="8"><w n="9.1">J</w>'<w n="9.2">n</w>'<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>s</w> <w n="9.5">d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t</w> <w n="9.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.7">t<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg></w> <w n="9.8">sʼr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s</w> <w n="9.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="9.10" punct="pt:8">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
	</lg>
</div></body></text></TEI>