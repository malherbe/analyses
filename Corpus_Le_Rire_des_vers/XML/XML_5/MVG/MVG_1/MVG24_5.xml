<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TROISIÈME TABLEAU.</head><head type="main_subpart">SCÈNE XVII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG24" modus="sm" lm_max="8">
						<head type="tune">AIR : Je loge au quatrième étage.</head>
						<lg n="1">
							<head type="main">LESTRADE.</head>
							<l n="1" num="1.1" lm="8"><w n="1.1">J</w>'<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="1.3">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg></w> <w n="1.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.6" punct="pt:8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s<seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						</lg>
						<lg n="2">
							<head type="main">DARBEL</head>
							<l n="2" num="2.1" lm="8"><w n="2.1">M<seg phoneme="wa" type="vs" value="1" rule="422" place="1">oi</seg></w> <w n="2.2">j</w>'<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="2">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.4">b<seg phoneme="o" type="vs" value="1" rule="314" place="4">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>p</w> <w n="2.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">em</seg>ps</w>-<w n="2.7" punct="pt:8">l<seg phoneme="a" type="vs" value="1" rule="341" place="8" punct="pt">à</seg></w>.</l>
						</lg>
						<lg n="3">
							<head type="main">LESTRADE</head>
							<l n="3" num="3.1" lm="8"><w n="3.1">C</w>'<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="3.3">qu</w>'<w n="3.4"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="3.5">n<seg phoneme="ɛ" type="vs" value="1" rule="383" place="3">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.6">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="3.8" punct="pt:8">R<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>ss<seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						</lg>
						<lg n="4">
							<head type="main">BONAPARTE.</head>
							<l n="4" num="4.1" lm="8"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w>-<w n="4.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="4.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="4.5" punct="pi:8">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pi">a</seg></w> ?</l>
							<l n="5" num="4.2" lm="8"><w n="5.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg>s</w>, <w n="5.2">s<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>r</w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="5.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">am</seg>p</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6">b<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="6" num="4.3" lm="8"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="6.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="6.3">s<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>r</w> <w n="6.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5" punct="pv:8">s<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>gn<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>l<seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pv">er</seg></w> ;</l>
							<l n="7" num="4.4" lm="8"><w n="7.1">L</w>'<w n="7.2">h<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="63" place="2">e</seg>r</w> <w n="7.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="3">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="7.6">m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>tr<seg phoneme="a" type="vs" value="1" rule="306" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="8" num="4.5" lm="8"><w n="8.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t</w> <w n="8.4">j<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s</w> <w n="8.5" punct="pt:8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pt">er</seg></w>.</l>
						</lg>
					</div></body></text></TEI>