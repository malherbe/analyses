<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER TABLEAU.</head><head type="main_subpart">SCÈNE IV.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG6" modus="sp" lm_max="8">
						<head type="tune">AIR : Merveilleuse dans ses vertus.</head>
						<lg n="1">
							<head type="main">BONAPARTE.</head>
							<l n="1" num="1.1" lm="8"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="1.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>s</w>, <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w>-<w n="1.5" punct="dp:8">m<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="dp">oi</seg></w> :</l>
							<l n="2" num="1.2" lm="8"><w n="2.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="2.2">j<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3" punct="vg:4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg">on</seg>t</w>, <w n="2.4">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.5">nu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg>t</w> <w n="2.6" punct="vg:8">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="3" num="1.3" lm="8"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="3.2">c<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="2">ein</seg>t</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="3.5">b<seg phoneme="o" type="vs" value="1" rule="314" place="5">eau</seg></w> <w n="3.6" punct="vg:8">d<seg phoneme="i" type="vs" value="1" rule="dc-1" place="6">i</seg><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="4" num="1.4" lm="8"><w n="4.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="4.2">j</w>'<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg></w> <w n="4.4">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></w> <w n="4.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.6">j</w>'<w n="4.7"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>s</w> <w n="4.8" punct="pv:8">r<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pv">oi</seg></w> ;</l>
							<l n="5" num="1.5" lm="7"><space unit="char" quantity="2"></space><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="5.2" punct="vg:2">r<seg phoneme="wa" type="vs" value="1" rule="422" place="2" punct="vg">oi</seg></w>, <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="4">en</seg>s</w>-<w n="5.5">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="5">e</seg></w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.7" punct="pi:7">d<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pi">e</seg></w> ?</l>
							<l n="6" num="1.6" lm="7"><space unit="char" quantity="2"></space><w n="6.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="6.2">pl<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s</w> <w n="6.3">h<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>t</w> <w n="6.4">j</w>'<w n="6.5"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s</w> <w n="6.6" punct="vg:7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="7" punct="vg">é</seg></w>,</l>
							<l n="7" num="1.7" lm="7"><space unit="char" quantity="2"></space><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="7.2">c</w>'<w n="7.3"><seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="7.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>d</w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">em</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
							<l n="8" num="1.8" lm="7"><space unit="char" quantity="2"></space><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">cr<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="8.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="8.4" punct="pt:7">v<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="7" punct="pt">é</seg></w>.</l>
							<l n="9" num="1.9" lm="8"><w n="9.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="9.3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>x</w> <w n="9.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="9.5">s</w>'<w n="9.6" punct="pv:8"><seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pv">i</seg>r</w> ;</l>
							<l n="10" num="1.10" lm="8"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="10.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.3">pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="10.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d</w> <w n="10.5" punct="pt:8">h<seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
							<l n="11" num="1.11" lm="8"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="11.3" punct="vg:5">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t</w> <w n="11.5" punct="vg:8">R<seg phoneme="ɔ" type="vs" value="1" rule="440" place="8">o</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="12" num="1.12" lm="8"><w n="12.1">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>xpr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>s</w> <w n="12.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="12.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.5" punct="pt:8">b<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pt">i</seg>r</w>.</l>
							<l n="13" num="1.13" lm="7"><space unit="char" quantity="2"></space><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="13.2"><seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="13.3">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.4">f<seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
							<l n="14" num="1.14" lm="7"><space unit="char" quantity="2"></space><w n="14.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="14.2">s<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="381" place="3">e</seg>il</w> <w n="14.3">br<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t</w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="14.5" punct="pt:7">lu<seg phoneme="i" type="vs" value="1" rule="490" place="7" punct="pt">i</seg></w>.</l>
							<l n="15" num="1.15" lm="7"><space unit="char" quantity="2"></space><w n="15.1">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="15.4" punct="dp:7">r<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="409" place="7">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="dp">e</seg></w> :</l>
							<l n="16" num="1.16" lm="7"><space unit="char" quantity="2"></space><w n="16.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rs</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4">e</seg>l</w> <w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="16.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>r</w> <w n="16.6" punct="pt:7">lu<seg phoneme="i" type="vs" value="1" rule="490" place="7" punct="pt">i</seg></w>.</l>
							<l n="17" num="1.17" lm="8"><w n="17.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="17.3" punct="vg:4">p<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="17.4">j<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>squ</w>'<w n="17.5"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg></w> <w n="17.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="7">ain</seg>t</w> <w n="17.7" punct="vg:8">li<seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="vg">eu</seg></w>,</l>
							<l n="18" num="1.18" lm="8"><w n="18.1">J</w>'<w n="18.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>ds</w> <w n="18.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="5">en</seg>s</w> <w n="18.5"><seg phoneme="y" type="vs" value="1" rule="452" place="6">u</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>n<seg phoneme="i" type="vs" value="1" rule="466" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
							<l n="19" num="1.19" lm="8"><w n="19.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="19.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="19.3">s<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>ls</w> <w n="19.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="19.5">r<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>s</w> <w n="19.6" punct="vg:8">l<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<seg phoneme="i" type="vs" value="1" rule="466" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
							<l n="20" num="1.20" lm="8"><w n="20.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="20.2">s<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="20.3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>x</w> <w n="20.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="20.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="20.6">v<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>x</w> <w n="20.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="20.8" punct="pt:8">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pt">eu</seg></w>.</l>
							<l n="21" num="1.21" lm="7"><space unit="char" quantity="2"></space><w n="21.1">D<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="21.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="21.3">c<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418" place="5">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="21.5" punct="pv:7">pr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></w> ;</l>
							<l n="22" num="1.22" lm="7"><space unit="char" quantity="2"></space><w n="22.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="22.2">br<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="22.3">s<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>r</w> <w n="22.4">l</w>'<w n="22.5" punct="pt:7"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="345" place="7" punct="pt">e</seg>l</w>.</l>
							<l n="23" num="1.23" lm="7"><space unit="char" quantity="2"></space><w n="23.1">J</w>'<w n="23.2"><seg phoneme="i" type="vs" value="1" rule="496" place="1">y</seg></w> <w n="23.3" punct="pv:2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="pv">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="23.6">s<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>r</w> <w n="23.7">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="23.8" punct="dp:7">t<seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="dp">e</seg></w> :</l>
							<l n="24" num="1.24" lm="7"><space unit="char" quantity="2"></space><w n="24.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">su<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>s</w> <w n="24.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="24.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="24.6" punct="pt:7">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="345" place="7" punct="pt">e</seg>l</w>.</l>
							<l n="25" num="1.25" lm="8"><w n="25.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="25.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4">en</seg>t</w> <w n="25.4">f<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>s</w> <w n="25.5">fr<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="25.6">l</w>'<w n="25.7" punct="pv:8"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pv">ai</seg>r</w> ;</l>
							<l n="26" num="1.26" lm="8"><w n="26.1">D</w>'<w n="26.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">en</seg></w> <w n="26.3">h<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="26.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="26.5">h<seg phoneme="i" type="vs" value="1" rule="492" place="4">y</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="26.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="26.7" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>nt</w>,</l>
							<l n="27" num="1.27" lm="8"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="27.2">d</w>'<w n="27.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="27.4">b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="27.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="27.6">v<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>x</w> <w n="27.7">l<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>r</w> <w n="27.8" punct="dp:8">r<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg>nt</w> :</l>
							<l n="28" num="1.28" lm="8"><w n="28.1" punct="vg:2">P<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="28.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="28.3">lu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="28.4">t<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg></w> <w n="28.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>s</w> <w n="28.6" punct="pt:8">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-8" place="8" punct="pt">e</seg>r</w>.</l>
							<l n="29" num="1.29" lm="7"><space unit="char" quantity="2"></space><w n="29.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="29.2">r<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="29.3" punct="vg:4">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4" punct="vg">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>, <w n="29.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="29.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="29.6" punct="vg:7">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
							<l n="30" num="1.30" lm="7"><space unit="char" quantity="2"></space><w n="30.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="30.2">c<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="30.3" punct="pt:7">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">om</seg>ph<seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="pt">a</seg>l</w>.</l>
							<l n="31" num="1.31" lm="7"><space unit="char" quantity="2"></space><w n="31.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="31.2">c<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>p</w> <w n="31.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="31.4">f<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="31.5">l</w>'<w n="31.6" punct="pv:7"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="409" place="7">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></w> ;</l>
							<l n="32" num="1.32" lm="7"><space unit="char" quantity="2"></space><w n="32.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="32.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="4">en</seg>s</w> <w n="32.3" punct="pt:7">c<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>p<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="ps pt">a</seg>l</w>….</l>
						</lg>
						<div type="section" n="1">
							<head type="main">bis. {</head>
							<lg n="1">
								<l n="33" num="1.1" lm="8"><w n="33.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="33.2" punct="vg:3">r<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381" place="3" punct="vg">e</seg>il</w>, <w n="33.3" punct="pe:5">h<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="pe">a</seg>s</w> ! <w n="33.4">f<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>t</w> <w n="33.5">tr<seg phoneme="o" type="vs" value="1" rule="432" place="7">o</seg>p</w> <w n="33.6" punct="pt:8">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="pt">om</seg>pt</w>.</l>
								<l n="34" num="1.2" lm="8"><w n="34.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="34.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="34.3" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="pe">i</seg>s</w> ! <w n="34.4">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg></w> <w n="34.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="34.6" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rd<seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
								<l n="35" num="1.3" lm="8"><w n="35.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="35.2">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="35.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>r</w> <w n="35.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="35.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="35.6">c<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
								<l n="36" num="1.4" lm="8"><w n="36.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="1">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>r</w> <w n="36.2">l</w>'<w n="36.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>pr<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="4">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="36.4">s<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>r</w> <w n="36.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="36.6" punct="pt:8">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="pt">on</seg>t</w>.</l>
							</lg>
							<lg n="2">
								<l n="37" num="2.1" lm="8"><subst reason="analysis" hand="LG" type="repetition"><del></del><add rend="hidden"><w n="37.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="37.2" punct="vg:3">r<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381" place="3" punct="vg">e</seg>il</w>, <w n="37.3" punct="pe:5">h<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="pe">a</seg>s</w> ! <w n="37.4">f<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>t</w> <w n="37.5">tr<seg phoneme="o" type="vs" value="1" rule="432" place="7">o</seg>p</w> <w n="37.6" punct="pt:8">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="pt">om</seg>pt</w>.</add></subst></l>
								<l n="38" num="2.2" lm="8"><subst reason="analysis" hand="LG" type="repetition"><del></del><add rend="hidden"><w n="38.1"><seg phoneme="o" type="vs" value="1" rule="414" place="1">Ô</seg></w> <w n="38.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="38.3" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="pe">i</seg>s</w> ! <w n="38.4">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg></w> <w n="38.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="38.6" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rd<seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</add></subst></l>
								<l n="39" num="2.3" lm="8"><subst reason="analysis" hand="LG" type="repetition"><del></del><add rend="hidden"><w n="39.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="39.2">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="39.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>r</w> <w n="39.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="39.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="39.6">c<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></add></subst></l>
								<l n="40" num="2.4" lm="8"><subst reason="analysis" hand="LG" type="repetition"><del></del><add rend="hidden"><w n="40.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="1">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>r</w> <w n="40.2">l</w>'<w n="40.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>pr<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="4">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="40.4">s<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>r</w> <w n="40.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="40.6" punct="pt:8">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="pt">on</seg>t</w>.</add></subst></l>
							</lg>
						</div>
					</div></body></text></TEI>