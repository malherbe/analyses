<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER TABLEAU.</head><head type="main_subpart">SCÈNE V.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG9" modus="cp" lm_max="10">
						<head type="tune">AIR : du Piége.</head>
							<lg n="1">
								<head type="main">BONAPARTE.</head>
								<l n="1" num="1.1" lm="8"><space unit="char" quantity="4"></space><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="1.2">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>t</w> <w n="1.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="1.4">n</w>'<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="1.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.7">l</w>'<w n="1.8" punct="vg:8">h<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>r</w>,</l>
								<l n="2" num="1.2" lm="8"><space unit="char" quantity="4"></space><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="2.2">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rs<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="2.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="2.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>g</w> <w n="2.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>r</w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="2.7" punct="vg:8">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
								<l n="3" num="1.3" lm="10"><w n="3.1">F<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>t</w> <w n="3.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">in</seg>s<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>lt<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r</w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="3.5">j<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="3.6" punct="dp:10">s<seg phoneme="ɛ" type="vs" value="1" rule="383" place="9">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="dp">eu</seg>r</w> :</l>
								<l n="4" num="1.4" lm="8"><space unit="char" quantity="4"></space><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="4.3">d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>sp<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>t</w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="4.5" punct="pv:8">d<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
								<l n="5" num="1.5" lm="10"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="5.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3" punct="vg:4">b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>t</w>, <w n="5.4">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s</w> <w n="5.5" punct="vg:7">j<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="7" punct="vg">ez</seg></w>, <w n="5.6" punct="vg:10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="383" place="9">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="vg">eu</seg>r</w>,</l>
								<l n="6" num="1.6" lm="10"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>l</w> <w n="6.3">c<seg phoneme="o" type="vs" value="1" rule="414" place="3">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></w> <w n="6.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5">tr<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t</w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w> <w n="6.7" punct="dp:10">b<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="351" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp">e</seg></w> :</l>
								<l n="7" num="1.7" lm="8"><space unit="char" quantity="4"></space><w n="7.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">l</w>'<w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="7.4">s<seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="7.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="7.6" punct="vg:8">h<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>r</w>,</l>
								<l n="8" num="1.8" lm="8"><space unit="char" quantity="4"></space><w n="8.1">L</w>'<w n="8.2"><seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="8.4">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg></w> <w n="8.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="8.6" punct="pt:8">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="351" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
							</lg>
							<lg n="2">
								<l n="9" num="2.1" lm="10"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="9.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>t</w> <w n="9.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5">l</w>'<w n="9.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="9.7">d<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>t</w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>cc<seg phoneme="y" type="vs" value="1" rule="449" place="9">u</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="10">er</seg></w></l>
								<l n="10" num="2.2" lm="10"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="10.2">s</w>'<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="10.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="10.5">l<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>s</w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg></w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w> <w n="10.8" punct="pv:10">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9">en</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="310" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
								<l n="11" num="2.3" lm="10"><w n="11.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="11.2">s<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="11.3">f<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="11.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="11.5">d<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="346" place="8">ez</seg></w> <w n="11.6"><seg phoneme="y" type="vs" value="1" rule="449" place="9">u</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="10">er</seg></w></l>
								<l n="12" num="2.4" lm="8"><space unit="char" quantity="4"></space><w n="12.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="12.2">dr<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>t</w> <w n="12.3">s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="12.4">b<seg phoneme="o" type="vs" value="1" rule="314" place="4">eau</seg></w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="12.7" punct="pt:8">cl<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
								<l n="13" num="2.5" lm="10"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4" punct="vg">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="13.4"><seg phoneme="o" type="vs" value="1" rule="414" place="5">ô</seg></w> <w n="13.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="13.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.7" punct="pe:10">pr<seg phoneme="o" type="vs" value="1" rule="443" place="8">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9">e</seg>ct<seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="pe">eu</seg>r</w> !</l>
								<l n="14" num="2.6" lm="10"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>s</w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="14.4"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rd<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w> <w n="14.5">qu</w>'<w n="14.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg></w> <w n="14.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="14.8">l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
								<l n="15" num="2.7" lm="8"><space unit="char" quantity="4"></space><w n="15.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="15.2">d<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="15.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="15.4">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s</w> <w n="15.5">d</w>'<w n="15.6" punct="dp:8">h<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="dp">eu</seg>r</w> :</l>
								<l n="16" num="2.8" lm="10"><w n="16.1">C</w>'<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.4">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="16.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="16.6">t<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="16.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="16.8" punct="pt:10">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9">o</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="351" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt">e</seg></w>.</l>
							</lg>
						</div></body></text></TEI>