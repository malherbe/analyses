<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES DEMOISELLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="BRA" sort="1">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="2">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LH">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML, application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>54 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BEC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
					<biblFull>
						<titleStmt>
						<title type="main">Les demoiselles</title>
					<author>BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=KF7AyuNs2gAC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque municipale de Lyon</repository>
								<idno type="URL">https://numelyo.bm-lyon.fr/f_view/BML:BML_00GOO0100137001100337604#</idno>
							</monogr>
						</biblStruct>                 
			</sourceDesc>
			</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : U+02BC MODIFIER LETTER APOSTROPHE) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">DEUXIEME ACTE</head><head type="main_subpart">SCÈNE XIV.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BEC2" modus="cp" lm_max="9">
					<head type="main">VAUDEVILLE</head>
					<head type="tune">Air : Vaudeville du Diner de garçons.</head>
					<lg n="1">
						<head type="main">MADAME BICHONNET.</head>
						<l n="1" num="1.1" lm="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="1.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>l</w> <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="1.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>xp<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>s<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w></l>
						<l n="2" num="1.2" lm="8"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="2.2">l<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg></w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.4">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="8"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="3.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>vi<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="3.3" punct="vg:4">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4" punct="vg">e</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="3.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6">j<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg></w>-<w n="3.7" punct="vg:8">l<seg phoneme="a" type="vs" value="1" rule="341" place="8" punct="vg">à</seg></w>,</l>
						<l n="4" num="1.4" lm="8"><w n="4.1">V<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="4.4">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="4.5" punct="pt:8">f<seg phoneme="a" type="vs" value="1" rule="192" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="5" num="1.5" lm="8"><w n="5.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="5.2">n</w>'<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="5.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="5.6" punct="vg:8">C<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="vg">on</seg></w>,</l>
						<l n="6" num="1.6" lm="8"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">em</seg>ps</w> <w n="6.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="6.5">r<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>gn<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="6.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="6.7" punct="pv:8"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
						<l n="7" num="1.7" lm="8"><w n="7.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w>-<w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="7.5" punct="vg:8">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="vg">on</seg></w>,</l>
						<l n="8" num="1.8" lm="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="8.2">n</w>'<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="8.4" punct="vg:4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4" punct="vg">u</seg>s</w>, <w n="8.5">vi<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>s</w></l>
						<l n="9" num="1.9" lm="8"><w n="9.1">V<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>lt<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="9.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>s</w> <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="9.4" punct="pt:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<head type="main">COQUENARD.</head>
						<l n="10" num="2.1" lm="8"><w n="10.1">J</w>'<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="1">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.3">mi<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="10.5">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8">in</seg></w></l>
						<l n="11" num="2.2" lm="8"><w n="11.1">Q</w>'<w n="11.2"><seg phoneme="y" type="vs" value="1" rule="452" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg></w> <w n="11.5">g<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="5">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>l</w> <w n="11.6" punct="pv:8">s<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="12" num="2.3" lm="8"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w>' <w n="12.2">ch<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg></w>' <w n="12.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.4">m<seg phoneme="ɛ" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="12.5">pl<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>s</w> <w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="12.7">tr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg></w></l>
						<l n="13" num="2.4" lm="8"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">pl<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>s</w> <w n="13.4">b<seg phoneme="o" type="vs" value="1" rule="314" place="4">eau</seg></w> <w n="13.5">pi<seg phoneme="e" type="vs" value="1" rule="240" place="5">e</seg>d</w> <w n="13.6">qu</w>'<w n="13.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="13.8" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="14" num="2.5" lm="8"><w n="14.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w>' <w n="14.2">b<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="381" place="3">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="384" place="5">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6" punct="vg">e</seg>st</w>, <w n="14.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.6" punct="vg:8">cr<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="vg">oi</seg></w>,</l>
						<l n="15" num="2.6" lm="8"><w n="15.1">Pr<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>f<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>x</w> <w n="15.3">t<seg phoneme="a" type="vs" value="1" rule="306" place="5">a</seg>illʼs</w> <w n="15.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="15.5">pl<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>s</w> <w n="15.6" punct="pt:8">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
						<l n="16" num="2.7" lm="8"><w n="16.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w>' <w n="16.2">p<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">in</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="16.3">v<seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>t</w> <w n="16.4">d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>x</w> <w n="16.5">f<seg phoneme="ɛ" type="vs" value="1" rule="365" place="6">e</seg>mmʼs</w> <w n="16.6">p<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r</w> <w n="16.7" punct="vg:8">m<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="vg">oi</seg></w>,</l>
						<l n="17" num="2.8" lm="8"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="17.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r</w> <w n="17.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w>' <w n="17.4">d<seg phoneme="a" type="vs" value="1" rule="144" place="4">a</seg>m</w>' <w n="17.5" punct="vg:6">j<seg phoneme="a" type="vs" value="1" rule="309" place="5">ea</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="17.6">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="17.7" punct="vg:8">f<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="vg">oi</seg></w>,</l>
						<l n="18" num="2.9" lm="9"><w n="18.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">d<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>nn</w>'<w n="18.3">r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="18.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="18.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="18.6" punct="pt:9">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<head type="main">COLIN.</head>
						<l n="19" num="3.1" lm="8"><w n="19.1">H<seg phoneme="i" type="vs" value="1" rule="d-1" place="1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="63" place="2">e</seg>r</w> <w n="19.2"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg></w> <w n="19.3" punct="vg:4">s<seg phoneme="wa" type="vs" value="1" rule="419" place="4" punct="vg">oi</seg>r</w>, <w n="19.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="19.5">m</w>'<w n="19.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg>t</w>,</l>
						<l n="20" num="3.2" lm="8"><w n="20.1">Qu</w>'<w n="20.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="20.3">br<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>d</w> <w n="20.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</w> <w n="20.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.6">b<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>s</w> <w n="20.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="20.8" punct="pt:8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="21" num="3.3" lm="8">« <w n="21.1">C</w>'<w n="21.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">en</seg></w> <w n="21.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="21.4" punct="ps:4"><seg phoneme="y" type="vs" value="1" rule="452" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="ps">e</seg></w>… <w n="21.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="21.6">su<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>s</w>-<w n="21.7">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="7">e</seg></w> <w n="21.8" punct="pt:8">d<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pt">i</seg>t</w>.</l>
						<l n="22" num="3.4" lm="8">« <w n="22.1">C<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="22.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="3">en</seg></w> <w n="22.3">v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="22.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="22.6" punct="pt:8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>. »</l>
						<l n="23" num="3.5" lm="8"><w n="23.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">m</w>'<w n="23.3"><seg phoneme="i" type="vs" value="1" rule="496" place="2">y</seg></w> <w n="23.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6">e</seg>c</w> <w n="23.6" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pv">eu</seg>r</w> ;</l>
						<l n="24" num="3.6" lm="8"><w n="24.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="24.4">d<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>pl<seg phoneme="wa" type="vs" value="1" rule="439" place="5">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="24.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="24.6" punct="pv:8">z<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="25" num="3.7" lm="8"><w n="25.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1" punct="vg">ai</seg>s</w>, <w n="25.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="25.3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="3" punct="ps">oi</seg>s</w>-<w n="25.4" punct="ps:3">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w>… <w n="25.5"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg></w> <w n="25.6">li<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg></w> <w n="25.7">d</w>'<w n="25.8"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="25.9">v<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>l<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>r</w></l>
						<l n="26" num="3.8" lm="8"><w n="26.1">C</w>'<w n="26.2"><seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="26.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="26.4">v<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">in</seg></w> <w n="26.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <hi rend="ital"><w n="26.6" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>r</w></hi>,</l>
						<l n="27" num="3.9" lm="8"><w n="27.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="27.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="27.3"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="5">e</seg>c</w> <w n="27.4">s<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <hi rend="ital"><w n="27.5" punct="pt:8">d</w>'<w n="27.6">m<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w></hi>.</l>
					</lg>
					<lg n="4">
						<head type="main">SIMONNE, au public.</head>
						<l n="28" num="4.1" lm="8"><w n="28.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="28.2">ptʼ<seg phoneme="i" type="vs" value="1" rule="496" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="28.3">l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="409" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="28.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="28.5">s<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r</w></l>
						<l n="29" num="4.2" lm="8"><w n="29.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="29.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="29.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ri<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="29.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="29.5" punct="pv:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="30" num="4.3" lm="8"><w n="30.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="30.2">d<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="30.3">s<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="6">en</seg>t</w> <w n="30.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="30.5" punct="vg:8">v<seg phoneme="wa" type="vs" value="1" rule="419" place="8" punct="vg">oi</seg>r</w>,</l>
						<l n="31" num="4.4" lm="8"><w n="31.1">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="31.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>c</w> <w n="31.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="31.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="31.5" punct="pt:8">v<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="32" num="4.5" lm="8"><w n="32.1">D<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="32.2"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="344" place="4">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>r</w> <w n="32.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="32.4">c<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>x</w></l>
						<l n="33" num="4.6" lm="8"><w n="33.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="33.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">in</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s</w> <w n="33.3" punct="vg:8">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="34" num="4.7" lm="8"><w n="34.1" punct="vg:2">M<seg phoneme="e" type="vs" value="1" rule="352" place="1">e</seg>ssi<seg phoneme="ø" type="vs" value="1" rule="396" place="2" punct="vg">eu</seg>rs</w>, <w n="34.2">s<seg phoneme="wa" type="vs" value="1" rule="439" place="3">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="34.3">g<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="34.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r</w> <w n="34.5" punct="pv:8">n<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pv">ou</seg>s</w> ;</l>
						<l n="35" num="4.8" lm="8"><w n="35.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="35.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>qu<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="35.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="35.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg></w> <w n="35.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w>-<w n="35.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>s</w></l>
						<l n="36" num="4.9" lm="8"><w n="36.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="36.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="36.3">d<seg phoneme="ɔ" type="vs" value="1" rule="418" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="36.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="36.5" punct="pt:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>