<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE3" modus="sm" lm_max="8">
	
			<head type="main">MICHEL.</head>

			<p>En v’là une dure ! ... il demande pardon à Dieu d’avoir été libéral ! ... C’est juste, puisque notre curé nous a dit encore dernièrement que tous ceux qu’étaient pas pour le ministère, frrr... dans la grande poële ... en friture pour l’éternité.</p>

		<head type="tune">Air du Vaudeville de l’Apothicaire.</head>

				<lg n="1"><l n="1" num="1.1" lm="8"><w n="1.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="1.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>l</w> <w n="1.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="1.4">v<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="1.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>c</w> <w n="1.6" punct="pe:8">pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>scr<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pe">i</seg>ts</w> !</l>
					<l n="2" num="1.2" lm="8"><w n="2.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="2.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="2.3">s<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>l<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>t</w> <w n="2.4">ch<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="2.5">s</w>’<w n="2.6" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
					<l n="3" num="1.3" lm="8"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="3.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="3.3">f<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>s</w></l>
					<l n="4" num="1.4" lm="8"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>ls</w> <w n="4.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="4">ain</seg>t</w> <w n="4.5">Pi<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="4.7" punct="pt:8">g<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>d<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="5" num="1.5" lm="8"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="5.2" punct="vg:4">l<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>b<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>r<seg phoneme="o" type="vs" value="1" rule="317" place="4" punct="vg">au</seg>x</w>, <w n="5.3">qu<seg phoneme="wa" type="vs" value="1" rule="280" place="5">oi</seg>qu</w>’ <w n="5.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s</w> <w n="5.5" punct="vg:8">cr<seg phoneme="wa" type="vs" value="1" rule="439" place="7">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="vg">an</seg>s</w>,</l>
					<l n="6" num="1.6" lm="8"><w n="6.1" punct="vg:1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="vg">on</seg>t</w>, <w n="6.2">d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t</w>-<w n="6.3" punct="vg:3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="vg">on</seg></w>, <w n="6.4">r<seg phoneme="ɛ" type="vs" value="1" rule="338" place="4">a</seg>y<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>s</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="6.7" punct="pv:8">l<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
					<l n="7" num="1.7" lm="8"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">aî</seg>t</w> <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>s</w> <w n="7.5">qu<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8">an</seg>s</w></l>
					<l n="8" num="1.8" lm="8"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="8.3">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg></w> <w n="8.4">s</w>’<w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="8.6">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="8.7" punct="pt:8">r<seg phoneme="wa" type="vs" value="1" rule="439" place="6">o</seg>y<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l></lg>
			</div></body></text></TEI>