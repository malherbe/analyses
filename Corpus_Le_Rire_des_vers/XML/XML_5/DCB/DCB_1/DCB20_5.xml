<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB20" modus="cp" lm_max="12">
	<head type="tune">AIR de la fête de la madone (de Panseron).</head>
		<div type="section" n="1">
		<lg n="1">
			<l n="1" num="1.1" lm="8"><w n="1.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="1.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">om</seg>bl<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="1.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="1.4" punct="pv:8"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
			<l n="2" num="1.2" lm="8"><w n="2.1">V<seg phoneme="wa" type="vs" value="1" rule="439" place="1">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rt</w> <w n="2.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="2.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="2.6" punct="vg:8">s<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg>t</w>,</l>
			<l n="3" num="1.3" lm="8"><w n="3.1">V<seg phoneme="wa" type="vs" value="1" rule="439" place="1">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="3.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="3.3">c<seg phoneme="œ" type="vs" value="1" rule="248" place="4">œu</seg>r</w> <w n="3.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="3.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="3.6" punct="ps:8">ch<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="ps">i</seg>t</w>…</l>
			<l n="4" num="1.4" lm="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="4.3">t<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>lb<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>r<seg phoneme="i" type="vs" value="1" rule="492" place="5">y</seg></w> <w n="4.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="4.5">s</w>'<w n="4.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
		</lg>
		<lg n="2">
		<head type="speaker">CÉLESTINE, avec transport à part…</head>
			<l n="5" num="2.1" lm="8"><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w>-<w n="5.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="5.4">t<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>bl<seg phoneme="o" type="vs" value="1" rule="314" place="5">eau</seg></w> <w n="5.5">pl<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>s</w> <w n="5.6" punct="pi:8">j<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pi">i</seg></w> ?</l>
			<l n="6" num="2.2" lm="8"><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="2">en</seg>t</w> <w n="6.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>p<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="6.3">s<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="6.4" punct="pi:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="351" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
			<l n="7" num="2.3" lm="8"><w n="7.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="7.2">qu</w>'<w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="7.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="7.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">in</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="351" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
			<l n="8" num="2.4" lm="12"><w n="8.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="8.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="8.3"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="8.5">c<seg phoneme="œ" type="vs" value="1" rule="248" place="6">œu</seg>r</w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="8">e</seg>c</w> <w n="8.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg></w> <w n="8.8" punct="pe:12">t<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>lb<seg phoneme="y" type="vs" value="1" rule="449" place="11">u</seg>r<seg phoneme="i" type="vs" value="1" rule="492" place="12" punct="pe">y</seg></w> !</l>
		</lg>
	</div>
	<div type="section" n="2">
		<head type="main">ENSEMBLE.</head>
		<lg n="1">
		<p>
			CÉLESTINE.<lb></lb>
			Est-il un tableau plus joli ? etc,<lb></lb>
		</p>
		<head type="speaker">CHARLES,</head>
			<l n="9" num="1.1" lm="8"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w>-<w n="9.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="9.4">t<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>bl<seg phoneme="o" type="vs" value="1" rule="314" place="5">eau</seg></w> <w n="9.5">pl<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>s</w> <w n="9.6" punct="pi:8">j<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pi">i</seg></w> ?</l>
			<l n="10" num="1.2" lm="8"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="10.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="10.3">c<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="10.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="10.6" punct="pv:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="351" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
			<l n="11" num="1.3" lm="8"><w n="11.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rs</w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t</w> <w n="11.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">in</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="351" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
			<l n="12" num="1.4" lm="12"><w n="12.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="12.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="12.3"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="12.5">c<seg phoneme="œ" type="vs" value="1" rule="248" place="6">œu</seg>r</w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="8">e</seg>c</w> <w n="12.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg></w> <w n="12.8" punct="pt:12">t<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>lb<seg phoneme="y" type="vs" value="1" rule="449" place="11">u</seg>r<seg phoneme="i" type="vs" value="1" rule="492" place="12" punct="pt">y</seg></w>.</l>
		</lg>
	</div>
</div></body></text></TEI>