<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB17" modus="cp" lm_max="10">
	<head type="tune">AIR d'une mazureck polonaise, arrangée par M. Ch. Tolbecque.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="5"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="1.3">sʼr<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="1.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="1.5" punct="vg:5">f<seg phoneme="a" type="vs" value="1" rule="192" place="5">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
		<l n="2" num="1.2" lm="5"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="2.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="2.4" punct="pe:5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="5" punct="pe">en</seg></w> !</l>
		<l n="3" num="1.3" lm="5"><w n="3.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">s<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>r</w> <w n="3.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="3.4" punct="vg:5">fl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
		<l n="4" num="1.4" lm="6"><w n="4.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="4.2">n</w>'<w n="4.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>q<seg phoneme="y" type="vs" value="1" rule="462" place="3">u</seg>ʼr<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5" punct="pt:6">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="6" punct="pt">en</seg></w>.</l>
		<l n="5" num="1.5" lm="5"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="5.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">d<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>x</w> <w n="5.4">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="5">en</seg></w></l>
		<l n="6" num="1.6" lm="10"><w n="6.1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="1">oin</seg>t</w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">s<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>s</w> <w n="6.4">n<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg></w> <w n="6.5">d</w>'<w n="6.6"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>p<seg phoneme="i" type="vs" value="1" rule="466" place="7">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="6.7" punct="vg:10">m<seg phoneme="o" type="vs" value="1" rule="317" place="9">au</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
		<l n="7" num="1.7" lm="7"><w n="7.1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="1">oin</seg>t</w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">fl<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r</w> <w n="7.4">h<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>cl<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
		<l n="8" num="1.8" lm="5"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="8.2" punct="pt:5">M<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rgu<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
	</lg>
	<p>
		(Ils dansent tous deux sur la ritournelle un pas comique, 
		avec gestes analogues, en s'envoyant des baisers.)
	</p>
	<lg n="2">
	<head type="speaker">TÉLÉMAQUE.</head>
	<head type="main">DEUXIÈME COUPLET.</head>
		<l n="9" num="2.1" lm="5"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="9.2">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">m<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
		<l n="10" num="2.2" lm="5"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w>'<w n="10.2">f<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="10.3" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="5" punct="vg">i</seg>s</w>,</l>
		<l n="11" num="2.3" lm="5"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="11.3">d</w>'<w n="11.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="11.5"><seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>vr<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
		<l n="12" num="2.4" lm="5"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">l</w>'<w n="12.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="12.4">s<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>t</w> <w n="12.5" punct="pt:5">s<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="467" place="5" punct="pt">i</seg>s</w>.</l>
		<l n="13" num="2.5" lm="5"><w n="13.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2">an</seg>s</w> <w n="13.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="3">en</seg></w> <w n="13.3" punct="vg:5">g<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="5" punct="vg">i</seg>ls</w>,</l>
		<l n="14" num="2.6" lm="4"><w n="14.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>li<seg phoneme="e" type="vs" value="1" rule="346" place="2">er</seg>s</w> <w n="14.2" punct="vg:4">j<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>s</w>,</l>
		<l n="15" num="2.7" lm="6"><w n="15.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="15.2">ç<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="15.3">s</w>'<w n="15.4">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="15.5">v<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
		<l n="16" num="2.8" lm="7"><w n="16.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="16.2" punct="vg:4">tr<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306" place="3">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="vg">an</seg>t</w>, <w n="16.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="16.4" punct="vg:7">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
		<l n="17" num="2.9" lm="5"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="17.2" punct="pt:5">M<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rgu<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
	</lg> 
	<p>
		ENSEMBLE.<lb></lb>
		Enfans bien gentils, etc.<lb></lb> 
	</p>
	<lg n="3">
	<head type="speaker">MARGUERITE</head>
		<l n="18" num="3.1" lm="5"><w n="18.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2">an</seg>s</w> <w n="18.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="3">en</seg></w> <w n="18.3" punct="vg:5">g<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="5" punct="vg">i</seg>ls</w>,</l>
		<l n="19" num="3.2" lm="4"><w n="19.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>li<seg phoneme="e" type="vs" value="1" rule="346" place="2">er</seg>s</w> <w n="19.2" punct="vg:4">j<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>s</w>,</l>
		<l n="20" num="3.3" lm="6"><w n="20.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2">ç<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="20.3">s</w>'<w n="20.4">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="20.5" punct="pe:6">v<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></w> !</l>
		<l n="21" num="3.4" lm="7"><w n="21.1">F<seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="21.2">qu</w>'<w n="21.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="21.4">f<seg phoneme="y" type="vs" value="1" rule="444" place="4">û</seg>t</w> <w n="21.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>t</w> <w n="21.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="21.7">su<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
		<l n="22" num="3.5" lm="5"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="22.2" punct="pt:5">M<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rgu<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
	</lg>
</div></body></text></TEI>