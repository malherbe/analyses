<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB2" modus="cp" lm_max="9">
	<head type="tune">AIR : Vous n'connaissez pas les belles (de M. H. Monpeou.)</head>
	<lg n="1">
		<l n="1" num="1.1" lm="7"><w n="1.1">C</w>'<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="1.3">qu</w>'<w n="1.4">M<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rgu<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="1.6" punct="pv:7">g<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="6">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></w> ;</l>
		<l n="2" num="1.2" lm="9"><w n="2.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2" punct="pe:4">t<seg phoneme="a" type="vs" value="1" rule="306" place="3">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pe">e</seg></w> ! <w n="2.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="5">e</seg>l</w> <w n="2.4">j<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="2.5" punct="pe:9">m<seg phoneme="i" type="vs" value="1" rule="466" place="8">i</seg>n<seg phoneme="wa" type="vs" value="1" rule="419" place="9" punct="pe">oi</seg>s</w> !</l>
		<l n="3" num="1.3" lm="7"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="496" place="1">Y</seg></w> <w n="3.2">m</w>'<w n="3.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">em</seg>bl</w>'<w n="3.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.5">d</w>'<w n="3.6"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg></w> <w n="3.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.8">v<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>s</w></l>
		<l n="4" num="1.4" lm="7"><w n="4.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="4.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>d</w> <w n="4.3"><seg phoneme="œ" type="vs" value="1" rule="285" place="3">œ</seg>il</w> <w n="4.4">n<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>r</w> <w n="4.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="4.6" punct="ps:7">p<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="ps">e</seg></w>…</l>
	<p>
		(parlé.) Oh ! quel œil ! comme ça vous regarde ! comme ça vous<lb></lb>
		 brûle ! comme ça vous…<lb></lb>
	</p>
		<l n="5" num="1.5" lm="7"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="5.2">c</w>'<w n="5.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="5.4">v<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>t</w> <w n="5.5">mi<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="5.6">qu</w>'<w n="5.7" punct="vg:5">t<seg phoneme="u" type="vs" value="1" rule="424" place="5" punct="vg">ou</seg>t</w>, <w n="5.8"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.9"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w></l>
		<l n="6" num="1.6" lm="7"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="6.2">pʼt<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t</w> <w n="6.3">pi<seg phoneme="e" type="vs" value="1" rule="240" place="3">e</seg>d</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="6.5">p<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>s</w> <w n="6.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>g</w> <w n="6.7">qu</w>'<w n="6.8" punct="pt:7">ç<seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="pt">a</seg></w>.</l>
	</lg>
	<lg n="2">
	<head type="main">DEUXIÈME COUPLET.</head>
		<l n="7" num="2.1" lm="7"><w n="7.1">J</w>'<w n="7.2">tr<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306" place="2">a</seg>ill</w>'<w n="7.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="7.4">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.6">j</w>'<w n="7.7" punct="dp:7"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="7">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="dp">e</seg></w> :</l>
		<l n="8" num="2.2" lm="7"><w n="8.1">Vʼl<seg phoneme="a" type="vs" value="1" rule="341" place="1">à</seg></w> <w n="8.2">l</w>'<w n="8.3"><seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>vr<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>g</w>' <w n="8.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="5">ô</seg>t</w> <w n="8.5" punct="pv:7">f<seg phoneme="i" type="vs" value="1" rule="466" place="6">i</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="7" punct="pv">i</seg></w> ;</l>
		<l n="9" num="2.3" lm="7"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="9.2">s<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="9.3">j</w>'<w n="9.4" punct="vg:3">p<seg phoneme="ø" type="vs" value="1" rule="397" place="3" punct="vg">eu</seg>x</w>, <w n="9.5">d<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>s</w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>rd</w>'<w n="9.7" punct="vg:7">hu<seg phoneme="i" type="vs" value="1" rule="490" place="7" punct="vg">i</seg></w>,</l>
		<l n="10" num="2.4" lm="7"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="10.4">ch<seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="10.5">m<seg phoneme="wa" type="vs" value="1" rule="422" place="6">oi</seg></w>-<w n="10.6" punct="pt:7">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
	<p>
		 (parlé.) Oui, que je lui dirai en me jetant à ses genoux : ravis<lb></lb>
		sante carottière, délicieuse marchande de consommations végé<lb></lb>
		tales, permets que je t'essaie cette chaussure pour laquelle j'ai<lb></lb>
		prodigué mon fil le plus fin, et les soins les plus délicats.<lb></lb>
		 (imitant Marguerite.) « Non, laissez-moi l'essayer moi-même ;<lb></lb>
		 « je ne veux pas : » et peut-être ben que… ( faisant le geste de<lb></lb>
		donner un soufflet.) V'lan ! ah bah ! ça y est ! tant pis, je l'risque.<lb></lb> 
	</p>
		<l n="11" num="2.5" lm="7"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="11.2">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="11.3">j<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>s</w> <w n="11.4">s<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>li<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg>s</w>-<w n="11.5" punct="vg:7">l<seg phoneme="a" type="vs" value="1" rule="341" place="7" punct="vg">à</seg></w>,</l>
		<l n="12" num="2.6" lm="7"><w n="12.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>ls</w> <w n="12.2">j<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>s</w> <w n="12.3">pʼt<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ts</w> <w n="12.4">pi<seg phoneme="e" type="vs" value="1" rule="240" place="5">e</seg>ds</w> <w n="12.5">ç<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="12.6" punct="pe:7">fʼr<seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="pe">a</seg></w> !</l>
	</lg>
</div></body></text></TEI>