<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB4" modus="cp" lm_max="10">
	<head type="tune">AIR de Céline.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8"><w n="1.1">C<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>tt</w>'<w n="1.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>du<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>t</w>' <w n="1.3">m</w>'<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="1.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="1.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="6">en</seg></w> <w n="1.7">d</w>'<w n="1.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="1.9" punct="vg:8">p<seg phoneme="ɛ" type="vs" value="1" rule="384" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="2" num="1.2" lm="8"><w n="2.1">J</w>'<w n="2.2">cr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="2.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>r</w> <w n="2.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r</w> <w n="2.6" punct="pv:8">t<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pv">oi</seg></w> ;</l>
		<l n="3" num="1.3" lm="8"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">fu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.5">s<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="304" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
		<l n="4" num="1.4" lm="8"><w n="4.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="1">e</seg>l</w> <w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4" punct="pi:5">m<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="5" punct="pi">i</seg>f</w> ? <w n="4.5">d<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>s</w>-<w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="7">e</seg></w>-<w n="4.7" punct="pt:8">m<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pt">oi</seg></w>.</l>
		<l n="5" num="1.5" lm="10"><w n="5.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="5.2">ç<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="5.3">d<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>t</w> <w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="y" type="vs" value="1" rule="452" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="6">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
		<l n="6" num="1.6" lm="10"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="6.2">t</w>'<w n="6.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t</w> <w n="6.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="3">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="6.5">n<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="6.6">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8">e</seg>s</w> <w n="6.7" punct="dp:10">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="10" punct="dp">oi</seg>rs</w> :</l>
		<l n="7" num="1.7" lm="8"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="280" place="2">oi</seg></w> <w n="7.2">fu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>r</w> <w n="7.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="4">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg></w> <w n="7.4">t<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="7.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d</w>'<w n="7.6" punct="pi:8">m<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
	</lg>
	<lg n="2">
	<head type="speaker">TÉLÉMAQUE, sans l'écouter.</head>
		<l n="8" num="2.1" lm="8"><w n="8.1">C</w>'<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="8.3">qu</w>'<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="5">en</seg></w> <w n="8.8">b<seg phoneme="o" type="vs" value="1" rule="314" place="6">eau</seg>x</w> <w n="8.9">y<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>x</w> <w n="8.10" punct="pt:8">n<seg phoneme="wa" type="vs" value="1" rule="419" place="8" punct="pt">oi</seg>rs</w>.</l>
	</lg>
	<lg n="3">
	<head type="speaker">MADAME DURAND.</head>
		<l n="9" num="3.1" lm="8"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="280" place="2">oi</seg></w> <w n="9.2">fu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>r</w> <w n="9.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="4">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg></w> <w n="9.4">t<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="9.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d</w>'<w n="9.6" punct="pi:8">m<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
	</lg>
	<lg n="4">
	<head type="speaker">TÉLÉMAQUE.</head>
		<l n="10" num="4.1" lm="8"><w n="10.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="10.2" punct="pe:2">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="2" punct="pe">eu</seg></w> ! <w n="10.3">qu</w>'<w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="10.6">d</w>'<w n="10.7">j<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>s</w> <w n="10.8">y<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>x</w> <w n="10.9" punct="pe:8">n<seg phoneme="wa" type="vs" value="1" rule="419" place="8" punct="pe">oi</seg>rs</w> !</l>
	</lg>
</div></body></text></TEI>