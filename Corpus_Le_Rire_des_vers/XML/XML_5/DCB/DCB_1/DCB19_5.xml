<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB19" modus="sm" lm_max="8">
	<head type="tune">AIR : Les pêcheurs de toutes nos rades.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8"><w n="1.1">Qu</w>'<w n="1.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="1">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rtʼnt</w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="1.4" punct="vg:5">r<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5" punct="in vg">e</seg>ss</w>', <w n="1.5">l</w>'<w n="1.6"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
		<l n="2" num="1.2" lm="8"><w n="2.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">l</w>'<w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="4">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="6">en</seg></w> <w n="2.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="2.7" punct="pi:8">g<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="8" punct="pi">en</seg>s</w> ?</l>
	</lg>
	<lg n="2">
	<head type="speaker">MADAME DURAND, d part.</head>
		<l n="3" num="2.1" lm="8"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="3.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4" punct="pe:8">d<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
	</lg>
	<lg n="3">
	<head type="speaker">CÉLESTINE.</head>
		<l n="4" num="3.1" lm="8"><w n="4.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="4.2">n</w>'<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>s</w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="4.5">l</w>'<w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t</w> <w n="4.7">d</w>'<w n="4.8">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="4.9" punct="pv:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8" punct="pv">em</seg>ps</w> ;</l>
		<l n="5" num="3.2" lm="8"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="5.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="5.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>c</w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="5.5" punct="vg:6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>d<seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="vg">eu</seg>r</w>, <w n="5.6">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="5.7" punct="pt:8">ch<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
	</lg>
	<lg n="4">
	<head type="speaker">MARGUERITE.</head>
		<l n="6" num="4.1" lm="8"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">b<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>r</w> <w n="6.5" punct="pt:8">s<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="8" punct="pt">en</seg>t</w>.</l>
	</lg>
	<lg n="5">
	<head type="speaker">CÉLESTINE.</head>
		<l n="7" num="5.1" lm="8"><w n="7.1" punct="vg:1">V<seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="vg">a</seg></w>, <w n="7.2">t<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="7.3">n</w>'<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="49" place="3">e</seg>s</w> <w n="7.5">qu</w>'<w n="7.6"><seg phoneme="y" type="vs" value="1" rule="452" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.7" punct="pv:8">st<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>ti<seg phoneme="o" type="vs" value="1" rule="434" place="7">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
		<l n="8" num="5.2" lm="8"><w n="8.1">M<seg phoneme="wa" type="vs" value="1" rule="422" place="1">oi</seg></w> <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">su<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>s</w> <w n="8.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r</w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.6" punct="pt:8">m<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="8" punct="pt">en</seg>t</w>.</l>
	</lg> 
</div></body></text></TEI>