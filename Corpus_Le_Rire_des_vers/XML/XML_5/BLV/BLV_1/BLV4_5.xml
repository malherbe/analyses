<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BANDEAU</title>
				<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="BLL" sort="1">
					<name>
						<forename>Jean-Nicolas</forename>
						<surname>BOUILLY</surname>
					</name>
					<date from="1763" to="1842">1763-1842</date>
				</author>
				<author key="VAN" sort="2">
					<name>
						<forename>Émile</forename>
						<surname>VANDERBURCH</surname>
						<addname type="other">Émile VANDERBURCH</addname>
						<addname type="other">Émile VANDER-BURCH</addname>
						<addname type="other">Émile VANDERBURCK</addname>
						<addname type="other">Émile VAN DER BURCK</addname>
					</name>
					<date from="1794" to="1862">1794-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>168 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE BANDEAU</title>
						<author>BOUILLY ET EM. VANDERBURCH</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GALLICA</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9782259n.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La Bibliothèque nationale de France</repository>
								<idno type="URL">https ://catalogue.bnf.fr/ark :/12148/cb31531335q</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">21 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BLV4" modus="cp" lm_max="10">
	<head type="tune">AIR du Vaudeville de Partie carrée.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="9"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="1.3" punct="vg:3">cli<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="3" punct="vg">en</seg>s</w>, <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="5">e</seg>c</w> <w n="1.5">s<seg phoneme="o" type="vs" value="1" rule="434" place="6">o</seg>ll<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>t<seg phoneme="y" type="vs" value="1" rule="449" place="9">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10">e</seg></w></l>
		<l n="2" num="1.2" lm="10"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="300" place="4">ai</seg>s</w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="2.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="6">en</seg>s</w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="2.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8">e</seg>s</w> <w n="2.7" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pv">ou</seg>rs</w> ;</l>
		<l n="3" num="1.3" lm="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="3.2">l</w>'<w n="3.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="3.4">p<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>t</w> <w n="3.5">c<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="3.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>t<seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
		<l n="4" num="1.4" lm="8"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">l</w>'<w n="4.3">h<seg phoneme="i" type="vs" value="1" rule="496" place="2">y</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="220" place="3">en</seg></w> <w n="4.4">pr<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>ge<seg phoneme="a" type="vs" value="1" rule="315" place="6">a</seg></w> <w n="4.5" punct="pt:8">t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">ou</seg>rs</w>.</l>
		<l n="5" num="1.5" lm="10"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="5.2">d</w>'<w n="5.3"><seg phoneme="i" type="vs" value="1" rule="496" place="2">y</seg></w> <w n="5.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="3">en</seg></w> <w n="5.5">v<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>r</w> <w n="5.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.7" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s<seg phoneme="e" type="vs" value="1" rule="382" place="7">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="vg">ai</seg>s</w>, <w n="5.8">p<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>r</w> <w n="5.9" punct="pv:10">c<seg phoneme="o" type="vs" value="1" rule="317" place="10">au</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
		<l n="6" num="1.6" lm="10"><w n="6.1" punct="vg:1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1" punct="vg">ou</seg>s</w>, <w n="6.2">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="2">oin</seg>s</w> <w n="6.3" punct="vg:4">pr<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4" punct="vg">en</seg>t</w>, <w n="6.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="6.5">b<seg phoneme="a" type="vs" value="1" rule="339" place="6">â</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="8">ez</seg></w> <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="9">en</seg></w> <w n="6.7">l</w>'<w n="6.8" punct="dp:10"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="10" punct="dp">ai</seg>r</w> :</l>
		<l n="7" num="1.7" lm="10"><w n="7.1">P<seg phoneme="ø" type="vs" value="1" rule="397" place="1">eu</seg>t</w>-<w n="7.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="7.3">tr<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="7.4">l</w>'<w n="7.5">h<seg phoneme="i" type="vs" value="1" rule="496" place="5">y</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="220" place="6">en</seg></w> <w n="7.6">c<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>r</w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="7.8" punct="vg:10">r<seg phoneme="o" type="vs" value="1" rule="443" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
		<l n="8" num="1.8" lm="6"><w n="8.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="8.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="8.3">n</w>'<w n="8.4"><seg phoneme="i" type="vs" value="1" rule="496" place="3">y</seg></w> <w n="8.5">v<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>t</w> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="8.7" punct="pi:6">cl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" punct="pi">ai</seg>r</w> ?</l>
	</lg>
</div></body></text></TEI>