<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE LIEUTENANT D'ARTILLERIE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="NSL" sort="2">
					<name>
						<forename>Jean-Pierre-Laurent</forename>
						<surname>DENOMBRET</surname>
						<addName type="pen_name">Charles NOMBRET SAINT-LAURENT</addName>
					</name>
					<date from="1791" to="1833">1791-1833</date>
				</author>
					<author key="DUV" sort="3">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>225 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SND_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=NWU:35556007830409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
								<repository>Northwestern university</repository>
								<idno type="URI">https://babel.hathitrust.org/cgi/pt?id=ien.35556007830409</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE VII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SND20" modus="cp" lm_max="10">
				<head type="tune">Air de Julie.</head>
					<lg n="1">
						<head type="main">CABOURDIN.</head>
						<l n="1" num="1.1" lm="8"><space unit="char" quantity="4"></space><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="1.2"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>bt<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r</w> <w n="1.3"><seg phoneme="y" type="vs" value="1" rule="452" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.4" punct="vg:8">b<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="10"><w n="2.1">J</w>'<w n="2.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="2.3" punct="vg:4">tr<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="vg">é</seg></w>, <w n="2.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5" punct="vg:6">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="vg">oi</seg>s</w>, <w n="2.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.7">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg></w> <w n="2.8" punct="vg:10">m<seg phoneme="wa" type="vs" value="1" rule="439" place="9">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="362" place="10" punct="vg">en</seg></w>,</l>
						<l n="3" num="1.3" lm="8"><space unit="char" quantity="4"></space><w n="3.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="188" place="1" punct="vg">E</seg>t</w>, <w n="3.2">qu<seg phoneme="wa" type="vs" value="1" rule="280" place="2">oi</seg>qu</w>'<w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="3.4">d<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.5">l</w>'<w n="3.6" punct="vg:8"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>rd<seg phoneme="o" type="vs" value="1" rule="434" place="7">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="8"><space unit="char" quantity="4"></space><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="4.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>x</w> <w n="4.3">s</w>'<w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="4.5">tr<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="7">è</seg>s</w>-<w n="4.7" punct="pt:8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="8" punct="pt">en</seg></w>.</l>
						<l n="5" num="1.5" lm="8"><space unit="char" quantity="4"></space><w n="5.1">D</w>'<w n="5.2" punct="pv:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2" punct="pv">o</seg>rd</w> ; <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="5.5">l<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>s</w> <w n="5.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="5.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>nt</w></l>
						<l n="6" num="1.6" lm="8"><space unit="char" quantity="4"></space><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="6.2">m<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4">p<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>ds</w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="6.6">n<seg phoneme="o" type="vs" value="1" rule="437" place="7">o</seg>s</w> <w n="6.7" punct="pv:8">f<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="8" punct="pv">oin</seg>s</w> ;</l>
						<l n="7" num="1.7" lm="8"><space unit="char" quantity="4"></space><w n="7.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1" punct="vg">ai</seg>s</w>, <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3" punct="vg:3">gr<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="vg">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="7.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="7.6">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="8">oin</seg>s</w></l>
						<l n="8" num="1.8" lm="8"><space unit="char" quantity="4"></space><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="8.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s<seg phoneme="o" type="vs" value="1" rule="434" place="4">o</seg>mm<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>rs</w> <w n="8.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.5" punct="pt:8">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>nt</w>.</l>
					</lg>
				</div></body></text></TEI>