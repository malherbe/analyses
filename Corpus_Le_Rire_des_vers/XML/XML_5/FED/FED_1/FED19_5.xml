<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANDRÉ LE CHANSONNIER</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="FTN" sort="1">
					<name>
						<forename>Louis Marie</forename>
						<surname>FONTAN</surname>
					</name>
					<date from="1801" to="1839">1801-1839</date>
				</author>
				<author key="DNY" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>DESNOYER</surname>
					</name>
					<date from="1806" to="1858">1806-1858</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>239 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>André le chansonnier.</title>
						<author>FONTAN ET DESNOYER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=aN6oyNpF3cMC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>André le chansonnier.</title>
								<author>FONTAN ET DESNOYER</author>
								<repository>Biblioteca Casanatense</repository>
								<idno type="URI">http://opac.casanatense.it/Record.htm?idlist=3</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Ode et Wodon</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-15" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE SECOND.</head><head type="main_subpart">SCÈNE XIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FED19" modus="cm" lm_max="10">
				<head type="tune">AIR de Téniers.</head>
					<lg n="1">
						<head type="main">ANDRÉ.</head>
						<l n="1" num="1.1" lm="10"><w n="1.1">P<seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.2" punct="vg:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">in</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>s<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="vg">é</seg></w>, <w n="1.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="1.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="1.5">d<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>rs</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="1.7">v<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
						<l n="2" num="1.2" lm="10"><w n="2.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="2.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="2.5">v<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>x</w> <w n="2.6">d</w>'<w n="2.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="8">un</seg></w> <w n="2.8" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" punct="vg">an</seg>t</w>,</l>
						<l n="3" num="1.3" lm="10"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="3.2">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>r</w> <w n="3.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="3.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="3.7">s</w>'<w n="3.8" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="9">en</seg><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="10"><w n="4.1">P<seg phoneme="ø" type="vs" value="1" rule="397" place="1">eu</seg>t</w>-<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="4.4">v<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="4.5">fu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>r</w> <w n="4.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</w> <w n="4.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="8">un</seg></w> <w n="4.8" punct="pt:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" punct="pt">an</seg>t</w>.</l>
						<l n="5" num="1.5" lm="10"><w n="5.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="vg">on</seg></w>, <w n="5.2">d<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="5.3">b<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="5.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="5.6">v<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>t</w> <w n="5.7">l</w>'<w n="5.8" punct="pv:10"><seg phoneme="o" type="vs" value="1" rule="317" place="9">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="442" place="10">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
						<l n="6" num="1.6" lm="10"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg></w> <w n="6.3">d<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>t</w> <w n="6.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="6.5">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="6.7" punct="pt:10">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" punct="pt">on</seg></w>.</l>
						<l n="7" num="1.7" lm="10"><w n="7.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2" punct="vg">en</seg>ds</w>, <w n="7.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="7.3" punct="vg:4">m<seg phoneme="y" type="vs" value="1" rule="449" place="4" punct="vg">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="7.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="7.5">n</w>'<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="7.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>s</w> <w n="7.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">em</seg>ps</w> <w n="7.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="10">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
						<l n="8" num="1.8" lm="10"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ct<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="8.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="8.5">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="409" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="8.6" punct="pt:10">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" punct="pt">on</seg></w>.</l>
						<l n="9" num="1.9" lm="10"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">n</w>'<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg></w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="9.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="9.6">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="9.7">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="409" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="9.8" punct="pt:10">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" punct="pt">on</seg></w>.</l>
					</lg>
				</div></body></text></TEI>