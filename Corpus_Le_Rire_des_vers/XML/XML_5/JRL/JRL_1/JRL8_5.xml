<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHABERT</title>
				<title type="sub">HISTOIRE CONTEMPORAINE EN DEUX ACTES, MÊLÉE DE CHANT</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="ARJ" sort="1">
					<name>
						<forename>Jacques</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1790" to="1855">1790-1855</date>
				</author>
				<author key="LUR" sort="2">
					<name>
						<forename>Louis</forename>
						<surname>LURINE</surname>
					</name>
					<date from="1810" to="1860">1810-1860</date>
				</author>
				<editor>Le rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>110 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">JRL_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
				   </p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CHABERT</title>
						<author>JACQUES ARAGO ET LOUIS LURINE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=vldoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849877.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">2 JUILLET 1832</date>
				<placeName>
					<settlement>THÉÂTRE NATIONAL DU VAUDEVILLE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="JRL8" modus="cm" lm_max="10">
	<head type="tune">AIR d'Aristippe.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="10"><w n="1.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="1.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="1.3">s<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>l</w> <w n="1.4" punct="vg:4">m<seg phoneme="o" type="vs" value="1" rule="437" place="4" punct="vg">o</seg>t</w>, <w n="1.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.6" punct="pe:10">m<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9">o</seg>rph<seg phoneme="o" type="vs" value="1" rule="443" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe">e</seg></w> !</l>
		<l n="2" num="1.2" lm="10"><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>t</w> <w n="2.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="2.3">b<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="2.4">s</w>'<w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg></w> <w n="2.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8">an</seg>s</w> <w n="2.8" punct="pt:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pt">ou</seg>r</w>.</l>
		<l n="3" num="1.3" lm="10"><w n="3.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="3.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="4">oi</seg></w> <w n="3.4">s<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="3.5">pr<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="3.6">d<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg>p<seg phoneme="o" type="vs" value="1" rule="443" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
		<l n="4" num="1.4" lm="10"><w n="4.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="1">en</seg></w> <w n="4.2">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="2">oin</seg>s</w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>r</w> <w n="4.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="4.6">f<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>l</w> <w n="4.7" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pt">ou</seg>r</w>.</l>
		<l n="5" num="1.5" lm="10"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="352" place="3">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rs</w> <w n="5.4">v<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="5.6" punct="pv:10">r<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">om</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
		<l n="6" num="1.6" lm="10"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="6.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="6.4">j</w>'<w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg></w> <w n="6.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="6.7" punct="dp:10">s<seg phoneme="o" type="vs" value="1" rule="443" place="8">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="381" place="9">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="dp">er</seg></w> :</l>
		<l n="7" num="1.7" lm="10"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">m</w>'<w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>s</w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6">e</seg>c</w> <w n="7.5"><seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
		<l n="8" num="1.8" lm="10"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rds</w> <w n="8.4">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="5">en</seg>t</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.7" punct="pt:10">r<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381" place="9">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pt">er</seg></w>.</l>
	</lg>
</div></body></text></TEI>