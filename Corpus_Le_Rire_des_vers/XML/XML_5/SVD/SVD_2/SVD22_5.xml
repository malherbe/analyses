<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANGÉLIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE V.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD22" modus="sp" lm_max="8">
				<head type="tune">Même Air.</head>
				<lg n="1">
					<head type="main">ANGÉLIQUE.</head>
					<l n="1" num="1.1" lm="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2" punct="vg:2">s<seg phoneme="wa" type="vs" value="1" rule="419" place="2" punct="vg">oi</seg>r</w>, <w n="1.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="1.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.5">d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t</w> <w n="1.6">qu</w>'<w n="1.7"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>l</w> <w n="1.8">m</w>'<w n="1.9" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="442" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
				</lg>
				<lg n="2">
					<head type="main">DELAUNAY.</head>
					<l n="2" num="2.1" lm="8"><w n="2.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="2.2" punct="vg:4">r<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="vg">ez</seg></w>, <w n="2.3">j</w>'<w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="2.5">su<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>s</w> <w n="2.6" punct="ps:8">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="ps">ain</seg></w> …</l>
					</lg>
				<lg n="3">
					<head type="main">ANGÉLIQUE.</head>
					<l n="3" num="3.1" lm="8"><w n="3.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="3.2" punct="vg:3">m<seg phoneme="œ" type="vs" value="1" rule="150" place="2">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="3" punct="vg">eu</seg>r</w>, <w n="3.3">c<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r</w> <w n="3.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="3.5">v<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>t</w> <w n="3.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="4" num="3.2" lm="8"><w n="4.1">M</w>'<w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">em</seg>br<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="4.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.5" punct="pt:8">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" punct="pt">in</seg></w>.</l>
					</lg>
				<lg n="4">
					<head type="main">DELAUNAY.</head>
				<l n="5" num="4.1" lm="8"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="5.3">lu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="5.4">r<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>st<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="5.5">p<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>t</w>-<w n="5.6" punct="pi:8"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi ps">e</seg></w> ?…</l>
				</lg>
				<lg n="5">
					<head type="main">ANGÉLIQUE.</head>
					<l n="6" num="5.1" lm="8"><w n="6.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="443" place="1" punct="pe">O</seg>h</w> ! <w n="6.2" punct="pe:2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="pe ps">on</seg></w> !… <w n="6.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">lu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="6.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>ds</w> <w n="6.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="6.7">d<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>x</w> <w n="6.8" punct="pt:8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="8" punct="pt">ain</seg>s</w>.</l>
					</lg>
				<lg n="6">
					<head type="main">DELAUNAY.</head>
					<l n="7" num="6.1" lm="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="132" place="1">E</seg>h</w> <w n="7.2" punct="pe:2">qu<seg phoneme="wa" type="vs" value="1" rule="280" place="2" punct="pe">oi</seg></w> ! <w n="7.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w> <w n="7.5">lu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="7.6" punct="ps:8">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
					</lg>
				<lg n="7">
					<head type="main">ANGÉLIQUE.</head>
					<l n="8" num="7.1" lm="7"><space unit="char" quantity="2"></space><w n="8.1">Pu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="8.3">s<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="8.4" punct="pt:7">v<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" punct="pt">in</seg>s</w>. <del type="repetition" hand="LG" reason="analysis">(bis.)</del></l>
					<l n="9" num="7.2" lm="7"><space unit="char" quantity="2"></space><subst type="repetition" hand="LG" reason="analysis"><del> </del><add rend="hidden"><w n="9.1">Pu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="9.3">s<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="9.4" punct="pe:7">v<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" punct="pe">in</seg>s</w> !</add></subst></l>
				</lg>
			</div></body></text></TEI>