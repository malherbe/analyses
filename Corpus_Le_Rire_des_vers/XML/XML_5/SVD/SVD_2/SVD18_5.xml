<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANGÉLIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE II.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD18" modus="sp" lm_max="7">
			<head type="tune">AIR : Garde à vous.</head>
				<lg n="1">
					<head type="main"></head>
					<l n="1" num="1.1" lm="3"><space unit="char" quantity="8"></space><w n="1.1">G<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rʼ</w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w>-<w n="1.3">dʼs<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <del reason="analysis" hand="LG" type="repetition">(bis)</del></l>
					<l n="2" num="1.2" lm="3"><space unit="char" quantity="8"></space><subst reason="analysis" hand="LG" type="repetition"><del> </del><add rend="hidden"><w n="2.1">G<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rʼ</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w>-<w n="2.3">dʼs<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> </add></subst></l>
					<l n="3" num="1.3" lm="6"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="3.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">em</seg>ps</w> <w n="3.6" punct="vg:6">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="6"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="4.2">dʼv<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="4.3"><seg phoneme="y" type="vs" value="1" rule="452" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4">gr<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
					<l n="5" num="1.5" lm="6"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="5.2">fl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="5.3">r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>st<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w> <w n="5.4" punct="ps:6">t<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="ps">ou</seg>s</w>…</l>
					<l n="6" num="1.6" lm="3"><space unit="char" quantity="8"></space><w n="6.1">G<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rʼ</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w>-<w n="6.3" punct="pe:3">dʼs<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="pe">ou</seg>s</w> !</l>
					<l n="7" num="1.7" lm="6"><w n="7.1">F<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ttʼs</w> <w n="7.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">d<seg phoneme="i" type="vs" value="1" rule="466" place="5">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
					<l n="8" num="1.8" lm="6"><w n="8.1">S<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="8.3">r<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.4" punct="vg:6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
					<l n="9" num="1.9" lm="7"><w n="9.1">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>dʼs</w> <w n="9.2">d<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="9.4" punct="vg:7">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>b<seg phoneme="u" type="vs" value="1" rule="424" place="7" punct="vg">ou</seg>s</w>,</l>
					<l n="10" num="1.10" lm="3"><space unit="char" quantity="8"></space><w n="10.1">G<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rʼ</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w>-<w n="10.3" punct="pe:3">dʼs<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="pe">ou</seg>s</w> !</l>
				</lg>
			</div></body></text></TEI>