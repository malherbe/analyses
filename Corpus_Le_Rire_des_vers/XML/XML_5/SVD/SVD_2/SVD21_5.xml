<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANGÉLIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE V.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD21" modus="sp" lm_max="8">
				<head type="tune">Air : de Madame Duchambge.</head>
				<lg n="1">
					<head type="main">JULES.</head>
					<l n="1" num="1.1" lm="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2" punct="vg:3">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3" punct="vg">in</seg></w>, <w n="1.3">l<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.5">m</w>'<w n="1.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="8"><w n="2.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="2.3">s<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="2.4">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.7" punct="ps:8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8" punct="ps">en</seg>ds</w>…</l>
				</lg>
				<lg n="2">
					<head type="main">DELAUNAY.</head>
					<l n="3" num="2.1" lm="8"><w n="3.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1" punct="vg">ai</seg>s</w>, <w n="3.2" punct="vg:3">m<seg phoneme="œ" type="vs" value="1" rule="150" place="2">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="3" punct="vg">eu</seg>r</w>, <w n="3.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>d</w> <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.5" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="381" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l part="I" n="4" num="2.2" lm="8"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w>-<w n="4.3" punct="pi:4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="pi ps">ou</seg>s</w> ?… </l>
					</lg>
				<lg n="3">
					<head type="main">JULES.</head>
					<l part="F" n="4" lm="8"><w n="4.4" punct="vg:6">D<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="4.5">j</w>'<w n="4.6" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8" punct="pe">en</seg>ds</w> !</l>
					<l n="5" num="3.1" lm="8"><w n="5.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2" punct="vg">è</seg>s</w>, <w n="5.2">s<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>r</w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="5.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg></w> <w n="5.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6">l</w>'<w n="5.7" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">em</seg>br<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="6" num="3.2" lm="8"><w n="6.1">C</w>'<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="6.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="6.4">b<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="6.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="6.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="6.7" punct="pt:8">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" punct="pt">in</seg>s</w>.</l>
					</lg>
				<lg n="4">
					<head type="main">DELAUNAY.</head>
					<l n="7" num="4.1" lm="7"><space unit="char" quantity="2"></space><w n="7.1" punct="pe:1">Qu<seg phoneme="wa" type="vs" value="1" rule="280" place="1" punct="pe ps">oi</seg></w> !… <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="7.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t</w> <w n="7.5">d</w>'<w n="7.6" punct="pe:7"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>d<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
					</lg>
				<lg n="5">
					<head type="main">JULES.</head>
					<l n="8" num="5.1" lm="7"><space unit="char" quantity="2"></space><w n="8.1">Pu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="8.3">s<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="8.4" punct="pt:7">v<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" punct="pt">in</seg>s</w>. <del reason="analysis" type="repetition" hand="LG">(bis.)</del></l>
					<l n="9" num="5.2" lm="7"><space unit="char" quantity="2"></space><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="9.1">Pu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="9.3">s<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="9.4" punct="pe:7">v<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" punct="pe">in</seg>s</w> !</add></subst></l>
				</lg>
			</div></body></text></TEI>