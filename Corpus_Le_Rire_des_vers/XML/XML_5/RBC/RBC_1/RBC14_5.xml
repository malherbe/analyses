<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE VI.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC14" modus="cp" lm_max="15">
				<head type="tune">AIR: Vous qui d'amoureuse aventure.</head>
				<lg n="1">
					<head type="main">LA VALEUR.</head>
					<l n="1" num="1.1" lm="8"><space unit="char" quantity="14"></space><w n="1.1">V<seg phoneme="e" type="vs" value="1" rule="382" place="1">e</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg></w> <w n="1.3">s<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>l<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>t</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="1.6" punct="vg:8">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="8"><space unit="char" quantity="14"></space><w n="2.1">V<seg phoneme="e" type="vs" value="1" rule="382" place="1">e</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg></w> <w n="2.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="4">ain</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="5">en</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="2.6" punct="vg:8">dr<seg phoneme="wa" type="vs" value="1" rule="419" place="8" punct="vg">oi</seg>ts</w>,</l>
					<l n="3" num="1.3" lm="8"><space unit="char" quantity="14"></space><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="3.2">qu</w>'<w n="3.3"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>rd</w>'<w n="3.4">hu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="3.5">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.6">v<seg phoneme="a" type="vs" value="1" rule="306" place="7">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="4" num="1.4" lm="8"><space unit="char" quantity="14"></space><w n="4.1">R<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="4">o</seg>s</w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>ci<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="6">en</seg>s</w> <w n="4.4" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>xpl<seg phoneme="wa" type="vs" value="1" rule="419" place="8" punct="pt">oi</seg>ts</w>.</l>
					<l n="5" num="1.5" lm="15"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="5.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <subst reason="analysis" type="repetition" hand="LG"><del>(bis) </del><add rend="hidden"><w n="5.3">S<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="5.4">j<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s</w></add></subst> <w n="5.5">l</w>'<w n="5.6"><seg phoneme="e" type="vs" value="1" rule="169" place="7">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg></w> <w n="5.7">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307" place="12">ai</seg>t</w> <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="13">a</seg></w> <w n="5.9" punct="vg:15">p<seg phoneme="a" type="vs" value="1" rule="339" place="14">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="15">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="16" punct="vg">e</seg></w>,</l>
					<l n="6" num="1.6" lm="15"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="6.2" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="pe">an</seg>t</w> ! <subst reason="analysis" type="repetition" hand="LG"><del>(bis) </del><add rend="hidden"><w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">En</seg></w> <w n="6.4" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="pe">an</seg>t</w> !</add></subst> <w n="6.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="6.6"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ri<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>s</w> <w n="6.7"><seg phoneme="o" type="vs" value="1" rule="317" place="10">au</seg></w>-<w n="6.8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12">an</seg>t</w> <w n="6.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="13">e</seg></w> <w n="6.10">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="14">e</seg>s</w> <w n="6.11" punct="pt:15">c<seg phoneme="u" type="vs" value="1" rule="424" place="15" punct="pt">ou</seg>ps</w>.</l>
					<l n="7" num="1.7" lm="8"><space unit="char" quantity="14"></space><w n="7.1">Lʼ</w> <w n="7.2">s<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="381" place="2">e</seg>il</w> <w n="7.3">dʼ</w> <w n="7.4">l</w>'<w n="7.5"><seg phoneme="e" type="vs" value="1" rule="408" place="3">É</seg>g<seg phoneme="i" type="vs" value="1" rule="492" place="4">y</seg>pt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="7.7">dʼ</w> <w n="7.8">l</w>'<w n="7.9"><seg phoneme="i" type="vs" value="1" rule="467" place="6">I</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>l<seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="8" num="1.8" lm="8"><space unit="char" quantity="14"></space><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>r</w> <w n="8.3">br<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="8.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r</w> <w n="8.5" punct="pt:8">n<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">ou</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>