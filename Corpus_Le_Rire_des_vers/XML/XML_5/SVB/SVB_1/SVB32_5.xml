<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB32" modus="cm" lm_max="10">
	<head type="tune">Air : O mon pays. (Madame Duchambge.)</head>
	<lg n="1">
		<l n="1" num="1.1" lm="10"><w n="1.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">cr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="1.3" punct="pv:4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="4" punct="pv">en</seg></w> ; <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5" punct="ps:7">m<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="467" place="7" punct="ps">i</seg>s</w>… <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="1.7">s<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
		<l n="2" num="1.2" lm="10"><w n="2.1">C<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>t</w> <w n="2.2" punct="ps:4">t<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="ps">ou</seg>t</w>… <w n="2.3">g<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="2.4">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="2.5" punct="vg:10">b<seg phoneme="o" type="vs" value="1" rule="443" place="9">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="vg">eu</seg>r</w>,</l>
		<l n="3" num="1.3" lm="10"><w n="3.1" punct="vg:2">R<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>gn<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="vg">ez</seg></w>, <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="3.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="3.6">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="3.7" punct="pv:10">pu<seg phoneme="i" type="vs" value="1" rule="490" place="9">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
		<l n="4" num="1.4" lm="10"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="4.3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="3">oi</seg></w> <w n="4.4">s<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rti<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="7">en</seg>t</w> <w n="4.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg></w> <w n="4.7" punct="pt:10">h<seg phoneme="o" type="vs" value="1" rule="443" place="9">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="pt">eu</seg>r</w>.</l>
		<l n="5" num="1.5" lm="10"><w n="5.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="5.3" punct="vg:4">l<seg phoneme="wa" type="vs" value="1" rule="419" place="4" punct="vg">oi</seg>s</w>, <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.5" punct="pt:10">d<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>p<seg phoneme="o" type="vs" value="1" rule="443" place="8">o</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt">e</seg></w>.</l>
		<l n="6" num="1.6" lm="10"><w n="6.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="6.2" punct="vg:2">cr<seg phoneme="i" type="vs" value="1" rule="466" place="2" punct="vg">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="6.4">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">in</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.7">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.8"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="9">un</seg></w> <w n="6.9" punct="vg:10">j<seg phoneme="ø" type="vs" value="1" rule="397" place="10" punct="vg">eu</seg></w>,</l>
		<l n="7" num="1.7" lm="10"><w n="7.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="7.3">j<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="418" place="7">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg></w> <w n="7.6" punct="vg:10">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
		<l n="8" num="1.8" lm="10"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="8.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="341" place="9">à</seg></w> <w n="8.7" punct="pe:10">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="10" punct="pe">eu</seg></w> !</l> 
	</lg>
</div></body></text></TEI>