<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB20" modus="sp" lm_max="7">
	<head type="tune">Air : Est-il supplice égal.</head>
		<div type="section" n="1">
			<lg n="1">
				<l n="1" num="1.1" lm="6"><w n="1.1">Pr<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>x</w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="1.4" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>v<seg phoneme="u" type="vs" value="1" rule="428" place="5">ou</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="6" punct="vg">en</seg>t</w>,</l>
				<l n="2" num="1.2" lm="6"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">t</w>'<w n="2.3"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="2.5" punct="pt:6">r<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>g<seg phoneme="i" type="vs" value="1" rule="466" place="5">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" punct="pt">en</seg>t</w>.</l>
			</lg>
			<lg n="2">
			<head type="speaker">DESŒILLETS.</head>
				<l n="3" num="2.1" lm="6"><w n="3.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="3.2" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="vg">an</seg>t</w>, <w n="3.3" punct="pe:6">c<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></w> !</l>
			</lg> 
			<lg n="3">
			<head type="speaker">DUPOIS.</head>
				<l n="4" num="3.1" lm="6"><w n="4.1" punct="pe:1">Qu<seg phoneme="wa" type="vs" value="1" rule="280" place="1" punct="pe">oi</seg></w> ! <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="4.5" punct="pi:6">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6" punct="pi">e</seg>l</w> ?</l>
			</lg>
			<lg n="4">
			<head type="speaker">HENRI.</head>
				<l n="5" num="4.1" lm="6"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="5.4" punct="pt:6">c<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>l<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6" punct="pt">e</seg>l</w>.</l>
			</lg>
			<lg n="5">
			<head type="speaker">DESŒILLETS.</head>
				<l n="6" num="5.1" lm="6"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="6.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5" punct="pe:6">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></w> !</l>
			</lg>
			<lg n="6">
			<head type="speaker">HENRI.</head>
				<l n="7" num="6.1" lm="4"><w n="7.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="7.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4" punct="ps:4">pr<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="ps">i</seg>x</w>…</l>
			</lg>
			<lg n="7">
			<head type="speaker">DUPUIS.</head>
				<l n="8" num="7.1" lm="6"><w n="8.1" punct="vg:3">V<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>ti<seg phoneme="e" type="vs" value="1" rule="346" place="3" punct="vg">er</seg>s</w>, <w n="8.2">j</w>'<w n="8.3"><seg phoneme="i" type="vs" value="1" rule="496" place="4">y</seg></w> <w n="8.4" punct="pt:6">s<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>scr<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="pt">i</seg>s</w>.</l>
				<l n="9" num="7.2" lm="6"><w n="9.1" punct="vg:2">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="1">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="2" punct="vg">ô</seg>t</w>, <w n="9.2">g<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="9.4" punct="vg:6">pl<seg phoneme="ɛ" type="vs" value="1" rule="304" place="6">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
				<l n="10" num="7.3" lm="4"><w n="10.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="314" place="2">eau</seg></w> <w n="10.2" punct="vg:4">P<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>s</w>,</l>
				<l n="11" num="7.4" lm="6"><w n="11.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="11.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">in</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="442" place="3">o</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="11.3" punct="vg:6">C<seg phoneme="i" type="vs" value="1" rule="492" place="5">y</seg>pr<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg">i</seg>s</w>,</l>
				<l n="12" num="7.5" lm="7"><w n="12.1">J</w>'<w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-27" place="5">e</seg></w> <w n="12.4" punct="pe:7">H<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409" place="7">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
			</lg>
		</div>
		<div type="section" n="2">
			<head type="main">ENSEMBLE.</head>
			<lg n="1">
			<head type="speaker">DUPUIS.</head>
				<l n="13" num="1.1" lm="6"><w n="13.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="13.2" punct="vg:3">vr<seg phoneme="ɛ" type="vs" value="1" rule="304" place="2">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="3" punct="vg">en</seg>t</w>, <w n="13.3">c</w>'<w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="13.5" punct="vg:6">ch<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg">an</seg>t</w>,</l>
				<l n="14" num="1.2" lm="6"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>r</w> <w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="14.3" punct="pe:6">r<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>g<seg phoneme="i" type="vs" value="1" rule="466" place="5">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" punct="pe">en</seg>t</w> !</l>
				<l n="15" num="1.3" lm="6"><w n="15.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="15.2">t<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>l</w> <w n="15.3">pr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>x</w> <w n="15.4">m</w>'<w n="15.5" punct="pt:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
				<l n="16" num="1.4" lm="6"><w n="16.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="16.5" punct="vg:6">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6" punct="vg">e</seg>l</w>,</l>
				<l n="17" num="1.5" lm="6"><w n="17.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="17.3" punct="dp:6">c<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>l<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6" punct="dp">e</seg>l</w> :</l>
				<l n="18" num="1.6" lm="6"><w n="18.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="18.2">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="18.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r</w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.5" punct="pt:6">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
			</lg>
			<lg n="2">
			<head type="speaker">HENRI et DESŒILLETS.</head>
				<l n="19" num="2.1" lm="6"><w n="19.1">Pr<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>x</w> <w n="19.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="19.4" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>v<seg phoneme="u" type="vs" value="1" rule="428" place="5">ou</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="6" punct="vg">en</seg>t</w>,</l>
				<l n="20" num="2.2" lm="6"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>pt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="20.3" punct="pt:6">r<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>g<seg phoneme="i" type="vs" value="1" rule="466" place="5">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" punct="pt">en</seg>t</w>.</l>
				<l n="21" num="2.3" lm="6"><w n="21.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="21.2" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="vg">an</seg>t</w>, <w n="21.3" punct="pe:6">c<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></w> !</l>
				<l n="22" num="2.4" lm="6"><w n="22.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="22.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="22.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="22.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="22.5" punct="vg:6">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6" punct="vg">e</seg>l</w>,</l>
				<l n="23" num="2.5" lm="6"><w n="23.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="23.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="23.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="23.4" punct="dp:6">c<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>l<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6" punct="dp">e</seg>l</w> :</l>
				<l n="24" num="2.6" lm="6"><w n="24.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="24.2">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="24.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r</w> <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="24.5" punct="pt:6">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
			</lg>
		</div>
</div></body></text></TEI>