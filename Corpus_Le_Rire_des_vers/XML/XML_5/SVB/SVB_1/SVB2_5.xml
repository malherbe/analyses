<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB2" modus="sm" lm_max="8">
	<head type="tune">Air :</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8"><w n="1.1" punct="vg:2">Vr<seg phoneme="ɛ" type="vs" value="1" rule="304" place="1">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="2" punct="vg">en</seg>t</w>, <w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="1.3">v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s</w> <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="1.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">em</seg>ps</w></l>
		<l n="2" num="1.2" lm="8"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Où</seg></w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="2.3">c<seg phoneme="œ" type="vs" value="1" rule="248" place="3">œu</seg>rs</w> <w n="2.4">tr<seg phoneme="o" type="vs" value="1" rule="432" place="4">o</seg>p</w> <w n="2.5">t<seg phoneme="o" type="vs" value="1" rule="414" place="5">ô</seg>t</w> <w n="2.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.7" punct="pv:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>vi<seg phoneme="ɛ" type="vs" value="1" rule="365" place="8">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>nt</w> ;</l>
		<l n="3" num="1.3" lm="8"><w n="3.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="3.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="3.3">n<seg phoneme="o" type="vs" value="1" rule="437" place="5">o</seg>s</w> <w n="3.4">j<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="3.5" punct="vg:8">g<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="8" punct="vg">en</seg>s</w>,</l>
		<l n="4" num="1.4" lm="8"><w n="4.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">qu</w>'<w n="4.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="4.4">l<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r</w> <w n="4.5" punct="vg:5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>f<seg phoneme="y" type="vs" value="1" rule="449" place="5" punct="vg">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="4.6"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>ls</w> <w n="4.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.8" punct="pt:8">pr<seg phoneme="ɛ" type="vs" value="1" rule="365" place="8">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>nt</w>.</l> 
	</lg>
		<stage>( A demi-voiw.)</stage> 
	<lg n="2">
		<l n="5" num="2.1" lm="8"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="2">ain</seg>s</w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="5.4" punct="vg:5">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406" place="5" punct="vg">eu</seg>r</w>, <w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="5.6" punct="pv:8"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pv">a</seg>t</w> ;</l>
		<l n="6" num="2.2" lm="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="6.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">v<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="6.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.7">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="6.8">f<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
		<l n="7" num="2.3" lm="8"><w n="7.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2" punct="vg:3">f<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="7.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</w> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="7.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="7.6" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>tr<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg>t</w>,</l>
		<l n="8" num="2.4" lm="8"><w n="8.1">L</w>'<w n="8.2">h<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r</w> <w n="8.3">d</w>'<w n="8.4"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>gm<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="8.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="8.6" punct="pt:8">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
	</lg>
</div></body></text></TEI>