<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
				<title type="sub">INVRAISEMBLANCE EN TROIS PARTIES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
				  <name>
					<forename>Félix-Auguste</forename>
					<surname>DUVERT</surname>
				  </name>
				  <date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="DLV" sort="2">
					<name>
						<forename>Augustin Théodore</forename>
						<nameLink>de</nameLink>
						<surname>LAUZANNE DE VAUROUSSEL</surname>
						<addname type="other">Lauzanne</addname>
					</name>
					<date from="1805" to="1877">1805-1877</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>135 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
                    <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
						<author>F.-A. DUVERT EN SOCIÉTÉ AVEC M. LAUZANNE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?vid=UOM:39015076863409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>University of Michigan Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/mdp.39015076863409</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">16 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DLV14" modus="cp" lm_max="10">
	<head type="tune">AIR : Et voilà tout ce que je sais.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="10"><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="1.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>c<seg phoneme="o" type="vs" value="1" rule="434" place="6">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="1.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8">an</seg>s</w> <w n="1.6">v<seg phoneme="o" type="vs" value="1" rule="437" place="9">o</seg>t</w>'<w n="1.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
		<l n="2" num="1.2" lm="10"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="2.4">l</w>'<w n="2.5">pl<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>s</w> <w n="2.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>d</w> <w n="2.7">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="2.8" punct="vg:10">sc<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="vg">a</seg>ts</w>,</l>
		<l n="3" num="1.3" lm="8"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="3.2">h<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>mm</w>' <w n="3.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>t</w> <w n="3.4">c<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rt</w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6" punct="vg:8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>, </l>
		<l n="4" num="1.4" lm="10"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="4.2" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397" place="4" punct="vg">eu</seg>x</w>, <w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="4.4" punct="vg:7">br<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" punct="vg">an</seg>d</w>, <w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="8">un</seg></w> <w n="4.6" punct="vg:10">J<seg phoneme="y" type="vs" value="1" rule="449" place="9">u</seg>d<seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="vg">a</seg>s</w>, </l>
		<l n="5" num="1.5" lm="8"><w n="5.1" punct="vg:3">J<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>th<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="vg">a</seg>s</w>, <w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="5.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.4">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="6">ê</seg>vi<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w> <w n="5.5" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pt">a</seg>s</w>. </l>
		<l n="6" num="1.6" lm="8"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="6.2" punct="vg:2">s<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg></w>, <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>s</w> <w n="6.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5">m<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="6.6">d</w>'<w n="6.7" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="351" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>, </l>
		<l n="7" num="1.7" lm="10"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="7.3">cr<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">c</w>'<w n="7.6">b<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.7">qu</w>'<w n="7.8">j</w>'<w n="7.9"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg></w> <w n="7.10" punct="vg:10">tr<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="vg">é</seg></w>, </l>
		<l n="8" num="1.8" lm="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="8.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>s</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="8.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="8.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="351" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>, </l>
		<l n="9" num="1.9" lm="8"><w n="9.1">J<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>th<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <del reason="analysis" type="repetition" hand="KL">(bis)</del> , <w n="9.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="9.4" punct="pt:8">r<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pt">é</seg></w>. </l>
		<stage>Elle lui remet la lettre, Jonathas reste stupéfait.</stage>
	</lg> 
</div></body></text></TEI>