<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE V.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC9" modus="cp" lm_max="10">
					<lg n="1">
						<head type="main">AIR d'Yelva.</head>
						<l n="1" num="1.1" lm="8"><space unit="char" quantity="4"></space><w n="1.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="1.2" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="vg">ez</seg></w>, <w n="1.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="1.5" punct="pt:8">s<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>ppl<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<head type="main">ARWED.</head>
						<l n="2" num="2.1" lm="10"><w n="2.1">J</w>'<w n="2.2" punct="vg:2">h<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="2.3" punct="pe:4">h<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="pe">a</seg>s</w> ! <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="2.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="2.6"><seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>r</w> <w n="2.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg></w> <w n="2.8" punct="pt:10">c<seg phoneme="œ" type="vs" value="1" rule="248" place="10" punct="pt">œu</seg>r</w>.</l>
						<l n="3" num="2.2" lm="8"><space unit="char" quantity="4"></space><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>s</w> <w n="3.2">d</w>'<w n="3.3"><seg phoneme="y" type="vs" value="1" rule="452" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">f<seg phoneme="a" type="vs" value="1" rule="192" place="5">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.5">j<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>l<seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="4" num="2.3" lm="10"><w n="4.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">d<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>t</w>-<w n="4.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="4.5">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="5">ain</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.6">pl<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>s</w> <w n="4.7">d</w>'<w n="4.8"><seg phoneme="y" type="vs" value="1" rule="452" place="8">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.9" punct="pi:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="9">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="pi">eu</seg>r</w> ?</l>
						<l n="5" num="2.4" lm="10"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="5.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="vg">ai</seg>ts</w>, <w n="5.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>d</w> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6">p<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r</w> <w n="5.7">s<seg phoneme="y" type="vs" value="1" rule="449" place="9">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="10">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
						<l n="6" num="2.5" lm="10"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="6.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="6.4">pi<seg phoneme="e" type="vs" value="1" rule="240" place="4">e</seg>ds</w> <w n="6.5">m<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</w> <w n="6.7">ch<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="6.8" punct="vg:10">j<seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="vg">ou</seg>r</w>,</l>
						<l n="7" num="2.6" lm="10"><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="7.2">r<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4">en</seg></w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.6">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">om</seg>p<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w> <w n="7.7">s<seg phoneme="wa" type="vs" value="1" rule="422" place="9">oi</seg></w>-<w n="7.8">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="10">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
						<l n="8" num="2.7" lm="10"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="8.2">lu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="8.3">f<seg phoneme="œ" type="vs" value="1" rule="303" place="3">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="8.4">l</w>'<w n="8.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg></w> <w n="8.6">d</w>'<w n="8.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="8.8"><seg phoneme="o" type="vs" value="1" rule="317" place="8">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.9" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pt">ou</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>