<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE VII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC23" modus="cp" lm_max="11">
					<head type="tune">AIR : Le choir qu'a fait tout le village,</head>
						<lg n="1">
							<head type="main">ARWED.</head>
							<l n="1" num="1.1" lm="8"><space unit="char" quantity="6"></space><w n="1.1">C<seg phoneme="ɛ" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t</w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.6" punct="vg:8">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>st<seg phoneme="i" type="vs" value="1" rule="466" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="2" num="1.2" lm="10"><w n="2.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="2.2">l</w>'<w n="2.3"><seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="2.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>d</w> <w n="2.5"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>l</w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="2.7">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg></w> <w n="2.8" punct="pt:10">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10" punct="pt">em</seg>ps</w>.</l>
						</lg>
						<lg n="2">
							<head type="main">JOB.</head>
							<l n="3" num="2.1" lm="8"><space unit="char" quantity="6"></space><w n="3.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="3.2" punct="vg:4">c<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4" punct="vg">e</seg>l</w>, <w n="3.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="3.5" punct="vg:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="i" type="vs" value="1" rule="466" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="4" num="2.2" lm="10"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">pu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg>s</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>pt<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="4.6">v<seg phoneme="o" type="vs" value="1" rule="437" place="8">o</seg>s</w> <w n="4.7" punct="pt:10">pr<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="10" punct="pt">en</seg>s</w>.</l>
							<l n="5" num="2.3" lm="10"><w n="5.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3" punct="vg:4">s<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="vg">ez</seg></w>, <w n="5.4">j</w>' <w n="5.5">n</w>'<w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="EVC23_1" place="5">ai</seg>mʼ</w> <w n="5.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.8">v<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="5.9">s<seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>r</w> <w n="5.10">l<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg></w> <w n="5.11" punct="vg:10">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
							<l n="6" num="2.4" lm="10"><w n="6.1">C</w>'<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="6.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="6.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>rs</w> <w n="6.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="6.6">f<seg phoneme="y" type="vs" value="1" rule="444" place="6">û</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="6.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg></w> <w n="6.8" punct="pv:10">s<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="10" punct="pv">en</seg></w> ;</l>
							<l n="7" num="2.5" lm="10"><w n="7.1">V<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></w> <w n="7.3">s<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.4">m</w>'<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="7.6" punct="pv:10">n<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg>c<seg phoneme="e" type="vs" value="1" rule="352" place="9">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
							<l n="8" num="2.6" lm="11"><w n="8.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="8.4">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rds</w> <w n="8.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.6">n</w>'<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg></w> <w n="8.8">pl<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>s</w> <w n="8.9">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="9">oin</seg></w> <w n="8.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="8.11" punct="pt:11">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="11" punct="pt">en</seg></w>.</l>
							<l n="9" num="2.7" lm="10"><w n="9.1">V<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></w> <w n="9.3">s<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.4">m</w>'<w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="9.6" punct="pv:10">n<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg>c<seg phoneme="e" type="vs" value="1" rule="352" place="9">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
							<l n="10" num="2.8" lm="10"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="10.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="10.4">j</w>' <w n="10.5">n</w>'<w n="10.6"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg></w> <w n="10.7">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="8">oin</seg></w> <w n="10.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="10.9" punct="pt:10">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="10" punct="pt">en</seg></w>.</l>
						</lg>
					</div></body></text></TEI>