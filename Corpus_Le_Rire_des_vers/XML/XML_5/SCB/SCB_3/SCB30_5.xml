<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB30" modus="cp" lm_max="10">
	<head type="tune">AIR du Piège.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="1.4" punct="vg:5">s<seg phoneme="y" type="vs" value="1" rule="444" place="5" punct="vg">û</seg>r</w>, <w n="1.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="1.6" punct="pi:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pi">i</seg></w> ?</l>
	</lg>
	<lg n="2">
	<head type="speaker">LE SURINTENDANT.</head>
		<l n="2" num="2.1" lm="8"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">s<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="2.3" punct="vg:4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>d<seg phoneme="œ" type="vs" value="1" rule="406" place="4" punct="vg">eu</seg>r</w>, <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="2.6" punct="pi:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
		<l part="I" n="3" num="2.2" lm="8"><w n="3.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="3.2">j</w>'<w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="3.4" punct="pt:4">r<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="pt">on</seg>ds</w>. </l>
	</lg>
	<lg n="3">
	<head type="speaker">LE GRAND-DUC.</head>
		<l part="F" n="3" lm="8"><w n="3.5">C</w>'<w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="3.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="6">en</seg></w> <w n="3.8" punct="pv:8">h<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pv">i</seg></w> ;</l>
		<l n="4" num="3.1" lm="8"><w n="4.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="4.3">r<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>squ<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="4.4" punct="vg:6">b<seg phoneme="o" type="vs" value="1" rule="314" place="5">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="vg">ou</seg>p</w>, <w n="4.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.6" punct="pt:8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
		<l n="5" num="3.2" lm="8"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="2">er</seg></w> <w n="5.2" punct="vg:4">r<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg">on</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="5.4">v<seg phoneme="o" type="vs" value="1" rule="437" place="6">o</seg>s</w> <w n="5.5" punct="vg:8">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8" punct="vg">en</seg>ts</w>,</l>
		<l n="6" num="3.3" lm="8"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="6.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>d<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="6.4">d</w>'<w n="6.5"><seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6" punct="pe:8"><seg phoneme="o" type="vs" value="1" rule="317" place="8">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
		<l n="7" num="3.4" lm="10"><w n="7.1">C</w>'<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="7.3">d<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="7.4" punct="vg:4">tr<seg phoneme="o" type="vs" value="1" rule="432" place="4" punct="vg">o</seg>p</w>, <w n="7.5">m<seg phoneme="e" type="vs" value="1" rule="352" place="5">e</seg>ssi<seg phoneme="ø" type="vs" value="1" rule="396" place="6">eu</seg>rs</w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="7.7" punct="vg:10">c<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="10" punct="vg">an</seg>s</w>,</l>
		<l n="8" num="3.5" lm="8"><w n="8.1">D</w>'<w n="8.2"><seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="2">er</seg></w> <w n="8.3">r<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="8.6" punct="pt:8">v<seg phoneme="o" type="vs" value="1" rule="414" place="8">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l> 
	</lg>
</div></body></text></TEI>