<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB58" modus="cm" lm_max="10">
	<head type="tune">AIR : Dans un vieux château de l'Andalousie.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="10"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="1.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>di<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w> <w n="1.4">qu</w>'<w n="1.5"><seg phoneme="y" type="vs" value="1" rule="452" place="6">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="1.6">h<seg phoneme="œ̃" type="vs" value="1" rule="260" place="7">um</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="353" place="8">e</seg>x<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
		<l n="2" num="1.2" lm="10"><w n="2.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="2.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>di<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w> <w n="2.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="6">en</seg></w> <w n="2.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.6">d</w>'<w n="2.7"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8" punct="pv:10"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="9">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="pv">é</seg></w> ;</l>
		<l n="3" num="1.3" lm="10"><w n="3.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">om</seg>pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="3.3">j<seg phoneme="wa" type="vs" value="1" rule="422" place="5">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="3.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="3.6" punct="dp:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp">e</seg></w> :</l>
		<l n="4" num="1.4" lm="10"><w n="4.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">pr<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="4.3">s<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="4.4" punct="vg:5">d<seg phoneme="u" type="vs" value="1" rule="424" place="5" punct="vg">ou</seg>x</w>, <w n="4.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.6">l</w>'<w n="4.7"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>s</w> <w n="4.8" punct="pt:10">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="pt">é</seg></w>.</l>
		<l n="5" num="1.5" lm="10"><w n="5.1" punct="vg:3">R<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="351" place="2">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="5.2" punct="vg:5">h<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="5" punct="vg">eu</seg>rs</w>, <w n="5.3" punct="vg:7">p<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="7" punct="vg">oi</seg>r</w>, <w n="5.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>g</w> <w n="5.5" punct="vg:10">s<seg phoneme="y" type="vs" value="1" rule="449" place="9">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="10">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
		<l n="6" num="1.6" lm="10"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">sc<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ptr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">qu</w>'<w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="6.5">r<seg phoneme="wa" type="vs" value="1" rule="422" place="5">oi</seg></w> <w n="6.6">v<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>t</w> <w n="6.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.8" punct="vg:10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="9">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="vg">er</seg></w>,</l>
		<l n="7" num="1.7" lm="10"><w n="7.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="422" place="1" punct="vg">oi</seg></w>, <w n="7.2">j</w>'<w n="7.3"><seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>s</w> <w n="7.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>t</w> <w n="7.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>r</w> <w n="7.6">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="490" place="8">i</seg></w> <w n="7.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="7.8">j</w>'<w n="7.9" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="10">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
		<l n="8" num="1.8" lm="10"><w n="8.1">M</w>'<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="1">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="8.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w> <w n="8.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>r</w> <w n="8.6">t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>t</w> <w n="8.7" punct="pi:10"><seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="9">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pi">er</seg></w> ?</l>
	</lg> 
</div></body></text></TEI>