<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB53" modus="sm" lm_max="8">
	<head type="tune">AIR de Voltaire chez Ninon.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8"><w n="1.1" punct="vg:2">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="vg">ez</seg></w>, <w n="1.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="1.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="63" place="4">e</seg>r</w> <w n="1.4" punct="vg:8">s<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="vg">an</seg>t</w>,</l>
		<l n="2" num="1.2" lm="8"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="2.2">s<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>l<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="2.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
		<l n="3" num="1.3" lm="8"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="3.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">r<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="3.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6">m<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8">en</seg>t</w></l>
		<l n="4" num="1.4" lm="8"><w n="4.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w> <w n="4.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="4.6">p<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r</w> <w n="4.7" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
		<l n="5" num="1.5" lm="8"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="5.3">qu</w>'<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.5" punct="vg:5">v<seg phoneme="ø" type="vs" value="1" rule="397" place="5" punct="vg">eu</seg>t</w>, <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="5.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.8">j<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>r</w></l>
		<l n="6" num="1.6" lm="8"><w n="6.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="6.2">pr<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="6.3">s<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="6.4" punct="pt:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>c<seg phoneme="o" type="vs" value="1" rule="434" place="6">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
		<stage>( Il va à la table et signe un papier.)</stage>
	</lg>
	<lg n="2">
	<head type="speaker">LE SURINTENDANT.</head>
		<l n="7" num="2.1" lm="8"><w n="7.1">S<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="7.2" punct="pe:5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>c<seg phoneme="o" type="vs" value="1" rule="434" place="3">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" punct="pe ps">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! … <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="7.5" punct="pe:8">c<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pe">ou</seg>r</w> !</l>
		<l n="8" num="2.2" lm="8"><w n="8.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="8.2">l</w>'<w n="8.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="8.4">v<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>t</w> <w n="8.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4">en</seg></w> <w n="8.6">qu</w>'<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.8" punct="pt:8">c<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
	</lg> 
</div></body></text></TEI>