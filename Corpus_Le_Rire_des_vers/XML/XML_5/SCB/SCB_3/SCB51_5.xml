<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB51" modus="cm" lm_max="10">
	<head type="tune">AIR du Carnaval.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="10"><w n="1.1" punct="pe:2">C<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="pe">on</seg>s</w> ! <w n="1.2"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="1.3">f<seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>t</w> <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="1.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">om</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="351" place="8">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>ppr<seg phoneme="ɛ" type="vs" value="1" rule="365" place="10">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
		<l n="2" num="1.2" lm="10"><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>t</w> <w n="2.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="2.4">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="4">en</seg>t</w> <w n="2.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="2.8" punct="pv:10">p<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pv">er</seg></w> ;</l>
		<l n="3" num="1.3" lm="10"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="3.3" punct="vg:4">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="3.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="3.6">c<seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="8">e</seg>st</w> <w n="3.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg></w> <w n="3.9" punct="vg:10">si<seg phoneme="ɛ" type="vs" value="1" rule="365" place="10">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
		<l n="4" num="1.4" lm="10"><w n="4.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="4.3">d<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="4.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="4.5">v<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>t</w> <w n="4.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="4.7" punct="pt:10">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">em</seg>pl<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>c<seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pt">er</seg></w>.</l>
		<l n="5" num="1.5" lm="10"><w n="5.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="5.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="5.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg">on</seg>s</w>, <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="5.5">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="5.6" punct="vg:10">c<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>rc<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
		<l n="6" num="1.6" lm="10"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="6.2">dr<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>ts</w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>g<seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>x</w> <w n="6.4">qu</w>'<w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.6">d<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg></w> <w n="6.7" punct="pv:10">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="10" punct="pv">en</seg></w> ;</l>
		<l n="7" num="1.7" lm="10"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">d</w>'<w n="7.3"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="7.4">mi<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="7.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="7.7" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">em</seg>pl<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="vg">oi</seg></w>, <w n="7.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="7.9" punct="vg:10">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
		<l n="8" num="1.8" lm="10"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w> <w n="8.2">pl<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s</w> <w n="8.3">f<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="8.5">d<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="8.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="8.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="8.8" punct="pt:10">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="10" punct="pt">en</seg></w>.</l>
	</lg>
</div></body></text></TEI>