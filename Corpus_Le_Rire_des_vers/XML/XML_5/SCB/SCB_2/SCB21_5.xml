<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <fileDesc>
   <titleStmt>
	<title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
	<title type="corpus">Le Rire des vers</title>
	<author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
	</author>
	<author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
	</author>
	<editor>Le Rire des vers, Université de Bâle</editor>
	<editor>
	 Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
	 <choice>
	  <abbr>CRISCO, Université de Caen Normandie</abbr>
	  <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	 </choice>
	 (EA 4255)
	</editor>
	<respStmt>
	 <resp>Encodage en XML (CRISCO, université de Caen)</resp>
	 <name id="KL">
	  <forename>Kedi</forename>
	  <surname>LI</surname>
	 </name>
	</respStmt>
	<respStmt>
	 <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
	 <name id="RR">
	  <forename>Richard</forename>
	  <surname>RENAULT</surname>
	 </name>
	</respStmt>
   </titleStmt>
   <extent>140 vers</extent>
   <publicationStmt>
	<publisher>
	 <orgname>
	  <choice>
	   <abbr>CRISCO, Université de Caen Normandie</abbr>
	   <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	  </choice>
	 </orgname>
	 <address>
	  <addrLine>Université de Caen</addrLine>
	  <addrLine>14032 CAEN CEDEX</addrLine>
	  <addrLine>FRANCE</addrLine>
	 </address>
	 <email>crisco.incipit@unicaen.fr</email>
	 <ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
	</publisher>
	<pubPlace>Caen</pubPlace>
	<date when="2022">2022</date>
	<idno type="local">SCB_2</idno>	
	<availability status="free">
		<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
		<p>
			Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
			CC = Licence Creative Commons
			BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
			sur les sources et sur les modifications introduites.
			NC = Pas d’utilisation commerciale.
			SA = Partage dans les mêmes conditions.
		</p>
	</availability>
   </publicationStmt>
   <sourceDesc>
	<biblFull>
	 <titleStmt>
	  <title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	  <author>SCRIBE ET BAYARD</author>
	 </titleStmt>
	 <publicationStmt>
	  <publisher>INTERNET ARCHIVE</publisher>
	  <idno type="URL">https://archive.org/details/camillaoulasoeur00scriuoft</idno>
	 </publicationStmt>
	 <sourceDesc>
	  <biblStruct>
	   <monogr>
		<repository>University of Toronto Libraries</repository>
		<idno type="URL">https://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991106543695706196</idno>
	   </monogr>
	  </biblStruct>                 
	 </sourceDesc>
	</biblFull> 
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <creation>
	<date when="1832">12 DÉCEMBRE 1832</date>
	<placeName>
	 <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
	</placeName>
   </creation>
  </profileDesc>
  <encodingDesc>
   <projectDesc>
	<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
   </projectDesc>
   <samplingDecl>
	<p>Les parties versifiées ont été prioritairement encodées.</p>
   </samplingDecl>
   <editorialDecl>
	<normalization>
	 <p>Les majuscules accentuées ont été restituées.</p>
	 <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
	 <p>La ponctuation a été normalisée.</p> 
	 <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
	 <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
	 <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
	</normalization>
   </editorialDecl>
  </encodingDesc>
 </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB21" modus="cp" lm_max="10">
	<head type="tune">AIR : Amis, voici la riante semaine.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="10"><w n="1.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="1.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>l</w> <w n="1.3" punct="vg:4">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>r</w>, <w n="1.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.5">d<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6" punct="pe:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe">e</seg></w> !</l>
		<l n="2" num="1.2" lm="10"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="3">a</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg></w> <w n="2.5" punct="pe:7">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="6">en</seg>t<seg phoneme="y" type="vs" value="1" rule="449" place="7" punct="pe ps">u</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! … <w n="2.6" punct="vg:8">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="8" punct="vg">i</seg></w>, <w n="2.7">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="9">oi</seg>s</w>-<w n="2.8" punct="vg:10">m<seg phoneme="wa" type="vs" value="1" rule="422" place="10" punct="vg">oi</seg></w>,</l>
		<l n="3" num="1.3" lm="10"><w n="3.1">R<seg phoneme="ɔ" type="vs" value="1" rule="442" place="1">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3" punct="vg:4">b<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg>l</w>, <w n="3.4" punct="vg:6">ch<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314" place="6" punct="vg">eau</seg>x</w>, <w n="3.5">m<seg phoneme="ɔ" type="vs" value="1" rule="442" place="7">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="3.7" punct="vg:10">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
		<l n="4" num="1.4" lm="10"><w n="4.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="1">en</seg></w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">tr<seg phoneme="o" type="vs" value="1" rule="432" place="3">o</seg>p</w> <w n="4.4" punct="vg:4">ch<seg phoneme="ɛ" type="vs" value="1" rule="63" place="4" punct="vg">e</seg>r</w>, <w n="4.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="5">en</seg></w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.7">tr<seg phoneme="o" type="vs" value="1" rule="432" place="7">o</seg>p</w> <w n="4.8">b<seg phoneme="o" type="vs" value="1" rule="314" place="8">eau</seg></w> <w n="4.9">p<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>r</w> <w n="4.10" punct="pe:10">t<seg phoneme="wa" type="vs" value="1" rule="422" place="10" punct="pe">oi</seg></w> !</l>
		<l n="5" num="1.5" lm="10"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="5.3" punct="pv:4">g<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="pv">er</seg></w> ; <w n="5.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5" punct="vg:8">g<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>gn<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8" punct="vg">ai</seg></w>, <w n="5.6">j</w>'<w n="5.7" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="9">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
		<l n="6" num="1.6" lm="10"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="6.2">c</w>'<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="6.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="6.5" punct="vg:4">t<seg phoneme="wa" type="vs" value="1" rule="422" place="4" punct="vg">oi</seg></w>, <w n="6.6">t<seg phoneme="wa" type="vs" value="1" rule="422" place="5">oi</seg></w> <w n="6.7" punct="vg:7">s<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="vg">e</seg></w>, <w n="6.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="6.9">j</w>'<w n="6.10"><seg phoneme="i" type="vs" value="1" rule="496" place="9">y</seg></w> <w n="6.11" punct="vg:10">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="10" punct="vg">en</seg></w>,</l>
		<l n="7" num="1.7" lm="10"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="7.3" punct="vg:4">b<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="4" punct="vg">eu</seg>r</w>, <w n="7.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.6" punct="vg:8">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8" punct="vg">ai</seg></w>, <w n="7.7">m<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg></w> <w n="7.8" punct="vg:10">ch<seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
		<l n="8" num="1.8" lm="8"><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w>-<w n="8.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.5">s<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>r</w> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.7" punct="pe:8">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="8" punct="pe">en</seg></w> !</l>
	</lg>
</div></body></text></TEI>