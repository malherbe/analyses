<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <fileDesc>
   <titleStmt>
	<title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
	<title type="corpus">Le Rire des vers</title>
	<author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
	</author>
	<author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
	</author>
	<editor>Le Rire des vers, Université de Bâle</editor>
	<editor>
	 Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
	 <choice>
	  <abbr>CRISCO, Université de Caen Normandie</abbr>
	  <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	 </choice>
	 (EA 4255)
	</editor>
	<respStmt>
	 <resp>Encodage en XML (CRISCO, université de Caen)</resp>
	 <name id="KL">
	  <forename>Kedi</forename>
	  <surname>LI</surname>
	 </name>
	</respStmt>
	<respStmt>
	 <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
	 <name id="RR">
	  <forename>Richard</forename>
	  <surname>RENAULT</surname>
	 </name>
	</respStmt>
   </titleStmt>
   <extent>140 vers</extent>
   <publicationStmt>
	<publisher>
	 <orgname>
	  <choice>
	   <abbr>CRISCO, Université de Caen Normandie</abbr>
	   <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	  </choice>
	 </orgname>
	 <address>
	  <addrLine>Université de Caen</addrLine>
	  <addrLine>14032 CAEN CEDEX</addrLine>
	  <addrLine>FRANCE</addrLine>
	 </address>
	 <email>crisco.incipit@unicaen.fr</email>
	 <ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
	</publisher>
	<pubPlace>Caen</pubPlace>
	<date when="2022">2022</date>
	<idno type="local">SCB_2</idno>	
	<availability status="free">
		<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
		<p>
			Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
			CC = Licence Creative Commons
			BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
			sur les sources et sur les modifications introduites.
			NC = Pas d’utilisation commerciale.
			SA = Partage dans les mêmes conditions.
		</p>
	</availability>
   </publicationStmt>
   <sourceDesc>
	<biblFull>
	 <titleStmt>
	  <title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	  <author>SCRIBE ET BAYARD</author>
	 </titleStmt>
	 <publicationStmt>
	  <publisher>INTERNET ARCHIVE</publisher>
	  <idno type="URL">https://archive.org/details/camillaoulasoeur00scriuoft</idno>
	 </publicationStmt>
	 <sourceDesc>
	  <biblStruct>
	   <monogr>
		<repository>University of Toronto Libraries</repository>
		<idno type="URL">https://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991106543695706196</idno>
	   </monogr>
	  </biblStruct>                 
	 </sourceDesc>
	</biblFull> 
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <creation>
	<date when="1832">12 DÉCEMBRE 1832</date>
	<placeName>
	 <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
	</placeName>
   </creation>
  </profileDesc>
  <encodingDesc>
   <projectDesc>
	<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
   </projectDesc>
   <samplingDecl>
	<p>Les parties versifiées ont été prioritairement encodées.</p>
   </samplingDecl>
   <editorialDecl>
	<normalization>
	 <p>Les majuscules accentuées ont été restituées.</p>
	 <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
	 <p>La ponctuation a été normalisée.</p> 
	 <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
	 <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
	 <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
	</normalization>
   </editorialDecl>
  </encodingDesc>
 </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB23" modus="cm" lm_max="10">
	<head type="tune">AIR : Elle a trahi ses sermens et sa foi.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="10"><w n="1.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="1.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>t</w> <w n="1.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="3">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="1.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="1.5" punct="pi:7">tr<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="346" place="7" punct="pi ps">er</seg></w> ? … <w n="1.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="8">e</seg>l</w> <w n="1.7" punct="pi:10">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="189" place="10" punct="pi">e</seg>t</w> ?</l>
		<l n="2" num="1.2" lm="10"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>xpl<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>qu<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w>-<w n="2.2" punct="ps:4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="ps">ou</seg>s</w>… <w n="2.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.4">pu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>s</w>-<w n="2.5">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="7">e</seg></w> <w n="2.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="2.7" punct="pi:10">c<seg phoneme="o" type="vs" value="1" rule="434" place="9">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi">e</seg></w> ?</l>
	</lg>
	<lg n="2">
	<head type="speaker">CAMILLA.</head> 
		<l n="3" num="2.1" lm="10"><w n="3.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="3.2">c</w>'<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="3.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="3.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="3.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="3.7"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="3.8" punct="pt:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8">in</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10" punct="pt">ê</seg>t</w>.</l>
		<l part="I" n="4" num="2.2" lm="10"><w n="4.1">N</w>'<w n="4.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">in</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>st<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="4.3" punct="pt:4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="pt">a</seg>s</w>. </l>
	</lg>
	<lg n="3">
	<head type="speaker">EDGARD.</head>
		<l part="F" n="4" lm="10"><w n="4.4">J</w>'<w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg></w> <w n="4.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.8">dr<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>t</w> <w n="4.9">p<seg phoneme="ø" type="vs" value="1" rule="397" place="9">eu</seg>t</w>-<w n="4.10" punct="pt:10"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="10">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt">e</seg></w>.</l>
		<l n="5" num="3.1" lm="10"><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w>-<w n="5.2">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="5.4" punct="pi:4">b<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="4" punct="pi ps">eu</seg>r</w> ? … <w n="5.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.6">p<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</w> <w n="5.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.8" punct="ps:10">p<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rt<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="ps">er</seg></w>…</l>
		<l n="6" num="3.2" lm="10"><w n="6.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w>-<w n="6.2">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="6.4" punct="pi:4">ch<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" punct="pi ps">in</seg></w> ? … <w n="6.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.6">v<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</w> <w n="6.7">s<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>l</w> <w n="6.8">m</w>'<w n="6.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="8">en</seg></w> <w n="6.10" punct="pe:10">ch<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pe">er</seg></w> !</l>
		<l n="7" num="3.3" lm="10"><w n="7.1">V<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2" punct="vg:4">b<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="4" punct="vg">eu</seg>r</w>, <w n="7.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.4">p<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.6" punct="pv:10">p<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rt<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pv">er</seg></w> ;</l>
		<l n="8" num="3.4" lm="10"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="8.2">v<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>s</w> <w n="8.3" punct="vg:4">ch<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" punct="vg">in</seg>s</w>, <w n="8.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">v<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</w> <w n="8.6">s<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>l</w> <w n="8.7">m</w>'<w n="8.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="8">en</seg></w> <w n="8.9" punct="pt:10">ch<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pt">er</seg></w>.</l>
	</lg>
</div></body></text></TEI>