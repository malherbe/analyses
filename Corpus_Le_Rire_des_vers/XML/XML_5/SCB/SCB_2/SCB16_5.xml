<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <fileDesc>
   <titleStmt>
	<title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
	<title type="corpus">Le Rire des vers</title>
	<author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
	</author>
	<author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
	</author>
	<editor>Le Rire des vers, Université de Bâle</editor>
	<editor>
	 Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
	 <choice>
	  <abbr>CRISCO, Université de Caen Normandie</abbr>
	  <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	 </choice>
	 (EA 4255)
	</editor>
	<respStmt>
	 <resp>Encodage en XML (CRISCO, université de Caen)</resp>
	 <name id="KL">
	  <forename>Kedi</forename>
	  <surname>LI</surname>
	 </name>
	</respStmt>
	<respStmt>
	 <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
	 <name id="RR">
	  <forename>Richard</forename>
	  <surname>RENAULT</surname>
	 </name>
	</respStmt>
   </titleStmt>
   <extent>140 vers</extent>
   <publicationStmt>
	<publisher>
	 <orgname>
	  <choice>
	   <abbr>CRISCO, Université de Caen Normandie</abbr>
	   <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	  </choice>
	 </orgname>
	 <address>
	  <addrLine>Université de Caen</addrLine>
	  <addrLine>14032 CAEN CEDEX</addrLine>
	  <addrLine>FRANCE</addrLine>
	 </address>
	 <email>crisco.incipit@unicaen.fr</email>
	 <ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
	</publisher>
	<pubPlace>Caen</pubPlace>
	<date when="2022">2022</date>
	<idno type="local">SCB_2</idno>	
	<availability status="free">
		<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
		<p>
			Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
			CC = Licence Creative Commons
			BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
			sur les sources et sur les modifications introduites.
			NC = Pas d’utilisation commerciale.
			SA = Partage dans les mêmes conditions.
		</p>
	</availability>
   </publicationStmt>
   <sourceDesc>
	<biblFull>
	 <titleStmt>
	  <title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	  <author>SCRIBE ET BAYARD</author>
	 </titleStmt>
	 <publicationStmt>
	  <publisher>INTERNET ARCHIVE</publisher>
	  <idno type="URL">https://archive.org/details/camillaoulasoeur00scriuoft</idno>
	 </publicationStmt>
	 <sourceDesc>
	  <biblStruct>
	   <monogr>
		<repository>University of Toronto Libraries</repository>
		<idno type="URL">https://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991106543695706196</idno>
	   </monogr>
	  </biblStruct>                 
	 </sourceDesc>
	</biblFull> 
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <creation>
	<date when="1832">12 DÉCEMBRE 1832</date>
	<placeName>
	 <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
	</placeName>
   </creation>
  </profileDesc>
  <encodingDesc>
   <projectDesc>
	<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
   </projectDesc>
   <samplingDecl>
	<p>Les parties versifiées ont été prioritairement encodées.</p>
   </samplingDecl>
   <editorialDecl>
	<normalization>
	 <p>Les majuscules accentuées ont été restituées.</p>
	 <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
	 <p>La ponctuation a été normalisée.</p> 
	 <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
	 <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
	 <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
	</normalization>
   </editorialDecl>
  </encodingDesc>
 </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB16" modus="cp" lm_max="10">
	<head type="tune">Air : Il n'est pas tems de nous quitter.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8"><w n="1.1">V<seg phoneme="wa" type="vs" value="1" rule="439" place="1">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="1.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="3">e</seg>l</w> <w n="1.3"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="343" place="5">ue</seg>il</w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.6" punct="pv:8">si<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="8" punct="pv">en</seg></w> ;</l>
		<l n="2" num="1.2" lm="8"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="2.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>t</w> <w n="2.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>c</w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="2.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.6">s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="2.7" punct="pi:8">fi<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
		<l n="3" num="1.3" lm="8"><w n="3.1">S<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="3.2" punct="vg:2">d<seg phoneme="o" type="vs" value="1" rule="437" place="2" punct="vg">o</seg>t</w>, <w n="3.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="3.4" punct="pi:5">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="pi ps">e</seg>s</w> ? … <w n="3.5">j</w>'<w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="3.7" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="8" punct="vg">en</seg></w>,</l>
		<l n="4" num="1.4" lm="8"><w n="4.1">C</w>'<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="4.3">b<seg phoneme="o" type="vs" value="1" rule="314" place="2">eau</seg></w> <w n="4.4">d</w>'<w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.6">r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="4.7" punct="pt:8">h<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
		<l n="5" num="1.5" lm="10"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="5.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>t</w> <w n="5.3">n</w>'<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>r</w> <w n="5.5">n<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg></w> <w n="5.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg></w> <w n="5.7">n<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg></w> <w n="5.8" punct="vg:10">t<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="10" punct="vg">en</seg>t</w>,</l>
		<l n="6" num="1.6" lm="8"><w n="6.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">l</w>'<w n="6.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="6.7" punct="pt:8">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="452" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
	</lg> 
	<lg n="2">
	<head type="speaker">PRETTY.</head> 
		<l n="7" num="2.1" lm="8"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rs</w> <w n="7.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="7.3" punct="vg:4">d<seg phoneme="wa" type="vs" value="1" rule="419" place="4" punct="vg">oi</seg>t</w>, <w n="7.4">c</w>'<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="7.6">pl<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>s</w> <w n="7.7" punct="vg:8">pr<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="8" punct="vg">en</seg>t</w>,</l>
		<l n="8" num="2.2" lm="8"><w n="8.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="8.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="381" place="3">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="8.3">d</w>'<w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>r</w> <w n="8.6" punct="pt:8"><seg phoneme="y" type="vs" value="1" rule="452" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
	</lg>
</div></body></text></TEI>