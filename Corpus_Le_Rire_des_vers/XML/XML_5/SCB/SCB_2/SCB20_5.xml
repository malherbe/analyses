<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <fileDesc>
   <titleStmt>
	<title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
	<title type="corpus">Le Rire des vers</title>
	<author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
	</author>
	<author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
	</author>
	<editor>Le Rire des vers, Université de Bâle</editor>
	<editor>
	 Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
	 <choice>
	  <abbr>CRISCO, Université de Caen Normandie</abbr>
	  <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	 </choice>
	 (EA 4255)
	</editor>
	<respStmt>
	 <resp>Encodage en XML (CRISCO, université de Caen)</resp>
	 <name id="KL">
	  <forename>Kedi</forename>
	  <surname>LI</surname>
	 </name>
	</respStmt>
	<respStmt>
	 <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
	 <name id="RR">
	  <forename>Richard</forename>
	  <surname>RENAULT</surname>
	 </name>
	</respStmt>
   </titleStmt>
   <extent>140 vers</extent>
   <publicationStmt>
	<publisher>
	 <orgname>
	  <choice>
	   <abbr>CRISCO, Université de Caen Normandie</abbr>
	   <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	  </choice>
	 </orgname>
	 <address>
	  <addrLine>Université de Caen</addrLine>
	  <addrLine>14032 CAEN CEDEX</addrLine>
	  <addrLine>FRANCE</addrLine>
	 </address>
	 <email>crisco.incipit@unicaen.fr</email>
	 <ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
	</publisher>
	<pubPlace>Caen</pubPlace>
	<date when="2022">2022</date>
	<idno type="local">SCB_2</idno>	
	<availability status="free">
		<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
		<p>
			Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
			CC = Licence Creative Commons
			BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
			sur les sources et sur les modifications introduites.
			NC = Pas d’utilisation commerciale.
			SA = Partage dans les mêmes conditions.
		</p>
	</availability>
   </publicationStmt>
   <sourceDesc>
	<biblFull>
	 <titleStmt>
	  <title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	  <author>SCRIBE ET BAYARD</author>
	 </titleStmt>
	 <publicationStmt>
	  <publisher>INTERNET ARCHIVE</publisher>
	  <idno type="URL">https://archive.org/details/camillaoulasoeur00scriuoft</idno>
	 </publicationStmt>
	 <sourceDesc>
	  <biblStruct>
	   <monogr>
		<repository>University of Toronto Libraries</repository>
		<idno type="URL">https://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991106543695706196</idno>
	   </monogr>
	  </biblStruct>                 
	 </sourceDesc>
	</biblFull> 
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <creation>
	<date when="1832">12 DÉCEMBRE 1832</date>
	<placeName>
	 <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
	</placeName>
   </creation>
  </profileDesc>
  <encodingDesc>
   <projectDesc>
	<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
   </projectDesc>
   <samplingDecl>
	<p>Les parties versifiées ont été prioritairement encodées.</p>
   </samplingDecl>
   <editorialDecl>
	<normalization>
	 <p>Les majuscules accentuées ont été restituées.</p>
	 <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
	 <p>La ponctuation a été normalisée.</p> 
	 <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
	 <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
	 <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
	</normalization>
   </editorialDecl>
  </encodingDesc>
 </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB20" modus="sm" lm_max="8">
	<head type="tune">AIR du Verre.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8"><w n="1.1" punct="vg:1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="vg">a</seg>r</w>, <w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4" punct="vg:4">s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="vg">ai</seg>s</w>, <w n="1.5">j</w>'<w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="5">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.7">Pr<seg phoneme="e" type="vs" value="1" rule="352" place="7">e</seg>tt<seg phoneme="i" type="vs" value="1" rule="492" place="8">y</seg></w></l>
		<l n="2" num="1.2" lm="8"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4">pu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg>s</w> <w n="2.5">v<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</w> <w n="2.7" punct="pe:8"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
		<l n="3" num="1.3" lm="8"><w n="3.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="3.4" punct="vg:4">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4" punct="vg">e</seg>rds</w>, <w n="3.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="3.6">s<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>l</w> <w n="3.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg></w></l>
		<l n="4" num="1.4" lm="8"><w n="4.1">C</w>'<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.5">br<seg phoneme="y" type="vs" value="1" rule="444" place="4">û</seg>l<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="4.7" punct="pe:8">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
	</lg>
	<lg n="2">
	<head type="speaker">CAMILLA.</head>
		<l part="I" n="5" num="2.1" lm="8"><w n="5.1">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="5.2" punct="pe:2">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="2" punct="pe">eu</seg></w> ! </l> 
	</lg>
	<lg n="3">
	<head type="speaker">LIONEL.</head>
		<l part="F" n="5" lm="8"><w n="5.3">P<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="5.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>r</w> <w n="5.5">d</w>'<w n="5.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">em</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg>s</w>,</l>
		<l n="6" num="3.1" lm="8"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">m<seg phoneme="wa" type="vs" value="1" rule="439" place="2">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="362" place="3">en</seg></w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="6.4">s<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="6">en</seg>t</w> <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.6" punct="ps:8">n<seg phoneme="o" type="vs" value="1" rule="414" place="8">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
		<l n="7" num="3.2" lm="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3" punct="vg:4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="vg">ai</seg>s</w>, <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381" place="7">e</seg>il</w> <w n="7.6" punct="vg:8">c<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg>s</w>,</l>
		<l n="8" num="3.3" lm="8"><w n="8.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="1">en</seg></w> <w n="8.2">s<seg phoneme="y" type="vs" value="1" rule="444" place="2">û</seg>r</w> <w n="8.3">d</w>'<w n="8.4"><seg phoneme="i" type="vs" value="1" rule="496" place="3">y</seg></w> <w n="8.5">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.6">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="6">oin</seg>s</w> <w n="8.7">qu</w>'<w n="8.8"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="8.9" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="317" place="8">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
	</lg>
</div></body></text></TEI>