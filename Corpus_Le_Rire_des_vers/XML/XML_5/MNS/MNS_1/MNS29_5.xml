<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS29" modus="sp" lm_max="7">
	<head type="tune">AIR : Filles de nos montagnes. (de l'Illusion)</head>
	<lg n="1">
		<l n="1" num="1.1" lm="6"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="1.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="1.5">c<seg phoneme="ɛ" type="vs" value="1" rule="351" place="6">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
		<l n="2" num="1.2" lm="7"><w n="2.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="2">ai</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="5">e</seg>c</w> <w n="2.4"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="351" place="7">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w> <del reason="analysis" type="repetition" hand="KL">(bis.)</del></l>
		<l n="3" num="1.3" lm="3"><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="434" place="2">o</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w></l>
		<l n="4" num="1.4" lm="3"><w n="4.1">N<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="4.2" punct="vg:3">h<seg phoneme="o" type="vs" value="1" rule="434" place="2">o</seg>mm<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
		<l n="5" num="1.5" lm="3"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="5.2">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w></l>
		<l n="6" num="1.6" lm="3"><w n="6.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="6.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">g<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></w></l>
		<l n="7" num="1.7" lm="6"><w n="7.1">Lu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="7.2">pr<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="7.3">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4" punct="ps:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="ps">ou</seg>r</w>…</l>
		<l n="8" num="1.8" lm="6"><w n="8.1">P<seg phoneme="ɛ" type="vs" value="1" rule="338" place="1">a</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="8.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4" punct="pt:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="pt">ou</seg>r</w>.</l>
		<l ana="unanalyzable" n="9" num="1.9">A celle que sans cesse, etc.</l>
	</lg>
</div></body></text></TEI>