<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS23" modus="sm" lm_max="8">
	<head type="tune">Air du vaudeville de l'Apothicaire.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8"><w n="1.1">J<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="1.2">f<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>x</w> <w n="1.4">y<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</w> <w n="1.5">s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="1.6" punct="vg:8">d<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="vg">ou</seg>x</w>, </l>
		<l n="2" num="1.2" lm="8"><w n="2.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="352" place="4">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="422" place="5">oi</seg></w> <w n="2.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.5" punct="pv:8">r<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ; </l>
		<l n="3" num="1.3" lm="8"><w n="3.1">D</w>'<w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="3.3">vi<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="3.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>t</w> <w n="3.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.6">cr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w>-<w n="3.7" punct="pi:8">v<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pi">ou</seg>s</w> ? </l>
		<l n="4" num="1.4" lm="8"><w n="4.1">Cr<seg phoneme="wa" type="vs" value="1" rule="439" place="1">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="4.2" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="3" punct="vg">oi</seg></w>, <w n="4.3">r<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="4.4">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ; </l>
		<l n="5" num="1.5" lm="8"><w n="5.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="5.2">s<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="5.3">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="5.4"><seg phoneme="y" type="vs" value="1" rule="452" place="4">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w>-<w n="5.5" punct="vg:8">c<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg></w>, </l>
		<l n="6" num="1.6" lm="8"><w n="6.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="6.3">c<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>rs</w> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="6.5" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="466" place="6">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>, </l>
		<l n="7" num="1.7" lm="8"><w n="7.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="2">en</seg>t</w> <w n="7.2"><seg phoneme="e" type="vs" value="1" rule="352" place="3">e</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="338" place="4">a</seg>y<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="7.3">l</w>'<w n="7.4" punct="ps:8"><seg phoneme="e" type="vs" value="1" rule="169" place="6">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="ps">i</seg></w>… </l>
		<l n="8" num="1.8" lm="8"><w n="8.1">J<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="8.2"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="8.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t</w> <w n="8.5">p<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>r</w> <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>x</w> <w n="8.7" punct="pt:8">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>. </l>
	</lg> 
</div></body></text></TEI>