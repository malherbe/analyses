<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC31" modus="sp" lm_max="7">
	<head type="main">CHOEUR DE JUGES.</head>
	<head type="tune">AIR : du château de mon oncle.</head>
	<div type="section" n="1">
		<lg n="1">
			<l n="1" num="1.1" lm="7"><w n="1.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="1.2">c</w>'<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="1.4"><seg phoneme="y" type="vs" value="1" rule="452" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.5" punct="pe:7">tr<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" punct="pe">on</seg></w> !</l>
			<l n="2" num="1.2" lm="7"><w n="2.1">C</w>'<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="2.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg></w> <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.7" punct="pe:7">d<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" punct="pe">on</seg></w> !</l>
			<l n="3" num="1.3" lm="6"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="3.2" punct="pe:3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="pe">ou</seg>rs</w> ! <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg></w> <w n="3.4" punct="pe:6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="pe">ou</seg>rs</w> !</l>
			<l n="4" num="1.4" lm="7"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="4.2">j<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="4.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="4.5" punct="pe:7">c<seg phoneme="u" type="vs" value="1" rule="424" place="7" punct="pe">ou</seg>rs</w> !</l>
			<l n="5" num="1.5" lm="7"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="5.3">b<seg phoneme="o" type="vs" value="1" rule="314" place="3">eau</seg></w> <w n="5.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="5.5" punct="vg:7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">in</seg>s<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>lt<seg phoneme="e" type="vs" value="1" rule="346" place="7" punct="vg">er</seg></w>,</l>
			<l n="6" num="1.6" lm="7"><w n="6.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="6.2" punct="vg:3">v<seg phoneme="ɛ" type="vs" value="1" rule="354" place="2">e</seg>x<seg phoneme="e" type="vs" value="1" rule="346" place="3" punct="vg">er</seg></w>, <w n="6.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="6.4" punct="vg:7">m<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>st<seg phoneme="e" type="vs" value="1" rule="346" place="7" punct="vg">er</seg></w>,</l>
			<l n="7" num="1.7" lm="6"><w n="7.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="7.2">s<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="7.3" punct="vg:6">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rs<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>st<seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="vg">er</seg></w>,</l>
			<l n="8" num="1.8" lm="3"><w n="8.1">R<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>st<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w></l>
			<l n="9" num="1.9" lm="4"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="9.2" punct="pe:4">pr<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>st<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="pe">er</seg></w> !</l>
		</lg>
	</div>
	<div type="section" n="2">
		<head type="main">SCENE IX.</head>
		<head type="sub">LES MÊMES, BARBAROK, GARDES.</head>
		<lg n="1">
		<head type="speaker">BARBAROK.</head>
			<l n="10" num="1.1" lm="5"><w n="10.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="1">e</seg>l</w> <w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="10.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4" punct="pi:5">t<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pi">e</seg></w> ?</l>
		</lg>
		<lg n="2">
		<head type="speaker">LES JUGES.</head>
			<l n="11" num="2.1" lm="5"><w n="11.1" punct="vg:1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1" punct="vg">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="11.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="11.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="11.4" punct="pe:5"><seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>tr<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></w> !</l>
		</lg>
		<lg n="3">
		<head type="speaker">BARBAROK</head>
			<l n="12" num="3.1" lm="6"><w n="12.1">D<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w>-<w n="12.2" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="3" punct="vg">oi</seg></w>, <w n="12.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="12.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w></l>
			<l n="13" num="3.2" lm="7"><w n="13.1">J<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>ch<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>s</w> <w n="13.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="13.3"><seg phoneme="u" type="vs" value="1" rule="425" place="4">où</seg></w> <w n="13.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="13.5" punct="pi:7">v<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="7" punct="pi">à</seg></w> ? </l>
			<l n="14" num="3.3" lm="5"><w n="14.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="14.2">h<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3" punct="ps:5">j<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="ps">e</seg></w>… </l>
		</lg> 
		<lg n="4">
		<head type="speaker">LES JUGES.</head>
			<l n="15" num="4.1" lm="5"><w n="15.1">C</w>'<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="15.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="15.4" punct="pe:5">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></w> !</l>
		</lg>
		<lg n="5">
		<head type="speaker">BARBAROK.</head>
			<l n="16" num="5.1" lm="6"><w n="16.1">C</w>'<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="16.3">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="2">a</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="16.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="16.5">p<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg></w> <w n="16.6" punct="vg:6">ch<seg phoneme="ɛ" type="vs" value="1" rule="63" place="6" punct="vg">e</seg>r</w>,</l>
			<l n="17" num="5.2" lm="7"><w n="17.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="17.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="17.3">j<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="5">en</seg>t</w> <w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="17.5">l</w>'<w n="17.6" punct="pt:7"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="7" punct="pt">ai</seg>r</w>.</l>
		</lg>
	</div>
</div></body></text></TEI>