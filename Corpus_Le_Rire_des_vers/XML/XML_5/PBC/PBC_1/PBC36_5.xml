<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC36" modus="sm" lm_max="8">
	<head type="tune">AIR : du Marché de la Muette.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8"><w n="1.1" punct="vg:2"><seg phoneme="u" type="vs" value="1" rule="424" place="1">Ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="vg">ez</seg></w>, <w n="1.2"><seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w>-<w n="1.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="1.5">l</w>'<w n="1.6" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="vg">an</seg>t</w>,</l>
		<l n="2" num="1.2" lm="8"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="2.5" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8" punct="pv">en</seg>d</w> ;</l>
		<l n="3" num="1.3" lm="8"><w n="3.1" punct="vg:2"><seg phoneme="u" type="vs" value="1" rule="424" place="1">Ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="vg">ez</seg></w>, <w n="3.2"><seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w>-<w n="3.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="3.5">l</w>'<w n="3.6" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="vg">an</seg>t</w>,</l>
		<l n="4" num="1.4" lm="8"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="4.3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>r</w> <w n="4.4"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l</w> <w n="4.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="4.6" punct="pt:*">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>t<seg phoneme="?" type="va" value="1" rule="161" place="8" punct="pt">en</seg>t</w>.</l>
		<l ana="unanalyzable" n="5" num="1.5">Ouvrez, etc.</l>
	</lg>
</div></body></text></TEI>