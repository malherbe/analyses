<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">27, 28 ET 29 JUILLET, TABLEAU ÉPISODIQUE DES TROIS JOURNÉES.</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ARA" sort="1">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<author key="DUV" sort="2">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>495 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
						<author>ARAGO ET DUVERT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=xjVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
								<author>ARAGO ET DUVERT</author>
								<repository>Österreichische Nationalbibliothek</repository>
								<idno type="URI">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886206</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : U+02BC MODIFIER LETTER APOSTROPHE) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<change when="2021-06-14" who="RR">Quelques corrections concernant la distribution du signe signe ʼ (UNICODE : ʼ).</change>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE JOURNÉE.</head><head type="main_subpart">SCÈNE XI.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="AED9" modus="cp" lm_max="14">
			<head type="tune">AIR : De l'orage, du Barbier.</head>
			<lg n="1">
				<head type="main">CHŒUR.</head>
				<l n="1" num="1.1" lm="14"><w n="1.1" punct="pe:1">Ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="1" punct="pe">e</seg>l</w> ! <w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w>-<w n="1.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <subst hand="LG" reason="analysis" type="repetition"><del>(bis)</del><add rend="hidden"><w n="1.4" punct="pe:6">Ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6" punct="pe">e</seg>l</w> ! <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="9">ez</seg></w>-<w n="1.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>s</w></add></subst> <w n="1.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11">e</seg>s</w> <w n="1.8" punct="pi:14">f<seg phoneme="y" type="vs" value="1" rule="449" place="12">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="13">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="339" place="14">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="15" punct="pi">e</seg>s</w> ?</l>
				<l n="2" num="1.2" lm="14"><w n="2.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="2.2">c<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="2.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <subst hand="LG" reason="analysis" type="repetition"><del>(bis)</del><add rend="hidden"><w n="2.4" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="pe">A</seg>h</w> ! <w n="2.5">c<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>s</w> <w n="2.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9">en</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="10">er</seg></w></add></subst> <w n="2.7">n<seg phoneme="o" type="vs" value="1" rule="437" place="11">o</seg>s</w> <w n="2.8" punct="pt:14">c<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>m<seg phoneme="a" type="vs" value="1" rule="339" place="13">a</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="14">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="15" punct="pt">e</seg>s</w>.</l>
				<l n="3" num="1.3" lm="12"><w n="3.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg>s</w>, <w n="3.2">c<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <subst hand="LG" reason="analysis" type="repetition"><del>(bis)</del><add rend="hidden"><w n="3.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg">i</seg>s</w>, <w n="3.4">c<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>s</w></add></subst> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="317" place="9">au</seg>x</w> <w n="3.6" punct="pe:12">b<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>c<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg>s</w> !</l>
				<l n="4" num="1.4" lm="4"><space unit="char" quantity="20"></space><w n="4.1">Qu</w>'<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>ls</w> <w n="4.3">s<seg phoneme="wa" type="vs" value="1" rule="422" place="2">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.4">p<seg phoneme="y" type="vs" value="1" rule="452" place="3">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>s</w></l>
				<l n="5" num="1.5" lm="4"><space unit="char" quantity="20"></space><w n="5.1">N<seg phoneme="o" type="vs" value="1" rule="437" place="1">o</seg>s</w> <w n="5.2" punct="pe:4"><seg phoneme="e" type="vs" value="1" rule="169" place="2">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="pe">i</seg>s</w> !</l>
			</lg>
		</div></body></text></TEI>