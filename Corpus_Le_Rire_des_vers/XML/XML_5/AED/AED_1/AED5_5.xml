<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">27, 28 ET 29 JUILLET, TABLEAU ÉPISODIQUE DES TROIS JOURNÉES.</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ARA" sort="1">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<author key="DUV" sort="2">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>495 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
						<author>ARAGO ET DUVERT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=xjVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
								<author>ARAGO ET DUVERT</author>
								<repository>Österreichische Nationalbibliothek</repository>
								<idno type="URI">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886206</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : U+02BC MODIFIER LETTER APOSTROPHE) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<change when="2021-06-14" who="RR">Quelques corrections concernant la distribution du signe signe ʼ (UNICODE : ʼ).</change>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE JOURNÉE.</head><head type="main_subpart">SCÈNE IV.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="AED5" modus="cm" lm_max="10">
			<head type="tune">Air : Au temps heureux de la chevalerie.</head>
			<lg n="1">
				<head type="main">CAFFARDIN.</head>
				<l n="1" num="1.1" lm="10"><w n="1.1" punct="pe:1">B<seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">a</seg>h</w> ! <w n="1.2">s<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="1.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4">p<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="1.6">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="1.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9">e</seg>s</w> <w n="1.8"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg>s</w></l>
				<l n="2" num="1.2" lm="10"><w n="2.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="2.3" punct="pe:4">lu<seg phoneme="i" type="vs" value="1" rule="490" place="4" punct="pe">i</seg></w> ! <w n="2.4">c<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r</w> <w n="2.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="2.6">l</w>'<w n="2.7" punct="dp:10"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>cc<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>ri<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" punct="dp">on</seg>s</w> :</l>
				<l n="3" num="1.3" lm="10"><w n="3.1">N</w>'<w n="3.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w>-<w n="3.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="3.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="3.6" punct="vg:7">Su<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" punct="vg">e</seg>s</w>, <w n="3.7">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8">e</seg>s</w> <w n="3.8" punct="vg:10">G<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9">en</seg>d<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>,</l>
				<l n="4" num="1.4" lm="10"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">qu<seg phoneme="wa" type="vs" value="1" rule="280" place="2">oi</seg></w> <w n="4.3">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">om</seg>br<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>x</w> <w n="4.6" punct="pi:10">b<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306" place="9">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" punct="pi">on</seg>s</w> ?</l>
				<l n="5" num="1.5" lm="10"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="5.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>s</w> <w n="5.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ts</w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>t</w> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="5.6">pl<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="5.7" punct="vg:10">n<seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>,</l>
				<l n="6" num="1.6" lm="10"><w n="6.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="6.3" punct="vg:4">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rri<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="vg">ez</seg></w>, <w n="6.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="6.5">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="6.6">h<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437" place="8">o</seg>s</w> <w n="6.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="9">an</seg>s</w> <w n="6.8" punct="vg:10">p<seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="vg">eu</seg>r</w>,</l>
				<l n="7" num="1.7" lm="10"><w n="7.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">l<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r</w> <w n="7.3">v<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>l</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>t</w> <w n="7.5">n<seg phoneme="o" type="vs" value="1" rule="437" place="7">o</seg>s</w> <w n="7.6" punct="vg:10">b<seg phoneme="a" type="vs" value="1" rule="342" place="8">a</seg>i<seg phoneme="o" type="vs" value="1" rule="443" place="9">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>,</l>
				<l n="8" num="1.8" lm="10"><w n="8.1">C<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>mmʼ</w> <w n="8.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="8.3">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rdr<seg phoneme="o" type="vs" value="1" rule="314" place="4">eau</seg>x</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="8.5">l</w>'<w n="8.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>ct</w> <w n="8.7">d<seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg></w> <w n="8.8" punct="pt:10">ch<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>ss<seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="pt">eu</seg>r</w>.</l>
			</lg>
			<lg n="2">
				<head type="main">RAIMOND.</head>
				<l n="9" num="2.1" lm="10"><w n="9.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w>-<w n="9.3" punct="pi:4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="pi">ou</seg>s</w> ? <w n="9.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="9.6">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="9.7" punct="pi:10">f<seg phoneme="o" type="vs" value="1" rule="443" place="9">o</seg>l<seg phoneme="i" type="vs" value="1" rule="481" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi">e</seg></w> ?</l>
				<l n="10" num="2.2" lm="10"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="10.2">t<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>l</w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>r</w> <w n="10.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="10.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="10.6" punct="pe:10"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pe">er</seg></w> !</l>
				<l n="11" num="2.3" lm="10"><w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="11.2">p<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="vg">er</seg></w>, <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>rs</w> <w n="11.5">qu</w>'<w n="11.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="11.7">l</w>'<w n="11.8" punct="vg:10">h<seg phoneme="y" type="vs" value="1" rule="452" place="8">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>l<seg phoneme="i" type="vs" value="1" rule="481" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
				<l n="12" num="2.4" lm="10"><w n="12.1">C</w>'<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg></w> <w n="12.4">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="12.5">qu</w>'<w n="12.6"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="12.7">f<seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>t</w> <w n="12.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.9" punct="ps:10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">om</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="ps">er</seg></w> …</l>
				<l n="13" num="2.5" lm="10"><w n="13.1">L<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>g</w>-<w n="13.2" punct="vg:2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2" punct="vg">em</seg>ps</w>, <w n="13.3">p<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>t</w>-<w n="13.4" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="4" punct="vg">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="13.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="13.6">s<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="13.7">l</w>'<w n="13.8" punct="pv:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9">in</seg>j<seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
				<l n="14" num="2.6" lm="10"><w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="14.2">qu</w>'<w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="14.4">s<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>l</w> <w n="14.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="14.6">vi<seg phoneme="ɛ" type="vs" value="1" rule="365" place="5">e</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ffl<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w> <w n="14.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg></w> <w n="14.9" punct="vg:10">c<seg phoneme="œ" type="vs" value="1" rule="248" place="10" punct="vg">œu</seg>r</w>,</l>
				<l n="15" num="2.7" lm="10"><w n="15.1">S</w>'<w n="15.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>l</w> <w n="15.3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>t</w> <w n="15.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>g</w> <w n="15.6">c<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="15.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.8">s<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w> <w n="15.9" punct="vg:10">bl<seg phoneme="e" type="vs" value="1" rule="352" place="9">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
				<l n="16" num="2.8" lm="10"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="16.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="16.5">d<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>v<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>rʼ</w> <w n="16.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="16.7" punct="pt:10">ch<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>ss<seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="pt">eu</seg>r</w>.</l>
			</lg>
		</div></body></text></TEI>