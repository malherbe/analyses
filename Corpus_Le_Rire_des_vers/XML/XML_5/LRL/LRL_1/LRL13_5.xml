<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL13" modus="sm" lm_max="8">
	<head type="tune">Même air.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8"><w n="1.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>vi<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="1.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>pr<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>du<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>t</w> <hi rend="ital"><w n="1.4" punct="vg:8">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="1.5">Gl<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w></hi>,</l>
		<l n="2" num="1.2" lm="8"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="2.3">tr<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></w> <w n="2.4">p<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg></w> <w n="2.5">d</w>'<w n="2.6" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pe ps">eu</seg>rs</w> ! …</l>
		<l n="3" num="1.3" lm="8"><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="3.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>pti<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="3.3">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rt</w> <w n="3.4">s<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>r</w> <hi rend="ital"><w n="3.5" punct="vg:8">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="3.6">V<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w></hi>,</l>
		<l n="4" num="1.4" lm="8"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="4.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="2">en</seg></w> <w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="4.4">g<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="4">en</seg>s</w> <w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="4.7">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t</w> <w n="4.8" punct="pe:8">p<seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pe ps">eu</seg>r</w> ! …</l>
		<l n="5" num="1.5" lm="8"><w n="5.1">R<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="5.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>g<seg phoneme="y" type="vs" value="1" rule="447" place="5">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.4">m<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
		<l n="6" num="1.6" lm="8"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <hi rend="ital"><w n="6.2" punct="ps:5">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="6.3">L<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="5" punct="ps">é</seg></w></hi>… <w n="6.4">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ls</w> <w n="6.5" punct="pe:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="189" place="8" punct="pe ps">e</seg>ts</w> ! …</l>
		<l n="7" num="1.7" lm="8"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg></w> <w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="7.6" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="8" num="1.8" lm="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="8.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="8.4">s<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="8.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>r</w> <w n="8.6">n<seg phoneme="o" type="vs" value="1" rule="437" place="7">o</seg>s</w> <w n="8.7" punct="pt:8">fr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pt">ai</seg>s</w>. <del reason="analysis" type="repetition" hand="KL">( bis.)</del></l>
	</lg>
</div></body></text></TEI>