<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL6" modus="cp" lm_max="10">
	<head type="tune">Air d'Aristippe.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="10"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="1.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="1.4"><seg phoneme="u" type="vs" value="1" rule="425" place="5">où</seg></w> <w n="1.5">l</w>'<w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></w> <w n="1.7">n<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>s</w> <w n="1.8" punct="vg:10">l<seg phoneme="i" type="vs" value="1" rule="481" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
		<l n="2" num="1.2" lm="8"><w n="2.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="2.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="63" place="2">e</seg>rs</w> <w n="2.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ls</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="2.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="408" place="5">É</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>m<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="vg">é</seg></w>,</l>
		<l n="3" num="1.3" lm="10"><w n="3.1">V<seg phoneme="ɛ" type="vs" value="1" rule="63" place="1">e</seg>rs</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></w> <w n="3.4">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="3.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="3.7" punct="vg:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
		<l n="4" num="1.4" lm="10"><w n="4.1">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="4.4">s</w>'<w n="4.5"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="4.6">r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>n<seg phoneme="i" type="vs" value="1" rule="466" place="9">i</seg>m<seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg></w></l>
		<l n="5" num="1.5" lm="10"><w n="5.1">D</w>'<w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="5.3">d<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>x</w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="267" place="4">um</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.6">j<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="351" place="7">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7" punct="dp:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">em</seg>b<seg phoneme="o" type="vs" value="1" rule="317" place="9">au</seg>m<seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="dp">é</seg></w> :</l>
		<l n="6" num="1.6" lm="10"><w n="6.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w>-<w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="4">e</seg></w> <w n="6.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.4">n<seg phoneme="ø" type="vs" value="1" rule="247" place="6">œu</seg>d</w> <w n="6.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg></w> <w n="6.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>s</w> <w n="6.7" punct="vg:10">r<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>, </l>
		<l n="7" num="1.7" lm="10"><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="7.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="7.4" punct="vg:4">d<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg">ou</seg>x</w>, <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>r</w> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.7">d<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>cl<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8">in</seg></w> <w n="7.8">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9">e</seg>s</w> <w n="7.9" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="10" punct="vg">an</seg>s</w>,</l>
		<l n="8" num="1.8" lm="8"><w n="8.1">D</w>'<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg></w> <w n="8.4">b<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>t</w> <w n="8.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
		<l n="9" num="1.9" lm="10"><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="9.3">s</w>'<w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="9.5">m<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>s</w> <w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="9.7">r<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.8"><seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg></w> <w n="9.9">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="9.10" punct="pt:10">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10" punct="pt">em</seg>ps</w>. <del reason="analysis" type="repetition" hand="KL">( bis.)</del></l>
	</lg>
</div></body></text></TEI>