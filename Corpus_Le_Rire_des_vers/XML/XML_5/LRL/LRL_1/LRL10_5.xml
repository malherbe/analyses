<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL10" modus="sm" lm_max="7">
	<head type="tune">AIR : Vos maris en Palestine,</head>
	<lg n="1">
		<l n="1" num="1.1" lm="7"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="1.2">m<seg phoneme="ɔ" type="vs" value="1" rule="442" place="2">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="1.3"><seg phoneme="i" type="vs" value="1" rule="496" place="4">y</seg></w> <w n="1.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="1.5" punct="vg:7">b<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>z<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</w>,</l>
		<l n="2" num="1.2" lm="7"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="2.2">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>ls</w> <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>rs</w> <w n="2.4" punct="vg:7"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="5">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7" punct="vg">an</seg>s</w>,</l>
		<l n="3" num="1.3" lm="7"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="3.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>ds</w> <w n="3.3">t<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="4">en</seg>s</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="3.5" punct="pv:7">r<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg>s</w> ;</l>
		<l n="4" num="1.4" lm="7"><w n="4.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2" punct="vg">in</seg></w>, <w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="4.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="4.4">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>st<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</w></l>
		<l n="5" num="1.5" lm="7"><w n="5.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>rs</w> <w n="5.3">b<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="5.5" punct="pt:7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">am</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7" punct="pt">an</seg>s</w>.</l>
		<l n="6" num="1.6" lm="7"><w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="6.3">pl<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="6.4">s</w>'<w n="6.5"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>bti<seg phoneme="ɛ" type="vs" value="1" rule="365" place="7">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>nt</w></l>
		<l n="7" num="1.7" lm="7"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="7.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="7.3">b<seg phoneme="o" type="vs" value="1" rule="314" place="4">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>p</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.5" punct="vg:7">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" punct="vg">on</seg>t</w>,</l>
		<l n="8" num="1.8" lm="7"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="8.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rn<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="5">e</seg>c</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>pl<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">om</seg>b</w></l>
		<l n="9" num="1.9" lm="7"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">v<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>s<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="9.4">c<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="9.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="9.6" punct="vg:7">vi<seg phoneme="ɛ" type="vs" value="1" rule="365" place="7">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>nt</w>,</l>
		<l n="10" num="1.10" lm="7"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">d<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>s</w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="10.4">c<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="10.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="10.6">s</w>'<w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="10.8" punct="pt:7">v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" punct="pt">on</seg>t</w>.</l>
	</lg>
</div></body></text></TEI>