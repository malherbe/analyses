<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL7" modus="sp" lm_max="8">
	<head type="tune">AIR de Madame Gibou (de Plantade).</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8"><w n="1.1" punct="pe:1">D<seg phoneme="i" type="vs" value="1" rule="467" place="1" punct="pe ps">i</seg>g</w> ! … <w n="1.2" punct="pe:2">d<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2" punct="pe ps">in</seg></w> ! … <w n="1.3" punct="pe:3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="pe ps">on</seg></w> ! … <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="1.5">cl<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.6">s</w>'<w n="1.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="2" num="1.2" lm="8"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">m<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2">e</seg>ts</w> <w n="2.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="2.4">r<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="2.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="2.7" punct="pv:8">b<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="189" place="8" punct="pv">e</seg>t</w> ;</l>
		<l n="3" num="1.3" lm="8"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="3.2">f<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="3.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">pr<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
		<l n="4" num="1.4" lm="8"><w n="4.1">C<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="4.3">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>cs</w> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg></w> <w n="4.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>d</w> <w n="4.6" punct="pe:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="189" place="8" punct="pe">e</seg>t</w> !</l>
		<l n="5" num="1.5" lm="8"><w n="5.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="5.2">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="5.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>pr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>s</w> <w n="5.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="5.6" punct="vg:8">pl<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="6" num="1.6" lm="8"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="6.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="6.5" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="pt">an</seg>s</w>.</l>
		<l n="7" num="1.7" lm="4"><w n="7.1">H<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="7.2" punct="pe:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4" punct="pe">an</seg>s</w> !</l>
		<l n="8" num="1.8" lm="4"><w n="8.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>ls</w> <w n="8.2">d<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>x</w> <w n="8.3" punct="pe:4">m<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="4" punct="pe">en</seg>s</w> !</l>
		
		<p>
		Silence donc ! … voyons les lecons… M. Bernard, récitez vos <lb></lb>
		racines grecques… ( imitant la voix d'un enfant.) <lb></lb>

		Onux, l'ongle te représente ; <lb></lb>
		  Onos, l'âne qui si bien chante ;<lb></lb>
		  Onos, l'âne ; onos…<lb></lb>
		</p>
		
		<stage>
		( avec sa voix naturelle.) L'âne, l'âne, c'est toi… et je me dis, <lb></lb>
		en riant sous cape : ( reprenant l'air.)
		</stage>
		
		<l n="9" num="1.9" lm="8"><w n="9.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="1">e</seg>l</w> <w n="9.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="9.6">cl<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
		<l n="10" num="1.10" lm="8"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381" place="5">e</seg>ils</w> <w n="10.4" punct="pe:8">g<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rn<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="8" punct="pe">en</seg>s</w> ! <del reason="analysis" type="repetition" hand="KL">(bis)</del></l>
	</lg>
	<lg n="2">
		<l n="11" num="2.1" lm="8"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3">Au</seg>g<seg phoneme="y" type="vs" value="1" rule="447" place="4">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="11.5" punct="dp:8">r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
		<l n="12" num="2.2" lm="8"><w n="12.1">J</w>'<w n="12.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="12.4">f<seg phoneme="œ" type="vs" value="1" rule="303" place="4">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t</w> <w n="12.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="12.6" punct="pv:8">m<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pv">er</seg></w> ;</l>
		<l n="13" num="2.3" lm="8"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="y" type="vs" value="1" rule="452" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.4" punct="vg:8">s<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rb<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="14" num="2.4" lm="8"><w n="14.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="14.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="2">oin</seg></w> <w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="14.4">b<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="14.6" punct="ps:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>pi<seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="ps">er</seg></w>…</l>
		<l n="15" num="2.5" lm="8"><w n="15.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="15.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="15.3">b<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="15.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s</w> <w n="15.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg></w> <w n="15.7">b<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
		<l n="16" num="2.6" lm="8"><w n="16.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="16.2">p<seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="16.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="16.4" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="pt">an</seg>s</w>.</l>
		<l n="17" num="2.7" lm="4"><w n="17.1">H<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="17.2" punct="pe:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4" punct="pe">an</seg>s</w> !</l>
		<l n="18" num="2.8" lm="4"><w n="18.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>ls</w> <w n="18.2">d<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>x</w> <w n="18.3" punct="pe:4">m<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="4" punct="pe">en</seg>s</w> !</l>
		<p>
			Et vous, monsieur Félix… Qu'est-ce que vous lisez donc là <lb></lb>
			dans le fond de votre casquette… Apportez l… apportez ! … Comment ! … <lb></lb>
			Faublas ! … et ou avez-vous pris ce vilain livre ? … ( roix <lb></lb>
			d'enfant.) chez la femme du proviseur… ( voix naturelle.) Faublas ! <lb></lb>
			chez la femme du proviseur ! … O tempora ! … ô mores ! ( reprenant l'air.) <lb></lb>
		</p>
		<l n="19" num="2.9" lm="8"><w n="19.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="1">e</seg>l</w> <w n="19.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r</w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="19.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="19.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="19.6">cl<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
		<l n="20" num="2.10" lm="8"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="20.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381" place="5">e</seg>ils</w> <w n="20.4" punct="pe:8">g<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rn<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="8" punct="pe">en</seg>s</w> !<del reason="analysis" type="repetition" hand="KL">( bis.)</del></l>
	</lg>
</div></body></text></TEI>