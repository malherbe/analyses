<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHABERT</title>
				<title type="sub">HISTOIRE CONTEMPORAINE EN DEUX ACTES, MÊLÉE DE CHANT</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="ARJ" sort="1">
					<name>
						<forename>Jacques</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1790" to="1855">1790-1855</date>
				</author>
				<author key="LUR" sort="2">
					<name>
						<forename>Louis</forename>
						<surname>LURINE</surname>
					</name>
					<date from="1810" to="1860">1810-1860</date>
				</author>
				<editor>Le rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>110 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">JRL_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
				   </p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CHABERT</title>
						<author>JACQUES ARAGO ET LOUIS LURINE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=vldoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849877.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">2 JUILLET 1832</date>
				<placeName>
					<settlement>THÉÂTRE NATIONAL DU VAUDEVILLE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="JRL9">
	<head type="tune">AIR : T'en souviens-tu ?</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="1.2">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="1.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.6">P<seg phoneme="i" type="vs" value="1" rule="492">y</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
		<l n="2" num="1.2"><w n="2.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="2.2">d</w>'<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>k<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>, <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="2.5">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="2.6">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>g<seg phoneme="o" type="vs" value="1" rule="443">o</seg></w>.</l>
		<l n="3" num="1.3"><w n="3.1">Qu</w>'<w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="3.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w>-<w n="3.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="3.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> ? <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="3.7">ci<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> ! <w n="3.8">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.9">y<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="3.10"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
		<l n="4" num="1.4"><w n="4.1">L<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>, <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>, <w n="4.4">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w>, <w n="4.5">Tr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="o" type="vs" value="1" rule="443">o</seg></w> !</l>
		<l n="5" num="1.5"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="5.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="5.3">b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="5.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.5">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="5.6">vi<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.7">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
		<l n="6" num="1.6"><w n="6.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="6.2">pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="6.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>,</l>
		<l n="7" num="1.7"><w n="7.1">L<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="7.2">j<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="7.3">d</w>'<w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.7">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.8">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
		<l n="8" num="1.8"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="8.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="8.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="8.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>.</l>
	</lg>
</div></body></text></TEI>