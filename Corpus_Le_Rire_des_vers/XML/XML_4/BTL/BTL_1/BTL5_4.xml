<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CONSEIL DE RÉVISION, OU LES MAUVAIS NUMÉROS</title>
				<title type="sub">TABLEAU-VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="BRU" sort="1">
					<name>
						<forename>Léon-Lévy</forename>
						<surname>BRUNSWICK</surname>
						<addname type="other">Léon LHÉRIE</addname>
					</name>
					<date from="1805" to="1859">1805-1859</date>
				</author>
				<author key="THO" sort="2">
				  <name>
					<forename>Mathieu Barthélemy</forename>
					<surname>THOUIN</surname>
					<addname type="other">Barthélemy</addname>
				  </name>
				  <date from="1804" to="18..">1804-18..</date>
				</author>
				<author key="LVY" sort="3">
				  <name>
					<forename>Léon-Victor</forename>
					<surname>LÉVY</surname>
					<addname type="pen_name">Victor LHÉRIE</addname>
					<addname type="other">Lhérie</addname>
				  </name>
				  <date from="1808" to="1845">1808-1845</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>212 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BTL_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CONSEIL DE RÉVISION, OU LES MAUVAIS NUMÉROS</title>
						<author>BRUNSWICK, BARTHÉLEMY ET LHÉRIE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=NYPL :33433075800833</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>New York Public Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/nyp.33433075800833</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">4 AOÛT 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BTL5">
	<head type="tune">Air : Ah ! ah ! ah ! ( Savetier et Financier.)</head>
		<div type="section" n="1">
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg>h</w> ! <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> ! <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> ! <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> ! <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> ! <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> !</l>
				<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="2.4">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> !</l>
				<l n="3" num="1.3"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="3.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				<l n="4" num="1.4"><w n="4.1">J</w>'<w n="4.2">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.4">v<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				<l n="5" num="1.5"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg>h</w> ! <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> ! <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> ! <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> ! <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> ! <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> !</l>
				<l n="6" num="1.6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">n</w>' <w n="6.3">pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="6.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>…</l>
				<l n="7" num="1.7"><w n="7.1">L</w>'<w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="7.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss</w>' <w n="7.5">j<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="7.7">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>.</l>
				<l n="8" num="1.8"><w n="8.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">s<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="8.4">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="8.5">cr<seg phoneme="y" type="vs" value="1" rule="453">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="9" num="1.9"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.2">tr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="9.3">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w>, <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="9.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>,</l>
				<l n="10" num="1.10"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="10.3">pr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="10.4">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>,</l>
				<l n="11" num="1.11"><w n="11.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
			</lg>
		</div>
		<div type="section" n="2">
			<head type="main">SCÈNE VII.</head>
			<head type="sub">CHOPIN, BLOQUET, ADÈLE, MOUFFLLT.</head>
			<lg n="1">
			<head type="speaker">CHOPIN et MOUFFLET entrant.</head>
				<l n="12" num="1.1"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>h</w> ! <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>h</w> ! <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>h</w> ! <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>h</w> ! <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>h</w> !</l>
				<l n="13" num="1.2"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="13.4">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> !</l>
				<l n="14" num="1.3"><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="14.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				<l n="15" num="1.4"><w n="15.1">J</w>'<w n="15.2">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="15.4">v<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				<l n="16" num="1.5"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>h</w> ! <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>h</w> ! <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>h</w> ! <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>h</w> ! <w n="16.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>h</w> !</l>
				<l n="17" num="1.6"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="17.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="17.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>…</l>
				<l n="18" num="1.7"><w n="18.1">L</w>'<w n="18.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="18.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss</w>' <w n="18.5">j<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="18.7">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>.</l>
			</lg>
		</div>
</div></body></text></TEI>