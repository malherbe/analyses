<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE30">			
	
	<head type="main">SAINT SYLVESTRE.</head>

			<p>Je vous avoue que je n’ai guère chaud !..</p>

			<head type="tune">AIR : Et voilà comme tout s’arrange.</head>

			<lg n="1"><l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>s</w> <w n="1.4">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="1.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>-<w n="1.6">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w></l>
			<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="2.3">su<seg phoneme="i" type="vs" value="1" rule="490">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">l</w>’<w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg></w> <w n="2.6">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="2.7">s</w>’<w n="2.8"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>squ<seg phoneme="i" type="vs" value="1" rule="490">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
		   <l n="3" num="1.3"> <w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w>, <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.4">n<seg phoneme="ɛ" type="vs" value="1" rule="383">ei</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rgl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w></l>
			<l n="4" num="1.4"><w n="4.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="4.4">l<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.5">j</w>’<w n="4.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ....</l>
			<l n="5" num="1.5"><w n="5.1">H<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ! <w n="5.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="5.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="5.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="5.6">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rts</w> ;</l>
			<l n="6" num="1.6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">f<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d</w> <w n="6.5">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
			<l n="7" num="1.7"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="7.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w> <w n="7.7">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> ...</l>
			<l n="8" num="1.8"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">fr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>d</w> <w n="8.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">gr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> ....</l>
			<l n="9" num="1.9"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">su<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="9.3">l</w>’<w n="9.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg>h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="9.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t</w> <w n="9.6">S<seg phoneme="i" type="vs" value="1" rule="492">y</seg>lv<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ! <del hand="LN" type="repetition" reason="analysis">(bis).</del></l></lg></div></body></text></TEI>