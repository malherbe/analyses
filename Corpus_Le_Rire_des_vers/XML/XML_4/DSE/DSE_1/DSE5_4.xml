<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE5">
		
			<head type="main">BIFFARD.</head>

			<p>Comment ! il n’y a pas de quoi ? Seriez-vous au nombre de ces esprits endurcis dans l’erreur qui nient les services immenses que nous rendons tous les jours à la monarchie ? détrompez-vous, monsieur.</p>
		
		<head type="tune">AIR : Moi, je flâne.</head>

				<lg n="1"><l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">S<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="2.3">s<seg phoneme="y" type="vs" value="1" rule="444">û</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="3" num="1.3"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="3.2">g<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="3.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">l</w>’<w n="3.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="4" num="1.4"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="5" num="1.5"><w n="5.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="5.2">bl<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="1.6"><w n="6.1">Gu<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w></l>
					<l n="7" num="1.7"><w n="7.1">L<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w>.</l></lg>

				<lg n="2"><l n="8" num="2.1"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="8.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</l>
					<l n="9" num="2.2"><w n="9.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="9.2">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.3">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rv<seg phoneme="e" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="2.3"><w n="10.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="10.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>tʼr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="10.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="10.4">V<seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="11" num="2.4"><w n="11.1">J<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>’<w n="11.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="11.3">rʼfr<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">B<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ;</l>
					<l n="12" num="2.5"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="12.2">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="12.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.5">r<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="13" num="2.6"><w n="13.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="13.2">r<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.4">fl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>,</l>
					<l n="14" num="2.7"><w n="14.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="14.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ri<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="15" num="2.8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="15.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> <w n="15.4">d</w>’<w n="15.5"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>tz</w>.</l>
				</lg>
				
				<lg type="refrain" n="3">
					<l ana="unanalyzable" n="16" num="3.1">La censure, etc.</l>
				</lg>

				<lg n="4"><l n="17" num="4.1"><w n="17.1">F<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w>-<w n="17.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="17.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="17.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="17.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w></l>
					<l n="18" num="4.2"><w n="18.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="18.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="18.3">m<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="19" num="4.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="19.2">l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="19.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="19.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="20" num="4.4"><w n="20.1">D</w>’<w n="20.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>tʼs</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> ?</l>
					<l n="21" num="4.5"><w n="21.1">Ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="21.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt</w> <w n="21.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="21.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="22" num="4.6"><w n="22.1">Pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ctr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c</w>’ <w n="22.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="22.3">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>ts</w> <w n="22.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>,</l>
					<l n="23" num="4.7"><w n="23.1">C</w>’<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="23.3">l</w>’<w n="23.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="23.6">g<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="24" num="4.8"><w n="24.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="24.2">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w> <w n="24.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="24.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="24.5">c<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>.</l>
				</lg>

				<lg n="5">
					<l ana="unanalyzable" n="25" num="5.1">La censure, etc.</l>
				</lg>

				<lg n="6"><l n="26" num="6.1"><w n="26.1">C</w>’<w n="26.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="26.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="26.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> !</l>
					<l n="27" num="6.2"><w n="27.1">Qu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> ! ... <w n="27.2">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="27.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w> <w n="27.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="o" type="vs" value="1" rule="443">o</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="28" num="6.3"><w n="28.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="28.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="28.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>b<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="29" num="6.4"><w n="29.1">J<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="29.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="29.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>il</w> <w n="29.4">d</w>’<w n="29.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> !</l>
					<l n="30" num="6.5"><w n="30.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="30.2">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="30.4">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.5">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="31" num="6.6"><w n="31.1">F<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w>-<w n="31.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="31.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>’ <w n="31.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="31.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="31.6">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> ?</l>
					<l n="32" num="6.7"><w n="32.1"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="32.2">pl<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="32.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.4">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="33" num="6.8"><w n="33.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="33.2">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w>, <w n="33.3">ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="33.4">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="33.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w>.</l>
				</lg>

				<lg n="7">
					<l ana="unanalyzable" n="34" num="7.1">La censure, etc.</l>
				</lg>
	</div></body></text></TEI>