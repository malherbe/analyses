<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE18">			
	
	<head type="main">MATHIEU.</head>

			<p>(Il fait avec sa baguette plusieurs démonstrations cabalistiques ; l’ Auréole et Polycarpe paraissent fort étonnés.)</p>

			<head type="tune">AIR : L’Enfer vous demande.</head>

				<lg n="1"><l n="1" num="1.1"><w n="1.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="1.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">m<seg phoneme="i" type="vs" value="1" rule="492">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ....</l>
					<l n="2" num="1.2"><w n="2.1">R<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ct<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="2.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>ts</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ppr<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>ts</w> !</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="3.2">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="3.3">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>cc<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="4.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.4">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x</w> .... <w n="4.5">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> !</l></lg>

				<p>(Regardant à terre.)</p>

				<lg n="2"><l n="5" num="2.1"><w n="5.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="5.2"><seg phoneme="œ" type="vs" value="1" rule="285">œ</seg>il</w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="2.2"><w n="6.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> ! ... <w n="6.2">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w></l>
					<l n="7" num="2.3"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.2">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">s</w>’<w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>trʼ<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				<p>
				(On entend une musique harmonieuse et légère. Mathieu Lænsberg frappe la terre de sa baguette comme pour en faire sortir les Libertés, et c’est du ciel qu’on les voit descendre dans une gloire. Apercevant le nuage qui descend.)
				</p>
				<l n="8" num="2.4"><w n="8.1">Ti<seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>s</w>, <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> !</l>
				</lg>



			</div></body></text></TEI>