<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE VIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC25">
					<head type="tune">AIR de la Robe et des Bottes.</head>
						<lg n="1">
							<head type="main">POLWARTH.</head>
							<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="1.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="1.4">m</w>'<w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="1.6">bl<seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="1.7">j</w>'<w n="1.8"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> …</l>
						</lg>
						<lg n="2">
							<head type="main">ARWED.</head>
							<l n="2" num="2.1"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="2.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>il</w> <w n="2.3">z<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="2.5">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.6">d</w>'<w n="2.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="2.8">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w>.</l>
						</lg>
						<lg n="3">
							<head type="main">POLWARTH.</head>
							<l n="3" num="3.1"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="3.3">d</w>'<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd</w> <w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="3.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="3.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.9">cu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> …</l>
						</lg>
						<lg n="4">
							<head type="main">ARWED.</head>
							<l n="4" num="4.1"><space unit="char" quantity="4"></space><w n="4.1">Ou<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="4.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="4.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ct<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.5">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rn<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w>.</l>
							<l n="5" num="4.2"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ds</w> <w n="5.3">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>x<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ct<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l part="I" n="6" num="4.3"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w>, <w n="6.4">c</w>'<w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="6.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w>-<w n="6.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> … </l>
							</lg>
						<lg n="5">
							<head type="main">POLWARTH.</head>
							<l part="F" n="6"><w n="6.8">Ou<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="6.9">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rbl<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> !</l>
							<l n="7" num="5.1"><w n="7.1">Ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.2">pr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="7.4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>s</w> <w n="7.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">j</w>'<w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="7.8">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="7.9">l</w>'<w n="7.10">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="8" num="5.2"><space unit="char" quantity="4"></space><w n="8.1">D</w>'<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="8.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="8.6">f<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>.</l>
						</lg>
					</div></body></text></TEI>