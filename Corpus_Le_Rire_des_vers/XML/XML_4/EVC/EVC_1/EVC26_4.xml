<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE IX.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC26">
					<head type="tune">AIR nouveau de M. Doche.</head>
					<lg n="1">
						<head type="main">JOB.</head>
						<l part="I" n="1" num="1.1"><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> ! </l>
					</lg>
					<lg n="2">
						<head type="main">POLWARTH.</head>
						<l part="F" n="1"><w n="1.2"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> ! <w n="1.3">c</w>'<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="1.5">l</w>'<w n="1.6">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> …</l>
						</lg>
					<lg n="3">
						<head type="main">JOB, se levant.</head>
						<l n="2" num="3.1"><space unit="char" quantity="8"></space><w n="2.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="2.2">bru<seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w>.</l>
						</lg>
					<lg n="4">
						<head type="main">ARWED.</head>
						<l n="3" num="4.1"><space unit="char" quantity="8"></space><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="3.2">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="3.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> …</l>
						<l n="4" num="4.2"><space unit="char" quantity="8"></space><w n="4.1">V<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="4.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="4.5">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w> !</l>
						</lg>
					<lg n="5">
						<head type="main">JOB.</head>
						<l rhyme="none" n="5" num="5.1"><space unit="char" quantity="12"></space><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> ! <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> !</l>
						</lg>
					<lg n="6">
						<head type="main">POLWARTH et ARWED, se lèvent.</head>
						<l n="6" num="6.1"><space unit="char" quantity="8"></space><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="6.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> … <w n="6.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="6.6">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="7" num="6.2"><space unit="char" quantity="8"></space><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="7.4">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="7.5">d</w>'<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						</lg>
					<lg n="7">
						<head type="main">JOB.</head>
						<l n="8" num="7.1"><space unit="char" quantity="4"></space><w n="8.1">Ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.2">m</w>'<w n="8.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="8.4">j</w>' <w n="8.5">n</w>'<w n="8.6"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="8.8">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</l>
					</lg>
					<p>. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</p>
					<lg n="8">
						<head type="main">LADY.</head>
						<l n="9" num="8.1"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="9.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="9.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">m</w>'<w n="9.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.9"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="9.10">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.11">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="10" num="8.2"><space unit="char" quantity="8"></space><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>rw<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>d</w>, <w n="10.2">l</w>'<w n="10.3">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="10.5">d</w>'<w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xp<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
						<l n="11" num="8.3"><space unit="char" quantity="4"></space><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="11.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="11.7">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t</w> <w n="11.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="9">
						<head type="main">ARWED.</head>
						<l n="12" num="9.1"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="12.2">l</w>'<w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="12.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="12.5">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="12.6">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="12.7">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</l>
						<l n="13" num="9.2"><space unit="char" quantity="8"></space><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="13.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.4">g<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="13.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						</lg>
					<lg n="10">
						<head type="main">POLWARTH.</head>
						<l n="14" num="10.1"><space unit="char" quantity="4"></space><w n="14.1">Ou<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="14.2">W<seg phoneme="a" type="vs" value="1" rule="339">a</seg>sh<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gt<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.4">t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="14.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.6">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="15" num="10.2"><space unit="char" quantity="8"></space><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>rw<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>d</w> <w n="15.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="15.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="15.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
						<l n="16" num="10.3"><space unit="char" quantity="8"></space><w n="16.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="16.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="16.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>nn<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="16.5">tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						</lg>
					<lg n="11">
						<head type="main">LADY.</head>
						<l n="17" num="11.1"><space unit="char" quantity="8"></space><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="17.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="17.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="17.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="17.5">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="17.6">d</w>'<w n="17.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						</lg>
					<lg n="12">
						<head type="main">ЈOB.</head>
						<l n="18" num="12.1"><space unit="char" quantity="8"></space><w n="18.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="18.2">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="o" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="18.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="18.4">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
						</lg>
					<lg n="13">
						<head type="main">TOUS.</head>
						<l n="19" num="13.1"><space unit="char" quantity="16"></space><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">su<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="19.3">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
						<l n="20" num="13.2"><space unit="char" quantity="8"></space><w n="20.1">Ou<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="20.2">c</w>'<w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="20.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="20.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>, <w n="20.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="20.7"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="20.8">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
						<l n="21" num="13.3"><space unit="char" quantity="16"></space><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="21.3">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ! <del hand="RR" reason="analysis" type="alternative">Je suis sauvé !</del>!</l>
					</lg>
					<p>(Il jette son chapeau en l'air.-On entend un coup de canon.-Il se fait un moment de silence, pendant lequel les personnages montrent leur douleur.)</p>
					<div type="section" n="1">
						<head type="tune">Suite de l'air.</head>
						<lg n="1">
							<head type="main">TOUS.</head>
							<l n="22" num="1.1"><space unit="char" quantity="12"></space><w n="22.1">F<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.2">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
							<l n="23" num="1.2"><space unit="char" quantity="12"></space><w n="23.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="23.2">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="23.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.4">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="23.5">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> !</l>
							<l n="24" num="1.3"><space unit="char" quantity="12"></space><w n="24.1">C<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.2">tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.3">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="25" num="1.4"><space unit="char" quantity="12"></space><w n="25.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="25.3">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="25.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> !</l>
						</lg>
					</div>
				</div></body></text></TEI>