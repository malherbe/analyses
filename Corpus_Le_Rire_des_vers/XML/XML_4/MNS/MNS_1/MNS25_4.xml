<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS25">
	<head type="tune">Air du dernier soupir de Veber.</head> 
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.2">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="2" num="1.2"><w n="2.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="3" num="1.3"><w n="3.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>j<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="3.3">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> ; </l>
		<l n="4" num="1.4"><w n="4.1">Pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="4.2">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="5" num="1.5"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="5.3">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>, </l>
		<l n="6" num="1.6"><w n="6.1">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="6.2"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bj<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.5">s<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>s</w> <w n="6.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w>. </l>
		<l n="7" num="1.7"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="7.2">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="7.4">l</w>'<w n="7.5">h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>, </l>
		<l n="8" num="1.8"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> <w n="8.2">l</w>'<w n="8.3">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w></l>
		<l n="9" num="1.9"><w n="9.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.4">d<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.6">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="10" num="1.10"><w n="10.1">M<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t</w>-<w n="10.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="10.3">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="10.5">r<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ? </l>
		<l n="11" num="1.11"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.2">tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="12" num="1.12"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="12.2">m</w>'<w n="12.3"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ppr<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="13" num="1.13"><w n="13.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="13.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="13.3">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.4">y<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> ; </l>
		<l n="14" num="1.14"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">m</w>'<w n="14.3"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="14.4">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="15" num="1.15"><w n="15.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="15.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="16" num="1.16"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="16.2">d</w>'<w n="16.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="16.4">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.5">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>. </l>
		<l n="17" num="1.17"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="17.2">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.3">p<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="18" num="1.18"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>-<w n="18.2">h<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="18.3">j</w>'<w n="18.4"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="19" num="1.19"><w n="19.1">Br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="19.3">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="19.6"><seg phoneme="e" type="vs" value="1" rule="169">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> ; </l>
		<l n="20" num="1.20"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="20.2">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="20.3">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.4">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="21" num="1.21"><w n="21.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="21.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="21.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>, </l>
		<l n="22" num="1.22"><w n="22.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="22.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="22.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="22.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="22.5">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> <w n="22.6">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <del reason="analysis" type="irrelevant" hand="KL">( Il se lève.)</del></l>
		<l n="23" num="1.23"><w n="23.1">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.3"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="24" num="1.24"><w n="24.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="24.2">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="25" num="1.25"><w n="25.1">J<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="25.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="25.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="25.4">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="25.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w>. </l>
		<l rhyme="none" n="26" num="1.26"><w n="26.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">Om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.2">ch<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l rhyme="none" n="27" num="1.27"><w n="27.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="27.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="27.3">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="28" num="1.28"><w n="28.1">Pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.3">vi<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="28.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> ! </l>
	</lg>
</div></body></text></TEI>