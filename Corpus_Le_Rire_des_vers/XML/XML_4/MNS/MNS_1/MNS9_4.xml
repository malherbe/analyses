<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS9">
	<head type="tune">Air : Cupidon, bas de ses folies, ou Il porte l'épée et la lyre <lb></lb>(du vaudeville de la Belle au bois Dormant).</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">c<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">bi<seg phoneme="ɑ̃" type="vs" value="1" rule="377">en</seg>f<seg phoneme="œ" type="vs" value="1" rule="303">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="2" num="1.2"><w n="2.1">F<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>-<w n="2.2">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="2.3">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">l</w>'<w n="2.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> ; </l>
		<l n="3" num="1.3"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.3">t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> ! <w n="3.4">c</w>'<w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.6"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>, </l>
		<l n="4" num="1.4"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>ç<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="4.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ! </l>
		<l n="5" num="1.5"><w n="5.1">C<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <w n="5.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="5.3">d</w>'<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.5">m<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ; </l>
		<l n="6" num="1.6"><w n="6.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.3">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="6.4">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="6.5">s</w>'<w n="6.6"><seg phoneme="e" type="vs" value="1" rule="352">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, </l>
		<l n="7" num="1.7"><w n="7.1">Cr<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="7.2">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="7.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.5">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, </l>
		<l n="8" num="1.8"><w n="8.1">C</w>'<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="8.5">d</w>'<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>scr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.  <del reason="analysis" type="repetition" hand="KL">( Bis.)</del></l>
	</lg>
</div></body></text></TEI>