<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">COMMÈRE JEANNETON</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>261 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE X.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD6">
				<head type="tune">Air : Montagnard de la Fiancée.</head>
				<lg n="1">
					<head type="main">GRATIEN.</head>
					<l n="1" num="1.1"><space unit="char" quantity="6"></space><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>h</w> ! <w n="1.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="1.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="1.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">f<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w></l>
					<l n="2" num="1.2"><space unit="char" quantity="6"></space><w n="2.1">J</w>'<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="2.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffl<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="2.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="2.5">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.6">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>gts</w> !</l>
					<l n="3" num="1.3"><space unit="char" quantity="6"></space><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.5">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="6"></space><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="4.2">n</w>'<w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="4.4">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vʼr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="4.6">tr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>.</l>
					<l n="5" num="1.5"><space unit="char" quantity="12"></space><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>,</l>
					<l n="6" num="1.6"><space unit="char" quantity="12"></space><w n="6.1">Jʼ</w> <w n="6.2">gr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>,</l>
					<l n="7" num="1.7"><space unit="char" quantity="12"></space><w n="7.1">Jʼ</w> <w n="7.2">m</w>'<w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>rh<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>,</l>
					<l n="8" num="1.8"><space unit="char" quantity="12"></space><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>.</l>
					<l n="9" num="1.9"><space unit="char" quantity="6"></space><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="9.2">dʼv<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="9.3">lʼ</w> <w n="9.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="9.5">dʼ</w> <w n="9.6">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>trʼ</w> <w n="9.7">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="10" num="1.10"><space unit="char" quantity="6"></space><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="10.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> ;</l>
					<l n="11" num="1.11"><space unit="char" quantity="6"></space><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="u" type="vs" value="1" rule="426">ou</seg>illʼ</w> <w n="11.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w></l>
					<l n="12" num="1.12"><space unit="char" quantity="12"></space><w n="12.1">M<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">v<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>,</l>
					<l n="13" num="1.13"><space unit="char" quantity="12"></space><w n="13.1">M</w>'<w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>,</l>
					<l n="14" num="1.14"><space unit="char" quantity="2"></space><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="14.2">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="14.4">vi<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.6">m<seg phoneme="e" type="vs" value="1" rule="352">e</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> ;</l>
					<l n="15" num="1.15"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="15.2">lʼ</w> <w n="15.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w>, <w n="15.4">rʼbr<seg phoneme="y" type="vs" value="1" rule="444">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="16" num="1.16"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">rʼv<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="16.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="16.4">dʼ</w> <w n="16.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="16.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="16.7">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> :</l>
					<l n="17" num="1.17"><space unit="char" quantity="6"></space><w n="17.1">C</w>'<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="17.3">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="17.4">qu</w>'<w n="17.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="17.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.7">m<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="18" num="1.18"><space unit="char" quantity="6"></space><w n="18.1">C</w>'<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="18.3">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="18.4">qu</w>'<w n="18.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="18.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.7">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>.</l>
				</lg>
			</div></body></text></TEI>