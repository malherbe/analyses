<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">COMMÈRE JEANNETON</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>261 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIEME</head><head type="main_subpart">SCENE PREMIERE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD9">
				<head type="main">MÈRE GIBOU.</head>
				<head type="tune">AIR : Une fille est un oiseau.</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mmʼ</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w> <w n="1.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="1.5">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>p<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> !</l>
					<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>-<w n="2.2">jʼ</w> <w n="2.3">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>rʼ</w> <w n="2.4">ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.5">dʼ</w> <w n="2.6">l</w>'<w n="2.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="3" num="1.3"><w n="3.1">M<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="3.2">qu</w>'<w n="3.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="3.4">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="3.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.4">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="4.5">zh<seg phoneme="y" type="vs" value="1" rule="449">u</seg>pp<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Dʼp<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="5.2">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="5.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">jʼ</w> <w n="5.6">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="5.7">fr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>du<seg phoneme="i" type="vs" value="1" rule="490">i</seg>tʼ</w> <w n="6.3">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t</w> <w n="6.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="6.5">cl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="7" num="1.7"><w n="7.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>tʼn<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="7.2">jʼ</w> <w n="7.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">cʼt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ni<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="1.8"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="8.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.3">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="8.4">dʼ</w> <w n="8.5">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="8.6">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>tru<seg phoneme="i" type="vs" value="1" rule="490">i</seg>ts</w>.</l>
					<l n="9" num="1.9"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="9.2">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="9.3">dʼ</w> <w n="9.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="9.6">dʼ</w> <w n="9.7">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="1.10"><w n="10.1">C</w>'<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="10.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w>, <w n="10.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="10.5">l</w>'<w n="10.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="10.7">qu</w>' <w n="10.8">j</w>'<w n="10.9"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>x<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="11" num="1.11"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">nʼ</w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="11.5">rʼc<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="11.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.7">fru<seg phoneme="i" type="vs" value="1" rule="490">i</seg>ts</w>.</l>
				</lg>
				<div type="section" n="1">
					<head type="main">ENSEMBLE. |</head>
					<lg n="1">
						<head type="sub">MÈRE GIBOU.</head>
						<l n="12" num="1.1"><space unit="char" quantity="8"></space><w n="12.1">T<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <del reason="analysis" type="repetition" hand="LG">(bis)</del></l>
						<l n="13" num="1.2"><space unit="char" quantity="8"></space><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="13.1">T<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>,</add></subst></l>
						<l n="14" num="1.3"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="14.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="14.5">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>, <del reason="analysis" type="repetition" hand="LG">etc.</del></l>
						<l n="15" num="1.4"><space unit="char" quantity="8"></space><w n="15.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="15.2">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <del reason="analysis" type="repetition" hand="LG">(bis)</del></l>
						<l n="16" num="1.5"><space unit="char" quantity="8"></space><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="16.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="16.2">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w></add></subst></l>
						<l n="17" num="1.6"><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="17.1"><seg phoneme="ɛ" type="vs" value="1" rule="198">E</seg>st</w> <w n="17.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="17.3">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="17.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</add></subst></l>
						<l n="18" num="1.7"><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="18.2">jʼ</w> <w n="18.3">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="18.4">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="18.6">l</w>'<w n="18.7"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</add></subst></l>
						<l n="19" num="1.8"><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="19.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="19.2">jʼ</w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="19.4">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.6">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>, </add></subst></l>
						<l n="20" num="1.9"><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="20.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="20.2">nʼ</w> <w n="20.3">m</w>'<w n="20.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="20.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.6">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>, </add></subst></l>
						<l n="21" num="1.10"><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="21.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">cr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="21.3">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="21.4">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="21.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.6">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>. </add></subst></l>
					</lg>
					<lg n="2">
						<head type="main">LES COMMÈRES.</head>
						<l n="22" num="2.1"><space unit="char" quantity="8"></space><w n="22.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lm<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="22.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <del reason="analysis" type="repetition" hand="LG">(bis)</del></l>
						<l n="23" num="2.2"><space unit="char" quantity="8"></space><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="23.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lm<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="23.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>,</add></subst></l>
						<l n="24" num="2.3"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="24.2">qu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> <w n="24.3">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt</w> <w n="24.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="24.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.6">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ? <del reason="analysis" type="repetition" hand="LG">etc.</del></l>
						<l n="25" num="2.4"><space unit="char" quantity="8"></space><w n="25.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lm<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="25.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>,<del reason="analysis" type="repetition" hand="LG">(bis)</del></l>
						<l n="26" num="2.5"><space unit="char" quantity="8"></space><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="26.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lm<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="26.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>,</add></subst></l>
						<l n="27" num="2.6"><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="27.1">Nʼ</w> <w n="27.2">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="27.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="27.4">tʼn<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="27.5">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="27.7">l</w>'<w n="27.8"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>. </add></subst></l>
						<l n="28" num="2.7"><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="28.1">C<seg phoneme="o" type="vs" value="1" rule="434">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="28.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.3">sʼr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="28.4">d<seg phoneme="o" type="vs" value="1" rule="434">o</seg>mm<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></add></subst></l>
						<l n="29" num="2.8"><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="29.1">Dʼ</w> <w n="29.2">s</w>'<w n="29.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="29.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="29.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="29.6">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ; </add></subst></l>
						<l n="30" num="2.9"><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="30.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w> <w n="30.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="30.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <w n="30.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.5">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>, </add></subst></l>
						<l n="31" num="2.10"><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="31.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="31.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="31.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="31.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>. </add></subst></l>
					</lg>
				</div>
			</div></body></text></TEI>