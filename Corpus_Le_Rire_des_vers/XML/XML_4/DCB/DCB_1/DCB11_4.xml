<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB11">
	<head type="main">ENSEMBLE</head>
	<lg n="1">
	<head type="speaker">RICHOUX</head>
	<head type="tune">AIR des gardes-marines.</head>
		<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">su<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="1.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="1.4">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="1.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>.</l>
		<l n="2" num="1.2"><w n="2.1">C</w>'<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>cʼm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="2.5">d</w>'<w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="2.7">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="2.8">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> :</l>
		<p>
			Madame Durand, Charles, Richoux, Télémaque.
		</p> 
		<l n="3" num="1.3"><w n="3.1">J</w>'<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="3.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="3.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="3.5">j</w>'<w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="3.7">b<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.9">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="4.2">j</w>'<w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="4.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="4.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">j</w>'<w n="4.8"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
		<l n="5" num="1.5"><w n="5.1">T<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="5.4">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="5.5">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.6">l</w>'<w n="5.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> ;</l>
		<l n="6" num="1.6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>, <w n="6.2">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>vʼti<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="6.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="6.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w></l>
		<l n="7" num="1.7"><w n="7.1">D<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>-<w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="7.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> !</l>
	</lg> 
	<lg n="2">
	<head type="speaker">TÉLÉMAQUE.</head>
		<l n="8" num="2.1"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="8.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="8.4">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="8.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> :</l>
		<l n="9" num="2.2"><w n="9.1">C</w>'<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>cʼm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="9.5">d</w>'<w n="9.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="9.7">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="9.8">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>.</l>
		<l n="10" num="2.3"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="10.4">b<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w>, <w n="10.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.7">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
		<l n="11" num="2.4"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="11.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="11.5">qu</w>'<w n="11.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="11.7"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
		<l n="12" num="2.5"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="12.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="12.4">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="12.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> ;</l>
		<l n="13" num="2.6"><w n="13.1">J</w>'<w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="13.3">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>, <w n="13.5">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="13.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="13.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w></l>
		<l n="14" num="2.7"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">n</w>'<w n="14.3">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="14.5">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> : <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="14.7">l</w>'<w n="14.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> !</l>
	</lg> 
	<lg n="3">
	<head type="speaker">CHARLES et MADAME DURAND.</head>
		<l n="15" num="3.1"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="15.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="15.4">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="15.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> :</l>
		<l n="16" num="3.2"><w n="16.1">C</w>'<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>cʼm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="16.5">d</w>'<w n="16.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="16.7">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="16.8">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>.</l>
		<l n="17" num="3.3"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="17.4">b<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w>, <w n="17.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="17.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.7">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
		<l n="18" num="3.4"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="18.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="18.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="18.5">qu</w>'<w n="18.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="18.7"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
		<l n="19" num="3.5"><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="19.2">l</w>'<w n="19.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.4">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rdʼr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="19.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="19.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> :</l>
		<l n="20" num="3.6"><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="20.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="20.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="20.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w></l>
		<l n="21" num="3.7"><w n="21.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="21.2">lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="21.3">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss</w>'<w n="21.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> : <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="21.6">l</w>'<w n="21.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> !</l>
	</lg> 
</div></body></text></TEI>