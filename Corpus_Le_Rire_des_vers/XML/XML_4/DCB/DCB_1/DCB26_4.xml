<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB26">
	<head type="main">CHŒUR</head>
	<head type="tune">AIR : Vive ma petite Jeannette.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">L</w>'<w n="1.2">j<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
		<l n="2" num="1.2"><w n="2.1">Qu</w>'<w n="2.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="2.4">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="2.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="2.6">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w></l>
		<l n="3" num="1.3"><w n="3.1">D</w>'<w n="3.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="3.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="3.4">h<seg phoneme="o" type="vs" value="1" rule="434">o</seg>mm<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="4" num="1.4"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ls</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> !</l>
	</lg>
	<lg n="2">
	<head type="speaker">TÉLÉMAQUE.</head>
		<l n="5" num="2.1"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.2">v<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w>, <w n="5.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>-<w n="5.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="5.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="6" num="2.2"><w n="6.1"><seg phoneme="u" type="vs" value="1" rule="425">Où</seg></w> <w n="6.2">f<seg phoneme="a" type="vs" value="1" rule="192">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.4">bʼs<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w> <w n="6.5">d</w>'<w n="6.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="o" type="vs" value="1" rule="443">o</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ;</l>
		<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="7.2">l</w>'<w n="7.3">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="7.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="7.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.7">v<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="8" num="2.4"><w n="8.1">D<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="8.3">ch<seg phoneme="o" type="vs" value="1" rule="317">au</seg>ss<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="8.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="8.6">pi<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
	</lg>
	<lg n="3">
	<head type="speaker">CHARLES.</head>
		<l n="9" num="3.1"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.2">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="9.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">l</w>'<w n="9.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.6">s<seg phoneme="o" type="vs" value="1" rule="434">o</seg>ll<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="10" num="3.2"><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.2">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.3">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="10.5">p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
	</lg>
	<lg n="4">
	<head type="speaker">CÉLESTINE, vivement.</head>
		<l n="11" num="4.1"><w n="11.1">Ou<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="11.2">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="11.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="11.4">v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="11.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="11.6">tr<seg phoneme="o" type="vs" value="1" rule="432">o</seg>p</w> <w n="11.7">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="12" num="4.2"><w n="12.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rsqu</w>'<w n="12.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="12.3">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="12.4">l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.5">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="12.7">pi<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
	</lg>
	<lg n="5">
	<head type="speaker">RICHOUX</head>
		<l n="13" num="5.1"><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>tr</w>' <w n="13.3">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="13.4"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="14" num="5.2"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="14.2">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="14.3">qu</w>'<w n="14.4">l</w>'<w n="14.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="14.6">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="14.7">m<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
		<l n="15" num="5.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="15.2">j</w>'<w n="15.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="15.6">p<seg phoneme="o" type="vs" value="1" rule="434">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="16" num="5.4"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">n</w>'<w n="16.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="16.5">s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="16.6">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="16.7">pi<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
	</lg>
	<lg n="6">
	<head type="speaker">TÉLÉMAQUE, au public.</head>
		<l n="17" num="6.1"><w n="17.1">M<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ssi<seg phoneme="ø" type="vs" value="1" rule="396">eu</seg>rs</w>, <w n="17.2">l<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="18" num="6.2"><w n="18.1">F<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.2">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="18.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.5">m<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>ti<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
		<l n="19" num="6.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="19.2">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>qu</w>'<w n="19.3">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="19.4">ch<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="19.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="19.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.7">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="19.8">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="20" num="6.4"><w n="20.1">Vʼn<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="20.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="20.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="20.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> <w n="20.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="20.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="20.7">pi<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
	</lg> 
	<lg n="7">
	<head type="main">CHŒUR.</head>
		<l n="21" num="7.1"><w n="21.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="21.2">vi<seg phoneme="ɑ̃" type="vs" value="1" rule="377">en</seg>dr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <w n="21.3">j</w>'<w n="21.4"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="22" num="7.2"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="22.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ;</l>
		<l n="23" num="7.3"><w n="23.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="23.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="23.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="23.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="24" num="7.4"><w n="24.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="24.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="24.3">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="24.4">pi<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
	</lg> 
</div></body></text></TEI>