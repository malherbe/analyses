<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANDRÉ LE CHANSONNIER</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="FTN" sort="1">
					<name>
						<forename>Louis Marie</forename>
						<surname>FONTAN</surname>
					</name>
					<date from="1801" to="1839">1801-1839</date>
				</author>
				<author key="DNY" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>DESNOYER</surname>
					</name>
					<date from="1806" to="1858">1806-1858</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>239 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>André le chansonnier.</title>
						<author>FONTAN ET DESNOYER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=aN6oyNpF3cMC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>André le chansonnier.</title>
								<author>FONTAN ET DESNOYER</author>
								<repository>Biblioteca Casanatense</repository>
								<idno type="URI">http://opac.casanatense.it/Record.htm?idlist=3</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Ode et Wodon</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-15" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE I</head><head type="main_subpart">SCÈNE VIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FED7">
				<head type="tune">AIR de la Vieille.</head>
					<lg n="1">
						<head type="main">ANDRÉ.</head>
						<l n="1" num="1.1"><space unit="char" quantity="4"></space><w n="1.1">T<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.2">d</w>'<w n="1.3"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>x<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w>, <w n="1.4">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="1.6">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.7">y<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="2.2">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="2.3">t</w>'<w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="49">e</seg>s</w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="3.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="3.4">d</w>'<w n="3.5"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">V<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.2">m</w>'<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="4.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.6">li<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>.</l>
						<l n="5" num="1.5"><space unit="char" quantity="4"></space><w n="5.1">Pl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>rs</w>, <w n="5.2">g<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="5.3">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.4">f<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="1.6"><space unit="char" quantity="4"></space><w n="6.1">V<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>lt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>g<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="6.2">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="6.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="6.5">j<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>,</l>
						<l n="7" num="1.7"><space unit="char" quantity="4"></space><w n="7.1">G<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="7.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>rs</w>, <w n="7.3">s<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="7.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.5">di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>.</l>
						<l n="8" num="1.8"><w n="8.1">Fl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="8.2">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="8.3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w>, <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="8.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="8.6">d</w>'<w n="8.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="9" num="1.9"><w n="9.1">L</w>'<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r</w> <w n="9.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.4">p<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w>, <w n="9.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ;</l>
						<l n="10" num="1.10"><w n="10.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="10.2">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>scr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>pt<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>, <w n="10.3">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>sf<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="11" num="1.11"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.2">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>f</w> <w n="11.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="11.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="11.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="11.6">l</w>'<w n="11.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="11.8">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="11.9">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="12" num="1.12"><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s</w> <w n="12.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>, <w n="12.5">j</w>'<w n="12.6"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="12.8">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.9">su<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="12.10"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ;</l>
						<l n="13" num="1.13"><space unit="char" quantity="4"></space><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="13.3">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>, <w n="13.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">su<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ;</l>
						<l n="14" num="1.14"><w n="14.1">D</w>'<w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="14.3">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.4"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="343">ue</seg>il</w> <w n="14.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.6">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="14.7"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="14.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>mm<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
						<l n="15" num="1.15"><space unit="char" quantity="4"></space><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s</w> <w n="15.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>, <w n="15.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">su<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="15.7"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
					</lg>
				</div></body></text></TEI>