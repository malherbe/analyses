<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANDRÉ LE CHANSONNIER</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="FTN" sort="1">
					<name>
						<forename>Louis Marie</forename>
						<surname>FONTAN</surname>
					</name>
					<date from="1801" to="1839">1801-1839</date>
				</author>
				<author key="DNY" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>DESNOYER</surname>
					</name>
					<date from="1806" to="1858">1806-1858</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>239 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>André le chansonnier.</title>
						<author>FONTAN ET DESNOYER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=aN6oyNpF3cMC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>André le chansonnier.</title>
								<author>FONTAN ET DESNOYER</author>
								<repository>Biblioteca Casanatense</repository>
								<idno type="URI">http://opac.casanatense.it/Record.htm?idlist=3</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Ode et Wodon</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-15" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE I</head><head type="main_subpart">SCÈNE XIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FED9">
				<head type="tune">AIR de Missolonghi. (Ballade du premier acte.)</head>
					<lg n="1">
						<head type="main">ANDRÉ.</head>
						<l n="1" num="1.1"><space unit="char" quantity="4"></space><w n="1.1">V<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="2"></space><w n="2.1">L</w>'<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="169">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="2.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="2.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> ?</l>
					</lg>
					<p>(Les soldats répètent.)</p>
					<lg n="2">
						<l n="3" num="2.1"><space unit="char" quantity="4"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="3.1">V<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>, </add></subst></l>
						<l n="4" num="2.2"><space unit="char" quantity="2"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="4.1">L</w>'<w n="4.2"><seg phoneme="e" type="vs" value="1" rule="169">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> ? </add></subst></l>
					</lg>
					<lg n="3">
						<head type="main">ANDRÉ.</head>
						<l n="5" num="3.1"><space unit="char" quantity="2"></space><w n="5.1">J<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="5.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="5.3">lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="5.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="5.5">d</w>'<w n="5.6"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="5.7">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l part="I" n="6" num="3.2"><space unit="char" quantity="2"></space><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="6.2">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>. </l>
					</lg>
					<lg n="4">
						<head type="main">LES SOLDATS.</head>
						<l part="F" n="6"><space unit="char" quantity="2"></space><w n="6.4">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> !</l>
					</lg>
					<lg n="5">
						<head type="main">ANDRÉ.</head>
						<l n="7" num="5.1"><w n="7.1">Qu</w>'<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="7.3">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.5">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="5.2"><w n="8.1">C<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="8.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="8.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>str<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="9" num="5.3"><space unit="char" quantity="2"></space><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="9.2">gl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">d</w>'<w n="9.4"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rs</w> <w n="9.5">t<seg phoneme="i" type="vs" value="1" rule="492">y</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> !</l>
						<l n="10" num="5.4"><space unit="char" quantity="2"></space><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="10.2">pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.4">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="10.5">l</w>'<w n="10.6"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>x<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="11" num="5.5"><space unit="char" quantity="2"></space><w n="11.1">S</w>'<w n="11.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="11.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="12" num="5.6"><space unit="char" quantity="2"></space><w n="12.1">S<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w>, <w n="12.2"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>-<w n="12.3">lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="12.4">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="12.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>gs</w> !</l>
						<l n="13" num="5.7"><space unit="char" quantity="6"></space><w n="13.1">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="13.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="13.4">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="5.8"><space unit="char" quantity="6"></space><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>scr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> ;</l>
						<l n="15" num="5.9"><space unit="char" quantity="6"></space><w n="15.1">Gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="15.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="15.4">t</w>'<w n="15.5"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="16" num="5.10"><space unit="char" quantity="8"></space><w n="16.1">P<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>x</w> <w n="16.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="16.3">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>scr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> !</l>
					</lg>
					</div></body></text></TEI>