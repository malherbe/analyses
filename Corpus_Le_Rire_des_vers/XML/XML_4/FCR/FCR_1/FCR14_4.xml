<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA MÉTEMPSYCOSE</title>
				<title type="sub">BÊTISE EN UN ACTE, MÊLÉE DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="COU" sort="1">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<author key="JAI" sort="2">
					<name>
						<forename>Ernest</forename>
						<surname>JAIME</surname>
					</name>
					<date from="1804" to="1884">1804-1884</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>282 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FCR_1</idno>	
				<availability status="free">
					<licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA MÉTEMPSYCOSE</title>
						<author>FRÉDÉRIC DE COURCY ET JAIME</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=c1RoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032819917.0x000001# ?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">7 FÉVRIER 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FCR14">
	<head type="tune">AIR : Vaudeville de l'Ours et le Pacha.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.2">pe<seg phoneme="i" type="vs" value="1" rule="496">i</seg>nʼs</w> <w n="1.3">d<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>q<seg phoneme="y" type="vs" value="1" rule="462">u</seg>ʼs</w> <w n="1.4">j</w>'<w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="1.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt</w>,</l>
		<l n="2" num="1.2"><w n="2.1">F<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="2.2">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="2.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="3" num="1.3"><w n="3.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w>, <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="3.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>s</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">s</w>' <w n="3.6">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="3.7">b<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w>,</l>
		<l n="4" num="1.4"><w n="4.1">C</w>'<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="4.3"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.5">b<seg phoneme="ɛ̃" type="vs" value="1" rule="220">en</seg></w> <w n="4.6">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>mm<seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
		<l n="5" num="1.5"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w> <w n="5.4">n</w>'<w n="5.5"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.7">qu</w>'<w n="5.8"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="5.9">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>,</l>
		<l n="6" num="1.6"><w n="6.1">L</w>'<w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.3">s</w>' <w n="6.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t</w> <w n="6.5">d</w>' <w n="6.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.7">g<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr</w>', <w n="6.8">l</w>'<w n="6.9"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr</w>' <w n="6.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.11">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.12">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="7" num="1.7"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>tr</w>'<w n="7.2"><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="7.3">c</w>'<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="7.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="7.6">qu<seg phoneme="ø" type="vs" value="1" rule="404">eu</seg>qu</w>' <w n="7.7">b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>sb<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
		<l n="8" num="1.8"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="8.2">c</w>' <w n="8.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="8.5">n</w>'<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="8.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="8.8">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w>' <w n="8.9">d</w>'<w n="8.10"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w></l>
		<l n="9" num="1.9"><w n="9.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="9.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="9.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.5">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
	</lg>
</div></body></text></TEI>