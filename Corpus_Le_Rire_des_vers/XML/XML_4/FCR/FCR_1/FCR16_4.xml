<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA MÉTEMPSYCOSE</title>
				<title type="sub">BÊTISE EN UN ACTE, MÊLÉE DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="COU" sort="1">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<author key="JAI" sort="2">
					<name>
						<forename>Ernest</forename>
						<surname>JAIME</surname>
					</name>
					<date from="1804" to="1884">1804-1884</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>282 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FCR_1</idno>	
				<availability status="free">
					<licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA MÉTEMPSYCOSE</title>
						<author>FRÉDÉRIC DE COURCY ET JAIME</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=c1RoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032819917.0x000001# ?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">7 FÉVRIER 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FCR16">
	<head type="tune">AIR de Plantade.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Vʼl<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="1.2">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">j</w>' <w n="1.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch</w>' <w n="1.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="1.7">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.8">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="2" num="1.2"><w n="2.1">C</w>' <w n="2.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">j</w>' <w n="2.4">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="2.5">t</w>'<w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="2.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.9">tr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ;</l>
		<l n="3" num="1.3"><w n="3.1">Ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.2">m</w>'<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">dʼv<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="3.7">b<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="4" num="1.4"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="4.2">j</w>'<w n="4.3">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.5">b<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tʼs</w> <w n="4.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">l</w>'<w n="4.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="4.9">n</w>' <w n="4.10">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w>' <w n="4.11">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>.</l>
		<l n="5" num="1.5"><w n="5.1">J</w>'<w n="5.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="5.3">b<seg phoneme="ɛ̃" type="vs" value="1" rule="220">en</seg></w> <w n="5.4">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr</w>' <w n="5.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.7">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vi<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="6" num="1.6"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="6.2">p<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.3">c</w>'<w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="6.5">qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg>qu</w>'<w n="6.6">f<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="6.7">tr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w>—<w n="6.8">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> ;</l>
		<l n="7" num="1.7"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="7.2">ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.3">n</w>'<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="7.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="7.7">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ni<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="8" num="1.8"><w n="8.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="8.2">s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="8.3">qu</w>' <w n="8.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">n</w>' <w n="8.6">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="8.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="8.8">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="8.9">l</w>'<w n="8.10"><seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w>…</l>
	</lg>
	<p>
		Quoique ça, ça serait encore un fameux moyen pour vivre<lb></lb>
		long-teinps, que de se mettre goujon… Nous v'là goujons<lb></lb>
		tous les deux : nous nous en allons en nous promenant tout<lb></lb>
		du long ; il arrive un grand inalin avec son hameçon, il<lb></lb>
		jette ça, floc ! Mais un instant, nous autres qu'a pêché dans<lb></lb>
		not' temps, nous ne donnons pas dans la couleur, deini-tour<lb></lb>
		à droite, et enfoncé le marin.<lb></lb> 
	</p>
	<lg n="2">
	<head type="speaker">ENSEMBLE.</head>
		<l n="9" num="2.1"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="9.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.3">Di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>, <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.5">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.6">ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="10" num="2.2"><w n="10.1">Qu</w>' <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.3">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>trentʼs<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="10.4">ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
		<l n="11" num="2.3"><w n="11.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="11.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w></l>
		<l n="12" num="2.4"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w></l>
		<l n="13" num="2.5"><w n="13.1">Pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>squ</w>'<w n="13.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="13.3">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="13.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> !</l>
	</lg>
	<lg n="3">
	<head type="speaker">ÉTIENNE.</head>
		<l n="14" num="3.1"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="14.2">cʼt</w> <w n="14.3">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>' <w n="14.4">c</w>'<w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="14.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w>-<w n="14.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w>, <w n="14.8">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="14.9">j</w>' <w n="14.10">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>pp<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="15" num="3.2"><w n="15.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="15.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="15.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="15.4">m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rph<seg phoneme="o" type="vs" value="1" rule="443">o</seg>sʼr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>,</l>
		<l n="16" num="3.3"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="16.2">s</w>'<w n="16.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="16.4">d</w>' <w n="16.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="16.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w>' <w n="16.7">ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="17" num="3.4"><w n="17.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="17.2">c</w>' <w n="17.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="17.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="17.6">rʼc<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>.</l>
		<l n="18" num="3.5"><w n="18.1">J</w>' <w n="18.2">n</w>'<w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="18.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="18.5">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="18.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="18.7">n<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="18.8">p<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r</w>', <w n="18.9">n<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="18.10">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="19" num="3.6"><w n="19.1">J</w>'<w n="19.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="19.3">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="19.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="19.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>cl</w>' <w n="19.6">R<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="i" type="vs" value="1" rule="492">y</seg></w>,</l>
		<l n="20" num="3.7"><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">n</w>' <w n="20.3">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="20.4">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="20.5">rʼv<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="20.6">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="20.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.8">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="21" num="3.8"><w n="21.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="21.2">j</w>' <w n="21.3">n</w>'<w n="21.4"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="21.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr</w>' <w n="21.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="21.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="21.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>…</l>
	</lg>
	<p>
		Te v'là, loi ! Christophe ? tu me taquines, mais c'est<lb></lb>
		tout de même, moi je t'estime.<lb></lb>
		CHRISTOPHE.<lb></lb>
		Tu n'estimes ! moi, je te considère…,<lb></lb> 
		ÉTIENNE.<lb></lb> 
		Écoute, il faudra inventer un geste : quand tu voudras<lb></lb>
		voir si c'est moi, tu mettras ta patte dans la mienne.<lb></lb> 
		CHRISTOPHE.<lb></lb>
		Je ne peux pas : tu n'as qu'à être une fourmi et que je<lb></lb>
		soye un éléphant, si je mets ma patte dans la tienne, ça<lb></lb>
		pourrait t'inconmoder. Non, écoute voir : tu me montes<lb></lb>
		sur le dos, tu me piques où ce que tu veux ; je sais que<lb></lb>
		c'est toi, et en avant la reconnaissance ! …<lb></lb> 
	</p>
	<lg n="4">
	<head type="speaker">ENSEMBLE.</head>
		<l n="22" num="4.1"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="22.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="22.3">Di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>, <w n="22.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="22.5">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.6">ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="23" num="4.2"><w n="23.1">Qu</w>' <w n="23.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.3">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>' <w n="23.4">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="23.5">ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
		<l n="24" num="4.3"><w n="24.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="24.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w></l>
		<l n="25" num="4.4"><w n="25.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.2">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w></l>
		<l n="26" num="4.5"><w n="26.1">Pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>squ</w>'<w n="26.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="26.3">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="26.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="26.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> !</l>
	</lg>
</div></body></text></TEI>