<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L'HOMME QUI BAT SA FEMME</title>
				<title type="sub">TABLEAU POPULAIRE EN UN ACTE, MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DML" sort="1">
				  <name>
					<forename>Julien</forename>
					<nameLink>de</nameLink>
					<surname>MAILLAN</surname>
					<addname type="other">Julien</addname>
					<addname type="other">Julien de M.</addname>
				  </name>
				  <date from="1805" to="1851">1805-1851</date>
				</author>
				<author key="DNR" sort="2">
				  <name>
					<forename>Philippe-François</forename>
					<surname>PINEL</surname>
					<addname type="pen_name">DUMANOIR</addname>
					<addname type="other">Philippe Dumanoir</addname>
					<addname type="other">Philippe D.</addname>
					<addname type="other">Philippe D***</addname>
				  </name>
				  <date from="1806" to="1865">1806-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DMP_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
          <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L'HOMME QUI BAT SA FEMME</title>
						<author>JULIEN DE MALLIAN ET PHILIPPE DUMANOIR</author>
					</titleStmt>
					<publicationStmt>
						<publisher>INTERNET ARCHIVE</publisher>
						<idno type="URL">https ://archive.org/details/lhommequibatsafe00malluoft</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>University of Toronto Libraries</repository>
								<idno type="URL">https ://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991105986034006196</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">8 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DMP11">
	<head type="tune">AIR : des Coups d'poings.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.2">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ps</w> <w n="1.3">d</w>'<w n="1.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>g</w> ! <del reason="analysis" type="repetition" hand="KL">(bis.)</del></l>
		<l n="2" num="1.2"><w n="2.1">Qu</w>'<w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.3">s</w>'<w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="3.2">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="3.3">pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
		<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.2">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ps</w> <w n="4.3">d</w>'<w n="4.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>g</w> !</l>
		<l n="5" num="1.5"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>qu<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="5.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t</w> ! <del reason="analysis" type="repetition" hand="KL">(bis.)</del></l>
		<l n="6" num="1.6"><w n="6.1">P<seg phoneme="i" type="vs" value="1" rule="467">i</seg>f</w> ! <w n="6.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg></w> !<del reason="analysis" type="repetition" hand="KL">(bis.)</del></l>
		<l n="7" num="1.7"><w n="7.1">Fr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="7.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="7.3">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> !</l>
		<l n="8" num="1.8"><w n="8.1">P<seg phoneme="i" type="vs" value="1" rule="467">i</seg>f</w> ! <w n="8.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg></w> ! <del reason="analysis" type="repetition" hand="KL">(bis.)</del></l>
		<l n="9" num="1.9"><w n="9.1">Ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.2">v<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">l</w>'<w n="9.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="9.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> !</l>
	</lg>
	<lg n="2">
	<head type="speaker">BARBEAU, à part.</head>
		<l n="10" num="2.1"><w n="10.1">Di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> ! <w n="10.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>-<w n="10.4">t</w>-<w n="10.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="10.6">m</w>'<w n="10.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ?</l>
		<l n="11" num="2.2"><w n="11.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="11.2">m<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="362">en</seg></w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">m</w>'<w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>squ<seg phoneme="i" type="vs" value="1" rule="490">i</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ;</l>
		<l n="12" num="2.3"><w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="411">Ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.2">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>. <del reason="analysis" type="repetition" hand="KL">(bis.)</del></l>
		<l n="13" num="2.4"><w n="13.1">M<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="13.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>.</l>
	</lg> 
</div></body></text></TEI>