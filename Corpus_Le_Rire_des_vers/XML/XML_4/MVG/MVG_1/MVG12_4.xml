<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER TABLEAU.</head><head type="main_subpart">SCÈNE VI.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG12">
						<head type="tune">AIR : du Hussard de Felsheim,</head>
						<lg n="1">
							<head type="main">MOREL.</head>
							<l n="1" num="1.1"><space unit="char" quantity="2"></space><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="1.2">c<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rps</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="1.4">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="1.7">g<seg phoneme="o" type="vs" value="1" rule="317">au</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
							<l n="2" num="1.2"><space unit="char" quantity="2"></space><w n="2.1">V<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="2.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="2.4">pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>.</l>
						</lg>
						<lg n="2">
							<head type="main">BONAPARTE.</head>
							<l n="3" num="2.1"><space unit="char" quantity="2"></space><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="3.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">l</w>'<w n="3.4"><seg phoneme="e" type="vs" value="1" rule="169">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="3.5">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
							<l n="4" num="2.2"><space unit="char" quantity="2"></space><w n="4.1">V<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="4.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="4.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="4.4">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>…</l>
						</lg>
						<p>(Ici le tambour et la trompette se font entendre à l'orchestre pendant que Bonaparte plante des épingles sur la carte.)</p>
						<lg n="3">
							<l n="5" num="3.1"><space unit="char" quantity="2"></space><w n="5.1">Pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="5.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="6" num="3.2"><space unit="char" quantity="2"></space><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="6.2">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="6.3">pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="6.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w></l>
							<l n="7" num="3.3"><space unit="char" quantity="2"></space><w n="7.1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gt</w> <w n="7.2">pi<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.3">d</w>'<w n="7.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="8" num="3.4"><space unit="char" quantity="2"></space><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="8.2">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>dr<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.3">l</w>'<w n="8.4"><seg phoneme="e" type="vs" value="1" rule="169">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>.</l>
						</lg>
						<div n="1" type="section">
							<head type="main">ENSEMBLE.</head>
							<lg n="1">
								<head type="main">MOREL, à part.</head>
								<l n="9" num="1.1"><space unit="char" quantity="10"></space><w n="9.1">C</w>'<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="9.3">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> ! <del hand="LG" type="repetition" reason="analysis">(bis.)</del></l>
								<l n="10" num="1.2"><space unit="char" quantity="10"></space><subst hand="LG" type="repetition" reason="analysis"><del></del><add rend="hidden"><w n="10.1">C</w>'<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="10.3">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> !</add></subst></l>
								<l n="11" num="1.3"><w n="11.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="11.2">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>-<w n="11.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="11.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="11.5">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> !</l>
							</lg>
							<lg n="2">
								<head type="main">BONAPARTE</head>
								<l n="12" num="2.1"><space unit="char" quantity="10"></space><w n="12.1">C</w>'<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="12.3">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> ! <del hand="LG" type="repetition" reason="analysis">(bis.)</del></l>
								<l n="13" num="2.2"><space unit="char" quantity="10"></space><subst hand="LG" type="repetition" reason="analysis"><del></del><add rend="hidden"><w n="13.1">C</w>'<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="13.3">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> !</add></subst></l>
								<l n="14" num="2.3"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="14.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> <w n="14.3">n</w>'<w n="14.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>-<w n="14.5">j<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="14.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="14.7">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> !</l>
							</lg>
						</div>
						<lg n="4">
							<head type="main">BONAPARTE.</head>
							<l n="15" num="4.1"><space unit="char" quantity="2"></space><w n="15.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="15.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="310">an</seg>t</w> <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.4">r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="16" num="4.2"><space unit="char" quantity="2"></space><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="16.2">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.5">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rge<seg phoneme="ɑ̃" type="vs" value="1" rule="310">an</seg>t</w></l>
							<l n="17" num="4.3"><space unit="char" quantity="2"></space><w n="17.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="17.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="17.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="18" num="4.4"><space unit="char" quantity="2"></space><w n="18.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="18.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="18.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="18.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>.</l>
						</lg>
						<p>(Même jeu. Bonaparte s'anime davantage en finissant ce couplet.)</p>
						<lg n="5">
							<l n="19" num="5.1"><space unit="char" quantity="2"></space><w n="19.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rn<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="19.2">l</w>'<w n="19.3"><seg phoneme="e" type="vs" value="1" rule="169">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="19.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="19.5">r<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="20" num="5.2"><space unit="char" quantity="2"></space><w n="20.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="20.2">qu</w>'<w n="20.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="20.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="20.6">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>,</l>
							<l n="21" num="5.3"><space unit="char" quantity="2"></space><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="21.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rsu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="21.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="21.5">s</w>'<w n="21.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="22" num="5.4"><space unit="char" quantity="2"></space><w n="22.1">J</w>'<w n="22.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="22.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="22.4">R<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>sb<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch</w> <w n="22.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="22.6">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> !</l>
						</lg>
						<div n="2" type="section">
							<head type="main">ENSEMBLE.</head>
							<lg n="1">
								<head type="main">MOREL, à part, avec étonnement.</head>
								<l n="23" num="1.1"><space unit="char" quantity="8"></space><w n="23.1">C</w>'<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="23.3">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> ! (<w n="23.4">b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>.)</l>
								<l n="24" num="1.2"><space unit="char" quantity="10"></space><subst hand="LG" type="repetition" reason="analysis"><del></del><add rend="hidden"><w n="24.1">C</w>'<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="24.3">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> !</add></subst></l>
								<l n="25" num="1.3"><w n="25.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="25.2">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>-<w n="25.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="25.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="25.5">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> !</l>
							</lg>
							<lg n="2">
								<head type="main">BONAPARTE</head>
								<l n="26" num="2.1"><space unit="char" quantity="8"></space><w n="26.1">C</w>'<w n="26.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="26.3">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> ! (<w n="26.4">b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>.)</l>
								<l n="27" num="2.2"><space unit="char" quantity="10"></space><subst hand="LG" type="repetition" reason="analysis"><del></del><add rend="hidden"><w n="27.1">C</w>'<w n="27.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="27.3">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> !</add></subst></l>
								<l n="28" num="2.3"><w n="28.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="28.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> <w n="28.3">n</w>'<w n="28.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>-<w n="28.5">j<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="28.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="28.7">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> !</l>
							</lg>
						</div>
						<lg n="6">
							<head type="main">BONAPARTE.</head>
							<l n="29" num="6.1"><space unit="char" quantity="2"></space><w n="29.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="29.2">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="29.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rgn<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="30" num="6.2"><space unit="char" quantity="2"></space><w n="30.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="30.2"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="30.4">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="30.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w>,</l>
							<l n="31" num="6.3"><space unit="char" quantity="2"></space><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="31.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="31.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="31.5">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> …</l>
						</lg>
						<lg n="7">
							<head type="main">JOSÉPHINE, montrant une épingle.</head>
							<l n="32" num="7.1"><space unit="char" quantity="2"></space><w n="32.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.2">n</w>'<w n="32.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="32.4">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="32.5">qu</w>'<w n="32.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="32.7">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w> <w n="32.8">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> !</l>
						</lg>
						<lg n="8">
							<head type="main">BONAPARTE.</head>
							<l n="33" num="8.1"><space unit="char" quantity="2"></space><w n="33.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="33.2">l</w>'<w n="33.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="33.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="33.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.6">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="34" num="8.2"><space unit="char" quantity="2"></space><w n="34.1">J</w>'<w n="34.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="34.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="34.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="34.5">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="34.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> …</l>
							<l n="35" num="8.3"><space unit="char" quantity="2"></space><w n="35.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="35.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="35.3">d</w>'<w n="35.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="35.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="36" num="8.4"><space unit="char" quantity="2"></space><w n="36.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="36.2">j</w>'<w n="36.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="36.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="36.5">B<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rl<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> !</l>
						</lg>
						<div n="3" type="section">
							<head type="main">ENSEMBLE.</head>
							<lg n="1">
								<head type="main"></head>
								<l n="37" num="1.1"><space unit="char" quantity="8"></space><w n="37.1">C</w>'<w n="37.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="37.3">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> ! (<w n="37.4">b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>.)</l>
								<l n="38" num="1.2"><space unit="char" quantity="10"></space><subst hand="LG" type="repetition" reason="analysis"><del></del><add rend="hidden"><w n="38.1">C</w>'<w n="38.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="38.3">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> !</add></subst></l>
								<l n="39" num="1.3"><w n="39.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="39.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> <w n="39.3">n</w>'<w n="39.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>-<w n="39.5">j<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="39.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="39.7">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> !</l>
							</lg>
							<lg n="2">
								<head type="main">MOREL.</head>
								<l n="40" num="2.1"><space unit="char" quantity="8"></space><w n="40.1">C</w>'<w n="40.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="40.3">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> ! (<w n="40.4">b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>.)</l>
								<l n="41" num="2.2"><space unit="char" quantity="10"></space><subst hand="LG" type="repetition" reason="analysis"><del></del><add rend="hidden"><w n="41.1">C</w>'<w n="41.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="41.3">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> !</add></subst></l>
								<l n="42" num="2.3"><w n="42.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="42.2">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>-<w n="42.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="42.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="42.5">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> !</l>
							</lg>
						</div>
					</div></body></text></TEI>