<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME TABLEAU.</head><head type="main_subpart">SCÈNE XIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG17">
						<head type="tune">AIR : d'Aristippe.</head>
						<lg n="1">
							<head type="main">BONAPARTE.</head>
							<l n="1" num="1.1"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="1.4">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="1.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199">om</seg></w> <w n="1.7">qu</w>'<w n="1.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.9">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.10">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">Ou<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.6">n<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="3.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w>, <w n="3.4">fr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.6">t<seg phoneme="i" type="vs" value="1" rule="492">y</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>nn<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">L</w>'<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="4.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w></l>
							<l n="5" num="1.5"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="5.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="5.4">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>g</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>,</l>
							<l n="6" num="1.6"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="6.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ds</w>, <w n="6.4">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="6.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>lqu</w>'<w n="6.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.8">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.9">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="7" num="1.7"><space unit="char" quantity="4"></space><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="7.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
							<l n="8" num="1.8"><space unit="char" quantity="4"></space><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s</w> <w n="8.3">m<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.6">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="9" num="1.9"><space unit="char" quantity="4"></space><w n="9.1">M<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>. <del type="repetition" hand="LG" reason="analysis">(bis.)</del></l>
							<l n="10" num="1.10"><space unit="char" quantity="4"></space><subst type="repetition" hand="LG" reason="analysis"><del></del><add rend="hidden"><w n="10.1">M<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>.</add></subst></l>
						</lg>
					</div></body></text></TEI>