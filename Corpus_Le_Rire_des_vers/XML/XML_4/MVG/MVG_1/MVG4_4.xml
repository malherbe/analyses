<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER TABLEAU.</head><head type="main_subpart">SCÈNE III.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG4">
						<head type="tune">AIR : Restez, restez, troupe jolie.</head>
						<lg n="1">
							<head type="main">DARBEL.</head>
							<l n="1" num="1.1"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="1.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="1.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.6">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="2" num="1.2"><w n="2.1">C<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.3">j<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="2.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="2.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="2.6">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="3.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="3.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="3.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="4" num="1.4"><w n="4.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="4.3">m<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w>, <w n="4.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="4.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="4.7">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w>,</l>
							<l n="5" num="1.5"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="5.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="5.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="5.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="5.5">m<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="5.7">g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w>.</l>
							<l n="6" num="1.6"><w n="6.1">S<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.3">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="6.5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>s</w> <w n="6.6">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="7" num="1.7"><w n="7.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="7.3">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="7.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.5">g<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
							<l n="8" num="1.8"><w n="8.1">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="8.3">qu</w>'<w n="8.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="8.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="9" num="1.9"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="9.2">j<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>r</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.5">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
						</lg>
						<lg n="2">
							<head type="main">TOUS.</head>
							<l n="10" num="2.1"><w n="10.1">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="10.3">qu</w>'<w n="10.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="10.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="11" num="2.2"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="11.2">j<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>r</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.5">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
						</lg>
					</div></body></text></TEI>