<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONARDIN DANS LA LUNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="HON">
					<name>
						<forename>Charles-Honoré</forename>
						<surname>RÉMY</surname>
						<addName type="pen_name">HONORÉ</addName>
					</name>
					<date from="1793" to="1858">1793-1858</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>16 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HON_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>BONARDIN DANS LA LUNE, OU LA MONOMANIE ASTRONOMIQUE</title>
						<author>HONORÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=-DFMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>BONARDIN DANS LA LUNE, OU LA MONOMANIE ASTRONOMIQUE</title>
								<author>HONORÉ</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160357301</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les lettres ligaturées (œ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DIVERTISSEMENT.</head><head type="main_subpart">OBSERVATION.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="HON2">
					<head type="tune">AIR :</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>'<w n="1.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>, <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="1.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="1.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.6">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="2.2">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="2.4">fr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>.</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="3.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="3.4">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="3.6">v<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> <w n="4.4">qu</w>'<w n="4.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="4.6">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="4.7">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="5.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="1.6"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="6.2">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="6.3">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="6.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>,</l>
						<l n="7" num="1.7"><w n="7.1">M<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ssi<seg phoneme="ø" type="vs" value="1" rule="396">eu</seg>rs</w>, <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="7.3">lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="7.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>,</l>
						<l n="8" num="1.8"><w n="8.1">L</w>'<w n="8.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>lg<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>