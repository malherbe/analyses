<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE LIEUTENANT D'ARTILLERIE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="NSL" sort="2">
					<name>
						<forename>Jean-Pierre-Laurent</forename>
						<surname>DENOMBRET</surname>
						<addName type="pen_name">Charles NOMBRET SAINT-LAURENT</addName>
					</name>
					<date from="1791" to="1833">1791-1833</date>
				</author>
					<author key="DUV" sort="3">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>225 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SND_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=NWU:35556007830409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
								<repository>Northwestern university</repository>
								<idno type="URI">https://babel.hathitrust.org/cgi/pt?id=ien.35556007830409</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE VIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SND10">
				<head type="tune">AIR : De votre bonté généreuse.</head>
				<lg n="1">
					<head type="main">DELAUNAY, avec exaltation.</head>
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="1.2">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w>, <w n="1.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="1.6">qu</w>'<w n="1.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="1.8">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="1.9">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.10">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="2.2">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="2.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">m<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>ti<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Ou<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">su<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="3.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="4.2">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="4.3">M<seg phoneme="œ" type="vs" value="1" rule="150">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396">eu</seg>r</w>, <w n="4.4">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
				</lg>
				<p>(Il lui tend la main ; Bonaparte la reçoit en la serrant avec cordialité.)</p>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="5.2"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="343">ue</seg>il</w>, <w n="5.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="5.5">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="5.7">fi<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="2.2"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.2">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>g<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="6.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w>.</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="7.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rds</w> <w n="7.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="7.5">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.7">l<seg phoneme="y" type="vs" value="1" rule="452">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="8.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="8.3">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="8.4">d</w>'<w n="8.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.7">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w>.</l>
				</lg>
			</div></body></text></TEI>