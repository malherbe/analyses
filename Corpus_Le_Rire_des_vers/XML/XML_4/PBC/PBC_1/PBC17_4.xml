<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC17">
	<head type="tune">AIR : Je suis la petite bergère.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="1.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">j</w>'<w n="1.4">f<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm</w>'<w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.6">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rg<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="2.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="2.4">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="2.5">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w> ?</l>
  </lg>
	<lg n="2">
	<head type="speaker">LE FERMIER.</head>
		<l n="3" num="2.1"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.2">f<seg phoneme="a" type="vs" value="1" rule="192">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="3.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="3.6">l</w>'<w n="3.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="4" num="2.2"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="4.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pt</w>' <w n="4.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.4">b<seg phoneme="ø" type="vs" value="1" rule="246">œu</seg>fs</w>, <w n="4.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.6">br<seg phoneme="e" type="vs" value="1" rule="408">é</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="4.7">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.8">t<seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w>.</l>
		<l n="5" num="2.3"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="5.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="5.3">b<seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="a" type="vs" value="1" rule="306">a</seg>il</w> <w n="5.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.5">s<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>s</w> <w n="5.6">n</w>'<w n="5.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="5.8">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t</w> <w n="5.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.10">b<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
		<l n="6" num="2.4"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="6.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="6.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>t</w>.</l>
		<l n="7" num="2.5"><w n="7.1">S</w>'<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="7.3">lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="7.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="7.5">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>lʼm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="7.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w>'<w n="7.7">b<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="7.9">c<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
		<l n="8" num="2.6"><w n="8.1">C</w>'<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="8.4">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="8.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll</w>' <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.7">rʼd<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>dʼr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>.<del reason="analysis" type="repetition" hand="KL">(bis.)</del></l>
  </lg>
</div></body></text></TEI>