<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC39">
	<head type="tune">AIR : Pour un curé patriote.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="1.2">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="1.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>pr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
		<l n="2" num="1.2"><w n="2.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="2.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w> <w n="2.4">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="2.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w></l>
		<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.2">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="3.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w>-<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="3.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="3.6">n<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
		<l n="4" num="1.4"><w n="4.1">S</w>'<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="4.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="4.6">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w>-<w n="4.8">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>.</l>
		<l n="5" num="1.5"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="5.2">l</w>'<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>str<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>,</l>
		<l n="6" num="1.6"><w n="6.1">Ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.2">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> :</l>
		<l n="7" num="1.7"><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="7.2">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>,</l>
		<l n="8" num="1.8"><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="8.2">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w></l>
		<l n="9" num="1.9"><w n="9.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="9.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r</w> <w n="9.4">ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>.</l> 
	</lg>
	<lg n="2">
		<l n="10" num="2.1"><w n="10.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="10.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="10.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="11" num="2.2"><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="11.2">v<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="11.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="11.5">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="320">y</seg>s</w></l>
		<l n="12" num="2.3"><w n="12.1">S</w>'<w n="12.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="12.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>il</w> <w n="12.4"><seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="13" num="2.4"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="13.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="13.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>,</l>
		<l n="14" num="2.5"><w n="14.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.2">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w></l>
		<l n="15" num="2.6"><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="15.3">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="15.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="15.5">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w>.</l>
		<l n="16" num="2.7"><w n="16.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="16.2">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>,</l>
		<l n="17" num="2.8"><w n="17.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="17.3">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w></l>
		<l n="18" num="2.9"><w n="18.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="18.3">qu</w>'<w n="18.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="18.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>,</l>
		<l n="19" num="2.10"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="19.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="19.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>g<seg phoneme="y" type="vs" value="1" rule="447">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.4">qu</w>'<w n="19.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="19.6">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> !</l>
	</lg>
</div></body></text></TEI>