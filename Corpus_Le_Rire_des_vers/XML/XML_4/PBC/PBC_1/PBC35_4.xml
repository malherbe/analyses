<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC35">
	<head type="tune">AIR : du petit Courrier.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="1.2">v<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="1.4">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.5">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> ;</l>
		<l n="2" num="1.2"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg>h</w> ! <w n="2.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">j</w>'<w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.5">c<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
	</lg>
	<lg n="2">
	<head type="speaker">SERPENTINE.</head>
		<l n="3" num="2.1"><w n="3.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="3.2">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="3.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="3.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.6">m<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="4" num="2.2"><w n="4.1">S<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="4.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w>.</l>
		<l n="5" num="2.3"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="5.3">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="5.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="5.8">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="6" num="2.4"><w n="6.1">C</w>'<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="6.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.5">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rri<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="6.6">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> ;</l>
		<l n="7" num="2.5"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="7.2">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.3">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="7.4">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.7">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="8" num="2.6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="8.2">n</w>'<w n="8.3"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w>' <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="8.8">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>.</l>
	</lg>
</div></body></text></TEI>