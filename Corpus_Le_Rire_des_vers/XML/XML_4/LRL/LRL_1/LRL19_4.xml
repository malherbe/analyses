<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL19">
	<head type="tune">AIR des Créoles.</head>
	<div type="section" n="1">
		<lg n="1">
			<l n="1" num="1.1"><w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.2">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
			<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rgn<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="2.2">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="2.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.4">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w> <w n="2.5">y<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>,</l>
			<l n="3" num="1.3"><w n="3.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="3.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg></w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
			<l n="4" num="1.4"><w n="4.1">J</w>'<w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="4.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> !</l>
			<l n="5" num="1.5"><w n="5.1">S<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="5.2"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">r<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">cr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
			<l n="6" num="1.6"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="6.2">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w> <w n="6.3">j</w>'<w n="6.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>, <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>,</l>
			<l n="7" num="1.7"><w n="7.1">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.2">f<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">t<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">gu<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
			<l n="8" num="1.8"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="8.2">t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="8.5">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>gts</w>.</l>
			<l n="9" num="1.9"><w n="9.1">Ou<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.4">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>, <w n="9.5">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.6">t<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.8">gu<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
			<l n="10" num="1.10"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">j</w>'<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="10.4">t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.6">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.7">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>gts</w>.</l>
		</lg>
	</div>
	<div type="section" n="2">
		<head type="main">SCENE XII.</head>
		<head type="sub">LUDOVIC, MADAME MORIN, grosse maman de bonne mine, <lb></lb>en costume de danseuse d'opéra.</head>
		<lg n="1">
			<head type="speaker">MADAME MORIN, entrant sur un pas de rephir.</head> 
			<head type="tune">Suite de l'air précédent.</head>
			<l n="11" num="1.1"><w n="11.1">M<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> ! …, <w n="11.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="11.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>,</l>
			<l n="12" num="1.2"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.2">Gr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="12.5">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> :</l>
			<l n="13" num="1.3"><w n="13.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="13.4">l</w>'<w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.7">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.8"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
			<l n="14" num="1.4"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w> <w n="14.4">c<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.5">d</w>'<w n="14.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> !</l>
			<l n="15" num="1.5"><w n="15.1">Ou<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w> <w n="15.4">c<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.5">d</w>'<w n="15.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="15.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> ! <del reason="analysis" type="repetition" hand="KL">( bis)</del></l>
	  </lg>
	</div>
</div></body></text></TEI>