<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL11">
	<head type="tune">AIR de Fanchon.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="1.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>, <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="1.4">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="1.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
		<l n="2" num="1.2"><w n="2.1">D</w>'<w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
		<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="3.2">J<seg phoneme="o" type="vs" value="1" rule="443">o</seg>k<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w>,</l>
		<l n="4" num="1.4"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="4.2">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="4.3">T<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> ;</l>
		<l n="5" num="1.5"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">Fl<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="6" num="1.6"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>c</w>, <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>-<w n="6.7">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w>,</l>
		<l n="7" num="1.7"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="7.2">d</w>'<w n="7.3"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xc<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="8" num="1.8"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="8.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="8.4">Th<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> !</l>
	</lg>
</div></body></text></TEI>