<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL26">
	<head type="tune">AIR : Un corps à droite, un à gauche. ( de Bonaparte à Brienne.)</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">N<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="1.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="1.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
		<l n="2" num="1.2"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="2.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>, <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="2.5">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>, <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w>'<w n="2.7">hu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>,</l>
		<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">l</w>'<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="3.4">d</w>'<w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w>' <w n="3.6">s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="4.2">l</w>'<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="4.4">d</w>'<w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="4.6">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> !</l>
	</lg>
	<p>
		( Ici le chœur frappe sur les casseroles en chantant.) <lb></lb>
		Silence donc ! vous autres ! vous voyez bien que ces mes <lb></lb>
		sieurs ne se sont pas encore prononcés. (au public.) Messieurs… <lb></lb>
		(moment de silence. Il promène ses yeux dans la salle. Se tournant <lb></lb>
		-vers les charitariseurs.) allons, allons, bon espoir ! (au public.) <lb></lb>
	</p>
	<lg n="2">
		<l n="5" num="2.1"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">c</w>'<w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w>' <w n="5.7">s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="6" num="2.2"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>lg<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c</w>' <w n="6.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.5">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> ;</l>
		<l n="7" num="2.3"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>, <w n="7.2">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="7.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.4">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu</w>' <w n="7.5">m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>ss<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="8" num="2.4"><w n="8.1">Pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="8.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>t</w>…</l>
	</lg>
	<p>
		Malheur au récalcitrant, il ne sait pas à quoi il s'expose ! ce <lb></lb>
		soir… à minuit, sous ses fenêtres… 
	</p>
	<lg n="3">
		<l n="9" num="3.1"><w n="9.1">G<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="9.3">lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <del reason="analysis" type="repetition" hand="KL">(bis.)</del></l>
		<l n="10" num="3.2"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="10.3">s<seg phoneme="y" type="vs" value="1" rule="444">û</seg>r</w> <w n="10.4">d</w>'<w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="10.6">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> !</l> 
	</lg>
	<lg n="4">
	<head type="speaker">CHŒUR</head> 
		<l n="11" num="4.1"><w n="11.1">G<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="11.3">lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <del reason="analysis" type="repetition" hand="KL">( bis)</del></l>
		<l n="12" num="4.2"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="12.3">s<seg phoneme="y" type="vs" value="1" rule="444">û</seg>r</w> <w n="12.4">d</w>'<w n="12.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="12.6">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> !</l>
	</lg>
</div></body></text></TEI>