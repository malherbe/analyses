<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB56">
	<head type="tune">AIR d'Yelva.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="1.3">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="1.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>, <w n="1.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="1.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>, <w n="2.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="2.6">lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="2.7">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="2.8">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="u" type="vs" value="1" rule="424">oû</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>.</l>
		<l n="3" num="1.3"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">l</w>'<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="3.4">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="3.6">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>s</w>, <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="3.8">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.9">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l> 
	</lg>
	<lg n="2">
	<head type="speaker">RODOLPHE.</head>
		<l part="I" n="4" num="2.1"><w n="4.1">V<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… </l>
	</lg>
	<lg n="3">
	<head type="speaker">AUGUSTA.</head>
		<l part="F" n="4"><w n="4.3"><seg phoneme="e" type="vs" value="1" rule="132">E</seg>h</w> <w n="4.4">ou<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> ! <w n="4.5">vr<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>.</l>
		<l n="5" num="3.1"><w n="5.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.3">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="5.5">d</w>'<w n="5.6"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.7">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="5.8">d</w>'<w n="5.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="6" num="3.2"><w n="6.1">J</w>'<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="6.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="6.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>, <w n="6.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="6.6">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.7">v<seg phoneme="ø" type="vs" value="1" rule="247">œu</seg>x</w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>,</l>
		<l n="7" num="3.3"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.2">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ts</w>… <w n="7.3">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">n</w>'<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="7.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="7.7">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="7.8">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="7.9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="8" num="3.4"><w n="8.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="8.3">c<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="8.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="8.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="8.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ç<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>.</l>
		<l n="9" num="3.5"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="9.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.3">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ts</w> <w n="9.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">n</w>'<w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="9.8">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="9.9">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="9.10">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="10" num="3.6"><w n="10.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="10.3">c<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="10.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ç<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>.</l>
	</lg>
</div></body></text></TEI>