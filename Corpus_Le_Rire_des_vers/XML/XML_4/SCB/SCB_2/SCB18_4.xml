<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <fileDesc>
   <titleStmt>
	<title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
	<title type="corpus">Le Rire des vers</title>
	<author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
	</author>
	<author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
	</author>
	<editor>Le Rire des vers, Université de Bâle</editor>
	<editor>
	 Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
	 <choice>
	  <abbr>CRISCO, Université de Caen Normandie</abbr>
	  <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	 </choice>
	 (EA 4255)
	</editor>
	<respStmt>
	 <resp>Encodage en XML (CRISCO, université de Caen)</resp>
	 <name id="KL">
	  <forename>Kedi</forename>
	  <surname>LI</surname>
	 </name>
	</respStmt>
	<respStmt>
	 <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
	 <name id="RR">
	  <forename>Richard</forename>
	  <surname>RENAULT</surname>
	 </name>
	</respStmt>
   </titleStmt>
   <extent>140 vers</extent>
   <publicationStmt>
	<publisher>
	 <orgname>
	  <choice>
	   <abbr>CRISCO, Université de Caen Normandie</abbr>
	   <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	  </choice>
	 </orgname>
	 <address>
	  <addrLine>Université de Caen</addrLine>
	  <addrLine>14032 CAEN CEDEX</addrLine>
	  <addrLine>FRANCE</addrLine>
	 </address>
	 <email>crisco.incipit@unicaen.fr</email>
	 <ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
	</publisher>
	<pubPlace>Caen</pubPlace>
	<date when="2022">2022</date>
	<idno type="local">SCB_2</idno>	
	<availability status="free">
		<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
		<p>
			Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
			CC = Licence Creative Commons
			BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
			sur les sources et sur les modifications introduites.
			NC = Pas d’utilisation commerciale.
			SA = Partage dans les mêmes conditions.
		</p>
	</availability>
   </publicationStmt>
   <sourceDesc>
	<biblFull>
	 <titleStmt>
	  <title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	  <author>SCRIBE ET BAYARD</author>
	 </titleStmt>
	 <publicationStmt>
	  <publisher>INTERNET ARCHIVE</publisher>
	  <idno type="URL">https://archive.org/details/camillaoulasoeur00scriuoft</idno>
	 </publicationStmt>
	 <sourceDesc>
	  <biblStruct>
	   <monogr>
		<repository>University of Toronto Libraries</repository>
		<idno type="URL">https://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991106543695706196</idno>
	   </monogr>
	  </biblStruct>                 
	 </sourceDesc>
	</biblFull> 
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <creation>
	<date when="1832">12 DÉCEMBRE 1832</date>
	<placeName>
	 <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
	</placeName>
   </creation>
  </profileDesc>
  <encodingDesc>
   <projectDesc>
	<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
   </projectDesc>
   <samplingDecl>
	<p>Les parties versifiées ont été prioritairement encodées.</p>
   </samplingDecl>
   <editorialDecl>
	<normalization>
	 <p>Les majuscules accentuées ont été restituées.</p>
	 <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
	 <p>La ponctuation a été normalisée.</p> 
	 <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
	 <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
	 <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
	</normalization>
   </editorialDecl>
  </encodingDesc>
 </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB18">
	<head type="tune">Air : Venez, mon père, etc.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.2">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> ; <w n="1.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="1.5">p<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>-<w n="1.7">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w></l>
		<l n="2" num="1.2"><w n="2.1"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="2.2">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="2.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.5">t<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
		<stage>(Passant auprès de Ludworth.)</stage>
		<l n="3" num="1.3"><w n="3.1">M<seg phoneme="œ" type="vs" value="1" rule="150">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396">eu</seg>r</w>, <w n="3.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">th<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="3.5">qu</w>'<w n="3.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ppr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="4.5">t</w>-<w n="4.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="4.8">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> ?</l>
	</lg>
	<lg n="2">
	<head type="speaker">LUDWORTH, lui offrant la main.</head>
		<l n="5" num="2.1"><w n="5.1">C</w>'<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="5.3">tr<seg phoneme="o" type="vs" value="1" rule="432">o</seg>p</w> <w n="5.4">d</w>'<w n="5.5">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>, <w n="5.6">tr<seg phoneme="o" type="vs" value="1" rule="432">o</seg>p</w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.8">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w></l>
	</lg>
	<lg n="3">
	<head type="speaker">LIONEL, bas à Pretty.</head>
		<l n="6" num="3.1"><w n="6.1">V<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>, <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.4">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>pr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="7" num="3.2"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">l</w>'<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="7.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>, <w n="7.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="7.6">n</w>'<w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="7.8">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w></l>
		<l n="8" num="3.3"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="8.2">lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="8.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.8">v<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
	</lg>
	<lg n="4">
	<head type="speaker">MISTRISS CARINGTON, PRETTY, INDIANA.</head>
		<l n="9" num="4.1"><w n="9.1">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.2">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> ; <w n="9.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="9.5">p<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>-<w n="9.7">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w></l>
		<l n="10" num="4.2"><w n="10.1"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="10.2">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="10.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.5">t<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="11" num="4.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="11.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>, <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="11.5">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w> <w n="11.6">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="11.7">s</w>'<w n="11.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ppr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="12" num="4.4"><w n="12.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.3">pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>rs</w> <w n="12.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">d<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="12.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="12.7">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>.</l>
	</lg>
	<lg n="5">
	<head type="speaker">LIONEL, à Ludworth.</head>
		<l n="13" num="5.1"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>, <w n="13.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="13.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>r</w>, <w n="13.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.5">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.6">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="13.7">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> !</l>
		<l n="14" num="5.2"><w n="14.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="14.2">vr<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="14.3">c</w>'<w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="14.5"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="15" num="5.3"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="15.3">qu</w>'<w n="15.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="15.5">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w> <w n="15.6">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="15.7">s</w>'<w n="15.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ppr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="16" num="5.4"><w n="16.1">V<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.2">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="16.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="16.4">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="16.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.6">j<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w>.</l>
	</lg>
	<lg n="6">
	<head type="speaker">LUDWORTH.</head>
		<l n="17" num="6.1"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>, <w n="17.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="17.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>r</w>, <w n="17.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">s<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="17.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t</w> <w n="17.7">j<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w>,</l>
		<l n="18" num="6.2"><w n="18.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>s</w> <w n="18.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="18.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="18.6">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>-<w n="18.7"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>-<w n="18.8">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
		<l n="19" num="6.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="19.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.3">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>, <w n="19.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="19.5">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w> <w n="19.6">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="19.7">s</w>'<w n="19.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ppr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="20" num="6.4"><w n="20.1">J</w>'<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="20.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="20.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="20.7">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="20.8">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w>.</l>
	</lg>
 </div></body></text></TEI>