<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB14">
	<head type="tune">Air : Voilà trois ans qu'en co village. ( Léocadie.)</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">H<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ! <w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="1.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="2.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="2.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">n</w>'<w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="2.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> !</l>
		<l n="3" num="1.3"><w n="3.1">J</w>'<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="3.3">s<seg phoneme="ɛ" type="vs" value="1" rule="383">ei</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w>… <w n="3.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.6">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="3.7">qu</w>'<w n="3.8"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="3.9">c<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.10"><seg phoneme="a" type="vs" value="1" rule="339">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="4" num="1.4"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="4.2">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="4.3">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="4.5">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="4.6">l<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>,</l>
		<l n="5" num="1.5"><w n="5.1">J</w>'<w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="5.3">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="5.4">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="5.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">n</w>'<w n="5.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.9">si<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> !</l>
		<l n="6" num="1.6"><w n="6.1">Ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> : <w n="6.4">V<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.6">j<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="7" num="1.7"><w n="7.1">C</w>'<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.4">R<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.7">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="7.8">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>…</l>
		<l n="8" num="1.8"><w n="8.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">n</w>'<w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="8.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.7">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="8.8">j<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
		<l n="9" num="1.9"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>, <w n="9.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.3">Di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>, <w n="9.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="9.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.7">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> ?</l>
	</lg>
	<lg n="2">
		<l n="10" num="2.1"><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="10.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
		<l n="11" num="2.2"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">b<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="11.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="11.4">d<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> ;</l>
		<l n="12" num="2.3"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">su<seg phoneme="i" type="vs" value="1" rule="490">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="12.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.5">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="13" num="2.4"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="13.3"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> ;</l>
		<l n="14" num="2.5"><w n="14.1">Vi<seg phoneme="ɛ" type="vs" value="1" rule="365">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="14.3">b<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="14.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w>.</l>
		<l n="15" num="2.6"><w n="15.1">L</w>'<w n="15.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>t</w> <w n="15.4">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.5">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="15.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
		<l n="16" num="2.7"><w n="16.1">B<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="16.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="16.3">g<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w> <w n="16.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>cs</w>, <w n="16.5">r<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="16.7">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> ;</l>
		<l n="17" num="2.8"><w n="17.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.3">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w>, <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.6">fl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="17.7">d</w>'<w n="17.8"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ! …</l>
		<l n="18" num="2.9"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="18.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="18.3">Di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>, <w n="18.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="18.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="18.7">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> !</l>
	</lg>
</div></body></text></TEI>