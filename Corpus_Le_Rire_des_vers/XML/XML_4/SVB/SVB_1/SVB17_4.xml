<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB17">
	<head type="tune">Air de Marianne.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="1.2">li<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="1.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">j</w>'<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="2.4">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="2.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="2.6">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>,</l>
		<l n="3" num="1.3"><w n="3.1">C</w>'<w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="3.5">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.6">l</w>'<w n="3.7"><seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="4" num="1.4"><w n="4.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>.</l>
	</lg>
	<lg n="2">
	<head type="speaker">DESŒILLETS.</head>
		<l n="5" num="2.1"><w n="5.1">Qu</w>'<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w>-<w n="5.3">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> ? …</l>
	</lg>
	<lg n="3">
	<head type="speaker">HENRI.</head>
		<l n="6" num="3.1"><w n="6.1">H<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg></w> ? <w n="6.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>-<w n="6.4">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> ?</l>
	</lg>
	<lg n="4">
	<head type="speaker">DESŒILLETS.</head>
		<l n="7" num="4.1"><w n="7.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>. <del reason="analysis" type="stage" hand="KL">( À part.)</del> <w n="7.2">J</w>'<w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="7.4">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> !</l>
		<stage>( À Dupuis)</stage>
		<l part="I" n="8" num="4.2"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="8.3">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> ? … </l>
	</lg>
	<lg n="5">
	<head type="speaker">DUPUIS.</head>
		<l part="F" n="8"><w n="8.4">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="8.6">vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> !</l>
	</lg>
	<lg n="6">
	<head type="speaker">DESŒILLETS.</head>
		<l part="I" n="9" num="6.1"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.2">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ? … </l>
	</lg>
	<lg n="7">
	<head type="speaker">DUPUIS.</head>
		<l part="F" n="9"><w n="9.3">Ou<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> !</l>
	</lg>
	<lg n="8">
	<head type="speaker">DESŒILLETS.</head>
		<l part="I" n="10" num="8.1"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! … </l>
	</lg>
	<lg n="9">
	<head type="speaker">DUPUIS,</head>
		<l part="F" n="10"><w n="10.3"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> !</l>
	</lg>
	<lg n="10">
		<l n="11" num="10.1"><w n="11.1">C</w>'<w n="11.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>, <w n="11.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">cr<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>,</l>
		<l part="I" n="12" num="10.2"><w n="12.1">L<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="12.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ss<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>. </l>
	</lg>
	<lg n="11">
	<head type="speaker">DESŒILLETS, pâle et défait.</head>
		<l part="F" n="12"><w n="12.3">T<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>-<w n="12.4">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>.</l>
	</lg>
	<lg n="12">
	<head type="speaker">HENRI, lui remettant le billet.</head>
		<l n="13" num="12.1"><w n="13.1">Ti<seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>s</w>, <w n="13.2">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>bb<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>… <w n="13.4">pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.5">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="13.6">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
		<l part="I" n="14" num="12.2"><w n="14.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="14.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="14.3">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>… </l>
	</lg>
	<lg n="13">
	<head type="speaker">DESŒILLETS.</head>
		<l part="F" n="14"><w n="14.5">Vr<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> ! …</l>
		<stage>( à part.)</stage>
		<l n="15" num="13.1"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="15.2">c</w>'<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="15.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">di<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
		<l n="16" num="13.2"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="16.2">s</w>'<w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="16.4">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="16.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.6">mi<seg phoneme="ɛ" type="vs" value="1" rule="365">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
	</lg>
</div></body></text></TEI>