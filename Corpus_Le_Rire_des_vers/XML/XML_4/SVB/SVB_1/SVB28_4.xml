<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB28">
	<head type="tune">Air : À soixante ans on ne doit pas remettre.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>, <w n="1.2">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rbl<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> ! <w n="1.3">l</w>'<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>bb<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="1.5">f<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="1.6">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
		<l n="2" num="1.2"><w n="2.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="2.2">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>sp<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="2.7">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.8">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> ;</l>
		<l n="3" num="1.3"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="3.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.4"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="3.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="3.7">t</w>'<w n="3.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="4" num="1.4"><w n="4.1">N</w>'<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w>, <w n="4.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>s</w>-<w n="4.4">t</w>-<w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w>, <w n="4.6">qu</w>'<w n="4.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="4.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="4.9">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="4.10"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> ;</l>
		<l n="5" num="1.5"><w n="5.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> ! … <w n="5.3">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="5.5">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w>.</l>
		<l n="6" num="1.6"><w n="6.1">Qu</w>'<w n="6.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.5">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="6.7">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="7" num="1.7"><w n="7.1">Pl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>rs</w>, <w n="7.2">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>, <w n="7.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="7.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="7.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="7.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>,</l>
		<l n="8" num="1.8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="8.2">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="8.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rds</w>, <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="8.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>,</l>
		<l n="9" num="1.9"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="9.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.5">l</w>'<w n="9.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
		<l n="10" num="1.10"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">d<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="10.6">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>s</w> <w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="10.8">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>-<w n="10.9">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>.</l>
	</lg>
</div></body></text></TEI>