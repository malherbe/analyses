<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB19">
	<head type="tune">Air : Je loge au quatrième étage.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> ! <w n="1.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.5">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="1.6">f<seg phoneme="a" type="vs" value="1" rule="192">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
		<l n="2" num="1.2"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg></w>, <w n="2.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="2.3">mi<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="2.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.6">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="2.7">f<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> !</l>
		<l n="3" num="1.3"><w n="3.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="3.3">j</w>'<w n="3.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="3.5">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.6">d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> :</l>
		<l n="4" num="1.4"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>s</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="4.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="4.5">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.6">y<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>.</l>
		<l n="5" num="1.5"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="5.2">j</w>'<w n="5.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.6">ch<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="6" num="1.6"><w n="6.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.2">H<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">m</w>'<w n="6.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w>, <w n="6.5">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> !</l>
	</lg>
	<lg n="2">
	<head type="speaker">DUPIS.</head>
		<l n="7" num="2.1"><w n="7.1">Vr<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> ? <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">n<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-27">e</seg></w> <w n="7.5">H<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
	</lg>
	<lg n="3">
	<head type="speaker">DESŒILLETS.</head>
		<l n="8" num="3.1"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="8.2">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="8.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">su<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="8.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="8.6">M<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>.</l>
	</lg>
</div></body></text></TEI>