<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB12">
	<head type="tune">Air : Que de mal, de tourment.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w>, <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">d</w>'<w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="358">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> !</l>
		<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ls</w> <w n="2.2">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="2.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.5">nu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>ts</w>,</l>
		<l n="3" num="1.3"><w n="3.1">J</w>'<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="3.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="4.2">qu</w>'<w n="4.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="4.4">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
		<l n="5" num="1.5"><w n="5.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">sc<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="5.3">d</w>'<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>bb<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w></l>
		<l n="6" num="1.6"><w n="6.1"><seg phoneme="ɛ" type="vs" value="1" rule="198">E</seg>st</w> <w n="6.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="6.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="6.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>, <w n="6.5">n<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="6.6">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
		<l n="7" num="1.7"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.3">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>vi<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="7.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.5">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w></l>
		<l n="8" num="1.8"><w n="8.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="8.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>,</l>
		<l n="9" num="1.9"><w n="9.1">D</w>'<w n="9.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="9.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w></l>
		<l n="10" num="1.10"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="10.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>,</l>
		<l n="11" num="1.11"><w n="11.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rsqu</w>'<w n="11.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="11.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w></l>
		<l n="12" num="1.12"><w n="12.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="12.2">j<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t</w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="12.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.5">m<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>rs</w> !</l>
		<l n="13" num="1.13"><w n="13.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.2">l</w>'<w n="13.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.6">m<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>rs</w> !</l>
		<l n="14" num="1.14"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w>, <w n="14.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>gr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>,</l>
		<l n="15" num="1.15"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="15.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>s</w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="15.5">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w></l>
		<l n="16" num="1.16"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>, <w n="16.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="16.4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>s</w>, <w n="16.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="16.6">vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="16.7">v<seg phoneme="ø" type="vs" value="1" rule="404">eu</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
		<l n="17" num="1.17"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="17.3">qu</w>'<w n="17.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="17.5">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="17.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> !</l>
		<l n="18" num="1.18"><w n="18.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="18.3">m</w>'<w n="18.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="18.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="18.6">d<seg phoneme="y" type="vs" value="1" rule="444">û</seg></w>,</l>
		<l n="19" num="1.19"><w n="19.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="19.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="19.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="20" num="1.20"><w n="20.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w> <w n="20.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="20.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w></l>
		<l n="21" num="1.21"><w n="21.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">l</w>'<w n="21.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>bb<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="21.4">m</w>'<w n="21.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="21.6">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> !</l>
	</lg>
</div></body></text></TEI>