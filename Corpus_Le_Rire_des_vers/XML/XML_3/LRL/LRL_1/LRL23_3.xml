<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL23">
	<head type="tune">AIR des aubergistes de qualité.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>dm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <w n="1.2">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>-<w n="1.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.4">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
		<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">s<seg phoneme="i" type="vs" value="1" rule="492">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>m</w>' <w n="2.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>f</w>…</l>
		<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl</w>' <w n="3.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="3.4">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>-<w n="3.5">m<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>m</w>' <w n="3.6">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
		<l n="4" num="1.4"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="4.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w> <w n="4.3">n</w>'<w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>f</w> ! …</l>
		<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="5.2">qu<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="5.4">qu</w>'<w n="5.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="5.6">s</w>'<w n="5.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="5.8">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="6" num="1.6"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.3">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="6.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="6.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.7">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="7" num="1.7"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="7.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="7.5">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w>.</l>
		<l n="8" num="1.8"><w n="8.1">Qu</w>'<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>-<w n="8.3">t</w>-<w n="8.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="8.5">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ? … <w n="8.6">qu</w>'<w n="8.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>-<w n="8.8">t</w>-<w n="8.9"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="8.10">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> ? …</l>
		<l n="9" num="1.9"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="9.2">t<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.5">qu<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>' <w n="9.6">d</w>'<w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.8">p<seg phoneme="wa" type="vs" value="1" rule="157">oê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
		<l n="10" num="1.10"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="10.2">dʼp<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="10.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g</w>-<w n="10.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w> <w n="10.5">l</w>'<w n="10.6">b<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="10.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> !</l>
		<l n="11" num="1.11"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-6">e</seg></w> <w n="11.3">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="11.4">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.6">p<seg phoneme="wa" type="vs" value="1" rule="157">oê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="12" num="1.12"><w n="12.1">P<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="12.3">p<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr</w>' <w n="12.4">b<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="12.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> ! <del reason="analysis" type="repetition" hand="KL">(bis)</del></l>
	</lg>
</div></body></text></TEI>