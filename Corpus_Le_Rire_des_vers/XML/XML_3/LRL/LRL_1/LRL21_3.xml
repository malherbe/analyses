<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL21">
	<head type="tune">AIR : Bien malin qui m'éclipsera (de Panseron).</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">B<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="1.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="1.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="1.4">m</w>'<w n="1.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ps<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>,</l>
		<l n="2" num="1.2"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="2.6">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> ! …</l>
		<p>( Elle danse.)</p>
		<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="3.3">c<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="3.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.5">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.7">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> ;</l>
		<l n="4" num="1.4"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ppl<seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.5">l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
		<l n="5" num="1.5"><w n="5.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="5.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="5.4">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w> <w n="5.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w></l>
		<l n="6" num="1.6"><w n="6.1"><seg phoneme="w" type="sc" value="0" rule="430">Ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="6.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.4">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w>, <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.6">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w> <w n="6.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w>,</l>
		<l n="7" num="1.7"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">p<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="7.4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>s</w> <w n="7.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="434">o</seg>ffr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="7.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.8">tr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="7.9">Gr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
		<l n="8" num="1.8"><w n="8.1">B<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="8.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="8.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="8.4">m</w>'<w n="8.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ps<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>,</l>
		<l n="9" num="1.9"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="9.6">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> ! …</l>
		<p>( Elle danse.)</p>
 	</lg>
 	<lg n="2">
		<head type="main">DEUXIÈME COUPLET.</head>
		<l n="10" num="2.1"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="10.3">z<seg phoneme="e" type="vs" value="1" rule="408">é</seg>pb<seg phoneme="i" type="vs" value="1" rule="467">i</seg>rs</w>, <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="10.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.7">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w></l>
		<l n="11" num="2.2"><w n="11.1">F<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.2"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>… <w n="11.3">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.5">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="11.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
		<l n="12" num="2.3"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="12.3">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.6">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w>… <w n="12.7">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.8">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>… <del reason="analysis" type="repetition" hand="KL">(bis.)</del></l>
		<l n="13" num="2.4"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">T<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gl<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="o" type="vs" value="1" rule="443">o</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="13.3">l</w>'<w n="13.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="13.5">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="13.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.7">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="13.8">j<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
		<l n="14" num="2.5"><w n="14.1">B<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="14.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="14.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="14.4">m</w>'<w n="14.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ps<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>,</l>
		<l n="15" num="2.6"><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="15.6">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="15.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="15.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> ! …</l>
	</lg>
</div></body></text></TEI>