<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL1">
	<head type="tune">AIR nouveau de M. Amédée Beauplan.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="a" type="vs" value="1" rule="306">a</seg>il</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">l</w>'<w n="1.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">j<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="2.3">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.6">N<seg phoneme="o" type="vs" value="1" rule="443">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="174">ë</seg>l</w>,</l>
		<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.4">v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.5">f<seg phoneme="a" type="vs" value="1" rule="192">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="4" num="1.4"><w n="4.1">S<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.2">l</w>'<w n="4.3"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t</w> <w n="4.6">M<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> ?</l>
		<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.4">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.5">l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="6" num="1.6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>cc<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rt</w> <w n="6.2">f<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="a" type="vs" value="1" rule="339">â</seg>tr<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="6.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.5"><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> ;</l>
		<l n="7" num="1.7"><w n="7.1">N<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="7.2">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="7.3">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.4">b<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.5">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="8" num="1.8"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="8.2">v<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="8.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="8.7">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="8.8">j<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> ? …</l>
		<l n="9" num="1.9"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w>, <w n="9.2">c</w>'<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.5">f<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="9.7">m<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
		<l n="10" num="1.10"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="10.2">v<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="10.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w></l>
		<l n="11" num="1.11"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="11.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="11.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.4">h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
		<l n="12" num="1.12"><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="12.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="12.4">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rc<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">p<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w>.</l>
 	</lg>
 	<lg n="2">
	<head type="main">CHŒUR.</head> 
		<l ana="unanalyzable" n="13" num="2.1">Écoliers, etc.</l>
	</lg>
	<lg n="3">
	<head type="speaker">LA FÉE.</head>
		<l n="14" num="3.1"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="14.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="14.3"><seg phoneme="y" type="vs" value="1" rule="390">eu</seg>t</w> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.5">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="15" num="3.2"><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>, <w n="15.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="15.4">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="15.5">fr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
		<l n="16" num="3.3"><w n="16.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="16.3">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="16.5">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>ll<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="17" num="3.4"><w n="17.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="17.3">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="17.5">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ?</l>
		<l n="18" num="3.5"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="18.2">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>, <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="18.4">l</w>'<w n="18.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="18.6">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="18.7">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="18.8">d<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
		<l n="19" num="3.6"><w n="19.1">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="19.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="19.3">b<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="19.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="19.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>r</w> <w n="19.6">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> !</l>
		<l n="20" num="3.7"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="20.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="20.3">d<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.5">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="21" num="3.8"><w n="21.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.2">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="21.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.4">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="21.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="21.6">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w> !</l>
	</lg>
	<lg n="4">
	<head type="main">CHŒUR.</head>
		<l ana="unanalyzable" n="22" num="4.1">Écoliers, etc.</l>
	</lg>
	<lg n="5">
	<head type="speaker">LA FÉE.</head>
		<l n="23" num="5.1"><w n="23.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="23.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="23.3">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.6">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="24" num="5.2"><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="24.3">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w>, <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="24.6">l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> ?</l>
		<l n="25" num="5.3"><w n="25.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="25.2">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="25.3">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="25.4">d</w>'<w n="25.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="25.6">j<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.7">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="26" num="5.4"><w n="26.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="26.2">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="26.4">s<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>s</w>, <w n="26.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="26.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> ?</l>
		<l n="27" num="5.5"><w n="27.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="27.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="27.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="27.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.5">f<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="27.7">cl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="28" num="5.6"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="28.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="28.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.5">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="28.6">s</w>'<w n="28.7"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>,</l>
		<l n="29" num="5.7"><w n="29.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="29.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="29.3">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="29.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="29.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="30" num="5.8"><w n="30.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="30.2">pr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="30.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="30.4">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> ?</l>
	</lg>
</div></body></text></TEI>