<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE III.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC18">
					<head type="tune">AIR nouveau de Doche fils. (premier acte.)</head>
						<lg n="1">
							<head type="main">ARWED.</head>
							<l n="1" num="1.1"><space unit="char" quantity="4"></space><w n="1.1">M<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="1.2">l</w>'<w n="1.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">j</w>'<w n="1.8"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
							<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="2.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="2.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="3.2">l</w>'<w n="3.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>pt<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="3.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="3.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="3.7">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="4" num="1.4"><w n="4.1">C</w>'<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="4.3">m</w>'<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.6">h<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="4.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.9">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>.</l>
							<l n="5" num="1.5"><space unit="char" quantity="4"></space><w n="5.1"><seg phoneme="w" type="sc" value="0" rule="430">Ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="5.3">l</w>'<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="5.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="6" num="1.6"><w n="6.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.2">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="6.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="6.7">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="6.8">l<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w> …</l>
							<l n="7" num="1.7"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="7.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w>, <w n="7.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="8" num="1.8"><space unit="char" quantity="4"></space><w n="8.1">L</w>'<w n="8.2">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="8.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.7">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg>s</w>.</l>
						</lg>
					</div></body></text></TEI>