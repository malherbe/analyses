<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE IV.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC5">
				<head type="tune">AIR du comte Ory.</head>
					<lg n="1">
						<head type="main">ARWED.</head>
						<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="1.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="1.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="1.6">b<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="2.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>s</w> <w n="2.4">s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="3.2">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="4.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="4.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> !</l>
					</lg>
					<lg n="2">
						<head type="main">LES AUTRES.</head>
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="w" type="sc" value="0" rule="430">Ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="5.2">c</w>'<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="5.4">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="5.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="5.7">b<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="4"></space><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="6.3">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="6.4">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="7" num="2.3"><space unit="char" quantity="4"></space><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w>, <w n="7.2">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="8.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="8.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> !</l>
					</lg>
					
				<lg n="3">
					<head type="main">ARWED, seul.</head>
						<l n="9" num="3.1"><space unit="char" quantity="4"></space><w n="9.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">br<seg phoneme="ɥ" type="sc" value="0" rule="454">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="9.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="4"></space><w n="10.1">C<seg phoneme="o" type="vs" value="1" rule="317">au</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="10.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="11" num="3.3"><space unit="char" quantity="4"></space><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="11.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg></w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">l</w>'<w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w></l>
						<l n="12" num="3.4"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="12.4">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="12.5"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
					</lg>
					<lg n="4">
						<head type="main">TOUS.</head>
						<l n="13" num="4.1"><space unit="char" quantity="4"></space><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="13.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg></w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">l</w>'<w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w></l>
						<l n="14" num="4.2"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="14.4">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="14.5"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
					</lg>
				</div></body></text></TEI>