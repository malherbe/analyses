<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA MÉTEMPSYCOSE</title>
				<title type="sub">BÊTISE EN UN ACTE, MÊLÉE DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="COU" sort="1">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<author key="JAI" sort="2">
					<name>
						<forename>Ernest</forename>
						<surname>JAIME</surname>
					</name>
					<date from="1804" to="1884">1804-1884</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>282 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FCR_1</idno>	
				<availability status="free">
					<licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA MÉTEMPSYCOSE</title>
						<author>FRÉDÉRIC DE COURCY ET JAIME</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=c1RoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032819917.0x000001# ?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">7 FÉVRIER 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">VAUDEVILLE.CHOEUR.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FCR24">
			<head type="tune">AIR : Vaudeville de la Nuit de Noël.</head>
			<div type="section" n="1">
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">s<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.4"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="1.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="1.6">s<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="2.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="2.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="2.4">d</w>' <w n="2.5">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">s<seg phoneme="o" type="vs" value="1" rule="437">o</seg>ts</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Qu</w>'<w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="3.4">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="3.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.6">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="4" num="1.4"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w>.</l>
				</lg>
				<lg n="2">
					<head type="speaker">JULES.</head>
					<l n="5" num="2.1"><w n="5.1">V<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="5.2">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.3">p<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.4">gr<seg phoneme="y" type="vs" value="1" rule="453">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.3">pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w>,</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="7.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>gs<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="8" num="2.4"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="8.2">p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>dg<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> ! …</l>
				</lg>
				<lg n="3">
					<l ana="unanalyzable" n="9" num="3.1">Dans le siècle, etc.</l> 
				</lg>
				<lg n="4">
					<head type="speaker">MARIE.</head>
					<l n="10" num="4.1"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.2">f<seg phoneme="a" type="vs" value="1" rule="192">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="10.4"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.5">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="11" num="4.2"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.2">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mmʼs</w> <w n="11.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.5">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> ;</l>
					<l n="12" num="4.3"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="12.2">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p</w> <w n="12.3">d</w>'<w n="12.4"><seg phoneme="œ" type="vs" value="1" rule="380">oe</seg>il</w>, <w n="12.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="12.6">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p</w> <w n="12.7">d</w>'<w n="12.8">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="13" num="4.4"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w>, <w n="13.2">cr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c</w>, <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.4">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="13.5">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> !</l>
				</lg>
				<lg n="5">
					<l ana="unanalyzable" n="14" num="5.1">Dans le siècle, etc.</l> 
				</lg>
				<lg n="6">
					<head type="speaker">CHRISTOPHE.</head>
					<l n="15" num="6.1"><w n="15.1">Vr<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="15.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="15.3">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="15.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="6.2"><w n="16.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.2">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>plʼs</w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="16.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.5">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w> ;</l>
					<l n="17" num="6.3"><w n="17.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="17.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="17.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="17.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="18" num="6.4"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.3">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="18.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.5">d<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w>.</l>
				</lg>
				<lg n="7">
					<l ana="unanalyzable" n="19" num="7.1">Dans le siècle, etc.</l> 
				</lg>
				<lg n="8">
					<head type="speaker">MADAME DUVAL.</head> 
					<l n="20" num="8.1"><w n="20.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="20.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="20.3">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="20.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="21" num="8.2"><w n="21.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">j</w>'<w n="21.3"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="21.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.7">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s</w>,</l>
					<l n="22" num="8.3"><w n="22.1">M<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="22.3">l</w>'<w n="22.4"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="22.5">d</w>'<w n="22.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w>' <w n="22.7">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="23" num="8.4"><w n="23.1"><seg phoneme="u" type="vs" value="1" rule="425">Où</seg></w> <w n="23.2">l</w>'<w n="23.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="23.4">m<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="23.6">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>s</w>.</l>
				</lg>
				<lg n="9">
					<l ana="unanalyzable" n="24" num="9.1">Dans le siècle, etc.</l> 
				</lg>
				<lg n="10">
					<head type="speaker">ÉTIENNE.</head> 
					<l n="25" num="10.1"><w n="25.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="25.2">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>llʼs</w> <w n="25.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="25.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="25.5">dʼm<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="26" num="10.2"><w n="26.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="26.2">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rc<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="26.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="26.4">p<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> ;</l>
					<l n="27" num="10.3"><w n="27.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="27.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s<seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>sʼs</w> <w n="27.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="27.4">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>tʼr<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="28" num="10.4"><w n="28.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="28.2">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rds</w> <w n="28.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="28.4">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>chʼr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>.</l>
				</lg>
				<lg n="11">
					<l ana="unanalyzable" n="29" num="11.1">Dans le siècle, etc.</l> 
				</lg>
				<lg n="12">
					<head type="speaker">ERNEST.</head> 
					<l n="30" num="12.1"><w n="30.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="30.2">p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>x</w> <w n="30.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="30.4">tr<seg phoneme="o" type="vs" value="1" rule="432">o</seg>p</w> <w n="30.5">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="31" num="12.2"><w n="31.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="31.2">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="31.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="31.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.5">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>,</l>
					<l n="32" num="12.3"><w n="32.1"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="32.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="32.3">m<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="33" num="12.4"><w n="33.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.2">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.3">c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>q</w> <w n="33.4">g<seg phoneme="o" type="vs" value="1" rule="317">au</seg>l<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> ! …</l>
				</lg>
				<lg n="13">
					<l ana="unanalyzable" n="34" num="13.1">Dans le siècle, etc.</l> 
				</lg>
			</div>
			<div type="section" n="2">
				<head type="main">ON REPREND LE CHOEUR.</head>
				<lg n="1">
					<l n="35" num="1.1"><w n="35.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="35.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.3">s<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="35.4"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="35.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="35.6">s<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="36" num="1.2"><w n="36.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="36.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="36.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="36.4">d</w>' <w n="36.5">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="36.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="36.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.8">s<seg phoneme="o" type="vs" value="1" rule="437">o</seg>ts</w>,</l>
					<l n="37" num="1.3"><w n="37.1">Qu</w>'<w n="37.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="37.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="37.4">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="37.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="37.6">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="38" num="1.4"><w n="38.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="38.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="38.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w>.</l>
			</lg>
				</div>
		</div></body></text></TEI>