<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <fileDesc>
   <titleStmt>
	<title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
	<title type="corpus">Le Rire des vers</title>
	<author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
	</author>
	<author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
	</author>
	<editor>Le Rire des vers, Université de Bâle</editor>
	<editor>
	 Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
	 <choice>
	  <abbr>CRISCO, Université de Caen Normandie</abbr>
	  <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	 </choice>
	 (EA 4255)
	</editor>
	<respStmt>
	 <resp>Encodage en XML (CRISCO, université de Caen)</resp>
	 <name id="KL">
	  <forename>Kedi</forename>
	  <surname>LI</surname>
	 </name>
	</respStmt>
	<respStmt>
	 <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
	 <name id="RR">
	  <forename>Richard</forename>
	  <surname>RENAULT</surname>
	 </name>
	</respStmt>
   </titleStmt>
   <extent>140 vers</extent>
   <publicationStmt>
	<publisher>
	 <orgname>
	  <choice>
	   <abbr>CRISCO, Université de Caen Normandie</abbr>
	   <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	  </choice>
	 </orgname>
	 <address>
	  <addrLine>Université de Caen</addrLine>
	  <addrLine>14032 CAEN CEDEX</addrLine>
	  <addrLine>FRANCE</addrLine>
	 </address>
	 <email>crisco.incipit@unicaen.fr</email>
	 <ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
	</publisher>
	<pubPlace>Caen</pubPlace>
	<date when="2022">2022</date>
	<idno type="local">SCB_2</idno>	
	<availability status="free">
		<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
		<p>
			Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
			CC = Licence Creative Commons
			BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
			sur les sources et sur les modifications introduites.
			NC = Pas d’utilisation commerciale.
			SA = Partage dans les mêmes conditions.
		</p>
	</availability>
   </publicationStmt>
   <sourceDesc>
	<biblFull>
	 <titleStmt>
	  <title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	  <author>SCRIBE ET BAYARD</author>
	 </titleStmt>
	 <publicationStmt>
	  <publisher>INTERNET ARCHIVE</publisher>
	  <idno type="URL">https://archive.org/details/camillaoulasoeur00scriuoft</idno>
	 </publicationStmt>
	 <sourceDesc>
	  <biblStruct>
	   <monogr>
		<repository>University of Toronto Libraries</repository>
		<idno type="URL">https://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991106543695706196</idno>
	   </monogr>
	  </biblStruct>                 
	 </sourceDesc>
	</biblFull> 
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <creation>
	<date when="1832">12 DÉCEMBRE 1832</date>
	<placeName>
	 <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
	</placeName>
   </creation>
  </profileDesc>
  <encodingDesc>
   <projectDesc>
	<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
   </projectDesc>
   <samplingDecl>
	<p>Les parties versifiées ont été prioritairement encodées.</p>
   </samplingDecl>
   <editorialDecl>
	<normalization>
	 <p>Les majuscules accentuées ont été restituées.</p>
	 <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
	 <p>La ponctuation a été normalisée.</p> 
	 <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
	 <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
	 <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
	</normalization>
   </editorialDecl>
  </encodingDesc>
 </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB24">
	<head type="tune">AIR Anglais.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>,</l>
		<l n="2" num="1.2"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="2.2">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="2.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="2.5">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
		<l n="3" num="1.3"><w n="3.1">Tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>,</l>
		<l n="4" num="1.4"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="4.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="?" type="va" value="1" rule="161">en</seg>t</w>, <w n="4.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="4.6">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>,</l>
		<l n="5" num="1.5"><w n="5.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="5.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bʼ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="6" num="1.6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w>, <w n="6.2">gr<seg phoneme="a" type="vs" value="1" rule="339">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.6">j<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>,</l>
		<l n="7" num="1.7"><w n="7.1">J</w>'<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="7.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="7.4">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="7.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="7.6">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>.</l>
		<l n="8" num="1.8"><w n="8.1">Tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>,</l>
	<p>
		(Camilla veut lui parler, il continue toujours sans l'écouter.)
	</p>
		<l n="9" num="1.9"><w n="9.1"><seg phoneme="w" type="sc" value="0" rule="430">Ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="9.2">j</w>'<w n="9.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="9.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
		<l n="10" num="1.10"><w n="10.1">Tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>,</l>
		<l n="11" num="1.11"><w n="11.1">J</w>'<w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="11.4">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w>, <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w>, <w n="11.7">vr<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
		<l n="12" num="1.12"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>rrr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="12.3">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>j<seg phoneme="ø" type="vs" value="1" rule="389">eû</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>.</l>
		<l n="13" num="1.13"><w n="13.1">Tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>.</l>
	</lg>
</div></body></text></TEI>