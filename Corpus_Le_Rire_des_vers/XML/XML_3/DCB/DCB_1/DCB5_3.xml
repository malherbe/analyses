<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB5">
	<head type="tune">AIR : Ah ! baisez papa, baisez maman.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="1.2">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="1.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.4">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w></l>
		<l n="2" num="1.2"><w n="2.1">B<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="2.2">d<seg phoneme="o" type="vs" value="1" rule="443">o</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>, <w n="2.3">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="2.4">r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>ds</w> !</l>
		<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="3.2"><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="3.3">j</w>'<w n="3.4">m</w>'<w n="3.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
		<l n="4" num="1.4"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="4.3">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="4.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> !</l>
		<l n="5" num="1.5"><w n="5.1">Qu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.2">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="5.3">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>,</l>
		<l n="6" num="1.6"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="6.2">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lʼnt</w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.4">d<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> :</l>
		<l n="7" num="1.7"><w n="7.1">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.3">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> !</l>
	</lg>
	<lg n="2">
		<l n="8" num="2.1"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="8.3">d</w>'<w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w>'<w n="8.5">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>,</l>
		<l n="9" num="2.2"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="9.2">l</w>'<w n="9.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d</w>', <w n="9.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>-<w n="9.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>,</l>
		<l n="10" num="2.3"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="10.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="10.3">b<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">m<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
		<l n="11" num="2.4"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="11.2"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="11.3">j</w>'<w n="11.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.5">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v</w>'<w n="11.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.7">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="11.8">g<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w>,</l>
		<l n="12" num="2.5"><w n="12.1">C</w>'<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="12.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="12.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="12.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="12.6">r<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>.</l>
	</lg>
</div></body></text></TEI>