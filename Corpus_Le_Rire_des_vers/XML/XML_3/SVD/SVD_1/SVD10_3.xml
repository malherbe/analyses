<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">COMMÈRE JEANNETON</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>261 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIEME</head><head type="main_subpart">SCENE PREMIERE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD10">
			<head type="tune">AIR du Maçon.</head>
				<lg n="1">
					<head type="main">TOUTES LES COMMÈRES.</head>
					<l n="1" num="1.1"><space unit="char" quantity="6"></space><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>, <w n="1.2">s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>-<w n="1.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="2"></space><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">qu<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="2.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rs<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>-<w n="2.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>,</l>
					<l n="3" num="1.3"><space unit="char" quantity="2"></space><w n="3.1">C</w>'<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="3.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">sʼr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.6">lʼ</w> <w n="3.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="3.8">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>.</l>
					<l n="4" num="1.4"><space unit="char" quantity="6"></space><w n="4.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="4.2">cʼ</w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w></l>
					<l n="5" num="1.5"><space unit="char" quantity="10"></space><w n="5.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="5.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">lʼ</w> <w n="5.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>.</l>
					<l n="6" num="1.6"><w n="6.1">Pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="6.2">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">lʼ</w> <w n="6.7">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>,</l>
					<l n="7" num="1.7"><space unit="char" quantity="2"></space><w n="7.1">Qu</w>' <w n="7.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="7.3">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="7.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">lʼ</w> <w n="7.7">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>.</l>
					<l n="8" num="1.8"><space unit="char" quantity="10"></space><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>, <w n="8.2">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="9" num="1.9"><space unit="char" quantity="10"></space><w n="9.1">Pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="10" num="1.10"><space unit="char" quantity="2"></space><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="10.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lm<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="10.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="10.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="10.6">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>,</l>
					<l n="11" num="1.11"><space unit="char" quantity="2"></space><w n="11.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="11.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>.</l>
				</lg>
		</div></body></text></TEI>