<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">COMMÈRE JEANNETON</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>261 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIEME</head><head type="main_subpart">SCÈNE IV.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD11">
			<head type="tune">Air d'Ielva.</head>
				<lg n="1">
					<head type="main">DELAUNAY.</head>
					<l n="1" num="1.1"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.3">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">l</w>'<w n="1.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>lg<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="2" num="1.2"><w n="2.1">J<seg phoneme="a" type="vs" value="1" rule="309">ea</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="2.2">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ! <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.4">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="2.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="3.2">qu</w>'<w n="3.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.4">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="3.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.8">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="4.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>,</l>
					<l n="5" num="1.5"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="5.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>.</l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="6.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>il</w> <w n="6.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="6.4">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="6.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w> <w n="6.6">d</w>'<w n="6.7"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.8">s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="7" num="1.7"><w n="7.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="7.2">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w> <w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w> <w n="7.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>lg<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w>,</l>
					<l n="8" num="1.8"><w n="8.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="8.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.6">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="8.7">d</w>'<w n="8.8"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.9">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="9" num="1.9"><w n="9.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="9.4">l</w>'<w n="9.5">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>.</l>
				</lg>
			</div></body></text></TEI>