<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS11">
	<head type="form">ROMANCE.</head> 
	<lg n="1">
		<head type="speaker">OCTAVIE.</head>
		<l n="1" num="1.1"><w n="1.1">V<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>-<w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="1.3">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.5">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="2" num="1.2"><w n="2.1">D<seg phoneme="e" type="vs" value="1" rule="408">é</seg>pl<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="2.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.3">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.4">dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w> ? </l>
		<l n="3" num="1.3"><w n="3.1">B<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rg<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="3.4">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.5">ch<seg phoneme="o" type="vs" value="1" rule="317">au</seg>m<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>, </l>
		<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="4.2">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.3">h<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="4.5">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.6">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w>, </l>
		<l n="5" num="1.5"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.6">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="6" num="1.6"><w n="6.1">V<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>s</w> <w n="6.2">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="6.3">d</w>'<w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.5">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="6.6">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w>, </l>
		<l n="7" num="1.7"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="7.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.6">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ; </l>
		<l n="8" num="1.8"><w n="8.1">F<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>-<w n="8.2">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="8.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> ! <del reason="analysis" type="repetition" hand="KL">( bis.)</del></l>
	</lg>
	<lg n="2">
		<head type="speaker">NINETTE.</head>
		<l n="9" num="2.1"><w n="9.1">S<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w>, <w n="9.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>-<w n="9.3">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="9.4">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w> <w n="9.6">d<seg phoneme="o" type="vs" value="1" rule="443">o</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
		<l n="10" num="2.2"><w n="10.1">S<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="10.5">c<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> ? </l>
		<l n="11" num="2.3"><w n="11.1">C</w>'<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="11.3">l</w>'<w n="11.4"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.6">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="11.7">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="484">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
		<l n="12" num="2.4"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="12.2">s</w>'<w n="12.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="12.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="12.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w>. </l>
		<l n="13" num="2.5"><w n="13.1">T<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.2">l<seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
		<l n="14" num="2.6"><w n="14.1">N</w>'<w n="14.2"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="14.3">qu</w>'<w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="14.5">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="14.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ; </l>
		<l n="15" num="2.7"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">n<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="15.4">p<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w>, <w n="15.5">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.6">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ; </l>
		<l n="16" num="2.8"><w n="16.1">F<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>-<w n="16.2">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="16.3">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rg<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>. <del reason="analysis" type="repetition" hand="KL">( bis.)</del></l>
	</lg>
	<lg n="3">
		<head type="speaker">OCTAVIE.</head> 
		<l n="17" num="3.1"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="17.2">f<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.6">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="18" num="3.2"><w n="18.1">Tr<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="18.3">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="18.4">g<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> ; </l>
	</lg>
	<lg n="4">
		<head type="speaker">NINETTE.</head> 
		<l n="19" num="4.1"><w n="19.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="19.2">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rds</w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="19.6">m<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>, </l>
		<l n="20" num="4.2"><w n="20.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="20.3">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>. </l>
	</lg>
	<lg n="5">
		<head type="speaker">OCTAVIE.</head> 
		<l n="21" num="5.1"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>d<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>, <w n="21.2">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rg<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="21.3">l</w>'<w n="21.4">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="21.5">m</w>'<w n="21.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,  </l>
		<l n="22" num="5.2"><w n="22.1">j</w>'<w n="22.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ds</w> <w n="22.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.4">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w> <w n="22.5">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="22.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> !</l>
	</lg>
	<lg n="6">
		<head type="speaker">NINETTE</head>
		<l n="23" num="6.1"><w n="23.1">V<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="23.2">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="23.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>st<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="24" num="6.2"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>d<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="24.2">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> ! <del reason="analysis" type="irrelevant" hand="KL">(I)</del></l>
	</lg>
</div></body></text></TEI>