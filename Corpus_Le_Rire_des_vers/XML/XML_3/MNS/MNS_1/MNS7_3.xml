<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ENSEMBLE ; le mouvement de l'air plus vif.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS7">
			<lg n="1">
				<head type="speaker">NINETTE.</head>
				<l n="1" num="1.1"><w n="1.1"><seg phoneme="w" type="sc" value="0" rule="430">Ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="1.2">c</w>'<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="1.4">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="1.5">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="1.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="1.7">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.8">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w></l>
				<l n="2" num="1.2"><w n="2.1">D<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>x<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>.</l>
				<l n="3" num="1.3"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="3.2">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="3.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">m</w>'<w n="3.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ppr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
				<l n="4" num="1.4"><w n="4.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="4.2">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="4.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="4.5">fl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>,</l>
				<l n="5" num="1.5"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="5.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="5.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="5.5">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w></l>
				<l n="6" num="1.6"><w n="6.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.2">f<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="6.3">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.4">N<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
			</lg>
		</div></body></text></TEI>