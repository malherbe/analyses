<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS17">
	<head type="tune">Air : C'est Lucifer échappé des enfers.</head> 
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="1.2">ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>, </l>
		<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="2.3"><seg phoneme="w" type="sc" value="0" rule="430">ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="2.4">d<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>, </l>
		<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.4">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>, </l>
		<l n="4" num="1.4"><w n="4.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="4.2">j</w>'<w n="4.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ttr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>. </l>
		<l n="5" num="1.5"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="5.2">ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>, </l>
		<l n="6" num="1.6"><w n="6.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.2">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>tʼs</w> <w n="6.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>llʼs</w> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w>, <w n="6.5"><seg phoneme="w" type="sc" value="0" rule="430">ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="6.6">d<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>, </l>
		<l n="7" num="1.7"><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.2">l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s</w></l>
		<l n="8" num="1.8"><w n="8.1">P<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="8.2">qu</w>' <w n="8.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s</w>.</l>
	</lg> 
	<lg n="2">
		<head type="speaker">NINETTE.</head> 
		<l n="9" num="2.1"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="9.3">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">br<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>. </l>
	</lg>
	<lg n="3">
		<head type="speaker">RESTITUE.</head> 
		<l n="10" num="3.1"><w n="10.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="10.3"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="10.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="10.5">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w>, </l>
		<l n="11" num="3.2"><w n="11.1">J</w>' <w n="11.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.5">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>. </l>
	</lg>
	<lg n="4">
		<head type="speaker">NINETTE.</head> 
		<l n="12" num="4.1"><w n="12.1">C</w>'<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w>, <w n="12.4">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> ! </l>
	</lg>
</div></body></text></TEI>