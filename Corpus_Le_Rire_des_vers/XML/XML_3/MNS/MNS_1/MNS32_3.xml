<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS32">
	<head type="tune">Air : Pour plaire à l'ami de mon ami.</head>
	<lg n="1"> 
		<l n="1" num="1.1"><w n="1.1"><seg phoneme="w" type="sc" value="0" rule="430">Ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="1.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="1.3">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.5">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t</w> <w n="1.6">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>, </l>
		<l n="2" num="1.2"><w n="2.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.6">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w></l>
		<l n="3" num="1.3"><w n="3.1">V<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="3.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>-<w n="3.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="3.5">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> </l>
		<l n="4" num="1.4"><w n="4.1">D</w>'<w n="4.2"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> ? </l>
		<l n="5" num="1.5"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="5.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="5.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.6">pr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ; </l>
		<l n="6" num="1.6"><w n="6.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="6.2">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="6.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="6.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, </l>
		<l n="7" num="1.7"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="7.3">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="7.4">d</w>'<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ; </l>
		<l n="8" num="1.8"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <del reason="analysis" type="repetition" hand="KL">(bis)</del> <w n="8.2">c</w>'<w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="8.4">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="8.5">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ! </l>
	</lg>
</div></body></text></TEI>