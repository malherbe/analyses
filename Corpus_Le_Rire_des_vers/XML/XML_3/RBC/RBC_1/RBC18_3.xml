<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE VI.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC18">
				<head type="tune">AIR : Connaissez mieux le grand Eugène.</head>
				<lg n="1">
					<head type="main">LA VALEUR .</head>
					<l n="1" num="1.1"><w n="1.1">Jʼ</w> <w n="1.2">n</w>'<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="1.4">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="1.5">v<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="2.2">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="2.3">n<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="2.4">l</w>'<w n="2.5">h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="2.7">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="2.8">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w></l>
					<l n="3" num="1.3"><w n="3.1">Qu</w>'<w n="3.2">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rs</w> <w n="3.4">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.6">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">D<seg phoneme="ø" type="vs" value="1" rule="404">eu</seg>c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">P<seg phoneme="i" type="vs" value="1" rule="492">y</seg>rrh<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>.</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="5.2">n</w>'<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="5.4">qu</w>'<w n="5.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>rʼ</w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.7">dʼm<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="5.8">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.9">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>cr<seg phoneme="y" type="vs" value="1" rule="453">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="6" num="1.6"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.2">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="6.3">sʼ</w> <w n="6.4">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="6.6">l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>…</l>
					<l n="7" num="1.7"><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="7.3">m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rʼ</w> <w n="7.4">qu</w>'<w n="7.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="7.6">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="7.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.8">r<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="8" num="1.8"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.2">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mmʼs</w> <w n="8.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>.</l>
				</lg>
				<p>
				LE GARÇON.<lb></lb>
				Messieurs et mesdames, l'audience publique va commencer ;
				passez dans le salon du ministre...<lb></lb>
				MADAME COURBETTE, qui tient M. Courbette par la main, et voulant
				le faire passer avant tout le monde.<lb></lb>
				Place !.. place !.. c'est mon mari qui a pris le Louvre !.<lb></lb>
				</p>
				<lg n="2">
					<head type="main">CHŒUR.</head>
					<l n="9" num="2.1"><space unit="char" quantity="4"></space><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="9.2">l</w>'<w n="9.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="9.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="9.7">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="10" num="2.2"><space unit="char" quantity="4"></space><w n="10.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="10.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="10.4">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="10.5">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w></l>
					<l n="11" num="2.3"><space unit="char" quantity="4"></space><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="11.3"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="11.4"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.5">pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="2.4"><space unit="char" quantity="4"></space><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="12.2">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="12.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="12.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="12.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>.</l>
				</lg>
			</div></body></text></TEI>