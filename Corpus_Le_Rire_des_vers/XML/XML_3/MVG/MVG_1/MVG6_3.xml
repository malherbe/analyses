<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER TABLEAU.</head><head type="main_subpart">SCÈNE IV.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG6">
						<head type="tune">AIR : Merveilleuse dans ses vertus.</head>
						<lg n="1">
							<head type="main">BONAPARTE.</head>
							<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="1.5">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> :</l>
							<l n="2" num="1.2"><w n="2.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.2">j<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w>, <w n="2.4">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.5">n<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="2.6">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="3.2">c<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>t</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="3.5">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="3.6">d<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="4" num="1.4"><w n="4.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="4.2">j</w>'<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="4.4">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="4.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">j</w>'<w n="4.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="4.8">r<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> ;</l>
							<l n="5" num="1.5"><space unit="char" quantity="2"></space><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="5.2">r<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>s</w>-<w n="5.5">j<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.7">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
							<l n="6" num="1.6"><space unit="char" quantity="2"></space><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="6.2">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="6.3">h<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="6.4">j</w>'<w n="6.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="6.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
							<l n="7" num="1.7"><space unit="char" quantity="2"></space><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="7.2">c</w>'<w n="7.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="7.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="8" num="1.8"><space unit="char" quantity="2"></space><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">cr<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="8.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.4">v<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
							<l n="9" num="1.9"><w n="9.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x</w> <w n="9.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="9.5">s</w>'<w n="9.6"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> ;</l>
							<l n="10" num="1.10"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="10.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="10.5">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
							<l n="11" num="1.11"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="11.5">R<seg phoneme="ɔ" type="vs" value="1" rule="440">o</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="12" num="1.12"><w n="12.1">V<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xpr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="12.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="12.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">b<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>.</l>
							<l n="13" num="1.13"><space unit="char" quantity="2"></space><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="13.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="13.3">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.4">f<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="14" num="1.14"><space unit="char" quantity="2"></space><w n="14.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="14.2">s<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>il</w> <w n="14.3">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.5">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>.</l>
							<l n="15" num="1.15"><space unit="char" quantity="2"></space><w n="15.1">Ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="15.4">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
							<l n="16" num="1.16"><space unit="char" quantity="2"></space><w n="16.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">c<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="16.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="16.6">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>.</l>
							<l n="17" num="1.17"><w n="17.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="17.3">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="17.4">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="17.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="17.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t</w> <w n="17.7">l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>,</l>
							<l n="18" num="1.18"><w n="18.1">J</w>'<w n="18.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ds</w> <w n="18.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w> <w n="18.5"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="19" num="1.19"><w n="19.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="19.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="19.3">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ls</w> <w n="19.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="19.5">r<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="19.6">l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="20" num="1.20"><w n="20.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="20.2">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x</w> <w n="20.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="20.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.6">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x</w> <w n="20.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.8">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>.</l>
							<l n="21" num="1.21"><space unit="char" quantity="2"></space><w n="21.1">D<seg phoneme="e" type="vs" value="1" rule="408">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="21.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="21.3">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="21.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="22" num="1.22"><space unit="char" quantity="2"></space><w n="22.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.2">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.3">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="22.4">l</w>'<w n="22.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w>.</l>
							<l n="23" num="1.23"><space unit="char" quantity="2"></space><w n="23.1">J</w>'<w n="23.2"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="23.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="23.6">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="23.7">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.8">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
							<l n="24" num="1.24"><space unit="char" quantity="2"></space><w n="24.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.3">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="24.4">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="24.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="24.6">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w>.</l>
							<l n="25" num="1.25"><w n="25.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="25.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="25.4">f<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="25.5">fr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.6">l</w>'<w n="25.7"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r</w> ;</l>
							<l n="26" num="1.26"><w n="26.1">D</w>'<w n="26.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="26.3">h<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="26.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="26.5">h<seg phoneme="i" type="vs" value="1" rule="492">y</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="26.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w>,</l>
							<l n="27" num="1.27"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="27.2">d</w>'<w n="27.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="27.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="27.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="27.6">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x</w> <w n="27.7">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="27.8">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w> :</l>
							<l n="28" num="1.28"><w n="28.1">P<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="28.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.3">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="28.4">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="28.5">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="28.6">f<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="?" type="va" value="1" rule="33">er</seg></w>.</l>
							<l n="29" num="1.29"><space unit="char" quantity="2"></space><w n="29.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="29.2">r<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="29.3">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>, <w n="29.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="29.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.6">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="30" num="1.30"><space unit="char" quantity="2"></space><w n="30.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="30.2">c<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.3">tr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>ph<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w>.</l>
							<l n="31" num="1.31"><space unit="char" quantity="2"></space><w n="31.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="31.2">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p</w> <w n="31.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.4">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.5">l</w>'<w n="31.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="32" num="1.32"><space unit="char" quantity="2"></space><w n="32.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>s</w> <w n="32.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w>….</l>
						</lg>
						<div type="section" n="1">
							<head type="main">bis. {</head>
							<lg n="1">
								<l n="33" num="1.1"><w n="33.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.2">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>il</w>, <w n="33.3">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ! <w n="33.4">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t</w> <w n="33.5">tr<seg phoneme="o" type="vs" value="1" rule="432">o</seg>p</w> <w n="33.6">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pt</w>.</l>
								<l n="34" num="1.2"><w n="34.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="34.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="34.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> ! <w n="34.4">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="34.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
								<l n="35" num="1.3"><w n="35.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.2">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="35.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="35.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="35.6">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
								<l n="36" num="1.4"><w n="36.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="36.2">l</w>'<w n="36.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pr<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="36.4">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="36.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="36.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w>.</l>
							</lg>
							<lg n="2">
								<l n="37" num="2.1"><subst reason="analysis" hand="LG" type="repetition"><del></del><add rend="hidden"><w n="37.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.2">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>il</w>, <w n="37.3">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ! <w n="37.4">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t</w> <w n="37.5">tr<seg phoneme="o" type="vs" value="1" rule="432">o</seg>p</w> <w n="37.6">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pt</w>.</add></subst></l>
								<l n="38" num="2.2"><subst reason="analysis" hand="LG" type="repetition"><del></del><add rend="hidden"><w n="38.1"><seg phoneme="o" type="vs" value="1" rule="414">Ô</seg></w> <w n="38.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="38.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> ! <w n="38.4">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="38.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</add></subst></l>
								<l n="39" num="2.3"><subst reason="analysis" hand="LG" type="repetition"><del></del><add rend="hidden"><w n="39.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.2">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="39.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="39.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="39.6">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></add></subst></l>
								<l n="40" num="2.4"><subst reason="analysis" hand="LG" type="repetition"><del></del><add rend="hidden"><w n="40.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="40.2">l</w>'<w n="40.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pr<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="40.4">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="40.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="40.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w>.</add></subst></l>
							</lg>
						</div>
					</div></body></text></TEI>