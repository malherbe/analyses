<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER TABLEAU.</head><head type="main_subpart">SCÈNE VI.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG11">
						<head type="tune">AIR : J'en guette un petit de mon âge.</head>
						<lg n="1">
							<head type="main">BONAPARTE.</head>
							<l n="1" num="1.1"><w n="1.1">Qu</w>'<w n="1.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="1.3">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="1.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> <w n="1.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">n<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>mm<seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="2" num="1.2"><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="2.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="2.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="2.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rv<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">Ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="3.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="434">o</seg>ff<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.3">m<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="365">e</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="4.5">s</w>'<w n="4.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> …</l>
							<l n="5" num="1.5"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="5.2">n</w>'<w n="5.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
							<l n="6" num="1.6"><space unit="char" quantity="4"></space><w n="6.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w>, <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="6.3">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="6.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w>,</l>
							<l n="7" num="1.7"><space unit="char" quantity="4"></space><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">d<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="7.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.5">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w></l>
							<l n="8" num="1.8"><space unit="char" quantity="4"></space><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199">om</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.5">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="8.6">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						</lg>
					</div></body></text></TEI>