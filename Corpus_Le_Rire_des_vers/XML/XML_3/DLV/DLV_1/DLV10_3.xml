<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
				<title type="sub">INVRAISEMBLANCE EN TROIS PARTIES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
				  <name>
					<forename>Félix-Auguste</forename>
					<surname>DUVERT</surname>
				  </name>
				  <date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="DLV" sort="2">
					<name>
						<forename>Augustin Théodore</forename>
						<nameLink>de</nameLink>
						<surname>LAUZANNE DE VAUROUSSEL</surname>
						<addname type="other">Lauzanne</addname>
					</name>
					<date from="1805" to="1877">1805-1877</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>135 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
                    <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
						<author>F.-A. DUVERT EN SOCIÉTÉ AVEC M. LAUZANNE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?vid=UOM:39015076863409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>University of Michigan Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/mdp.39015076863409</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">16 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DLV10">
	<head type="tune">AIR du Baiser au porteur.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="1.6">qu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> <w n="1.7">ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.8">m</w>'<w n="1.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>… </l>
	</lg>
	<lg n="2">
	<head type="speaker">JONATHAS.</head>
		<l part="I" n="2" num="2.1"><w n="2.1">J</w>'<w n="2.2">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="2.3">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm</w>' <w n="2.4">l</w>'<w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pʼr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>. </l>
	</lg>
	<lg n="3">
	<head type="speaker">PINGOT.</head>
		<l part="F" n="2"><w n="2.6"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w>, <w n="2.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="2.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, </l>
		<l n="3" num="3.1"><w n="3.1">V<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.4">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm</w>' <w n="3.5">l</w>'<w n="3.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>, </l>
		<l n="4" num="3.2"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w>, <w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.5">m<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>m</w>' <w n="4.6">r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, </l>
		<l n="5" num="3.3"><w n="5.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="5.3">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.5">f<seg phoneme="ɛ" type="vs" value="1" rule="365">e</seg>mm</w>' <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.7">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lm<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>. </l>
		<p>( Parle.) C'est clair.</p> 
	</lg>
	<lg n="4">
	<head type="speaker">JONATHAS.</head>
		<l n="6" num="4.1"><w n="6.1">M<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="6.2">j</w>'<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="6.4">b<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w> <w n="6.5">d</w>'<w n="6.6"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.7">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="7" num="4.2"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="7.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>cs</w>, <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> ; </l>
		<l n="8" num="4.3"><w n="8.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="8.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="8.4">j</w>'<w n="8.5">g<seg phoneme="a" type="vs" value="1" rule="339">â</seg>chʼr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="8.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>x<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="9" num="4.4"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="9.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>p<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.4">c<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w>. </l>
		<l n="10" num="4.5"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="10.5">g<seg phoneme="a" type="vs" value="1" rule="339">â</seg>ch<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="10.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>x<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="11" num="4.6"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="11.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>p<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="11.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.4">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">c<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w>. </l>
	</lg>
</div></body></text></TEI>