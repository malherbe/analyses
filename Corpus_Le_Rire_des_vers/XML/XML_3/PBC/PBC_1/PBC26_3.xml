<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC26">
	<head type="tune">AIR de Marianne.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1"><seg phoneme="w" type="sc" value="0" rule="430">Ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="1.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="310">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="1.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="2.2">p<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">j</w>'<w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="2.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>,</l>
		<l n="3" num="1.3"><w n="3.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>sc<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="3.2">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
		<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="4.3">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="4.4">p<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>-<w n="4.5">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="4.6">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>…</l>
		<l n="5" num="1.5"><w n="5.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="5.2">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="5.3">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="6" num="1.6"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="6.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="7" num="1.7"><w n="7.1">T<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> <w n="7.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w></l>
		<l n="8" num="1.8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="8.2">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.4">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>.</l>
		<l n="9" num="1.9"><w n="9.1">P<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.3">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="10" num="1.10"><w n="10.1">R<seg phoneme="e" type="vs" value="1" rule="408">é</seg>d<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="11" num="1.11"><w n="11.1">T<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>,</l>
		<l n="12" num="1.12"><w n="12.1">T<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="12.4">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.5">p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>ts</w>…</l>
		<l n="13" num="1.13"><w n="13.1">J</w>'<w n="13.2">v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="13.3">t</w>'<w n="13.4">pr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w></l>
		<l n="14" num="1.14"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="14.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">gr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="15" num="1.15"><w n="15.1">Qu</w>'<w n="15.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="15.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="15.4">n</w>'<w n="15.5">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="15.6">m</w>'<w n="15.7">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
		<l n="16" num="1.16"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="16.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="16.4">m<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>r</w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="16.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
		<l n="17" num="1.17"><w n="17.1">N</w>'<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="17.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.5">m<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>r</w> <w n="17.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="17.7">b<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
	</lg>
</div></body></text></TEI>