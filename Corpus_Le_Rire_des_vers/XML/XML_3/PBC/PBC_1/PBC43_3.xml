<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC43">
	<head type="tune">AIR du Vaudeville de la Fermière.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="1.2"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="1.4">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="1.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
		<l n="2" num="1.2"><w n="2.1">C</w>'<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="352">e</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="2.4">d</w>'<w n="2.5">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> !</l>
		<l n="3" num="1.3"><w n="3.1">C<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="4" num="1.4"><w n="4.1">L</w>'<w n="4.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>pr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="4.5">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>…</l>
		<l n="5" num="1.5"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="5.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="377">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="6" num="1.6"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="6.2">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="6.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w>,</l>
		<l n="7" num="1.7"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="7.2">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="7.3">l</w>'<w n="7.4"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="443">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="8" num="1.8"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="8.2">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="8.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="8.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w>.</l>
		<l n="9" num="1.9"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="10" num="1.10"><w n="10.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="10.2">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="10.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> :</l>
		<l n="11" num="1.11"><w n="11.1">Qu</w>'<w n="11.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="11.3">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="11.4">ch<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="11.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.6">m<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="12" num="1.12"><w n="12.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="12.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">tr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w>.</l>
		<l n="13" num="1.13"><w n="13.1"><seg phoneme="u" type="vs" value="1" rule="425">Où</seg></w> <w n="13.2">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="13.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
		<l n="14" num="1.14"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">gr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.5">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> ?</l>
		<l n="15" num="1.15"><w n="15.1">V<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="365">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.4">j</w>'<w n="15.5"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="16" num="1.16"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="16.4">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>.</l>
	</lg>
</div></body></text></TEI>