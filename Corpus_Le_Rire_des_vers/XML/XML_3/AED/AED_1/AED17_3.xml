<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">27, 28 ET 29 JUILLET, TABLEAU ÉPISODIQUE DES TROIS JOURNÉES.</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ARA" sort="1">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<author key="DUV" sort="2">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>495 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
						<author>ARAGO ET DUVERT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=xjVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
								<author>ARAGO ET DUVERT</author>
								<repository>Österreichische Nationalbibliothek</repository>
								<idno type="URI">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886206</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : U+02BC MODIFIER LETTER APOSTROPHE) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<change when="2021-06-14" who="RR">Quelques corrections concernant la distribution du signe signe ʼ (UNICODE : ʼ).</change>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME JOURNÉE</head><head type="main_subpart">SCÈNE IX.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="AED17">
			<head type="tune">AIR : Tra la la.</head>
				<lg n="1">
					<head type="main">COLOMBON</head>
					<l n="1" num="1.1"><space unit="char" quantity="10"></space><w n="1.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.2">b<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <del hand="LG" type="repetition" reason="analysis">(bis)</del></l>
					<l n="2" num="1.2"><space unit="char" quantity="10"></space><subst hand="LG" type="repetition" reason="analysis"><del> </del><add rend="hidden"><w n="2.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.2">b<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, </add></subst></l>
					<l n="3" num="1.3"><space unit="char" quantity="2"></space><w n="3.1">C</w>'<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="3.4">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="3.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">jʼ</w> <w n="3.7">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="3.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.9">rʼn<seg phoneme="ɔ̃" type="vs" value="1" rule="199">om</seg></w> !</l>
					<l n="4" num="1.4"><space unit="char" quantity="10"></space><w n="4.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="4.2">b<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <del hand="LG" type="repetition" reason="analysis">(bis)</del></l>
					<l n="5" num="1.5"><space unit="char" quantity="10"></space><subst hand="LG" type="repetition" reason="analysis"><del> </del><add rend="hidden"><w n="5.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="5.2">b<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, </add></subst></l>
					<l n="6" num="1.6"><space unit="char" quantity="2"></space><w n="6.1">Vʼl<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rmʼs</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.6">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>.</l>
					<l n="7" num="1.7"><w n="7.1">J</w>'<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="7.3">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="7.4"><seg phoneme="y" type="vs" value="1" rule="462">u</seg>nʼ</w> <w n="7.5">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w></l>
					<l n="8" num="1.8"><space unit="char" quantity="2"></space><w n="8.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="8.3">p<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="8.4">lʼ</w> <w n="8.5">c<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> :</l>
					<l n="9" num="1.9"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="9.2">v<seg phoneme="o" type="vs" value="1" rule="443">o</seg>tʼ</w> <w n="9.3">c<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.4"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="9.7">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w></l>
					<l n="10" num="1.10"><space unit="char" quantity="2"></space><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gʼ</w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="10.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w>.</l>
					<l n="11" num="1.11"><space unit="char" quantity="10"></space><w n="11.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="11.2">b<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <del hand="LG" type="repetition" reason="analysis">(bis)</del></l>
					<l n="12" num="1.12"><space unit="char" quantity="10"></space><subst hand="LG" type="repetition" reason="analysis"><del> </del><add rend="hidden"><w n="12.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="12.2">b<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, </add></subst></l>
					<l n="13" num="1.13"><space unit="char" quantity="2"></space><w n="13.1">C</w>'<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="13.4">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="13.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">jʼ</w> <w n="13.7">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="13.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="13.9">rʼn<seg phoneme="ɔ̃" type="vs" value="1" rule="199">om</seg></w> !</l>
					<l n="14" num="1.14"><space unit="char" quantity="10"></space><w n="14.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.2">b<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <del hand="LG" type="repetition" reason="analysis">(bis)</del></l>
					<l n="15" num="1.15"><space unit="char" quantity="10"></space><subst hand="LG" type="repetition" reason="analysis"><del> </del><add rend="hidden"><w n="15.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="15.2">b<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, </add></subst></l>
					<l n="16" num="1.16"><space unit="char" quantity="2"></space><subst hand="LG" type="repetition" reason="analysis"><del> </del><add rend="hidden"><w n="16.1">Vʼl<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rmʼs</w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="16.6">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>.</add></subst></l>
					<l n="17" num="1.17"><subst hand="LG" type="repetition" reason="analysis"><del> </del><add rend="hidden"><w n="17.1">J</w>'<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="17.3">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="17.4"><seg phoneme="y" type="vs" value="1" rule="462">u</seg>nʼ</w> <w n="17.5">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w></add></subst></l>
					<l n="18" num="1.18"><space unit="char" quantity="2"></space><subst hand="LG" type="repetition" reason="analysis"><del> </del><add rend="hidden"><w n="18.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="18.3">p<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="18.4">lʼ</w> <w n="18.5">c<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> : </add></subst></l>
					<l n="19" num="1.19"><subst hand="LG" type="repetition" reason="analysis"><del> </del><add rend="hidden"><w n="19.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="19.2">v<seg phoneme="o" type="vs" value="1" rule="443">o</seg>tʼ</w> <w n="19.3">c<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="19.4"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="19.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="19.7">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w></add></subst></l>
					<l n="20" num="1.20"><space unit="char" quantity="2"></space><subst hand="LG" type="repetition" reason="analysis"><del> </del><add rend="hidden"><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gʼ</w> <w n="20.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="20.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="20.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w>. </add></subst></l>
				</lg>
				<lg n="2">
					<l n="21" num="2.1"><space unit="char" quantity="2"></space><w n="21.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="21.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">n<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <w n="21.4">jʼ</w> <w n="21.5">s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="21.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="21.7">g<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rds</w></l>
					<l n="22" num="2.2"><w n="22.1">Qu</w>' <w n="22.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>vʼnt</w> <w n="22.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="22.4">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="22.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.6">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w> <w n="22.7">l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rds</w> ;</l>
					<l n="23" num="2.3"><space unit="char" quantity="2"></space><w n="23.1">M<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="23.2">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="23.3">m<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>, <w n="23.4">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="23.5">qu</w>'<w n="23.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="23.7">b<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>,</l>
					<l n="24" num="2.4"><space unit="char" quantity="2"></space><w n="24.1">J</w>'<w n="24.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>vʼ</w> <w n="24.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="24.4">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>cʼs</w> <w n="24.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>.</l>
					<l n="25" num="2.5"><space unit="char" quantity="10"></space><w n="25.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="25.2">b<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>,</l>
					<l n="26" num="2.6"><space unit="char" quantity="10"></space><w n="26.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="26.2">b<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>,</l>
					<l n="27" num="2.7"><space unit="char" quantity="2"></space><w n="27.1">C</w>'<w n="27.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="27.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="27.4">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="27.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.6">jʼ</w> <w n="27.7">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="27.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="27.9">rʼn<seg phoneme="ɔ̃" type="vs" value="1" rule="199">om</seg></w> !</l>
					<l n="28" num="2.8"><space unit="char" quantity="10"></space><w n="28.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="28.2">b<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>,</l>
					<l n="29" num="2.9"><space unit="char" quantity="2"></space><w n="29.1">Vʼl<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="29.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="29.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rmʼs</w> <w n="29.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="29.6">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>.</l>
				</lg>
		</div></body></text></TEI>