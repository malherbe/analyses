<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">27, 28 ET 29 JUILLET, TABLEAU ÉPISODIQUE DES TROIS JOURNÉES.</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ARA" sort="1">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<author key="DUV" sort="2">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>495 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
						<author>ARAGO ET DUVERT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=xjVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
								<author>ARAGO ET DUVERT</author>
								<repository>Österreichische Nationalbibliothek</repository>
								<idno type="URI">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886206</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : U+02BC MODIFIER LETTER APOSTROPHE) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<change when="2021-06-14" who="RR">Quelques corrections concernant la distribution du signe signe ʼ (UNICODE : ʼ).</change>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE JOURNÉE.</head><head type="main_subpart">SCÈNE PREMIÈRE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="AED1">
			<head type="tune">AIR : Ces postillons sont d'une maladresse.</head>
			<lg n="1">
				<head type="main">RAIMOND, se levant.</head>
				<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="1.2">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>, <w n="1.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>r</w>, <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="1.6">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.8">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="1.9">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="2" num="1.2"><w n="2.1">R<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="2.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.3">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>ts</w> <w n="2.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> ;</l>
				<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="3.2">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="3.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="304">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.4">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>.</l>
				<l n="5" num="1.5"><w n="5.1">Br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>, <w n="5.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>-<w n="5.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="5.5">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="o" type="vs" value="1" rule="434">o</seg>tt<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> ?</l>
				<l n="6" num="1.6"><w n="6.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="6.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> ! <w n="6.3">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> ! <w n="6.4">j</w>'<w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="6.7">l</w>'<w n="6.8"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xp<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="377">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				<l n="7" num="1.7"><w n="7.1">D<seg phoneme="e" type="vs" value="1" rule="408">é</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.2">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w> ! <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.4">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="7.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="7.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="7.8">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c</w> ;</l>
				<l n="8" num="1.8"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>, <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="8.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">l</w>'<w n="8.7">h<seg phoneme="o" type="vs" value="1" rule="434">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.8"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="9.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> !</l>
			</lg>
		</div></body></text></TEI>