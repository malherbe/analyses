<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANDRÉ LE CHANSONNIER</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="FTN" sort="1">
					<name>
						<forename>Louis Marie</forename>
						<surname>FONTAN</surname>
					</name>
					<date from="1801" to="1839">1801-1839</date>
				</author>
				<author key="DNY" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>DESNOYER</surname>
					</name>
					<date from="1806" to="1858">1806-1858</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>239 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>André le chansonnier.</title>
						<author>FONTAN ET DESNOYER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=aN6oyNpF3cMC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>André le chansonnier.</title>
								<author>FONTAN ET DESNOYER</author>
								<repository>Biblioteca Casanatense</repository>
								<idno type="URI">http://opac.casanatense.it/Record.htm?idlist=3</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Ode et Wodon</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-15" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE I</head><head type="main_subpart">SCÈNE VI.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FED4">
				<head type="tune">AIR : Aux armes, janissaires ! de M.Herold. ( Missolonghi. )</head>
					<p>(A voix basse.)</p>
					<lg n="1">
						<head type="main">ANDRÉ.</head>
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="1.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="1.4">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">l</w>'<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="2.4">v<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> ! <w n="2.5">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="2.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w></l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="3.3">pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">S<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>v<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="4.2">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>.</l>
					</lg>
					<p>Changeant de ton, et plus bas encore.)</p>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="10"></space><w n="5.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <w n="5.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="5.3">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> !</l>
						<l n="6" num="2.2"><space unit="char" quantity="6"></space><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="6.2">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="œ" type="vs" value="1" rule="285">œ</seg>il</w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="7" num="2.3"><space unit="char" quantity="10"></space><w n="7.1">S<seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>str<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="6"></space><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="8.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="8.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="8.5">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>.</l>
					</lg>
					<p>Nouveau changement de ton.)</p>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="8"></space><w n="9.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="10.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="10.4">f<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="10.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="10.6">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> !</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space><w n="11.1">G<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.2">qu</w>'<w n="11.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="11.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">s</w>'<w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>squ<seg phoneme="i" type="vs" value="1" rule="490">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="3.4"><space unit="char" quantity="10"></space><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w>'<w n="12.3">h<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>.</l>
						<l n="13" num="3.5"><space unit="char" quantity="16"></space><w n="13.1">C</w>'<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="13.3">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>,</l>
						<l n="14" num="3.6"><space unit="char" quantity="16"></space><w n="14.1">C</w>'<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="14.3">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>,</l>
						<l n="15" num="3.7"><space unit="char" quantity="14"></space><w n="15.1">C</w>'<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="15.3">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="15.4">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> !</l>
						<l n="16" num="3.8"><space unit="char" quantity="18"></space><w n="16.1"><seg phoneme="w" type="sc" value="0" rule="430">Ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> !</l>
					</lg>
					<p>Très bas, et ayant l'air de se cacher.)</p>
					<lg n="4">
						<l n="17" num="4.1"><w n="17.1">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="17.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="17.3">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>scr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w>, <w n="17.4">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="17.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="17.7">j</w>'<w n="17.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>pl<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="4.2"><w n="18.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="18.3">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="18.5">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>,</l>
						<l n="19" num="4.3"><w n="19.1">F<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="19.3"><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="19.6">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>,</l>
						<l n="20" num="4.4"><w n="20.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>, <w n="20.2">gr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="20.4">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="20.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.6">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="20.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="21" num="4.5"><w n="21.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="21.2">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="21.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="21.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="21.5">j</w>'<w n="21.6"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.7">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.8">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ppl<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
						<l n="22" num="4.6"><w n="22.1">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> ! <w n="22.2">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="22.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="22.4"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="22.6">p<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</l>
						<l n="23" num="4.7"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="23.2">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="23.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <subst type="repetition" reason="analysis" hand="LG"><del>etc.</del><add rend="hidden"><w n="23.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="23.6">p<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</add></subst></l>
					</lg>
					<p>Nouveau changement de ton.)</p>
					<lg n="5">
						<l n="24" num="5.1"><space unit="char" quantity="8"></space><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="24.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="24.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="24.4">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="25" num="5.2"><space unit="char" quantity="8"></space><w n="25.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="25.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="25.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>s</w>,</l>
						<l n="26" num="5.3"><space unit="char" quantity="8"></space><w n="26.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="26.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="26.3">pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="27" num="5.4"><space unit="char" quantity="8"></space><w n="27.1">C<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.2">f<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>, <w n="27.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.5">t<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>s</w>.</l>
					</lg>
					<p>Très fort et très gaîment.)</p>
					<lg n="6">
						<l n="28" num="6.1"><space unit="char" quantity="10"></space><w n="28.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <w n="28.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="28.3">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> !</l>
						<l n="29" num="6.2"><space unit="char" quantity="6"></space><w n="29.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="29.2">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.3"><seg phoneme="œ" type="vs" value="1" rule="285">œ</seg>il</w>, <w n="29.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.5">t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="30" num="6.3"><space unit="char" quantity="10"></space><w n="30.1">S<seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="30.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>str<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="31" num="6.4"><space unit="char" quantity="6"></space><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="31.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="31.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.4">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="31.5">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>.</l>
					</lg>
				</div></body></text></TEI>