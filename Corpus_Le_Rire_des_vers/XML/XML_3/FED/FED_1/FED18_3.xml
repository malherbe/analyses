<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANDRÉ LE CHANSONNIER</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="FTN" sort="1">
					<name>
						<forename>Louis Marie</forename>
						<surname>FONTAN</surname>
					</name>
					<date from="1801" to="1839">1801-1839</date>
				</author>
				<author key="DNY" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>DESNOYER</surname>
					</name>
					<date from="1806" to="1858">1806-1858</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>239 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>André le chansonnier.</title>
						<author>FONTAN ET DESNOYER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=aN6oyNpF3cMC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>André le chansonnier.</title>
								<author>FONTAN ET DESNOYER</author>
								<repository>Biblioteca Casanatense</repository>
								<idno type="URI">http://opac.casanatense.it/Record.htm?idlist=3</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Ode et Wodon</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-15" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE SECOND.</head><head type="main_subpart">SCÈNE XIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FED18">
					<head type="tune">Même air.</head>
					<lg n="1">
						<head type="main">LOUISE.</head>
						<l n="1" num="1.1"><space unit="char" quantity="4"></space><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="1.3">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="1.4">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> ;</l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">P<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w>, <w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="2.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="2.5">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.6"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>… <w n="3.4">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="3.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="3.8">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>.</l>
						<p>A part.)</p>
						<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> ! <w n="4.3">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="o" type="vs" value="1" rule="443">o</seg></w> ! <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="4.5">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="5" num="1.5"><space unit="char" quantity="4"></space><w n="5.1">P<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">An</seg>dr<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ! <w n="5.3">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.4">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>, <w n="5.5">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> !</l>
						<l n="6" num="1.6"><space unit="char" quantity="4"></space><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="6.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="6.3">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="6.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="6.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.7">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>pl<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="7" num="1.7"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="7.4">t<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w>, <w n="7.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="7.6">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="7.7">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.8">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="7.9">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> :</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="8.2">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="8.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>