<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L'HOMME QUI BAT SA FEMME</title>
				<title type="sub">TABLEAU POPULAIRE EN UN ACTE, MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DML" sort="1">
				  <name>
					<forename>Julien</forename>
					<nameLink>de</nameLink>
					<surname>MAILLAN</surname>
					<addname type="other">Julien</addname>
					<addname type="other">Julien de M.</addname>
				  </name>
				  <date from="1805" to="1851">1805-1851</date>
				</author>
				<author key="DNR" sort="2">
				  <name>
					<forename>Philippe-François</forename>
					<surname>PINEL</surname>
					<addname type="pen_name">DUMANOIR</addname>
					<addname type="other">Philippe Dumanoir</addname>
					<addname type="other">Philippe D.</addname>
					<addname type="other">Philippe D***</addname>
				  </name>
				  <date from="1806" to="1865">1806-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DMP_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
          <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L'HOMME QUI BAT SA FEMME</title>
						<author>JULIEN DE MALLIAN ET PHILIPPE DUMANOIR</author>
					</titleStmt>
					<publicationStmt>
						<publisher>INTERNET ARCHIVE</publisher>
						<idno type="URL">https ://archive.org/details/lhommequibatsafe00malluoft</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>University of Toronto Libraries</repository>
								<idno type="URL">https ://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991105986034006196</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">8 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DMP3">
	<head type="tune">AIR : La belle chose qu'un assaut ! (Sergent Mathieu)</head>
		<div type="section" n="1">
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>pʼs</w> <w n="1.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="1.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> :</l>
				<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.3">p<seg phoneme="o" type="vs" value="1" rule="434">o</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w>' <w n="2.5">f<seg phoneme="ɛ" type="vs" value="1" rule="365">e</seg>mm</w>' <w n="2.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				<l n="3" num="1.3"><w n="3.1">F<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r</w>' <w n="3.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.3">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>… <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="3.5">qu</w>' <w n="3.6">p<seg phoneme="o" type="vs" value="1" rule="434">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="4" num="1.4"><w n="4.1">V<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <del reason="analysis" type="repetition" hand="KL">(bis.)</del> <w n="4.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>pʼs</w>, <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.5">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>.</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1"><w n="5.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="5.2">qu</w>'<w n="5.3">l</w>'<w n="5.4"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="5.5">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.6">c<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				<l n="6" num="2.2"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="6.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.4">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="6.5">s</w>'<w n="6.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="6.7">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="6.8">g<seg phoneme="ɛ" type="vs" value="1" rule="304">aî</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w></l>
				<l n="7" num="2.3"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="7.2">fr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w>' <w n="7.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="y" type="vs" value="1" rule="449">u</seg>l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				<l n="8" num="2.4"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="8.2">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="8.3">d<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">l</w>'<w n="8.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>gr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>.</l>
				<l n="9" num="2.5"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="9.2">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				<l n="10" num="2.6"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="10.2">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="10.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.5">gʼn<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w></l>
				<l n="11" num="2.7"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.2">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>ts</w> <w n="11.3">d</w>'<w n="11.4">l</w>'<w n="11.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>str<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				<l n="12" num="2.8"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="12.2">n</w>'<w n="12.3">c<seg phoneme="u" type="vs" value="1" rule="424">oû</seg>t</w>' <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="12.5">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="12.6">d</w>'<w n="12.7">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="12.8">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>.</l>
			</lg>
		</div> 
		<p>
			(parlé.), Un bonnet, un foulard anglais de Lyon… ça lui fait plai-<lb></lb>
			sir, à la petite mère, çâ la flatte ; et pour lors… (Il fait le signe<lb></lb>
			d'embrasser.)<lb></lb>
			(Reprise.)<lb></lb>
			Pour les princip's, etc.
		</p>
		<div type="section" n="2">
			<head type="main">DEUXIÈME COUPLET.</head>
			<lg n="1">
				<l n="13" num="1.1"><w n="13.1">S</w>'<w n="13.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="13.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="13.7">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.8">f<seg phoneme="a" type="vs" value="1" rule="192">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				<l n="14" num="1.2"><w n="14.1">Qu</w>'<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="14.4">d</w>'<w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.6">gr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="14.8">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="14.9">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w> <w n="14.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.11">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
				<l n="15" num="1.3"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="15.2">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="15.3">lʼs</w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="15.6">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				<l n="16" num="1.4"><w n="16.1">R<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ct</w> <w n="16.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="16.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="16.4">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>pr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
				<l n="17" num="1.5"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="17.2">d</w>'<w n="17.3">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.4">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				<l n="18" num="1.6"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="18.2">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.3">d</w>'<w n="18.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.5">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="18.6">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w></l>
				<l n="19" num="1.7"><w n="19.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="19.2">c<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="19.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				<l n="20" num="1.8"><w n="20.1">Qu</w>'<w n="20.2">l</w>'<w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="358">en</seg>nʼm<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="20.4">n</w>'<w n="20.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>.</l>
			</lg>
	</div> 
</div></body></text></TEI>