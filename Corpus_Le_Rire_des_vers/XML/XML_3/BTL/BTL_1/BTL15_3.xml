<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CONSEIL DE RÉVISION, OU LES MAUVAIS NUMÉROS</title>
				<title type="sub">TABLEAU-VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="BRU" sort="1">
					<name>
						<forename>Léon-Lévy</forename>
						<surname>BRUNSWICK</surname>
						<addname type="other">Léon LHÉRIE</addname>
					</name>
					<date from="1805" to="1859">1805-1859</date>
				</author>
				<author key="THO" sort="2">
				  <name>
					<forename>Mathieu Barthélemy</forename>
					<surname>THOUIN</surname>
					<addname type="other">Barthélemy</addname>
				  </name>
				  <date from="1804" to="18..">1804-18..</date>
				</author>
				<author key="LVY" sort="3">
				  <name>
					<forename>Léon-Victor</forename>
					<surname>LÉVY</surname>
					<addname type="pen_name">Victor LHÉRIE</addname>
					<addname type="other">Lhérie</addname>
				  </name>
				  <date from="1808" to="1845">1808-1845</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>212 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BTL_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CONSEIL DE RÉVISION, OU LES MAUVAIS NUMÉROS</title>
						<author>BRUNSWICK, BARTHÉLEMY ET LHÉRIE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=NYPL :33433075800833</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>New York Public Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/nyp.33433075800833</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">4 AOÛT 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BTL15">
	<lg n="1">
	<head type="speaker">FRANCIS.</head>
	<p>
		C'est bien de prendre votre<lb></lb>
		parti. ( A Adèle qui se dispose à suivre Au<lb></lb>
		guste.) Restez donc ici, jeunesse, vous ne<lb></lb>
		pouvez traverser la ville sous ce déguise<lb></lb>
		ment ; l'ami Auguste va vous envoyer tout<lb></lb>
		ce qu'il vous faut. Allons, mon brave, rap<lb></lb>
		pelez-vous ce que je vous disais ce matin.<lb></lb> 
	</p>
		<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>sp<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="2" num="1.2"><w n="2.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>f<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
		<l n="3" num="1.3"><w n="3.1">C</w>'<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>fr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w></l>
		<l n="4" num="1.4"><w n="4.1">D<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="4.2">f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>.</l>
	</lg> 
</div></body></text></TEI>