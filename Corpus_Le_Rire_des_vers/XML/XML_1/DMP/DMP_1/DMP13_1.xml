<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L'HOMME QUI BAT SA FEMME</title>
				<title type="sub">TABLEAU POPULAIRE EN UN ACTE, MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DML" sort="1">
				  <name>
					<forename>Julien</forename>
					<nameLink>de</nameLink>
					<surname>MAILLAN</surname>
					<addname type="other">Julien</addname>
					<addname type="other">Julien de M.</addname>
				  </name>
				  <date from="1805" to="1851">1805-1851</date>
				</author>
				<author key="DNR" sort="2">
				  <name>
					<forename>Philippe-François</forename>
					<surname>PINEL</surname>
					<addname type="pen_name">DUMANOIR</addname>
					<addname type="other">Philippe Dumanoir</addname>
					<addname type="other">Philippe D.</addname>
					<addname type="other">Philippe D***</addname>
				  </name>
				  <date from="1806" to="1865">1806-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DMP_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
          <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L'HOMME QUI BAT SA FEMME</title>
						<author>JULIEN DE MALLIAN ET PHILIPPE DUMANOIR</author>
					</titleStmt>
					<publicationStmt>
						<publisher>INTERNET ARCHIVE</publisher>
						<idno type="URL">https ://archive.org/details/lhommequibatsafe00malluoft</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>University of Toronto Libraries</repository>
								<idno type="URL">https ://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991105986034006196</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">8 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DMP13">
	<head type="tune">AIR : Sortez à l'instant, sortez.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Me</w> <w n="1.2">résister</w> <w n="1.3">de</w> <w n="1.4">la</w> <w n="1.5">sorte</w> !</l>
		<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">colère</w> <w n="2.3">me</w> <w n="2.4">transporte</w> !</l>
		<l n="3" num="1.3"><w n="3.1">Quoi</w> ! <w n="3.2">tu</w> <w n="3.3">peux</w></l>
		<l n="4" num="1.4"><w n="4.1">Dʼvant</w> <w n="4.2">mes</w> <w n="4.3">yeux</w></l>
		<l n="5" num="1.5"><w n="5.1">Te</w> <w n="5.2">montrer</w> <w n="5.3">audacieux</w> !</l>
		<l n="6" num="1.6"><w n="6.1">Après</w> <w n="6.2">dix</w> <w n="6.3">ans</w> <w n="6.4">de</w> <w n="6.5">ménage</w></l>
		<l n="7" num="1.7"><w n="7.1">Je</w> <w n="7.2">reçois</w> <w n="7.3">un</w> <w n="7.4">tel</w> <w n="7.5">outrage</w> !</l>
		<l n="8" num="1.8"><w n="8.1">C</w>'<w n="8.2">est</w> <w n="8.3">affreux</w> ! <w n="8.4">un</w> <w n="8.5">mari</w></l>
		<l n="9" num="1.9"><w n="9.1">Peut</w>-<w n="9.2">il</w> <w n="9.3">se</w> <w n="9.4">conduire</w> <w n="9.5">ainsi</w> ?</l>
		<l n="10" num="1.10"><w n="10.1">Cède</w>, <w n="10.2">quand</w> <w n="10.3">j</w>'<w n="10.4">ordonne</w> !</l>
	</lg>
	<lg n="2">
	<head type="speaker">PICHARD.</head>
		<l n="11" num="2.1"><w n="11.1">Sois</w> <w n="11.2">gentille</w> <w n="11.3">et</w> <w n="11.4">bonne</w>.</l>
	</lg>
	<lg n="3">
	<head type="speaker">SUZANNE.</head>
		<l n="12" num="3.1"><w n="12.1">Non</w>, <w n="12.2">jamais</w> ! <w n="12.3">point</w> <w n="12.4">d</w>'<w n="12.5">pardon</w> !</l>
		<l n="13" num="3.2"><w n="13.1">Mais</w> <w n="13.2">ce</w> <w n="13.3">feu</w>, <w n="13.4">l</w>'<w n="13.5">allumʼra</w>-<w n="13.6">t</w>-<w n="13.7">on</w> ?</l>
		<l n="14" num="3.3"><w n="14.1">Cède</w> <w n="14.2">quand</w> <w n="14.3">j</w>'<w n="14.4">ordonne</w> ;</l>
		<l n="15" num="3.4"><w n="15.1">Jamais</w> <w n="15.2">je</w> <w n="15.3">n</w>'<w n="15.4">pardonne</w> !</l>
		<l n="16" num="3.5"><w n="16.1">Et</w> <w n="16.2">la</w> <w n="16.3">preuv</w>', <w n="16.4">la</w> <w n="16.5">voilà</w> !</l>
		<stage>( Elle lui donne un soufflet.)</stage>
	</lg>
	<lg n="4">
	<head type="speaker">PICHARD.</head>
		<l n="17" num="4.1"><w n="17.1">Je</w> <w n="17.2">rʼçois</w>-<w n="17.3">t</w>-<w n="17.4">y</w> <w n="17.5">cʼte</w> <w n="17.6">preuve</w>-<w n="17.7">là</w> !</l>
	</lg>
</div></body></text></TEI>