<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC18">
	<head type="tune">AIR : T'en souviens-tu ?</head> 
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Tant</w> <w n="1.2">de</w> <w n="1.3">rigueur</w> <w n="1.4">a</w> <w n="1.5">droit</w> <w n="1.6">de</w> <w n="1.7">me</w> <w n="1.8">surprendre</w>,</l>
		<l n="2" num="1.2"><w n="2.1">Vous</w> <w n="2.2">m</w>' <w n="2.3">refusez</w> <w n="2.4">un</w> <w n="2.5">peu</w> <w n="2.6">de</w> <w n="2.7">lait</w> ! … <w n="2.8">pourtant</w></l>
		<l n="3" num="1.3"><w n="3.1">Quand</w> <w n="3.2">j</w>'<w n="3.3">suis</w> <w n="3.4">aux</w> <w n="3.5">champs</w> <w n="3.6">je</w> <w n="3.7">pourrais</w> <w n="3.8">vous</w> <w n="3.9">en</w> <w n="3.10">prendre</w>;</l>
		<l n="4" num="1.4"><w n="4.1">Mais</w> <w n="4.2">la</w> <w n="4.3">probité</w> <w n="4.4">m</w>'<w n="4.5">le</w> <w n="4.6">défend</w>.</l>
	</lg>
	<lg n="2">
		<l n="5" num="2.1"><w n="5.1">Allez</w>, <w n="5.2">Monsieur</w>, <w n="5.3">n</w>'<w n="5.4">faut</w> <w n="5.5">mépriser</w> <w n="5.6">personne</w>,</l>
		<l n="6" num="2.2"><w n="6.1">Sous</w> <w n="6.2">des</w> <w n="6.3">haillons</w> <w n="6.4">l</w>'<w n="6.5">honneur</w> <w n="6.6">peut</w> <w n="6.7">ben</w> <w n="6.8">parler</w>.</l>
		<l n="7" num="2.3"><w n="7.1">Le</w> <w n="7.2">pauvre</w> <w n="7.3">honnête</w> <w n="7.4">sait</w> <w n="7.5">attendr</w>' <w n="7.6">qu</w>'<w n="7.7">on</w> <w n="7.8">lui</w> <w n="7.9">donne</w>,</l>
		<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">meurt</w> <w n="8.3">de</w> <w n="8.4">faim</w> <w n="8.5">plutôt</w> <w n="8.6">que</w> <w n="8.7">de</w> <w n="8.8">voler</w>.</l>
	</lg>
</div></body></text></TEI>