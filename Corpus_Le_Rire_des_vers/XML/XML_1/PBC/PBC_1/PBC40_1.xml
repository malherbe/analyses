<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC40">
	<head type="tune">AIR : Bataille, bataille.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">J</w>'<w n="1.2">arrive</w>,<del reason="analysis" type="repetition" hand="KL">(bis)</del></l>
		<l n="2" num="1.2"><w n="2.1">Mon</w> <w n="2.2">prince</w>, <w n="2.3">je</w> <w n="2.4">suis</w> <w n="2.5">en</w> <w n="2.6">amour</w></l>
		<l n="3" num="1.3"><w n="3.1">Naïve</w>,<del reason="analysis" type="repetition" hand="KL">(bis)</del></l>
		<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">sans</w> <w n="4.3">détour</w>.</l>
	</lg>
	<lg n="2">
	<head type="speaker">AZOLIN.</head>
		<l n="5" num="2.1"><w n="5.1">Tant</w> <w n="5.2">mieux</w>. <w n="5.3">Soyez</w> <w n="5.4">la</w> <w n="5.5">bien</w> <w n="5.6">venue</w>.</l> 
		<l n="6" num="2.2"><del reason="analysis" type="irrelevant" hand="KL">(A part.)</del> <w n="6.1">Elle</w> <w n="6.2">me</w> <w n="6.3">paraît</w> <w n="6.4">ingénue</w>.</l>
	</lg>
	<lg n="3">
	<head type="speaker">LA LAITIÈRE.</head>
		<l n="7" num="3.1"><w n="7.1">Quoiqu</w>'<w n="7.2">mon</w> <w n="7.3">villag</w>'<w n="7.4">soit</w> <w n="7.5">près</w> <w n="7.6">d</w>' <w n="7.7">Paris</w>,</l>
		<l n="8" num="3.2"><w n="8.1">De</w> <w n="8.2">la</w> <w n="8.3">sagess</w>', <w n="8.4">dans</w> <w n="8.5">mon</w> <w n="8.6">pays</w>,</l>
		<l n="9" num="3.3"><w n="9.1">On</w> <w n="9.2">m</w>'<w n="9.3">destine</w> <w n="9.4">le</w> <w n="9.5">prix</w>.</l>
		<l ana="unanalyzable" n="10" num="3.4">J'arrive, etc.</l>
	</lg>
</div></body></text></TEI>