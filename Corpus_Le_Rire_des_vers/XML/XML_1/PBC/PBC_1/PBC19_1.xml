<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC19">
	<head type="tune">AIR : De la Coquette.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Sans</w> <w n="1.2">être</w> <w n="1.3">coquette</w>,</l>
		<l n="2" num="1.2"><w n="2.1">Devant</w> <w n="2.2">un</w> <w n="2.3">miroir</w>,</l>
		<l n="3" num="1.3"><w n="3.1">En</w> <w n="3.2">riche</w> <w n="3.3">toilette</w>,</l>
		<l n="4" num="1.4"><w n="4.1">On</w> <w n="4.2">aime</w> <w n="4.3">à</w> <w n="4.4">se</w> <w n="4.5">voir</w>.</l>
	</lg> 
	<lg n="2"> 
		<l n="5" num="2.1"><w n="5.1">La</w> <w n="5.2">glace</w> <w n="5.3">polie</w>,</l>
		<l n="6" num="2.2"><w n="6.1">Semble</w> <w n="6.2">dire</w>, <w n="6.3">hélas</w> !</l>
		<l n="7" num="2.3"><w n="7.1">Que</w> <w n="7.2">je</w> <w n="7.3">suis</w> <w n="7.4">jolie</w>,</l>
		<l n="8" num="2.4"><w n="8.1">Mais</w> <w n="8.2">je</w> <w n="8.3">n</w>'<w n="8.4">y</w> <w n="8.5">crois</w> <w n="8.6">pas</w>.</l>
	</lg> 
	<lg n="3">   
		<l n="9" num="3.1"><w n="9.1">Quel</w> <w n="9.2">éclat</w> <w n="9.3">extrême</w> !</l>
		<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">personne</w> <w n="10.3">ici</w>…</l>
		<l n="11" num="3.3"><w n="11.1">Si</w> <w n="11.2">celui</w> <w n="11.3">que</w> <w n="11.4">j</w>'<w n="11.5">aime</w></l>
		<l n="12" num="3.4"><w n="12.1">Me</w> <w n="12.2">voyait</w> <w n="12.3">ainsi</w>…</l>
	</lg>
	<lg n="4">    
		<l n="13" num="4.1"><w n="13.1">Que</w> <w n="13.2">cette</w> <w n="13.3">toilette</w>,</l>
		<l n="14" num="4.2"><w n="14.1">Devant</w> <w n="14.2">un</w> <w n="14.3">miroir</w>,</l>
		<l n="15" num="4.3"><w n="15.1">Sans</w> <w n="15.2">être</w> <w n="15.3">coquette</w>,</l>
		<l n="16" num="4.4"><w n="16.1">Fait</w> <w n="16.2">plaisir</w> <w n="16.3">à</w> <w n="16.4">voir</w> !</l>
	</lg> 
	<lg n="5">  
		<l n="17" num="5.1"><w n="17.1">La</w> <w n="17.2">parure</w> <w n="17.3">entière</w>,</l>
		<l n="18" num="5.2"><w n="18.1">Le</w> <w n="18.2">bandeau</w> <w n="18.3">virginal</w>…</l>
		<l n="19" num="5.3"><w n="19.1">Pour</w> <w n="19.2">une</w> <w n="19.3">bergère</w></l>
		<l n="20" num="5.4"><w n="20.1">Je</w> <w n="20.2">ne</w> <w n="20.3">suis</w> <w n="20.4">pas</w> <w n="20.5">mal</w>…</l>
	</lg> 
	<lg n="6"> 
		<l n="21" num="6.1"><w n="21.1">Triste</w>, <w n="21.2">solitaire</w>,</l>
		<l n="22" num="6.2"><w n="22.1">Et</w> <w n="22.2">loin</w> <w n="22.3">des</w> <w n="22.4">galans</w>,</l>
		<l n="23" num="6.3"><w n="23.1">Je</w> <w n="23.2">puis</w> <w n="23.3">bien</w> <w n="23.4">me</w> <w n="23.5">faire</w></l>
		<l n="24" num="6.4"><w n="24.1">Quelques</w> <w n="24.2">complimens</w>.</l>
	</lg> 
	<lg n="7">    
		<l n="25" num="7.1"><w n="25.1">Quand</w> <w n="25.2">on</w> <w n="25.3">est</w> <w n="25.4">coquette</w>,</l>
		<l n="26" num="7.2"><w n="26.1">Devant</w> <w n="26.2">un</w> <w n="26.3">miroir</w> ! …</l>
		<l n="27" num="7.3"><w n="27.1">Même</w> <w n="27.2">étant</w> <w n="27.3">seulette</w>,</l>
		<l n="28" num="7.4"><w n="28.1">On</w> <w n="28.2">aime</w> <w n="28.3">à</w> <w n="28.4">se</w> <w n="28.5">voir</w>.</l>
	</lg>
</div></body></text></TEI>