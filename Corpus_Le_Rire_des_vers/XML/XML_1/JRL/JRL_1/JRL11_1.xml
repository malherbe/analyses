<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHABERT</title>
				<title type="sub">HISTOIRE CONTEMPORAINE EN DEUX ACTES, MÊLÉE DE CHANT</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="ARJ" sort="1">
					<name>
						<forename>Jacques</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1790" to="1855">1790-1855</date>
				</author>
				<author key="LUR" sort="2">
					<name>
						<forename>Louis</forename>
						<surname>LURINE</surname>
					</name>
					<date from="1810" to="1860">1810-1860</date>
				</author>
				<editor>Le rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>110 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">JRL_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
				   </p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CHABERT</title>
						<author>JACQUES ARAGO ET LOUIS LURINE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=vldoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849877.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">2 JUILLET 1832</date>
				<placeName>
					<settlement>THÉÂTRE NATIONAL DU VAUDEVILLE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="JRL11">
	<head type="tune">AIR : Au brave Hussard du deuxième, (des Mauvaises têtes.)</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Du</w> <w n="1.2">temps</w> <w n="1.3">passé</w> <w n="1.4">seigneur</w> <w n="1.5">parfumé</w> <w n="1.6">d</w>'<w n="1.7">ambre</w>,</l>
		<l n="2" num="1.2"><w n="2.1">Quel</w> <w n="2.2">est</w> <w n="2.3">le</w> <w n="2.4">prix</w>, <w n="2.5">dites</w>-<w n="2.6">moi</w>, <w n="2.7">d</w>'<w n="2.8">un</w> <w n="2.9">blason</w> ?</l>
		<l n="3" num="1.3"><w n="3.1">Combien</w> <w n="3.2">de</w> <w n="3.3">fois</w> <w n="3.4">fîtes</w>-<w n="3.5">vous</w> <w n="3.6">antichambre</w></l>
		<l n="4" num="1.4"><w n="4.1">Afin</w> <w n="4.2">de</w> <w n="4.3">voir</w> <w n="4.4">alonger</w> <w n="4.5">votre</w> <w n="4.6">nom</w> ?</l>
		<l n="5" num="1.5"><w n="5.1">Pour</w> <w n="5.2">mériter</w> <w n="5.3">quelques</w> <w n="5.4">faveurs</w>, <w n="5.5">naguère</w>,</l>
		<l n="6" num="1.6"><w n="6.1">Fallait</w> <w n="6.2">grandir</w>, <w n="6.3">non</w> <w n="6.4">se</w> <w n="6.5">rapetisser</w> :</l>
		<l n="7" num="1.7"><w n="7.1">On</w> <w n="7.2">les</w> <w n="7.3">attend</w> <w n="7.4">aujourd</w>'<w n="7.5">hui</w> <w n="7.6">terre</w> <w n="7.7">à</w> <w n="7.8">terre</w>,</l>
		<l n="8" num="1.8"><w n="8.1">Sans</w> <w n="8.2">doute</w> <w n="8.3">afin</w> <w n="8.4">de</w> <w n="8.5">mieux</w> <w n="8.6">les</w> <w n="8.7">ramasser</w>.</l>
	</lg>
</div></body></text></TEI>