<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB14">
	<head type="tune">Air : Voilà trois ans qu'en co village. ( Léocadie.)</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Hélas</w> ! <w n="1.2">encore</w> <w n="1.3">un</w> <w n="1.4">mariage</w>,</l>
		<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">pourtant</w> <w n="2.3">ce</w> <w n="2.4">n</w>'<w n="2.5">est</w> <w n="2.6">pas</w> <w n="2.7">le</w> <w n="2.8">mien</w> !</l>
		<l n="3" num="1.3"><w n="3.1">J</w>'<w n="3.2">ai</w> <w n="3.3">seize</w> <w n="3.4">ans</w>… <w n="3.5">on</w> <w n="3.6">dit</w> <w n="3.7">qu</w>'<w n="3.8">à</w> <w n="3.9">cet</w> <w n="3.10">âge</w>,</l>
		<l n="4" num="1.4"><w n="4.1">On</w> <w n="4.2">doit</w> <w n="4.3">former</w> <w n="4.4">un</w> <w n="4.5">doux</w> <w n="4.6">lien</w>,</l>
		<l n="5" num="1.5"><w n="5.1">J</w>'<w n="5.2">en</w> <w n="5.3">pleurerais</w> <w n="5.4">si</w> <w n="5.5">ce</w> <w n="5.6">n</w>'<w n="5.7">était</w> <w n="5.8">le</w> <w n="5.9">sien</w> !</l>
		<l n="6" num="1.6"><w n="6.1">Chacun</w> <w n="6.2">se</w> <w n="6.3">dit</w> : <w n="6.4">Voilà</w> <w n="6.5">la</w> <w n="6.6">jeune</w> <w n="6.7">épouse</w>,</l>
		<l n="7" num="1.7"><w n="7.1">C</w>'<w n="7.2">est</w> <w n="7.3">la</w> <w n="7.4">Reine</w> <w n="7.5">de</w> <w n="7.6">ce</w> <w n="7.7">beau</w> <w n="7.8">jour</w>…</l>
		<l n="8" num="1.8"><w n="8.1">Ce</w> <w n="8.2">n</w>'<w n="8.3">est</w> <w n="8.4">pas</w> <w n="8.5">que</w> <w n="8.6">je</w> <w n="8.7">sois</w> <w n="8.8">jalouse</w> ;</l>
		<l n="9" num="1.9"><w n="9.1">Mais</w>, <w n="9.2">mon</w> <w n="9.3">Dieu</w>, <w n="9.4">quand</w> <w n="9.5">viendra</w> <w n="9.6">mon</w> <w n="9.7">tour</w> ?</l>
	</lg>
	<lg n="2">
		<l n="10" num="2.1"><w n="10.1">Tout</w> <w n="10.2">le</w> <w n="10.3">village</w> <w n="10.4">vous</w> <w n="10.5">regarde</w> ;</l>
		<l n="11" num="2.2"><w n="11.1">Le</w> <w n="11.2">bailli</w> <w n="11.3">vous</w> <w n="11.4">donne</w> <w n="11.5">la</w> <w n="11.6">main</w> ;</l>
		<l n="12" num="2.3"><w n="12.1">Le</w> <w n="12.2">suisse</w>, <w n="12.3">avec</w> <w n="12.4">sa</w> <w n="12.5">hallebarde</w>,</l>
		<l n="13" num="2.4"><w n="13.1">Devant</w> <w n="13.2">vous</w>, <w n="13.3">ouvre</w> <w n="13.4">le</w> <w n="13.5">chemin</w> ;</l>
		<l n="14" num="2.5"><w n="14.1">Viennent</w> <w n="14.2">après</w> <w n="14.3">bedeaux</w> <w n="14.4">et</w> <w n="14.5">sacristain</w>.</l>
		<l n="15" num="2.6"><w n="15.1">L</w>'<w n="15.2">on</w> <w n="15.3">paraît</w> <w n="15.4">belle</w> <w n="15.5">comme</w> <w n="15.6">un</w> <w n="15.7">ange</w> :</l>
		<l n="16" num="2.7"><w n="16.1">Beau</w> <w n="16.2">voile</w>, <w n="16.3">gants</w> <w n="16.4">blancs</w>, <w n="16.5">robe</w> <w n="16.6">à</w> <w n="16.7">jour</w> ;</l>
		<l n="17" num="2.8"><w n="17.1">Que</w> <w n="17.2">ça</w> <w n="17.3">va</w> <w n="17.4">bien</w>, <w n="17.5">la</w> <w n="17.6">fleur</w> <w n="17.7">d</w>'<w n="17.8">orange</w> ! …</l>
		<l n="18" num="2.9"><w n="18.1">Ah</w> ! <w n="18.2">mon</w> <w n="18.3">Dieu</w>, <w n="18.4">quand</w> <w n="18.5">viendra</w> <w n="18.6">mon</w> <w n="18.7">tour</w> !</l>
	</lg>
</div></body></text></TEI>