<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB7">
	<head type="tune">Air du Premier prix.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">À</w> <w n="1.2">mon</w> <w n="1.3">valet</w>, <w n="1.4">pour</w> <w n="1.5">ma</w> <w n="1.6">voiture</w>,</l>
		<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">vais</w> <w n="2.3">donner</w> <w n="2.4">contr</w>'<w n="2.5">ordre</w> <w n="2.6">ici</w>.</l>
	</lg>
	<lg n="2">
	<head type="speaker">Mme GERVAIS,</head>
		<l n="3" num="2.1"><w n="3.1">Ah</w> ! <w n="3.2">restez</w>, <w n="3.3">je</w> <w n="3.4">vous</w> <w n="3.5">en</w> <w n="3.6">conjure</w> !</l>
	</lg>
	<lg n="3">
	<head type="speaker">DESŒILLETS</head>
		<l part="I" n="4" num="3.1"><w n="4.1">Je</w> <w n="4.2">suis</w> <w n="4.3">à</w> <w n="4.4">vous</w> ! </l>
	</lg>
  <stage>Il sont.</stage>
	<lg n="4">
	<head type="speaker">HENRI, bas, d Mimi.</head>
		<l part="F" n="4"><w n="4.5">Sortez</w> <w n="4.6">aussi</w> !</l>
	</lg>
	<lg n="5">
	<head type="speaker">MIMI.</head>
		<l n="5" num="5.1"><w n="5.1">Je</w> <w n="5.2">vais</w> <w n="5.3">donc</w> <w n="5.4">quitter</w> <w n="5.5">ma</w> <w n="5.6">couronne</w> !</l>
		<l n="6" num="5.2"><w n="6.1">J</w>'<w n="6.2">avais</w> <w n="6.3">cru</w> <w n="6.4">qu</w>'<w n="6.5">elle</w> <w n="6.6">servirait</w>,</l>
		<l n="7" num="5.3"><w n="7.1">Et</w> <w n="7.2">qu</w>'<w n="7.3">un</w> <w n="7.4">autre</w>… <w n="7.5">à</w> <w n="7.6">qui</w> <w n="7.7">je</w> <w n="7.8">pardonne</w>, :</l>
		<l n="8" num="5.4"><w n="8.1">Viendrait</w> <w n="8.2">détacher</w> <w n="8.3">mon</w> <w n="8.4">bouquet</w>.</l>
	</lg>
</div></body></text></TEI>