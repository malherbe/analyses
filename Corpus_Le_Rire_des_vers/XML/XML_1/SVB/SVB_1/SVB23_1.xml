<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB23">
	<head type="form">Chanson du Régent.</head>
	<head type="tune">Air nouveau de M. Paris.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Insensés</w> ! <w n="1.2">nous</w> <w n="1.3">ne</w> <w n="1.4">voyons</w> <w n="1.5">pas</w></l>
		<l n="2" num="1.2"><w n="2.1">Les</w> <w n="2.2">chagrins</w> <w n="2.3">des</w> <w n="2.4">autres</w> <w n="2.5">états</w>,</l>
		<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">nous</w> <w n="3.3">voulons</w> <w n="3.4">changer</w> <w n="3.5">le</w> <w n="3.6">notre</w></l>
		<l n="4" num="1.4"><w n="4.1">Souvent</w> <w n="4.2">contre</w> <w n="4.3">celui</w> <w n="4.4">d</w>'<w n="4.5">un</w> <w n="4.6">autre</w>,</l>
		<l n="5" num="1.5"><w n="5.1">À</w> <w n="5.2">qui</w> <w n="5.3">le</w> <w n="5.4">sien</w> <w n="5.5">déplaît</w> <w n="5.6">autant</w> ;</l>
		<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">voilà</w> <w n="6.3">comme</w></l>
		<l n="7" num="1.7"><w n="7.1">L</w>'<w n="7.2">homme</w></l>
		<l n="8" num="1.8"><w n="8.1">N</w>'<w n="8.2">est</w> <w n="8.3">jamais</w> <w n="8.4">content</w> !</l>
	</lg> 
	<p>
* TOUS. 
Et voilà conime, etc. <lb></lb>
On danse. Pendant le refrain du couplet, Madame Gervais, <lb></lb>
conduite par des dames, entre dans la chambre à gauche. <lb></lb>
PELLEVIN. <lb></lb>
Chut ! silence…, la mariée a disparu. <lb></lb>
MIMI, <lb></lb>
Quand je pense que ce devrait être moi. <lb></lb>
Quel sacrifice ! … elle est si jolie…'<lb></lb>
DESOEILLETS, d part. <lb></lb>
Pellevin, à Desvillets, bas, <lb></lb>
Dites donc, à vous… ( Haut.) Second couplet ! <lb></lb>
	</p>
	<lg n="2">
		<l n="9" num="2.1"><w n="9.1">L</w>'<w n="9.2">enfant</w> <w n="9.3">voudrait</w> <w n="9.4">devenir</w> <w n="9.5">grand</w>,</l>
		<l n="10" num="2.2"><w n="10.1">Le</w> <w n="10.2">vieillard</w> <w n="10.3">être</w> <w n="10.4">adolescent</w>,</l>
		<l n="11" num="2.3"><w n="11.1">La</w> <w n="11.2">fille</w> <w n="11.3">être</w> <w n="11.4">femme</w> <w n="11.5">et</w> <w n="11.6">puis</w> <w n="11.7">veuve</w>,</l>
		<l n="12" num="2.4"><w n="12.1">La</w> <w n="12.2">venve</w> <w n="12.3">se</w> <w n="12.4">donner</w> <w n="12.5">pour</w> <w n="12.6">neuve</w>,</l>
		<l n="13" num="2.5"><w n="13.1">La</w> <w n="13.2">vieille</w> <w n="13.3">fixer</w> <w n="13.4">un</w> <w n="13.5">amant</w> ;</l>
		<l n="14" num="2.6"><w n="14.1">Et</w> <w n="14.2">voilà</w> <w n="14.3">comme</w></l>
		<l n="15" num="2.7"><w n="15.1">L</w>'<w n="15.2">homme</w></l>
		<l n="16" num="2.8"><w n="16.1">N</w>'<w n="16.2">est</w> <w n="16.3">jamais</w> <w n="16.4">content</w>.</l>
	</lg>
</div></body></text></TEI>