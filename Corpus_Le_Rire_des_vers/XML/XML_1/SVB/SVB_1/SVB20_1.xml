<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB20">
	<head type="tune">Air : Est-il supplice égal.</head>
		<div type="section" n="1">
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">Prix</w> <w n="1.2">de</w> <w n="1.3">ton</w> <w n="1.4">dévouement</w>,</l>
				<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">t</w>'<w n="2.3">offre</w> <w n="2.4">un</w> <w n="2.5">régiment</w>.</l>
			</lg>
			<lg n="2">
			<head type="speaker">DESŒILLETS.</head>
				<l n="3" num="2.1"><w n="3.1">Allons</w>, <w n="3.2">enfant</w>, <w n="3.3">courage</w> !</l>
			</lg> 
			<lg n="3">
			<head type="speaker">DUPOIS.</head>
				<l n="4" num="3.1"><w n="4.1">Quoi</w> ! <w n="4.2">le</w> <w n="4.3">don</w> <w n="4.4">est</w> <w n="4.5">formel</w> ?</l>
			</lg>
			<lg n="4">
			<head type="speaker">HENRI.</head>
				<l n="5" num="4.1"><w n="5.1">Je</w> <w n="5.2">te</w> <w n="5.3">fais</w> <w n="5.4">colonel</w>.</l>
			</lg>
			<lg n="5">
			<head type="speaker">DESŒILLETS.</head>
				<l n="6" num="5.1"><w n="6.1">Un</w> <w n="6.2">dernier</w> <w n="6.3">tour</w> <w n="6.4">de</w> <w n="6.5">page</w> !</l>
			</lg>
			<lg n="6">
			<head type="speaker">HENRI.</head>
				<l n="7" num="6.1"><w n="7.1">Tu</w> <w n="7.2">vois</w> <w n="7.3">le</w> <w n="7.4">prix</w>…</l>
			</lg>
			<lg n="7">
			<head type="speaker">DUPUIS.</head>
				<l n="8" num="7.1"><w n="8.1">Volontiers</w>, <w n="8.2">j</w>'<w n="8.3">y</w> <w n="8.4">souscris</w>.</l>
				<l n="9" num="7.2"><w n="9.1">Bientôt</w>, <w n="9.2">gagnant</w> <w n="9.3">la</w> <w n="9.4">plaine</w>,</l>
				<l n="10" num="7.3"><w n="10.1">Nouveau</w> <w n="10.2">Paris</w>,</l>
				<l n="11" num="7.4"><w n="11.1">En</w> <w n="11.2">invoquant</w> <w n="11.3">Cypris</w>,</l>
				<l n="12" num="7.5"><w n="12.1">J</w>'<w n="12.2">enlève</w> <w n="12.3">votre</w> <w n="12.4">Hélène</w> !</l>
			</lg>
		</div>
		<div type="section" n="2">
			<head type="main">ENSEMBLE.</head>
			<lg n="1">
			<head type="speaker">DUPUIS.</head>
				<l n="13" num="1.1"><w n="13.1">Ah</w> ! <w n="13.2">vraiment</w>, <w n="13.3">c</w>'<w n="13.4">est</w> <w n="13.5">charmant</w>,</l>
				<l n="14" num="1.2"><w n="14.1">Avoir</w> <w n="14.2">un</w> <w n="14.3">régiment</w> !</l>
				<l n="15" num="1.3"><w n="15.1">Un</w> <w n="15.2">tel</w> <w n="15.3">prix</w> <w n="15.4">m</w>'<w n="15.5">encourage</w>.</l>
				<l n="16" num="1.4"><w n="16.1">Oui</w>, <w n="16.2">le</w> <w n="16.3">don</w> <w n="16.4">est</w> <w n="16.5">formel</w>,</l>
				<l n="17" num="1.5"><w n="17.1">Me</w> <w n="17.2">voilà</w> <w n="17.3">colonel</w> :</l>
				<l n="18" num="1.6"><w n="18.1">Un</w> <w n="18.2">dernier</w> <w n="18.3">tour</w> <w n="18.4">de</w> <w n="18.5">page</w>.</l>
			</lg>
			<lg n="2">
			<head type="speaker">HENRI et DESŒILLETS.</head>
				<l n="19" num="2.1"><w n="19.1">Prix</w> <w n="19.2">de</w> <w n="19.3">ton</w> <w n="19.4">dévouement</w>,</l>
				<l n="20" num="2.2"><w n="20.1">Accepte</w> <w n="20.2">un</w> <w n="20.3">régiment</w>.</l>
				<l n="21" num="2.3"><w n="21.1">Allons</w>, <w n="21.2">enfant</w>, <w n="21.3">courage</w> !</l>
				<l n="22" num="2.4"><w n="22.1">Oui</w>, <w n="22.2">le</w> <w n="22.3">don</w> <w n="22.4">est</w> <w n="22.5">formel</w>,</l>
				<l n="23" num="2.5"><w n="23.1">On</w> <w n="23.2">te</w> <w n="23.3">fait</w> <w n="23.4">colonel</w> :</l>
				<l n="24" num="2.6"><w n="24.1">Un</w> <w n="24.2">dernier</w> <w n="24.3">tour</w> <w n="24.4">de</w> <w n="24.5">page</w>.</l>
			</lg>
		</div>
</div></body></text></TEI>