<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL15">
	<head type="tune">AIR : L'or est une chimère.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">De</w> <w n="1.2">mes</w> <w n="1.3">grotesques</w> <w n="1.4">statues</w></l>
		<l n="2" num="1.2"><w n="2.1">On</w> <w n="2.2">voit</w> <w n="2.3">le</w> <w n="2.4">type</w> <w n="2.5">en</w> <w n="2.6">tous</w> <w n="2.7">lieux</w> ;</l>
		<l n="3" num="1.3"><w n="3.1">Mes</w> <w n="3.2">modèlʼs</w> <w n="3.3">courent</w> <w n="3.4">les</w> <w n="3.5">rues</w>,</l>
		<l n="4" num="1.4"><w n="4.1">Je</w> <w n="4.2">dois</w> <w n="4.3">courir</w> <w n="4.4">après</w> <w n="4.5">eux</w> !</l>
		<l n="5" num="1.5"><w n="5.1">Si</w> <w n="5.2">devant</w> <w n="5.3">mes</w> <w n="5.4">figures</w></l>
		<l n="6" num="1.6"><w n="6.1">Accourt</w> <w n="6.2">la</w> <w n="6.3">foule</w> <w n="6.4">des</w> <w n="6.5">badauds</w>,</l>
		<l n="7" num="1.7"><w n="7.1">En</w> <w n="7.2">fait</w> <w n="7.3">d</w>'<w n="7.4">caricatures</w></l>
		<l n="8" num="1.8"><w n="8.1">J</w>'<w n="8.2">en</w> <w n="8.3">vois</w> <w n="8.4">d</w>'<w n="8.5">fameusʼs</w> <w n="8.6">qui</w> <w n="8.7">s</w>'<w n="8.8">arrêtʼnt</w> <w n="8.9">aux</w> <w n="8.10">carreaux</w>.</l>
		<l ana="unanalyzable" n="9" num="1.9">De mes grotesques statues, etc.</l> 
	</lg>
</div></body></text></TEI>