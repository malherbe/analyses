<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL17">
	<head type="main">LA MONTAGNE EN TRAVAIL.</head>
		<p>
			LUDOVIC. <lb></lb>
			Quelle montagne ? la butte Montmartre ?<lb></lb>
		</p>
		 <p>
			GUSTAVE. <lb></lb>
			 Non, la montagne de Ménilmontant. ( Il déclame.)<lb></lb>
		 </p>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">O</w> <w n="1.2">fuyez</w> <w n="1.3">les</w> <w n="1.4">cités</w>, <w n="1.5">venez</w> <w n="1.6">à</w> <w n="1.7">la</w> <w n="1.8">campagne</w>,</l>
		<l n="2" num="1.2"><w n="2.1">Venez</w>-<w n="2.2">y</w> <w n="2.3">savourer</w> <w n="2.4">le</w> <w n="2.5">bonheur</w> <w n="2.6">des</w> <w n="2.7">élus</w> ;</l>
		<l n="3" num="1.3"><w n="3.1">Saint</w>-<w n="3.2">Simon</w> <w n="3.3">vous</w> <w n="3.4">appelle</w> <w n="3.5">à</w> <w n="3.6">la</w> <w n="3.7">sainte</w> <w n="3.8">montagne</w>…</l>
		<l n="4" num="1.4"><w n="4.1">On</w> <w n="4.2">y</w> <w n="4.3">va</w> <w n="4.4">par</w> <w n="4.5">les</w> <w n="4.6">Omnibus</w> !</l>
	</lg>
	<lg n="2">
		<l n="5" num="2.1"><w n="5.1">Vous</w> <w n="5.2">y</w> <w n="5.3">verrez</w>, <w n="5.4">vainqueurs</w> <w n="5.5">de</w> <w n="5.6">préjugés</w> <w n="5.7">gothiques</w>,</l>
		<l n="6" num="2.2"><w n="6.1">Vers</w> <w n="6.2">sa</w> <w n="6.3">mission</w> <w n="6.4">noble</w> <w n="6.5">avançant</w> <w n="6.6">d</w>'<w n="6.7">un</w> <w n="6.8">pas</w> <w n="6.9">sûr</w>,</l>
		<l n="7" num="2.3"><w n="7.1">L</w>'<w n="7.2">homme</w> <w n="7.3">libre</w>, <w n="7.4">occupé</w> <w n="7.5">de</w> <w n="7.6">travaux</w> <w n="7.7">domestiques</w>,</l>
		<l n="8" num="2.4"><w n="8.1">Les</w> <w n="8.2">mains</w> <w n="8.3">sales</w> <w n="8.4">et</w> <w n="8.5">le</w> <w n="8.6">cœur</w> <w n="8.7">pur</w> !</l>
	</lg>
	<lg n="3">
		<l n="9" num="3.1"><w n="9.1">Car</w>, <w n="9.2">dans</w> <w n="9.3">notre</w> <w n="9.4">maison</w>, <w n="9.5">chacun</w> <w n="9.6">avec</w> <w n="9.7">courage</w></l>
		<l n="10" num="3.2"><w n="10.1">Se</w> <w n="10.2">livre</w>, <w n="10.3">sans</w> <w n="10.4">orgueil</w>, <w n="10.5">aux</w> <w n="10.6">soins</w> <w n="10.7">les</w> <w n="10.8">plus</w> <w n="10.9">grossiers</w> :</l>
		<l n="11" num="3.3"><w n="11.1">C</w>'<w n="11.2">est</w> <w n="11.3">un</w> <w n="11.4">baron</w> <w n="11.5">qui</w> <w n="11.6">met</w> <w n="11.7">la</w> <w n="11.8">main</w> <w n="11.9">à</w> <w n="11.10">l</w>'<w n="11.11">éclairage</w></l>
		<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">récure</w> <w n="12.3">les</w> <w n="12.4">chandeliers</w> !</l>
	</lg>
	<lg n="4">
		<l n="13" num="4.1"><w n="13.1">Un</w> <w n="13.2">savant</w> <w n="13.3">avocat</w>, <w n="13.4">qui</w> <w n="13.5">d</w>'<w n="13.6">esprit</w> <w n="13.7">étincelle</w>,</l>
		<l n="14" num="4.2"><w n="14.1">Écume</w> <w n="14.2">la</w> <w n="14.3">marmite</w>, <w n="14.4">hache</w> <w n="14.5">les</w> <w n="14.6">épinards</w> ;</l>
		<l n="15" num="4.3"><w n="15.1">Ce</w> <w n="15.2">sont</w> <w n="15.3">deux</w> <w n="15.4">sous</w>-<w n="15.5">préfets</w> <w n="15.6">qui</w> <w n="15.7">lavent</w> <w n="15.8">la</w> <w n="15.9">vaisselle</w> ;</l>
		<l n="16" num="4.4"><w n="16.1">Un</w> <w n="16.2">banquier</w> <w n="16.3">plume</w> <w n="16.4">les</w> <w n="16.5">canards</w> !</l>
	</lg>
	<lg n="5">
		<l n="17" num="5.1"><w n="17.1">Un</w> <w n="17.2">major</w> <w n="17.3">de</w> <w n="17.4">dragons</w> <w n="17.5">brode</w> <w n="17.6">et</w> <w n="17.7">fait</w> <w n="17.8">des</w> <w n="17.9">reprises</w>,</l>
		<l n="18" num="5.2"><w n="18.1">Un</w> <w n="18.2">tendre</w> <w n="18.3">soprano</w> <w n="18.4">scie</w> <w n="18.5">et</w> <w n="18.6">monte</w> <w n="18.7">le</w> <w n="18.8">bois</w>,</l>
		<l n="19" num="5.3"><w n="19.1">Un</w> <w n="19.2">président</w> <w n="19.3">de</w> <w n="19.4">cour</w> <w n="19.5">savonne</w> <w n="19.6">nos</w> <w n="19.7">chemises</w></l>
		<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">met</w> <w n="20.3">nos</w> <w n="20.4">faux</w> <w n="20.5">cols</w> <w n="20.6">à</w> <w n="20.7">l</w>'<w n="20.8">empois</w> !</l>
	</lg> 
	<lg n="6">
		<l n="21" num="6.1"><w n="21.1">Un</w> <w n="21.2">enfant</w> <w n="21.3">d</w>'<w n="21.4">Apollon</w> <w n="21.5">nous</w> <w n="21.6">décrotte</w> <w n="21.7">nos</w> <w n="21.8">bottes</w></l>
		<l n="22" num="6.2"><w n="22.1">Un</w> <w n="22.2">ancien</w> <w n="22.3">auditeur</w> <w n="22.4">a</w> <w n="22.5">soin</w> <w n="22.6">de</w> <w n="22.7">nous</w> <w n="22.8">brosser</w> ;</l>
		<l n="23" num="6.3"><w n="23.1">Un</w> <w n="23.2">duc</w> <w n="23.3">et</w> <w n="23.4">pair</w> <w n="23.5">cultive</w> <w n="23.6">oignons</w>, <w n="23.7">poireaux</w>, <w n="23.8">carottes</w>,</l>
		<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">mène</w> <w n="24.3">les</w> <w n="24.4">poules</w>… <w n="24.5">coucher</w> ! ! !</l>
	</lg>
</div></body></text></TEI>