<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL22">
	<head type="tune">AIR : Eh ! gai, gai, gai, mon officier !</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Eh</w> ! <w n="1.2">pan</w> ! <w n="1.3">pan</w> ! <w n="1.4">pan</w> !</l>
		<l n="2" num="1.2"><w n="2.1">Tapant</w>,</l>
		<l n="3" num="1.3"><w n="3.1">Frappant</w> !</l>
		<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">vacarme</w></l>
		<l n="5" num="1.5"><w n="5.1">À</w> <w n="5.2">son</w> <w n="5.3">charme</w> !</l>
		<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">même</w> <w n="6.3">aux</w> <w n="6.4">sourds</w>, <w n="6.5">que</w> <w n="6.6">nos</w> <w n="6.7">pan</w> <w n="6.8">pan</w></l>
		<l n="7" num="1.7"><w n="7.1">Écorchent</w> <w n="7.2">le</w> <w n="7.3">tympan</w> !</l>
	</lg>
	<lg n="2">
		<head type="main">CHŒUR.</head>
		<l ana="unanalyzable" n="8" num="2.1">Eh ! pan ! pan ! pan ! etc.</l> 
	</lg>
	<lg n="3">
		<head type="speaker">BALTHAZARD.</head>
		<l n="9" num="3.1"><w n="9.1">Aux</w> <w n="9.2">sots</w> <w n="9.3">nos</w> <w n="9.4">farandoles</w></l>
		<l n="10" num="3.2"><w n="10.1">En</w> <w n="10.2">vain</w> <w n="10.3">font</w> <w n="10.4">fair</w>' <w n="10.5">des</w> <w n="10.6">bonds</w> ;</l>
		<l n="11" num="3.3"><w n="11.1">C</w>'<w n="11.2">est</w> <w n="11.3">l</w>' <w n="11.4">métier</w> <w n="11.5">des</w> <w n="11.6">cassʼroles</w></l>
		<l n="12" num="3.4"><w n="12.1">D</w>'<w n="12.2">fair</w>' <w n="12.3">sauter</w> <w n="12.4">les</w> <w n="12.5">dindons</w> ! …</l>
		<l ana="unanalyzable" n="13" num="3.5">Eh ! pan pan ! pan ! etc.</l>
	</lg> 
</div></body></text></TEI>