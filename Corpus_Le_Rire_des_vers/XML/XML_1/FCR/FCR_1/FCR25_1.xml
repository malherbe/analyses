<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA MÉTEMPSYCOSE</title>
				<title type="sub">BÊTISE EN UN ACTE, MÊLÉE DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="COU" sort="1">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<author key="JAI" sort="2">
					<name>
						<forename>Ernest</forename>
						<surname>JAIME</surname>
					</name>
					<date from="1804" to="1884">1804-1884</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>282 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FCR_1</idno>	
				<availability status="free">
					<licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA MÉTEMPSYCOSE</title>
						<author>FRÉDÉRIC DE COURCY ET JAIME</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=c1RoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032819917.0x000001# ?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">7 FÉVRIER 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">VAUDEVILLE.CHOEUR.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FCR25">
			<head type="main">CHRISTOPHE, au public.</head>
			<head type="tune">AIR de Garriok.</head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">Avec</w> <w n="1.2">ses</w> <w n="1.3">terribles</w> <w n="1.4">acteurs</w>,</l>
				<l n="2" num="1.2"><w n="2.1">Dont</w> <w n="2.2">nous</w> <w n="2.3">montrons</w> <w n="2.4">les</w> <w n="2.5">bien</w> <w n="2.6">faibles</w> <w n="2.7">copistes</w>,</l>
				<l n="3" num="1.3"><w n="3.1">Monsieur</w> <w n="3.2">Martin</w> <w n="3.3">voyait</w> <w n="3.4">les</w> <w n="3.5">spectateurs</w></l>
				<l n="4" num="1.4"><w n="4.1">Trembler</w> <w n="4.2">chez</w> <w n="4.3">lui</w> <w n="4.4">devant</w> <w n="4.5">tous</w> <w n="4.6">les</w> <w n="4.7">artistes</w>.</l>
				<l n="5" num="1.5"><w n="5.1">Au</w> <w n="5.2">tigre</w>, <w n="5.3">au</w> <w n="5.4">lion</w>, <w n="5.5">à</w> <w n="5.6">travers</w> <w n="5.7">leurs</w> <w n="5.8">barreaux</w>,</l>
				<l n="6" num="1.6"><w n="6.1">En</w> <w n="6.2">frisonnant</w> <w n="6.3">on</w> <w n="6.4">donnait</w> <w n="6.5">des</w> <w n="6.6">éloges</w>…</l>
				<l n="7" num="1.7"><w n="7.1">Mais</w>, <w n="7.2">sans</w> <w n="7.3">espoir</w> <w n="7.4">d</w>'<w n="7.5">obtenir</w> <w n="7.6">des</w> <w n="7.7">bravos</w>,</l>
				<l n="8" num="1.8"><w n="8.1">Chez</w> <w n="8.2">nous</w>, <w n="8.3">messieurs</w>, <w n="8.4">ce</w> <w n="8.5">sont</w> <w n="8.6">les</w> <w n="8.7">animaux</w></l>
				<l n="9" num="1.9"><w n="9.1">Qui</w> <w n="9.2">tremblent</w> <w n="9.3">tous</w> <w n="9.4">devant</w> <w n="9.5">les</w> <w n="9.6">loges</w>.</l>
			</lg>
		</div></body></text></TEI>