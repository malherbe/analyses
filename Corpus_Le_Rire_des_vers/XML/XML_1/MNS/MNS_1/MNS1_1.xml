<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS1">
	<head type="tune">Air des Scythes et des Amazones.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Daubez</w> <w n="1.2">sur</w> <w n="1.3">moi</w>, <w n="1.4">pisque</w> <w n="1.5">ça</w> <w n="1.6">peut</w> <w n="1.7">vous</w> <w n="1.8">plaire</w>,</l>
		<l n="2" num="1.2"><w n="2.1">Quoiqu</w>' <w n="2.2">vous</w> <w n="2.3">n</w>' <w n="2.4">soyez</w> <w n="2.5">pas</w> <w n="2.6">très</w> <w n="2.7">malign</w>', <w n="2.8">je</w> <w n="2.9">croi</w> ;</l>
		<l n="3" num="1.3"><w n="3.1">Par</w> <w n="3.2">modestie</w> <w n="3.3">ici</w>, <w n="3.4">j</w>' <w n="3.5">vous</w> <w n="3.6">laissʼrons</w> <w n="3.7">faire</w>,</l>
		<l n="4" num="1.4"><w n="4.1">J</w>'<w n="4.2">suis</w> <w n="4.3">bon</w> <w n="4.4">enfant</w> <w n="4.5">quand</w> <w n="4.6">on</w> <w n="4.7">n</w>'<w n="4.8">s</w>'<w n="4.9">amus</w>' <w n="4.10">qu</w>'<w n="4.11">de</w> <w n="4.12">moi</w>. <del reason="analysis" type="repetition" hand="KL">( bis.)</del></l>
		<l n="5" num="1.5"><w n="5.1">Mais</w> <w n="5.2">du</w> <w n="5.3">pays</w> <w n="5.4">ousqu</w>' <w n="5.5">André</w> <w n="5.6">s</w>'<w n="5.7">est</w> <w n="5.8">vu</w> <w n="5.9">naître</w></l>
		<l n="6" num="1.6"><w n="6.1">N</w>' <w n="6.2">dites</w> <w n="6.3">pas</w> <w n="6.4">d</w>' <w n="6.5">mal</w>… <w n="6.6">Si</w> <w n="6.7">vous</w> <w n="6.8">m</w>' <w n="6.9">poussiez</w> <w n="6.10">à</w> <w n="6.11">bout</w>,</l>
		<l n="7" num="1.7"><w n="7.1">De</w> <w n="7.2">mes</w> <w n="7.3">deux</w> <w n="7.4">poingts</w> <w n="7.5">j</w>' <w n="7.6">pourrions</w> <w n="7.7">ben</w> <w n="7.8">n</w>'<w n="7.9">êt</w>' <w n="7.10">pas</w> <w n="7.11">l</w>' <w n="7.12">maître</w> ;</l>
		<l n="8" num="1.8"><w n="8.1">J</w>'<w n="8.2">suis</w> <w n="8.3">morvandiay</w>, <w n="8.4">mon</w> <w n="8.5">pays</w> <w n="8.6">avant</w> <w n="8.7">tout</w>. <del reason="analysis" type="repetition" hand="KL">( bis.)</del></l>
	</lg>
</div></body></text></TEI>