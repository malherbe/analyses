<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE GENTILHOMME</title>
				<title type="sub">VAUDEVILLE ANECDOTIQUE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DPY" sort="1">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="COU" sort="2">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>135 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DDC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE GENTILHOMME</title>
						<author>DUPEUTY ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=blZoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032862369.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">21 MARS 1833</date>
				<placeName>
					<settlement>THÉÂTRE NATIONAL DU VAUDEVILLE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DDC11">
	<head type="main">( Ils reprennent tous le refrain en chœur. ‒ Au public.)</head> 
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Vous</w> <w n="1.2">tous</w>, <w n="1.3">dans</w> <w n="1.4">les</w> <w n="1.5">loges</w>,</l>
		<l n="2" num="1.2"><w n="2.1">Comblez</w>-<w n="2.2">moi</w> <w n="2.3">d</w>'<w n="2.4">éloges</w></l>
		<l n="3" num="1.3"><w n="3.1">Délicats</w> <w n="3.2">et</w> <w n="3.3">fins</w> ;</l>
		<l n="4" num="1.4"><w n="4.1">Toi</w>, <w n="4.2">digne</w> <w n="4.3">parterre</w>,</l>
		<l n="5" num="1.5"><w n="5.1">Mon</w> <w n="5.2">dieu</w> <w n="5.3">tutélaire</w>,</l>
		<l n="6" num="1.6"><w n="6.1">Ouvre</w> <w n="6.2">tes</w> <w n="6.3">mains</w>…</l>
		<p>
		( parlant.) Surtout, messieurs, que personne n'ait la malheu<lb></lb>
		reuse idée de faire entendre le signe de ralliement des voleurs :<lb></lb> 
		vous avez vu comme la police se fait à Paris, ct monsieur de<lb></lb>
		Sardine, que voici, ne manquerait pas de faire courir sus au dé<lb></lb>
		Jinquant… Néanmoins, néanmoinsse, vous avez une manière<lb></lb>
		toute simple de manifester votre opinion, manière dont mon<lb></lb>
		sieun de sardine, que voilà, serait lui-même fort satisfait…<lb></lb> 
		Voici tà manière : 
		</p>
		<l n="7" num="1.7"><w n="7.1">Des</w> <w n="7.2">bravos</w>, <w n="7.3">des</w> <w n="7.4">bravos</w>,</l>
		<l n="8" num="1.8"><w n="8.1">Mais</w> <w n="8.2">pas</w> <w n="8.3">d</w>'<w n="8.4">attaques</w> ;</l>
		<l n="9" num="1.9"><w n="9.1">Des</w> <w n="9.2">bravos</w>, <w n="9.3">des</w> <w n="9.4">bravos</w>,</l>
		<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">des</w> <w n="10.3">bravissimos</w> !</l>
		<l n="11" num="1.11"><w n="11.1">J</w>'<w n="11.2">aime</w> <w n="11.3">les</w> <w n="11.4">bravos</w>, <w n="11.5">les</w> <w n="11.6">bravos</w>.</l>
		<l n="12" num="1.12"><w n="12.1">Et</w> <w n="12.2">même</w> <w n="12.3">les</w> <w n="12.4">claques</w> ;</l>
		<l n="13" num="1.13"><w n="13.1">J</w>'<w n="13.2">aime</w> <w n="13.3">les</w> <w n="13.4">bravos</w>, <w n="13.5">les</w> <w n="13.6">bravos</w></l>
		<l n="14" num="1.14"><w n="14.1">Et</w> <w n="14.2">les</w> <w n="14.3">bravissimos</w>.</l>
		<l n="15" num="1.15"><w n="15.1">Bravo</w> ! <w n="15.2">bravo</w> !</l>
		<l n="16" num="1.16"><w n="16.1">Allons</w>, <w n="16.2">des</w> <w n="16.3">claques</w> !</l>
		<l n="17" num="1.17"><w n="17.1">Bravo</w> ! <w n="17.2">bravo</w> !</l>
		<l n="18" num="1.18"><w n="18.1">Bravissimo</w> !</l>
	</lg>
</div></body></text></TEI>