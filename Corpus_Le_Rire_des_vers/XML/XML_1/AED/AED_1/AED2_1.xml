<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">27, 28 ET 29 JUILLET, TABLEAU ÉPISODIQUE DES TROIS JOURNÉES.</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ARA" sort="1">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<author key="DUV" sort="2">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>495 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
						<author>ARAGO ET DUVERT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=xjVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
								<author>ARAGO ET DUVERT</author>
								<repository>Österreichische Nationalbibliothek</repository>
								<idno type="URI">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886206</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : U+02BC MODIFIER LETTER APOSTROPHE) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<change when="2021-06-14" who="RR">Quelques corrections concernant la distribution du signe signe ʼ (UNICODE : ʼ).</change>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE JOURNÉE.</head><head type="main_subpart">SCÈNE PREMIÈRE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="AED2">
			<head type="tune">AIR : J'avais mis mon petit chapeau.</head>
			<lg n="1">
				<head type="main">COLOMBON.</head>
				<l n="1" num="1.1"><space unit="char" quantity="4"></space><w n="1.1">Vengeons</w> <w n="1.2">nos</w> <w n="1.3">papas</w>, <w n="1.4">nos</w> <w n="1.5">mamans</w> !</l>
				<l n="2" num="1.2"><w n="2.1">Pour</w> <w n="2.2">les</w> <w n="2.3">combats</w> <w n="2.4">je</w> <w n="2.5">lâchʼrais</w> <w n="2.6">la</w> <w n="2.7">futaille</w> !</l>
				<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1">Ma</w> <w n="3.2">vieillʼ</w> <w n="3.3">mèrʼ</w>, <w n="3.4">depuis</w> <w n="3.5">vingt</w>-<w n="3.6">cinq</w> <w n="3.7">ans</w>,</l>
				<l n="4" num="1.4"><w n="4.1">Vend</w> <w n="4.2">des</w> <w n="4.3">saucissʼ</w> <w n="4.4">sur</w> <w n="4.5">le</w> <w n="4.6">quai</w> <w n="4.7">de</w> <w n="4.8">lʼ</w> <w n="4.9">Ferraille</w> ;</l>
				<l n="5" num="1.5"><w n="5.1">Mais</w> <w n="5.2">vʼlà</w> <w n="5.3">Mangin</w> <w n="5.4">qui</w> <w n="5.5">nʼ</w> <w n="5.6">veut</w> <w n="5.7">plus</w> <w n="5.8">qu</w>'<w n="5.9">ellʼ</w> <w n="5.10">travaille</w>.</l>
				<l n="6" num="1.6"><space unit="char" quantity="4"></space><w n="6.1">Si</w> <w n="6.2">nous</w> <w n="6.3">triomphions</w>, <w n="6.4">dès</w> <w n="6.5">demain</w></l>
				<l n="7" num="1.7"><w n="7.1">Ellʼ</w> <w n="7.2">reprendrait</w> <w n="7.3">sa</w> <w n="7.4">poêle</w> <w n="7.5">et</w> <w n="7.6">son</w> <w n="7.7">service</w> ;</l>
				<l n="8" num="1.8"><space unit="char" quantity="4"></space><w n="8.1">Ellʼ</w> <w n="8.2">ferait</w> <w n="8.3">refrirʼ</w> <w n="8.4">la</w> <w n="8.5">saucisse</w>,</l>
				<l n="9" num="1.9"><space unit="char" quantity="4"></space><w n="9.1">Sans</w> <w n="9.2">qu</w>'<w n="9.3">un</w> <w n="9.4">damné</w> <w n="9.5">préfet</w> <w n="9.6">dʼ</w> <w n="9.7">police</w></l>
				<l n="10" num="1.10"><space unit="char" quantity="4"></space><w n="10.1">Viennʼ</w> <w n="10.2">lui</w> <w n="10.3">rʼtirer</w> <w n="10.4">le</w> <w n="10.5">pain</w> <w n="10.6">dʼ</w> <w n="10.7">la</w> <w n="10.8">main</w>.</l>
			</lg>
		</div></body></text></TEI>