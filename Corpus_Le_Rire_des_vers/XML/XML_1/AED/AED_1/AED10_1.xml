<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">27, 28 ET 29 JUILLET, TABLEAU ÉPISODIQUE DES TROIS JOURNÉES.</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ARA" sort="1">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<author key="DUV" sort="2">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>495 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
						<author>ARAGO ET DUVERT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=xjVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
								<author>ARAGO ET DUVERT</author>
								<repository>Österreichische Nationalbibliothek</repository>
								<idno type="URI">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886206</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : U+02BC MODIFIER LETTER APOSTROPHE) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<change when="2021-06-14" who="RR">Quelques corrections concernant la distribution du signe signe ʼ (UNICODE : ʼ).</change>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME JOURNÉE</head><head type="main_subpart">SCÈNE PREMIÈRE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="AED10">
			<head type="tune">Air De l'Enfant du régiment.</head>
				<lg n="1">
					<head type="main">CHŒUR.</head>
					<l n="1" num="1.1"><w n="1.1">Troupʼ</w> <w n="1.2">mercenairʼ</w>, <w n="1.3">qu</w>' <w n="1.4">nous</w> <w n="1.5">payons</w> <w n="1.6">à</w> <w n="1.7">grands</w> <w n="1.8">frais</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Gare</w> <w n="2.2">à</w> <w n="2.3">toi</w> <w n="2.4">si</w> <w n="2.5">tu</w> <w n="2.6">bouges</w> ;</l>
					<l n="3" num="1.3"><space unit="char" quantity="6"></space><w n="3.1">Tirons</w> <w n="3.2">sur</w> <w n="3.3">les</w> <w n="3.4">habits</w> <w n="3.5">rouges</w> :</l>
					<l n="4" num="1.4"><space unit="char" quantity="2"></space><w n="4.1">N</w>'<w n="4.2">y</w> <w n="4.3">en</w> <w n="4.4">a</w> <w n="4.5">pas</w> <w n="4.6">un</w> <w n="4.7">qui</w> <w n="4.8">soit</w> <w n="4.9">Français</w>.</l>
					<l n="5" num="1.5"><space unit="char" quantity="10"></space><w n="5.1">En</w> <w n="5.2">avant</w> ! <w n="5.3">marchons</w> !</l>
					<l n="6" num="1.6"><space unit="char" quantity="16"></space><w n="6.1">Prenons</w></l>
					<l n="7" num="1.7"><space unit="char" quantity="14"></space><w n="7.1">Leurs</w> <w n="7.2">canons</w>,</l>
					<l n="8" num="1.8"><space unit="char" quantity="4"></space><w n="8.1">Malgré</w> <w n="8.2">lʼ</w> <w n="8.3">feu</w> <w n="8.4">de</w> <w n="8.5">leurs</w> <w n="8.6">bataillons</w> !</l>
				</lg>
				<lg n="2">
					<head type="main">PRUNEAU.</head>
					<l n="9" num="2.1"><space unit="char" quantity="4"></space><w n="9.1">Nos</w> <w n="9.2">balles</w> <w n="9.3">leur</w> <w n="9.4">sont</w> <w n="9.5">destinées</w>,</l>
					<l n="10" num="2.2"><space unit="char" quantity="6"></space><w n="10.1">Puisqu</w>' <w n="10.2">ces</w> <w n="10.3">Suissʼ</w> <w n="10.4">s</w> <w n="10.5">nous</w> <w n="10.6">poussʼnt</w> <w n="10.7">à</w> <w n="10.8">bout</w> ;</l>
					<l n="11" num="2.3"><space unit="char" quantity="4"></space><w n="11.1">Avançons</w> <w n="11.2">de</w> <w n="11.3">douze</w> <w n="11.4">journées</w></l>
					<l n="12" num="2.4"><space unit="char" quantity="6"></space><w n="12.1">L</w>'<w n="12.2">anniversairʼ</w> <w n="12.3">du</w> <w n="12.4">dix</w> <w n="12.5">août</w>.</l>
				</lg>
				<lg n="3">
				<head type="main">CHŒUR.</head>
					<l n="13" num="3.1"><w n="13.1">Troupʼ</w> <w n="13.2">mercenairʼ</w>, <subst type="repetition" hand="LG" reason="analysis"><del>etc.</del><add rend="hidden"><w n="13.3">qu</w>' <w n="13.4">nous</w> <w n="13.5">payons</w> <w n="13.6">à</w> <w n="13.7">grands</w> <w n="13.8">frais</w>,</add></subst>.</l>
					<l n="14" num="3.2"><space unit="char" quantity="8"></space><subst type="repetition" hand="LG" reason="analysis"><del> </del><add rend="hidden"><w n="14.1">Gare</w> <w n="14.2">à</w> <w n="14.3">toi</w> <w n="14.4">si</w> <w n="14.5">tu</w> <w n="14.6">bouges</w> ; </add></subst></l>
					<l n="15" num="3.3"><space unit="char" quantity="6"></space><subst type="repetition" hand="LG" reason="analysis"><del> </del><add rend="hidden"><w n="15.1">Tirons</w> <w n="15.2">sur</w> <w n="15.3">les</w> <w n="15.4">habits</w> <w n="15.5">rouges</w> : </add></subst></l>
					<l n="16" num="3.4"><space unit="char" quantity="2"></space><subst type="repetition" hand="LG" reason="analysis"><del> </del><add rend="hidden"><w n="16.1">N</w>'<w n="16.2">y</w> <w n="16.3">en</w> <w n="16.4">a</w> <w n="16.5">pas</w> <w n="16.6">un</w> <w n="16.7">qui</w> <w n="16.8">soit</w> <w n="16.9">Français</w>. </add></subst></l>
					<l n="17" num="3.5"><space unit="char" quantity="10"></space><subst type="repetition" hand="LG" reason="analysis"><del> </del><add rend="hidden"><w n="17.1">En</w> <w n="17.2">avant</w> ! <w n="17.3">marchons</w> ! </add></subst></l>
					<l n="18" num="3.6"><space unit="char" quantity="16"></space><subst type="repetition" hand="LG" reason="analysis"><del> </del><add rend="hidden"><w n="18.1">Prenons</w></add></subst></l>
					<l n="19" num="3.7"><space unit="char" quantity="14"></space><subst type="repetition" hand="LG" reason="analysis"><del> </del><add rend="hidden"><w n="19.1">Leurs</w> <w n="19.2">canons</w>, </add></subst></l>
					<l n="20" num="3.8"><space unit="char" quantity="4"></space><subst type="repetition" hand="LG" reason="analysis"><del> </del><add rend="hidden"><w n="20.1">Malgré</w> <w n="20.2">lʼ</w> <w n="20.3">feu</w> <w n="20.4">de</w> <w n="20.5">leurs</w> <w n="20.6">bataillons</w> ! </add></subst></l>
				</lg>
		</div></body></text></TEI>