<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">27, 28 ET 29 JUILLET, TABLEAU ÉPISODIQUE DES TROIS JOURNÉES.</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ARA" sort="1">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<author key="DUV" sort="2">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>495 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
						<author>ARAGO ET DUVERT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=xjVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
								<author>ARAGO ET DUVERT</author>
								<repository>Österreichische Nationalbibliothek</repository>
								<idno type="URI">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886206</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : U+02BC MODIFIER LETTER APOSTROPHE) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<change when="2021-06-14" who="RR">Quelques corrections concernant la distribution du signe signe ʼ (UNICODE : ʼ).</change>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TROISIÈME JOURNÉE</head><head type="main_subpart">SCÈNE VI.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="AED26">
			<head type="tune">AIR : Gai, gai, mariez-vous.</head>
				<lg n="1">
					<head type="main">COLOMBON.</head>
					<l n="1" num="1.1"><space unit="char" quantity="4"></space><w n="1.1">Non</w>, <w n="1.2">non</w>, <w n="1.3">donnons</w>-<w n="1.4">nous</w> <w n="1.5">lʼ</w> <w n="1.6">bras</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Qu</w>' <w n="2.2">notrʼ</w> <w n="2.3">alliance</w></l>
					<l n="3" num="1.3"><space unit="char" quantity="10"></space><w n="3.1">Sauvʼ</w> <w n="3.2">la</w> <w n="3.3">France</w> ;</l>
					<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">Non</w>, <w n="4.2">non</w>, <w n="4.3">donnons</w>-<w n="4.4">nous</w> <w n="4.5">lʼ</w> <w n="4.6">bras</w></l>
					<l n="5" num="1.5"><space unit="char" quantity="2"></space><w n="5.1">Les</w> <w n="5.2">Jésuitʼs</w> <w n="5.3">ne</w> <w n="5.4">rʼviendront</w> <w n="5.5">pas</w>.</l>
				</lg>
				
				<lg n="2">
					<head type="main">EN CHŒUR.</head>
					<l n="6" num="2.1"><space unit="char" quantity="4"></space><w n="6.1">Non</w>, <w n="6.2">non</w>, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden"><w n="6.3">donnons</w>-<w n="6.4">nous</w> <w n="6.5">lʼ</w> <w n="6.6">bras</w>,</add></subst></l>
					<l n="7" num="2.2"><space unit="char" quantity="8"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="7.1">Qu</w>' <w n="7.2">notrʼ</w> <w n="7.3">alliance</w></add></subst></l>
					<l n="8" num="2.3"><space unit="char" quantity="10"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="8.1">Sauvʼ</w> <w n="8.2">la</w> <w n="8.3">France</w> ; </add></subst></l>
					<l n="9" num="2.4"><space unit="char" quantity="4"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="9.1">Non</w>, <w n="9.2">non</w>, <w n="9.3">donnons</w>-<w n="9.4">nous</w> <w n="9.5">lʼ</w> <w n="9.6">bras</w></add></subst></l>
					<l n="10" num="2.5"><space unit="char" quantity="2"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="10.1">Les</w> <w n="10.2">Jésuitʼs</w> <w n="10.3">ne</w> <w n="10.4">rʼviendront</w> <w n="10.5">pas</w>. </add></subst></l>
				</lg>
				
				<lg n="3">
					<head type="main">COLOMBON.</head>
					<l n="11" num="3.1"><space unit="char" quantity="2"></space><w n="11.1">Après</w> <w n="11.2">nos</w> <w n="11.3">glorieux</w> <w n="11.4">travaux</w>,</l>
					<l n="12" num="3.2"><space unit="char" quantity="2"></space><w n="12.1">S</w>'<w n="12.2">ils</w> <w n="12.3">revenaient</w>, <w n="12.4">ces</w> <w n="12.5">transfuges</w>,</l>
					<l n="13" num="3.3"><space unit="char" quantity="2"></space><w n="13.1">Nous</w> <w n="13.2">aurions</w> <w n="13.3">au</w> <w n="13.4">lieu</w> <w n="13.5">de</w> <w n="13.6">juges</w></l>
					<l n="14" num="3.4"><space unit="char" quantity="2"></space><w n="14.1">Des</w> <w n="14.2">prévôts</w> <w n="14.3">et</w> <w n="14.4">des</w> <w n="14.5">bourreaux</w> ?</l>
					<l n="15" num="3.5"><space unit="char" quantity="4"></space><w n="15.1">Non</w>, <w n="15.2">non</w>, <w n="15.3">donnons</w>-<w n="15.4">nous</w> <w n="15.5">lʼ</w> <w n="15.6">bras</w> !</l>
					<l n="16" num="3.6"><space unit="char" quantity="10"></space><w n="16.1">Qu</w>'<w n="16.2">on</w> <w n="16.3">bénisse</w></l>
					<l n="17" num="3.7"><space unit="char" quantity="10"></space><w n="17.1">La</w> <w n="17.2">justice</w> ;</l>
					<l n="18" num="3.8"><space unit="char" quantity="4"></space><w n="18.1">Non</w>, <w n="18.2">non</w>, <w n="18.3">donnons</w>-<w n="18.4">nous</w> <w n="18.5">lʼ</w> <w n="18.6">bras</w> !</l>
					<l n="19" num="3.9"><space unit="char" quantity="2"></space><w n="19.1">Les</w> <w n="19.2">prévôts</w> <w n="19.3">ne</w> <w n="19.4">rʼviendront</w> <w n="19.5">pas</w>.</l>
				</lg>
				
				<lg n="4">
					<head type="main">EN CHŒUR.</head>
					<l n="20" num="4.1"><space unit="char" quantity="4"></space><w n="20.1">Non</w>, <w n="20.2">non</w>, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden"><w n="20.3">donnons</w>-<w n="20.4">nous</w> <w n="20.5">lʼ</w> <w n="20.6">bras</w>,</add></subst></l>
					<l n="21" num="4.2"><space unit="char" quantity="8"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="21.1">Qu</w>' <w n="21.2">notrʼ</w> <w n="21.3">alliance</w></add></subst></l>
					<l n="22" num="4.3"><space unit="char" quantity="10"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="22.1">Sauvʼ</w> <w n="22.2">la</w> <w n="22.3">France</w> ; </add></subst></l>
					<l n="23" num="4.4"><space unit="char" quantity="4"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="23.1">Non</w>, <w n="23.2">non</w>, <w n="23.3">donnons</w>-<w n="23.4">nous</w> <w n="23.5">lʼ</w> <w n="23.6">bras</w></add></subst></l>
					<l n="24" num="4.5"><space unit="char" quantity="2"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="24.1">Les</w> <w n="24.2">Jésuitʼs</w> <w n="24.3">ne</w> <w n="24.4">rʼviendront</w> <w n="24.5">pas</w>. </add></subst></l>
					</lg>
				
				<lg n="5">
					<head type="main">RAIMOND.</head>
					<l n="25" num="5.1"><space unit="char" quantity="2"></space><w n="25.1">Nous</w> <w n="25.2">rʼverrions</w> <w n="25.3">ces</w> <w n="25.4">noirs</w> <w n="25.5">Judas</w></l>
					<l n="26" num="5.2"><space unit="char" quantity="2"></space><w n="26.1">Envahir</w> <w n="26.2">les</w> <w n="26.3">ministères</w></l>
					<l n="27" num="5.3"><space unit="char" quantity="2"></space><w n="27.1">Et</w> <w n="27.2">doter</w> <w n="27.3">les</w> <w n="27.4">séminaires</w></l>
					<l n="28" num="5.4"><space unit="char" quantity="2"></space><w n="28.1">Aux</w> <w n="28.2">dépens</w> <w n="28.3">dʼ</w> <w n="28.4">nos</w> <w n="28.5">vieux</w> <w n="28.6">soldats</w> ?</l>
					<l n="29" num="5.5"><space unit="char" quantity="4"></space><w n="29.1">Non</w>, <w n="29.2">non</w>, <w n="29.3">donnons</w>-<w n="29.4">nous</w> <w n="29.5">lʼ</w> <w n="29.6">bras</w> !</l>
					<l n="30" num="5.6"><space unit="char" quantity="8"></space><w n="30.1">Plus</w> <w n="30.2">dʼ</w> <w n="30.3">ces</w> <w n="30.4">ministres</w></l>
					<l n="31" num="5.7"><space unit="char" quantity="12"></space><w n="31.1">Sinistres</w> !</l>
					<l n="32" num="5.8"><space unit="char" quantity="4"></space><w n="32.1">Non</w>, <w n="32.2">non</w>, <w n="32.3">donnons</w>-<w n="32.4">nous</w> <w n="32.5">lʼ</w> <w n="32.6">bras</w> !</l>
					<l n="33" num="5.9"><space unit="char" quantity="2"></space><w n="33.1">Les</w> <w n="33.2">traîtres</w> <w n="33.3">ne</w> <w n="33.4">rʼviendront</w> <w n="33.5">pas</w>.</l>
				</lg>
				
				<lg n="6">
					<head type="main">EN CHŒUR.</head>
					<l n="34" num="6.1"><space unit="char" quantity="4"></space><w n="34.1">Non</w>, <w n="34.2">non</w>, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden"><w n="34.3">donnons</w>-<w n="34.4">nous</w> <w n="34.5">lʼ</w> <w n="34.6">bras</w>,</add></subst></l>
					<l n="35" num="6.2"><space unit="char" quantity="8"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="35.1">Qu</w>' <w n="35.2">notrʼ</w> <w n="35.3">alliance</w></add></subst></l>
					<l n="36" num="6.3"><space unit="char" quantity="10"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="36.1">Sauvʼ</w> <w n="36.2">la</w> <w n="36.3">France</w> ; </add></subst></l>
					<l n="37" num="6.4"><space unit="char" quantity="4"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="37.1">Non</w>, <w n="37.2">non</w>, <w n="37.3">donnons</w>-<w n="37.4">nous</w> <w n="37.5">lʼ</w> <w n="37.6">bras</w></add></subst></l>
					<l n="38" num="6.5"><space unit="char" quantity="2"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="38.1">Les</w> <w n="38.2">Jésuitʼs</w> <w n="38.3">ne</w> <w n="38.4">rʼviendront</w> <w n="38.5">pas</w>. </add></subst></l>
					</lg>
				
				<lg n="7">
					<head type="main">LE GARDE NATIONAL.</head>
					<l n="39" num="7.1"><space unit="char" quantity="2"></space><w n="39.1">Quoi</w> ! <w n="39.2">nous</w> <w n="39.3">rʼverrions</w> <w n="39.4">désormais</w></l>
					<l n="40" num="7.2"><space unit="char" quantity="2"></space><w n="40.1">Ces</w> <w n="40.2">gendarmʼs</w> <w n="40.3">patibulaires</w></l>
					<l n="41" num="7.3"><space unit="char" quantity="2"></space><w n="41.1">Dont</w> <w n="41.2">les</w> <w n="41.3">sabres</w> <w n="41.4">mercenaires</w></l>
					<l n="42" num="7.4"><space unit="char" quantity="2"></space><w n="42.1">N</w>'<w n="42.2">ont</w> <w n="42.3">versé</w> <w n="42.4">qu</w>' <w n="42.5">du</w> <w n="42.6">sang</w> <w n="42.7">français</w> ?</l>
					<l n="43" num="7.5"><space unit="char" quantity="4"></space><w n="43.1">Non</w>, <w n="43.2">non</w>, <w n="43.3">donnons</w> <w n="43.4">nous</w> <w n="43.5">lʼ</w> <w n="43.6">bras</w> !</l>
					<l n="44" num="7.6"><space unit="char" quantity="10"></space><w n="44.1">Vils</w> <w n="44.2">gendarmes</w> !</l>
					<l n="45" num="7.7"><space unit="char" quantity="10"></space><w n="45.1">Bas</w> <w n="45.2">les</w> <w n="45.3">armes</w> !</l>
					<l n="46" num="7.8"><space unit="char" quantity="4"></space><w n="46.1">Non</w>, <w n="46.2">non</w>, <w n="46.3">donnons</w>-<w n="46.4">nous</w> <w n="46.5">lʼ</w> <w n="46.6">bras</w> !</l>
					<l n="47" num="7.9"><space unit="char" quantity="2"></space><w n="47.1">Les</w> <w n="47.2">gendarmʼs</w> <w n="47.3">ne</w> <w n="47.4">rʼviendront</w> <w n="47.5">pas</w>.</l>
				</lg>
				
				<lg n="8">
					<head type="main">EN CHŒUR.</head>
					<l n="48" num="8.1"><space unit="char" quantity="4"></space><w n="48.1">Non</w>, <w n="48.2">non</w>, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden"><w n="48.3">donnons</w>-<w n="48.4">nous</w> <w n="48.5">lʼ</w> <w n="48.6">bras</w>,</add></subst></l>
					<l n="49" num="8.2"><space unit="char" quantity="8"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="49.1">Qu</w>' <w n="49.2">notrʼ</w> <w n="49.3">alliance</w></add></subst></l>
					<l n="50" num="8.3"><space unit="char" quantity="10"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="50.1">Sauvʼ</w> <w n="50.2">la</w> <w n="50.3">France</w> ; </add></subst></l>
					<l n="51" num="8.4"><space unit="char" quantity="4"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="51.1">Non</w>, <w n="51.2">non</w>, <w n="51.3">donnons</w>-<w n="51.4">nous</w> <w n="51.5">lʼ</w> <w n="51.6">bras</w></add></subst></l>
					<l n="52" num="8.5"><space unit="char" quantity="2"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="52.1">Les</w> <w n="52.2">Jésuitʼs</w> <w n="52.3">ne</w> <w n="52.4">rʼviendront</w> <w n="52.5">pas</w>. </add></subst></l>
					</lg>
				
				<lg n="9">
					<head type="main">ATKINSON.</head>
					<l n="53" num="9.1"><space unit="char" quantity="2"></space><w n="53.1">Pour</w> <w n="53.2">chaqu</w>' <w n="53.3">croyancʼ</w> <w n="53.4">plus</w> <w n="53.5">dʼ</w> <w n="53.6">tracas</w>,</l>
					<l n="54" num="9.2"><space unit="char" quantity="2"></space><w n="54.1">Et</w> <w n="54.2">la</w> <w n="54.3">procession</w> <w n="54.4">qui</w> <w n="54.5">passe</w></l>
					<l n="55" num="9.3"><space unit="char" quantity="2"></space><w n="55.1">Ne</w> <w n="55.2">forcʼra</w> <w n="55.3">plus</w> <w n="55.4">sur</w> <w n="55.5">la</w> <w n="55.6">place</w></l>
					<l n="56" num="9.4"><space unit="char" quantity="2"></space><w n="56.1">Le</w> <w n="56.2">Juif</w> <w n="56.3">à</w> <w n="56.4">mettrʼ</w> <w n="56.5">chapeau</w> <w n="56.6">bas</w>.</l>
					<l n="57" num="9.5"><space unit="char" quantity="4"></space><w n="57.1">Non</w>, <w n="57.2">non</w>, <w n="57.3">donnez</w>-<w n="57.4">vous</w> <w n="57.5">lʼ</w> <w n="57.6">bras</w>,</l>
					<l n="58" num="9.6"><space unit="char" quantity="8"></space><w n="58.1">Qu</w>' <w n="58.2">la</w> <w n="58.3">tolérance</w></l>
					<l n="59" num="9.7"><space unit="char" quantity="10"></space><w n="59.1">Règne</w> <w n="59.2">en</w> <w n="59.3">France</w> ;</l>
					<l n="60" num="9.8"><space unit="char" quantity="4"></space><w n="60.1">Non</w>, <w n="60.2">non</w>, <w n="60.3">donnez</w>-<w n="60.4">vous</w> <w n="60.5">lʼ</w> <w n="60.6">bras</w> !</l>
					<l n="61" num="9.9"><space unit="char" quantity="2"></space><w n="61.1">Les</w> <w n="61.2">tartuffʼs</w> <w n="61.3">ne</w> <w n="61.4">rʼviendront</w> <w n="61.5">pas</w>.</l>
				</lg>
				
				<lg n="10">
					<head type="main">EN CHŒUR.</head>
					<l n="62" num="10.1"><space unit="char" quantity="4"></space><w n="62.1">Non</w>, <w n="62.2">non</w>, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden"><w n="62.3">donnons</w>-<w n="62.4">nous</w> <w n="62.5">lʼ</w> <w n="62.6">bras</w>,</add></subst></l>
					<l n="63" num="10.2"><space unit="char" quantity="8"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="63.1">Qu</w>' <w n="63.2">notrʼ</w> <w n="63.3">alliance</w></add></subst></l>
					<l n="64" num="10.3"><space unit="char" quantity="10"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="64.1">Sauvʼ</w> <w n="64.2">la</w> <w n="64.3">France</w> ; </add></subst></l>
					<l n="65" num="10.4"><space unit="char" quantity="4"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="65.1">Non</w>, <w n="65.2">non</w>, <w n="65.3">donnons</w>-<w n="65.4">nous</w> <w n="65.5">lʼ</w> <w n="65.6">bras</w></add></subst></l>
					<l n="66" num="10.5"><space unit="char" quantity="2"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="66.1">Les</w> <w n="66.2">Jésuitʼs</w> <w n="66.3">ne</w> <w n="66.4">rʼviendront</w> <w n="66.5">pas</w>. </add></subst></l>
				</lg>
				
				<lg n="11">
					<head type="main">ADOLPHE.</head>
					<l n="67" num="11.1"><space unit="char" quantity="2"></space><w n="67.1">Plus</w> <w n="67.2">de</w> <w n="67.3">censeurs</w> <w n="67.4">infernaux</w>,</l>
					<l n="68" num="11.2"><space unit="char" quantity="2"></space><w n="68.1">Gendarmes</w> <w n="68.2">de</w> <w n="68.3">la</w> <w n="68.4">pensée</w>,</l>
					<l n="69" num="11.3"><space unit="char" quantity="2"></space><w n="69.1">Que</w> <w n="69.2">leur</w> <w n="69.3">horde</w> <w n="69.4">soit</w> <w n="69.5">chassée</w> !</l>
					<l n="70" num="11.4"><space unit="char" quantity="2"></space><w n="70.1">Plus</w> <w n="70.2">dʼ</w> <w n="70.3">baillons</w> <w n="70.4">pour</w> <w n="70.5">les</w> <w n="70.6">journaux</w>.</l>
					<l n="71" num="11.5"><space unit="char" quantity="4"></space><w n="71.1">Non</w>, <w n="71.2">non</w>, <w n="71.3">donnons</w>-<w n="71.4">nous</w> <w n="71.5">lʼ</w> <w n="71.6">bras</w>,</l>
					<l n="72" num="11.6"><space unit="char" quantity="10"></space><w n="72.1">L</w>'<w n="72.2">imprimʼrie</w></l>
					<l n="73" num="11.7"><space unit="char" quantity="8"></space><w n="73.1">Est</w> <w n="73.2">affranchie</w> ;</l>
					<l n="74" num="11.8"><space unit="char" quantity="4"></space><w n="74.1">Non</w>, <w n="74.2">non</w>, <w n="74.3">donnons</w>-<w n="74.4">nous</w> <w n="74.5">lʼ</w> <w n="74.6">bras</w></l>
					<l n="75" num="11.9"><space unit="char" quantity="2"></space><w n="75.1">Les</w> <w n="75.2">censeurs</w> <w n="75.3">ne</w> <w n="75.4">rʼviendront</w> <w n="75.5">pas</w>.</l>
				</lg>
				
				<lg n="12">
					<head type="main">EN CHŒUR.</head>
					<l n="76" num="12.1"><space unit="char" quantity="4"></space><w n="76.1">Non</w>, <w n="76.2">non</w>, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden"><w n="76.3">donnons</w>-<w n="76.4">nous</w> <w n="76.5">lʼ</w> <w n="76.6">bras</w>,</add></subst></l>
					<l n="77" num="12.2"><space unit="char" quantity="8"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="77.1">Qu</w>' <w n="77.2">notrʼ</w> <w n="77.3">alliance</w></add></subst></l>
					<l n="78" num="12.3"><space unit="char" quantity="10"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="78.1">Sauvʼ</w> <w n="78.2">la</w> <w n="78.3">France</w> ; </add></subst></l>
					<l n="79" num="12.4"><space unit="char" quantity="4"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="79.1">Non</w>, <w n="79.2">non</w>, <w n="79.3">donnons</w>-<w n="79.4">nous</w> <w n="79.5">lʼ</w> <w n="79.6">bras</w></add></subst></l>
					<l n="80" num="12.5"><space unit="char" quantity="2"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="80.1">Les</w> <w n="80.2">Jésuitʼs</w> <w n="80.3">ne</w> <w n="80.4">rʼviendront</w> <w n="80.5">pas</w>. </add></subst></l>
				</lg>
				
				<lg n="13">
					<head type="main">L'APPRENTI.</head>
					<l n="81" num="13.1"><space unit="char" quantity="2"></space><w n="81.1">Aux</w> <w n="81.2">sottisʼs</w> <w n="81.3">du</w> <w n="81.4">spirituel</w></l>
					<l n="82" num="13.2"><space unit="char" quantity="2"></space><w n="82.1">La</w> <w n="82.2">raison</w> <w n="82.3">va</w> <w n="82.4">mettʼ</w> <w n="82.5">des</w> <w n="82.6">bornes</w>,</l>
					<l n="83" num="13.3"><space unit="char" quantity="2"></space><w n="83.1">Et</w> <w n="83.2">les</w> <w n="83.3">frères</w> <w n="83.4">à</w> <w n="83.5">trois</w> <w n="83.6">cornes</w>,</l>
					<l n="84" num="13.4"><space unit="char" quantity="2"></space><w n="84.1">N</w>'<w n="84.2">enfoncʼront</w> <w n="84.3">plus</w> <w n="84.4">lʼ</w> <w n="84.5">mutuel</w>.</l>
					<l n="85" num="13.5"><space unit="char" quantity="4"></space><w n="85.1">Non</w>, <w n="85.2">non</w>, <w n="85.3">donnons</w>-<w n="85.4">nous</w> <w n="85.5">lʼ</w> <w n="85.6">bras</w>,</l>
					<l n="86" num="13.6"><space unit="char" quantity="10"></space><w n="86.1">Plus</w> <w n="86.2">dʼ</w> <w n="86.3">férule</w></l>
					<l n="87" num="13.7"><space unit="char" quantity="10"></space><w n="87.1">Ridicule</w>,</l>
					<l n="88" num="13.8"><space unit="char" quantity="4"></space><w n="88.1">Non</w>, <w n="88.2">non</w>, <w n="88.3">donnons</w>-<w n="88.4">nous</w> <w n="88.5">lʼ</w> <w n="88.6">bras</w> !</l>
					<l n="89" num="13.9"><space unit="char" quantity="2"></space><w n="89.1">Les</w> <w n="89.2">fouetteurs</w> <w n="89.3">ne</w> <w n="89.4">rʼviendront</w> <w n="89.5">pas</w>.</l>
				</lg>
				
				<lg n="14">
					<head type="main">EN CHŒUR.</head>
					<l n="90" num="14.1"><space unit="char" quantity="4"></space><w n="90.1">Non</w>, <w n="90.2">non</w>, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden"><w n="90.3">donnons</w>-<w n="90.4">nous</w> <w n="90.5">lʼ</w> <w n="90.6">bras</w>,</add></subst></l>
					<l n="91" num="14.2"><space unit="char" quantity="8"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="91.1">Qu</w>' <w n="91.2">notrʼ</w> <w n="91.3">alliance</w></add></subst></l>
					<l n="92" num="14.3"><space unit="char" quantity="10"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="92.1">Sauvʼ</w> <w n="92.2">la</w> <w n="92.3">France</w> ; </add></subst></l>
					<l n="93" num="14.4"><space unit="char" quantity="4"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="93.1">Non</w>, <w n="93.2">non</w>, <w n="93.3">donnons</w>-<w n="93.4">nous</w> <w n="93.5">lʼ</w> <w n="93.6">bras</w></add></subst></l>
					<l n="94" num="14.5"><space unit="char" quantity="2"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="94.1">Les</w> <w n="94.2">Jésuitʼs</w> <w n="94.3">ne</w> <w n="94.4">rʼviendront</w> <w n="94.5">pas</w>. </add></subst></l>
				</lg>
				
				<lg n="15">
					<head type="main">UN JEUNE BOURGEOIS.</head>
					<l n="95" num="15.1"><space unit="char" quantity="2"></space><w n="95.1">Gens</w> <w n="95.2">de</w> <w n="95.3">la</w> <w n="95.4">congrégation</w>,</l>
					<l n="96" num="15.2"><space unit="char" quantity="2"></space><w n="96.1">Dans</w> <w n="96.2">votʼ</w> <w n="96.3">systèmʼ</w> <w n="96.4">déplorable</w>,</l>
					<l n="97" num="15.3"><space unit="char" quantity="2"></space><w n="97.1">Molièr</w> <w n="97.2">sʼrait</w> <w n="97.3">un</w> <w n="97.4">misérable</w> ?</l>
					<l n="98" num="15.4"><space unit="char" quantity="2"></space><w n="98.1">Talma</w> <w n="98.2">sʼrait</w> <w n="98.3">un</w> <w n="98.4">histrion</w> ?</l>
					<l n="99" num="15.5"><space unit="char" quantity="4"></space><w n="99.1">Non</w>, <w n="99.2">non</w>, <w n="99.3">donnons</w>-<w n="99.4">nous</w> <w n="99.5">lʼ</w> <w n="99.6">bras</w> !</l>
					<l n="100" num="15.6"><space unit="char" quantity="10"></space><w n="100.1">Qu</w>'<w n="100.2">on</w> <w n="100.3">les</w> <w n="100.4">traîne</w></l>
					<l n="101" num="15.7"><space unit="char" quantity="10"></space><w n="101.1">Sur</w> <w n="101.2">la</w> <w n="101.3">scène</w> !</l>
					<l n="102" num="15.8"><space unit="char" quantity="4"></space><w n="102.1">Non</w>, <w n="102.2">non</w>, <w n="102.3">donnons</w>-<w n="102.4">nous</w> <w n="102.5">lʼ</w> <w n="102.6">bras</w> !</l>
					<l n="103" num="15.9"><space unit="char" quantity="2"></space><w n="103.1">Les</w> <w n="103.2">Boudet</w> <w n="103.3">ne</w> <w n="103.4">rʼviendront</w> <w n="103.5">pas</w>.</l>
				</lg>
				
				<lg n="16">
					<head type="main">EN CHŒUR.</head>
					<l n="104" num="16.1"><space unit="char" quantity="4"></space><w n="104.1">Non</w>, <w n="104.2">non</w>, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden"><w n="104.3">donnons</w>-<w n="104.4">nous</w> <w n="104.5">lʼ</w> <w n="104.6">bras</w>,</add></subst></l>
					<l n="105" num="16.2"><space unit="char" quantity="8"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="105.1">Qu</w>' <w n="105.2">notrʼ</w> <w n="105.3">alliance</w></add></subst></l>
					<l n="106" num="16.3"><space unit="char" quantity="10"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="106.1">Sauvʼ</w> <w n="106.2">la</w> <w n="106.3">France</w> ; </add></subst></l>
					<l n="107" num="16.4"><space unit="char" quantity="4"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="107.1">Non</w>, <w n="107.2">non</w>, <w n="107.3">donnons</w>-<w n="107.4">nous</w> <w n="107.5">lʼ</w> <w n="107.6">bras</w></add></subst></l>
					<l n="108" num="16.5"><space unit="char" quantity="2"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="108.1">Les</w> <w n="108.2">Jésuitʼs</w> <w n="108.3">ne</w> <w n="108.4">rʼviendront</w> <w n="108.5">pas</w>. </add></subst></l>
				</lg>
				
				<lg n="17">
					<head type="main">LOUISE.</head>
					<l n="109" num="17.1"><space unit="char" quantity="2"></space><w n="109.1">Chansonniers</w> <w n="109.2">dʼ</w> <w n="109.3">tous</w> <w n="109.4">les</w> <w n="109.5">partis</w> !</l>
					<l n="110" num="17.2"><space unit="char" quantity="2"></space><w n="110.1">Fidèlʼs</w> <w n="110.2">à</w> <w n="110.3">votre</w> <w n="110.4">principe</w>,</l>
					<l n="111" num="17.3"><space unit="char" quantity="2"></space><w n="111.1">Chantʼrez</w>-<w n="111.2">vous</w> <w n="111.3">la</w> <w n="111.4">Saint</w>-<w n="111.5">Philippe</w>,</l>
					<l n="112" num="17.4"><space unit="char" quantity="2"></space><w n="112.1">Vous</w> <w n="112.2">qui</w> <w n="112.3">chantiez</w> <w n="112.4">la</w> <w n="112.5">Saint</w>-<w n="112.6">Louis</w> ?</l>
					<l n="113" num="17.5"><space unit="char" quantity="4"></space><w n="113.1">Non</w>, <w n="113.2">non</w>, <w n="113.3">donnons</w>-<w n="113.4">nous</w> <w n="113.5">lʼ</w> <w n="113.6">bras</w> !</l>
					<l n="114" num="17.6"><space unit="char" quantity="10"></space><w n="114.1">Plus</w> <w n="114.2">d</w>'<w n="114.3">hommage</w></l>
					<l n="115" num="17.7"><space unit="char" quantity="8"></space><w n="115.1">A</w> <w n="115.2">tant</w> <w n="115.3">la</w> <w n="115.4">page</w>.</l>
					<l n="116" num="17.8"><space unit="char" quantity="4"></space><w n="116.1">Non</w>, <w n="116.2">non</w>, <w n="116.3">donnons</w>-<w n="116.4">nous</w> <w n="116.5">lʼ</w> <w n="116.6">bras</w> ?</l>
					<l n="117" num="17.9"><w n="117.1">Les</w> <w n="117.2">girouettʼs</w> <w n="117.3">ne</w> <w n="117.4">reviendront</w> <w n="117.5">pas</w>.</l>
				</lg>
				
				<lg n="18">
					<head type="main">EN CHŒUR.</head>
					<l n="118" num="18.1"><space unit="char" quantity="4"></space><w n="118.1">Non</w>, <w n="118.2">non</w>, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden"><w n="118.3">donnons</w>-<w n="118.4">nous</w> <w n="118.5">lʼ</w> <w n="118.6">bras</w>,</add></subst></l>
					<l n="119" num="18.2"><space unit="char" quantity="8"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="119.1">Qu</w>' <w n="119.2">notrʼ</w> <w n="119.3">alliance</w></add></subst></l>
					<l n="120" num="18.3"><space unit="char" quantity="10"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="120.1">Sauvʼ</w> <w n="120.2">la</w> <w n="120.3">France</w> ; </add></subst></l>
					<l n="121" num="18.4"><space unit="char" quantity="4"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="121.1">Non</w>, <w n="121.2">non</w>, <w n="121.3">donnons</w>-<w n="121.4">nous</w> <w n="121.5">lʼ</w> <w n="121.6">bras</w></add></subst></l>
					<l n="122" num="18.5"><space unit="char" quantity="2"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="122.1">Les</w> <w n="122.2">Jésuitʼs</w> <w n="122.3">ne</w> <w n="122.4">rʼviendront</w> <w n="122.5">pas</w>. </add></subst></l>
				</lg>
			
				<lg n="19">
						<head type="main">PRUNEAU.</head>
					<l n="123" num="19.1"><space unit="char" quantity="2"></space><w n="123.1">Si</w> <w n="123.2">lʼ</w> <w n="123.3">trônʼ</w> <w n="123.4">courait</w> <w n="123.5">quelqu</w>' <w n="123.6">danger</w>,</l>
					<l n="124" num="19.2"><w n="124.1">Le</w> <w n="124.2">défendʼ</w> <w n="124.3">c</w>'<w n="124.4">est</w> <w n="124.5">nous</w> <w n="124.6">qu</w>' <w n="124.7">ça</w> <w n="124.8">regarde</w> ;</l>
					<l n="125" num="19.3"><space unit="char" quantity="2"></space><w n="125.1">Nous</w> <w n="125.2">sommʼs</w> <w n="125.3">sa</w> <w n="125.4">meilleure</w> <w n="125.5">garde</w>,</l>
					<l n="126" num="19.4"><space unit="char" quantity="2"></space><w n="126.1">Plus</w> <w n="126.2">d</w>'<w n="126.3">uniforme</w> <w n="126.4">étranger</w>.</l>
					<l n="127" num="19.5"><space unit="char" quantity="4"></space><w n="127.1">Non</w>, <w n="127.2">non</w>, <w n="127.3">donnons</w>-<w n="127.4">nous</w> <w n="127.5">lʼ</w> <w n="127.6">bras</w>,</l>
					<l n="128" num="19.6"><space unit="char" quantity="10"></space><w n="128.1">Qu</w>'<w n="128.2">on</w> <w n="128.3">les</w> <w n="128.4">berne</w></l>
					<l n="129" num="19.7"><space unit="char" quantity="10"></space><w n="129.1">Jusqu</w>'<w n="129.2">à</w> <w n="129.3">Berne</w> !</l>
					<l n="130" num="19.8"><space unit="char" quantity="4"></space><w n="130.1">Non</w>, <w n="130.2">non</w>, <w n="130.3">donnons</w>-<w n="130.4">nous</w> <w n="130.5">lʼ</w> <w n="130.6">bras</w> !</l>
					<l n="131" num="19.9"><w n="131.1">Non</w> ! <w n="131.2">les</w> <w n="131.3">Suissʼs</w> <w n="131.4">ne</w> <w n="131.5">reviendront</w> <w n="131.6">pas</w>.</l>
				</lg>
				
				<lg n="20">
					<head type="main">EN CHŒUR.</head>
					<l n="132" num="20.1"><space unit="char" quantity="4"></space><w n="132.1">Non</w>, <w n="132.2">non</w>, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden"><w n="132.3">donnons</w>-<w n="132.4">nous</w> <w n="132.5">lʼ</w> <w n="132.6">bras</w>,</add></subst></l>
					<l n="133" num="20.2"><space unit="char" quantity="8"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="133.1">Qu</w>' <w n="133.2">notrʼ</w> <w n="133.3">alliance</w></add></subst></l>
					<l n="134" num="20.3"><space unit="char" quantity="10"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="134.1">Sauvʼ</w> <w n="134.2">la</w> <w n="134.3">France</w> ; </add></subst></l>
					<l n="135" num="20.4"><space unit="char" quantity="4"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="135.1">Non</w>, <w n="135.2">non</w>, <w n="135.3">donnons</w>-<w n="135.4">nous</w> <w n="135.5">lʼ</w> <w n="135.6">bras</w></add></subst></l>
					<l n="136" num="20.5"><space unit="char" quantity="2"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="136.1">Les</w> <w n="136.2">Jésuitʼs</w> <w n="136.3">ne</w> <w n="136.4">rʼviendront</w> <w n="136.5">pas</w>. </add></subst></l>
				</lg>
				
				<lg n="21">
					<head type="main">JULIEN.</head>
					<l n="137" num="21.1"><space unit="char" quantity="2"></space><w n="137.1">Amis</w>, <w n="137.2">partout</w> <w n="137.3">j</w>'<w n="137.4">ai</w> <w n="137.5">couru</w>,</l>
					<l n="138" num="21.2"><space unit="char" quantity="2"></space><w n="138.1">Je</w> <w n="138.2">n</w>'<w n="138.3">ai</w> <w n="138.4">pas</w> <w n="138.5">vu</w> <w n="138.6">dʼ</w> <w n="138.7">robe</w> <w n="138.8">noire</w> ;</l>
					<l n="139" num="21.3"><space unit="char" quantity="2"></space><w n="139.1">Depuis</w> <w n="139.2">que</w> <w n="139.3">lʼ</w> <w n="139.4">coq</w> <w n="139.5">chantʼ</w> <w n="139.6">victoire</w>,</l>
					<l n="140" num="21.4"><space unit="char" quantity="2"></space><w n="140.1">Les</w> <w n="140.2">dindons</w> <w n="140.3">ont</w> <w n="140.4">disparu</w>.</l>
					<l n="141" num="21.5"><space unit="char" quantity="4"></space><w n="141.1">Bon</w>, <w n="141.2">bon</w>, <w n="141.3">donnons</w>-<w n="141.4">nous</w> <w n="141.5">lʼ</w> <w n="141.6">bras</w>,</l>
					<l n="142" num="21.6"><space unit="char" quantity="8"></space><w n="142.1">Qu</w>'<w n="142.2">on</w> <w n="142.3">les</w> <w n="142.4">escorte</w></l>
					<l n="143" num="21.7"><space unit="char" quantity="10"></space><w n="143.1">A</w> <w n="143.2">la</w> <w n="143.3">porte</w> !</l>
					<l n="144" num="21.8"><space unit="char" quantity="4"></space><w n="144.1">Bon</w> ! <w n="144.2">bon</w> ! <w n="144.3">donnons</w>-<w n="144.4">nous</w> <w n="144.5">lʼ</w> <w n="144.6">bras</w>,</l>
					<l n="145" num="21.9"><space unit="char" quantity="2"></space><w n="145.1">Les</w> <w n="145.2">dindons</w> <w n="145.3">ne</w> <w n="145.4">rʼviendront</w> <w n="145.5">pas</w>.</l>
				</lg>
				
				<lg n="22">
					<head type="main">EN CHŒUR,</head>
					<l n="146" num="22.1"><space unit="char" quantity="4"></space><w n="146.1">Non</w>, <w n="146.2">non</w>, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden"><w n="146.3">donnons</w>-<w n="146.4">nous</w> <w n="146.5">lʼ</w> <w n="146.6">bras</w>,</add></subst></l>
					<l n="147" num="22.2"><space unit="char" quantity="8"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="147.1">Qu</w>' <w n="147.2">notrʼ</w> <w n="147.3">alliance</w></add></subst></l>
					<l n="148" num="22.3"><space unit="char" quantity="10"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="148.1">Sauvʼ</w> <w n="148.2">la</w> <w n="148.3">France</w> ; </add></subst></l>
					<l n="149" num="22.4"><space unit="char" quantity="4"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="149.1">Non</w>, <w n="149.2">non</w>, <w n="149.3">donnons</w>-<w n="149.4">nous</w> <w n="149.5">lʼ</w> <w n="149.6">bras</w></add></subst></l>
					<l n="150" num="22.5"><space unit="char" quantity="2"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="150.1">Les</w> <w n="150.2">Jésuitʼs</w> <w n="150.3">ne</w> <w n="150.4">rʼviendront</w> <w n="150.5">pas</w>. </add></subst></l>
				</lg>
		</div></body></text></TEI>