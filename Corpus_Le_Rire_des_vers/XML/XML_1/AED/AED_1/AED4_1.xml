<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">27, 28 ET 29 JUILLET, TABLEAU ÉPISODIQUE DES TROIS JOURNÉES.</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ARA" sort="1">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<author key="DUV" sort="2">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>495 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
						<author>ARAGO ET DUVERT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=xjVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
								<author>ARAGO ET DUVERT</author>
								<repository>Österreichische Nationalbibliothek</repository>
								<idno type="URI">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886206</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : U+02BC MODIFIER LETTER APOSTROPHE) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<change when="2021-06-14" who="RR">Quelques corrections concernant la distribution du signe signe ʼ (UNICODE : ʼ).</change>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE JOURNÉE.</head><head type="main_subpart">SCÈNE III.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="AED4">
			<head type="tune">AIR : De Turenne.</head>
			<lg n="1">
				<head type="main">RAIMOND.</head>
				<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">artisan</w> <w n="1.3">ne</w> <w n="1.4">fait</w> <w n="1.5">pas</w> <w n="1.6">tant</w> <w n="1.7">dʼ</w> <w n="1.8">grimace</w>.</l>
				<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">la</w> <w n="2.3">rʼligion</w> <w n="2.4">que</w> <w n="2.5">d</w>'<w n="2.6">autres</w> <w n="2.7">fassʼnt</w> <w n="2.8">métier</w>.</l>
				<l n="3" num="1.3"><space unit="char" quantity="6"></space><w n="3.1">Dieu</w> <w n="3.2">n</w>'<w n="3.3">exige</w> <w n="3.4">pas</w> <w n="3.5">que</w> <w n="3.6">l</w>'<w n="3.7">on</w> <w n="3.8">passe</w></l>
				<l n="4" num="1.4"><space unit="char" quantity="6"></space><w n="4.1">Douze</w> <w n="4.2">heurʼs</w> <w n="4.3">par</w> <w n="4.4">jour</w> <w n="4.5">à</w> <w n="4.6">le</w> <w n="4.7">prier</w>.</l>
				<l n="5" num="1.5"><w n="5.1">Lʼ</w> <w n="5.2">travail</w>, <w n="5.3">voilà</w> <w n="5.4">la</w> <w n="5.5">prièrʼ</w> <w n="5.6">dʼ</w> <w n="5.7">l</w>'<w n="5.8">ouvrier</w>.</l>
				<l n="6" num="1.6"><w n="6.1">Franchʼment</w>, <w n="6.2">quand</w> <w n="6.3">jʼ</w> <w n="6.4">vois</w> <w n="6.5">venir</w> <w n="6.6">un</w> <w n="6.7">bon</w> <w n="6.8">apôtre</w></l>
				<l n="7" num="1.7"><w n="7.1">Qui</w> <w n="7.2">vers</w> <w n="7.3">le</w> <w n="7.4">ciel</w> <w n="7.5">lèvʼ</w> <w n="7.6">toujours</w> <w n="7.7">son</w> <w n="7.8">regard</w>,</l>
				<l n="8" num="1.8"><w n="8.1">Je</w> <w n="8.2">me</w> <w n="8.3">dis</w> : <w n="8.4">Il</w> <w n="8.5">est</w> <w n="8.6">ou</w> <w n="8.7">jésuite</w>, <w n="8.8">ou</w> <w n="8.9">mouchard</w>,</l>
				<l n="9" num="1.9"><space unit="char" quantity="6"></space><w n="9.1">Si</w> <w n="9.2">même</w> <w n="9.3">il</w> <w n="9.4">n</w>'<w n="9.5">est</w> <w n="9.6">pas</w> <w n="9.7">l</w>'<w n="9.8">un</w> <w n="9.9">et</w> <w n="9.10">l</w>'<w n="9.11">autre</w>.</l>
			</lg>
		</div></body></text></TEI>