<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANGÉLIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIEME.</head><head type="main_subpart">SCÈNE V.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD31">
				<head type="tune">Air : Je voulais bien.</head>
					<lg n="1">
						<head type="main">MÈRE GIBOU.</head>
						<l n="1" num="1.1"><space unit="char" quantity="10"></space><w n="1.1">Ça</w> <w n="1.2">t</w>'<w n="1.3">allait</w> <w n="1.4">bien</w> <del type="repetition" reason="analysis" hand="LG">(bis)</del></l>
						<l n="2" num="1.2"><space unit="char" quantity="10"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="2.1">Ça</w> <w n="2.2">t</w>'<w n="2.3">allait</w> <w n="2.4">bien</w></add></subst></l>
						<l n="3" num="1.3"><w n="3.1">Quand</w> <w n="3.2">tu</w> <w n="3.3">fʼsais</w> <w n="3.4">la</w> <w n="3.5">cour</w> <w n="3.6">à</w> <w n="3.7">Jeannette</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Ton</w> <w n="4.2">tablier</w>, <w n="4.3">ta</w> <w n="4.4">grossʼ</w> <w n="4.5">casquette</w></l>
						<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">mêmʼ</w> <w n="5.3">ta</w> <w n="5.4">veste</w> <w n="5.5">dʼ</w> <w n="5.6">faubourien</w>,</l>
						<l n="6" num="1.6"><space unit="char" quantity="10"></space><w n="6.1">Ça</w> <w n="6.2">t</w>'<w n="6.3">allait</w> <w n="6.4">bien</w> <del type="repetition" reason="analysis" hand="LG">(bis)</del></l>
						<l n="7" num="1.7"><space unit="char" quantity="10"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="7.1">Ça</w> <w n="7.2">t</w>'<w n="7.3">allait</w> <w n="7.4">bien</w></add></subst></l>
						<l n="8" num="1.8"><w n="8.1">Mais</w> <w n="8.2">maintenant</w> <w n="8.3">paré</w> <w n="8.4">comme</w> <w n="8.5">unʼ</w> <w n="8.6">châsse</w></l>
						<l n="9" num="1.9"><w n="9.1">Quand</w> <w n="9.2">tu</w> <w n="9.3">ris</w>, <w n="9.4">tu</w> <w n="9.5">fais</w> <w n="9.6">la</w> <w n="9.7">grimace</w> …</l>
						<l n="10" num="1.10"><w n="10.1">Tʼas</w> <w n="10.2">l</w>'<w n="10.3">air</w> <w n="10.4">du</w> <w n="10.5">marquis</w> <w n="10.6">dʼ</w> <w n="10.7">Carabas</w>.</l>
						<l n="11" num="1.11"><space unit="char" quantity="10"></space><w n="11.1">Ça</w> <w n="11.2">nʼ</w> <w n="11.3">te</w> <w n="11.4">va</w> <w n="11.5">pas</w>. <del type="repetition" reason="analysis" hand="LG">(bis)</del></l>
						<l n="12" num="1.12"><space unit="char" quantity="10"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="12.1">Ça</w> <w n="12.2">nʼte</w> <w n="12.3">va</w> <w n="12.4">pas</w>.</add></subst></l>
						<l n="13" num="1.13"><w n="13.1">Non</w>, <w n="13.2">non</w>, <w n="13.3">non</w>, <w n="13.4">non</w>, <w n="13.5">ça</w> <w n="13.6">nʼ</w> <w n="13.7">te</w> <w n="13.8">va</w> <w n="13.9">pas</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Non</w>, <w n="14.2">mon</w> <w n="14.3">garçon</w>, <w n="14.4">ça</w> <w n="14.5">nʼ</w> <w n="14.6">te</w> <w n="14.7">va</w> <w n="14.8">pas</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Non</w>, <w n="15.2">non</w>, <w n="15.3">non</w>, <w n="15.4">non</w> … <w n="15.5">ça</w> <w n="15.6">nʼ</w> <w n="15.7">te</w> <w n="15.8">va</w> <w n="15.9">pas</w>.</l>
					</lg>
			</div></body></text></TEI>