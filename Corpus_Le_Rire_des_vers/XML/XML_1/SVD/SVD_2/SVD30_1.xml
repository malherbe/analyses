<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANGÉLIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIEME.</head><head type="main_subpart">SCÈNE IV.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD30">
				<head type="tune">AIR : Bonjour, mon ami Vincent.</head>
				<lg n="1">
					<head type="main">JULES.</head>
					<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">Entendez</w> <w n="1.2">ces</w> <w n="1.3">cris</w> <w n="1.4">joyeux</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Dʼ</w> <w n="2.2">mes</w> <w n="2.3">amis</w>, <w n="2.4">la</w> <w n="2.5">voix</w> <w n="2.6">m</w>'<w n="2.7">appelle</w>…</l>
				</lg>
				<lg n="2">
					<head type="main">ANGÉLIQUE.</head>
					<l n="3" num="2.1"><space unit="char" quantity="8"></space><w n="3.1">Allons</w> <w n="3.2">oublier</w> <w n="3.3">près</w> <w n="3.4">d</w>'<w n="3.5">eux</w></l>
					<l n="4" num="2.2"><space unit="char" quantity="8"></space><w n="4.1">Cette</w> <w n="4.2">mauvaise</w> <w n="4.3">nouvelle</w>…</l>
				</lg>
				<lg n="3">
					<head type="main">JULES.</head>
					<l n="5" num="3.1"><w n="5.1">Quoi</w>, <w n="5.2">sous</w> <w n="5.3">les</w> <w n="5.4">verroux</w> <w n="5.5">vouloir</w> <w n="5.6">m</w>'<w n="5.7">enfermer</w>,</l>
					<l n="6" num="3.2"><w n="6.1">Quand</w> <w n="6.2">le</w> <w n="6.3">mois</w> <w n="6.4">de</w> <w n="6.5">mai</w> <w n="6.6">rʼvient</w> <w n="6.7">pour</w> <w n="6.8">nous</w> <w n="6.9">charmer</w> ;</l>
					<l n="7" num="3.3"><w n="7.1">L</w>'<w n="7.2">oiseau</w> <w n="7.3">nʼ</w> <w n="7.4">se</w> <w n="7.5">laissʼ</w> <w n="7.6">pas</w> <w n="7.7">comme</w> <w n="7.8">ça</w> <w n="7.9">couper</w> <w n="7.10">l</w>'<w n="7.11">aile</w></l>
					<l n="8" num="3.4"><w n="8.1">Lorsque</w> <w n="8.2">d</w>'<w n="8.3">être</w> <w n="8.4">libre</w> <w n="8.5">il</w> <w n="8.6">est</w> <w n="8.7">encore</w> <w n="8.8">tems</w>…</l>
					<l n="9" num="3.5"><space unit="char" quantity="12"></space><w n="9.1">Il</w> <w n="9.2">dit</w> <w n="9.3">dans</w> <w n="9.4">ses</w> <w n="9.5">chants</w> :</l>
					<l n="10" num="3.6"><space unit="char" quantity="12"></space> <w n="10.1">Voilà</w> <w n="10.2">le</w> <w n="10.3">printems</w>,</l>
					<l n="11" num="3.7"><w n="11.1">Prenons</w> <w n="11.2">ma</w> <w n="11.3">volée</w> <w n="11.4">et</w> <w n="11.5">la</w> <w n="11.6">clé</w> <w n="11.7">des</w> <w n="11.8">champs</w> ! »</l>
					<p>(Il se sauve avec Angelique en répétant :)</p>
					<l n="12" num="3.8"><space unit="char" quantity="12"></space> <w n="12.1">Voilà</w> <w n="12.2">le</w> <w n="12.3">printems</w>,</l>
					<l n="13" num="3.9"><w n="13.1">Prenons</w> <w n="13.2">notʼ</w> <w n="13.3">volée</w> <w n="13.4">et</w> <w n="13.5">la</w> <w n="13.6">clé</w> <w n="13.7">des</w> <w n="13.8">champs</w> !</l>
				</lg>
			</div></body></text></TEI>