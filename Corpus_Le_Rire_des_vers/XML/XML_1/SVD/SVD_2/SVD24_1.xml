<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANGÉLIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE VIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD24">
				<head type="tune">Air : En ce tems-là (de la Fiancée.)</head>
				<lg n="1">
					<head type="main">JEANNETON.</head>
					<l n="1" num="1.1"><w n="1.1">Avancez</w>, <w n="1.2">jeune</w> <w n="1.3">fille</w> !</l>
					</lg>
				<lg n="2">
					<head type="main">GRATIEN.</head>
					<l n="2" num="2.1"><w n="2.1">Quels</w> <w n="2.2">sont</w> <w n="2.3">vos</w> <w n="2.4">revenus</w> ?</l>
					</lg>
				<lg n="3">
					<head type="main">ANGÉLIQUE.</head>
					<l n="3" num="3.1"><w n="3.1">Je</w> <w n="3.2">n</w>'<w n="3.3">ai</w> <w n="3.4">rien</w> <w n="3.5">qu</w>' <w n="3.6">mon</w> <w n="3.7">aiguille</w> …</l>
					</lg>
				<lg n="4">
					<head type="main">JEANNETON.</head>
					<l part="I" n="4" num="4.1"><w n="4.1">Vos</w> <w n="4.2">parens</w> ? </l>
					</lg>
				<lg n="5">
					<head type="main">ANGÉLIQUE.</head>
					<l part="F" n="4">…<w n="4.3">Jʼ</w> <w n="4.4">n</w>'<w n="4.5">en</w> <w n="4.6">ai</w> <w n="4.7">plus</w>…</l>
					<l n="5" num="5.1"><w n="5.1">Je</w> <w n="5.2">n</w>'<w n="5.3">eus</w> <w n="5.4">sur</w> <w n="5.5">cette</w> <w n="5.6">terre</w></l>
					<l n="6" num="5.2"><w n="6.1">Qu</w>'<w n="6.2">un</w> <w n="6.3">seul</w> <w n="6.4">cœur</w> <w n="6.5">qui</w> <w n="6.6">m</w>'<w n="6.7">aima</w>,</l>
					<l n="7" num="5.3"><w n="7.1">Encor</w> <w n="7.2">c</w>'<w n="7.3">est</w> <w n="7.4">un</w> <w n="7.5">mystère</w>.</l>
					</lg>
				<lg n="6">
					<head type="main">JEANNETON.</head>
					<l n="8" num="6.1"><w n="8.1">Expliquez</w>-<w n="8.2">nous</w> <w n="8.3">cela</w> ?</l>
				</lg>

				<div n="1" type="section">
					<head type="main">(bis.) {</head>
					<lg n="1">
						<head type="main">ANGÉLIQUE, montrant Jules.</head>
						<l n="9" num="1.1"><space unit="char" quantity="4"></space><w n="9.1">Ce</w> <w n="9.2">secret</w> <w n="9.3">là</w> …</l>
						<l n="10" num="1.2"><w n="10.1">Monsieur</w> <w n="10.2">vous</w> <w n="10.3">le</w> <w n="10.4">dira</w> !</l>
						<l n="11" num="1.3"><space unit="char" quantity="4"></space><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="11.1">Ce</w> <w n="11.2">secret</w> <w n="11.3">là</w> … </add></subst></l>
						<l n="12" num="1.4"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="12.1">Monsieur</w> <w n="12.2">vous</w> <w n="12.3">le</w> <w n="12.4">dira</w> !</add></subst></l>
					</lg>
				</div>
			</div></body></text></TEI>