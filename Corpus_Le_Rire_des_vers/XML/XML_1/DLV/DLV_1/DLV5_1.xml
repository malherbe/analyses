<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
				<title type="sub">INVRAISEMBLANCE EN TROIS PARTIES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
				  <name>
					<forename>Félix-Auguste</forename>
					<surname>DUVERT</surname>
				  </name>
				  <date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="DLV" sort="2">
					<name>
						<forename>Augustin Théodore</forename>
						<nameLink>de</nameLink>
						<surname>LAUZANNE DE VAUROUSSEL</surname>
						<addname type="other">Lauzanne</addname>
					</name>
					<date from="1805" to="1877">1805-1877</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>135 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
                    <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
						<author>F.-A. DUVERT EN SOCIÉTÉ AVEC M. LAUZANNE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?vid=UOM:39015076863409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>University of Michigan Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/mdp.39015076863409</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">16 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DLV5">
	<head type="tune">AIR : Gymnasiens ! remettons à quinzaine.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Dors</w>, <w n="1.2">Jonathas</w>, <w n="1.3">et</w> <w n="1.4">que</w> <w n="1.5">l</w>'<w n="1.6">diable</w> <w n="1.7">t</w>'<w n="1.8">enlève</w> ! </l>
		<l n="2" num="1.2"><w n="2.1">À</w> <w n="2.2">ton</w> <w n="2.3">réveil</w>, <w n="2.4">nous</w> <w n="2.5">viendrons</w> <w n="2.6">te</w> <w n="2.7">revoir</w>. </l>
		<l n="3" num="1.3"><w n="3.1">Laissons</w> <w n="3.2">en</w> <w n="3.3">paix</w> <w n="3.4">l</w>'<w n="3.5">ambitieux</w> <w n="3.6">qui</w> <w n="3.7">réve</w>, </l>
		<l n="4" num="1.4"><w n="4.1">Jusqu</w>'<w n="4.2">au</w> <w n="4.3">revoir</w>, </l>
		<l n="5" num="1.5"><w n="5.1">Bonsoir</w>. </l>
		<l n="6" num="1.6"><w n="6.1">Ronfle</w> <w n="6.2">toujours</w> <w n="6.3">comme</w> <w n="6.4">un</w> <w n="6.5">toupi</w>' <w n="6.6">d</w>'<w n="6.7">Allʼmagne</w>, </l>
		<l n="7" num="1.7"><w n="7.1">Car</w> <w n="7.2">le</w> <w n="7.3">sommeil</w> <w n="7.4">est</w> <w n="7.5">le</w> <w n="7.6">dieu</w> <w n="7.7">des</w> <w n="7.8">maris</w>. </l>
		<l n="8" num="1.8"><w n="8.1">Oui</w>, <w n="8.2">mais</w> <w n="8.3">le</w> <w n="8.4">soir</w> <w n="8.5">l</w>'<w n="8.6">amour</w> <w n="8.7">entre</w> <w n="8.8">en</w> <w n="8.9">campagne</w>, </l>
		<l n="9" num="1.9"><w n="9.1">Les</w> <w n="9.2">amoureux</w> <w n="9.3">sont</w> <w n="9.4">comm</w>' <w n="9.5">les</w> <w n="9.6">chauvʼs</w>-<w n="9.7">souris</w>. </l>
	</lg>
	<lg n="2">
	<head type="speaker">ENSEMBLE.</head>
		<l ana="unanalyzable" n="10" num="2.1">Dors, Jonathas, etc.</l>
	</lg>
	<lg n="3">
	<head type="speaker">DOROTHÉE.</head>
		<l n="11" num="3.1"><w n="11.1">Dors</w>, <w n="11.2">Jonathas</w>, <w n="11.3">que</w> <w n="11.4">ton</w> <w n="11.5">bonheur</w> <w n="11.6">s</w>'<w n="11.7">achève</w>, </l>
		<l n="12" num="3.2"><w n="12.1">À</w> <w n="12.2">ton</w> <w n="12.3">réveil</w> <w n="12.4">nous</w> <w n="12.5">viendrons</w> <w n="12.6">te</w> <w n="12.7">revoir</w>. </l>
		<l n="13" num="3.3"><w n="13.1">Faut</w> <w n="13.2">pas</w> <w n="13.3">troubler</w> <w n="13.4">l</w>'<w n="13.5">honnête</w> <w n="13.6">homme</w> <w n="13.7">qui</w> <w n="13.8">réve</w> ; </l>
		<l n="14" num="3.4"><w n="14.1">Jusqu</w>'<w n="14.2">au</w> <w n="14.3">revoir</w>, </l>
		<l n="15" num="3.5"><w n="15.1">Bonsoir</w>. </l>
	</lg>
</div></body></text></TEI>