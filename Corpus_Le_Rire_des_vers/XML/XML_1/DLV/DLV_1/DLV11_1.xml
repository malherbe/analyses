<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
				<title type="sub">INVRAISEMBLANCE EN TROIS PARTIES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
				  <name>
					<forename>Félix-Auguste</forename>
					<surname>DUVERT</surname>
				  </name>
				  <date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="DLV" sort="2">
					<name>
						<forename>Augustin Théodore</forename>
						<nameLink>de</nameLink>
						<surname>LAUZANNE DE VAUROUSSEL</surname>
						<addname type="other">Lauzanne</addname>
					</name>
					<date from="1805" to="1877">1805-1877</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>135 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
                    <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
						<author>F.-A. DUVERT EN SOCIÉTÉ AVEC M. LAUZANNE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?vid=UOM:39015076863409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>University of Michigan Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/mdp.39015076863409</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">16 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DLV11">
	<head type="tune">AIR de la Dame du Lac.</head>
		<div type="section" n="1">
			<head type="main">ENSEMBLE</head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">Ah</w> ! <w n="1.2">quelle</w> <w n="1.3">horreur</w> ! <w n="1.4">est</w>-<w n="1.5">il</w> <w n="1.6">permis</w> ? </l>
				<l n="2" num="1.2"><w n="2.1">Ah</w> ! <w n="2.2">c</w>'<w n="2.3">est</w> <w n="2.4">une</w> <w n="2.5">infamie</w> ! </l>
				<l n="3" num="1.3"><w n="3.1">Ce</w> <w n="3.2">couple</w> <w n="3.3">l</w>'<w n="3.4">injurie</w>, </l>
				<l n="4" num="1.4"><w n="4.1">Qu</w>'<w n="4.2">à</w> <w n="4.3">la</w> <w n="4.4">porte</w> <w n="4.5">il</w> <w n="4.6">soit</w> <w n="4.7">mis</w>.</l>
			</lg> 
			<lg n="2">
			<head type="speaker">JONATHAS.</head>
				<l n="5" num="2.1"><w n="5.1">Ah</w> ! <w n="5.2">quelle</w> <w n="5.3">horreur</w> ! <w n="5.4">est</w>-<w n="5.5">il</w> <w n="5.6">permis</w> ? </l>
				<l n="6" num="2.2"><w n="6.1">Ah</w> ! <w n="6.2">c</w>'<w n="6.3">est</w> <w n="6.4">une</w> <w n="6.5">infamie</w> ! </l>
				<l n="7" num="2.3"><w n="7.1">Ce</w> <w n="7.2">couple</w> <w n="7.3">m</w>'<w n="7.4">injurie</w>, </l>
				<l n="8" num="2.4"><w n="8.1">Qu</w>'<w n="8.2">à</w> <w n="8.3">la</w> <w n="8.4">porte</w> <w n="8.5">il</w> <w n="8.6">soit</w> <w n="8.7">mis</w>.</l>
			</lg>
			<lg n="3">
			<head type="speaker">DOROTHÉE et PINGOT.</head>
				<l n="9" num="3.1"><w n="9.1">Ah</w> ! <w n="9.2">quelle</w> <w n="9.3">horreur</w> ! <w n="9.4">est</w>-<w n="9.5">il</w> <w n="9.6">permis</w> ? </l>
				<l n="10" num="3.2"><w n="10.1">Ah</w> ! <w n="10.2">c</w>'<w n="10.3">est</w> <w n="10.4">une</w> <w n="10.5">infamie</w> ! </l>
				<l n="11" num="3.3"><w n="11.1">C</w>'<w n="11.2">est</w> <w n="11.3">ainsi</w> <w n="11.4">qu</w>'<w n="11.5">il</w> <w n="11.6">oublie</w></l>
				<l n="12" num="3.4"><w n="12.1">Tous</w> <w n="12.2">ses</w> <w n="12.3">anciens</w> <w n="12.4">amis</w>.</l>
			</lg> 
		</div>
		<div type="section" n="2">
			<lg n="1">
			<head type="speaker">PINGOT, se démenant au milieu des domestiques.</head>
				<l n="13" num="1.1"><w n="13.1">Souviens</w>-<w n="13.2">toi</w> <w n="13.3">d</w>'<w n="13.4">ça</w>, <w n="13.5">j</w>'<w n="13.6">le</w> <w n="13.7">rendrai</w> <w n="13.8">la</w> <w n="13.9">peau</w> <w n="13.10">jaune</w> ; </l>
				<l n="14" num="1.2"><w n="14.1">J</w>'<w n="14.2">ai</w>, <w n="14.3">pour</w> <w n="14.4">la</w> <w n="14.5">teindre</w>, <w n="14.6">un</w> <w n="14.7">parʼment</w> <w n="14.8">de</w> <w n="14.9">fagot</w>.</l>
			</lg> 
			<lg n="2">
			<head type="speaker">DOROTHÉE, se démenant de même.</head>
				<l n="15" num="2.1"><w n="15.1">Et</w> <w n="15.2">moi</w>, <w n="15.3">d</w>'<w n="15.4">ta</w> <w n="15.5">peau</w>, <w n="15.6">moi</w>, <w n="15.7">je</w> <w n="15.8">retiens</w> <w n="15.9">une</w> <w n="15.10">aune</w>, </l>
				<l n="16" num="2.2"><w n="16.1">Afin</w> <w n="16.2">d</w>'<w n="16.3">en</w> <w n="16.4">fair</w>' <w n="16.5">des</w> <w n="16.6">bretellʼs</w> <w n="16.7">à</w> <w n="16.8">Pingot</w>.</l>
			</lg>
		</div>
</div></body></text></TEI>