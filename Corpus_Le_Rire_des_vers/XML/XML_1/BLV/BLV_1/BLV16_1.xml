<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BANDEAU</title>
				<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="BLL" sort="1">
					<name>
						<forename>Jean-Nicolas</forename>
						<surname>BOUILLY</surname>
					</name>
					<date from="1763" to="1842">1763-1842</date>
				</author>
				<author key="VAN" sort="2">
					<name>
						<forename>Émile</forename>
						<surname>VANDERBURCH</surname>
						<addname type="other">Émile VANDERBURCH</addname>
						<addname type="other">Émile VANDER-BURCH</addname>
						<addname type="other">Émile VANDERBURCK</addname>
						<addname type="other">Émile VAN DER BURCK</addname>
					</name>
					<date from="1794" to="1862">1794-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>168 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE BANDEAU</title>
						<author>BOUILLY ET EM. VANDERBURCH</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GALLICA</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9782259n.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La Bibliothèque nationale de France</repository>
								<idno type="URL">https ://catalogue.bnf.fr/ark :/12148/cb31531335q</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">21 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BLV16">
	<head type="tune">AIR Vaudeville du Baiser au porteur.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Plaire</w> <w n="1.2">au</w> <w n="1.3">public</w> <w n="1.4">sans</w> <w n="1.5">doute</w> <w n="1.6">est</w> <w n="1.7">un</w> <w n="1.8">beau</w> <w n="1.9">rêve</w>,</l>
		<l n="2" num="1.2"><w n="2.1">Plus</w> <w n="2.2">d</w>'<w n="2.3">un</w> <w n="2.4">auteur</w> <w n="2.5">par</w> <w n="2.6">ce</w> <w n="2.7">songe</w> <w n="2.8">est</w> <w n="2.9">leurré</w> ;</l>
		<l n="3" num="1.3"><w n="3.1">Mais</w> <w n="3.2">dès</w> <w n="3.3">que</w> <w n="3.4">le</w> <w n="3.5">rideau</w> <w n="3.6">se</w> <w n="3.7">lève</w>,</l>
		<l n="4" num="1.4"><w n="4.1">Pour</w> <w n="4.2">lui</w> <w n="4.3">le</w> <w n="4.4">voile</w> <w n="4.5">est</w> <w n="4.6">déchiré</w> ;</l>
		<l n="5" num="1.5"><w n="5.1">Il</w> <w n="5.2">tremble</w>, <w n="5.3">il</w> <w n="5.4">craint</w>, <w n="5.5">il</w> <w n="5.6">est</w> <w n="5.7">désespéré</w>.</l>
		<l n="6" num="1.6"><w n="6.1">Le</w> <w n="6.2">nôtre</w>, <w n="6.3">sans</w> <w n="6.4">perdre</w> <w n="6.5">courage</w>,</l>
		<l n="7" num="1.7"><w n="7.1">Attend</w> <w n="7.2">ce</w> <w n="7.3">soir</w>, <w n="7.4">d</w>'<w n="7.5">un</w> <w n="7.6">public</w> <w n="7.7">généreux</w>,</l>
		<l n="8" num="1.8"><w n="8.1">Que</w> <w n="8.2">pour</w> <w n="8.3">juger</w> <w n="8.4">ce</w> <w n="8.5">faible</w> <w n="8.6">ouvrage</w>,</l>
		<l n="9" num="1.9"><w n="9.1">Il</w> <w n="9.2">gardera</w> <w n="9.3">le</w> <w n="9.4">bandeau</w> <w n="9.5">sur</w> <w n="9.6">les</w> <w n="9.7">yeux</w>.</l>
	</lg> 
	<lg n="2">
	<head type="speaker">ENSEMBLE.</head>
		<l n="10" num="2.1"><w n="10.1">Pour</w> <w n="10.2">juger</w> <w n="10.3">ce</w> <w n="10.4">modeste</w> <w n="10.5">ouvrage</w>,</l>
		<l n="11" num="2.2"><w n="11.1">Restez</w>, <w n="11.2">messieurs</w>, <w n="11.3">un</w> <w n="11.4">bandeau</w> <w n="11.5">sur</w> <w n="11.6">les</w> <w n="11.7">yeux</w>.</l>
	</lg>
</div></body></text></TEI>