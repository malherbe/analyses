<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CONSEIL DE RÉVISION, OU LES MAUVAIS NUMÉROS</title>
				<title type="sub">TABLEAU-VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="BRU" sort="1">
					<name>
						<forename>Léon-Lévy</forename>
						<surname>BRUNSWICK</surname>
						<addname type="other">Léon LHÉRIE</addname>
					</name>
					<date from="1805" to="1859">1805-1859</date>
				</author>
				<author key="THO" sort="2">
				  <name>
					<forename>Mathieu Barthélemy</forename>
					<surname>THOUIN</surname>
					<addname type="other">Barthélemy</addname>
				  </name>
				  <date from="1804" to="18..">1804-18..</date>
				</author>
				<author key="LVY" sort="3">
				  <name>
					<forename>Léon-Victor</forename>
					<surname>LÉVY</surname>
					<addname type="pen_name">Victor LHÉRIE</addname>
					<addname type="other">Lhérie</addname>
				  </name>
				  <date from="1808" to="1845">1808-1845</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>212 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BTL_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CONSEIL DE RÉVISION, OU LES MAUVAIS NUMÉROS</title>
						<author>BRUNSWICK, BARTHÉLEMY ET LHÉRIE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=NYPL :33433075800833</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>New York Public Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/nyp.33433075800833</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">4 AOÛT 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BTL19">
	<head type="tune">AIR de Partie et Revanche.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Lorsqu</w>'<w n="1.2">il</w> <w n="1.3">atteint</w> <w n="1.4">sa</w> <w n="1.5">vingt</w>-<w n="1.6">unième</w> <w n="1.7">année</w>,</l>
		<l n="2" num="1.2"><w n="2.1">Plus</w> <w n="2.2">d</w>'<w n="2.3">un</w> <w n="2.4">conscrit</w> <w n="2.5">qu</w>'<w n="2.6">on</w> <w n="2.7">appell</w>' <w n="2.8">sous</w> <w n="2.9">l</w>' <w n="2.10">drapeau</w></l>
		<l n="3" num="1.3"><w n="3.1">Espère</w> <w n="3.2">avoir</w> <w n="3.3">un</w>' <w n="3.4">chance</w> <w n="3.5">fortunée</w></l>
		<l n="4" num="1.4"><w n="4.1">Qui</w> <w n="4.2">l</w>'<w n="4.3">fass</w>' <w n="4.4">tomber</w> <w n="4.5">sur</w> <w n="4.6">un</w> <w n="4.7">bon</w> <w n="4.8">numéro</w>,</l>
		<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">qui</w> <w n="5.3">l</w>'<w n="5.4">exempt</w>' <w n="5.5">de</w> <w n="5.6">porter</w> <w n="5.7">le</w> <w n="5.8">schako</w>.</l>
		<l n="6" num="1.6"><w n="6.1">Mais</w>, <w n="6.2">sur</w> <w n="6.3">la</w> <w n="6.4">frontière</w>, <w n="6.5">je</w> <w n="6.6">gage</w>,</l>
		<l n="7" num="1.7"><w n="7.1">Si</w> <w n="7.2">l</w>'<w n="7.3">étranger</w> <w n="7.4">revenait</w> <w n="7.5">de</w> <w n="7.6">nouveau</w>,</l>
		<l n="8" num="1.8"><w n="8.1">Tous</w> <w n="8.2">nos</w> <w n="8.3">jeunʼs</w> <w n="8.4">gens</w>, <w n="8.5">quand</w> <w n="8.6">viendrait</w> <w n="8.7">le</w> <w n="8.8">tirage</w>,</l>
		<l n="9" num="1.9"><w n="9.1">Voudraient</w> <w n="9.2">avoir</w> <w n="9.3">un</w> <w n="9.4">mauvais</w> <w n="9.5">numéro</w>.</l>
	</lg>
	<lg n="2">
	<head type="speaker">MOUFFLET.</head>
		<l n="10" num="2.1"><w n="10.1">C</w>'<w n="10.2">mari</w> <w n="10.3">court</w> <w n="10.4">vit</w>', <w n="10.5">chez</w> <w n="10.6">le</w> <w n="10.7">commissair</w>',</l>
		<l n="11" num="2.2"><w n="11.1">Porter</w> <w n="11.2">sa</w> <w n="11.3">plainte</w>… <w n="11.4">ah</w> ! <w n="11.5">craignez</w> <w n="11.6">son</w> <w n="11.7">destin</w> ! …</l>
		<l n="12" num="2.3"><w n="12.1">Il</w> <w n="12.2">a</w> <w n="12.3">surpris</w> <w n="12.4">la</w> <w n="12.5">semain</w>' <w n="12.6">dernière</w></l>
		<l n="13" num="2.4"><w n="13.1">Sa</w> <w n="13.2">femme</w> <w n="13.3">avec</w> <w n="13.4">son</w> <w n="13.5">grand</w> <w n="13.6">cousin</w>,</l>
		<l n="14" num="2.5"><w n="14.1">En</w> <w n="14.2">tête</w>-<w n="14.3">à</w>-<w n="14.4">tête</w>, <w n="14.5">en</w> <w n="14.6">modeste</w> <w n="14.7">sapin</w>.</l>
		<l n="15" num="2.6"><w n="15.1">On</w> <w n="15.2">lui</w> <w n="15.3">demand</w>' <w n="15.4">l</w>'<w n="15.5">adress</w>' <w n="15.6">de</w> <w n="15.7">la</w> <w n="15.8">voiture</w>,</l>
		<l n="16" num="2.7"><w n="16.1">Il</w> <w n="16.2">n</w>' <w n="16.3">la</w> <w n="16.4">sait</w> <w n="16.5">pas</w>… <w n="16.6">mais</w>, <w n="16.7">dit</w>-<w n="16.8">il</w>, <w n="16.9">c</w>' <w n="16.10">quiproquo</w></l>
		<l n="17" num="2.8"><w n="17.1">S</w>'<w n="17.2">éclaircira</w>… <w n="17.3">car</w> <w n="17.4">un</w>' <w n="17.5">autr</w>' <w n="17.6">fois</w>, <w n="17.7">je</w> <w n="17.8">le</w> <w n="17.9">jure</w>,</l>
		<l n="18" num="2.9"><w n="18.1">J</w>'<w n="18.2">n</w>'<w n="18.3">oublirai</w> <w n="18.4">pas</w> <w n="18.5">de</w> <w n="18.6">prendr</w>' <w n="18.7">l</w>' <w n="18.8">numéro</w>.</l> 
	</lg>
	<lg n="3">
	<head type="speaker">CHOPIN.</head>
		<l n="19" num="3.1"><w n="19.1">J</w>'<w n="19.2">ai</w> <w n="19.3">feint</w> <w n="19.4">d</w>'<w n="19.5">êtr</w>' <w n="19.6">myop</w>', <w n="19.7">ma</w> <w n="19.8">ruse</w> <w n="19.9">est</w> <w n="19.10">excusable</w>,</l>
		<l n="20" num="3.2"><w n="20.1">Pour</w> <w n="20.2">éviter</w> <w n="20.3">d</w>'<w n="20.4">être</w> <w n="20.5">soldat</w> ;</l>
		<l n="21" num="3.3"><w n="21.1">Porter</w> <w n="21.2">lunettʼs</w> <w n="21.3">vous</w> <w n="21.4">donne</w> <w n="21.5">un</w> <w n="21.6">air</w> <w n="21.7">capable</w>,</l>
		<l n="22" num="3.4"><w n="22.1">Voyez</w> <w n="22.2">plutôt</w> <w n="22.3">nos</w> <w n="22.4">hommes</w> <w n="22.5">d</w>'<w n="22.6">état</w> !</l>
		<l n="23" num="3.5"><w n="23.1">Combien</w> <w n="23.2">de</w> <w n="23.3">myopʼs</w> <w n="23.4">hommʼs</w> <w n="23.5">d</w>'<w n="23.6">état</w> !</l>
		<l n="24" num="3.6"><w n="24.1">Ils</w> <w n="24.2">cherchʼnt</w> <w n="24.3">en</w> <w n="24.4">vain</w> <w n="24.5">d</w>' <w n="24.6">la</w> <w n="24.7">politique</w></l>
		<l n="25" num="3.7"><w n="25.1">À</w> <w n="25.2">démêler</w> <w n="25.3">le</w> <w n="25.4">long</w> <w n="25.5">imbroglio</w>,</l>
		<l n="26" num="3.8"><w n="26.1">On</w> <w n="26.2">us</w>', <w n="26.3">poureux</w>, <w n="26.4">toutʼs</w> <w n="26.5">les</w> <w n="26.6">rʼssourcʼs</w> <w n="26.7">de</w> <w n="26.8">l</w>'<w n="26.9">optique</w> :</l>
		<l n="27" num="3.9"><w n="27.1">On</w> <w n="27.2">n</w>' <w n="27.3">peut</w> <w n="27.4">jamais</w> <w n="27.5">trouver</w> <w n="27.6">leur</w> <w n="27.7">numéro</w>.</l> 
	</lg>
	<lg n="4">
	<head type="speaker">BLOQUET.</head>
		<l n="28" num="4.1"><w n="28.1">Nous</w> <w n="28.2">avons</w> <w n="28.3">vu</w> <w n="28.4">tomber</w> <w n="28.5">dans</w> <w n="28.6">la</w> <w n="28.7">disgrâce</w></l>
		<l n="29" num="4.2"><w n="29.1">Des</w> <w n="29.2">rois</w> <w n="29.3">et</w> <w n="29.4">des</w> <w n="29.5">gouvernemens</w> ;</l>
		<l n="30" num="4.3"><w n="30.1">Par</w> <w n="30.2">l</w>' <w n="30.3">temps</w> <w n="30.4">qui</w> <w n="30.5">court</w>, <w n="30.6">les</w> <w n="30.7">gens</w> <w n="30.8">en</w> <w n="30.9">place</w></l>
		<l n="31" num="4.4"><w n="31.1">Sont</w> <w n="31.2">sujets</w> <w n="31.3">aux</w> <w n="31.4">changemens</w>,</l>
		<l n="32" num="4.5"><w n="32.1">Il</w> <w n="32.2">faut</w> <w n="32.3">s</w>' <w n="32.4">soumettre</w> <w n="32.5">aux</w> <w n="32.6">événʼmens</w>.</l>
		<l n="33" num="4.6"><w n="33.1">Quelquʼs</w>-<w n="33.2">uns</w> <w n="33.3">pourtant</w>, <w n="33.4">toute</w> <w n="33.5">leur</w> <w n="33.6">vie</w>,</l>
		<l n="34" num="4.7"><w n="34.1">Gardent</w> <w n="34.2">leur</w> <w n="34.3">place</w> <w n="34.4">et</w> <w n="34.5">restent</w> <w n="34.6">en</w> <w n="34.7">repos</w>,</l>
		<l n="35" num="4.8"><w n="35.1">Dʼpuis</w> <w n="35.2">soixante</w> <w n="35.3">ans</w>, <w n="35.4">à</w> <w n="35.5">la</w> <w n="35.6">lotʼrie</w>,</l>
		<l n="36" num="4.9"><w n="36.1">C</w>'<w n="36.2">est</w> <w n="36.3">l</w>'<w n="36.4">même</w> <w n="36.5">enfant</w> <w n="36.6">qui</w> <w n="36.7">tir</w>' <w n="36.8">les</w> <w n="36.9">numéros</w>.</l> 
	</lg> 
	<lg n="5">
	<head type="speaker">FRANCIS.</head>
		<l n="37" num="5.1"><w n="37.1">À</w> <w n="37.2">notr</w>' <w n="37.3">théâtre</w> <w n="37.4">afin</w> <w n="37.5">que</w> <w n="37.6">chacun</w> <w n="37.7">vienne</w>,</l>
		<l n="38" num="5.2"><w n="38.1">Vʼlà</w> <w n="38.2">son</w> <w n="38.3">adress</w>' <w n="38.4">que</w> <w n="38.5">j</w>' <w n="38.6">vous</w> <w n="38.7">donn</w>' <w n="38.8">tout</w> <w n="38.9">au</w> <w n="38.10">long</w> ;</l>
		<l n="39" num="5.3"><w n="39.1">Vous</w> <w n="39.2">vʼnez</w> <w n="39.3">d</w>'<w n="39.4">la</w> <w n="39.5">Bours</w>', <w n="39.6">prenez</w> <w n="39.7">la</w> <w n="39.8">ru</w>' <w n="39.9">Vivienne</w>,</l>
		<l n="40" num="5.4"><w n="40.1">Vous</w> <w n="40.2">arrivez</w> <w n="40.3">en</w> <w n="40.4">face</w> <w n="40.5">du</w> <w n="40.6">perron</w>,</l>
		<l n="41" num="5.5"><w n="41.1">Vous</w> <w n="41.2">descendez</w> <w n="41.3">bien</w> <w n="41.4">vite</w> <w n="41.5">le</w> <w n="41.6">perron</w>.</l>
		<l n="42" num="5.6"><w n="42.1">Vous</w> <w n="42.2">êtes</w> <w n="42.3">dans</w> <w n="42.4">la</w> <w n="42.5">galerie</w> !</l>
		<l n="43" num="5.7"><w n="43.1">Tournez</w> <w n="43.2">à</w> <w n="43.3">droite</w>, <w n="43.4">au</w> <w n="43.5">bout</w> <w n="43.6">est</w> <w n="43.7">le</w> <w n="43.8">bureau</w> ;</l>
		<l n="44" num="5.8"><w n="44.1">Cette</w> <w n="44.2">adressʼ</w>-<w n="44.3">là</w>, <w n="44.4">vous</w> <w n="44.5">suffit</w>, <w n="44.6">je</w> <w n="44.7">l</w>' <w n="44.8">parie</w>,</l>
		<l n="45" num="5.9"><w n="45.1">J</w>' <w n="45.2">n</w>'<w n="45.3">ai</w> <w n="45.4">pas</w> <w n="45.5">besoin</w> <w n="45.6">d</w>' <w n="45.7">vous</w> <w n="45.8">dir</w> <w n="45.9">le</w> <w n="45.10">numéro</w>.</l>
	</lg>
</div></body></text></TEI>