<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CONSEIL DE RÉVISION, OU LES MAUVAIS NUMÉROS</title>
				<title type="sub">TABLEAU-VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="BRU" sort="1">
					<name>
						<forename>Léon-Lévy</forename>
						<surname>BRUNSWICK</surname>
						<addname type="other">Léon LHÉRIE</addname>
					</name>
					<date from="1805" to="1859">1805-1859</date>
				</author>
				<author key="THO" sort="2">
				  <name>
					<forename>Mathieu Barthélemy</forename>
					<surname>THOUIN</surname>
					<addname type="other">Barthélemy</addname>
				  </name>
				  <date from="1804" to="18..">1804-18..</date>
				</author>
				<author key="LVY" sort="3">
				  <name>
					<forename>Léon-Victor</forename>
					<surname>LÉVY</surname>
					<addname type="pen_name">Victor LHÉRIE</addname>
					<addname type="other">Lhérie</addname>
				  </name>
				  <date from="1808" to="1845">1808-1845</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>212 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BTL_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CONSEIL DE RÉVISION, OU LES MAUVAIS NUMÉROS</title>
						<author>BRUNSWICK, BARTHÉLEMY ET LHÉRIE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=NYPL :33433075800833</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>New York Public Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/nyp.33433075800833</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">4 AOÛT 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BTL3">
	<head type="tune">AIR : À soixante ans.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">de</w> <w n="1.3">la</w> <w n="1.4">foule</w> <w n="1.5">un</w> <w n="1.6">jeune</w> <w n="1.7">homme</w> <w n="1.8">s</w>'<w n="1.9">élance</w>,</l>
		<l n="2" num="1.2"><w n="2.1">Il</w> <w n="2.2">me</w> <w n="2.3">saisit</w>, <w n="2.4">m</w>'<w n="2.5">arrache</w> <w n="2.6">de</w> <w n="2.7">leurs</w> <w n="2.8">bras</w> ;</l>
		<l n="3" num="1.3"><w n="3.1">Puis</w> <w n="3.2">il</w> <w n="3.3">leur</w> <w n="3.4">dit</w> : <w n="3.5">Quelle</w> <w n="3.6">lâche</w> <w n="3.7">vengeance</w> !</l>
		<l n="4" num="1.4"><w n="4.1">Ah</w> ! <w n="4.2">combattez</w>, <w n="4.3">mais</w> <w n="4.4">n</w>'<w n="4.5">assassinez</w> <w n="4.6">pas</w></l>
		<l n="5" num="1.5"><w n="5.1">C</w>'<w n="5.2">est</w> <w n="5.3">un</w> <w n="5.4">blessé</w>, <w n="5.5">ne</w> <w n="5.6">l</w>'<w n="5.7">assassinez</w> <w n="5.8">pas</w> !</l>
		<l n="6" num="1.6"><w n="6.1">Des</w> <w n="6.2">étrangers</w> <w n="6.3">qu</w>'<w n="6.4">un</w> <w n="6.5">même</w> <w n="6.6">but</w> <w n="6.7">rallie</w></l>
		<l n="7" num="1.7"><w n="7.1">Voient</w> <w n="7.2">nos</w> <w n="7.3">discordʼs</w> <w n="7.4">avec</w> <w n="7.5">plaisir</w> ;</l>
		<l n="8" num="1.8"><w n="8.1">Déjà</w> <w n="8.2">chez</w> <w n="8.3">nous</w> <w n="8.4">ils</w> <w n="8.5">songʼnt</w> <w n="8.6">à</w> <w n="8.7">revenir</w>,</l>
		<l n="9" num="1.9"><w n="9.1">Ah</w> ! <w n="9.2">conservez</w> <w n="9.3">à</w> <w n="9.4">notr</w>' <w n="9.5">chère</w> <w n="9.6">patrie</w></l>
		<l n="10" num="1.10"><w n="10.1">Deux</w> <w n="10.2">bras</w> <w n="10.3">qui</w> <w n="10.4">peuvʼnt</w> <w n="10.5">encore</w> <w n="10.6">la</w> <w n="10.7">servir</w>.</l>
	</lg>
</div></body></text></TEI>