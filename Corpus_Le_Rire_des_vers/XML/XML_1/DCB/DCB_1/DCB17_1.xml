<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB17">
	<head type="tune">AIR d'une mazureck polonaise, arrangée par M. Ch. Tolbecque.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">tu</w> <w n="1.3">sʼras</w> <w n="1.4">ma</w> <w n="1.5">femme</w>,</l>
		<l n="2" num="1.2"><w n="2.1">Que</w> <w n="2.2">nous</w> <w n="2.3">serons</w> <w n="2.4">bien</w> !</l>
		<l n="3" num="1.3"><w n="3.1">Compte</w> <w n="3.2">sur</w> <w n="3.3">ma</w> <w n="3.4">flamme</w>,</l>
		<l n="4" num="1.4"><w n="4.1">Tu</w> <w n="4.2">n</w>'<w n="4.3">manquʼras</w> <w n="4.4">de</w> <w n="4.5">rien</w>.</l>
		<l n="5" num="1.5"><w n="5.1">Dans</w> <w n="5.2">ce</w> <w n="5.3">doux</w> <w n="5.4">lien</w></l>
		<l n="6" num="1.6"><w n="6.1">Point</w> <w n="6.2">de</w> <w n="6.3">soucis</w> <w n="6.4">ni</w> <w n="6.5">d</w>'<w n="6.6">épine</w> <w n="6.7">maudite</w>,</l>
		<l n="7" num="1.7"><w n="7.1">Point</w> <w n="7.2">de</w> <w n="7.3">fleur</w> <w n="7.4">hétéroclite</w></l>
		<l n="8" num="1.8"><w n="8.1">Avec</w> <w n="8.2">Marguerite</w>.</l>
	</lg>
	<p>
		(Ils dansent tous deux sur la ritournelle un pas comique, 
		avec gestes analogues, en s'envoyant des baisers.)
	</p>
	<lg n="2">
	<head type="speaker">TÉLÉMAQUE.</head>
	<head type="main">DEUXIÈME COUPLET.</head>
		<l n="9" num="2.1"><w n="9.1">Dans</w> <w n="9.2">notre</w> <w n="9.3">ménage</w></l>
		<l n="10" num="2.2"><w n="10.1">Un</w>'<w n="10.2">fois</w> <w n="10.3">établis</w>,</l>
		<l n="11" num="2.3"><w n="11.1">Je</w> <w n="11.2">veux</w> <w n="11.3">d</w>'<w n="11.4">mon</w> <w n="11.5">ouvrage</w></l>
		<l n="12" num="2.4"><w n="12.1">Que</w> <w n="12.2">l</w>'<w n="12.3">on</w> <w n="12.4">soit</w> <w n="12.5">surpris</w>.</l>
		<l n="13" num="2.5"><w n="13.1">Enfans</w> <w n="13.2">bien</w> <w n="13.3">gentils</w>,</l>
		<l n="14" num="2.6"><w n="14.1">Souliers</w> <w n="14.2">jolis</w>,</l>
		<l n="15" num="2.7"><w n="15.1">Comme</w> <w n="15.2">ça</w> <w n="15.3">s</w>'<w n="15.4">fera</w> <w n="15.5">vite</w></l>
		<l n="16" num="2.8"><w n="16.1">En</w> <w n="16.2">travaillant</w>, <w n="16.3">ma</w> <w n="16.4">petite</w>,</l>
		<l n="17" num="2.9"><w n="17.1">Avec</w> <w n="17.2">Marguerite</w>.</l>
	</lg> 
	<p>
		ENSEMBLE.<lb></lb>
		Enfans bien gentils, etc.<lb></lb> 
	</p>
	<lg n="3">
	<head type="speaker">MARGUERITE</head>
		<l n="18" num="3.1"><w n="18.1">Enfans</w> <w n="18.2">bien</w> <w n="18.3">gentils</w>,</l>
		<l n="19" num="3.2"><w n="19.1">Souliers</w> <w n="19.2">jolis</w>,</l>
		<l n="20" num="3.3"><w n="20.1">Comme</w> <w n="20.2">ça</w> <w n="20.3">s</w>'<w n="20.4">fera</w> <w n="20.5">vite</w> !</l>
		<l n="21" num="3.4"><w n="21.1">Faudrait</w> <w n="21.2">qu</w>'<w n="21.3">il</w> <w n="21.4">fût</w> <w n="21.5">tout</w> <w n="21.6">de</w> <w n="21.7">suite</w></l>
		<l n="22" num="3.5"><w n="22.1">Avec</w> <w n="22.2">Marguerite</w>.</l>
	</lg>
</div></body></text></TEI>