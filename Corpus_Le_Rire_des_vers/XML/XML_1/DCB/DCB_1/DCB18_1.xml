<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB18">
	<head type="tune">AIR : Tenez, moi, je suis un bonhomme.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">À</w> <w n="1.2">l</w>'<w n="1.3">ouvrage</w> <w n="1.4">il</w> <w n="1.5">est</w> <w n="1.6">intrépide</w>,</l>
		<l n="2" num="1.2"><w n="2.1">Son</w> <w n="2.2">coup</w> <w n="2.3">d</w>'<w n="2.4">œil</w> <w n="2.5">ne</w> <w n="2.6">saurait</w> <w n="2.7">mentir</w> ;</l>
		<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">ne</w> <w n="3.3">donne</w> <w n="3.4">que</w> <w n="3.5">du</w> <w n="3.6">solide</w> :</l>
		<l n="4" num="1.4"><w n="4.1">Tout</w> <w n="4.2">ce</w> <w n="4.3">qui</w> <w n="4.4">vient</w> <w n="4.5">d</w>'<w n="4.6">lui</w> <w n="4.7">fait</w> <w n="4.8">plaisir</w>.</l>
		<l n="5" num="1.5"><w n="5.1">En</w> <w n="5.2">tout</w> <w n="5.3">par</w> <w n="5.4">son</w> <w n="5.5">mérite</w> <w n="5.6">il</w> <w n="5.7">brille</w> ;</l>
		<l n="6" num="1.6"><w n="6.1">De</w> <w n="6.2">lui</w> <w n="6.3">l</w>'<w n="6.4">on</w> <w n="6.5">n</w>'<w n="6.6">doit</w> <w n="6.7">se</w> <w n="6.8">plaindre</w> <w n="6.9">en</w> <w n="6.10">rien</w> ;</l>
		<l n="7" num="1.7"><w n="7.1">Mêm</w>' <w n="7.2">quand</w> <w n="7.3">il</w> <w n="7.4">embrasse</w> <w n="7.5">un</w>' <w n="7.6">jeun</w>' <w n="7.7">fille</w>,</l>
		<l n="8" num="1.8"><w n="8.1">N</w>'<w n="8.2">est</w>-<w n="8.3">ce</w> <w n="8.4">pas</w> <w n="8.5">qu</w>'<w n="8.6">il</w> <w n="8.7">travaille</w> <w n="8.8">bien</w> ?</l>
	</lg> 
</div></body></text></TEI>