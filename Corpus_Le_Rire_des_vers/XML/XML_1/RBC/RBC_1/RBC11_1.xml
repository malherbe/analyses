<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE III.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC11">
				<head type="main">LE RÉMOULEUR</head>
				<head type="tune">AIR de Turenne.</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">De</w> <w n="1.2">nos</w> <w n="1.3">boulʼvards</w> <w n="1.4">on</w> <w n="1.5">rʼsablʼ</w> <w n="1.6">les</w> <w n="1.7">avenues</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">On</w> <w n="2.2">a</w> <w n="2.3">guéri</w>, <w n="2.4">grâce</w> <w n="2.5">à</w> <w n="2.6">Dieu</w>, <w n="2.7">nos</w> <w n="2.8">blessés</w> ;</l>
					<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1">On</w> <w n="3.2">a</w> <w n="3.3">déblayé</w> <w n="3.4">toutʼs</w> <w n="3.5">les</w> <w n="3.6">rues</w> ;</l>
					<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">réverbèrʼs</w> <w n="4.3">ont</w> <w n="4.4">été</w> <w n="4.5">replacés</w>,</l>
					<l n="5" num="1.5"><w n="5.1">On</w> <w n="5.2">a</w> <w n="5.3">remis</w> <w n="5.4">tous</w> <w n="5.5">les</w> <w n="5.6">carreaux</w> <w n="5.7">cassés</w> ;</l>
					<l n="6" num="1.6"><space unit="char" quantity="4"></space><w n="6.1">On</w> <w n="6.2">a</w> <w n="6.3">repeint</w> <w n="6.4">toutʼs</w> <w n="6.5">les</w> <w n="6.6">boutiques</w>,</l>
					<l n="7" num="1.7"><w n="7.1">On</w> <w n="7.2">a</w> <w n="7.3">rʼplanté</w> <w n="7.4">plus</w> <w n="7.5">d</w>'<w n="7.6">un</w> <w n="7.7">arbre</w> <w n="7.8">abattu</w> ;</l>
					<l n="8" num="1.8"><space unit="char" quantity="4"></space><w n="8.1">Mais</w> <w n="8.2">dʼpuis</w> <w n="8.3">juillet</w> <w n="8.4">on</w> <w n="8.5">n</w>'<w n="8.6">a</w> <w n="8.7">pas</w> <w n="8.8">pu</w></l>
					<l n="9" num="1.9"><space unit="char" quantity="4"></space><w n="9.1">Rafistoler</w> <w n="9.2">les</w> <w n="9.3">romantiques</w>.</l>
				</lg>
<p>
LA LIBERTÉ.<lb></lb>
Allons, messieurs les auteurs, si vous ne faites pas à présent de
bonnes pièces, ce sera votre faute... Les sujets ne vous manqueront
plus.... et comme désormais c'est sous mon influence que vous allez
travailler, je vous promets de vous faire part de tous les travers, de
tous les ridicules qui s'offriront à moi... de même que des vertus
que je rencontrerai par hasard.<lb></lb>

LE RÉMOULEUR,<lb></lb>

Madame la Liberté, maintenant que je suis mon maître, voulez-vous
me prendre à votre service ?<lb></lb>

LA LIBERTÉ<lb></lb>

Volontiers, mon garçon !<lb></lb>
(On reprend en chœur :)
</p>
				<lg n="2">
					<l n="10" num="2.1"><space unit="char" quantity="4"></space><w n="10.1">La</w> <w n="10.2">Liberté</w> ! <w n="10.3">la</w> <w n="10.4">Liberté</w> !</l>
					<l n="11" num="2.2"><space unit="char" quantity="4"></space><w n="11.1">Dont</w> <w n="11.2">les</w> <w n="11.3">Français</w> <w n="11.4">sont</w> <w n="11.5">idolâtres</w>,</l>
					<l n="12" num="2.3"><space unit="char" quantity="4"></space><w n="12.1">Vient</w> <w n="12.2">d</w>'<w n="12.3">affranchir</w> <w n="12.4">tous</w> <w n="12.5">les</w> <w n="12.6">théâtres</w>,</l>
					<l n="13" num="2.4"><space unit="char" quantity="4"></space><w n="13.1">Plus</w> <w n="13.2">d</w>'<w n="13.3">entraves</w> <w n="13.4">pour</w> <w n="13.5">la</w> <w n="13.6">gaîté</w> :</l>
					<l n="14" num="2.5"><space unit="char" quantity="10"></space><w n="14.1">Vive</w> <w n="14.2">la</w> <w n="14.3">gaîté</w> ! <del type="repetition" reason="analysis" hand="LG">(bis.)</del></l>
					<l n="15" num="2.6"><space unit="char" quantity="10"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="15.1">Vive</w> <w n="15.2">la</w> <w n="15.3">gaîté</w> !</add></subst></l>
				</lg>
			</div></body></text></TEI>