<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">À PROPOS PATRIOTIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="VIL" sort="1">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="MAS" sort="2">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>273 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">VEM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>À propos patriotique</title>
						<author>VILLENEUVE ET MASSON</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=0FA6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>À propos patriotique</title>
								<author>VILLENEUVE ET MASSON</author>
								<repository>Bayerische Staatsbibliothek</repository>
								<idno type="URI">https://opacplus.bsb-muenchen.de/title/BV008031366</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_subpart">SCENE IV</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="VEM12">
				<head type="tune">AIR : Le cordon s'il vous plaît.</head>
				<lg n="1">
					<head type="main">Mitouflet.</head>
					<l n="1" num="1.1"><w n="1.1">Que</w> <w n="1.2">c</w>'<w n="1.3">était</w> <w n="1.4">beau</w> <subst reason="analysis" hand="LG" type="repetition"><del>(bis.)</del><add rend="hidden"><w n="1.5">Que</w> <w n="1.6">c</w>'<w n="1.7">était</w> <w n="1.8">beau</w></add></subst> <w n="1.9">sur</w> <w n="1.10">cette</w> <w n="1.11">place</w></l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Pas</w> <w n="2.2">un</w> <w n="2.3">Français</w> <w n="2.4">nʼ</w> <w n="2.5">songeait</w> <w n="2.6">à</w> <w n="2.7">fuir</w>,</l>
					<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Il</w> <w n="3.2">fallait</w> <w n="3.3">voir</w> <w n="3.4">le</w> <w n="3.5">peuple</w> <w n="3.6">en</w> <w n="3.7">masse</w></l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Sans</w> <w n="4.2">être</w> <w n="4.3">armé</w>, <w n="4.4">mais</w> <w n="4.5">sans</w> <w n="4.6">frémir</w>,</l>
					<l n="5" num="1.5"><space unit="char" quantity="4"></space><w n="5.1">Sur</w> <w n="5.2">l</w>'<w n="5.3">ennemi</w> <w n="5.4">s</w>'<w n="5.5">élancer</w> <w n="5.6">et</w> <w n="5.7">courir</w> ;</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">On</w> <w n="6.2">en</w> <w n="6.3">perdait</w> <w n="6.4">lʼ</w> <w n="6.5">manger</w> <w n="6.6">et</w> <w n="6.7">lʼ</w> <w n="6.8">boire</w>,</l>
					<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">On</w> <w n="7.2">n</w>'<w n="7.3">avait</w> <w n="7.4">soif</w> <w n="7.5">que</w> <w n="7.6">dʼ</w> <w n="7.7">la</w> <w n="7.8">victoire</w>,</l>
					<l n="8" num="1.8"><space unit="char" quantity="4"></space><w n="8.1">Sur</w> <w n="8.2">les</w> <w n="8.3">marchʼs</w> <w n="8.4">à</w> <w n="8.5">gʼnoux</w> <w n="8.6">chacun</w> <w n="8.7">s</w>'<w n="8.8">écriait</w> :</l>
					<l n="9" num="1.9"><space unit="char" quantity="12"></space><w n="9.1">Un</w> <w n="9.2">fusil</w>, <w n="9.3">s</w>'<w n="9.4">il</w> <w n="9.5">vous</w> <w n="9.6">plaît</w>, <del reason="analysis" hand="LG" type="repetition">(bis)</del></l>
					<l n="10" num="1.10"><space unit="char" quantity="12"></space><subst reason="analysis" hand="LG" type="repetition"><del> </del><add rend="hidden"><w n="10.1">Un</w> <w n="10.2">fusil</w>, <w n="10.3">s</w>'<w n="10.4">il</w> <w n="10.5">vous</w> <w n="10.6">plaît</w>,</add></subst></l>
					<l n="11" num="1.11"><space unit="char" quantity="12"></space><w n="11.1">Un</w> <w n="11.2">fusil</w>, <w n="11.3">s</w>'<w n="11.4">il</w> <w n="11.5">vous</w> <w n="11.6">plaît</w>,</l>
					<l n="12" num="1.12"><space unit="char" quantity="6"></space><w n="12.1">Un</w> <w n="12.2">fusil</w> <w n="12.3">et</w> <w n="12.4">tout</w> <w n="12.5">lʼ</w> <w n="12.6">mondʼ</w> <w n="12.7">se</w> <w n="12.8">battait</w>,</l>
				</lg>
			</div></body></text></TEI>