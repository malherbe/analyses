<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB51">
	<head type="tune">AIR du Carnaval.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Courons</w> ! <w n="1.2">il</w> <w n="1.3">faut</w> <w n="1.4">que</w> <w n="1.5">la</w> <w n="1.6">comtesse</w> <w n="1.7">apprenne</w></l>
		<l n="2" num="1.2"><w n="2.1">Tout</w> <w n="2.2">ce</w> <w n="2.3">qui</w> <w n="2.4">vient</w> <w n="2.5">ici</w> <w n="2.6">de</w> <w n="2.7">se</w> <w n="2.8">passer</w> ;</l>
		<l n="3" num="1.3"><w n="3.1">On</w> <w n="3.2">la</w> <w n="3.3">menace</w>, <w n="3.4">et</w> <w n="3.5">ma</w> <w n="3.6">cause</w> <w n="3.7">est</w> <w n="3.8">la</w> <w n="3.9">sienne</w>,</l>
		<l n="4" num="1.4"><w n="4.1">Car</w> <w n="4.2">toutes</w> <w n="4.3">deux</w> <w n="4.4">on</w> <w n="4.5">veut</w> <w n="4.6">nous</w> <w n="4.7">remplacer</w>.</l>
		<l n="5" num="1.5"><w n="5.1">Oui</w>, <w n="5.2">nous</w> <w n="5.3">avons</w>, <w n="5.4">en</w> <w n="5.5">cette</w> <w n="5.6">circonstance</w>,</l>
		<l n="6" num="1.6"><w n="6.1">Des</w> <w n="6.2">droits</w> <w n="6.3">égaux</w> <w n="6.4">qu</w>'<w n="6.5">elle</w> <w n="6.6">défendra</w> <w n="6.7">bien</w> ;</l>
		<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">d</w>'<w n="7.3">autant</w> <w n="7.4">mieux</w> <w n="7.5">que</w> <w n="7.6">son</w> <w n="7.7">emploi</w>, <w n="7.8">je</w> <w n="7.9">pense</w>,</l>
		<l n="8" num="1.8"><w n="8.1">Est</w> <w n="8.2">plus</w> <w n="8.3">facile</w> <w n="8.4">à</w> <w n="8.5">doubler</w> <w n="8.6">que</w> <w n="8.7">le</w> <w n="8.8">mien</w>.</l>
	</lg>
</div></body></text></TEI>