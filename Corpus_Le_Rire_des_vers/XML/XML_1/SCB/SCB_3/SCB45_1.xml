<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB45">
	<head type="tune">AIR des Frères de lait.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">De</w> <w n="1.2">tous</w> <w n="1.3">côtés</w> <w n="1.4">les</w> <w n="1.5">peuples</w> <w n="1.6">sont</w> <w n="1.7">en</w> <w n="1.8">armes</w>,</l>
		<l n="2" num="1.2"><w n="2.1">Les</w> <w n="2.2">rois</w> <w n="2.3">partout</w> <w n="2.4">ont</w> <w n="2.5">besoin</w> <w n="2.6">d</w>'<w n="2.7">un</w> <w n="2.8">abri</w>…</l>
		<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">liberté</w> <w n="3.3">qui</w> <w n="3.4">cause</w> <w n="3.5">leurs</w> <w n="3.6">alarmes</w></l>
		<l n="4" num="1.4"><w n="4.1">De</w> <w n="4.2">leur</w> <w n="4.3">couronne</w> <w n="4.4">est</w> <w n="4.5">le</w> <w n="4.6">plus</w> <w n="4.7">ferme</w> <w n="4.8">appui</w>.</l>
		<l n="5" num="1.5"><w n="5.1">Tel</w>, <w n="5.2">en</w> <w n="5.3">voyant</w> <w n="5.4">l</w>'<w n="5.5">aiguille</w> <w n="5.6">tutélaire</w></l>
		<l n="6" num="1.6"><w n="6.1">Par</w> <w n="6.2">qui</w> <w n="6.3">la</w> <w n="6.4">foudre</w> <w n="6.5">est</w> <w n="6.6">facile</w> <w n="6.7">à</w> <w n="6.8">braver</w>,</l>
		<l n="7" num="1.7"><w n="7.1">L</w>'<w n="7.2">ignorant</w> <w n="7.3">craint</w> <w n="7.4">d</w>'<w n="7.5">attirer</w> <w n="7.6">le</w> <w n="7.7">tonnerre</w>,</l>
		<l n="8" num="1.8"><w n="8.1">Le</w> <w n="8.2">sage</w> <w n="8.3">sait</w> <w n="8.4">qu</w>'<w n="8.5">elle</w> <w n="8.6">en</w> <w n="8.7">doit</w> <w n="8.8">préserver</w>.</l>
	</lg>
</div></body></text></TEI>