<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <fileDesc>
   <titleStmt>
	<title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
	<title type="corpus">Le Rire des vers</title>
	<author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
	</author>
	<author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
	</author>
	<editor>Le Rire des vers, Université de Bâle</editor>
	<editor>
	 Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
	 <choice>
	  <abbr>CRISCO, Université de Caen Normandie</abbr>
	  <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	 </choice>
	 (EA 4255)
	</editor>
	<respStmt>
	 <resp>Encodage en XML (CRISCO, université de Caen)</resp>
	 <name id="KL">
	  <forename>Kedi</forename>
	  <surname>LI</surname>
	 </name>
	</respStmt>
	<respStmt>
	 <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
	 <name id="RR">
	  <forename>Richard</forename>
	  <surname>RENAULT</surname>
	 </name>
	</respStmt>
   </titleStmt>
   <extent>140 vers</extent>
   <publicationStmt>
	<publisher>
	 <orgname>
	  <choice>
	   <abbr>CRISCO, Université de Caen Normandie</abbr>
	   <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	  </choice>
	 </orgname>
	 <address>
	  <addrLine>Université de Caen</addrLine>
	  <addrLine>14032 CAEN CEDEX</addrLine>
	  <addrLine>FRANCE</addrLine>
	 </address>
	 <email>crisco.incipit@unicaen.fr</email>
	 <ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
	</publisher>
	<pubPlace>Caen</pubPlace>
	<date when="2022">2022</date>
	<idno type="local">SCB_2</idno>	
	<availability status="free">
		<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
		<p>
			Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
			CC = Licence Creative Commons
			BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
			sur les sources et sur les modifications introduites.
			NC = Pas d’utilisation commerciale.
			SA = Partage dans les mêmes conditions.
		</p>
	</availability>
   </publicationStmt>
   <sourceDesc>
	<biblFull>
	 <titleStmt>
	  <title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	  <author>SCRIBE ET BAYARD</author>
	 </titleStmt>
	 <publicationStmt>
	  <publisher>INTERNET ARCHIVE</publisher>
	  <idno type="URL">https://archive.org/details/camillaoulasoeur00scriuoft</idno>
	 </publicationStmt>
	 <sourceDesc>
	  <biblStruct>
	   <monogr>
		<repository>University of Toronto Libraries</repository>
		<idno type="URL">https://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991106543695706196</idno>
	   </monogr>
	  </biblStruct>                 
	 </sourceDesc>
	</biblFull> 
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <creation>
	<date when="1832">12 DÉCEMBRE 1832</date>
	<placeName>
	 <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
	</placeName>
   </creation>
  </profileDesc>
  <encodingDesc>
   <projectDesc>
	<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
   </projectDesc>
   <samplingDecl>
	<p>Les parties versifiées ont été prioritairement encodées.</p>
   </samplingDecl>
   <editorialDecl>
	<normalization>
	 <p>Les majuscules accentuées ont été restituées.</p>
	 <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
	 <p>La ponctuation a été normalisée.</p> 
	 <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
	 <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
	 <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
	</normalization>
   </editorialDecl>
  </encodingDesc>
 </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB26">
	<head type="tune">AIR : Voici ma tante Lajonchère.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Moi</w>, <w n="1.2">vois</w>-<w n="1.3">tu</w>, <w n="1.4">je</w> <w n="1.5">suis</w> <w n="1.6">peu</w> <w n="1.7">sévère</w>…</l>
		<l n="2" num="1.2"><w n="2.1">Pour</w> <w n="2.2">les</w> <w n="2.3">autres</w> <w n="2.4">moins</w> <w n="2.5">que</w> <w n="2.6">pour</w> <w n="2.7">moi</w> ;</l>
		<l n="3" num="1.3"><w n="3.1">Mais</w> <w n="3.2">elle</w> <w n="3.3">me</w> <w n="3.4">met</w> <w n="3.5">en</w> <w n="3.6">colère</w> !</l>
		<l part="I" n="4" num="1.4"><w n="4.1">Nous</w> <w n="4.2">tromper</w> <w n="4.3">ainsi</w> ! </l>
	</lg>
	<lg n="2">
	<head type="speaker">EDGARD.</head>
		<l part="F" n="4"><w n="4.4">Calme</w>-<w n="4.5">toi</w> !</l>
	</lg>     
	<lg n="3">
	<head type="speaker">LIONEL.</head>
		<l n="5" num="3.1"><w n="5.1">Non</w>, <w n="5.2">en</w> <w n="5.3">ces</w> <w n="5.4">lieux</w> <w n="5.5">je</w> <w n="5.6">vais</w> <w n="5.7">l</w>'<w n="5.8">attendre</w> !</l>
		<l n="6" num="3.2"><w n="6.1">Mes</w> <w n="6.2">sermons</w> <w n="6.3">seront</w> <w n="6.4">entendus</w> ! …</l>
	<p>
		( A part.)
	</p>
	   <l n="7" num="3.3"><w n="7.1">Car</w> <w n="7.2">je</w> <w n="7.3">suis</w> <w n="7.4">en</w> <w n="7.5">fonds</w> <w n="7.6">de</w> <w n="7.7">lui</w> <w n="7.8">rendre</w></l>
	   <l n="8" num="3.4"><w n="8.1">Tous</w> <w n="8.2">ceux</w> <w n="8.3">que</w> <w n="8.4">d</w>'<w n="8.5">elle</w> <w n="8.6">j</w>'<w n="8.7">ai</w> <w n="8.8">reçus</w>.</l>
	</lg>
</div></body></text></TEI>