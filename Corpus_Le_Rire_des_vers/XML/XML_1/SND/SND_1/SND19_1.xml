<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE LIEUTENANT D'ARTILLERIE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="NSL" sort="2">
					<name>
						<forename>Jean-Pierre-Laurent</forename>
						<surname>DENOMBRET</surname>
						<addName type="pen_name">Charles NOMBRET SAINT-LAURENT</addName>
					</name>
					<date from="1791" to="1833">1791-1833</date>
				</author>
					<author key="DUV" sort="3">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>225 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SND_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=NWU:35556007830409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
								<repository>Northwestern university</repository>
								<idno type="URI">https://babel.hathitrust.org/cgi/pt?id=ien.35556007830409</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE VI.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SND19">
				<head type="tune">AIR : J'en guette un petit de mon âge.</head>
				<lg n="1">
					<head type="main">LORIS, lui tendant la main,</head>
					<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">fus</w> <w n="1.3">toujours</w> <w n="1.4">dans</w> <w n="1.5">le</w> <w n="1.6">nord</w> <w n="1.7">de</w> <w n="1.8">la</w> <w n="1.9">France</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">Car</w>, <w n="2.2">à</w> <w n="2.3">Jemmappe</w>, <w n="2.4">en</w> <w n="2.5">chargeant</w> <w n="2.6">les</w> <w n="2.7">houlans</w>,</l>
					<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">Dumouriez</w>, <w n="3.3">officier</w> <w n="3.4">d</w>'<w n="3.5">ordonnance</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Je</w> <w n="4.2">combattais</w> <w n="4.3">près</w> <w n="4.4">d</w>'<w n="4.5">un</w> <w n="4.6">fils</w> <w n="4.7">d</w>'<w n="4.8">Orléans</w>,</l>
				</lg>
				<lg n="2">
					<head type="main">DELAUNAY, avec surprise.</head>
					<l part="I" n="5" num="2.1"><w n="5.1">Un</w> <w n="5.2">d</w>'<w n="5.3">Orléans</w> ! </l>
				</lg>
				<lg n="3">
					<head type="main">LORIS.</head>
					<l part="F" n="5"><w n="5.4">Brave</w>, <w n="5.5">par</w> <w n="5.6">parenthèse</w>,</l>
					<l n="6" num="3.1"><w n="6.1">Par</w> <w n="6.2">maint</w> <w n="6.3">exploit</w> <w n="6.4">son</w> <w n="6.5">bras</w> <w n="6.6">se</w> <w n="6.7">signala</w>.</l>
				</lg>
				<lg n="4">
					<head type="main">BONAPARTE.</head>
					<l n="7" num="4.1"><w n="7.1">Je</w> <w n="7.2">n</w>'<w n="7.3">en</w> <w n="7.4">suis</w> <w n="7.5">pas</w> <w n="7.6">surpris</w> ; <w n="7.7">car</w> <w n="7.8">celui</w>-<w n="7.9">là</w></l>
					<l n="8" num="4.2"><space unit="char" quantity="4"></space><w n="8.1">A</w> <w n="8.2">toujours</w> <w n="8.3">eu</w> <w n="8.4">l</w>'<w n="8.5">âme</w> <w n="8.6">française</w>.</l>
				</lg>
			</div></body></text></TEI>