<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE LIEUTENANT D'ARTILLERIE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="NSL" sort="2">
					<name>
						<forename>Jean-Pierre-Laurent</forename>
						<surname>DENOMBRET</surname>
						<addName type="pen_name">Charles NOMBRET SAINT-LAURENT</addName>
					</name>
					<date from="1791" to="1833">1791-1833</date>
				</author>
					<author key="DUV" sort="3">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>225 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SND_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=NWU:35556007830409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
								<repository>Northwestern university</repository>
								<idno type="URI">https://babel.hathitrust.org/cgi/pt?id=ien.35556007830409</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE XIV.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SND23">
				<head type="tune">Même air que le couplet précédent ; mais beaucoup plus vite.</head>
				<lg n="1">
					<head type="main">VICTORINE.</head>
					<l n="1" num="1.1"><space unit="char" quantity="4"></space><w n="1.1">Son</w> <w n="1.2">crime</w>, <w n="1.3">sans</w> <w n="1.4">doute</w>, <w n="1.5">est</w> <w n="1.6">bien</w> <w n="1.7">grand</w> ;</l>
					<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">Si</w> <w n="2.2">vous</w> <w n="2.3">parlez</w>, <w n="2.4">il</w> <w n="2.5">perd</w> <w n="2.6">la</w> <w n="2.7">vie</w> !</l>
					<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1">C</w>'<w n="3.2">est</w> <w n="3.3">moi</w>, <w n="3.4">c</w>'<w n="3.5">est</w> <w n="3.6">moi</w> <w n="3.7">qui</w> <w n="3.8">vous</w> <w n="3.9">supplie</w></l>
					<l n="4" num="1.4"><w n="4.1">De</w> <w n="4.2">conserver</w> <w n="4.3">un</w> <w n="4.4">père</w> <w n="4.5">à</w> <w n="4.6">mon</w> <w n="4.7">enfant</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Ah</w> ! <w n="5.2">conservez</w> <w n="5.3">un</w> <w n="5.4">père</w> <w n="5.5">à</w> <w n="5.6">mon</w> <w n="5.7">enfant</w> !</l>
					<p>(Elle tombe tout-à-fait à genoux. )</p>
					<l n="6" num="1.6"><w n="6.1">Songez</w>, <w n="6.2">hélas</w> ! <w n="6.3">que</w> <w n="6.4">cette</w> <w n="6.5">pauvre</w> <w n="6.6">femme</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Dans</w> <w n="7.2">un</w> <w n="7.3">ami</w>, <w n="7.4">crut</w> <w n="7.5">trouver</w> <w n="7.6">un</w> <w n="7.7">soutien</w>.</l>
				</lg>
				<lg n="2">
					<head type="main">BONAPARTE, avec dignité.</head>
					<l n="8" num="2.1"><space unit="char" quantity="4"></space><w n="8.1">J</w>'<w n="8.2">ai</w> <w n="8.3">promis</w> <w n="8.4">le</w> <w n="8.5">secret</w>… <w n="8.6">Eh</w> <w n="8.7">bien</w> !</l>
					<l n="9" num="2.2"><space unit="char" quantity="4"></space><w n="9.1">Votre</w> <w n="9.2">ami</w> <w n="9.3">seul</w> <w n="9.4">sait</w> <w n="9.5">tout</w>, <w n="9.6">Madame</w> ;</l>
					<l n="10" num="2.3"><space unit="char" quantity="4"></space><w n="10.1">Mais</w> <w n="10.2">le</w> <w n="10.3">consul</w> <w n="10.4">n</w>'<w n="10.5">en</w> <w n="10.6">saura</w> <w n="10.7">rien</w>,</l>
					<l n="11" num="2.4"><space unit="char" quantity="4"></space><w n="11.1">Non</w>, <w n="11.2">Madame</w>, <w n="11.3">il</w> <w n="11.4">n</w>'<w n="11.5">en</w> <w n="11.6">saura</w> <w n="11.7">rien</w>.</l>
				</lg>
				<lg n="3">
					<head type="main">VICTORINE, levant les mains d'un air suppliant.</head>
					<l n="12" num="3.1"><space unit="char" quantity="4"></space><w n="12.1">De</w> <w n="12.2">grâce</w> ! <w n="12.3">qu</w>'<w n="12.4">il</w> <w n="12.5">n</w>'<w n="12.6">en</w> <w n="12.7">sache</w> <w n="12.8">rien</w> !</l>
				</lg>
			</div></body></text></TEI>