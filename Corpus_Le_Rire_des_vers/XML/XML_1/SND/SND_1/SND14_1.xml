<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE LIEUTENANT D'ARTILLERIE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="NSL" sort="2">
					<name>
						<forename>Jean-Pierre-Laurent</forename>
						<surname>DENOMBRET</surname>
						<addName type="pen_name">Charles NOMBRET SAINT-LAURENT</addName>
					</name>
					<date from="1791" to="1833">1791-1833</date>
				</author>
					<author key="DUV" sort="3">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>225 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SND_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=NWU:35556007830409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
								<repository>Northwestern university</repository>
								<idno type="URI">https://babel.hathitrust.org/cgi/pt?id=ien.35556007830409</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE II.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SND14">
				<head type="tune">Air de l'Ecu de six francs.</head>
				<lg n="1">
					<head type="main">Michel, à part.</head>
					<l n="1" num="1.1"><w n="1.1">Tantôt</w> <w n="1.2">c</w>'<w n="1.3">est</w> <w n="1.4">mes</w> <w n="1.5">yeux</w> <w n="1.6">qu</w>'<w n="1.7">ellʼ</w> <w n="1.8">consulte</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Tantôt</w> <w n="2.2">elle</w> <w n="2.3">s</w>'<w n="2.4">appuiʼ</w> <w n="2.5">sur</w> <w n="2.6">moi</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">ces</w> <w n="3.3">deux</w> <w n="3.4">choses</w> <w n="3.5">il</w> <w n="3.6">résulte</w></l>
					<l n="4" num="1.4"><w n="4.1">Qu</w>' <w n="4.2">je</w> <w n="4.3">nʼ</w> <w n="4.4">sais</w> <w n="4.5">plus</w> <w n="4.6">quel</w> <w n="4.7">est</w> <w n="4.8">mon</w> <w n="4.9">emploi</w>.<del type="repetition" reason="analysis" hand="LG">(bis.)</del></l>
					<l n="5" num="1.5"><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="5.1">Qu</w>' <w n="5.2">je</w> <w n="5.3">nʼ</w> <w n="5.4">sais</w> <w n="5.5">plus</w> <w n="5.6">quel</w> <w n="5.7">est</w> <w n="5.8">mon</w> <w n="5.9">emploi</w>. </add></subst></l>
					<l n="6" num="1.6"><w n="6.1">Certʼ</w> <w n="6.2">tous</w> <w n="6.3">les</w> <w n="6.4">états</w> <w n="6.5">sont</w> <w n="6.6">honnêtes</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">Mais</w> <w n="7.2">jʼ</w> <w n="7.3">veux</w> <w n="7.4">savoir</w>, <w n="7.5">dans</w> <w n="7.6">la</w> <w n="7.7">maison</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Si</w> <w n="8.2">jʼ</w> <w n="8.3">suis</w> <w n="8.4">r</w>’<w n="8.5">gardé</w> <w n="8.6">comme</w> <w n="8.7">un</w> <w n="8.8">bâton</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Ou</w> <w n="9.2">bien</w> <w n="9.3">comme</w> <w n="9.4">unʼ</w> <w n="9.5">pairʼ</w> <w n="9.6">de</w> <w n="9.7">lunettes</w>.</l>
				</lg>
			</div></body></text></TEI>