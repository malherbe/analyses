<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE34"> 	
 	
 			<head type="main">LA LIBERTÉ POLITIQUE,</head> <p>au public.</p>

			<head type="tune">AIR : Soldat français né d’obscurs laboureurs.</head>

		 <lg n="1"> <l n="1" num="1.1"><w n="1.1">En</w> <w n="1.2">célébrant</w> <w n="1.3">notre</w> <w n="1.4">prince</w> <w n="1.5">et</w> <w n="1.6">nos</w> <w n="1.7">lois</w>,</l>
			<l n="2" num="1.2"><w n="2.1">En</w> <w n="2.2">célébrant</w> <w n="2.3">la</w> <w n="2.4">gloire</w> <w n="2.5">de</w> <w n="2.6">la</w> <w n="2.7">France</w>,</l>
			<l n="3" num="1.3"><w n="3.1">Nos</w> <w n="3.2">auteurs</w> <w n="3.3">ont</w> <w n="3.4">pu</w> <w n="3.5">quelquefois</w></l>
			<l n="4" num="1.4"><w n="4.1">Montrer</w> <w n="4.2">ici</w> <w n="4.3">de</w> <w n="4.4">l</w>’<w n="4.5">inexpérience</w>.</l>
			<l n="5" num="1.5"><w n="5.1">Par</w> <w n="5.2">eux</w> <w n="5.3">l</w>’<w n="5.4">éloge</w> <w n="5.5">est</w> <w n="5.6">rendu</w> <w n="5.7">faiblement</w>,</l>
			<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">en</w> <w n="6.3">blâmer</w> <w n="6.4">serait</w> <w n="6.5">leur</w> <w n="6.6">faire</w> <w n="6.7">outrage</w> :</l>
			<l n="7" num="1.7"><w n="7.1">L</w>’<w n="7.2">art</w> <w n="7.3">de</w> <w n="7.4">louer</w> <w n="7.5">s</w>’<w n="7.6">acquiert</w> <w n="7.7">péniblement</w>,</l>
			<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">sous</w> <w n="8.3">l</w>’<w n="8.4">ancien</w> <w n="8.5">gouvernement</w></l>
			<l n="9" num="1.9"><w n="9.1">Ils</w> <w n="9.2">n</w>’<w n="9.3">ont</w> <w n="9.4">pas</w> <w n="9.5">fait</w> <w n="9.6">d</w>’<w n="9.7">apprentissage</w>. <del hand="LN" type="repetition" reason="analysis">(bis.)</del></l></lg>

			  <head type="main">CHŒUR.</head>

		   <lg n="2"><l n="10" num="2.1"><w n="10.1">Bonne</w> <w n="10.2">année</w>, <w n="10.3">bonne</w> <w n="10.4">année</w> !</l>
			<l n="11" num="2.2"><w n="11.1">Tout</w> <w n="11.2">l</w>’<w n="11.3">annonce</w>, <w n="11.4">tout</w> <w n="11.5">le</w> <w n="11.6">dit</w> :</l>
			<l n="12" num="2.3"><w n="12.1">Quelle</w> <w n="12.2">heureuse</w> <w n="12.3">destinée</w></l>
			<l n="13" num="2.4"><w n="13.1">Mathieu</w> <w n="13.2">Lænsberg</w> <w n="13.3">nous</w> <w n="13.4">prédit</w> !</l></lg></div></body></text></TEI>