<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE24">			
	
	<head type="main">LA LIBERTÉ POLITIQUE.</head>

			<p>Avez-vous donc oublié l’outrage fait à l’orateur que pleure encore la France ? l’hôtel des Quatre-Nations !</p>

			<head type="tune">Air du Vaudeville du jour des Noces.</head>

		    <lg n="1"><l n="1" num="1.1"><w n="1.1">On</w> <w n="1.2">trouve</w> <w n="1.3">là</w> <w n="1.4">quarante</w> <w n="1.5">locataires</w>,</l>
				<l n="2" num="1.2"><w n="2.1">Pour</w> <w n="2.2">qui</w> <w n="2.3">souvent</w> <w n="2.4">les</w> <w n="2.5">lauriers</w> <w n="2.6">ne</w> <w n="2.7">sont</w> <w n="2.8">rien</w> ;</l>
				<l n="3" num="1.3"><w n="3.1">Ils</w> <w n="3.2">ont</w> <w n="3.3">été</w> <w n="3.4">bien</w> <w n="3.5">injustes</w> <w n="3.6">naguère</w></l>
				<l n="4" num="1.4"><w n="4.1">Envers</w> <w n="4.2">un</w> <w n="4.3">grand</w> <w n="4.4">et</w> <w n="4.5">noble</w> <w n="4.6">citoyen</w>.</l>
				<l n="5" num="1.5"><w n="5.1">Son</w> <w n="5.2">nom</w> <w n="5.3">semblait</w> <w n="5.4">présager</w> <w n="5.5">sa</w> <w n="5.6">victoire</w> ...</l>
				<l n="6" num="1.6"><w n="6.1">Mais</w> <w n="6.2">par</w> <w n="6.3">ces</w> <w n="6.4">mots</w> <w n="6.5">on</w> <w n="6.6">arrêta</w> <w n="6.7">ses</w> <w n="6.8">pas</w>...</l>
				<l n="7" num="1.7"><w n="7.1">Quel</w> <w n="7.2">est</w> <w n="7.3">ton</w> <w n="7.4">titre</w> ? ... <w n="7.5">Il</w> <w n="7.6">répondit</w> : <w n="7.7">la</w> <w n="7.8">gloire</w> !</l>
				<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">le</w> <w n="8.3">concierge</w> <w n="8.4">a</w> <w n="8.5">dit</w> : <w n="8.6">On</w> <w n="8.7">n</w>’<w n="8.8">entre</w> <w n="8.9">pas</w> ....</l></lg></div></body></text></TEI>