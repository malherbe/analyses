<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER TABLEAU.</head><head type="main_subpart">SCÈNE VI.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG12">
						<head type="tune">AIR : du Hussard de Felsheim,</head>
						<lg n="1">
							<head type="main">MOREL.</head>
							<l n="1" num="1.1"><space unit="char" quantity="2"></space><w n="1.1">Un</w> <w n="1.2">corps</w> <w n="1.3">à</w> <w n="1.4">droite</w>, <w n="1.5">un</w> <w n="1.6">à</w> <w n="1.7">gauche</w> :</l>
							<l n="2" num="1.2"><space unit="char" quantity="2"></space><w n="2.1">Voilà</w> <w n="2.2">mes</w> <w n="2.3">Français</w> <w n="2.4">placés</w>.</l>
						</lg>
						<lg n="2">
							<head type="main">BONAPARTE.</head>
							<l n="3" num="2.1"><space unit="char" quantity="2"></space><w n="3.1">Au</w> <w n="3.2">centre</w> <w n="3.3">l</w>'<w n="3.4">ennemi</w> <w n="3.5">fauche</w>.</l>
							<l n="4" num="2.2"><space unit="char" quantity="2"></space><w n="4.1">Vos</w> <w n="4.2">Français</w> <w n="4.3">sont</w> <w n="4.4">terrassés</w>…</l>
						</lg>
						<p>(Ici le tambour et la trompette se font entendre à l'orchestre pendant que Bonaparte plante des épingles sur la carte.)</p>
						<lg n="3">
							<l n="5" num="3.1"><space unit="char" quantity="2"></space><w n="5.1">Prenant</w> <w n="5.2">cette</w> <w n="5.3">batterie</w>,</l>
							<l n="6" num="3.2"><space unit="char" quantity="2"></space><w n="6.1">Il</w> <w n="6.2">fallait</w> <w n="6.3">placer</w> <w n="6.4">ici</w></l>
							<l n="7" num="3.3"><space unit="char" quantity="2"></space><w n="7.1">Vingt</w> <w n="7.2">pièces</w> <w n="7.3">d</w>'<w n="7.4">artillerie</w></l>
							<l n="8" num="3.4"><space unit="char" quantity="2"></space><w n="8.1">Qui</w> <w n="8.2">foudroyaient</w> <w n="8.3">l</w>'<w n="8.4">ennemi</w>.</l>
						</lg>
						<div n="1" type="section">
							<head type="main">ENSEMBLE.</head>
							<lg n="1">
								<head type="main">MOREL, à part.</head>
								<l n="9" num="1.1"><space unit="char" quantity="10"></space><w n="9.1">C</w>'<w n="9.2">est</w> <w n="9.3">cela</w> ! <del hand="LG" type="repetition" reason="analysis">(bis.)</del></l>
								<l n="10" num="1.2"><space unit="char" quantity="10"></space><subst hand="LG" type="repetition" reason="analysis"><del></del><add rend="hidden"><w n="10.1">C</w>'<w n="10.2">est</w> <w n="10.3">cela</w> !</add></subst></l>
								<l n="11" num="1.3"><w n="11.1">Comment</w> <w n="11.2">peut</w>-<w n="11.3">il</w> <w n="11.4">savoir</w> <w n="11.5">cela</w> !</l>
							</lg>
							<lg n="2">
								<head type="main">BONAPARTE</head>
								<l n="12" num="2.1"><space unit="char" quantity="10"></space><w n="12.1">C</w>'<w n="12.2">est</w> <w n="12.3">cela</w> ! <del hand="LG" type="repetition" reason="analysis">(bis.)</del></l>
								<l n="13" num="2.2"><space unit="char" quantity="10"></space><subst hand="LG" type="repetition" reason="analysis"><del></del><add rend="hidden"><w n="13.1">C</w>'<w n="13.2">est</w> <w n="13.3">cela</w> !</add></subst></l>
								<l n="14" num="2.3"><w n="14.1">Ah</w> ! <w n="14.2">pourquoi</w> <w n="14.3">n</w>'<w n="14.4">étais</w>-<w n="14.5">je</w> <w n="14.6">pas</w> <w n="14.7">là</w> !</l>
							</lg>
						</div>
						<lg n="4">
							<head type="main">BONAPARTE.</head>
							<l n="15" num="4.1"><space unit="char" quantity="2"></space><w n="15.1">Maintenant</w>, <w n="15.2">changeant</w> <w n="15.3">de</w> <w n="15.4">route</w></l>
							<l n="16" num="4.2"><space unit="char" quantity="2"></space><w n="16.1">Et</w> <w n="16.2">sur</w> <w n="16.3">le</w> <w n="16.4">centre</w> <w n="16.5">chargeant</w></l>
							<l n="17" num="4.3"><space unit="char" quantity="2"></space><w n="17.1">Pour</w> <w n="17.2">enlever</w> <w n="17.3">la</w> <w n="17.4">redoute</w>,</l>
							<l n="18" num="4.4"><space unit="char" quantity="2"></space><w n="18.1">Nous</w> <w n="18.2">marchons</w> <w n="18.3">tambour</w> <w n="18.4">battant</w>.</l>
						</lg>
						<p>(Même jeu. Bonaparte s'anime davantage en finissant ce couplet.)</p>
						<lg n="5">
							<l n="19" num="5.1"><space unit="char" quantity="2"></space><w n="19.1">Tournant</w> <w n="19.2">l</w>'<w n="19.3">ennemi</w> <w n="19.4">par</w> <w n="19.5">ruse</w>,</l>
							<l n="20" num="5.2"><space unit="char" quantity="2"></space><w n="20.1">Tandis</w> <w n="20.2">qu</w>'<w n="20.3">il</w> <w n="20.4">me</w> <w n="20.5">croit</w> <w n="20.6">surpris</w>,</l>
							<l n="21" num="5.3"><space unit="char" quantity="2"></space><w n="21.1">À</w> <w n="21.2">me</w> <w n="21.3">poursuivre</w> <w n="21.4">il</w> <w n="21.5">s</w>'<w n="21.6">amuse</w>,</l>
							<l n="22" num="5.4"><space unit="char" quantity="2"></space><w n="22.1">J</w>'<w n="22.2">avance</w>, <w n="22.3">et</w> <w n="22.4">Rosbach</w> <w n="22.5">est</w> <w n="22.6">pris</w> !</l>
						</lg>
						<div n="2" type="section">
							<head type="main">ENSEMBLE.</head>
							<lg n="1">
								<head type="main">MOREL, à part, avec étonnement.</head>
								<l n="23" num="1.1"><space unit="char" quantity="8"></space><w n="23.1">C</w>'<w n="23.2">est</w> <w n="23.3">cela</w> ! (<w n="23.4">bis</w>.)</l>
								<l n="24" num="1.2"><space unit="char" quantity="10"></space><subst hand="LG" type="repetition" reason="analysis"><del></del><add rend="hidden"><w n="24.1">C</w>'<w n="24.2">est</w> <w n="24.3">cela</w> !</add></subst></l>
								<l n="25" num="1.3"><w n="25.1">Comment</w> <w n="25.2">peut</w>-<w n="25.3">il</w> <w n="25.4">savoir</w> <w n="25.5">cela</w> !</l>
							</lg>
							<lg n="2">
								<head type="main">BONAPARTE</head>
								<l n="26" num="2.1"><space unit="char" quantity="8"></space><w n="26.1">C</w>'<w n="26.2">est</w> <w n="26.3">cela</w> ! (<w n="26.4">bis</w>.)</l>
								<l n="27" num="2.2"><space unit="char" quantity="10"></space><subst hand="LG" type="repetition" reason="analysis"><del></del><add rend="hidden"><w n="27.1">C</w>'<w n="27.2">est</w> <w n="27.3">cela</w> !</add></subst></l>
								<l n="28" num="2.3"><w n="28.1">Ah</w> ! <w n="28.2">pourquoi</w> <w n="28.3">n</w>'<w n="28.4">étais</w>-<w n="28.5">je</w> <w n="28.6">pas</w> <w n="28.7">là</w> !</l>
							</lg>
						</div>
						<lg n="6">
							<head type="main">BONAPARTE.</head>
							<l n="29" num="6.1"><space unit="char" quantity="2"></space><w n="29.1">Ma</w> <w n="29.2">réserve</w> <w n="29.3">est</w> <w n="29.4">épargnée</w> ;</l>
							<l n="30" num="6.2"><space unit="char" quantity="2"></space><w n="30.1">Elle</w> <w n="30.2">offre</w> <w n="30.3">un</w> <w n="30.4">dernier</w> <w n="30.5">combat</w>,</l>
							<l n="31" num="6.3"><space unit="char" quantity="2"></space><w n="31.1">Et</w> <w n="31.2">la</w> <w n="31.3">bataille</w> <w n="31.4">est</w> <w n="31.5">gagnée</w> …</l>
						</lg>
						<lg n="7">
							<head type="main">JOSÉPHINE, montrant une épingle.</head>
							<l n="32" num="7.1"><space unit="char" quantity="2"></space><w n="32.1">Je</w> <w n="32.2">n</w>'<w n="32.3">ai</w> <w n="32.4">plus</w> <w n="32.5">qu</w>'<w n="32.6">un</w> <w n="32.7">seul</w> <w n="32.8">soldat</w> !</l>
						</lg>
						<lg n="8">
							<head type="main">BONAPARTE.</head>
							<l n="33" num="8.1"><space unit="char" quantity="2"></space><w n="33.1">Dans</w> <w n="33.2">l</w>'<w n="33.3">ardeur</w> <w n="33.4">qui</w> <w n="33.5">me</w> <w n="33.6">dévore</w>,</l>
							<l n="34" num="8.2"><space unit="char" quantity="2"></space><w n="34.1">J</w>'<w n="34.2">aurais</w> <w n="34.3">fait</w> <w n="34.4">bien</w> <w n="34.5">du</w> <w n="34.6">chemin</w> …</l>
							<l n="35" num="8.3"><space unit="char" quantity="2"></space><w n="35.1">Un</w> <w n="35.2">cent</w> <w n="35.3">d</w>'<w n="35.4">épingles</w> <w n="35.5">encore</w>,</l>
							<l n="36" num="8.4"><space unit="char" quantity="2"></space><w n="36.1">Et</w> <w n="36.2">j</w>'<w n="36.3">arrivais</w> <w n="36.4">à</w> <w n="36.5">Berlin</w> !</l>
						</lg>
						<div n="3" type="section">
							<head type="main">ENSEMBLE.</head>
							<lg n="1">
								<head type="main"></head>
								<l n="37" num="1.1"><space unit="char" quantity="8"></space><w n="37.1">C</w>'<w n="37.2">est</w> <w n="37.3">cela</w> ! (<w n="37.4">bis</w>.)</l>
								<l n="38" num="1.2"><space unit="char" quantity="10"></space><subst hand="LG" type="repetition" reason="analysis"><del></del><add rend="hidden"><w n="38.1">C</w>'<w n="38.2">est</w> <w n="38.3">cela</w> !</add></subst></l>
								<l n="39" num="1.3"><w n="39.1">Ah</w> ! <w n="39.2">pourquoi</w> <w n="39.3">n</w>'<w n="39.4">étais</w>-<w n="39.5">je</w> <w n="39.6">pas</w> <w n="39.7">là</w> !</l>
							</lg>
							<lg n="2">
								<head type="main">MOREL.</head>
								<l n="40" num="2.1"><space unit="char" quantity="8"></space><w n="40.1">C</w>'<w n="40.2">est</w> <w n="40.3">cela</w> ! (<w n="40.4">bis</w>.)</l>
								<l n="41" num="2.2"><space unit="char" quantity="10"></space><subst hand="LG" type="repetition" reason="analysis"><del></del><add rend="hidden"><w n="41.1">C</w>'<w n="41.2">est</w> <w n="41.3">cela</w> !</add></subst></l>
								<l n="42" num="2.3"><w n="42.1">Comment</w> <w n="42.2">peut</w>-<w n="42.3">il</w> <w n="42.4">savoir</w> <w n="42.5">cela</w> !</l>
							</lg>
						</div>
					</div></body></text></TEI>