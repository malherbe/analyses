<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TROISIÈME TABLEAU.</head><head type="main_subpart">SCÈNE XIX.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG26">
						<head type="tune">AIR : En avant bon courage (de Trois Jours en une heure.)</head>
						<lg n="1">
							<head type="main">CHŒUR GENERAL.</head>
							<l n="1" num="1.1"><w n="1.1">Pour</w> <w n="1.2">lui</w> <w n="1.3">la</w> <w n="1.4">chance</w> <w n="1.5">est</w> <w n="1.6">bonne</w> !</l>
							<l n="2" num="1.2"><w n="2.1">Il</w> <w n="2.2">a</w> <w n="2.3">le</w> <w n="2.4">prix</w> <w n="2.5">d</w>'<w n="2.6">honneur</w> :</l>
							<l n="3" num="1.3"><w n="3.1">Plaçons</w> <w n="3.2">cette</w> <w n="3.3">couronne</w></l>
							<l n="4" num="1.4"><w n="4.1">Sur</w> <w n="4.2">le</w> <w n="4.3">front</w> <w n="4.4">du</w> <w n="4.5">vainqueur</w>.</l>
						</lg>
						<lg n="2">
							<head type="main">BONAPARTE, prenant une couronne.</head>
							<l n="5" num="2.1"><w n="5.1">Quel</w> <w n="5.2">bonheur</w> <w n="5.3">pour</w> <w n="5.4">ma</w> <w n="5.5">mère</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Je</w> <w n="6.2">vois</w> <w n="6.3">mon</w> <w n="6.4">frère</w> <w n="6.5">admis</w> !</l>
							<l n="7" num="2.3"><w n="7.1">Quels</w> <w n="7.2">progrès</w> <w n="7.3">je</w> <w n="7.4">vais</w> <w n="7.5">faire</w>,</l>
							<l n="8" num="2.4"><w n="8.1">On</w> <w n="8.2">me</w> <w n="8.3">mène</w> <w n="8.4">à</w> <w n="8.5">Paris</w> !</l>
						</lg>
						<lg n="3">
							<head type="main">CHŒUR.</head>
							<l n="9" num="3.1"><w n="9.1">Pour</w> <w n="9.2">lui</w> <w n="9.3">la</w> <w n="9.4">chance</w> <w n="9.5">est</w> <w n="9.6">bonne</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Il</w> <w n="10.2">a</w> <w n="10.3">le</w> <w n="10.4">prix</w>, <subst reason="analysis" type="repetition" hand="LG"><del>etc.</del><add rend="hidden"><w n="10.5">d</w>'<w n="10.6">honneur</w> :</add></subst></l>
							<l n="11" num="3.3"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="11.1">Plaçons</w> <w n="11.2">cette</w> <w n="11.3">couronne</w></add></subst></l>
							<l n="12" num="3.4"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="12.1">Sur</w> <w n="12.2">le</w> <w n="12.3">front</w> <w n="12.4">du</w> <w n="12.5">vainqueur</w>.</add></subst></l>
						</lg>
						<lg n="4">
							<head type="main">JOSÉPHINE.</head>
							<l n="13" num="4.1"><w n="13.1">L</w>'<w n="13.2">amour</w>, <w n="13.3">je</w> <w n="13.4">le</w> <w n="13.5">devine</w>,</l>
							<l n="14" num="4.2"><w n="14.1">Vous</w> <w n="14.2">garde</w> <w n="14.3">d</w>'<w n="14.4">heureux</w> <w n="14.5">jours</w>,</l>
							<l n="15" num="4.3"><w n="15.1">Du</w> <w n="15.2">nom</w> <w n="15.3">de</w> <w n="15.4">Joséphine</w></l>
							<l n="16" num="4.4"><w n="16.1">Souvenez</w>-<w n="16.2">vous</w> <w n="16.3">toujours</w>.</l>
						</lg>
						<lg n="5">
							<head type="main">CHŒUR.</head>
							<l n="17" num="5.1"><w n="17.1">Pour</w> <w n="17.2">lui</w> <w n="17.3">la</w> <w n="17.4">chance</w> <w n="17.5">est</w> <w n="17.6">bonne</w>,</l>
							<l n="18" num="5.2"><w n="18.1">Il</w> <w n="18.2">a</w> <w n="18.3">le</w> <w n="18.4">prix</w>, <subst reason="analysis" type="repetition" hand="LG"><del>etc.</del><add rend="hidden"><w n="18.5">d</w>'<w n="18.6">honneur</w> :</add></subst></l>
							<l n="19" num="5.3"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="19.1">Plaçons</w> <w n="19.2">cette</w> <w n="19.3">couronne</w></add></subst></l>
							<l n="20" num="5.4"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="20.1">Sur</w> <w n="20.2">le</w> <w n="20.3">front</w> <w n="20.4">du</w> <w n="20.5">vainqueur</w>.</add></subst></l>
						</lg>
						<lg n="6">
							<head type="main">DARBEL.</head>
							<l n="21" num="6.1"><w n="21.1">Dans</w> <w n="21.2">quelque</w> <w n="21.3">temps</w>, <w n="21.4">je</w> <w n="21.5">pense</w>,</l>
							<l n="22" num="6.2"><w n="22.1">Tu</w> <w n="22.2">seras</w> <w n="22.3">général</w> ;</l>
							<l n="23" num="6.3"><w n="23.1">Mais</w> <w n="23.2">toujours</w> <w n="23.3">pour</w> <w n="23.4">la</w> <w n="23.5">France</w></l>
							<l n="24" num="6.4"><w n="24.1">Le</w> <w n="24.2">petit</w> <w n="24.3">caporal</w> !</l>
						</lg>
						<lg n="7">
							<head type="main">CHŒUR</head>
							<l n="25" num="7.1"><w n="25.1">Pour</w> <w n="25.2">lui</w> <w n="25.3">la</w> <w n="25.4">chance</w> <w n="25.5">est</w> <w n="25.6">bonne</w>,</l>
							<l n="26" num="7.2"><w n="26.1">Il</w> <w n="26.2">a</w> <w n="26.3">le</w> <w n="26.4">prix</w>, <subst reason="analysis" type="repetition" hand="LG"><del>etc.</del><add rend="hidden"><w n="26.5">d</w>'<w n="26.6">honneur</w> :</add></subst></l>
							<l n="27" num="7.3"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="27.1">Plaçons</w> <w n="27.2">cette</w> <w n="27.3">couronne</w></add></subst></l>
							<l n="28" num="7.4"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="28.1">Sur</w> <w n="28.2">le</w> <w n="28.3">front</w> <w n="28.4">du</w> <w n="28.5">vainqueur</w>.</add></subst></l>
						</lg>
						<lg n="8">
							<head type="main">LE CAPITAINE.</head>
							<l n="29" num="8.1"><w n="29.1">Le</w> <w n="29.2">sort</w> <w n="29.3">au</w> <w n="29.4">rang</w> <w n="29.5">de</w> <w n="29.6">maître</w></l>
							<l n="30" num="8.2"><w n="30.1">L</w>'<w n="30.2">élèvera</w> <w n="30.3">bientôt</w> …</l>
							<l n="31" num="8.3"><w n="31.1">Il</w> <w n="31.2">doit</w> <w n="31.3">tomber</w> <w n="31.4">peut</w>-<w n="31.5">être</w>,</l>
							<l n="32" num="8.4"><w n="32.1">Mais</w> <w n="32.2">tomber</w> <w n="32.3">de</w> <w n="32.4">bien</w> <w n="32.5">haut</w> !…</l>
						</lg>
						<lg n="9">
							<head type="main">CHŒUR.</head>
							<l n="33" num="9.1"><w n="33.1">Pour</w> <w n="33.2">lui</w> <w n="33.3">la</w> <w n="33.4">chance</w> <w n="33.5">est</w> <w n="33.6">bonne</w>,</l>
							<l n="34" num="9.2"><w n="34.1">Il</w> <w n="34.2">a</w> <w n="34.3">le</w> <w n="34.4">prix</w>,<subst reason="analysis" type="repetition" hand="LG"><del>etc.</del><add rend="hidden"><w n="34.5">d</w>'<w n="34.6">honneur</w> :</add></subst></l>
							<l n="35" num="9.3"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="35.1">Plaçons</w> <w n="35.2">cette</w> <w n="35.3">couronne</w></add></subst></l>
							<l n="36" num="9.4"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="36.1">Sur</w> <w n="36.2">le</w> <w n="36.3">front</w> <w n="36.4">du</w> <w n="36.5">vainqueur</w>.</add></subst></l>
						</lg>
						<lg n="10">
							<head type="main">ÆGIDIUS.</head>
							<l n="37" num="10.1"><w n="37.1">Du</w> <w n="37.2">latin</w> <w n="37.3">de</w> <w n="37.4">Sénèque</w></l>
							<l n="38" num="10.2"><w n="38.1">S</w>'<w n="38.2">il</w> <w n="38.3">s</w>'<w n="38.4">était</w> <w n="38.5">bien</w> <w n="38.6">nourri</w>,</l>
							<l n="39" num="10.3"><w n="39.1">Quel</w> <w n="39.2">bon</w> <w n="39.3">petit</w> <w n="39.4">évêque</w></l>
							<l n="40" num="10.4"><w n="40.1">On</w> <w n="40.2">aurait</w> <w n="40.3">fait</w> <w n="40.4">de</w> <w n="40.5">lui</w> !</l>
						</lg>
						<lg n="11">
							<head type="main">CHŒUR</head>
							<l n="41" num="11.1"><w n="41.1">Pour</w> <w n="41.2">lui</w> <w n="41.3">la</w> <w n="41.4">chance</w> <w n="41.5">est</w> <w n="41.6">bonne</w>,</l>
							<l n="42" num="11.2"><w n="42.1">Il</w> <w n="42.2">a</w> <w n="42.3">le</w> <w n="42.4">prix</w>, <subst reason="analysis" type="repetition" hand="LG"><del>etc.</del><add rend="hidden"><w n="42.5">d</w>'<w n="42.6">honneur</w> :</add></subst></l>
							<l n="43" num="11.3"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="43.1">Plaçons</w> <w n="43.2">cette</w> <w n="43.3">couronne</w></add></subst></l>
							<l n="44" num="11.4"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="44.1">Sur</w> <w n="44.2">le</w> <w n="44.3">front</w> <w n="44.4">du</w> <w n="44.5">vainqueur</w>.</add></subst></l>
						</lg>
						<lg n="12">
							<head type="main">BONAPARTE, au public.</head>
							<l n="45" num="12.1"><w n="45.1">Il</w> <w n="45.2">nous</w> <w n="45.3">lègue</w> <w n="45.4">une</w> <w n="45.5">gloire</w></l>
							<l n="46" num="12.2"><w n="46.1">Que</w> <w n="46.2">rien</w> <w n="46.3">ne</w> <w n="46.4">peut</w> <w n="46.5">ternir</w> ;</l>
							<l n="47" num="12.3"><w n="47.1">Français</w> <w n="47.2">à</w> <w n="47.3">sa</w> <w n="47.4">mémoire</w></l>
							<l n="48" num="12.4"><w n="48.1">Donnons</w> <w n="48.2">un</w> <w n="48.3">souvenir</w>.</l>
						</lg>
						<lg n="13">
							<head type="main">CHŒUR.</head>
							<l n="49" num="13.1"><w n="49.1">Pour</w> <w n="49.2">lui</w> <w n="49.3">la</w> <w n="49.4">chance</w> <w n="49.5">est</w> <w n="49.6">bonne</w> !</l>
							<l n="50" num="13.2"><w n="50.1">Il</w> <w n="50.2">a</w> <w n="50.3">le</w> <w n="50.4">prix</w> <w n="50.5">d</w>'<w n="50.6">honneur</w> ;</l>
							<l n="51" num="13.3"><w n="51.1">Plaçons</w> <w n="51.2">cette</w> <w n="51.3">couronne</w></l>
							<l n="52" num="13.4"><w n="52.1">Sur</w> <w n="52.2">le</w> <w n="52.3">front</w> <w n="52.4">du</w> <w n="52.5">vainqueur</w>.</l>
						</lg>
					</div></body></text></TEI>