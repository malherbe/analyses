<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <fileDesc>
   <titleStmt>
	<title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
	<title type="corpus">Le Rire des vers</title>
	<author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
	</author>
	<author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
	</author>
	<editor>Le Rire des vers, Université de Bâle</editor>
	<editor>
	 Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
	 <choice>
	  <abbr>CRISCO, Université de Caen Normandie</abbr>
	  <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	 </choice>
	 (EA 4255)
	</editor>
	<respStmt>
	 <resp>Encodage en XML (CRISCO, université de Caen)</resp>
	 <name id="KL">
	  <forename>Kedi</forename>
	  <surname>LI</surname>
	 </name>
	</respStmt>
	<respStmt>
	 <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
	 <name id="RR">
	  <forename>Richard</forename>
	  <surname>RENAULT</surname>
	 </name>
	</respStmt>
   </titleStmt>
   <extent>140 vers</extent>
   <publicationStmt>
	<publisher>
	 <orgname>
	  <choice>
	   <abbr>CRISCO, Université de Caen Normandie</abbr>
	   <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	  </choice>
	 </orgname>
	 <address>
	  <addrLine>Université de Caen</addrLine>
	  <addrLine>14032 CAEN CEDEX</addrLine>
	  <addrLine>FRANCE</addrLine>
	 </address>
	 <email>crisco.incipit@unicaen.fr</email>
	 <ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
	</publisher>
	<pubPlace>Caen</pubPlace>
	<date when="2022">2022</date>
	<idno type="local">SCB_2</idno>	
	<availability status="free">
		<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
		<p>
			Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
			CC = Licence Creative Commons
			BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
			sur les sources et sur les modifications introduites.
			NC = Pas d’utilisation commerciale.
			SA = Partage dans les mêmes conditions.
		</p>
	</availability>
   </publicationStmt>
   <sourceDesc>
	<biblFull>
	 <titleStmt>
	  <title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	  <author>SCRIBE ET BAYARD</author>
	 </titleStmt>
	 <publicationStmt>
	  <publisher>INTERNET ARCHIVE</publisher>
	  <idno type="URL">https://archive.org/details/camillaoulasoeur00scriuoft</idno>
	 </publicationStmt>
	 <sourceDesc>
	  <biblStruct>
	   <monogr>
		<repository>University of Toronto Libraries</repository>
		<idno type="URL">https://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991106543695706196</idno>
	   </monogr>
	  </biblStruct>                 
	 </sourceDesc>
	</biblFull> 
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <creation>
	<date when="1832">12 DÉCEMBRE 1832</date>
	<placeName>
	 <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
	</placeName>
   </creation>
  </profileDesc>
  <encodingDesc>
   <projectDesc>
	<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
   </projectDesc>
   <samplingDecl>
	<p>Les parties versifiées ont été prioritairement encodées.</p>
   </samplingDecl>
   <editorialDecl>
	<normalization>
	 <p>Les majuscules accentuées ont été restituées.</p>
	 <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
	 <p>La ponctuation a été normalisée.</p> 
	 <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
	 <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
	 <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
	</normalization>
   </editorialDecl>
  </encodingDesc>
 </teiHeader><text><body><head type="main_part">CHŒUR D'ENTRÉE</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB22" modus="sp" lm_max="8" metProfile="8, 6, 4" form="suite de strophes" schema="1[aaa] 3[aa]" er_moy="8.8" er_max="42" er_min="0" er_mode="0(3/5)" er_moy_et="16.62" qr_moy="0.0" qr_max="C0" qr_mode="0(5/5)" qr_moy_et="0.0">
		<head type="tune">AIR de danse de la Bayadère.</head>
		<lg n="1" type="regexp" rhyme="aaa">
			<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="pe:1"><pgtc id="1" weight="42" schema="V[CV[CVCVC[V[CV[CV[CR" part="1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</pgtc></w>! <w n="1.2"><pgtc id="1" weight="42" schema="V[CV[CVCVC[V[CV[CV[CR" part="2">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>l</pgtc></w> <w n="1.3" punct="pe:4"><pgtc id="1" weight="42" schema="V[CV[CVCVC[V[CV[CV[CR" part="3">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="pe">i</seg>r</pgtc></w> ! <w n="1.4" punct="pe:5"><pgtc id="1" weight="42" schema="V[CV[CVCVC[V[CV[CV[CR" part="4"><seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="pe">A</seg>h</pgtc></w> ! <w n="1.5"><pgtc id="1" weight="42" schema="V[CV[CVCVC[V[CV[CV[CR" part="5">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6">e</seg>l</pgtc></w> <w n="1.6"><pgtc id="1" weight="42" schema="V[CV[CVCVC[V[CV[CV[CR" part="6">b<seg phoneme="o" type="vs" value="1" rule="314" place="7">eau</seg></pgtc></w> <w n="1.7" punct="pe:8"><pgtc id="1" weight="42" schema="V[CV[CVCVC[V[CV[CV[CR" part="7">j<rhyme label="a" id="1" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pe">ou</seg>r</rhyme></pgtc></w> !</l>
			<l rhyme="none" n="2" num="1.2" lm="6" met="6"><w n="2.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="2.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r</w> <w n="2.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="2.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.5" punct="pe:6"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="351" place="6">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></w> !</l>
			<l n="3" num="1.3" lm="8" met="8"><w n="3.1" punct="pe:1"><pgtc id="1" weight="42" schema="V[CV[CVCVC[V[CV[CV[CR" part="1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</pgtc></w> ! <w n="3.2"><pgtc id="1" weight="42" schema="V[CV[CVCVC[V[CV[CV[CR" part="2">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>l</pgtc></w> <w n="3.3" punct="pe:4"><pgtc id="1" weight="42" schema="V[CV[CVCVC[V[CV[CV[CR" part="3">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="pe">i</seg>r</pgtc></w> ! <w n="3.4" punct="pe:5"><pgtc id="1" weight="42" schema="V[CV[CVCVC[V[CV[CV[CR" part="4"><seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="pe">a</seg>h</pgtc></w> ! <w n="3.5"><pgtc id="1" weight="42" schema="V[CV[CVCVC[V[CV[CV[CR" part="5">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6">e</seg>l</pgtc></w> <w n="3.6"><pgtc id="1" weight="42" schema="V[CV[CVCVC[V[CV[CV[CR" part="6">b<seg phoneme="o" type="vs" value="1" rule="314" place="7">eau</seg></pgtc></w> <w n="3.7" punct="pe:8"><pgtc id="1" weight="42" schema="V[CV[CVCVC[V[CV[CV[CR" part="7">j<rhyme label="a" id="1" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pe">ou</seg>r</rhyme></pgtc></w> !</l>
			<l n="4" num="1.4" lm="6" met="6"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4" punct="pt:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<pgtc id="1" weight="0" schema="R" part="1"><rhyme label="a" id="1" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="pt">ou</seg>r</rhyme></pgtc></w>.</l>
		</lg>
		<lg n="2" type="regexp" rhyme="aaaaa">
		<head type="speaker">PRETTY.</head>
			<l n="5" num="2.1" lm="4" met="4"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="5.2">v<seg phoneme="wa" type="vs" value="1" rule="439" place="2">o</seg>y<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>g<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="a" id="2" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</rhyme></pgtc></w></l>
			<l n="6" num="2.2" lm="4" met="4"><w n="6.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="6.3">s<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="6.4" punct="vg:4">s<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="a" id="2" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="248" place="4" punct="vg">œu</seg>r</rhyme></pgtc></w>,</l>
			<l n="7" num="2.3" lm="4" met="4"><w n="7.1" punct="vg:2"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg></w>, <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r</w> <w n="7.3">t<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="422" place="4">oi</seg></rhyme></pgtc></w></l>
			<l n="8" num="2.4" lm="4" met="4"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">pr<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>v<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="m" type="e" stanza="3" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="422" place="4">oi</seg></rhyme></pgtc></w></l>
			<l n="9" num="2.5" lm="8" met="8"><w n="9.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">pr<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4">en</seg>t</w> <w n="9.3">m</w>'<w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="9.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg><pgtc id="4" weight="2" schema="CR" part="1">c<rhyme label="a" id="4" gender="m" type="a" stanza="4" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
		</lg>
		<lg n="3" type="regexp" rhyme="a">
		<head type="speaker">EDGARD.</head>
			<l n="10" num="3.1" lm="8" met="8"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="10.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>t</w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.5">j</w>'<w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg></w> <w n="10.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg><pgtc id="4" weight="2" schema="CR" part="1">s<rhyme label="a" id="4" gender="m" type="e" stanza="4" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></rhyme></pgtc></w></l>
		</lg>
	</div></body></text></TEI>