<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB44" modus="cp" lm_max="10" metProfile="4+6, (8)" form="suite de strophes" schema="2[abab]" er_moy="0.5" er_max="2" er_min="0" er_mode="0(3/4)" er_moy_et="0.87" qr_moy="0.0" qr_max="C0" qr_mode="0(4/4)" qr_moy_et="0.0">
	<head type="tune">AIR du Vaudeville de la Somnambule.</head>
	<lg n="1" type="regexp" rhyme="aba">
		<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="1.2">l<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="1.3">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="M">om</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="1.4">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="1.5" punct="pt:10">c<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>r<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
		<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="C">ou</seg>s</w> <w n="2.3" punct="ps:4">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rdi<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="ps" caesura="1">ez</seg></w>…<caesura></caesura> <w n="2.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="2.5">pr<seg phoneme="o" type="vs" value="1" rule="443" place="6" mp="M">o</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="300" place="8">ai</seg></w> <w n="2.6">v<seg phoneme="o" type="vs" value="1" rule="437" place="9" mp="C">o</seg>s</w> <w n="2.7" punct="pt:10"><pgtc id="2" weight="2" schema="[CR">p<rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
		<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1" mp="P">an</seg>s</w> <w n="3.2">v<seg phoneme="o" type="vs" value="1" rule="437" place="2" mp="C">o</seg>s</w> <w n="3.3" punct="vg:4">pr<seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="189" place="4" punct="vg" caesura="1">e</seg>ts</w>,<caesura></caesura> <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="C">u</seg></w> <w n="3.5" punct="vg:6">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="6" punct="vg">oin</seg>s</w>, <w n="3.6">s<seg phoneme="wa" type="vs" value="1" rule="439" place="7" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="8">ez</seg></w> <w n="3.7">pl<seg phoneme="y" type="vs" value="1" rule="449" place="9">u</seg>s</w> <w n="3.8" punct="vg:10">s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
		<l part="I" n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="M/mp">Ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="2" mp="M/mp">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="3" mp="Lp">ez</seg></w>-<w n="4.2" punct="pt:4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4" punct="pt" caesura="1">e</seg>s</w>.<caesura></caesura> </l>
	</lg>
	<lg n="2" type="regexp" rhyme="babab">
	<head type="speaker">RODOLPHE.</head>
		<l part="F" n="4" lm="10" met="4+6"><w n="4.3" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="pe">A</seg>h</w> ! <w n="4.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="4.6">cr<seg phoneme="wa" type="vs" value="1" rule="439" place="8" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="9">ez</seg></w> <w n="4.7" punct="pt:10"><pgtc id="2" weight="2" schema="[CR">p<rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
		<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="P">À</seg></w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="C">a</seg></w> <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w><caesura></caesura> <w n="5.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="C">i</seg>l</w> <w n="5.5">f<seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>t</w> <w n="5.6">r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7" mp="M">e</seg>st<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w> <w n="5.7" punct="pv:10">f<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>d<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
		<l n="6" num="2.2" lm="10" met="4+6"><w n="6.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="188" place="1" punct="vg">E</seg>t</w>, <w n="6.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="6.4" punct="vg:4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="4" punct="vg" caesura="1">en</seg>s</w>,<caesura></caesura> <w n="6.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="C">on</seg></w> <w n="6.6">b<seg phoneme="o" type="vs" value="1" rule="443" place="6" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r</w> <w n="6.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="6.8">pl<seg phoneme="y" type="vs" value="1" rule="449" place="9">u</seg>s</w> <w n="6.9" punct="vg:10">d<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="vg">ou</seg>x</rhyme></pgtc></w>,</l>
		<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>s</w> <w n="7.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="490" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="7.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="7.5">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="7.6">p<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="P">ou</seg>r</w> <w n="7.7" punct="vg:10"><pgtc id="3" weight="0" schema="[R"><rhyme label="a" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
		<l n="8" num="2.4" lm="8"><w n="8.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="8.2">d</w>'<w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.4">s<seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>r</w> <w n="8.6" punct="pt:8">v<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">ou</seg>s</rhyme></pgtc></w>.</l>
	</lg>
</div></body></text></TEI>