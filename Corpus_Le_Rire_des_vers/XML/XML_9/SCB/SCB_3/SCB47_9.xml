<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB47" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="1[ababb] 1[abba]" er_moy="0.6" er_max="3" er_min="0" er_mode="0(4/5)" er_moy_et="1.2" qr_moy="0.0" qr_max="C0" qr_mode="0(5/5)" qr_moy_et="0.0">
	<head type="tune">AIR de Turenne.</head>
	<lg n="1" type="regexp" rhyme="abab">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w>-<w n="1.3" punct="pe:3">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="3" punct="pe ps">e</seg></w> ! … <w n="1.4">d<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="6">en</seg>t</w> <w n="1.5" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>p<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="2.5">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="2.6" punct="vg:8"><pgtc id="2" weight="3" schema="[GR">li<rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="3.3">d<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>gu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="6">en</seg>t</w> <w n="3.4" punct="ps:8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">em</seg>bl<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></pgtc></w>…</l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1" punct="vg:2">M<seg phoneme="œ" type="vs" value="1" rule="150" place="1">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="2" punct="vg">eu</seg>r</w>, <w n="4.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3">d<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.4">v<seg phoneme="o" type="vs" value="1" rule="437" place="6">o</seg>s</w> <w n="4.5" punct="pi:8"><seg phoneme="a" type="vs" value="1" rule="342" place="7">a</seg><pgtc id="2" weight="3" schema="GR">ï<rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pi">eu</seg>x</rhyme></pgtc></w> ?</l>
	</lg>
	<lg n="2" type="regexp" rhyme="babba">
	<head type="speaker">RODOLPAE, bas.</head>
		<l n="5" num="2.1" lm="8" met="8"><w n="5.1" punct="pe:3">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe ps">e</seg></w> ! … <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>s</w> <w n="5.5">d</w>'<w n="5.6" punct="pt:8"><pgtc id="2" weight="0" schema="[R"><rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l> 
		<stage>(L'amenant sur le bord du théâtre.)</stage>
		<l n="6" num="2.2" lm="8" met="8"><w n="6.1">Qu</w>'<w n="6.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>ls</w> <w n="6.3">n</w>'<w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="6.5" punct="vg:5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="5" punct="vg">oin</seg>t</w>, <w n="6.6"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg></w> <w n="6.7" punct="pt:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>tr<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
		<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>ls</w> <w n="7.2">r<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="7.3">tr<seg phoneme="o" type="vs" value="1" rule="432" place="5">o</seg>p</w> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="7.5">v<seg phoneme="wa" type="vs" value="1" rule="439" place="7">o</seg>y<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>t</rhyme></pgtc></w></l>
		<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="8.2">l<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r</w> <w n="8.3">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>d<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>t</rhyme></pgtc></w></l>
		<l n="9" num="2.5" lm="8" met="8"><w n="9.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>r</w> <w n="9.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381" place="5">e</seg>il</w> <w n="9.4" punct="pt:8">m<seg phoneme="i" type="vs" value="1" rule="466" place="6">i</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>st<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
	</lg>
</div></body></text></TEI>