<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB52" modus="sm" lm_max="6" metProfile="6" form="suite de strophes" schema="8[aa] 4[abab]" er_moy="4.5" er_max="14" er_min="0" er_mode="0(5/16)" er_moy_et="4.66" qr_moy="0.0" qr_max="C0" qr_mode="0(16/16)" qr_moy_et="0.0">
	<head type="tune">AIR : O bords heureux du Gange ( de LA BAYADÈRE).</head>
		<div type="section" n="1">
			<head type="main">PREMIER COUPLET.</head>
			<lg n="1" type="regexp" rhyme="a">
			<head type="speaker">HENRIETTE.</head>
				<l n="1" num="1.1" lm="6" met="6"><w n="1.1">C</w>'<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="1.3">qu</w>'<w n="1.4"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="1.6"><seg phoneme="y" type="vs" value="1" rule="452" place="4">u</seg>n<pgtc id="1" weight="6" schema="V[CR" part="1"><seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></pgtc></w> <w n="1.7" punct="ps:6"><pgtc id="1" weight="6" schema="V[CR" part="2">gr<rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="6">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="ps">e</seg></rhyme></pgtc></w>…</l>
			</lg>
			<lg n="2" type="regexp" rhyme="a">
			<head type="speaker">LE GRAND-DUC.</head>
				<l n="2" num="2.1" lm="6" met="6"><w n="2.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="2.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>c</w> <w n="2.4">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>tt<pgtc id="1" weight="6" schema="V[CR" part="1"><seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></pgtc></w> <w n="2.5" punct="ps:6"><pgtc id="1" weight="6" schema="V[CR" part="2">gr<rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="6">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="ps">e</seg></rhyme></pgtc></w>…</l>
			</lg>
			<lg n="3" type="regexp" rhyme="a">
			<head type="speaker">HENRIETTE.</head>
				<l n="3" num="3.1" lm="6" met="6"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">v<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="3.4" punct="pt:6"><pgtc id="2" weight="12" schema="[VCVCR" part="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="4">im</seg>pl<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>r<rhyme label="a" id="2" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="pt">er</seg></rhyme></pgtc></w>.</l>
			</lg>
			<lg n="4" type="regexp" rhyme="a">
			<head type="speaker">LE GRAND-DUC.</head>
				<l n="4" num="4.1" lm="6" met="6"><w n="4.1">Qu</w>'<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.3">v<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>t</w> <w n="4.4" punct="pi:6"><pgtc id="2" weight="12" schema="[VCVCR" part="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="4">im</seg>pl<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>r<rhyme label="a" id="2" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="pi">er</seg></rhyme></pgtc></w> ?</l>
			</lg>
			<lg n="5" type="regexp" rhyme="a">
			<head type="speaker">HENRIETTE,</head>
				<l n="5" num="5.1" lm="6" met="6"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="5.2">c</w>'<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r</w> <w n="5.5">tr<seg phoneme="o" type="vs" value="1" rule="432" place="4">o</seg>p</w> <w n="5.6">d</w>'<w n="5.7" punct="pt:6"><pgtc id="3" weight="6" schema="[VCR" part="1"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>d<rhyme label="a" id="3" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
			</lg>
			<lg n="6" type="regexp" rhyme="a">
			<head type="speaker">LE GRAND-DUC.</head>
				<l n="6" num="6.1" lm="6" met="6"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">n</w>'<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="6.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="3">oin</seg>t</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.6">l</w>'<w n="6.7" punct="pt:6"><pgtc id="3" weight="6" schema="[VCR" part="1"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>d<rhyme label="a" id="3" gender="f" type="e" stanza="3" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
			</lg>
			<lg n="7" type="regexp" rhyme="a">
			<head type="speaker">HENRIETTE.</head>
				<l n="7" num="7.1" lm="6" met="6"><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="7.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.3" punct="pt:6"><pgtc id="4" weight="14" schema="[CVCVCR" part="1">r<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>r<rhyme label="a" id="4" gender="m" type="a" stanza="4" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="pt">er</seg></rhyme></pgtc></w>.</l>
			</lg>
			<lg n="8" type="regexp" rhyme="a">
			<head type="speaker">LE GRAND-DUC.</head>
				<l n="8" num="8.1" lm="6" met="6"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="8.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="8.3" punct="pt:6"><pgtc id="4" weight="14" schema="[CVCVCR" part="1">r<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>r<rhyme label="a" id="4" gender="m" type="e" stanza="4" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="pt">er</seg></rhyme></pgtc></w>.</l>
			</lg>
		</div>
		<div type="section" n="2">
			<head type="main">ENSEMBLE.</head> 
			<lg n="1" type="regexp" rhyme="abab">
			<head type="speaker">HENRIETTE.</head>
 				<l n="9" num="1.1" lm="6" met="6"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="9.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="9.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="338" place="3">a</seg>y<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="9.4">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rt<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="a" stanza="5" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
				<l n="10" num="1.2" lm="6" met="6"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">su<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="10.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="10.5" punct="pt:6">c<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg><pgtc id="6" weight="2" schema="CR" part="1">d<rhyme label="b" id="6" gender="m" type="a" stanza="5" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="pt">er</seg></rhyme></pgtc></w>.</l>
				<l n="11" num="1.3" lm="6" met="6"><w n="11.1"><seg phoneme="y" type="vs" value="1" rule="452" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.2">f<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="11.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>v<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="e" stanza="5" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
				<l n="12" num="1.4" lm="6" met="6"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="2">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="12.3" punct="pt:6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg><pgtc id="6" weight="2" schema="CR" part="1">d<rhyme label="b" id="6" gender="m" type="e" stanza="5" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="pt">er</seg></rhyme></pgtc></w>.</l>
			</lg>
			<lg n="2" type="regexp" rhyme="abab">
			<head type="speaker">LE GRAND-DUC.</head>
				<l n="13" num="2.1" lm="6" met="6"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="13.2">v<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>s</w> <w n="13.3"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="13.4" punct="vg:6">f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>d<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="a" stanza="6" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				<l n="14" num="2.2" lm="6" met="6"><w n="14.1">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="14.2">d<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>t</w> <w n="14.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="14.4" punct="pv:6">c<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg><pgtc id="8" weight="2" schema="CR" part="1">d<rhyme label="b" id="8" gender="m" type="a" stanza="6" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="pv">er</seg></rhyme></pgtc></w> ;</l>
				<l n="15" num="2.3" lm="6" met="6"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="15.2">c</w>'<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="15.6">pl<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>s</w> <w n="15.7">b<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="e" stanza="6" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
				<l n="16" num="2.4" lm="6" met="6"><w n="16.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rs</w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="16.3" punct="pt:6">c<seg phoneme="o" type="vs" value="1" rule="434" place="4">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg><pgtc id="8" weight="2" schema="CR" part="1">d<rhyme label="b" id="8" gender="m" type="e" stanza="6" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="pt">er</seg></rhyme></pgtc></w>.</l>
			</lg>
		</div>
		<div type="section" n="3">
			<head type="main">DEUXIÈME COUPLET.</head> 
			<lg n="1" type="regexp" rhyme="a">
			<head type="speaker">HENRIETTE.</head>
				<l n="17" num="1.1" lm="6" met="6"><w n="17.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>t</w> <w n="17.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.4">j<pgtc id="9" weight="12" schema="V[CVCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></pgtc></w> <w n="17.5" punct="ps:6"><pgtc id="9" weight="12" schema="V[CVCR" part="2">d<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>s<rhyme label="a" id="9" gender="f" type="a" stanza="7" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="ps">e</seg></rhyme></pgtc></w>…</l>
			</lg>
			<lg n="2" type="regexp" rhyme="a">
			<head type="speaker">LE GRAND-DUC.</head>
				<l n="18" num="2.1" lm="6" met="6"><w n="18.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>t</w> <w n="18.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3">qu</w>'<w n="18.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ll<pgtc id="9" weight="12" schema="V[CVCR" part="1"><seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></pgtc></w> <w n="18.5" punct="ps:6"><pgtc id="9" weight="12" schema="V[CVCR" part="2">d<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>s<rhyme label="a" id="9" gender="f" type="e" stanza="7" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="ps">e</seg></rhyme></pgtc></w>…</l>
			</lg>
			<lg n="3" type="regexp" rhyme="a">
			<head type="speaker">HENRIETTE.</head>
				<l n="19" num="3.1" lm="6" met="6"><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">s<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>l</w> <w n="19.3">v<seg phoneme="ø" type="vs" value="1" rule="247" place="3">œu</seg></w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="19.5">m<pgtc id="10" weight="6" schema="V[CR" part="1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></pgtc></w> <w n="19.6" punct="ps:6"><pgtc id="10" weight="6" schema="V[CR" part="2">c<rhyme label="a" id="10" gender="m" type="a" stanza="8" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="248" place="6" punct="ps">œu</seg>r</rhyme></pgtc></w>…</l>
			</lg>
			<lg n="4" type="regexp" rhyme="a">
			<head type="speaker">LE GRAND-DUC.</head>
				<l n="20" num="4.1" lm="6" met="6"><w n="20.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">s<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>l</w> <w n="20.3">v<seg phoneme="ø" type="vs" value="1" rule="247" place="3">œu</seg></w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="20.5">s<pgtc id="10" weight="6" schema="V[CR" part="1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></pgtc></w> <w n="20.6" punct="ps:6"><pgtc id="10" weight="6" schema="V[CR" part="2">c<rhyme label="a" id="10" gender="m" type="e" stanza="8" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="248" place="6" punct="ps">œu</seg>r</rhyme></pgtc></w>…</l>
			</lg>
			<lg n="5" type="regexp" rhyme="a">
			<head type="speaker">HENRIETTE.</head>
				<l n="21" num="5.1" lm="6" met="6"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="3">en</seg>s</w> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="21.4"><pgtc id="11" weight="8" schema="[CV[CR" part="1">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></pgtc></w> <w n="21.5" punct="ps:6"><pgtc id="11" weight="8" schema="[CV[CR" part="2">d<rhyme label="a" id="11" gender="f" type="a" stanza="9" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="ps">e</seg></rhyme></pgtc></w>…</l>
			</lg>
			<lg n="6" type="regexp" rhyme="a">
			<head type="speaker">LE GRAND-DUC.</head>
				<l n="22" num="6.1" lm="6" met="6"><w n="22.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="22.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>t</w> <w n="22.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4">en</seg></w> <w n="22.4"><pgtc id="11" weight="8" schema="[CV[CR" part="1">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></pgtc></w> <w n="22.5" punct="ps:6"><pgtc id="11" weight="8" schema="[CV[CR" part="2">d<rhyme label="a" id="11" gender="f" type="e" stanza="9" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="ps">e</seg></rhyme></pgtc></w>…</l>
			</lg>
			<lg n="7" type="regexp" rhyme="a">
			<head type="speaker">HENRIETTE</head>
				<l n="23" num="7.1" lm="6" met="6"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="23.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="23.3" punct="vg:3">s<seg phoneme="œ" type="vs" value="1" rule="406" place="3" punct="vg">eu</seg>l</w>, <w n="23.4" punct="pt:6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="383" place="5">ei</seg>gn<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="a" id="12" gender="m" type="a" stanza="10" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="pt">eu</seg>r</rhyme></pgtc></w>.</l>
			</lg>
			<lg n="8" type="regexp" rhyme="a">
			<head type="speaker">LE GRAND-DUC.</head>
				<l n="24" num="8.1" lm="6" met="6"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="24.2">m<seg phoneme="wa" type="vs" value="1" rule="422" place="2">oi</seg></w> <w n="24.3" punct="ps:3">s<seg phoneme="œ" type="vs" value="1" rule="406" place="3" punct="ps">eu</seg>l</w>… <w n="24.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4">e</seg>l</w> <w n="24.5" punct="pe:6">b<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>nh<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="a" id="12" gender="m" type="e" stanza="10" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="pe">eu</seg>r</rhyme></pgtc></w> !</l>
				<stage>( Il fait signe au Surintendant de s'éloigner.)</stage>
			</lg> 
		</div>
		<div type="section" n="4">
			<head type="main">ENSEMBLE.</head>
			<lg n="1" type="regexp" rhyme="abab">
			<head type="speaker">HENRIETTE.</head>
				<l n="25" num="1.1" lm="6" met="6"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="25.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="25.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="338" place="3">a</seg>y<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="25.4">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rt<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="f" type="a" stanza="11" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
				<l n="26" num="1.2" lm="6" met="6"><w n="26.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">su<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="26.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="26.5" punct="pt:6">c<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg><pgtc id="14" weight="2" schema="CR" part="1">d<rhyme label="b" id="14" gender="m" type="a" stanza="11" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="pt">er</seg></rhyme></pgtc></w>.</l>
				<l n="27" num="1.3" lm="6" met="6"><w n="27.1"><seg phoneme="y" type="vs" value="1" rule="452" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="27.2">f<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="27.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>v<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="f" type="e" stanza="11" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
				<l n="28" num="1.4" lm="6" met="6"><w n="28.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="2">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="28.3" punct="pt:6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg><pgtc id="14" weight="2" schema="CR" part="1">d<rhyme label="b" id="14" gender="m" type="e" stanza="11" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="pt">er</seg></rhyme></pgtc></w>.</l>
			</lg>
			<lg n="2" type="regexp" rhyme="abab">
			<head type="speaker">LE GRAND-DUC.</head>
				<l n="29" num="2.1" lm="6" met="6"><w n="29.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="29.2">v<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>s</w> <w n="29.3"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="29.4" punct="vg:6">f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>d<pgtc id="15" weight="0" schema="R" part="1"><rhyme label="a" id="15" gender="f" type="a" stanza="12" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				<l n="30" num="2.2" lm="6" met="6"><w n="30.1">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="30.2">d<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>t</w> <w n="30.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="30.4" punct="vg:6">c<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg><pgtc id="16" weight="2" schema="CR" part="1">d<rhyme label="b" id="16" gender="m" type="a" stanza="12" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="vg">er</seg></rhyme></pgtc></w>,</l>
				<l n="31" num="2.3" lm="6" met="6"><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="31.2">c</w>'<w n="31.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="31.4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="31.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="31.6">pl<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>s</w> <w n="31.7">b<pgtc id="15" weight="0" schema="R" part="1"><rhyme label="a" id="15" gender="f" type="e" stanza="12" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
				<l n="32" num="2.4" lm="6" met="6"><w n="32.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rs</w> <w n="32.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="32.3" punct="pt:6">c<seg phoneme="o" type="vs" value="1" rule="434" place="4">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg><pgtc id="16" weight="2" schema="CR" part="1">d<rhyme label="b" id="16" gender="m" type="e" stanza="12" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="pt">er</seg></rhyme></pgtc></w>.</l>
			</lg> 
		</div>
</div></body></text></TEI>