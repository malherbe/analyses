<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB43" modus="cp" lm_max="10" metProfile="8, 4+6" form="suite de strophes" schema="2[abab]" er_moy="0.5" er_max="2" er_min="0" er_mode="0(3/4)" er_moy_et="0.87" qr_moy="0.0" qr_max="C0" qr_mode="0(4/4)" qr_moy_et="0.0">
	<head type="tune">AIR du Partage de la richesse.</head>
	<lg n="1" type="regexp" rhyme="ab">
		<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="1.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="P">a</seg>r</w> <w n="1.3" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg" caesura="1">a</seg>rd</w>,<caesura></caesura> <w n="1.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="C">on</seg></w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="6" mp="M">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-27" place="8" mp="F">e</seg></w> <w n="1.6" punct="vg:10">H<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg>ri<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
		<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fm">e</seg></w>-<w n="2.2">m<seg phoneme="wa" type="vs" value="1" rule="422" place="3">oi</seg></w> <w n="2.3" punct="ps:4">s<seg phoneme="œ" type="vs" value="1" rule="406" place="4" punct="ps" caesura="1">eu</seg>l</w>…<caesura></caesura> <w n="2.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="C">i</seg>l</w> <w n="2.5">f<seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>t</w> <w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.7" punct="pt:10">d<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>s<pgtc id="2" weight="2" schema="CR">cr<rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="10" punct="pt">e</seg>t</rhyme></pgtc></w>.</l>
	</lg>
	<lg n="2" type="regexp" rhyme="ababab">
	<head type="speaker">HENRIETTE.</head>
		<l n="3" num="2.1" lm="10" met="4+6"><w n="3.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="443" place="1" punct="pe">O</seg>h</w> ! <w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg></w> <w n="3.3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="4" caesura="1">oi</seg></w><caesura></caesura> <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>t</w> <w n="3.5">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="3.6">m</w>'<w n="3.7" punct="pt:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" mp="M">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
		<l n="4" num="2.2" lm="10" met="4+6"><w n="4.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="397" place="2" punct="vg">eu</seg></w>, <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="4.3" punct="vg:4">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4" punct="vg" caesura="1">o</seg>rs</w>,<caesura></caesura> <w n="4.4">pu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="4.5">c</w>'<w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="4.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="8" mp="C">un</seg></w> <w n="4.8" punct="pt:10">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg><pgtc id="2" weight="2" schema="CR">cr<rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="10" punct="pt">e</seg>t</rhyme></pgtc></w>.</l>
		<l n="5" num="2.3" lm="8" met="8"><w n="5.1">J</w>'<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="5.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>rs</w> <w n="5.4">r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ct<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="5.6" punct="pv:8">v<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="o" type="vs" value="1" rule="414" place="8">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></pgtc></w> ;</l>
		<l n="6" num="2.4" lm="8" met="8"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="6.2">d<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>ch<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w>-<w n="6.3" punct="vg:5">v<seg phoneme="u" type="vs" value="1" rule="424" place="5" punct="vg">ou</seg>s</w>, <w n="6.4">s</w>'<w n="6.5"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>l</w> <w n="6.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="6.7" punct="pt:8">pl<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pt">aî</seg>t</rhyme></pgtc></w>.</l>
		<l n="7" num="2.5" lm="10" met="4+6"><w n="7.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="7.3">m<seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4" caesura="1">en</seg>ts</w><caesura></caesura> <w n="7.4"><seg phoneme="u" type="vs" value="1" rule="425" place="5">où</seg></w> <w n="7.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="7.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="C">ou</seg>s</w> <w n="7.7">l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="P">à</seg></w> <w n="7.9">d</w>'<w n="7.10"><pgtc id="3" weight="0" schema="[R"><rhyme label="a" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="o" type="vs" value="1" rule="317" place="10">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></pgtc></w></l>
		<l n="8" num="2.6" lm="8" met="8"><w n="8.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>ls</w> <w n="8.5">qu</w>'<w n="8.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="8.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.8" punct="pt:8">f<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pt">ai</seg>t</rhyme></pgtc></w>.</l>
	</lg> 
</div></body></text></TEI>