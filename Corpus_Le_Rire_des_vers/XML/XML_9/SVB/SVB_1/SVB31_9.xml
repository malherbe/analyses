<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB31" modus="cp" lm_max="10" metProfile="8, 4+6" form="strophe unique" schema="1(ababcdccd)" er_moy="0.0" er_max="0" er_min="0" er_mode="0(5/5)" er_moy_et="0.0" qr_moy="0.0" qr_max="C0" qr_mode="0(5/5)" qr_moy_et="0.0">
	<head type="tune">Air : Je n'ai pas vu ces bosquets de lauriers.</head>
	<lg n="1" type="neuvain" rhyme="ababcdccd">
		<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="1.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>t</w> <w n="1.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="C">on</seg></w> <w n="1.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">an</seg>g</w><caesura></caesura> <w n="1.5">s</w>'<w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="1.7">gl<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>c<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg></w> <w n="1.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" mp="P">an</seg>s</w> <w n="1.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="C">on</seg></w> <w n="1.10" punct="vg:10">c<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="248" place="10" punct="vg">œu</seg>r</rhyme></pgtc></w>,</l>
		<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="2.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg></w> <w n="2.3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="4" caesura="1">oi</seg></w><caesura></caesura> <w n="2.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="2.5" punct="vg:7">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="vg" mp="F">e</seg></w>, <w n="2.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="2.7" punct="pe:10">fr<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>ss<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe ps" mp="F">e</seg></rhyme></pgtc></w> ! …</l>
		<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">ez</seg></w><caesura></caesura> <w n="3.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="M">on</seg>s<seg phoneme="o" type="vs" value="1" rule="443" place="6" mp="M">o</seg>l<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.6" punct="vg:10">m<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>lh<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
		<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1" punct="vg:1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1" punct="vg">ou</seg>s</w>, <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.3">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="4.4">s<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="P">u</seg>r</w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6" mp="C">e</seg>s</w> <w n="4.6">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="4.7">d<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="C">u</seg></w> <w n="4.8" punct="pv:10">tr<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="414" place="10">ô</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
		<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="C">Au</seg></w> <w n="5.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="2">om</seg></w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="5.4">l<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>s</w><caesura></caesura> <w n="5.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="5.6">qu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="5.7" punct="vg:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="vg">ez</seg></w>, <w n="5.8">s<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="P">u</seg>r</w> <w n="5.9" punct="vg:10">n<pgtc id="2" weight="0" schema="R"><rhyme label="c" id="2" gender="m" type="a" qr="E0"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="vg">ou</seg>s</rhyme></pgtc></w>,</l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1">É</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="y" type="vs" value="1" rule="452" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg></w> <w n="6.4" punct="vg:8">pr<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>ctr<pgtc id="3" weight="0" schema="R"><rhyme label="d" id="3" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">N</w>'<w n="7.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="7.3" punct="ps:4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="ps" caesura="1">a</seg>s</w>…<caesura></caesura> <w n="7.4" punct="vg:5">v<seg phoneme="u" type="vs" value="1" rule="424" place="5" punct="vg">ou</seg>s</w>, <w n="7.5">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="7" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r</w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="P">à</seg></w> <w n="7.8" punct="pv:10">t<pgtc id="2" weight="0" schema="R"><rhyme label="c" id="2" gender="m" type="e" qr="E0"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pv">ou</seg>s</rhyme></pgtc></w> ;</l>
		<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="8.2">c</w>'<w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3" mp="C">un</seg></w> <w n="8.5" punct="vg:4">pi<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="vg" caesura="1">é</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5" mp="C">un</seg></w> <w n="8.7" punct="ps:7">cr<seg phoneme="i" type="vs" value="1" rule="466" place="6">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="ps" mp="F">e</seg></w>… <w n="8.8">c</w>'<w n="8.9"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="8">e</seg>st</w> <w n="8.10"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="P">à</seg></w> <w n="8.11">v<pgtc id="2" weight="0" schema="R"><rhyme label="c" id="2" gender="m" type="a" qr="E0"><seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>s</rhyme></pgtc></w></l>
		<l n="9" num="1.9" lm="8" met="8"><w n="9.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">j</w>'<w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="9.4">d<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>s</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="9.6" punct="pe:8">j<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>st<pgtc id="3" weight="0" schema="R"><rhyme label="d" id="3" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
	</lg>
</div></body></text></TEI>