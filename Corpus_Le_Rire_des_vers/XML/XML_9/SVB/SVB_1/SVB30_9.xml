<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB30" modus="cp" lm_max="9" metProfile="8, 4, 3÷6" form="suite de strophes" schema="2[ababb] 2[abab] 2[aabab]" er_moy="7.31" er_max="40" er_min="0" er_mode="0(7/16)" er_moy_et="11.28" qr_moy="0.0" qr_max="C0" qr_mode="0(16/16)" qr_moy_et="0.0">
	<head type="tune">Air : Verse, verse le vin de France (d'Adam).</head>
		<div type="section" n="1">
			<lg n="1" type="regexp" rhyme="a">
				<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:2">V<seg phoneme="wa" type="vs" value="1" rule="439" place="1">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="1.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.3">t</w>'<w n="1.4"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="6">en</seg>t</w> <w n="1.6" punct="pt:8">l<seg phoneme="u" type="vs" value="1" rule="d-2" place="7">ou</seg><pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="490" place="8" punct="pt">i</seg>s</rhyme></pgtc></w>.</l>
			</lg>
			<lg n="2" type="regexp" rhyme="babb">
				<head type="speaker">HÉLÈNE.</head>
				<l n="2" num="2.1" lm="8" met="8"><w n="2.1">C<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="1">en</seg>t</w> <w n="2.2" punct="pe:3">l<seg phoneme="u" type="vs" value="1" rule="d-2" place="2">ou</seg><seg phoneme="i" type="vs" value="1" rule="490" place="3" punct="pe">i</seg>s</w> ! <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="5">è</seg>s</w> <w n="2.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>t</w> <w n="2.5">d</w>'<w n="2.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg><pgtc id="2" weight="2" schema="CR">l<rhyme label="b" id="2" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
				<l n="3" num="2.2" lm="8" met="8"><w n="3.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>t</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">ch<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">in</seg>s</w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="3.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>t</w> <w n="3.6">d</w>'<w n="3.7" punct="pv:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="358" place="7">en</seg>nu<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="490" place="8" punct="pv">i</seg>s</rhyme></pgtc></w> ;</l>
				<l n="4" num="2.3" lm="8" met="8"><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w>-<w n="4.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="4.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>c</w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="4.5">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="5">a</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="4.6">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="4.7" punct="vg:8"><pgtc id="2" weight="2" schema="[CR">l<rhyme label="b" id="2" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
				<l n="5" num="2.4" lm="4" met="4"><w n="5.1"><pgtc id="2" weight="19" schema="CVGV[CV[CR" part="1">P<seg phoneme="ɛ" type="vs" value="1" rule="338" place="1">a</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="2">er</seg></pgtc></w> <w n="5.2"><pgtc id="2" weight="19" schema="CVGV[CV[CR" part="2">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</pgtc></w> <w n="5.3" punct="pi:4"><pgtc id="2" weight="19" schema="CVGV[CV[CR" part="3">l<rhyme label="b" id="2" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pi">e</seg>s</rhyme></pgtc></w> ?</l>
			</lg>
			<lg n="3" type="regexp" rhyme="">
				<head type="speaker">DUBOIS.</head>
				<l part="I" n="6" num="3.1" lm="8" met="8"><w n="6.1">Tr<seg phoneme="wa" type="vs" value="1" rule="419" place="1">oi</seg>s</w> <w n="6.2" punct="pe:2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="2" punct="pe">en</seg>ts</w> ! </l>
			</lg>
			<lg n="4" type="regexp" rhyme="aba">
				<head type="speaker">HÉLÈNE.</head>
				<l part="F" n="6" lm="8" met="8"><w n="6.3">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="3">e</seg>l</w> <w n="6.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w>-<w n="6.6">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="7">e</seg></w> <w n="6.7" punct="pi:8">l<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="8" punct="pi">à</seg></rhyme></pgtc></w> ?</l>
				<l n="7" num="4.1" lm="8" met="8"><w n="7.1">L</w>'<w n="7.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="7.4">tr<seg phoneme="o" type="vs" value="1" rule="432" place="3">o</seg>p</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.6">fi<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="7.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</w> <w n="7.8">l</w>'<w n="7.9" punct="pv:8"><pgtc id="4" weight="0" schema="[R" part="1"><rhyme label="b" id="4" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
				<l n="8" num="4.2" lm="9" met="3+6"><w n="8.1">J<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="8.2">l</w>'<w n="8.3"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="3" caesura="1">o</seg>r</w><caesura></caesura> <w n="8.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="8.5"><seg phoneme="y" type="vs" value="1" rule="452" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fc">e</seg></w> <w n="8.6" punct="pt:9">s<seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>du<seg phoneme="i" type="vs" value="1" rule="490" place="8" mp="M">i</seg>r<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="9" punct="pt">a</seg></rhyme></pgtc></w>.</l>
			</lg> 
			<lg n="5" type="regexp" rhyme="b">
				<head type="speaker">DUBOIS.</head>
				<l n="9" num="5.1" lm="8" met="8"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="9.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="9.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="6" punct="vg">o</seg>r</w>, <w n="9.5" punct="pt:8">M<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>d<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
			</lg> 
			<lg n="6" type="regexp" rhyme="a">
				<head type="speaker">HÉLÈNE.</head>
				<l n="10" num="6.1" lm="4" met="4"><w n="10.1" punct="vg:2">J<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2" punct="vg">ai</seg>s</w>, <w n="10.2" punct="pe:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg><pgtc id="5" weight="2" schema="CR" part="1">f<rhyme label="a" id="5" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></rhyme></pgtc></w> !</l>
			</lg>
			<lg n="7" type="regexp" rhyme="">
			<head type="speaker">DUBOIS.</head>
				<l part="I" n="11" num="7.1" lm="8" met="8"><w n="11.1">C<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">in</seg>q</w> <w n="11.2" punct="pe:2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="2" punct="pe">en</seg>ts</w> ! </l>
			</lg>
			<lg n="8" type="regexp" rhyme="ab">
				<head type="speaker">HÉLÈNE.</head>
				<l part="F" n="11" lm="8" met="8"><w n="11.3" punct="pv:3">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="pv">on</seg></w> ; <w n="11.4">l</w>'<w n="11.5">h<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>r</w> <w n="11.6">d</w>'<w n="11.7"><seg phoneme="y" type="vs" value="1" rule="452" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="11.8" punct="vg:8"><pgtc id="5" weight="2" schema="[CR" part="1">f<rhyme label="a" id="5" gender="f" type="e" stanza="3" qr="C0"><seg phoneme="a" type="vs" value="1" rule="192" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				<l n="12" num="8.1" lm="8" met="8"><w n="12.1">Cr<seg phoneme="wa" type="vs" value="1" rule="439" place="1">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="12.2" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="3" punct="vg">oi</seg></w>, <w n="12.3">v<seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>t</w> <w n="12.4">mi<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="12.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.6" punct="pt:8"><pgtc id="6" weight="8" schema="[CVCR" part="1">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="b" id="6" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pt">a</seg></rhyme></pgtc></w>.</l>
			</lg> 
			<lg n="9" type="regexp" rhyme="ab">
				<head type="speaker">DUBOIS.</head>
				<l n="13" num="9.1" lm="8" met="8"><w n="13.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="vg">on</seg></w>, <w n="13.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="13.3" punct="vg:3">f<seg phoneme="wa" type="vs" value="1" rule="422" place="3" punct="vg">oi</seg></w>, <w n="13.4">l</w>'<w n="13.5">h<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>r</w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.7">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="13.8" punct="vg:8"><pgtc id="5" weight="2" schema="[CR" part="1">f<rhyme label="a" id="5" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="a" type="vs" value="1" rule="192" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				<l n="14" num="9.2" lm="8" met="8"><w n="14.1">J</w>'<w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">en</seg></w> <w n="14.3" punct="vg:3">r<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="vg">on</seg>ds</w>, <w n="14.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.5">v<seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>t</w> <w n="14.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>s</w> <w n="14.7" punct="pt:8"><pgtc id="6" weight="8" schema="[CVCR" part="1">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="b" id="6" gender="m" type="e" stanza="3" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pt">a</seg></rhyme></pgtc></w>.</l>
			</lg> 
		</div>
		<div type="section" n="2">
			<head type="tune">Même air.</head>
			<lg n="1" type="regexp" rhyme="ab">
				<l n="15" num="1.1" lm="8" met="8"><w n="15.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="15.2">p<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg></w> <w n="15.3">t<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="15.4">v<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>s</w> <w n="15.5">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="15.6" punct="vg:8">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rr<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="m" type="a" stanza="4" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>rs</rhyme></pgtc></w>,</l>
				<l n="16" num="1.2" lm="8" met="8"><w n="16.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="16.2">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="2">ou</seg><seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>s</w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="16.5" punct="pt:8"><pgtc id="8" weight="12" schema="[VCVCR" part="1"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t<rhyme label="b" id="8" gender="f" type="a" stanza="4" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l> 
				<l part="I" n="17" num="1.3" lm="8" met="8"><w n="17.1">V<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="17.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r</w> <w n="17.3">s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>x</w> <w n="17.4" punct="ps:4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4" punct="ps">en</seg>ts</w>… </l>
			</lg>
			<lg n="2" type="regexp" rhyme="ab">
				<head type="speaker">HÉLÈNE.</head>
				<l part="F" n="17" lm="8" met="8"><w n="17.5">T<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="17.6">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="17.7">h<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>nn<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="m" type="e" stanza="4" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>rs</rhyme></pgtc></w></l>
				<l n="18" num="2.1" lm="8" met="8"><w n="18.1">V<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="18.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="18.3">t<seg phoneme="wa" type="vs" value="1" rule="422" place="4">oi</seg></w> <w n="18.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="5">en</seg></w> <w n="18.5" punct="pt:8">d<pgtc id="8" weight="12" schema="VCVCR" part="1"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t<rhyme label="b" id="8" gender="f" type="e" stanza="4" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
			</lg>
			<lg n="3" type="regexp" rhyme="b">
				<head type="speaker">DUBOIS.</head>
				<l n="19" num="3.1" lm="4" met="4"><w n="19.1" punct="pe:2">M<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rbl<seg phoneme="ø" type="vs" value="1" rule="397" place="2" punct="pe">eu</seg></w> ! <w n="19.2">j</w>'<w n="19.3" punct="pe:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>r<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="f" type="a" stanza="4" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				<l part="I" n="20" num="3.2" lm="8" met="8"><w n="20.1">Hu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg>t</w> <w n="20.2" punct="ps:2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="2" punct="ps">en</seg>ts</w>… </l>
			</lg>
			<lg n="4" type="regexp" rhyme="a">
				<head type="speaker">HÉLÈNE, hésitant.</head>
				<l part="F" n="20" lm="8" met="8"><w n="20.3" punct="ps:3">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="ps">on</seg></w>… <del reason="analysis" type="stage" hand="KL">(À part.)</del> <w n="20.4">C</w>'<w n="20.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="20.6">j<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="20.7" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>j<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="m" type="a" stanza="5" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="8" punct="pt">à</seg></rhyme></pgtc></w>.</l>
			</lg>
			<lg n="5" type="regexp" rhyme="b">
				<head type="speaker">DUBOIS.</head>
				<l n="21" num="5.1" lm="8" met="8"><w n="21.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="21.2" punct="pe:2">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2" punct="pe">e</seg>l</w> ! <w n="21.3">v<seg phoneme="ɛ" type="vs" value="1" rule="63" place="3">e</seg>rs</w> <w n="21.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="21.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>lqu</w>'<w n="21.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="21.7">s</w>'<w n="21.8" punct="pt:8"><pgtc id="10" weight="6" schema="[VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<rhyme label="b" id="10" gender="f" type="a" stanza="5" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				<l part="I" n="22" num="5.2" lm="9" mp4="F" mp6="M" met="9"><w n="22.1" punct="pe:2">M<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="pe ps" mp="F">e</seg></w> ! … </l>
			</lg>
			<lg n="6" type="regexp" rhyme="a">
				<head type="speaker">HÉLÈNE.</head>
				<l part="F" n="22" lm="9" mp4="F" mp6="M" met="9"><w n="22.2" punct="pe:4">M<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pe" mp="F">e</seg></w> ! <w n="22.3">qu</w>'<w n="22.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="22.5" punct="pt:9">g<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="M">an</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>r<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="m" type="e" stanza="5" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="9" punct="pt">a</seg></rhyme></pgtc></w>.</l>
			</lg>
			<lg n="7" type="regexp" rhyme="b">
				<head type="speaker">DUBOIS.</head>
				<l n="23" num="7.1" lm="8" met="8"><w n="23.1">M<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="23.2">l<seg phoneme="u" type="vs" value="1" rule="d-2" place="3">ou</seg><seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg>s</w> <w n="23.3">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="5">a</seg>y<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>s</w> <w n="23.4">d</w>'<w n="23.5" punct="pt:8"><pgtc id="10" weight="6" schema="[VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<rhyme label="b" id="10" gender="f" type="e" stanza="5" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
			</lg> 
			<lg n="8" type="regexp" rhyme="aab">
				<head type="speaker">HÉLÈNE, avec joie.</head>
				<l n="24" num="8.1" lm="4" met="4"><w n="24.1">P<seg phoneme="ɛ" type="vs" value="1" rule="338" place="1">a</seg>y<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>s</w> <w n="24.2">d</w>'<w n="24.3" punct="pt:4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<pgtc id="11" weight="0" schema="R" part="1"><rhyme label="a" id="11" gender="f" type="a" stanza="6" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				<l n="25" num="8.2" lm="8" met="8"><w n="25.1">J</w>'<w n="25.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="25.3" punct="vg:4">c<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg>r</w>, <w n="25.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="25.5" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>sc<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="11" weight="0" schema="R" part="1"><rhyme label="a" id="11" gender="f" type="e" stanza="6" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="377" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				<l n="26" num="8.3" lm="8" met="8"><w n="26.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="2">o</seg>qu<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg></w> <w n="26.3"><pgtc id="12" weight="26" schema="[CV[CV[CV[CVCR" part="1">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></pgtc></w> <w n="26.4"><pgtc id="12" weight="26" schema="[CV[CV[CV[CVCR" part="2">v<seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>t</pgtc></w> <w n="26.5"><pgtc id="12" weight="26" schema="[CV[CV[CV[CVCR" part="3">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>s</pgtc></w> <w n="26.6" punct="pt:8"><pgtc id="12" weight="26" schema="[CV[CV[CV[CVCR" part="4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="b" id="12" gender="m" type="a" stanza="6" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pt">a</seg></rhyme></pgtc></w>.</l>
			</lg> 
			<lg n="9" type="regexp" rhyme="ab">
				<head type="speaker">DUBOIS.</head>
				<l n="27" num="9.1" lm="8" met="8"><w n="27.1" punct="vg:3"><pgtc id="11" weight="40" schema="VCVCV[CVC[V[CVCVR" part="1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></pgtc></w>, <w n="27.2" punct="vg:4"><pgtc id="11" weight="40" schema="VCVCV[CVC[V[CVCVR" part="2">c<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg>r</pgtc></w>, <w n="27.3"><pgtc id="11" weight="40" schema="VCVCV[CVC[V[CVCVR" part="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></pgtc></w> <w n="27.4" punct="vg:8"><pgtc id="11" weight="40" schema="VCVCV[CVC[V[CVCVR" part="4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>sc<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="11" gender="f" type="a" stanza="6" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="377" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				<l n="28" num="9.2" lm="8" met="8"><w n="28.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="28.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>r<seg phoneme="i" type="vs" value="1" rule="466" place="3">i</seg><pgtc id="12" weight="26" schema="CV[CV[CV[CVCR" part="1">n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></pgtc></w> <w n="28.3"><pgtc id="12" weight="26" schema="CV[CV[CV[CVCR" part="2">v<seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>t</pgtc></w> <w n="28.4"><pgtc id="12" weight="26" schema="CV[CV[CV[CVCR" part="3">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>s</pgtc></w> <w n="28.5" punct="pt:8"><pgtc id="12" weight="26" schema="CV[CV[CV[CVCR" part="4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="b" id="12" gender="m" type="e" stanza="6" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pt">a</seg></rhyme></pgtc></w>.</l>
			</lg>
		</div>
</div></body></text></TEI>