<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB18" modus="sp" lm_max="7" metProfile="6, 7, (4), (3)" form="suite périodique" schema="1(abbaa) 2(abba)" er_moy="0.0" er_max="0" er_min="0" er_mode="0(7/7)" er_moy_et="0.0" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
	<head type="tune">Air : Gai, gai, etc.</head>
	<lg n="1" type="quintil" rhyme="abbaa">
		<l n="1" num="1.1" lm="6" met="6"><w n="1.1" punct="vg:1">G<seg phoneme="ɛ" type="vs" value="1" rule="305" place="1" punct="vg">ai</seg></w>, <w n="1.2" punct="vg:2">g<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2" punct="vg">ai</seg></w>, <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s</w>-<w n="1.4" punct="pe:6">n<pgtc id="1" weight="0" schema="R" part="1"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="pe">ou</seg>s</rhyme></pgtc></w> !</l>
		<l n="2" num="1.2" lm="4"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="2.3">b<seg phoneme="o" type="vs" value="1" rule="314" place="3">eau</seg></w> <w n="2.4">r<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
		<l n="3" num="1.3" lm="3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="3.2" punct="vg:3">m<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>d<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="4" num="1.4" lm="6" met="6"><w n="4.1" punct="vg:1">G<seg phoneme="ɛ" type="vs" value="1" rule="305" place="1" punct="vg">ai</seg></w>, <w n="4.2" punct="vg:2">g<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2" punct="vg">ai</seg></w>, <w n="4.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s</w>-<w n="4.4" punct="vg:6">n<pgtc id="1" weight="0" schema="R" part="1"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="vg">ou</seg>s</rhyme></pgtc></w>,</l>
		<l n="5" num="1.5" lm="7" met="7"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="5.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="5.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="3">en</seg></w> <w n="5.4"><seg phoneme="i" type="vs" value="1" rule="496" place="4">y</seg></w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="5.6" punct="pe:7">t<pgtc id="1" weight="0" schema="R" part="1"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="7" punct="pe">ou</seg>s</rhyme></pgtc></w> !</l> 
	</lg>
	<lg n="2" type="quatrain" rhyme="abba">
		<l n="6" num="2.1" lm="7" met="7"><w n="6.1">L</w>'<w n="6.2">h<seg phoneme="i" type="vs" value="1" rule="496" place="1">y</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="220" place="2">en</seg></w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="6.4">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="6.6" punct="vg:7">d<seg phoneme="y" type="vs" value="1" rule="d-3" place="6">u</seg><pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="443" place="7" punct="vg">o</seg></rhyme></pgtc></w>,</l>
		<l n="7" num="2.2" lm="7" met="7"><w n="7.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="7.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="7.4" punct="ps:7">c<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>mm<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="ps vg">e</seg></rhyme></pgtc></w>…,</l>
		<l n="8" num="2.3" lm="7" met="7"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="8.2">pl<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s</w> <w n="8.3" punct="vg:3">t<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="vg">a</seg>rd</w>, <w n="8.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="8.7">ch<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
		<l n="9" num="2.4" lm="7" met="7"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="9.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="9.5" punct="pt:7">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="443" place="7" punct="pt">o</seg></rhyme></pgtc></w>.</l>
	</lg>
	<lg n="3" rhyme="None">
	<head type="speaker">TOUS.</head>
		<l ana="unanalyzable" n="10" num="3.1">Gai, gai, marions-pous, etc.</l> 
	</lg>
	<lg n="4" type="quatrain" rhyme="abba">
	<head type="speaker">DUPUT, poussant le bras d Desvilleis.</head>
		<l n="11" num="4.1" lm="7" met="7"><w n="11.1" punct="vg:3">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="3" punct="vg">ez</seg></w>, <w n="11.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>s</w> <w n="11.3" punct="vg:7">h<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>r<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="397" place="7" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
		<l n="12" num="4.2" lm="7" met="7"><w n="12.1">Qu</w>'<w n="12.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>l</w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="12.4">s<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4">en</seg>t</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>gr<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg><pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
		<l n="13" num="4.3" lm="7" met="7"><w n="13.1">D</w>'<w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>v<seg phoneme="wa" type="vs" value="1" rule="439" place="2">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="13.3">s<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="13.4">f<seg phoneme="a" type="vs" value="1" rule="192" place="5">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg></w> <w n="13.6" punct="pe:7">di<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></pgtc></w> !</l>
		<l n="14" num="4.4" lm="6" met="6"><w n="14.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>t</w> <w n="14.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>d</w> <w n="14.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="14.5" punct="pe:6">d<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="397" place="6" punct="pe">eu</seg>x</rhyme></pgtc></w> !</l> 
	</lg> 
</div></body></text></TEI>