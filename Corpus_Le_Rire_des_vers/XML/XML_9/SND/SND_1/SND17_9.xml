<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE LIEUTENANT D'ARTILLERIE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="NSL" sort="2">
					<name>
						<forename>Jean-Pierre-Laurent</forename>
						<surname>DENOMBRET</surname>
						<addName type="pen_name">Charles NOMBRET SAINT-LAURENT</addName>
					</name>
					<date from="1791" to="1833">1791-1833</date>
				</author>
					<author key="DUV" sort="3">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>225 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SND_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=NWU:35556007830409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
								<repository>Northwestern university</repository>
								<idno type="URI">https://babel.hathitrust.org/cgi/pt?id=ien.35556007830409</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE V.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SND17" modus="cp" lm_max="10" metProfile="4+6, (8), (9)" form="strophe unique" schema="1(ababbcdcd)" er_moy="0.8" er_max="2" er_min="0" er_mode="0(3/5)" er_moy_et="0.98" qr_moy="0.0" qr_max="C0" qr_mode="0(5/5)" qr_moy_et="0.0">
				<head type="tune">Air des Frères de lait.</head>
				<lg n="1" type="neuvain" rhyme="ababbcdcd">
					<head type="main">LORIS.</head>
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="1">en</seg></w> <w n="1.2">n</w>'<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="1.4" punct="vg:4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="1.5">cr<seg phoneme="wa" type="vs" value="1" rule="439" place="5" mp="M/mp">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="6" mp="Lp">ez</seg></w>-<w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="1.7">m<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="1.8" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>r<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="10">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">su<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="2.6">d<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>v<seg phoneme="i" type="vs" value="1" rule="466" place="8" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg></rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="9"><w n="3.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="1">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="3.2" punct="pv:4">f<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4" punct="pv">en</seg>t</w> ; <w n="3.3">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s</w> <w n="3.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg></w> <w n="3.5">d</w>'<w n="3.6" punct="dp:9"><seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>d<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="9">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
					<l n="4" num="1.4" lm="8"><space unit="char" quantity="4"></space><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="4.2" punct="vg:3">mi<seg phoneme="ɛ" type="vs" value="1" rule="365" place="2">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="4.3">c</w>'<w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="4.6" punct="vg:8">l<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>r<pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="vg">é</seg></rhyme></pgtc></w>,</l>
					<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">J</w>'<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="5.3">pr<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>s</w> <w n="5.4">l</w>'<w n="5.5">h<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="P">à</seg></w> <w n="5.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" mp="C">on</seg></w> <w n="5.8">c<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>lt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.9" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>c<pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="pt">é</seg></rhyme></pgtc></w>.</l>
					<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="C">Au</seg></w> <w n="6.2">li<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg></w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="6.4" punct="vg:4">m<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="1">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">em</seg>pl<seg phoneme="wa" type="vs" value="1" rule="439" place="6" mp="M">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t</w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="6.7" punct="vg:10">m<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>st<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">on</seg></w> <w n="7.2">t<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="2">ein</seg>t</w> <w n="7.3" punct="vg:4">br<seg phoneme="y" type="vs" value="1" rule="452" place="3" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="C">u</seg></w> <w n="7.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6">e</seg>l</w> <w n="7.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>t</w> <w n="7.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="C">on</seg></w> <w n="7.8" punct="pv:10">f<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="pv">a</seg>rd</rhyme></pgtc></w> ;</l>
					<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="8.2" punct="vg:4">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="189" place="4" punct="vg" caesura="1">e</seg>t</w>,<caesura></caesura> <w n="8.3">j</w>'<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg></w> <w n="8.5">pr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>s</w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="8.7" punct="vg:10">s<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>br<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>d<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="9" num="1.9" lm="10" met="4+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="9.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="P">ou</seg>r</w> <w n="9.3" punct="vg:4">s<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">a</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5" mp="C">un</seg></w> <w n="9.5">d<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" mp="M">o</seg>lm<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg></w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="9.7" punct="pt:10">h<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>z<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="pt">a</seg>rd</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>