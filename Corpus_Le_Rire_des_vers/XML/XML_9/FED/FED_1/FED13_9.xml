<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANDRÉ LE CHANSONNIER</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="FTN" sort="1">
					<name>
						<forename>Louis Marie</forename>
						<surname>FONTAN</surname>
					</name>
					<date from="1801" to="1839">1801-1839</date>
				</author>
				<author key="DNY" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>DESNOYER</surname>
					</name>
					<date from="1806" to="1858">1806-1858</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>239 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>André le chansonnier.</title>
						<author>FONTAN ET DESNOYER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=aN6oyNpF3cMC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>André le chansonnier.</title>
								<author>FONTAN ET DESNOYER</author>
								<repository>Biblioteca Casanatense</repository>
								<idno type="URI">http://opac.casanatense.it/Record.htm?idlist=3</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Ode et Wodon</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-15" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE SECOND.</head><head type="main_subpart">SCÈNE III.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FED13" modus="sp" lm_max="8" metProfile="8, 4, 6, 3" form="suite de strophes" schema="1[abab] 1[aaa] 2[aa] 1[abba]" er_moy="2.75" er_max="20" er_min="0" er_mode="0(6/8)" er_moy_et="6.55" qr_moy="0.0" qr_max="C0" qr_mode="0(8/8)" qr_moy_et="0.0">
				<head type="tune">Air du troisième acte de Gillette. (M. Béancourt.)</head>
				<lg n="1" type="regexp" rhyme="ababaaaaaabbaaa">
					<head type="main">LOUISE. Elle a descendu le théâtre sans les voir.</head>
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="1.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="1.3" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>r</w>, <w n="1.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="1.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.6">l</w>'<w n="1.7" punct="vg:8"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>r<pgtc id="1" weight="2" schema="CR">d<rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="1">oin</seg></w> <w n="2.2">d</w>'<w n="2.3"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="2.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.5">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.6">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="2.7" punct="pv:8">p<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pv">a</seg>s</rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="3.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="3.3">l</w>'<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>r</w> <w n="3.5">m</w>'<w n="3.6" punct="ps:8"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg><pgtc id="1" weight="2" schema="CR">d<rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></pgtc></w> …</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg>s</w>, <w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="4.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6" punct="vg">ai</seg></w>-<w n="4.4" punct="vg:6">j<seg phoneme="ə" type="ee" value="0" rule="e-18">e</seg></w>, <w n="4.5" punct="pe:8">h<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>l<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pe">a</seg>s</rhyme></pgtc></w> !</l>
					<l n="5" num="1.5" lm="4" met="4"><space unit="char" quantity="8"></space><w n="5.1"><pgtc id="3" weight="20" schema="CVCVC[VCR" part="1">D<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r</pgtc></w> <w n="5.2" punct="pe:4"><pgtc id="3" weight="20" schema="CVCVC[VCR" part="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>xtr<rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></rhyme></pgtc></w> ! <del reason="analysis" type="repetition" hand="LG">(bis.)</del></l>
					<l n="6" num="1.6" lm="4" met="4"><space unit="char" quantity="8"></space><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="6.1"><pgtc id="3" weight="20" schema="CVCVC[VCR" part="1">D<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r</pgtc></w> <w n="6.2" punct="pe:4"><pgtc id="3" weight="20" schema="CVCVC[VCR" part="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>xtr<rhyme label="a" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></rhyme></pgtc></w> !</add></subst></l>
					<l n="7" num="1.7" lm="6" met="6"><space unit="char" quantity="4"></space><w n="7.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="1">oin</seg></w> <w n="7.2">d<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="7.3">b<seg phoneme="o" type="vs" value="1" rule="314" place="3">eau</seg></w> <w n="7.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4">e</seg>l</w> <w n="7.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.6">j</w>'<w n="7.7" punct="vg:6"><pgtc id="3" weight="0" schema="[R" part="1"><rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="6">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="1.8" lm="6" met="6"><space unit="char" quantity="4"></space><w n="8.1">F<seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>t</w>-<w n="8.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="8.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>g</w>-<w n="8.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">em</seg>ps</w> <w n="8.5" punct="pi:6">s<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>ffr<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="a" id="4" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="pi">i</seg>r</rhyme></pgtc></w> ?</l>
					<l n="9" num="1.9" lm="6" met="6"><space unit="char" quantity="4"></space><w n="9.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="9.2">v<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="9.3">mi<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="9.4" punct="pe:6">m<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="a" id="4" gender="m" type="e" stanza="3" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="pe">i</seg>r</rhyme></pgtc></w> !</l>
					<l n="10" num="1.10" lm="6" met="6"><space unit="char" quantity="4"></space><w n="10.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="vg">on</seg></w>, <w n="10.2">g<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="10.3">l</w>'<w n="10.4" punct="pv:6"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>r<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="a" stanza="4" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="11" num="1.11" lm="3" met="3"><space unit="char" quantity="10"></space><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2" punct="vg:3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="a" stanza="4" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="vg">ou</seg>r</rhyme></pgtc></w>,</l>
					<l n="12" num="1.12" lm="3" met="3"><space unit="char" quantity="10"></space><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="12.2">s<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>l</w> <w n="12.3" punct="vg:3">j<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="e" stanza="4" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="vg">ou</seg>r</rhyme></pgtc></w>,</l>
					<l n="13" num="1.13" lm="6" met="6"><space unit="char" quantity="4"></space><w n="13.1">Pu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg></w>-<w n="13.2">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="3">e</seg></w> <w n="13.3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>r</w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="13.5" punct="vg:6">Fr<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="e" stanza="4" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="1.14" lm="3" met="3"><space unit="char" quantity="10"></space><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="14.2">m<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="m" type="a" stanza="5" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r</rhyme></pgtc></w></l>
					<l n="15" num="1.15" lm="3" met="3"><space unit="char" quantity="10"></space><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2" punct="pt:3">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="m" type="e" stanza="5" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="pt">i</seg>r</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>