<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANDRÉ LE CHANSONNIER</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="FTN" sort="1">
					<name>
						<forename>Louis Marie</forename>
						<surname>FONTAN</surname>
					</name>
					<date from="1801" to="1839">1801-1839</date>
				</author>
				<author key="DNY" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>DESNOYER</surname>
					</name>
					<date from="1806" to="1858">1806-1858</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>239 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>André le chansonnier.</title>
						<author>FONTAN ET DESNOYER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=aN6oyNpF3cMC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>André le chansonnier.</title>
								<author>FONTAN ET DESNOYER</author>
								<repository>Biblioteca Casanatense</repository>
								<idno type="URI">http://opac.casanatense.it/Record.htm?idlist=3</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Ode et Wodon</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-15" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE I</head><head type="main_subpart">SCÈNE VIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FED7" modus="cp" lm_max="10" metProfile="8, 4+6" form="suite de strophes" schema="1[abbab] 1[aa] 1[abaab] 1[aaa]" er_moy="1.33" er_max="2" er_min="0" er_mode="2(5/9)" er_moy_et="0.82" qr_moy="0.0" qr_max="C0" qr_mode="0(9/9)" qr_moy_et="0.0">
				<head type="tune">AIR de la Vieille.</head>
					<lg n="1" type="regexp" rhyme="abbabaaabaabaaa">
						<head type="main">ANDRÉ.</head>
						<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="1.1">T<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">d</w>'<w n="1.3" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>x<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>l</w>, <w n="1.4">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="1.6">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="1.7" punct="vg:8"><pgtc id="1" weight="1" schema="GR">y<rhyme label="a" id="1" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="2.1" punct="vg:3">M<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="1">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">an</seg>t</w>, <w n="2.2">t<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="2.3">t</w>'<w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="49" place="5">e</seg>s</w> <w n="2.5" punct="pe:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">em</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg><pgtc id="2" weight="2" schema="CR">ll<rhyme label="b" id="2" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">d<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>x</w> <w n="3.3">s<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>r</w> <w n="3.4">d</w>'<w n="3.5"><seg phoneme="e" type="vs" value="1" rule="408" place="6">É</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="2" weight="2" schema="CR">l<rhyme label="b" id="2" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="4.1">V<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="4.2">m</w>'<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">om</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="4.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="4.6" punct="pt:8">l<pgtc id="1" weight="1" schema="GR">i<rhyme label="a" id="1" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
						<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="4"></space><w n="5.1" punct="vg:2">Pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg>rs</w>, <w n="5.2" punct="vg:4">g<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="vg">é</seg></w>, <w n="5.3">v<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.4" punct="vg:8">f<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg><pgtc id="2" weight="2" schema="CR">l<rhyme label="b" id="2" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="4"></space><w n="6.1">V<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>lt<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="6.2">s<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>r</w> <w n="6.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="6.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>t</w> <w n="6.5" punct="vg:8">j<seg phoneme="wa" type="vs" value="1" rule="439" place="7">o</seg><pgtc id="3" weight="1" schema="GR">y<rhyme label="a" id="3" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="4"></space><w n="7.1" punct="vg:2">G<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="2" punct="vg">é</seg></w>, <w n="7.2" punct="vg:4">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>rs</w>, <w n="7.3">s<seg phoneme="wa" type="vs" value="1" rule="439" place="5">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="7.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="7.5" punct="pt:8">d<pgtc id="3" weight="1" schema="GR">i<rhyme label="a" id="3" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
						<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">Fl<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>rs</w> <w n="8.2">d<seg phoneme="y" type="vs" value="1" rule="449" place="2" mp="C">u</seg></w> <w n="8.3" punct="vg:4">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3" mp="M">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4" punct="vg" caesura="1">em</seg>ps</w>,<caesura></caesura> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M/mp">em</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6" mp="M/mp">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="7" mp="Lp">ez</seg></w>-<w n="8.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>s</w> <w n="8.6">d</w>'<w n="8.7" punct="pv:10"><seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>cl<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="10">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="9" num="1.9" lm="10" met="4+6"><w n="9.1">L</w>'<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>r</w> <w n="9.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="9.4" punct="vg:4">p<seg phoneme="y" type="vs" value="1" rule="449" place="4" punct="vg" caesura="1">u</seg>r</w>,<caesura></caesura> <w n="9.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="9.6">v<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="9.7" punct="pv:10">p<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="452" place="9" mp="M">u</seg><pgtc id="5" weight="2" schema="CR">m<rhyme label="b" id="5" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="pv">é</seg></rhyme></pgtc></w> ;</l>
						<l n="10" num="1.10" lm="10" met="4+6"><w n="10.1" punct="vg:1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1" punct="vg">ou</seg>s</w>, <w n="10.2" punct="vg:4">pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2" mp="M">o</seg>scr<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>pt<seg phoneme="œ" type="vs" value="1" rule="406" place="4" punct="vg" caesura="1">eu</seg>rs</w>,<caesura></caesura> <w n="10.3">s<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="M">i</seg>sf<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg>c<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="e" stanza="3" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="10">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="11" num="1.11" lm="10" met="4+6"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="11.2">s<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>f</w> <w n="11.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="C">u</seg></w> <w n="11.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">an</seg>g</w><caesura></caesura> <w n="11.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="11.6">l</w>'<w n="11.7"><seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r</w> <w n="11.8">v<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="C">ou</seg>s</w> <w n="11.9" punct="pv:10">d<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>v<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="10">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="12" num="1.12" lm="10" met="4+6"><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="12.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="12.3">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="3">ain</seg>s</w> <w n="12.4" punct="vg:4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="4" punct="vg" caesura="1">en</seg></w>,<caesura></caesura> <w n="12.5">j</w>'<w n="12.6"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="5">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="12.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="12.9">su<seg phoneme="i" type="vs" value="1" rule="490" place="8">i</seg>s</w> <w n="12.10" punct="pv:10"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="9" mp="M">ai</seg><pgtc id="5" weight="2" schema="CR">m<rhyme label="b" id="5" gender="m" type="e" stanza="3" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="pv">é</seg></rhyme></pgtc></w> ;</l>
						<l n="13" num="1.13" lm="8" met="8"><space unit="char" quantity="4"></space><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">pu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="13.3" punct="vg:4">m<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>r</w>, <w n="13.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.5">su<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>s</w> <w n="13.6" punct="pv:8"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="7">ai</seg><pgtc id="6" weight="2" schema="CR">m<rhyme label="a" id="6" gender="m" type="a" stanza="4" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pv">é</seg></rhyme></pgtc></w> ;</l>
						<l n="14" num="1.14" lm="10" met="4+6"><w n="14.1">D</w>'<w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="14.3">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.4"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="3" mp="M">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="343" place="4" caesura="1">ue</seg>il</w><caesura></caesura> <w n="14.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="C">on</seg></w> <w n="14.6">c<seg phoneme="œ" type="vs" value="1" rule="248" place="6">œu</seg>r</w> <w n="14.7"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="14.8" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8" mp="M">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="6" weight="2" schema="CR">mm<rhyme label="a" id="6" gender="m" type="e" stanza="4" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="vg">é</seg></rhyme></pgtc></w>,</l>
						<l n="15" num="1.15" lm="8" met="8"><space unit="char" quantity="4"></space><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="3">ain</seg>s</w> <w n="15.4" punct="vg:4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="4" punct="vg">en</seg></w>, <w n="15.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.6">su<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>s</w> <w n="15.7" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="7">ai</seg><pgtc id="6" weight="2" schema="CR">m<rhyme label="a" id="6" gender="m" type="a" stanza="4" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>