<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHABERT</title>
				<title type="sub">HISTOIRE CONTEMPORAINE EN DEUX ACTES, MÊLÉE DE CHANT</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="ARJ" sort="1">
					<name>
						<forename>Jacques</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1790" to="1855">1790-1855</date>
				</author>
				<author key="LUR" sort="2">
					<name>
						<forename>Louis</forename>
						<surname>LURINE</surname>
					</name>
					<date from="1810" to="1860">1810-1860</date>
				</author>
				<editor>Le rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>110 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">JRL_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
				   </p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CHABERT</title>
						<author>JACQUES ARAGO ET LOUIS LURINE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=vldoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849877.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">2 JUILLET 1832</date>
				<placeName>
					<settlement>THÉÂTRE NATIONAL DU VAUDEVILLE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="JRL12" modus="cm" lm_max="10" metProfile="4+6" form="strophe unique" schema="1(ababcdcd)" er_moy="1.0" er_max="2" er_min="0" er_mode="0(2/4)" er_moy_et="1.0" qr_moy="0.0" qr_max="C0" qr_mode="0(4/4)" qr_moy_et="0.0">
	<head type="tune">AIR : d' Yelva.</head>
	<lg n="1" type="huitain" rhyme="ababcdcd">
		<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="vg">ez</seg></w>, <w n="1.2" punct="vg:4">m<seg phoneme="œ" type="vs" value="1" rule="150" place="3" mp="M">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="4" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="1.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="1.5">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="1.6" punct="pi:10">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg><pgtc id="1" weight="2" schema="CR">s<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></rhyme></pgtc></w> ?</l>
		<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="M">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="280" place="2">oi</seg></w> <w n="2.2">s<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="P">u</seg>r</w> <w n="2.3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="4" caesura="1">oi</seg></w><caesura></caesura> <w n="2.4">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="2.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rds</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="2.7" punct="pi:10">c<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>rr<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pi">ou</seg>x</rhyme></pgtc></w> ?</l>
		<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="3.2">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="3.3">c<seg phoneme="œ" type="vs" value="1" rule="248" place="4" caesura="1">œu</seg>r</w><caesura></caesura> <w n="3.4">su<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg>s</w>-<w n="3.5">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="6" mp="Fm">e</seg></w> <w n="3.6">d<seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341" place="8">à</seg></w> <w n="3.7" punct="pi:10">ch<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg><pgtc id="1" weight="2" schema="CR">ss<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></rhyme></pgtc></w> ?</l> 
		<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2" mp="M/mp">au</seg>dr<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="Lp">a</seg></w>-<w n="4.3">t</w>-<w n="4.4"><seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>l</w><caesura></caesura> <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">em</seg>br<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="4.6">v<seg phoneme="o" type="vs" value="1" rule="437" place="8" mp="C">o</seg>s</w> <w n="4.7" punct="pi:10">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pi">ou</seg>x</rhyme></pgtc></w> ?</l>
		<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="P">À</seg></w> <w n="5.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" mp="C">on</seg></w> <w n="5.3">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="5.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="5.5">f<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>s</w> <w n="5.6">t<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>rs</w> <w n="5.7" punct="pv:10">f<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg><pgtc id="3" weight="2" schema="CR">d<rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
		<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="6.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="6.3" punct="vg:4">v<seg phoneme="wa" type="vs" value="1" rule="439" place="3" mp="M">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="6.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="C">on</seg></w> <w n="6.5">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>rt</w> <w n="6.6">d<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>t</w> <w n="6.7">s</w>'<w n="6.8" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="M">om</seg>pl<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pv">i</seg>r</rhyme></pgtc></w> ;</l>
		<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">s<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="7.3">l</w>'<w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>r</w><caesura></caesura> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="406" place="6" mp="M">eu</seg>gl<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="7.6">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="8" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">A</seg><pgtc id="3" weight="2" schema="CR">d<rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
		<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w>-<w n="8.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2" mp="F">e</seg></w> <w n="8.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>c</w> <w n="8.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>s</w><caesura></caesura> <w n="8.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w> <w n="8.7">l</w>'<w n="8.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="8">en</seg></w> <w n="8.9" punct="pi:10">p<seg phoneme="y" type="vs" value="1" rule="452" place="9" mp="M">u</seg>n<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pi">i</seg>r</rhyme></pgtc></w> ?</l> 
	</lg>
</div></body></text></TEI>