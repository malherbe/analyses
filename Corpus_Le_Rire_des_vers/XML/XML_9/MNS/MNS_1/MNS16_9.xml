<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS16" modus="sm" lm_max="5" metProfile="5" form="suite de strophes" schema="3[abab]" er_moy="0.33" er_max="2" er_min="0" er_mode="0(5/6)" er_moy_et="0.75" qr_moy="0.0" qr_max="C0" qr_mode="0(6/6)" qr_moy_et="0.0">
	<head type="tune">AIR : C'est une amourette. ( de Marie)</head> 
	<lg n="1" type="regexp" rhyme="abababababab">
		<l n="1" num="1.1" lm="5" met="5"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">su<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>s</w> <w n="1.4" punct="pe:5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></pgtc></w> ! </l>
		<l n="2" num="1.2" lm="5" met="5"><w n="2.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="2.2">fr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="2.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.5" punct="vg:5">j<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="5" punct="vg">ou</seg>r</rhyme></pgtc></w>, </l>
		<l n="3" num="1.3" lm="5" met="5"><w n="3.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">om</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="3.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="3.3" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg><pgtc id="1" weight="2" schema="CR">tt<rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></pgtc></w>, </l>
		<l n="4" num="1.4" lm="5" met="5"><w n="4.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3" punct="pt:5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="5" punct="pt">ou</seg>r</rhyme></pgtc></w>. <del reason="analysis" type="repetition" hand="KL">(bis.)</del></l>
		<l n="5" num="1.5" lm="5" met="5"><w n="5.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="vg">on</seg></w>, <w n="5.2">pl<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4" punct="vg:5">tr<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>st<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="351" place="5">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></pgtc></w>, </l>
		<l n="6" num="1.6" lm="5" met="5"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="6.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>d</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="6.5" punct="vg:5">c<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="248" place="5" punct="vg">œu</seg>r</rhyme></pgtc></w>, </l>
		<l n="7" num="1.7" lm="5" met="5"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">v<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="7.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</w> <w n="7.4">c<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="351" place="5">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
		<l n="8" num="1.8" lm="5" met="5"><w n="8.1">G<seg phoneme="u" type="vs" value="1" rule="424" place="1">oû</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="2">er</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.3" punct="pt:5">b<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>nh<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="5" punct="pt">eu</seg>r</rhyme></pgtc></w>. </l>
		<l n="9" num="1.9" lm="5" met="5"><w n="9.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="5">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
		<l n="10" num="1.10" lm="5" met="5"><w n="10.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2" punct="pv:5">r<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>j<seg phoneme="u" type="vs" value="1" rule="d-2" place="3">ou</seg><seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg>r<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="pv">a</seg></rhyme></pgtc></w> ; </l>
		<l n="11" num="1.11" lm="5" met="5"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="11.2">c</w>'<w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="11.5">N<seg phoneme="i" type="vs" value="1" rule="466" place="4">i</seg>n<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" stanza="3" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
		<l n="12" num="1.12" lm="5" met="5"><w n="12.1">Qu</w>'<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.4" punct="pt:5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>vr<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" stanza="3" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="pt">a</seg></rhyme></pgtc></w>. </l>
		<l ana="unanalyzable" n="13" num="1.13">Que je suis contente, etc.</l>
	</lg>
</div></body></text></TEI>