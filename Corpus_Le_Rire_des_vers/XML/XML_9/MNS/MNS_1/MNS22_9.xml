<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS22" modus="sp" lm_max="6" metProfile="6, 5, (4), (3)" form="sonnet non classique" schema="abab cdcd eef fgg" er_moy="4.0" er_max="24" er_min="0" er_mode="0(4/7)" er_moy_et="8.21" qr_moy="1.43" qr_max="N5" qr_mode="0(5/7)" qr_moy_et="2.26">
	<head type="tune">Air : Vois-tu cette nacelle ? (d'Amédée de Beauplan.)</head>
	<lg n="1" rhyme="ababcdcdeeffgg">
		<l n="1" num="1.1" lm="6" met="6"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="1.3" punct="vg:6">s<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>, </l>
		<l n="2" num="1.2" lm="6" met="6"><w n="2.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="2.2">d</w>'<w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="2.5" punct="pv:6">d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>s<pgtc id="2" weight="2" schema="CR">cr<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="6" punct="pv">e</seg>t</rhyme></pgtc></w> ; </l>
		<l n="3" num="1.3" lm="6" met="6"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="3.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="3.5">pr<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>d<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
		<l n="4" num="1.4" lm="6" met="6"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="4.2">g<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="4.4" punct="pt:6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg><pgtc id="2" weight="2" schema="CR">cr<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="6" punct="pt">e</seg>t</rhyme></pgtc></w>. </l>
		<l n="5" num="1.5" lm="6" met="6"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="5.2">tr<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="5.3">s<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>mm<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="381" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
		<l n="6" num="1.6" lm="6" met="6"><w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rs</w> <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s</w>-<w n="6.3" punct="pv:6"><pgtc id="4" weight="2" schema="[CR">l<rhyme label="d" id="4" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="pv">a</seg></rhyme></pgtc></w> ; </l>
		<l n="7" num="1.7" lm="6" met="6"><w n="7.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="1">oin</seg></w> <w n="7.2">d<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="7.3">j<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>l<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>x</w> <w n="7.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="7.5" punct="vg:6">v<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="381" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>, </l>
		<l n="8" num="1.8" lm="6" met="6"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="8.3" punct="vg:3">f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3" punct="vg">in</seg></w>, <w n="8.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="8.5" punct="ps:6">v<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg><pgtc id="4" weight="2" schema="CR">l<rhyme label="d" id="4" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="6" punct="ps">à</seg></rhyme></pgtc></w>… </l>
		<l n="9" num="1.9" lm="6" met="6"><w n="9.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="9.2" punct="pe:2"><pgtc id="5" weight="24" schema="[VC[VC[VC[VC[R" part="1"><seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>h</pgtc></w> ! <w n="9.3" punct="pe:3"><pgtc id="5" weight="24" schema="[VC[VC[VC[VC[R" part="2"><seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="pe">a</seg>h</pgtc></w> ! <w n="9.4" punct="pe:4"><pgtc id="5" weight="24" schema="[VC[VC[VC[VC[R" part="3"><seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="pe">a</seg>h</pgtc></w> ! <w n="9.5" punct="pe:5"><pgtc id="5" weight="24" schema="[VC[VC[VC[VC[R" part="4"><seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="pe">a</seg>h</pgtc></w> ! <w n="9.6" punct="pe:6"><pgtc id="5" weight="24" schema="[VC[VC[VC[VC[R" part="5"><rhyme label="e" id="5" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="pe">a</seg>h</rhyme></pgtc></w> ! </l>
		<l n="10" num="1.10" lm="5" met="5"><w n="10.1" punct="pe:1"><pgtc id="5" weight="24" schema="VC[VC[VC[VC[R" part="1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</pgtc></w> ! <w n="10.2" punct="pe:2"><pgtc id="5" weight="24" schema="VC[VC[VC[VC[R" part="2"><seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>h</pgtc></w> ! <w n="10.3" punct="pe:3"><pgtc id="5" weight="24" schema="VC[VC[VC[VC[R" part="3"><seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="pe">a</seg>h</pgtc></w> ! <w n="10.4" punct="pe:4"><pgtc id="5" weight="24" schema="VC[VC[VC[VC[R" part="4"><seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="pe">a</seg>h</pgtc></w> ! <w n="10.5" punct="pe:5"><pgtc id="5" weight="24" schema="VC[VC[VC[VC[R" part="5"><rhyme label="e" id="5" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="pe">a</seg>h</rhyme></pgtc></w> ! </l>
		<l n="11" num="1.11" lm="5" met="5"><w n="11.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2" punct="vg">an</seg>s</w>, <w n="11.2">r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>st<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="11.3" punct="vg:5">l<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="f" id="6" gender="m" type="a" qr="N5"><seg phoneme="a" type="vs" value="1" rule="341" place="5" punct="vg">à</seg></rhyme></pgtc></w>, </l>
		<l n="12" num="1.12" lm="6" met="6"><w n="12.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="12.2" punct="pe:2"><seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>h</w> ! <w n="12.3" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="pe">a</seg>h</w> ! <w n="12.4" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="pe">a</seg>h</w> ! <w n="12.5" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="pe">a</seg>h</w> ! <w n="12.6" punct="pe:6"><pgtc id="6" weight="0" schema="[R" part="1"><rhyme label="f" id="6" gender="m" type="e" qr="N5"><seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="pe">a</seg>h</rhyme></pgtc></w> ! </l>
		<l n="13" num="1.13" lm="4"><w n="13.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="13.2" punct="pe:2"><seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>h</w> ! <w n="13.3" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="pe">a</seg>h</w> ! <w n="13.4" punct="pe:4"><pgtc id="7" weight="0" schema="[R" part="1"><rhyme label="g" id="7" gender="m" type="a" qr="N5"><seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="pe">a</seg>h</rhyme></pgtc></w> ! </l>
		<l n="14" num="1.14" lm="3"><w n="14.1">R<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>st<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="14.2" punct="pt:3">l<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="g" id="7" gender="m" type="e" qr="N5"><seg phoneme="a" type="vs" value="1" rule="341" place="3" punct="pt">à</seg></rhyme></pgtc></w>. </l>
	</lg>
</div></body></text></TEI>