<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS25" modus="sp" lm_max="8" metProfile="3, 7, 8, 4, (6)" form="suite de strophes" schema="5[aa] 3[abba] 1[abab]" er_moy="0.92" er_max="6" er_min="0" er_mode="0(9/13)" er_moy_et="1.69" qr_moy="0.0" qr_max="C0" qr_mode="0(13/13)" qr_moy_et="0.0">
	<head type="tune">Air du dernier soupir de Veber.</head> 
	<lg n="1" type="regexp" rhyme="aaabbaababaaabbaaaabbaaaaa">
		<l n="1" num="1.1" lm="3" met="3"><w n="1.1">S<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="1.2">s<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg><pgtc id="1" weight="2" schema="CR">ffr<rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></pgtc></w></l>
		<l n="2" num="1.2" lm="3" met="3"><w n="2.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="2.3"><pgtc id="1" weight="2" schema="[CR">Fr<rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></pgtc></w></l>
		<l n="3" num="1.3" lm="7" met="7"><w n="3.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>j<seg phoneme="a" type="vs" value="1" rule="306" place="2">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg>s</w> <w n="3.3">d<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4" punct="pv:7"><pgtc id="2" weight="0" schema="[R" part="1"><rhyme label="a" id="2" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7" punct="pv">an</seg>s</rhyme></pgtc></w> ; </l>
		<l n="4" num="1.4" lm="3" met="3"><w n="4.1" punct="vg:2">Pl<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="4.2">pl<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="b" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></pgtc></w></l>
		<l n="5" num="1.5" lm="3" met="3"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="5.3" punct="vg:3">h<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="b" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></pgtc></w>, </l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1" punct="vg:1">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" punct="vg">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="6.2"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>bj<seg phoneme="ɛ" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="6.5">s<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="6">oin</seg>s</w> <w n="6.6" punct="pt:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>st<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="a" id="2" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="pt">an</seg>s</rhyme></pgtc></w>. </l>
		<l n="7" num="1.7" lm="6"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="7.2">v<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>vr<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="7.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</w> <w n="7.4">l</w>'<w n="7.5" punct="vg:6">h<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>st<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="a" id="4" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>, </l>
		<l n="8" num="1.8" lm="7" met="7"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="280" place="2">oi</seg></w> <w n="8.2">l</w>'<w n="8.3">h<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.4">n<pgtc id="5" weight="6" schema="VCR" part="1"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>ch<rhyme label="b" id="5" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></rhyme></pgtc></w></l>
		<l n="9" num="1.9" lm="7" met="7"><w n="9.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="9.4">d<seg phoneme="o" type="vs" value="1" rule="434" place="4">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg></w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="9.6">gl<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="a" id="4" gender="f" type="e" stanza="3" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
		<l n="10" num="1.10" lm="7" met="7"><w n="10.1">M<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>t</w>-<w n="10.2"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="10.3">s<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>r</w> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="10.5" punct="pi:7">r<pgtc id="5" weight="6" schema="VCR" part="1"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>ch<rhyme label="b" id="5" gender="m" type="e" stanza="3" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="7" punct="pi">er</seg></rhyme></pgtc></w> ? </l>
		<l n="11" num="1.11" lm="3" met="3"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="11.2">tr<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>st<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="a" id="6" gender="f" type="a" stanza="4" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="351" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></pgtc></w></l>
		<l n="12" num="1.12" lm="3" met="3"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="12.2">m</w>'<w n="12.3"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>ppr<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="a" id="6" gender="f" type="e" stanza="4" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="351" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></pgtc></w></l>
		<l n="13" num="1.13" lm="7" met="7"><w n="13.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="1">en</seg>t</w> <w n="13.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r</w> <w n="13.3">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="13.4">y<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.6" punct="pv:7">pl<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="m" type="a" stanza="5" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="7" punct="pv">eu</seg>rs</rhyme></pgtc></w> ; </l>
		<l n="14" num="1.14" lm="3" met="3"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">m</w>'<w n="14.3"><seg phoneme="i" type="vs" value="1" rule="496" place="2">y</seg></w> <w n="14.4">pl<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="f" type="a" stanza="5" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></pgtc></w></l>
		<l n="15" num="1.15" lm="3" met="3"><w n="15.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="15.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">s<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="f" type="e" stanza="5" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></pgtc></w></l>
		<l n="16" num="1.16" lm="7" met="7"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="16.2">d</w>'<w n="16.3"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="16.4">v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="16.5" punct="pt:7">d<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>l<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="m" type="e" stanza="5" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="7" punct="pt">eu</seg>rs</rhyme></pgtc></w>. </l>
		<l n="17" num="1.17" lm="4" met="4"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="17.2">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="17.3"><pgtc id="9" weight="2" schema="CR" part="1">p<rhyme label="a" id="9" gender="f" type="a" stanza="6" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
		<l n="18" num="1.18" lm="4" met="4"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="341" place="1">à</seg></w>-<w n="18.2">h<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="18.3">j</w>'<w n="18.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>s<pgtc id="9" weight="2" schema="CR" part="1">p<rhyme label="a" id="9" gender="f" type="e" stanza="6" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
		<l n="19" num="1.19" lm="8" met="8"><w n="19.1">Br<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2"><seg phoneme="e" type="vs" value="1" rule="188" place="2">e</seg>t</w> <w n="19.3">r<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t</w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="19.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="19.6" punct="pv:8"><seg phoneme="e" type="vs" value="1" rule="169" place="6">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="a" id="10" gender="m" type="a" stanza="7" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pv">i</seg>s</rhyme></pgtc></w> ; </l>
		<l n="20" num="1.20" lm="4" met="4"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="20.2">s<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>r</w> <w n="20.3">s<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="20.4"><pgtc id="11" weight="2" schema="[CR" part="1">g<rhyme label="b" id="11" gender="f" type="a" stanza="7" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
		<l n="21" num="1.21" lm="4" met="4"><w n="21.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="21.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="21.3" punct="vg:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg><pgtc id="11" weight="2" schema="CR" part="1">g<rhyme label="b" id="11" gender="f" type="e" stanza="7" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>, </l>
		<l n="22" num="1.22" lm="8" met="8"><w n="22.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="22.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="22.3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>t</w> <w n="22.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="22.5">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg>s</w> <w n="22.6" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>br<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="a" id="10" gender="m" type="e" stanza="7" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg>s</rhyme></pgtc></w>, <del reason="analysis" type="irrelevant" hand="KL">( Il se lève.)</del></l>
		<l n="23" num="1.23" lm="3" met="3"><w n="23.1">S<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="23.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.3"><pgtc id="12" weight="0" schema="[R" part="1"><rhyme label="a" id="12" gender="f" type="a" stanza="8" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></pgtc></w></l>
		<l n="24" num="1.24" lm="3" met="3"><w n="24.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="24.2">r<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>cl<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="a" id="12" gender="f" type="e" stanza="8" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></pgtc></w></l>
		<l n="25" num="1.25" lm="7" met="7"><w n="25.1">J<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>squ</w>'<w n="25.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="25.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="25.4">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="25.5" punct="pt:7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">om</seg>b<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="m" type="a" stanza="9" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="pt">a</seg>t</rhyme></pgtc></w>. </l>
		<l rhyme="none" n="26" num="1.26" lm="4" met="4"><w n="26.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">Om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="26.2">ch<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>r<seg phoneme="i" type="vs" value="1" rule="481" place="4">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
		<l rhyme="none" n="27" num="1.27" lm="3" met="3"><w n="27.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="27.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="27.3">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></w></l>
		<l n="28" num="1.28" lm="7" met="7"><w n="28.1">Pr<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="28.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="28.3">vi<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="28.4" punct="pe:7">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>ld<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="m" type="e" stanza="9" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="pe">a</seg>t</rhyme></pgtc></w> ! </l>
	</lg>
</div></body></text></TEI>