<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">À PROPOS PATRIOTIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="VIL" sort="1">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="MAS" sort="2">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>273 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">VEM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>À propos patriotique</title>
						<author>VILLENEUVE ET MASSON</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=0FA6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>À propos patriotique</title>
								<author>VILLENEUVE ET MASSON</author>
								<repository>Bayerische Staatsbibliothek</repository>
								<idno type="URI">https://opacplus.bsb-muenchen.de/title/BV008031366</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_subpart">SCÈNE III.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="VEM8" modus="cp" lm_max="10" metProfile="8, 4+6" form="strophe unique" schema="1(ababcdcd)" er_moy="0.75" er_max="2" er_min="0" er_mode="0(2/4)" er_moy_et="0.83" qr_moy="0.0" qr_max="C0" qr_mode="0(4/4)" qr_moy_et="0.0">
				<head type="tune">AIR : Un page aimait la jeune Adelle.</head>
				<lg n="1" type="huitain" rhyme="ababcdcd">
					<head type="main">Bertrand.</head>
					<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="1.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="1.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="2">om</seg></w> <w n="1.3">s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="1.4">b<seg phoneme="o" type="vs" value="1" rule="314" place="4">eau</seg></w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.6">L<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="338" place="7">a</seg>y<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="2.1">H<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>n<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="2.2">n<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>tʼ</w> <w n="2.3">c<seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>rd</w>'<w n="2.5" punct="pv:8">h<pgtc id="2" weight="1" schema="GR">u<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="490" place="8" punct="pv">i</seg></rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="3.3">d<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="3.4">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="3.5">qu</w>'<w n="3.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="3.7" punct="pt:8">r<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>p<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="4.1" punct="vg:2">R<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2" punct="vg">e</seg>ct</w>, <w n="4.2" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg">ou</seg>r</w>, <w n="4.3">h<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>r</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="341" place="7">à</seg></w> <w n="4.5" punct="pt:8">l<pgtc id="2" weight="1" schema="GR">u<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="490" place="8" punct="pt">i</seg></rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">N<seg phoneme="o" type="vs" value="1" rule="437" place="1" mp="C">o</seg>s</w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="169" place="2" mp="M">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="5.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="5.4">d<seg phoneme="y" type="vs" value="1" rule="444" place="6">û</seg></w> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.6" punct="vg:10">m<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>c<seg phoneme="o" type="vs" value="1" rule="434" place="9" mp="M">o</seg>nn<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="6.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="6.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="C">on</seg></w> <w n="6.5">c<seg phoneme="œ" type="vs" value="1" rule="248" place="6">œu</seg>r</w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="6.7" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="d" id="4" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="vg">é</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="7.3">f<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>s</w><caesura></caesura> <w n="7.4">qu</w>'<w n="7.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="7.6">l</w>'<w n="7.7"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="7.8">v<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="7.9">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>r<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="8.2">v<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t</w> <w n="8.3">rʼp<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="8.6" punct="pt:10">l<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>r<pgtc id="4" weight="2" schema="CR">t<rhyme label="d" id="4" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="pt">é</seg></rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>