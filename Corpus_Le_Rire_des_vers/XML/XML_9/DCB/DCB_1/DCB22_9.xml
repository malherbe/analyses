<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB22" modus="cp" lm_max="11" metProfile="8, 4+6, (11)" form="strophe unique" schema="1(ababcdccd)" er_moy="0.0" er_max="0" er_min="0" er_mode="0(5/5)" er_moy_et="0.0" qr_moy="0.0" qr_max="C0" qr_mode="0(5/5)" qr_moy_et="0.0">
	<head type="tune">AIR : Pauvre soldat…</head>
	<lg n="1" type="neuvain" rhyme="ababcdccd">
		<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="1.2">m</w>'<w n="1.3"><seg phoneme="y" type="vs" value="1" rule="250" place="2">eû</seg>t</w> <w n="1.4">d<seg phoneme="o" type="vs" value="1" rule="434" place="3" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="1.5">B<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="M">ou</seg>rg<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="1.7">M<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="409" place="9">è</seg>r</w>' <w n="1.8" punct="vg:10">s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="345" place="10" punct="vg">e</seg>c</rhyme></pgtc></w>,</l>
		<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1" punct="vg:2">B<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1" mp="M">o</seg>rd<seg phoneme="o" type="vs" value="1" rule="314" place="2" punct="vg">eau</seg>x</w>, <w n="2.2" punct="vg:4">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="M">am</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="in vg" caesura="1">a</seg>gn</w>', <caesura></caesura><w n="2.3">l<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>rs</w> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="2.5">mi<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg>x</w> <w n="2.6" punct="vg:10">ch<seg phoneme="wa" type="vs" value="1" rule="419" place="9" mp="M">oi</seg>s<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="481" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>llou<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="3.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.6">b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="345" place="8">e</seg>c</rhyme></pgtc></w></l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>b<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>ʼs</w> <w n="4.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>tʼs</w> <w n="4.5" punct="pt:8">r<seg phoneme="o" type="vs" value="1" rule="414" place="7">ô</seg>t<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
		<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="5.2">j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r</w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="5.4">j<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>r</w><caesura></caesura> <w n="5.5">mi<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="5.6" punct="vg:7">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" mp="M">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" punct="vg">an</seg>t</w>, <w n="5.7">mi<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg>x</w> <w n="5.8" punct="vg:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>p<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="449" place="10" punct="vg">u</seg></rhyme></pgtc></w>,</l>
		<l n="6" num="1.6" lm="11"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="6.2">m</w>'<w n="6.3">h<seg phoneme="y" type="vs" value="1" rule="452" place="2" mp="M">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>ct<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="6.4">d</w>'<w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w>' <w n="6.6">r<seg phoneme="o" type="vs" value="1" rule="443" place="6" mp="M">o</seg>s<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg></w>' <w n="6.7" punct="vg:11">d<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="402" place="11">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1" punct="pe:2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">In</seg>gr<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4">e</seg>c</w> <w n="7.3">lu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="7.4">j</w>'<w n="7.5"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>s</w> <w n="7.6" punct="vg:8">b<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="449" place="8" punct="vg">u</seg></rhyme></pgtc></w>,</l>
		<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">J</w>'<w n="8.2"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="8.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="8.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>t</w> <w n="8.5">c</w>'<w n="8.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.7">j</w>'<w n="8.8"><seg phoneme="o" type="vs" value="1" rule="317" place="7" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>s</w> <w n="8.9" punct="ps:10">v<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>l<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="449" place="10" punct="ps">u</seg></rhyme></pgtc></w>…</l>
		<l n="9" num="1.9" lm="8" met="8"><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="9.3">t</w>'<w n="9.4"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="9.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>d<seg phoneme="y" type="vs" value="1" rule="456" place="6">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-42">e</seg></w> <w n="9.6" punct="pe:8">h<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="402" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
	</lg>
</div></body></text></TEI>