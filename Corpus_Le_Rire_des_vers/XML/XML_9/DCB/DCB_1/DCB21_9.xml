<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB21" modus="cp" lm_max="10" metProfile="8, 4+6" form="suite de strophes" schema="1[ababb] 1[abbab]" er_moy="0.67" er_max="2" er_min="0" er_mode="0(4/6)" er_moy_et="0.94" qr_moy="0.0" qr_max="C0" qr_mode="0(6/6)" qr_moy_et="0.0">
	<head type="tune">AIR : Il me faudra quitter l'empire.</head>
	<lg n="1" type="regexp" rhyme="ababbabbab">
		<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3">c<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3" mp="M">o</seg>st<seg phoneme="ɔ" type="vs" value="1" rule="450" place="4" caesura="1">u</seg>m</w>' <caesura></caesura><w n="1.4">d<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>t</w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">ê</seg>tr</w>' <w n="1.6">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t</w> <w n="1.7">p<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="P">ou</seg>r</w> <w n="1.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="1.9" punct="vg:10">t<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="306" place="10">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
		<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="2.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="2.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="2.5">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="6">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="2.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="2.7" punct="pv:10">h<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>b<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pv">i</seg>ts</rhyme></pgtc></w> ;</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="3.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="3.3">d</w>'<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rd</w> <w n="3.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.6">ç<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="3.7">v<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="3.8" punct="dp:8"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="306" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="4.2">s<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>mmʼs</w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w>-<w n="4.4">dʼss<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg></w> <w n="4.6">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="6">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pv">i</seg>s</rhyme></pgtc></w> ;</l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1">Ç<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">d<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>t</w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="5.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.7" punct="pt:8">pl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pt">i</seg>s</rhyme></pgtc></w>.</l>
		<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="6.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="C">ou</seg>s</w> <w n="6.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>li<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">ez</seg></w><caesura></caesura> <w n="6.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="C">ou</seg>s</w> <w n="6.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w> <w n="6.6" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg><pgtc id="3" weight="2" schema="CR">s<rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="7.3">n</w>'<w n="7.4"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>ri<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.7" punct="pe:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>gr<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="8" punct="pe">e</seg>ts</rhyme></pgtc></w> !</l>
		<l n="8" num="1.8" lm="8" met="8"><w n="8.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>t</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="8.3" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="3" punct="vg">oi</seg></w>, <w n="8.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5">m</w>'<w n="8.6" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pv">ai</seg>s</rhyme></pgtc></w> ;</l>
		<l n="9" num="1.9" lm="8" met="8"><w n="9.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="9.2">m</w>'<w n="9.3"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ri<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="9.4" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rf<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>tʼm<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" punct="vg">en</seg>t</w>, <w n="9.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.6">m</w>'<w n="9.7" punct="vg:8"><pgtc id="3" weight="2" schema="[CR">s<rhyme label="a" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="10" num="1.10" lm="8" met="8"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="10.2">j</w>'<w n="10.3">su<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="10.4">b<seg phoneme="ɛ̃" type="vs" value="1" rule="220" place="3">en</seg></w> <w n="10.5">s<seg phoneme="y" type="vs" value="1" rule="444" place="4">û</seg>r</w> <w n="10.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.7">j</w>'<w n="10.8">v<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="10.9" punct="pt:8"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="4" weight="2" schema="CR">r<rhyme label="b" id="4" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pt">ai</seg>s</rhyme></pgtc></w>.</l> 
	</lg>
</div></body></text></TEI>