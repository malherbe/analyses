<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB15" modus="sp" lm_max="7" metProfile="5, 7, 6" form="suite de strophes" schema="2[abba] 2[aa]" er_moy="0.33" er_max="2" er_min="0" er_mode="0(5/6)" er_moy_et="0.75" qr_moy="0.0" qr_max="C0" qr_mode="0(6/6)" qr_moy_et="0.0">
	<head type="tune">AIR : Cherchez bien (d'André le chansonnier).</head>
		<div type="section" n="1">
			<lg n="1" type="regexp" rhyme="abba">
				<l n="1" num="1.1" lm="5" met="5"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2" punct="vg:2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="vg">a</seg>rs</w>, <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg></w> <w n="1.4" punct="pv:5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="5" punct="pv">oi</seg>r</rhyme></pgtc></w> ;</l>
				<l n="2" num="1.2" lm="7" met="7"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="2">en</seg>s</w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">in</seg></w> <w n="2.4">d</w>'<w n="2.5">m</w>'<w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="2.7" punct="pv:7">d<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>f<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
				<l n="3" num="1.3" lm="5" met="5"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="3.2">n</w>'<w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="3.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.5" punct="dp:5">pl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="dp">e</seg></rhyme></pgtc></w> :</l>
				<l n="4" num="1.4" lm="7" met="7"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">r<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>s</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="4.5" punct="pt:7">d<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>sp<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="7" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
			</lg> 
			<lg n="2" type="regexp" rhyme="a">
			<head type="speaker">MARGUERITE.</head>
				<l n="5" num="2.1" lm="6" met="6"><w n="5.1">T</w>'<w n="5.2"><seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="5.3">d<seg phoneme="y" type="vs" value="1" rule="444" place="3">û</seg></w> <w n="5.4">l</w>'<w n="5.5" punct="pt:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>r<pgtc id="3" weight="2" schema="CR">v<rhyme label="a" id="3" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="pt">er</seg></rhyme></pgtc></w>.</l>
			</lg> 
			<lg n="3" type="regexp" rhyme="aa">
			<head type="speaker">TÉLÉMAQUE.</head>
				<l n="6" num="3.1" lm="6" met="6"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="6.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="6.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="6.4">cr<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>t</w> <w n="6.5">tr<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg><pgtc id="3" weight="2" schema="CR">v<rhyme label="a" id="3" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></rhyme></pgtc></w></l>
				<l n="7" num="3.2" lm="7" met="7"><w n="7.1">D<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>x</w> <w n="7.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="vg">ou</seg>r</w>, <w n="7.3">c<seg phoneme="œ" type="vs" value="1" rule="248" place="4">œu</seg>r</w> <w n="7.4" punct="pt:7"><seg phoneme="i" type="vs" value="1" rule="466" place="5">i</seg>nn<seg phoneme="ɔ" type="vs" value="1" rule="442" place="6">o</seg>c<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="7" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
			</lg> 
			<lg n="4" type="regexp" rhyme="a">
			<head type="speaker">CÉLESTINE, avec fierté.</head>
				<l n="8" num="4.1" lm="6" met="6"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="8.2">s</w>'<w n="8.3">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="8.4" punct="pe:6">j<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>l<seg phoneme="i" type="vs" value="1" rule="466" place="5">i</seg>m<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="e" stanza="3" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" punct="pe">en</seg>t</rhyme></pgtc></w> !</l>
			</lg> 
		</div>
		<div type="section" n="2">
			<head type="main">ENSEMBLE.</head>
			<lg n="1" type="regexp" rhyme="abba">
			<p>
				CÉLESTINE.<lb></lb>
				Je pars, au revoir, etc.<lb></lb>
			</p>
			<head type="speaker">TÉLÉMAQUE, MARGUERITE.</head>
				<l n="9" num="1.1" lm="5" met="5"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll</w>'<w n="9.2" punct="vg:2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="vg">a</seg>rt</w>, <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg></w> <w n="9.4" punct="pt:5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="a" stanza="4" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="5" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
				<l n="10" num="1.2" lm="7" met="7"><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ll</w>'<w n="10.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="3">en</seg>t</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.5">s</w>'<w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="10.7" punct="pe:7">d<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>f<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="a" stanza="4" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				<l n="11" num="1.3" lm="5" met="5"><w n="11.1">Pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="11.2">g<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rd</w>'<del reason="analysis" type="alternative" hand="KL">(Mais prends gard')</del>, <w n="11.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="11.4" punct="vg:5">ch<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="e" stanza="4" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				<l n="12" num="1.4" lm="7" met="7"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="12.2">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="12.3">d</w>'<w n="12.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="12.5" punct="pt:7">d<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>sp<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="e" stanza="4" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="7" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
			</lg>
		</div>
</div></body></text></TEI>