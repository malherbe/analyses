<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE2" modus="sp" lm_max="7" metProfile="6, 3, (7)" form="strophe unique" schema="1(aabbcdcd)" er_moy="0.75" er_max="2" er_min="0" er_mode="0(2/4)" er_moy_et="0.83" qr_moy="0.0" qr_max="C0" qr_mode="0(4/4)" qr_moy_et="0.0">
	
			<head type="main">MICHEL</head>

		<head type="tune">AIR du Vétéran.</head>

				<lg n="1" type="huitain" rhyme="aabbcdcd"><l n="1" num="1.1" lm="6" met="6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">g<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rç<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="1.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="4">im</seg>pr<seg phoneme="i" type="vs" value="1" rule="466" place="5">i</seg>m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>r</rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="3" met="3"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w> <w n="2.2" punct="pv:3">pi<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>ch<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="3" punct="pv">eu</seg>r</rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="6" met="6"><w n="3.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="3.2">l</w>’<w n="3.3">d<seg phoneme="i" type="vs" value="1" rule="466" place="2">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.4"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l</w> <w n="3.5">n</w>’<w n="3.6">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="3.7" punct="vg:6">r<pgtc id="2" weight="1" schema="GR">i<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="6" punct="vg">en</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="3" met="3"><w n="4.1">C</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="4.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>s</w> <w n="4.4" punct="vg:3">b<pgtc id="2" weight="1" schema="GR">i<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="3" punct="vg">en</seg></rhyme></pgtc></w>,</l>
					<l n="5" num="1.5" lm="6" met="6"><w n="5.1">L</w>’<w n="5.2">l<seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="5.3">c</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="5.6">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">ê</seg>m</w>’ <w n="5.7" punct="pv:6">ch<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="6" num="1.6" lm="3" met="3"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="6.2">l</w>’<w n="6.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>r<pgtc id="4" weight="2" schema="CR">d<rhyme label="d" id="4" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></rhyme></pgtc></w></l>
					<l n="7" num="1.7" lm="7"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="7.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="7.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="3">en</seg></w> <w n="7.4">qu</w>’<w n="7.5"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l</w> <w n="7.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>p<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="3" met="3"><w n="8.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="8.2" punct="pe:3">l<seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg><pgtc id="4" weight="2" schema="CR">d<rhyme label="d" id="4" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="pe pt pt">i</seg></rhyme></pgtc></w> !..</l>
					<l ana="unanalyzable" n="9" num="1.9">Tra la la la ....</l></lg>
			</div></body></text></TEI>