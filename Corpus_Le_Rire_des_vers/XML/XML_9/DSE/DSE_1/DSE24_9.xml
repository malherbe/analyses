<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE24" modus="cm" lm_max="10" metProfile="4+6" form="strophe unique" schema="1(ababcdcd)" er_moy="0.75" er_max="2" er_min="0" er_mode="0(2/4)" er_moy_et="0.83" qr_moy="1.25" qr_max="N5" qr_mode="0(3/4)" qr_moy_et="2.17">			
	
	<head type="main">LA LIBERTÉ POLITIQUE.</head>

			<p>Avez-vous donc oublié l’outrage fait à l’orateur que pleure encore la France ? l’hôtel des Quatre-Nations !</p>

			<head type="tune">Air du Vaudeville du jour des Noces.</head>

		    <lg n="1" type="huitain" rhyme="ababcdcd"><l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="1.2">tr<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="341" place="4" caesura="1">à</seg></w><caesura></caesura> <w n="1.4">qu<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="1.5" punct="vg:10">l<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg>c<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>t<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="N5"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
				<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="2.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="2.3">s<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="2.5">l<seg phoneme="o" type="vs" value="1" rule="317" place="6" mp="M">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg>s</w> <w n="2.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="2.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>t</w> <w n="2.8" punct="pv:10">r<pgtc id="2" weight="1" schema="GR">i<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="10" punct="pv">en</seg></rhyme></pgtc></w> ;</l>
				<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>ls</w> <w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="3.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="5">en</seg></w> <w n="3.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6" mp="M">in</seg>j<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="3.6">n<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>gu<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="N5"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
				<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1" mp="M">En</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="63" place="2">e</seg>rs</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3" mp="C">un</seg></w> <w n="4.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">an</seg>d</w><caesura></caesura> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="4.5">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="4.6" punct="pt:10">c<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="439" place="9" mp="M">o</seg><pgtc id="2" weight="1" schema="GR">y<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="362" place="10" punct="pt">en</seg></rhyme></pgtc></w>.</l>
				<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">on</seg></w> <w n="5.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="2">om</seg></w> <w n="5.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" mp="M">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="5.4">pr<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>s<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="5.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="5.6" punct="pt:10">v<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>ct<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt pt pt" mp="F">e</seg></rhyme></pgtc></w> ...</l>
				<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="6.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="P">a</seg>r</w> <w n="6.3">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="6.4">m<seg phoneme="o" type="vs" value="1" rule="437" place="4" caesura="1">o</seg>ts</w><caesura></caesura> <w n="6.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="C">on</seg></w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="7" mp="M">ê</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w> <w n="6.7">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="6.8" punct="pt:10"><pgtc id="4" weight="2" schema="[CR">p<rhyme label="d" id="4" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="pt pt pt">a</seg>s</rhyme></pgtc></w>...</l>
				<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="1">e</seg>l</w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="7.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="C">on</seg></w> <w n="7.4" punct="pi:4">t<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="pi pt pt pt" caesura="1">i</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ?<caesura></caesura> ... <w n="7.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="C">I</seg>l</w> <w n="7.6" punct="dp:8">r<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="M">on</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="dp">i</seg>t</w> : <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="7.8" punct="pe:10">gl<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
				<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="M">on</seg>ci<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4" caesura="1">e</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="8.5" punct="dp:6">d<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="dp">i</seg>t</w> : <w n="8.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="C">On</seg></w> <w n="8.7">n</w>’<w n="8.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.9" punct="pt:10"><pgtc id="4" weight="2" schema="[CR">p<rhyme label="d" id="4" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="pt pt pt pt">a</seg>s</rhyme></pgtc></w> ....</l></lg></div></body></text></TEI>