<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE II.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC3" modus="cm" lm_max="10" metProfile="4+6" form="strophe unique" schema="1(ababcdcd)" er_moy="0.5" er_max="2" er_min="0" er_mode="0(3/4)" er_moy_et="0.87" qr_moy="0.0" qr_max="C0" qr_mode="0(4/4)" qr_moy_et="0.0">
				<head type="tune">AIR : Vaudeville de l'Anonyme.</head>
				<lg n="1" type="huitain" rhyme="ababcdcd">
					<head type="main">LA CENSURE.</head>
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="1.2">l<seg phoneme="œ" type="vs" value="1" rule="406" place="2" mp="C">eu</seg>r</w> <w n="1.3" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg" caesura="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="7" mp="M">ê</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>t</w> <w n="1.6">l<seg phoneme="œ" type="vs" value="1" rule="406" place="9" mp="C">eu</seg>rs</w> <w n="1.7" punct="vg:10">pi<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="2.3">tr<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>c<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="2.5" punct="vg:10">r<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>p<seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" punct="vg">on</seg>s</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1" punct="vg:2">Ch<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg" mp="F">e</seg>s</w>, <w n="3.2">s<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="3.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="3.6" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>sp<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="4.3">j<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>rn<seg phoneme="o" type="vs" value="1" rule="317" place="4" caesura="1">au</seg>x</w><caesura></caesura> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="4.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="4.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="4.7" punct="pt:10">f<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>ç<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" punct="pt">on</seg>s</rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="5.2">l<seg phoneme="œ" type="vs" value="1" rule="406" place="2" mp="C">eu</seg>r</w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="6">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="5.6">c<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>pr<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="6.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="2">ain</seg>ts</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>ct<seg phoneme="œ" type="vs" value="1" rule="406" place="4" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="6.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="6.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>t</w> <w n="6.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="6.7" punct="pv:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="8" mp="M">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9" mp="M">o</seg>r<pgtc id="4" weight="2" schema="CR">t<rhyme label="d" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="10" punct="pv">an</seg>s</rhyme></pgtc></w> ;</l>
					<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">J<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="Lc">u</seg>squ</w>'<w n="7.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2" mp="C">au</seg></w> <w n="7.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="7.4">d</w>'<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="6" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg></w> <w n="7.7">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="7.8" punct="pe:10"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>ctr<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe ps" mp="F">e</seg>s</rhyme></pgtc></w> ! …</l>
					<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="8.2">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="8.3">m<seg phoneme="e" type="vs" value="1" rule="352" place="3" mp="M">e</seg>ssi<seg phoneme="ø" type="vs" value="1" rule="396" place="4" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="8.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="8.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>t</w> <w n="8.6">j<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>s</w> <w n="8.7" punct="pt:10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="M">on</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="d" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="10" punct="pt">en</seg>s</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>