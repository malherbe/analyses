<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE XII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC30" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(ababbcdcd)" er_moy="8.8" er_max="44" er_min="0" er_mode="0(4/5)" er_moy_et="17.6" qr_moy="0.0" qr_max="C0" qr_mode="0(5/5)" qr_moy_et="0.0">
						<head type="tune">AIR de l'Angelus.</head>
							<lg n="1" type="neuvain" rhyme="ababbcdcd">
								<head type="main">L'ANNÉE 1831.</head>
								<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rs</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="1.4">p<seg phoneme="ɛ" type="vs" value="1" rule="384" place="4">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="1.6" punct="vg:8">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rc<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="314" place="8" punct="vg">eau</seg></rhyme></pgtc></w>,</l>
								<l n="2" num="1.2" lm="8" met="8"><w n="2.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="2.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg>s</w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="2.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="2.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>r</w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="2.7" punct="vg:8">Fr<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
								<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">l</w>'<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>r</w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6">pl<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>s</w> <w n="3.7">b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="314" place="8">eau</seg></rhyme></pgtc></w></l>
								<l n="4" num="1.4" lm="8" met="8"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4">e</seg>c</w> <w n="4.4">m<seg phoneme="wa" type="vs" value="1" rule="422" place="5">oi</seg></w> <w n="4.5">l</w>'<w n="4.6" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>r<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>. <del type="repetition" reason="analysis" hand="LG">(bis)</del></l>
								<l n="5" num="1.5" lm="8" met="8"><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="5.1"><pgtc id="2" weight="44" schema="CV[CVC[VCV[CV[C[VCVCR" part="1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></pgtc></w> <w n="5.2"><pgtc id="2" weight="44" schema="CV[CVC[VCV[CV[C[VCVCR" part="2">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></pgtc></w> <w n="5.3"><pgtc id="2" weight="44" schema="CV[CVC[VCV[CV[C[VCVCR" part="3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4">e</seg>c</pgtc></w> <w n="5.4"><pgtc id="2" weight="44" schema="CV[CVC[VCV[CV[C[VCVCR" part="4">m<seg phoneme="wa" type="vs" value="1" rule="422" place="5">oi</seg></pgtc></w> <w n="5.5"><pgtc id="2" weight="44" schema="CV[CVC[VCV[CV[C[VCVCR" part="5">l</pgtc></w>'<w n="5.6" punct="pt:8"><pgtc id="2" weight="44" schema="CV[CVC[VCV[CV[C[VCVCR" part="6"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>r<rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</add></subst></l>
								<l n="6" num="1.6" lm="8" met="8"><w n="6.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="2">en</seg></w> <w n="6.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.3">b<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>s</w> <w n="6.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="6.5" punct="vg:8">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>st<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="c" id="3" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" punct="vg">in</seg></rhyme></pgtc></w>,</l>
								<l n="7" num="1.7" lm="8" met="8"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2" punct="vg:2">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2" punct="vg">e</seg>l</w>, <w n="7.3">d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t</w>-<w n="7.4" punct="vg:4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg">on</seg></w>, <w n="7.5">m</w>'<w n="7.6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="7.7">d<seg phoneme="o" type="vs" value="1" rule="434" place="6">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg></w> <w n="7.8">l</w>'<w n="7.9" punct="ps:8"><pgtc id="4" weight="0" schema="[R" part="1"><rhyme label="d" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></pgtc></w>…</l>
								<l n="8" num="1.8" lm="8" met="8"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="8.2">r<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="8.3">s<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>r</w> <w n="8.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="8.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="c" id="3" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8">in</seg></rhyme></pgtc></w></l>
								<l n="9" num="1.9" lm="8" met="8"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="9.2">m<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>x</w> <w n="9.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="9.5">s<seg phoneme="œ" type="vs" value="1" rule="248" place="5">œu</seg>r</w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="9.7">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t</w> <w n="9.8" punct="pt:8">n<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="d" id="4" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
							</lg>
						</div></body></text></TEI>