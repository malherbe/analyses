<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE VII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC23" modus="cp" lm_max="11" metProfile="8, 4+6, (11)" form="suite de strophes" schema="1[abab] 1[ababab]" er_moy="3.67" er_max="21" er_min="0" er_mode="0(4/6)" er_moy_et="7.76" qr_moy="0.0" qr_max="C0" qr_mode="0(6/6)" qr_moy_et="0.0">
					<head type="tune">AIR : Le choir qu'a fait tout le village,</head>
						<lg n="1" type="regexp" rhyme="ab">
							<head type="main">ARWED.</head>
							<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="6"></space><w n="1.1">C<seg phoneme="ɛ" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t</w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.6" punct="vg:8">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>st<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="466" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="C">u</seg></w> <w n="2.2">l</w>'<w n="2.3"><seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="M">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="2.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>d</w> <w n="2.5"><seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="C">i</seg>l</w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="2.7">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg></w> <w n="2.8" punct="pt:10">t<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10" punct="pt">em</seg>ps</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="2" type="regexp" rhyme="abababab">
							<head type="main">JOB.</head>
							<l n="3" num="2.1" lm="8" met="8"><space unit="char" quantity="6"></space><w n="3.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="3.2" punct="vg:4">c<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4" punct="vg">e</seg>l</w>, <w n="3.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="3.5" punct="vg:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="466" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="4" num="2.2" lm="10" met="4+6"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="4.4">pu<seg phoneme="i" type="vs" value="1" rule="490" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6" mp="M">e</seg>pt<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="4.6">v<seg phoneme="o" type="vs" value="1" rule="437" place="8" mp="C">o</seg>s</w> <w n="4.7" punct="pt:10">pr<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>s<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="10" punct="pt">en</seg>s</rhyme></pgtc></w>.</l>
							<l n="5" num="2.3" lm="10" met="4+6"><w n="5.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="C">ou</seg>s</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3" punct="vg:4">s<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="vg" caesura="1">ez</seg></w>,<caesura></caesura> <w n="5.4">j</w>' <w n="5.5">n</w>'<w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="EVC23_1" place="5">ai</seg>mʼ</w> <w n="5.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.8">v<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="5.9">s<seg phoneme="y" type="vs" value="1" rule="449" place="8" mp="P">u</seg>r</w> <w n="5.10">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="5.11" punct="vg:10">t<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="6" num="2.4" lm="10" met="4+6"><w n="6.1">C</w>'<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="6.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="C">ou</seg>s</w> <w n="6.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="6.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="6.6">f<seg phoneme="y" type="vs" value="1" rule="444" place="6">û</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="6.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="C">on</seg></w> <w n="6.8" punct="pv:10">s<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>t<pgtc id="4" weight="1" schema="GR">i<rhyme label="b" id="4" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="10" punct="pv">en</seg></rhyme></pgtc></w> ;</l>
							<l n="7" num="2.5" lm="10" met="4+6"><w n="7.1">V<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="7.3">s<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="7.4">m</w>'<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="7.6" punct="pv:10">n<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>c<seg phoneme="e" type="vs" value="1" rule="352" place="9" mp="M">e</seg>ss<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
							<l n="8" num="2.6" lm="11"><w n="8.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="C">ou</seg>s</w> <w n="8.4">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rds</w> <w n="8.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="8.6">n</w>'<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg></w> <w n="8.8">pl<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>s</w> <w n="8.9">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="9">oin</seg></w> <w n="8.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="8.11" punct="pt:11">r<pgtc id="4" weight="1" schema="GR">i<rhyme label="b" id="4" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="11" punct="pt">en</seg></rhyme></pgtc></w>.</l>
							<l n="9" num="2.7" lm="10" met="4+6"><w n="9.1">V<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="9.3">s<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="9.4">m</w>'<w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="9.6" punct="pv:10">n<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>c<seg phoneme="e" type="vs" value="1" rule="352" place="9" mp="M">e</seg>ss<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
							<l n="10" num="2.8" lm="10" met="4+6"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="10.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="3" mp="M">i</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="10.4">j</w>' <w n="10.5">n</w>'<w n="10.6"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg></w> <w n="10.7"><pgtc id="4" weight="21" schema="[CVCV[CV[CGR" part="1">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="8">oin</seg></pgtc></w> <w n="10.8"><pgtc id="4" weight="21" schema="[CVCV[CV[CGR" part="2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></pgtc></w> <w n="10.9" punct="pt:10"><pgtc id="4" weight="21" schema="[CVCV[CV[CGR" part="3">ri<rhyme label="b" id="4" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="10" punct="pt">en</seg></rhyme></pgtc></w>.</l>
						</lg>
					</div></body></text></TEI>