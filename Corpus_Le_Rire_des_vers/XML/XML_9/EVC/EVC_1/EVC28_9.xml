<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE XIV ET DERNIÈRE</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC28" modus="sp" lm_max="6" metProfile="6, (3)" form="suite périodique avec vers clausules" schema="2(aa) 1(aabbb) 1(b) 1(aaa) 1(a)" er_moy="0.29" er_max="2" er_min="0" er_mode="0(6/7)" er_moy_et="0.7" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
					<head type="tune">AIR : Garde à vous !</head>
					<lg n="1" type="distique" rhyme="aa">
						<head type="main">ANNA.</head>
						<l n="1" num="1.1" lm="3"><space unit="char" quantity="6"></space><w n="1.1">H<seg phoneme="a" type="vs" value="1" rule="339" place="1">â</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="1.2" punct="pe:3">v<pgtc id="1" weight="0" schema="R" part="1"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="pe">ou</seg>s</rhyme></pgtc></w> !</l>
						<l n="2" num="1.2" lm="3"><space unit="char" quantity="6"></space><w n="2.1">H<seg phoneme="a" type="vs" value="1" rule="339" place="1">â</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="2.2" punct="pe:3">v<pgtc id="1" weight="0" schema="R" part="1"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="pe">ou</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="quintil" rhyme="aabbb">
						<head type="main">LADY, lui donnant l'anneau.</head>
						<l n="3" num="2.1" lm="6" met="6"><w n="3.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>nn<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="vg">a</seg></w>, <w n="3.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="3.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4" punct="vg:6"><pgtc id="2" weight="2" schema="[CR" part="1">g<rhyme label="a" id="2" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="2.2" lm="6" met="6"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="4.3">d<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="4.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg><pgtc id="2" weight="2" schema="CR" part="1">g<rhyme label="a" id="2" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="5" num="2.3" lm="6" met="6"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="5.2">l</w>'<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>r</w> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.5">pl<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>s</w> <w n="5.6" punct="vg:6">d<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="b" id="3" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="vg">ou</seg>x</rhyme></pgtc></w>,</l>
						<l n="6" num="2.4" lm="3"><space unit="char" quantity="6"></space><w n="6.1">H<seg phoneme="a" type="vs" value="1" rule="339" place="1">â</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="6.2" punct="pe:3">v<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="b" id="3" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="pe">ou</seg>s</rhyme></pgtc></w> !</l>
						<l n="7" num="2.5" lm="3"><space unit="char" quantity="6"></space><w n="7.1">H<seg phoneme="a" type="vs" value="1" rule="339" place="1">â</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="7.2" punct="pe:3">v<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="pe">ou</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="3" type="vers clausule" rhyme="b">
						<head type="main">TOUS.</head>
						<l n="8" num="3.1" lm="3"><space unit="char" quantity="6"></space><w n="8.1">H<seg phoneme="a" type="vs" value="1" rule="339" place="1">â</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="8.2" punct="pe:3">v<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="pe">ou</seg>s</rhyme></pgtc></w> !</l>
						</lg>
					<lg n="4" type="distique" rhyme="aa">
						<head type="main">ARWED.</head>
						<l n="9" num="4.1" lm="6" met="6"><w n="9.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>s</w> ! <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>s</w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="9.4">gu<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="10" num="4.2" lm="6" met="6"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="3">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg></w> <w n="10.3">j</w>'<w n="10.4" punct="ps:6"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>sp<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="ps">e</seg></rhyme></pgtc></w> …</l>
						</lg>
					<lg n="5" type="tercet" rhyme="aaa">
						<head type="main">ANNA.</head>
						<l n="11" num="5.1" lm="6" met="6"><w n="11.1">J</w>'<w n="11.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg></w> <w n="11.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="11.4" punct="pe:6"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>p<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="a" id="6" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="pe">ou</seg>x</rhyme></pgtc></w> !</l>
						<l n="12" num="5.2" lm="3"><space unit="char" quantity="6"></space><w n="12.1">H<seg phoneme="a" type="vs" value="1" rule="339" place="1">â</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="12.2" punct="pe:3">v<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="a" id="6" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="pe">ou</seg>s</rhyme></pgtc></w> !</l>
						<l n="13" num="5.3" lm="3"><space unit="char" quantity="6"></space><w n="13.1">H<seg phoneme="a" type="vs" value="1" rule="339" place="1">â</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="13.2" punct="pe:3">v<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="pe">ou</seg>s</rhyme></pgtc></w> !</l>
						</lg>
					<lg n="6" type="vers clausule" rhyme="a">
						<head type="main">TOUS.</head>
						<l n="14" num="6.1" lm="3"><space unit="char" quantity="6"></space><w n="14.1">H<seg phoneme="a" type="vs" value="1" rule="339" place="1">â</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="14.2" punct="pe:3">v<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="pe">ou</seg>s</rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>