<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE VIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC12" modus="cp" lm_max="12" metProfile="8, 4+6, 6+6" form="suite de strophes" schema="3[abab] 3[aa] 1[aaa]" er_moy="1.45" er_max="14" er_min="0" er_mode="0(9/11)" er_moy_et="4.01" qr_moy="0.0" qr_max="C0" qr_mode="0(11/11)" qr_moy_et="0.0">
				<head type="tune">AIR du choix d'une femme.</head>
					<lg n="1" type="regexp" rhyme="abab">
						<head type="main">CHŒUR.</head>
						<l n="1" num="1.1" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="1.1">Pu<seg phoneme="i" type="vs" value="1" rule="490" place="1" mp="Lc">i</seg>squ</w>'<w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2" mp="Pc">en</seg></w> <w n="1.3">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="1.4">li<seg phoneme="ø" type="vs" value="1" rule="397" place="4" caesura="1">eu</seg>x</w><caesura></caesura> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="1.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r</w> <w n="1.7">n<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="C">ou</seg>s</w> <w n="1.8" punct="vg:10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="M">on</seg>v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="481" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="2.2">f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="2.3">g<seg phoneme="ɛ" type="vs" value="1" rule="304" place="3" mp="M">aî</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="443" place="5" mp="M">o</seg>b<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r</w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="P">à</seg></w> <w n="2.6">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="2.7" punct="pv:10">l<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="10" punct="pv">oi</seg>x</rhyme></pgtc></w> ;</l>
						<l n="3" num="1.3" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="3.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="3.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="C">u</seg></w> <w n="3.4">b<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>l</w><caesura></caesura> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="3.6">j<seg phoneme="wa" type="vs" value="1" rule="439" place="6" mp="M">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="402" place="7">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="3.7">h<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>n<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="481" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="4.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ts</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="4.5" punct="pt:8">v<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="8" punct="pt">oi</seg>x</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="regexp" rhyme="abab">
						<head type="main">ARWED, à Lady, il entre par le fond.</head>
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">pu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>s</w> <w n="5.4">c<seg phoneme="o" type="vs" value="1" rule="434" place="4" mp="M">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="M">an</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="P">à</seg></w> <w n="5.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="C">on</seg></w> <w n="5.7" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="9" mp="M">im</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="377" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1" punct="pe:2">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="pe">ez</seg></w> ! <w n="6.2">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>d</w> <w n="6.4">t</w>-<w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="6.7">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="6.8" punct="pi:8">v<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="247" place="8" punct="pi">œu</seg>x</rhyme></pgtc></w> ?</l>
						<l n="7" num="2.3" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="7.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="1">e</seg>l</w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="7.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="C">on</seg></w> <w n="7.4" punct="pi:4">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4" punct="pi" caesura="1">o</seg>rt</w> ?<caesura></caesura> <w n="7.5">V<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="C">ou</seg>s</w> <w n="7.6">g<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w> <w n="7.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="7.8" punct="ps:10">s<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>l<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg></rhyme></pgtc></w> …</l>
						<l n="8" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">l<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>s</w> <w n="8.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">ê</seg>t</w> <w n="8.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="8.6">v<seg phoneme="o" type="vs" value="1" rule="437" place="7">o</seg>s</w> <w n="8.7" punct="pt:8">y<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="regexp" rhyme="a">
						<head type="main">LADY, à Arwed.</head>
						<l n="9" num="3.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">n</w>'<w n="9.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="9.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg></w> <w n="9.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="6">en</seg></w> <w n="9.6" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg><pgtc id="5" weight="2" schema="CR">t<rhyme label="a" id="5" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="regexp" rhyme="aaa">
						<head type="main">ARWED.</head>
						<l n="10" num="4.1" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="P">À</seg></w> <w n="10.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="10.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406" place="4" caesura="1">eu</seg>r</w><caesura></caesura> <w n="10.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>s</w> <w n="10.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="8">en</seg></w> <w n="10.7">m</w>'<w n="10.8" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg><pgtc id="5" weight="2" schema="CR">tt<rhyme label="a" id="5" gender="f" type="e" stanza="3" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<p>(À Anna.)</p>
						<l n="11" num="4.2" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="11.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="11.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="C">ou</seg>s</w> <w n="11.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">ez</seg></w><caesura></caesura> <w n="11.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="11.6">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.7" punct="vg:10"><seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="a" stanza="4" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="vg">er</seg></rhyme></pgtc></w>,</l>
						<l n="12" num="4.3" lm="12" met="6+6"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M/mp">A</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2" mp="M/mp">e</seg>pt<seg phoneme="e" type="vs" value="1" rule="346" place="3" mp="Lp">ez</seg></w>-<w n="12.2">m<seg phoneme="wa" type="vs" value="1" rule="422" place="4">oi</seg></w> <w n="12.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="C">u</seg></w> <w n="12.4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="6" caesura="1">oin</seg>s</w><caesura></caesura> <w n="12.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="P">ou</seg>r</w> <w n="12.6">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="12.7" punct="pt:12">c<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>li<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="e" stanza="4" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="12" punct="pt">er</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="5" type="regexp" rhyme="a">
						<head type="main">LADY, à Anna.</head>
						<l n="13" num="5.1" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="13.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="C">ou</seg>s</w> <w n="13.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="13.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">ez</seg></w><caesura></caesura> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="13.5" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>f<seg phoneme="y" type="vs" value="1" rule="449" place="7" mp="M">u</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="vg">er</seg></w>, <w n="13.6">m<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="13.7" punct="pt:10">ch<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" stanza="5" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="6" type="regexp" rhyme="a">
						<head type="main">ANNA.</head>
						<l n="14" num="6.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="132" place="1">E</seg>h</w> <w n="14.2" punct="vg:2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="2" punct="vg">en</seg></w>, <w n="14.3">j</w>'<w n="14.4"><seg phoneme="i" type="vs" value="1" rule="496" place="3">y</seg></w> <w n="14.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="5">en</seg>s</w> <w n="14.6">p<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>r</w> <w n="14.7">v<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="14.8" punct="pt:8">pl<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" stanza="5" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="7" type="regexp" rhyme="aaa">
						<head type="main">ARWED.</head>
						<l n="15" num="7.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="15.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="15.2">d<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="15.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.4">d<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="15.6" punct="ps:8">m<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="m" type="a" stanza="6" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="ps">ain</seg></rhyme></pgtc></w> …</l>
						<l n="16" num="7.2" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="16.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="16.3" punct="vg:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="189" place="4" punct="vg" caesura="1">e</seg>ts</w>,<caesura></caesura> <w n="16.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="16.5">b<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>s</w> <w n="16.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="C">on</seg></w> <w n="16.7" punct="pe:10">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>st<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="m" type="e" stanza="6" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" punct="pe">in</seg></rhyme></pgtc></w> !</l>
						<l n="17" num="7.3" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="17.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="17.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="17.3">c<seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4" caesura="1">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="P">à</seg></w> <w n="17.5">b<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r</w> <w n="17.6"><pgtc id="8" weight="14" schema="[CV[CVCR" part="1">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="C">on</seg></pgtc></w> <w n="17.7" punct="pe:10"><pgtc id="8" weight="14" schema="[CV[CVCR" part="2">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>st<rhyme label="a" id="8" gender="m" type="a" stanza="6" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" punct="pe">in</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="8" type="regexp" rhyme="abab">
						<head type="main">CHŒUR.</head>
						<l n="18" num="8.1" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="18.1">Pu<seg phoneme="i" type="vs" value="1" rule="490" place="1" mp="Lc">i</seg>squ</w>'<w n="18.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2" mp="Pc">en</seg></w> <w n="18.3">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="18.4" punct="vg:4">li<seg phoneme="ø" type="vs" value="1" rule="397" place="4" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <subst hand="LG" reason="analysis" type="repetition"><del>etc.</del><add rend="hidden"><w n="18.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="18.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r</w> <w n="18.7">n<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="C">ou</seg>s</w> <w n="18.8" punct="vg:10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="M">on</seg>v<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="a" stanza="7" qr="C0"><seg phoneme="i" type="vs" value="1" rule="481" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</add></subst></l>
						<l n="19" num="8.2" lm="10" met="4+6"><space unit="char" quantity="4"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="19.2">f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="19.3">g<seg phoneme="ɛ" type="vs" value="1" rule="304" place="3" mp="M">aî</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="19.4"><seg phoneme="o" type="vs" value="1" rule="443" place="5" mp="M">o</seg>b<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r</w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="P">à</seg></w> <w n="19.6">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="19.7" punct="pv:10">l<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="a" stanza="7" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="10" punct="pv">oi</seg>x</rhyme></pgtc></w> ;</add></subst></l>
						<l n="20" num="8.3" lm="10" met="4+6"><space unit="char" quantity="4"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="20.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="20.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="20.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="C">u</seg></w> <w n="20.4">b<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>l</w><caesura></caesura> <w n="20.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="20.6">j<seg phoneme="wa" type="vs" value="1" rule="439" place="6" mp="M">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="402" place="7">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="20.7">h<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>n<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="e" stanza="7" qr="C0"><seg phoneme="i" type="vs" value="1" rule="481" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></add></subst></l>
						<l n="21" num="8.4" lm="8" met="8"><space unit="char" quantity="8"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="21.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="21.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ts</w> <w n="21.3"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="21.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="21.5" punct="pt:8">v<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="e" stanza="7" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="8" punct="pt">oi</seg>x</rhyme></pgtc></w>. </add></subst></l>
					</lg>
				</div></body></text></TEI>