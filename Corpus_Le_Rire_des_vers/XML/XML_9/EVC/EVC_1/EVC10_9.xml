<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE VII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC10" modus="cp" lm_max="10" metProfile="8, 4+6" form="suite de strophes" schema="1[ababb] 1[abab]" er_moy="2.4" er_max="8" er_min="0" er_mode="0(2/5)" er_moy_et="2.94" qr_moy="0.0" qr_max="C0" qr_mode="0(5/5)" qr_moy_et="0.0">
				<head type="tune">AIR d'Aristippe.</head>
					<lg n="1" type="regexp" rhyme="abab">
						<head type="main">LADY.</head>
						<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>ss<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="1.2">d</w>'<w n="1.3"><seg phoneme="y" type="vs" value="1" rule="452" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.4"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.5" punct="vg:8">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1" punct="vg:1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1" punct="vg">i</seg>f</w>, <w n="2.2" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>rd<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="2.3">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s</w> <w n="2.4">br<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="2.6" punct="pv:10">g<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg><pgtc id="2" weight="2" schema="CR">r<rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="397" place="10" punct="pv">eu</seg>x</rhyme></pgtc></w> ;</l>
						<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="3.2">lu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="3.3">pl<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>s</w> <w n="3.4">d</w>'<w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="3.6">m<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="3.7" punct="vg:8">br<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="4.3"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l</w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="4.5" punct="pt:8">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg><pgtc id="2" weight="2" schema="CR">r<rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="regexp" rhyme="b">
						<head type="main">ANNA.</head>
						<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="Fm">e</seg>s</w>-<w n="5.3" punct="pi:4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="pi" caesura="1">ou</seg>s</w> ?<caesura></caesura> <w n="5.4">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="5.6">cr<seg phoneme="wa" type="vs" value="1" rule="439" place="7" mp="M">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>s</w> <w n="5.7" punct="pt:10"><pgtc id="2" weight="8" schema="[CVCR">h<seg phoneme="œ" type="vs" value="1" rule="406" place="9" mp="M">eu</seg>r<rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="397" place="10" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="regexp" rhyme="abab">
						<head type="main">LADY.</head>
						<l n="6" num="3.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="6.1">S<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="6.2">g<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg></w> <w n="6.3">n</w>'<w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="6.5">qu</w>'<w n="6.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="6.7" punct="pt:8">str<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>g<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="7" num="3.2" lm="10" met="4+6"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">s<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>l</w> <w n="7.3">b<seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="4" caesura="1">eu</seg>r</w><caesura></caesura> <w n="7.4">qu</w>'<w n="7.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="7.6">pu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="7.7" punct="vg:10">d<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg><pgtc id="4" weight="2" schema="CR">r<rhyme label="b" id="4" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="vg">er</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="3.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="8.2">l</w>'<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>d</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.6">qu</w>'<w n="8.7"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>l</w> <w n="8.8" punct="ps:8"><pgtc id="3" weight="0" schema="[R"><rhyme label="a" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="8">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></pgtc></w> …</l>
						<l n="9" num="3.4" lm="10" met="4+6"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M/mp">A</seg>ppr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem/mp">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="3" mp="Lp">ez</seg></w>-<w n="9.2">lu<seg phoneme="i" type="vs" value="1" rule="490" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="9.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="9.4">qu</w>'<w n="9.5"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>l</w> <w n="9.6">d<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>t</w> <w n="9.7" punct="pt:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg><pgtc id="4" weight="2" schema="CR">r<rhyme label="b" id="4" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pt">er</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>