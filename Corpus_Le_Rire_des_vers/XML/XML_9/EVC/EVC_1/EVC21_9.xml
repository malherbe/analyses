<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE VI.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC21" modus="cp" lm_max="10" metProfile="8, 4+6" form="strophe unique" schema="1(ababcddc)" er_moy="0.5" er_max="2" er_min="0" er_mode="0(3/4)" er_moy_et="0.87" qr_moy="0.0" qr_max="C0" qr_mode="0(4/4)" qr_moy_et="0.0">
					<head type="tune">AIR de Julie.</head>
						<lg n="1" type="huitain" rhyme="ababcddc">
							<head type="main">ARWED.</head>
							<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="1.3">qu</w>'<w n="1.4"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="1.6"><seg phoneme="y" type="vs" value="1" rule="452" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="1.8" punct="ps:8">v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></pgtc></w> …</l>
							<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1" mp="C">Un</seg></w> <w n="2.2" punct="vg:2">di<seg phoneme="ø" type="vs" value="1" rule="397" place="2" punct="vg">eu</seg></w>, <w n="2.3">d<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="Lp">i</seg>t</w>-<w n="2.4" punct="vg:4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg" caesura="1">on</seg></w>,<caesura></caesura> <w n="2.5"><seg phoneme="i" type="vs" value="1" rule="496" place="5" mp="C">y</seg></w> <w n="2.6">p<seg phoneme="y" type="vs" value="1" rule="452" place="6" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t</w> <w n="2.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="2.8" punct="pt:10">m<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>ch<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="10" punct="pt">an</seg>s</rhyme></pgtc></w>.</l>
							<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="P">À</seg></w> <w n="3.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" mp="C">on</seg></w> <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>r</w><caesura></caesura> <w n="3.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="3.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="3.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.7" punct="pv:10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="M">on</seg>f<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="481" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
							<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2" mp="C">un</seg></w> <w n="4.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3" mp="M">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>t</w><caesura></caesura> <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="4.5">ci<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</w> <w n="4.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>t</w> <w n="4.7" punct="pt:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" mp="M">in</seg>d<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="M">u</seg>lg<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="10" punct="pt">en</seg>s</rhyme></pgtc></w>.</l>
							<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1" mp="P">an</seg>s</w> <w n="5.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">s<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>r</w><caesura></caesura> <w n="5.4">d<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="5.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="5.7" punct="ps:10">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="M">an</seg>sp<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="10">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg></rhyme></pgtc></w> …</l>
							<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2" mp="C">un</seg></w> <w n="6.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="6.4">l</w>'<w n="6.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="6.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="6.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>t</w> <w n="6.8" punct="pt:10">g<seg phoneme="ɛ" type="vs" value="1" rule="304" place="9" mp="M">aî</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="d" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
							<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="4"></space><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="7.2">br<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="7.5">r<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>g<seg phoneme="i" type="vs" value="1" rule="466" place="7">i</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="d" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8">en</seg>t</rhyme></pgtc></w></l>
							<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="4"></space><w n="8.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="8.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="8.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r</w> <w n="8.6">d</w>'<w n="8.7" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>sc<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						</lg>
					</div></body></text></TEI>