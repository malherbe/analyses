<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER TABLEAU.</head><head type="main_subpart">SCÈNE IV.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG5" modus="cp" lm_max="10" metProfile="8, 4+6" form="suite de strophes" schema="1[ababa] 4[aa] 1[aaa]" er_moy="7.33" er_max="44" er_min="0" er_mode="0(5/9)" er_moy_et="13.7" qr_moy="0.0" qr_max="C0" qr_mode="0(9/9)" qr_moy_et="0.0">
						<head type="tune">AIR : de la Vieille.</head>
						<lg n="1" type="regexp" rhyme="ababaaaaaaaaaaaa">
							<head type="main">BONAPARTE.</head>
							<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3" punct="vg:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="4" punct="vg">oi</seg>s</w>, <w n="1.4">s<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.5">d</w>'<w n="1.6"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="2.1">D</w>'<w n="2.2"><seg phoneme="u" type="vs" value="1" rule="425" place="1">où</seg></w> <w n="2.3">tr<seg phoneme="o" type="vs" value="1" rule="432" place="2">o</seg>p</w> <w n="2.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>g</w>-<w n="2.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">em</seg>ps</w> <w n="2.6">j</w>'<w n="2.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg></w> <w n="2.8" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>s<pgtc id="2" weight="6" schema="VCR"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rt<rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
							<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="3.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="3.2">c<seg phoneme="œ" type="vs" value="1" rule="248" place="2">œu</seg>r</w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="3">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="3.5" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="4.2">c</w>'<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4">e</seg>c</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="4.6" punct="pt:8">l<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>b<pgtc id="2" weight="6" schema="VCR"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rt<rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
							<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="4"></space><w n="5.1">L</w>'<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="5.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="5.5">m<seg phoneme="wa" type="vs" value="1" rule="422" place="6">oi</seg></w> <w n="5.6">tr<seg phoneme="o" type="vs" value="1" rule="432" place="7">o</seg>p</w> <w n="5.7" punct="pv:8">r<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
							<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="4"></space><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="6.3">l</w>'<w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>r</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="6.7" punct="pt:8"><pgtc id="3" weight="14" schema="[CVCVCR">l<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rt<rhyme label="a" id="3" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
							<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="4"></space><w n="7.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="7.2">j</w>'<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg></w> <w n="7.4">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="4">oin</seg></w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.6" punct="pt:8"><pgtc id="3" weight="14" schema="[CVCVCR">l<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rt<rhyme label="a" id="3" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
							<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="63" place="1">e</seg>rs</w> <w n="8.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" mp="M">om</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" caesura="1">on</seg>s</w><caesura></caesura> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="8.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r</w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="188" place="8">e</seg>t</w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="8.7" punct="vg:10">gl<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="9" num="1.9" lm="10" met="4+6"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="9.2">v<seg phoneme="o" type="vs" value="1" rule="437" place="2" mp="C">o</seg>s</w> <w n="9.3">tr<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>s</w> <w n="9.4">j<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="9.5">r<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M/mp">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" mp="M/mp">on</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="7" mp="Lp">ez</seg></w>-<w n="9.6">m<seg phoneme="wa" type="vs" value="1" rule="422" place="8">oi</seg></w> <w n="9.7">l</w>'<w n="9.8" punct="pt:10">h<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>st<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="e" stanza="3" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>. <del reason="analysis" hand="LG" type="repetition">(bis.)</del></l>
							<l n="10" num="1.10" lm="10" met="4+6"><subst reason="analysis" hand="LG" type="repetition"><del></del><add rend="hidden"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="10.2">v<seg phoneme="o" type="vs" value="1" rule="437" place="2" mp="C">o</seg>s</w> <w n="10.3">tr<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>s</w> <w n="10.4">j<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="10.5">r<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M/mp">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" mp="M/mp">on</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="7" mp="Lp">ez</seg></w>-<w n="10.6">m<seg phoneme="wa" type="vs" value="1" rule="422" place="8">oi</seg></w> <w n="10.7">l</w>'<w n="10.8" punct="pt:10">h<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>st<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" stanza="4" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</add></subst></l>
							<l n="11" num="1.11" lm="10" met="4+6"><w n="11.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="11.2">m</w>'<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="11.4" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="11.5">s<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg></w> <w n="11.6">j</w>'<w n="11.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg></w> <w n="11.8">b<seg phoneme="ɔ" type="vs" value="1" rule="418" place="7">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="11.9" punct="dp:10">m<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>m<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" stanza="4" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
							<l n="12" num="1.12" lm="10" met="4+6"><w n="12.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="12.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2" mp="P">an</seg>s</w> <w n="12.3">n<seg phoneme="o" type="vs" value="1" rule="437" place="3" mp="C">o</seg>s</w> <w n="12.4" punct="vg:4">j<seg phoneme="ø" type="vs" value="1" rule="397" place="4" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="12.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5" mp="P">an</seg>s</w> <w n="12.6">n<seg phoneme="o" type="vs" value="1" rule="437" place="6" mp="C">o</seg>s</w> <w n="12.7" punct="vg:8">tr<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>v<seg phoneme="o" type="vs" value="1" rule="317" place="8" punct="vg">au</seg>x</w>, <w n="12.8">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="9" mp="Lp">oi</seg>s</w>-<w n="12.9" punct="vg:10">m<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="a" stanza="5" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="422" place="10" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
							<l n="13" num="1.13" lm="8" met="8"><space unit="char" quantity="4"></space><w n="13.1">N<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>l</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="13.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="5">ain</seg>cr<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="13.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</w> <w n="13.7" punct="pt:8">t<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="e" stanza="5" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pt">oi</seg></rhyme></pgtc></w>.</l>
							<l n="14" num="1.14" lm="10" met="4+6"><w n="14.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="14.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2" mp="P">an</seg>s</w> <w n="14.3">v<seg phoneme="o" type="vs" value="1" rule="437" place="3" mp="C">o</seg>s</w> <w n="14.4" punct="vg:4">j<seg phoneme="ø" type="vs" value="1" rule="397" place="4" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="14.5">v<seg phoneme="o" type="vs" value="1" rule="437" place="5" mp="C">o</seg>s</w> <w n="14.6" punct="vg:7">tr<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>v<seg phoneme="o" type="vs" value="1" rule="317" place="7" punct="vg">au</seg>x</w>, <w n="14.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="14.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="14.9" punct="vg:10">cr<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="a" stanza="6" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="422" place="10" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
							<l n="15" num="1.15" lm="8" met="8"><space unit="char" quantity="4"></space><w n="15.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="15.2">n</w>'<w n="15.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="15.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="15.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="5">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="15.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</w> <w n="15.7" punct="pt:8">m<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="e" stanza="6" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pt">oi</seg></rhyme></pgtc></w>. <del reason="analysis" hand="LG" type="repetition">(bis.)</del></l>
							<l n="16" num="1.16" lm="8" met="8"><space unit="char" quantity="4"></space><subst reason="analysis" hand="LG" type="repetition"><del></del><add rend="hidden"><w n="16.1"><pgtc id="7" weight="44" schema="CV[C[VCV[CV[CVCV[CV[CR" part="1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</pgtc></w> <w n="16.2"><pgtc id="7" weight="44" schema="CV[C[VCV[CV[CVCV[CV[CR" part="2">n</pgtc></w>'<w n="16.3"><pgtc id="7" weight="44" schema="CV[C[VCV[CV[CVCV[CV[CR" part="3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></pgtc></w> <w n="16.4"><pgtc id="7" weight="44" schema="CV[C[VCV[CV[CVCV[CV[CR" part="4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</pgtc></w> <w n="16.5"><pgtc id="7" weight="44" schema="CV[C[VCV[CV[CVCV[CV[CR" part="5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="5">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></pgtc></w> <w n="16.6"><pgtc id="7" weight="44" schema="CV[C[VCV[CV[CVCV[CV[CR" part="6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</pgtc></w> <w n="16.7" punct="pt:8"><pgtc id="7" weight="44" schema="CV[C[VCV[CV[CVCV[CV[CR" part="7">m<rhyme label="a" id="7" gender="m" type="a" stanza="6" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pt">oi</seg></rhyme></pgtc></w>.</add></subst></l>
						</lg>
					</div></body></text></TEI>