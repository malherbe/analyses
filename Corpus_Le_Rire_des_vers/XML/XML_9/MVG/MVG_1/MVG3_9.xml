<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER TABLEAU.</head><head type="main_subpart">SCÈNE II.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG3" modus="cp" lm_max="10" metProfile="4+6, (9)" form="strophe unique" schema="1(abba)" er_moy="0.0" er_max="0" er_min="0" er_mode="0(2/2)" er_moy_et="0.0" qr_moy="0.0" qr_max="C0" qr_mode="0(2/2)" qr_moy_et="0.0">
						<head type="tune">AIR : Balancez-vous.</head>
						<lg n="1" type="quatrain" rhyme="abba">
							<head type="main">CHŒUR.</head>
							<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg>s</w>, <w n="1.2" punct="pv:4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" mp="M">en</seg>tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="pv" caesura="1">on</seg>s</w> ;<caesura></caesura> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="1.4">cl<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="1.5" punct="pv:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg>t<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pv">i</seg>t</rhyme></pgtc></w> ;</l>
							<l n="2" num="1.2" lm="9"><w n="2.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="2.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="2.3">l</w>'<w n="2.4">h<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="2.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="2.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="C">ou</seg>s</w> <w n="2.7" punct="pt:9"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>pp<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
							<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="C">Au</seg></w> <w n="3.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2" mp="M/mp">en</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="3" mp="Lp">ez</seg></w>-<w n="3.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>s</w><caesura></caesura> <w n="3.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">ch<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="3.6">s<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>t</w> <w n="3.7" punct="dp:10">f<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>d<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
							<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="4.2">d<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>t</w> <w n="4.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" mp="M">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="4.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>d</w> <w n="4.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" mp="C">on</seg></w> <w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>d</w> <w n="4.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="4.8" punct="pt:10">bru<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="490" place="10" punct="pt">i</seg>t</rhyme></pgtc></w>.</l>
						</lg>
					</div></body></text></TEI>