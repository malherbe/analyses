<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TROISIÈME TABLEAU.</head><head type="main_subpart">SCÈNE XVII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG23" modus="sp" lm_max="8" metProfile="6, (4), (3), (8), (5)" form="suite périodique avec alternance de type 2" schema="2{1(aabbccc) 1(aabb)} 1(aabbccc)" er_moy="0.12" er_max="2" er_min="0" er_mode="0(15/16)" er_moy_et="0.48" qr_moy="0.31" qr_max="N5" qr_mode="0(15/16)" qr_moy_et="1.21">
						<head type="tune">AIR : Au galop.</head>
						<lg n="1" type="septain" rhyme="aabbccc">
							<head type="main">CHŒUR.</head>
							<l n="1" num="1.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="1.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="1.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg>s</w>, <w n="1.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rd<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s</rhyme></pgtc></w></l>
							<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="2.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="2.2">j<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="2.3" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306" place="5">a</seg>ill<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="vg">on</seg>s</rhyme></pgtc></w>,</l>
							<l n="3" num="1.3" lm="4"><space unit="char" quantity="8"></space><w n="3.1">F<seg phoneme="œ" type="vs" value="1" rule="303" place="1">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3">si<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
							<l n="4" num="1.4" lm="3"><space unit="char" quantity="10"></space><w n="4.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="4.2" punct="pv:3">c<seg phoneme="o" type="vs" value="1" rule="434" place="2">o</seg>ll<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
							<l n="5" num="1.5" lm="6" met="6"><space unit="char" quantity="4"></space><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>dr<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>ts</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ll<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>rs</rhyme></pgtc></w></l>
							<l n="6" num="1.6" lm="6" met="6"><space unit="char" quantity="4"></space><w n="6.1">Fr<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>pp<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="6.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="3">o</seg>s</w> <w n="6.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ss<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="vg">eu</seg>rs</rhyme></pgtc></w>,</l>
							<l n="7" num="1.7" lm="8"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="7.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg></w> <w n="7.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="7.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s</w> <w n="7.5" punct="pt:8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="7">ain</seg>qu<pgtc id="4" weight="0" schema="R"><rhyme label="c" id="4" gender="m" type="a" qr="N5"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pt">eu</seg>rs</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="2" type="quatrain" rhyme="aabb">
							<head type="main">BONAPARTE,</head>
							<l n="8" num="2.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>l</w> <w n="8.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>s</w> <w n="8.4">s<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="8.5">r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>gu<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="e" qr="N5"><seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>r</rhyme></pgtc></w></l>
							<l n="9" num="2.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="9.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.2">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rd<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a" qr="N5"><seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
							<l n="10" num="2.3" lm="5"><space unit="char" quantity="6"></space><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="10.3">m<seg phoneme="y" type="vs" value="1" rule="452" place="3">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ti<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s</rhyme></pgtc></w></l>
							<l n="11" num="2.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="11.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="11.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r</w> <w n="11.4" punct="pt:6">fl<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>c<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="pt">on</seg>s</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="3" type="septain" rhyme="aabbccc">
							<head type="main">CHŒUR.</head>
							<l n="12" num="3.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="12.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg>s</w>, <w n="12.3" punct="vg:6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rd<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="vg">on</seg>s</rhyme></pgtc></w>, <del hand="LG" reason="analysis" type="repetition">etc.</del></l>
							<l n="13" num="3.2" lm="6" met="6"><space unit="char" quantity="4"></space><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="13.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="13.2">j<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="13.3" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306" place="5">a</seg>ill<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="vg">on</seg>s</rhyme></pgtc></w>,</add></subst></l>
							<l n="14" num="3.3" lm="4"><space unit="char" quantity="8"></space><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="14.1">F<seg phoneme="œ" type="vs" value="1" rule="303" place="1">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.3">si<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></add></subst></l>
							<l n="15" num="3.4" lm="3"><space unit="char" quantity="10"></space><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="15.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="15.2" punct="pv:3">c<seg phoneme="o" type="vs" value="1" rule="434" place="2">o</seg>ll<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pv">e</seg></rhyme></pgtc></w> ;</add></subst></l>
							<l n="16" num="3.5" lm="6" met="6"><space unit="char" quantity="4"></space><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="16.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>dr<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>ts</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ll<pgtc id="8" weight="0" schema="R"><rhyme label="c" id="8" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>rs</rhyme></pgtc></w></add></subst></l>
							<l n="17" num="3.6" lm="6" met="6"><space unit="char" quantity="4"></space><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="17.1">Fr<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>pp<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="17.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="3">o</seg>s</w> <w n="17.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ss<pgtc id="8" weight="0" schema="R"><rhyme label="c" id="8" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="vg">eu</seg>rs</rhyme></pgtc></w>,</add></subst></l>
							<l n="18" num="3.7" lm="8"><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="18.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg></w> <w n="18.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="18.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s</w> <w n="18.5" punct="pt:8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="7">ain</seg>qu<pgtc id="8" weight="0" schema="R"><rhyme label="c" id="8" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pt">eu</seg>rs</rhyme></pgtc></w>.</add></subst></l>
							
						</lg>
						<lg n="4" type="quatrain" rhyme="aabb">
							<head type="main">DARBEL</head>
							<l n="19" num="4.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="19.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="19.2">l</w>'<w n="19.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="19.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="19.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="4">en</seg>t</w> <w n="19.6"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>x</w> <w n="19.7" punct="vg:6"><pgtc id="9" weight="2" schema="[CR">m<rhyme label="a" id="9" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="6" punct="vg">ain</seg>s</rhyme></pgtc></w>,</l>
							<l n="20" num="4.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="20.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="20.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w>-<w n="20.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="20.4" punct="pv:6">h<seg phoneme="y" type="vs" value="1" rule="452" place="5">u</seg><pgtc id="9" weight="2" schema="CR">m<rhyme label="a" id="9" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="6" punct="pv">ain</seg>s</rhyme></pgtc></w> ;</l>
							<l n="21" num="4.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="21.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="21.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="21.3">n<seg phoneme="o" type="vs" value="1" rule="437" place="3">o</seg>s</w> <w n="21.4"><seg phoneme="e" type="vs" value="1" rule="169" place="4">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>s</rhyme></pgtc></w></l>
							<l n="22" num="4.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="22.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg>t</w> <w n="22.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="22.3">d<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>gts</w> <w n="22.4" punct="pt:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>g<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>rd<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="pt">i</seg>s</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="5" type="septain" rhyme="aabbccc">
							<head type="main">CHŒUR.</head>
							<l n="23" num="5.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="23.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg>s</w>, <w n="23.3" punct="vg:6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rd<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="vg">on</seg>s</rhyme></pgtc></w>, <del hand="LG" reason="analysis" type="repetition">etc.</del></l>
							<l n="24" num="5.2" lm="6" met="6"><space unit="char" quantity="4"></space><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="24.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="24.2">j<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="24.3" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306" place="5">a</seg>ill<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="vg">on</seg>s</rhyme></pgtc></w>,</add></subst></l>
							<l n="25" num="5.3" lm="4"><space unit="char" quantity="8"></space><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="25.1">F<seg phoneme="œ" type="vs" value="1" rule="303" place="1">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="25.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="25.3">si<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></add></subst></l>
							<l n="26" num="5.4" lm="3"><space unit="char" quantity="10"></space><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="26.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="26.2" punct="pv:3">c<seg phoneme="o" type="vs" value="1" rule="434" place="2">o</seg>ll<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pv">e</seg></rhyme></pgtc></w> ;</add></subst></l>
							<l n="27" num="5.5" lm="6" met="6"><space unit="char" quantity="4"></space><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="27.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="27.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>dr<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>ts</w> <w n="27.3"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ll<pgtc id="13" weight="0" schema="R"><rhyme label="c" id="13" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>rs</rhyme></pgtc></w></add></subst></l>
							<l n="28" num="5.6" lm="6" met="6"><space unit="char" quantity="4"></space><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="28.1">Fr<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>pp<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="28.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="3">o</seg>s</w> <w n="28.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ss<pgtc id="13" weight="0" schema="R"><rhyme label="c" id="13" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="vg">eu</seg>rs</rhyme></pgtc></w>,</add></subst></l>
							<l n="29" num="5.7" lm="8"><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="29.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="29.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg></w> <w n="29.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="29.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s</w> <w n="29.5" punct="pt:8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="7">ain</seg>qu<pgtc id="13" weight="0" schema="R"><rhyme label="c" id="13" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pt">eu</seg>rs</rhyme></pgtc></w>.</add></subst></l>
							
						</lg>
					</div></body></text></TEI>