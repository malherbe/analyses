<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">27, 28 ET 29 JUILLET, TABLEAU ÉPISODIQUE DES TROIS JOURNÉES.</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ARA" sort="1">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<author key="DUV" sort="2">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>495 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
						<author>ARAGO ET DUVERT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=xjVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
								<author>ARAGO ET DUVERT</author>
								<repository>Österreichische Nationalbibliothek</repository>
								<idno type="URI">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886206</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : U+02BC MODIFIER LETTER APOSTROPHE) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<change when="2021-06-14" who="RR">Quelques corrections concernant la distribution du signe signe ʼ (UNICODE : ʼ).</change>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TROISIÈME JOURNÉE</head><head type="main_subpart">SCÈNE PREMIÈRE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="AED20" modus="cp" lm_max="10" metProfile="8, 4, 4+6, (6)" form="suite de strophes" schema="1[abab] 2[aaa] 1[abba]" er_moy="0.75" er_max="2" er_min="0" er_mode="0(4/8)" er_moy_et="0.83" qr_moy="0.0" qr_max="C0" qr_mode="0(8/8)" qr_moy_et="0.0">
			<head type="tune">AIR : de Marianne.</head>
				<lg n="1" type="regexp" rhyme="ababaaaaaaabba">
					<head type="main">PRUNEAU.</head>
					<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="1.2">t<seg phoneme="a" type="vs" value="1" rule="339" place="2">â</seg>ch<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="1.6" punct="vg:8">p<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>t<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="2.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="2.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="2.3">s<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>r</w> <w n="2.4">l</w>'<w n="2.5" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg><pgtc id="2" weight="2" schema="CR">ch<rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pv">é</seg></rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="3.1">V<seg phoneme="wa" type="vs" value="1" rule="419" place="1">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="3.2">qu</w>' <w n="3.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s</w> <w n="3.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="3.7" punct="vg:8">c<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">m<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="4.5" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="2" weight="2" schema="CR">ch<rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="4" met="4"><space unit="char" quantity="12"></space><w n="5.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="5.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="5.3" punct="ps:4">b<pgtc id="3" weight="1" schema="GR">i<rhyme label="a" id="3" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4" punct="ps">en</seg></rhyme></pgtc></w> …</l>
					<l n="6" num="1.6" lm="4" met="4"><space unit="char" quantity="12"></space><w n="6.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="6.2">nʼ</w> <w n="6.3">tr<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="6.4" punct="vg:4">r<pgtc id="3" weight="1" schema="GR">i<rhyme label="a" id="3" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="4" punct="vg">en</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">J</w>'<w n="7.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="7.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5" mp="C">un</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>t</w> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="7.7" punct="pt:10">ch<pgtc id="3" weight="1" schema="GR">i<rhyme label="a" id="3" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="10" punct="pt">en</seg></rhyme></pgtc></w>.</l>
					<l n="8" num="1.8" lm="4" met="4"><space unit="char" quantity="12"></space><w n="8.1">Dʼ</w> <w n="8.2">l</w>'<w n="8.3" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="2" punct="vg">en</seg>t</w>, <w n="8.4">f<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>t</w> <w n="8.5" punct="vg:4">v<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="4" punct="vg">oi</seg>r</rhyme></pgtc></w>,</l>
					<l n="9" num="1.9" lm="4" met="4"><space unit="char" quantity="12"></space><w n="9.1">Pl<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="1">ein</seg></w> <w n="9.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="9.3" punct="vg:4">t<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="e" stanza="3" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="4" punct="vg">oi</seg>r</rhyme></pgtc></w>,</l>
					<l n="10" num="1.10" lm="10" met="4+6"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="10.2">c</w>'<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="10.5">ç<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg></w><caesura></caesura> <w n="10.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.7">n<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="C">ou</seg>s</w> <w n="10.8">v<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="M">ou</seg>li<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>s</w> <w n="10.9" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>v<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="10" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
					<l n="11" num="1.11" lm="8" met="8"><space unit="char" quantity="4"></space><w n="11.1">F<seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>t</w> <w n="11.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="11.3">fl<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>r</w> <w n="11.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="11.5" punct="vg:8">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rv<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" stanza="4" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="1.12" lm="8" met="8"><space unit="char" quantity="4"></space><w n="12.1" punct="vg:2"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg></w>, <w n="12.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>t</w> <w n="12.3">ç<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="12.4">f<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>t</w> <w n="12.5" punct="pv:8">r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>c<pgtc id="6" weight="2" schema="CR">t<rhyme label="b" id="6" gender="m" type="a" stanza="4" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pv">é</seg></rhyme></pgtc></w> ;</l>
					<l n="13" num="1.13" lm="8" met="8"><space unit="char" quantity="4"></space><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>x</w> <w n="13.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>n<seg phoneme="w a" type="vs" value="1" rule="AED20_1" place="3">oi</seg>nʼs</w> <w n="13.3">jʼ</w> <w n="13.4">n</w>'<w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg></w> <w n="13.6">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="5">en</seg></w> <w n="13.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>r<pgtc id="6" weight="2" schema="CR">t<rhyme label="b" id="6" gender="m" type="e" stanza="4" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></rhyme></pgtc></w></l>
					<l n="14" num="1.14" lm="6"><space unit="char" quantity="8"></space><w n="14.1">Qu</w>'<w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="14.3">dʼ</w> <w n="14.4">l<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>rs</w> <w n="14.5">b<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="189" place="4">e</seg>ts</w> <w n="14.6">dʼ</w> <w n="14.7" punct="pt:6">p<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>l<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" stanza="4" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
		</div></body></text></TEI>