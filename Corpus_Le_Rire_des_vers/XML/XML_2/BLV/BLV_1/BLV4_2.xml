<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BANDEAU</title>
				<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="BLL" sort="1">
					<name>
						<forename>Jean-Nicolas</forename>
						<surname>BOUILLY</surname>
					</name>
					<date from="1763" to="1842">1763-1842</date>
				</author>
				<author key="VAN" sort="2">
					<name>
						<forename>Émile</forename>
						<surname>VANDERBURCH</surname>
						<addname type="other">Émile VANDERBURCH</addname>
						<addname type="other">Émile VANDER-BURCH</addname>
						<addname type="other">Émile VANDERBURCK</addname>
						<addname type="other">Émile VAN DER BURCK</addname>
					</name>
					<date from="1794" to="1862">1794-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>168 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE BANDEAU</title>
						<author>BOUILLY ET EM. VANDERBURCH</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GALLICA</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9782259n.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La Bibliothèque nationale de France</repository>
								<idno type="URL">https ://catalogue.bnf.fr/ark :/12148/cb31531335q</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">21 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BLV4">
	<head type="tune">AIR du Vaudeville de Partie carrée.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.3">cl<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w>, <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="1.5">s<seg phoneme="o" type="vs" value="1" rule="434">o</seg>ll<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.2">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="300">ai</seg>s</w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.4">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg>s</w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="2.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> ;</l>
		<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="3.2">l</w>'<w n="3.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.4">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="3.5">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="3.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="4" num="1.4"><w n="4.1">Qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.2">l</w>'<w n="4.3">h<seg phoneme="i" type="vs" value="1" rule="496">y</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="220">en</seg></w> <w n="4.4">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>ge<seg phoneme="a" type="vs" value="1" rule="315">a</seg></w> <w n="4.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w>.</l>
		<l n="5" num="1.5"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="5.2">d</w>'<w n="5.3"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="5.4">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="5.5">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="5.6">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="e" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>, <w n="5.8">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="5.9">c<seg phoneme="o" type="vs" value="1" rule="317">au</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
		<l n="6" num="1.6"><w n="6.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="6.2">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>s</w> <w n="6.3">pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w>, <w n="6.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.5">b<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="6.7">l</w>'<w n="6.8"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r</w> :</l>
		<l n="7" num="1.7"><w n="7.1">P<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>-<w n="7.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="7.3">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="7.4">l</w>'<w n="7.5">h<seg phoneme="i" type="vs" value="1" rule="496">y</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="220">en</seg></w> <w n="7.6">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="7.7">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.8">r<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="8" num="1.8"><w n="8.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="8.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="8.3">n</w>'<w n="8.4"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="8.5">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="8.7">cl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r</w> ?</l>
	</lg>
</div></body></text></TEI>