<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BANDEAU</title>
				<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="BLL" sort="1">
					<name>
						<forename>Jean-Nicolas</forename>
						<surname>BOUILLY</surname>
					</name>
					<date from="1763" to="1842">1763-1842</date>
				</author>
				<author key="VAN" sort="2">
					<name>
						<forename>Émile</forename>
						<surname>VANDERBURCH</surname>
						<addname type="other">Émile VANDERBURCH</addname>
						<addname type="other">Émile VANDER-BURCH</addname>
						<addname type="other">Émile VANDERBURCK</addname>
						<addname type="other">Émile VAN DER BURCK</addname>
					</name>
					<date from="1794" to="1862">1794-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>168 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE BANDEAU</title>
						<author>BOUILLY ET EM. VANDERBURCH</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GALLICA</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9782259n.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La Bibliothèque nationale de France</repository>
								<idno type="URL">https ://catalogue.bnf.fr/ark :/12148/cb31531335q</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">21 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BLV15">
	<head type="tune">AIR de Galope.</head>
		<div type="section" n="1">
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="1.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.3">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.4">j</w>'<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
				<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.2">l</w>'<w n="2.3">h<seg phoneme="i" type="vs" value="1" rule="496">y</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="220">en</seg></w>, <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.5">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.6">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>,</l>
				<l n="3" num="1.3"><w n="3.1">N</w>'<w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
				<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.2">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="4.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.4">l</w>'<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> !</l>
			</lg>
		</div>
		<div type="section" n="2">
			<head type="main">ENSEMBLE.</head>
			<lg n="1">
			<head type="speaker">SAINT-VALERY.</head>
				<l n="5" num="1.1"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="5.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.3">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.4">j</w>'<w n="5.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
				<l n="6" num="1.2"><w n="6.1">Qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.2">l</w>'<w n="6.3">h<seg phoneme="i" type="vs" value="1" rule="496">y</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="220">en</seg></w>, <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="6.5">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.6">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>,</l> 
				<l n="7" num="1.3"><w n="7.1">N</w>'<w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l> 
				<l n="8" num="1.4"><w n="8.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.2">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="8.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.4">l</w>'<w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>.</l>
			</lg>
			<lg n="2">
			<head type="speaker">LAPLACE et JOSEPH.</head>
				<l n="9" num="2.1"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="9.2">c<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="9.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="9.4">l</w>'<w n="9.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
				<l n="10" num="2.2"><w n="10.1">Qu</w>'<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.3">c<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="10.5">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.6">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> ;</l>
				<l n="11" num="2.3"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="11.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="11.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
				<l n="12" num="2.4"><w n="12.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.2">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="12.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.4">l</w>'<w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> !</l>
			</lg>
			<lg n="3">
			<head type="speaker">AMÉLIE.</head>
				<l n="13" num="3.1"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="13.2">l</w>'<w n="13.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="13.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="13.5">m</w>'<w n="13.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
				<l n="14" num="3.2"><w n="14.1"><seg phoneme="w" type="sc" value="0" rule="430">Ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="14.2">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.3">c<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="14.5">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.6">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> ;</l>
				<l n="15" num="3.3"><w n="15.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="15.2">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="15.3">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
				<l n="16" num="3.4"><w n="16.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.2">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="16.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.4">l</w>'<w n="16.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>.</l>
			</lg>
		</div>
</div></body></text></TEI>