<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BANDEAU</title>
				<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="BLL" sort="1">
					<name>
						<forename>Jean-Nicolas</forename>
						<surname>BOUILLY</surname>
					</name>
					<date from="1763" to="1842">1763-1842</date>
				</author>
				<author key="VAN" sort="2">
					<name>
						<forename>Émile</forename>
						<surname>VANDERBURCH</surname>
						<addname type="other">Émile VANDERBURCH</addname>
						<addname type="other">Émile VANDER-BURCH</addname>
						<addname type="other">Émile VANDERBURCK</addname>
						<addname type="other">Émile VAN DER BURCK</addname>
					</name>
					<date from="1794" to="1862">1794-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>168 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE BANDEAU</title>
						<author>BOUILLY ET EM. VANDERBURCH</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GALLICA</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9782259n.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La Bibliothèque nationale de France</repository>
								<idno type="URL">https ://catalogue.bnf.fr/ark :/12148/cb31531335q</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">21 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BLV2">
	<head type="tune">AIR : De sommeiller encor, ma chère.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.2">b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>z<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.3">f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
		<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> ! <w n="2.2">s</w>'<w n="2.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.5">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> !</l>
		<l n="3" num="1.3"><w n="3.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="3.2">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="3.3">j</w>'<w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="3.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.7">f<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="4" num="1.4"><w n="4.1">J</w>'<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="4.3">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="4.4">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="4.6">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="4.7">f<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>.</l>
		<l n="5" num="1.5"><w n="5.1">L</w>'<w n="5.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>pr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.3">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="5.6">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="5.7">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
		<l n="6" num="1.6"><w n="6.1">C</w>'<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="6.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="6.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w> <w n="6.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="6.6">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.7">br<seg phoneme="u" type="vs" value="1" rule="426">ou</seg>ill<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w> ;</l>
		<l n="7" num="1.7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="7.2">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="7.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.6">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="8" num="1.8"><w n="8.1">C</w>'<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="8.3">j<seg phoneme="w" type="sc" value="0" rule="430">ou</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="8.5">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>-<w n="8.6">m<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w>.</l>
	</lg>
</div></body></text></TEI>