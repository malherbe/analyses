<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL8">
	<head type="tune">AIR : C'est l'amour.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="1.2">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.3">g<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="1.4">d</w>'<w n="1.5"><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="1.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w></l>
		<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.3">b<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>gr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ph<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
		<l n="3" num="1.3"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="3.2">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.3">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.4">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.5">t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>f<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="4" num="1.4"><w n="4.1">D<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="4.2">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>.</l>
		<l n="5" num="1.5"><w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="5.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="5.3">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.4">p<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <hi rend="ital"><w n="5.5">D<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sm<seg phoneme="a" type="vs" value="1" rule="339">a</seg>z<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></hi> ? …</l>
	</lg>
	<lg n="2">
		<head type="speaker">LUDOVIC.</head>
		<l n="6" num="2.1"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="6.2">th<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.3">c</w>'<w n="6.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.7">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rts</w>…</l>
	</lg>
	<lg n="3">
		<head type="speaker">DUBOURG, souriant.</head>
		<l n="7" num="3.1"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w>'<w n="7.4">h<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="7.5">p<seg phoneme="e" type="vs" value="1" rule="408">é</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="8" num="3.2"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="8.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w> <w n="8.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.4">l</w>'<w n="8.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>gu<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="8.6">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.8">c<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rs</w>.</l>
	</lg>
	<lg n="4">
		<head type="speaker">LUDOVIC.</head>
		<l n="9" num="4.1"><hi rend="ital"><w n="9.1">H<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></hi>, <w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="9.3">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.4">j<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="10" num="4.2"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>, <w n="10.2">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.3">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>, <w n="10.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.5">g<seg phoneme="u" type="vs" value="1" rule="424">oû</seg>t</w></l>
		<l n="11" num="4.3"><w n="11.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.2">d<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="11.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="11.4">c<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="12" num="4.4"><w n="12.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="12.3">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w>…</l>
	</lg>
	<lg n="5">
		<head type="speaker">DUBOURG.</head>
		<l n="13" num="5.1"><w n="13.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.2">l</w>'<w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="13.4">p<seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>… <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="132">E</seg>h</w> <w n="13.6">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> !</l>
		<l n="14" num="5.2"><w n="14.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="14.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w>, <w n="14.4">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.5">t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.6">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="15" num="5.3"><w n="15.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.2">l</w>'<w n="15.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.5">p<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="16" num="5.4"><w n="16.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.2">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="16.3">f<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.4">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> ! …</l>
		<l n="17" num="5.5"><hi rend="ital"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="354">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>dr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></hi>, <w n="17.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="17.3">r<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w></l>
		<l n="18" num="5.6"><w n="18.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w>'<w n="18.3">h<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="18.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="18.5">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.6">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="18.7">f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> ;</l>
		<l n="19" num="5.7"><w n="19.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="19.3">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>pt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w></l>
		<l n="20" num="5.8"><w n="20.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="20.2">n</w>'<w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="20.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="20.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="20.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.8">f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> :</l>
		<l n="21" num="5.9"><w n="21.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="21.2">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w> <w n="21.3">qu</w>'<w n="21.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="22" num="5.10"><w n="22.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="22.2">dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="22.3"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="22.4">l</w>'<w n="22.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="22.6">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w></l>
		<l n="23" num="5.11"><w n="23.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.2">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="23.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.4">Gr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="24" num="5.12"><w n="24.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>, <w n="24.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="24.3">c<seg phoneme="e" type="vs" value="1" rule="249">œ</seg>t<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>…</l>
		<l n="25" num="5.13"><hi rend="ital"><w n="25.1">D<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>lm<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></hi>,</l>
		<l n="26" num="5.14"><w n="26.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="26.2">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="e" type="vs" value="1" rule="352">e</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w></l>
		<l n="27" num="5.15"><w n="27.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="27.2">p<seg phoneme="o" type="vs" value="1" rule="443">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="27.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="449">u</seg>str<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="27.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="27.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="27.6">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="28" num="5.16"><w n="28.1">R<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d</w> <w n="28.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="28.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="28.4">d</w>'<w n="28.5"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>…</l>
		<l n="29" num="5.17"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="29.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>p<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>… <w n="29.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="29.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> !</l>
		<l n="30" num="5.18"><w n="30.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="30.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="30.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="30.4">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="30.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <hi rend="ital"><w n="30.6">B<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ch<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></hi>,</l>
		<l n="31" num="5.19"><w n="31.1">D<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="31.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="i" type="vs" value="1" rule="492">y</seg>s</w> <w n="31.3">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="31.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w>'<w n="31.5">h<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>,</l>
		<l n="32" num="5.20"><w n="32.1">C<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="32.2">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="33" num="5.21"><w n="33.1">Ç<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="33.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="34" num="5.22"><w n="34.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="34.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="34.3">l</w>'<w n="34.4"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="34.5">d</w>'<w n="34.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ɥ" type="sc" value="0" rule="454">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>.</l>
		<l n="35" num="5.23"><w n="35.1">L<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>, <w n="35.2">c</w>'<w n="35.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="35.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="35.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <hi rend="ital"><w n="35.6">J<seg phoneme="y" type="vs" value="1" rule="449">u</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w></hi>…</l>
	</lg>
	<lg n="6">
	<head type="speaker">LUDOVIC.</head>
	  <l part="I" n="36" num="6.1"><w n="36.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="36.2">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w> <w n="36.3">d</w>'<w n="36.4">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> ? … </l>
	</lg>
	<lg n="7">
	<head type="speaker">DUDOURG.</head>
		<l part="F" n="36"><w n="36.5"><seg phoneme="e" type="vs" value="1" rule="132">E</seg>h</w> ! <w n="36.6"><seg phoneme="w" type="sc" value="0" rule="430">ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> ! …</l>
		<l n="37" num="7.1"><w n="37.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="37.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="37.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="37.4">F<seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w></l>
		<l n="38" num="7.2"><w n="38.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="38.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="38.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w> <w n="38.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w>'<w n="38.5">h<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> ;</l>
		<l n="39" num="7.3"><w n="39.1">T<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lm<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="39.2">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="39.3">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="39.4">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="39.5">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w>,</l>
		<l n="40" num="7.4"><w n="40.1">N</w>'<w n="40.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>, <w n="40.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="40.4">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="40.5">qu</w>'<w n="40.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="40.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ff<seg phoneme="i" type="vs" value="1" rule="467">i</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="41" num="7.5"><w n="41.1">Qu</w>'<w n="41.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="41.3">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lt<seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="41.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="41.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="42" num="7.6"><w n="42.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="42.2">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="42.3">D<seg phoneme="e" type="vs" value="1" rule="408">é</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> !</l>
	</lg>
	<lg n="8">
	<head type="speaker">LUDOVIC.</head>
		<l n="43" num="8.1"><w n="43.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="43.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="43.3">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="43.4">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="43.5">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="44" num="8.2"><w n="44.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="44.2">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="44.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w> <w n="44.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="44.5">j<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="44.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> ?</l>
	</lg>
	<lg n="9">
	<head type="speaker">DUBOURG.</head>
		<l n="45" num="9.1"><w n="45.1">C</w>'<w n="45.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <hi rend="ital"><w n="45.3"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>g<seg phoneme="y" type="vs" value="1" rule="447">u</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></hi>, <w n="45.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="45.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w>-<w n="45.6">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="46" num="9.2"><w n="46.1">D<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="46.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="46.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="46.4">l</w>'<w n="46.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>bb<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="46.6">Ch<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> ;</l>
		<l n="47" num="9.3"><w n="47.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="47.2">d<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="47.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="47.4">f<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w></l>
		<l n="48" num="9.4"><w n="48.1">M<seg phoneme="œ" type="vs" value="1" rule="150">on</seg>s<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="396">eu</seg>r</w> <w n="48.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="48.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="48.4">l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w></l>
		<l n="49" num="9.5"><w n="49.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="49.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="49.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="49.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="49.5">b<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w></l>
		<l n="50" num="9.6"><w n="50.1">Qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="50.2">f<seg phoneme="œ" type="vs" value="1" rule="303">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="50.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="50.4">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>…</l>
		<l n="51" num="9.7"><w n="51.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="51.2">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="51.3">g<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="51.4">d</w>'<w n="51.5"><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="51.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w></l>
		<l n="52" num="9.8"><w n="52.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="52.2">t</w>'<w n="52.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="52.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="52.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="52.6">b<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>gr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ph<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
		<l n="53" num="9.9"><w n="53.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="53.2">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="53.3">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="53.4">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="53.5">t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="53.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>f<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="54" num="9.10"><w n="54.1">D<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="54.2">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="54.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="54.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>.</l>
	</lg> 
</div></body></text></TEI>