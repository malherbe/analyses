<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE III.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC17">
					<head type="tune">Même air.</head>
						<lg n="1">
							<head type="main">ARWED.</head>
							<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="1.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="1.3">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="1.4">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="1.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="1.6">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.7">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> !</l>
							<l n="2" num="1.2"><w n="2.1">J</w>'<w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>ç<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="2.4">l</w>'<w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="2.6">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.7">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="2.8">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> !</l>
							<l n="3" num="1.3"><w n="3.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="3.2">j</w>'<w n="3.3"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xpl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="3.5">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
							<l n="4" num="1.4"><w n="4.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.2">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.3">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="4.5">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.6">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>.</l>
							<l n="5" num="1.5"><w n="5.1">V<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="5.2"><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>, <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="169">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="5.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="5.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
							<l n="6" num="1.6"><w n="6.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="6.2">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.3">l</w>'<w n="6.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="w" type="sc" value="0" rule="430">ou</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="6.5">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> !</l>
							<l n="7" num="1.7"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="7.2">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="7.3">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.4">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="7.5">n</w>'<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="7.7">qu</w>'<w n="7.8"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="7.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
							<l n="8" num="1.8"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="8.2">p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="8.3">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.4">m</w>'<w n="8.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> !</l>
						</lg>
					</div></body></text></TEI>