<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE31">			
	
	
	<head type="main">TOUS</head>

			<head type="tune">AIR : Grace au vent.</head>

		   <lg n="1"><l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="1.2">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="1.3"><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>,</l>
			<l n="2" num="1.2"><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.3">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w></l>
			<l n="3" num="1.3"><w n="3.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="3.3">c<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> !</l>
		   <l n="4" num="1.4"> <w n="4.1">N<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="4.2">v<seg phoneme="ø" type="vs" value="1" rule="247">œu</seg>x</w>,</l>
			<l n="5" num="1.5"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="5.2">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="5.3">r<seg phoneme="ə" type="vi" value="1" rule="356">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w></l>
			<l n="6" num="1.6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="6.2">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="6.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ts</w>,</l>
			<l n="7" num="1.7"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>pt<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w>,</l>
			<l n="8" num="1.8"><w n="8.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="8.2">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>,</l>
			<l n="9" num="1.9"><w n="9.1">J<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w></l>
			<l n="10" num="1.10"><w n="10.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="10.2">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
			<l n="11" num="1.11"><w n="11.1">N<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="11.3">s<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.4">d<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
			<l n="12" num="1.12"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="12.2">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="12.3">n<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>,</l>
			<l n="13" num="1.13"><w n="13.1">L<seg phoneme="i" type="vs" value="1" rule="467">i</seg>br<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>, <w n="13.2">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> ...</l>
			<l n="14" num="1.14"><w n="14.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="14.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w>,</l>
			<l n="15" num="1.15"><w n="15.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="15.2">b<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> !</l></lg>

		   <lg n="2"><l n="16" num="2.1"><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="16.2">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.3">v<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>-<w n="16.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> ?</l>
			<l n="17" num="2.2"><w n="17.1">Qu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> ! ... <w n="17.2">c</w>’<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="17.4">l</w>’<w n="17.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.6">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> ....</l>
			<l n="18" num="2.3"><w n="18.1"><seg phoneme="w" type="sc" value="0" rule="430">Ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> ! ... <w n="18.4"><seg phoneme="w" type="sc" value="0" rule="430">ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="18.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.6">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> !</l>
			<l n="19" num="2.4"><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="19.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="19.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="19.5">l</w>’<w n="19.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> ?</l></lg>

			<lg n="3"><l n="20" num="3.1"><w n="20.1">R<seg phoneme="e" type="vs" value="1" rule="408">é</seg>gn<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <w n="20.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
			<l n="21" num="3.2"><w n="21.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="21.2">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.3">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
			<l n="22" num="3.3"><w n="22.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="22.2">s<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="22.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w></l>
			<l n="23" num="3.4"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="23.2">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="23.3">g<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> !</l>
			<l n="24" num="3.5"><w n="24.1">R<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="24.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> ! <w n="24.3">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="24.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> !</l></lg></div></body></text></TEI>