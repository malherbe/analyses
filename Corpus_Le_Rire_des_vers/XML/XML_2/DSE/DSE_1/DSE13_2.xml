<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE13">			
	
	<head type="main">FIGARO.</head>

			<p>Eh ! eh !.. pour la filleule d’une danseuse, elle n’est pas légère... Ah ! ah !..</p>

			<head type="tune">Air d’Yelva.</head>

				<lg n="1"><l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="1.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> ! ... <w n="1.3">C</w>’<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="1.6">qu</w>’<w n="1.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.8">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="1.9">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="2.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.3">c<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="2.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t</w> <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.6">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="2.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="?" type="va" value="1" rule="161">en</seg>t</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> ! <w n="3.2">vr<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="3.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="3.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="4.2">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.4">cl<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="4.6">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="4.7">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> ?</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="5.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="5.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.4">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="5.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w> <w n="5.6">r<seg phoneme="ə" type="vi" value="1" rule="356">e</seg>pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w></l>
					<l n="6" num="1.6"><w n="6.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.2">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.3">f<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="6.4">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="6.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>s</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="7.2">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="7.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.6">cl<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
					<l n="8" num="1.8"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="8.2">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="8.3">d</w>’<w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="8.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.6">chr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w>.</l></lg>
			</div></body></text></TEI>