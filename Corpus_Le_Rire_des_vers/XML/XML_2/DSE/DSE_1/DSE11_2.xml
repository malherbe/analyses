<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE11">
	
			<head type="main">FIGARO.</head>

		<head type="tune">AIR : Ah ! bravo ! Figaro !</head>
			
				<lg n="1"><l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="1.2">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="o" type="vs" value="1" rule="443">o</seg></w> ! <w n="1.3">F<seg phoneme="i" type="vs" value="1" rule="467">i</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="o" type="vs" value="1" rule="443">o</seg></w> !</l>
					<l n="2" num="1.2"><w n="2.1">Br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="o" type="vs" value="1" rule="443">o</seg></w> ! .... <w n="2.2">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="o" type="vs" value="1" rule="443">o</seg></w> !</l>
					<l n="3" num="1.3"><w n="3.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="3.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.4">s<seg phoneme="o" type="vs" value="1" rule="437">o</seg>ts</w> <w n="3.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="3.6">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="3.7">cr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> : <w n="3.8">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="o" type="vs" value="1" rule="443">o</seg></w> :</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="4.2">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="o" type="vs" value="1" rule="443">o</seg></w> ! <w n="4.3">F<seg phoneme="i" type="vs" value="1" rule="467">i</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="o" type="vs" value="1" rule="443">o</seg></w> !</l>
					<l n="5" num="1.5"><w n="5.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.3">b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.4">s</w>’<w n="5.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1">Ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.2">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="6.3">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>,</l>
					<l n="7" num="1.7"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="7.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.5">pl<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="8" num="1.8"><w n="8.1">Pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="8.2">qu</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="8.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="8.5">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> !</l>
					<l n="9" num="1.9"><w n="9.1">B<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rb<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="9.2">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rn<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="10" num="1.10"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="353">E</seg>x<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ct</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>nn<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="11" num="1.11"><w n="11.1">J</w>’<w n="11.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>scr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="11.3">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="11.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.5">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="12" num="1.12"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.2">s<seg phoneme="o" type="vs" value="1" rule="437">o</seg>ts</w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="12.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.5">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> ;</l>
					<l n="13" num="1.13"><w n="13.1">G<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="13.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="14.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>,</l>
					<l n="15" num="1.15"><w n="15.1">R<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="15.2">p<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> !</l>
					<l n="16" num="1.16"><w n="16.1">J<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>gl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="16.2">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
					<l n="17" num="1.17"><w n="17.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="17.3">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w>,</l>
					<l n="18" num="1.18"><w n="18.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="18.3">r<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w></l>
					<l n="19" num="1.19"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="19.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w>.</l>
					<l ana="unanalyzable" n="20" num="1.20">Ah ! bravo ! Figaro, etc.</l></lg>
			</div></body></text></TEI>