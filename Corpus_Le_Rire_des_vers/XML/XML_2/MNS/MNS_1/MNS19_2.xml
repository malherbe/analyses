<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS19">
	<head type="tune">Air de Julie.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1"><seg phoneme="w" type="sc" value="0" rule="430">Ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="1.2">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.3">m</w>'<w n="1.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="1.5">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w> <w n="1.7">qu</w>'<w n="1.8"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="1.9">m</w>'<w n="1.10">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ; </l>
		<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.2">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="2.3">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="2.5">j</w>'<w n="2.6">p<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="2.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="2.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, </l>
		<l n="3" num="1.3"><w n="3.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="3.2">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.3">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.4">n</w>' <w n="3.5">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.6">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, </l>
		<l n="4" num="1.4"><w n="4.1">P<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="4.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="4.3">l</w>' <w n="4.4">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ; </l>
		<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.2">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.3">g<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w>, <w n="5.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="5.6">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, </l>
		<l n="6" num="1.6"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ttr</w>' <w n="6.4">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.5">b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ; </l>
		<l n="7" num="1.7"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="7.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.3">j<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ! <w n="7.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w></l>
		<l n="8" num="1.8"><w n="8.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.2">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="8.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="8.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="310">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
	</lg>
</div></body></text></TEI>