<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS6">
	<head type="tune">Air. : Une deux, trois, etc.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.2">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="1.3">c</w>'<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w>… <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="1.6"><seg phoneme="i" type="vs" value="1" rule="467">I</seg></w> ;</l>
	</lg>
	<lg n="2">
		<head type="speaker">MISS SARA.</head>
		<l n="2" num="2.1"><w n="2.1">M<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="2.2">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.3">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>.</l>
		<l part="I" n="3" num="2.2"><w n="3.1"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <subst hand="RR" reason="analysis" type="phonemization"><del>S</del><add rend="hidden"><w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="49">è</seg>s</w></add></subst>. </l>  
	</lg>
	<lg n="3">
		<head type="speaker">TOUTES.</head>
		<l part="F" n="3"><w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">E</seg>st</w>-<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.5">b<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> :</l> 
	</lg>
	<lg n="4">
		<head type="speaker">HÉLÈNE.</head>
		<l n="4" num="4.1"><w n="4.1">C</w>'<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="4.4">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, … <w n="4.5">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.6">t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>… <w n="4.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <subst hand="RR" reason="analysis" type="phonemization"><del>B</del><add rend="hidden"><w n="4.8">b<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w></add></subst>.</l>
		<l n="5" num="4.2"><w n="5.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="5.2">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> ! <w n="5.3">j</w>'<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="5.5">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="5.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>  
	</lg>
	<lg n="5">
		<head type="speaker">TOUTES.</head>
		<l n="6" num="5.1"><w n="6.1">C</w>'<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>, <w n="6.6">N<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l> 
	</lg>
	<lg n="6">
		<head type="speaker">NINETTE.</head> 
		<l n="7" num="6.1"><w n="7.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="7.2">c</w>'<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="7.5">Cl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="7.6">p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>x</w>… <w n="7.7">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="o" type="vs" value="1" rule="443">o</seg></w> !</l>
		<l n="8" num="6.2"><w n="8.1">V<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="8.2">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="8.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w>,</l>
		<l n="9" num="6.3"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w>.</l>
	</lg>
	<lg n="7">
	<head type="speaker">HÉLÈNE à Ninette.</head>
		<l n="10" num="7.1"><w n="10.1">C</w>'<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="10.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="10.5">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="10.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l> 
	</lg>
	<lg n="8">
		<head type="speaker">NINETTE,</head>
		<l n="11" num="8.1"><w n="11.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.2">v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="11.3">l</w>'<w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="11.5">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.6">l</w>'<w n="11.7"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="12" num="8.2"><w n="12.1">R<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>… <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>h</w> !</l>
		<l n="13" num="8.3"><w n="13.1">C</w>'<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="13.3">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="13.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w>.</l> 
	</lg>
</div></body></text></TEI>