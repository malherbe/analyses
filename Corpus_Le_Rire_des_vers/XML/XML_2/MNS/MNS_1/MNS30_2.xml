<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS30">
	<head type="tune">Air de Pique-Assiette.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <del reason="analysis" type="repetition" hand="KL">( bis.)</del></l>
		<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="2.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.3">m<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ; </l>
		<l n="3" num="1.3"><w n="3.1">Gr<seg phoneme="a" type="vs" value="1" rule="339">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="3.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>-<w n="3.3">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="3.4">gr<seg phoneme="a" type="vs" value="1" rule="339">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ! </l>
	</lg>
	<lg n="2">
	<head type="speaker">MADAME RAINVILLE.</head> 
		<l n="4" num="2.1"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="4.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="4.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.4">m</w>'<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <del reason="analysis" type="repetition" hand="KL">( bis.)</del></l>
		<l n="5" num="2.2"><w n="5.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="5.2">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.3">n</w>'<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="5.5">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="5.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="5.7">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.8">pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>. </l>
	</lg>
	<lg n="3">
	<head type="speaker">TOUTES.</head>
		<l n="6" num="3.1"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rgn<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="6.2">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="6.3">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.4">l<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>.</l>
	</lg>
	<lg n="4">
	<head type="speaker">MADAME RAINVILLE.</head>
		<l n="7" num="4.1"><w n="7.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="7.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="7.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="7.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="7.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="7.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t</w> <w n="7.7">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.8">gr<seg phoneme="a" type="vs" value="1" rule="339">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ; </l>
		<l part="I" n="8" num="4.2"><w n="8.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="8.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="8.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="8.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, </l>
		<l part="F" n="8" num="4.2"><w n="8.5">P<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t</w> <w n="8.6">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>. </l>
	</lg>
	<lg n="5">
	<head type="speaker">TOUS.</head>
		<l n="9" num="5.1"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="9.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> !</l>
	</lg>
</div></body></text></TEI>