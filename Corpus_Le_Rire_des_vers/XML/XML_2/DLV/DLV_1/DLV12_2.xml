<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
				<title type="sub">INVRAISEMBLANCE EN TROIS PARTIES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
				  <name>
					<forename>Félix-Auguste</forename>
					<surname>DUVERT</surname>
				  </name>
				  <date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="DLV" sort="2">
					<name>
						<forename>Augustin Théodore</forename>
						<nameLink>de</nameLink>
						<surname>LAUZANNE DE VAUROUSSEL</surname>
						<addname type="other">Lauzanne</addname>
					</name>
					<date from="1805" to="1877">1805-1877</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>135 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
                    <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
						<author>F.-A. DUVERT EN SOCIÉTÉ AVEC M. LAUZANNE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?vid=UOM:39015076863409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>University of Michigan Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/mdp.39015076863409</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">16 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DLV12">
	<head type="main">Reprise du chœur.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="1.2">J<seg phoneme="o" type="vs" value="1" rule="443">o</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>th<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="1.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="1.4">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="1.5">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> ! </l>
		<l n="2" num="1.2"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="2.2"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bt<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.4">p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>. </l>
		<l n="3" num="1.3"><w n="3.1">M<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.3">s<seg phoneme="ɛ" type="vs" value="1" rule="383">ei</seg>gn<seg phoneme="ø" type="vs" value="1" rule="404">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="4" num="1.4"><w n="4.1">J<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="4.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="4.3">L<seg phoneme="y" type="vs" value="1" rule="449">u</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rg</w>.</l>
	</lg>
</div></body></text></TEI>