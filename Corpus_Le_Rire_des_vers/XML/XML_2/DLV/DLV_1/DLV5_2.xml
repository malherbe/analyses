<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
				<title type="sub">INVRAISEMBLANCE EN TROIS PARTIES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
				  <name>
					<forename>Félix-Auguste</forename>
					<surname>DUVERT</surname>
				  </name>
				  <date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="DLV" sort="2">
					<name>
						<forename>Augustin Théodore</forename>
						<nameLink>de</nameLink>
						<surname>LAUZANNE DE VAUROUSSEL</surname>
						<addname type="other">Lauzanne</addname>
					</name>
					<date from="1805" to="1877">1805-1877</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>135 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
                    <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
						<author>F.-A. DUVERT EN SOCIÉTÉ AVEC M. LAUZANNE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?vid=UOM:39015076863409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>University of Michigan Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/mdp.39015076863409</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">16 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DLV5">
	<head type="tune">AIR : Gymnasiens ! remettons à quinzaine.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rs</w>, <w n="1.2">J<seg phoneme="o" type="vs" value="1" rule="443">o</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>th<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="1.4">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.5">l</w>'<w n="1.6">d<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.7">t</w>'<w n="1.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ! </l>
		<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="2.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.3">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>il</w>, <w n="2.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="2.5">v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="2.6">t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.7">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>. </l>
		<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="3.3">p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>x</w> <w n="3.4">l</w>'<w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="3.6">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="3.7">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, </l>
		<l n="4" num="1.4"><w n="4.1">J<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="4.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="4.3">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>, </l>
		<l n="5" num="1.5"><w n="5.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>. </l>
		<l n="6" num="1.6"><w n="6.1">R<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>fl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="6.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>' <w n="6.6">d</w>'<w n="6.7"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>llʼm<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, </l>
		<l n="7" num="1.7"><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="7.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.3">s<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>il</w> <w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="7.5">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.6">d<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="7.7">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.8">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>. </l>
		<l n="8" num="1.8"><w n="8.1"><seg phoneme="w" type="sc" value="0" rule="430">Ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="8.2">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="8.3">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.4">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="8.5">l</w>'<w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="8.9">c<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, </l>
		<l n="9" num="1.9"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="9.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="9.4">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm</w>' <w n="9.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.6">ch<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vʼs</w>-<w n="9.7">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>. </l>
	</lg>
	<lg n="2">
	<head type="speaker">ENSEMBLE.</head>
		<l ana="unanalyzable" n="10" num="2.1">Dors, Jonathas, etc.</l>
	</lg>
	<lg n="3">
	<head type="speaker">DOROTHÉE.</head>
		<l n="11" num="3.1"><w n="11.1">D<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rs</w>, <w n="11.2">J<seg phoneme="o" type="vs" value="1" rule="443">o</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>th<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="11.3">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="11.5">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="11.6">s</w>'<w n="11.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, </l>
		<l n="12" num="3.2"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="12.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="12.3">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>il</w> <w n="12.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="12.5">v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="12.6">t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.7">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>. </l>
		<l n="13" num="3.3"><w n="13.1">F<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="13.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="13.3">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="13.4">l</w>'<w n="13.5">h<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.6">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.7">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="13.8">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ; </l>
		<l n="14" num="3.4"><w n="14.1">J<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="14.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="14.3">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>, </l>
		<l n="15" num="3.5"><w n="15.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>. </l>
	</lg>
</div></body></text></TEI>