<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANGÉLIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIEME.</head><head type="main_subpart">SCÈNE V.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD32">
				<head type="tune">Même air.</head>
				<lg n="1">
					<head type="main">MÈRE GIBOU.</head>
						<l n="1" num="1.1"><space unit="char" quantity="10"></space><w n="1.1">Ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.2">t</w>'<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="1.4">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <del hand="LG" reason="analysis" type="repetition">(bis)</del></l>
						<l n="2" num="1.2"><space unit="char" quantity="10"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="2.1">Ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.2">t</w>'<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="2.4">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w></add></subst></l>
						<l n="3" num="1.3"><w n="3.1">T<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.2"><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="3.3">b<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> … <w n="3.4">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.5">pʼt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.6">pr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="4.2">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.3"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="4.6">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.7">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="5" num="1.5"><w n="5.1">Lʼ</w> <w n="5.2">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="5.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.4">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.5">n<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="5.7">Gr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>,</l>
						<l n="6" num="1.6"><space unit="char" quantity="10"></space><w n="6.1">Ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.2">t</w>'<w n="6.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="6.4">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <del hand="LG" reason="analysis" type="repetition">(bis)</del></l>
						<l n="7" num="1.7"><space unit="char" quantity="10"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="7.1">Ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.2">t</w>'<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="7.4">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w></add></subst></l>
						<l n="8" num="1.8"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="8.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>tʼn<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="8.3">qu</w>' <w n="8.4">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="8.5">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ssʼ</w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.7">s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="9" num="1.9"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="9.2"><seg phoneme="y" type="vs" value="1" rule="462">u</seg>nʼ</w> <w n="9.3">j<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="9.5">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="9.6">l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="10" num="1.10"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="10.2">n</w>'<w n="10.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="10.5">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>mʼ</w> <w n="10.6">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="10.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="10.8">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="10.9">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>…</l>
						<l n="11" num="1.11"><space unit="char" quantity="10"></space><w n="11.1">Ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.2">nʼ</w> <w n="11.3">t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.4">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>. <del hand="LG" reason="analysis" type="repetition">(bis)</del></l>
						<l n="12" num="1.12"><space unit="char" quantity="10"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="12.1">Ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.2">nʼt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.3">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>.</add></subst></l>
						<l n="13" num="1.13"><space unit="char" quantity="4"></space><w n="13.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="13.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="13.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> … <w n="13.4">ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.5">nʼ</w> <w n="13.6">t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.7">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.8">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ;</l>
						<l n="14" num="1.14"><w n="14.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="14.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="14.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.4">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>llʼ</w>, <w n="14.5">ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.6">nʼ</w> <w n="14.7">t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.8">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.9">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>,</l>
						<l n="15" num="1.15"><w n="15.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="15.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="15.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="15.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> … <w n="15.5">ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="15.6">nʼ</w> <w n="15.7">t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.8">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="15.9">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> !</l>
				</lg>
			</div></body></text></TEI>