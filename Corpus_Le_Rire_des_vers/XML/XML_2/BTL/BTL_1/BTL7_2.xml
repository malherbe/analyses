<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CONSEIL DE RÉVISION, OU LES MAUVAIS NUMÉROS</title>
				<title type="sub">TABLEAU-VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="BRU" sort="1">
					<name>
						<forename>Léon-Lévy</forename>
						<surname>BRUNSWICK</surname>
						<addname type="other">Léon LHÉRIE</addname>
					</name>
					<date from="1805" to="1859">1805-1859</date>
				</author>
				<author key="THO" sort="2">
				  <name>
					<forename>Mathieu Barthélemy</forename>
					<surname>THOUIN</surname>
					<addname type="other">Barthélemy</addname>
				  </name>
				  <date from="1804" to="18..">1804-18..</date>
				</author>
				<author key="LVY" sort="3">
				  <name>
					<forename>Léon-Victor</forename>
					<surname>LÉVY</surname>
					<addname type="pen_name">Victor LHÉRIE</addname>
					<addname type="other">Lhérie</addname>
				  </name>
				  <date from="1808" to="1845">1808-1845</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>212 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BTL_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CONSEIL DE RÉVISION, OU LES MAUVAIS NUMÉROS</title>
						<author>BRUNSWICK, BARTHÉLEMY ET LHÉRIE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=NYPL :33433075800833</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>New York Public Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/nyp.33433075800833</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">4 AOÛT 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BTL7">
	<head type="tune">Air : Entendez-vous ? c'est le tambour ( de la Fiancée )</head>
		<div type="section" n="1">
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">F<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="1.2">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.3">j</w>' <w n="1.4">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w>', <w n="1.5">j</w>'<w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ds</w> <w n="1.7">l</w>'<w n="1.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>,</l>
				<l n="2" num="1.2"><w n="2.1">V<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="2.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>il</w> <w n="2.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="2.5">s</w>'<w n="2.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
				<l n="3" num="1.3"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="3.2">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="3.3">r<seg phoneme="y" type="vs" value="1" rule="449">u</seg>sʼs</w>, <w n="3.4">j</w>' <w n="3.5">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>ds</w> <w n="3.6">d</w>'<w n="3.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
				<l n="4" num="1.4"><w n="4.1">Qu</w>' <w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.3">sʼr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="4.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.7">f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="4.8">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="4.9">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>.</l>
			</lg>
			<lg n="2">
			<head type="speaker">BLOQUET.</head>
				<l n="5" num="2.1"><w n="5.1">F<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>-<w n="5.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="5.3">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="5.5">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>gr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> !</l>
			</lg>
			<lg n="3">
			<head type="speaker">MOUFFLET.</head>
				<l n="6" num="3.1"><w n="6.1">J</w>'<w n="6.2">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="6.3">qu</w>' <w n="6.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="6.6">sʼr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="6.7">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.9">tr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="6.10">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="6.11">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="6.12">fr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>.</l>
			</lg>
			 <lg n="4">
			<head type="speaker">FRANCIS.</head>
				<l n="7" num="4.1"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="7.3">m<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="7.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>, <w n="7.5">p<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vrʼs</w> <w n="7.6">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
				<l n="8" num="4.2"><w n="8.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="8.3">d<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="8.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.5">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nnʼs</w> <w n="8.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>.</l> 
			</lg>
		</div>
		<div type="section" n="2">
			<head type="main">ENSEMBLE.</head>
			<lg n="1">
			<head type="speaker">FRANCIS.</head>
				<l n="9" num="1.1"><w n="9.1">F<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="9.2">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.3">j</w>' <w n="9.4">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w>', <w n="9.5">j</w>'<w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ds</w> <w n="9.7">l</w>' <w n="9.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>,</l>
				<l n="10" num="1.2"><w n="10.1">V<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="10.2">l</w>' <w n="10.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>il</w> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="10.5">s</w>'<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
				<l n="11" num="1.3"><w n="11.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="11.2">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="11.3">r<seg phoneme="y" type="vs" value="1" rule="449">u</seg>sʼs</w>, <w n="11.4">j</w>'<w n="11.5">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>ds</w> <w n="11.6">d</w>'<w n="11.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
				<l n="12" num="1.4"><w n="12.1">Qu</w>' <w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="12.3">sʼr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="12.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.7">f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="12.8">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="12.9">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>.</l> 
			</lg>
			<lg n="2">
			<head type="speaker">BLOQUET, CHOPIN et MOUFFLET.</head>
				<l n="13" num="2.1"><w n="13.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="13.2">s<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="13.3">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="13.4">j</w>'<w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ds</w> <w n="13.6">l</w>' <w n="13.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>,</l>
				<l n="14" num="2.2"><w n="14.1">V<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="14.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>il</w> <w n="14.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="14.5">s</w>'<w n="14.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
				<l n="15" num="2.3"><w n="15.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="15.2">p<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="15.3">r<seg phoneme="y" type="vs" value="1" rule="449">u</seg>sʼs</w>, <w n="15.4">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.5">l</w>'<w n="15.6">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="15.7">d</w>'<w n="15.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
				<l n="16" num="2.4"><w n="16.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="16.2">sʼr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="16.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="16.6">f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="16.7">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="16.8">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>.</l>
			</lg>
		</div>
</div></body></text></TEI>