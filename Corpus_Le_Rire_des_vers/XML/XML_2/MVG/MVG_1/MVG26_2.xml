<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TROISIÈME TABLEAU.</head><head type="main_subpart">SCÈNE XIX.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG26">
						<head type="tune">AIR : En avant bon courage (de Trois Jours en une heure.)</head>
						<lg n="1">
							<head type="main">CHŒUR GENERAL.</head>
							<l n="1" num="1.1"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="1.2">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="1.6">b<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
							<l n="2" num="1.2"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.3">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.4">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w> <w n="2.5">d</w>'<w n="2.6">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> :</l>
							<l n="3" num="1.3"><w n="3.1">Pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="3.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.3">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
							<l n="4" num="1.4"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="4.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="4.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>.</l>
						</lg>
						<lg n="2">
							<head type="main">BONAPARTE, prenant une couronne.</head>
							<l n="5" num="2.1"><w n="5.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="5.2">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="5.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.5">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
							<l n="6" num="2.2"><w n="6.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="6.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> !</l>
							<l n="7" num="2.3"><w n="7.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ls</w> <w n="7.2">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="7.3">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.4">v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="7.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
							<l n="8" num="2.4"><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="8.2">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.3">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="8.5">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> !</l>
						</lg>
						<lg n="3">
							<head type="main">CHŒUR.</head>
							<l n="9" num="3.1"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="9.2">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="9.6">b<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
							<l n="10" num="3.2"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.3">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.4">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w>, <subst reason="analysis" type="repetition" hand="LG"><del>etc.</del><add rend="hidden"><w n="10.5">d</w>'<w n="10.6">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> :</add></subst></l>
							<l n="11" num="3.3"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="11.1">Pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="11.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.3">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></add></subst></l>
							<l n="12" num="3.4"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="12.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="12.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="12.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="12.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>.</add></subst></l>
						</lg>
						<lg n="4">
							<head type="main">JOSÉPHINE.</head>
							<l n="13" num="4.1"><w n="13.1">L</w>'<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>, <w n="13.3">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.5">d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
							<l n="14" num="4.2"><w n="14.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="14.2">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.3">d</w>'<w n="14.4">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="14.5">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w>,</l>
							<l n="15" num="4.3"><w n="15.1">D<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="15.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199">om</seg></w> <w n="15.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.4">J<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
							<l n="16" num="4.4"><w n="16.1">S<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="16.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="16.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w>.</l>
						</lg>
						<lg n="5">
							<head type="main">CHŒUR.</head>
							<l n="17" num="5.1"><w n="17.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="17.2">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="17.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="17.6">b<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
							<l n="18" num="5.2"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.3">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.4">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w>, <subst reason="analysis" type="repetition" hand="LG"><del>etc.</del><add rend="hidden"><w n="18.5">d</w>'<w n="18.6">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> :</add></subst></l>
							<l n="19" num="5.3"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="19.1">Pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="19.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.3">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></add></subst></l>
							<l n="20" num="5.4"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="20.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="20.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="20.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="20.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>.</add></subst></l>
						</lg>
						<lg n="6">
							<head type="main">DARBEL.</head>
							<l n="21" num="6.1"><w n="21.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="21.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w>, <w n="21.4">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
							<l n="22" num="6.2"><w n="22.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="22.2">s<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="22.3">g<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w> ;</l>
							<l n="23" num="6.3"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="23.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="23.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="23.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
							<l n="24" num="6.4"><w n="24.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="24.2">p<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="24.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w> !</l>
						</lg>
						<lg n="7">
							<head type="main">CHŒUR</head>
							<l n="25" num="7.1"><w n="25.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="25.2">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="25.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="25.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="25.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="25.6">b<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
							<l n="26" num="7.2"><w n="26.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="26.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="26.3">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="26.4">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w>, <subst reason="analysis" type="repetition" hand="LG"><del>etc.</del><add rend="hidden"><w n="26.5">d</w>'<w n="26.6">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> :</add></subst></l>
							<l n="27" num="7.3"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="27.1">Pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="27.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="27.3">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></add></subst></l>
							<l n="28" num="7.4"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="28.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="28.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="28.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="28.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="28.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>.</add></subst></l>
						</lg>
						<lg n="8">
							<head type="main">LE CAPITAINE.</head>
							<l n="29" num="8.1"><w n="29.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="29.2">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="29.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="29.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="29.5">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="29.6">m<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
							<l n="30" num="8.2"><w n="30.1">L</w>'<w n="30.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>v<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="30.3">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w> …</l>
							<l n="31" num="8.3"><w n="31.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="31.2">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="31.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="31.4">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>-<w n="31.5"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
							<l n="32" num="8.4"><w n="32.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="32.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="32.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="32.4">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="32.5">h<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> !…</l>
						</lg>
						<lg n="9">
							<head type="main">CHŒUR.</head>
							<l n="33" num="9.1"><w n="33.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="33.2">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="33.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="33.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="33.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="33.6">b<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
							<l n="34" num="9.2"><w n="34.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="34.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="34.3">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="34.4">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w>,<subst reason="analysis" type="repetition" hand="LG"><del>etc.</del><add rend="hidden"><w n="34.5">d</w>'<w n="34.6">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> :</add></subst></l>
							<l n="35" num="9.3"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="35.1">Pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="35.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="35.3">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></add></subst></l>
							<l n="36" num="9.4"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="36.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="36.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="36.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="36.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="36.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>.</add></subst></l>
						</lg>
						<lg n="10">
							<head type="main">ÆGIDIUS.</head>
							<l n="37" num="10.1"><w n="37.1">D<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="37.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="37.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="37.4">S<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
							<l n="38" num="10.2"><w n="38.1">S</w>'<w n="38.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="38.3">s</w>'<w n="38.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="38.5">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="38.6">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>,</l>
							<l n="39" num="10.3"><w n="39.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="39.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="39.3">p<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="39.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
							<l n="40" num="10.4"><w n="40.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="40.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="40.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="40.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="40.5">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> !</l>
						</lg>
						<lg n="11">
							<head type="main">CHŒUR</head>
							<l n="41" num="11.1"><w n="41.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="41.2">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="41.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="41.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="41.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="41.6">b<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
							<l n="42" num="11.2"><w n="42.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="42.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="42.3">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="42.4">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w>, <subst reason="analysis" type="repetition" hand="LG"><del>etc.</del><add rend="hidden"><w n="42.5">d</w>'<w n="42.6">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> :</add></subst></l>
							<l n="43" num="11.3"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="43.1">Pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="43.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="43.3">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></add></subst></l>
							<l n="44" num="11.4"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="44.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="44.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="44.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="44.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="44.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>.</add></subst></l>
						</lg>
						<lg n="12">
							<head type="main">BONAPARTE, au public.</head>
							<l n="45" num="12.1"><w n="45.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="45.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="45.3">l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>gu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="45.4"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="45.5">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
							<l n="46" num="12.2"><w n="46.1">Qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="46.2">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="46.3">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="46.4">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="46.5">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> ;</l>
							<l n="47" num="12.3"><w n="47.1">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="47.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="47.3">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="47.4">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
							<l n="48" num="12.4"><w n="48.1">D<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="48.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="48.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>.</l>
						</lg>
						<lg n="13">
							<head type="main">CHŒUR.</head>
							<l n="49" num="13.1"><w n="49.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="49.2">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="49.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="49.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="49.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="49.6">b<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
							<l n="50" num="13.2"><w n="50.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="50.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="50.3">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="50.4">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w> <w n="50.5">d</w>'<w n="50.6">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> ;</l>
							<l n="51" num="13.3"><w n="51.1">Pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="51.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="51.3">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
							<l n="52" num="13.4"><w n="52.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="52.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="52.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="52.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="52.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>.</l>
						</lg>
					</div></body></text></TEI>