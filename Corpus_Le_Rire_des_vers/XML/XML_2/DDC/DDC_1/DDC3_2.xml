<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE GENTILHOMME</title>
				<title type="sub">VAUDEVILLE ANECDOTIQUE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DPY" sort="1">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="COU" sort="2">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>135 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DDC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE GENTILHOMME</title>
						<author>DUPEUTY ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=blZoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032862369.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">21 MARS 1833</date>
				<placeName>
					<settlement>THÉÂTRE NATIONAL DU VAUDEVILLE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DDC3">
	<head type="tune">AIR : Ah ! vous avez des droits superbes !</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">D<seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd</w> <w n="1.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w>, <w n="1.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.5">l</w>'<w n="1.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> :</l>
		<l n="2" num="1.2"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.2">b<seg phoneme="y" type="vs" value="1" rule="444">û</seg>ch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w>, <w n="2.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="2.9">s<seg phoneme="ɛ" type="vs" value="1" rule="383">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> ;</l>
		<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.3">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="3.4">d<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.6">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>…</l>
		<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="4.2">p<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="4.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.4">p<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="4.5">d</w>'<w n="4.6">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>.</l>
		<l n="5" num="1.5"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>, <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.3">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.4">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="6" num="1.6"><w n="6.1">L</w>'<w n="6.2">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="6.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>tr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>, <w n="6.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="6.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rqu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.6">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.7">l</w>'<w n="6.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pl<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>,</l>
		<l n="7" num="1.7"><w n="7.1">R<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w>, <w n="7.2">d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.4">ch<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="8" num="1.8"><w n="8.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="8.2">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffl<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="8.7">r<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>,</l>
		<l n="9" num="1.9"><w n="9.1">R<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w>, <w n="9.2">d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.4">ch<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="10" num="1.10"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="10.2">j<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="10.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffl<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="10.6">r<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> ! …</l>
	</lg>
</div></body></text></TEI>