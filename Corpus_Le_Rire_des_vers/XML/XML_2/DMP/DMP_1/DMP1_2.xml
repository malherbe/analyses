<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L'HOMME QUI BAT SA FEMME</title>
				<title type="sub">TABLEAU POPULAIRE EN UN ACTE, MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DML" sort="1">
				  <name>
					<forename>Julien</forename>
					<nameLink>de</nameLink>
					<surname>MAILLAN</surname>
					<addname type="other">Julien</addname>
					<addname type="other">Julien de M.</addname>
				  </name>
				  <date from="1805" to="1851">1805-1851</date>
				</author>
				<author key="DNR" sort="2">
				  <name>
					<forename>Philippe-François</forename>
					<surname>PINEL</surname>
					<addname type="pen_name">DUMANOIR</addname>
					<addname type="other">Philippe Dumanoir</addname>
					<addname type="other">Philippe D.</addname>
					<addname type="other">Philippe D***</addname>
				  </name>
				  <date from="1806" to="1865">1806-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DMP_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
          <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L'HOMME QUI BAT SA FEMME</title>
						<author>JULIEN DE MALLIAN ET PHILIPPE DUMANOIR</author>
					</titleStmt>
					<publicationStmt>
						<publisher>INTERNET ARCHIVE</publisher>
						<idno type="URL">https ://archive.org/details/lhommequibatsafe00malluoft</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>University of Toronto Libraries</repository>
								<idno type="URL">https ://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991105986034006196</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">8 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DMP1">
	<head type="tune">AIR : Ah ! si madame me voyait !</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.2">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>ç<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="1.4">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="1.5">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.6">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="1.7">s<seg phoneme="ɛ" type="vs" value="1" rule="354">e</seg>x<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> :</l>
		<l n="2" num="1.2"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.3">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="2.4">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.5">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> ;</l>
		<l n="3" num="1.3"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="3.2">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="3.4">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="3.5">qu</w>'<w n="3.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.7">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.8">v<seg phoneme="ɛ" type="vs" value="1" rule="354">e</seg>x<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> :</l>
		<l n="4" num="1.4"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="4.2">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.3">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr</w>' <w n="4.4">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w></l>
		<l n="5" num="1.5"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="5.2">n</w>'<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.4">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="5.5">d</w>'<w n="5.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>glʼs</w> <w n="5.7"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="5.8">b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="5.9">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.10">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>gts</w>.</l>
		<l n="6" num="1.6"><w n="6.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr</w>' <w n="6.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="6.4">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="6.5">cr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="6.6">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.8">d<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="7" num="1.7"><w n="7.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.2">l</w>'<w n="7.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="7.5"><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>, <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="7.7">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w>…</l>
		<l n="8" num="1.8"><w n="8.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.2">n</w>'<w n="8.3">s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="8.5">d</w>'<w n="8.6">qu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> <w n="8.7">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.8">sʼr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="8.9">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="9" num="1.9"><w n="9.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="9.2">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="9.4">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm</w>' <w n="9.5">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.6">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> !</l> 
	</lg>
</div></body></text></TEI>