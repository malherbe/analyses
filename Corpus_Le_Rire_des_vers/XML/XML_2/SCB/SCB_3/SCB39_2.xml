<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB39">
	<head type="tune">AIR : J'en guette un petit de mon âge.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="1.3">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>, <w n="1.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
		<l part="I" n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="2.3">l</w>'<w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> ? </l>
	</lg>
	<lg n="2">
	<head type="speaker">AUGUSTA</head>
		<l part="F" n="2"><w n="2.5">Pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>.</l>
		<l n="3" num="2.1"><w n="3.1">C</w>'<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rc<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.4">qu</w>'<w n="3.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="3.6">m</w>'<w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="4" num="2.2"><w n="4.1">Qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.2">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>-<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.4">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.5">l</w>'<w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.7"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>.</l>
		<l n="5" num="2.3"><w n="5.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="5.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="5.5">m<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w>,</l>
		<l n="6" num="2.4"><w n="6.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="6.2">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="6.3">n</w>'<w n="6.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="6.6">v<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
		<l n="7" num="2.5"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="7.2">qu</w>'<w n="7.3"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>…</l>
		<l n="8" num="2.6"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="8.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="8.4">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.5">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.6">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w>.</l>
	</lg> 
</div></body></text></TEI>