<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB43">
	<head type="tune">AIR du Partage de la richesse.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="1.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w>, <w n="1.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.6">H<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>-<w n="2.2">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="2.3">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w>… <w n="2.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="2.5">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.7">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>scr<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w>.</l>
	</lg>
	<lg n="2">
	<head type="speaker">HENRIETTE.</head>
		<l n="3" num="2.1"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg>h</w> ! <w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="3.3">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="3.5">c<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.6">m</w>'<w n="3.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>qu<seg phoneme="j" type="sc" value="0" rule="488">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
		<l n="4" num="2.2"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>d<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>, <w n="4.2">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rs</w>, <w n="4.4">p<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>squ<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.5">c</w>'<w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="4.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="4.8">s<seg phoneme="ə" type="vi" value="1" rule="356">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w>.</l>
		<l n="5" num="2.3"><w n="5.1">J</w>'<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="5.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="5.4">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ct<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.6">v<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> ;</l>
		<l n="6" num="2.4"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="6.2">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>ch<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="6.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="6.4">s</w>'<w n="6.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="6.6">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>t</w>.</l>
		<l n="7" num="2.5"><w n="7.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.3">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ts</w> <w n="7.4"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="7.5">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.6">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.7">l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.8"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="7.9">d</w>'<w n="7.10"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w></l>
		<l n="8" num="2.6"><w n="8.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="8.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.4">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ls</w> <w n="8.5">qu</w>'<w n="8.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="8.7">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.8">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>.</l>
	</lg> 
</div></body></text></TEI>