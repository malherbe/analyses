<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB33">
	<head type="tune">AIR nouveau de Mme DUCHAMBRE.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">J<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="1.3">m<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.3">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
		<l n="3" num="1.3"><w n="3.1">J</w>'<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="3.4">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="4" num="1.4"><w n="4.1">Tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306">a</seg>il</w> <w n="4.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="4.3">g<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l> 
	</lg>
	<lg n="2">
		<l n="5" num="2.1"><w n="5.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.3">s<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="6" num="2.2"><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="6.2">j</w>'<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="6.4">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
		<l n="7" num="2.3"><w n="7.1">Qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.2">d<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.3">v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="365">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="8" num="2.4"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="8.3"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l> 
	</lg>
	<lg n="3">
		<l n="9" num="3.1"><w n="9.1">J<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="9.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="9.3">m<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="10" num="3.2"><w n="10.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.3">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
		<l n="11" num="3.3"><w n="11.1">J</w>'<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="11.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="11.4">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="12" num="3.4"><w n="12.1">Tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306">a</seg>il</w> <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="12.3">g<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
	</lg>
	<lg n="4">
		<l n="13" num="4.1"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w>'<w n="13.2">h<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="13.3">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="14" num="4.2"><w n="14.1">H<seg phoneme="œ̃" type="vs" value="1" rule="260">um</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="14.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.4">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> ;</l>
		<l n="15" num="4.3"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="15.2">j</w>'<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="15.4">l</w>'<w n="15.5"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="16" num="4.4"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="16.2">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.3">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> : <w n="16.4">D<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w>.</l> 
	</lg>
	<lg n="5">
		<l n="17" num="5.1"><w n="17.1">J<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="17.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="17.3">m<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="18" num="5.2"><w n="18.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.3">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
		<l n="19" num="5.3"><w n="19.1">J</w>'<w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="19.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="19.4">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="20" num="5.4"><w n="20.1">Tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306">a</seg>il</w> <w n="20.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="20.3">g<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
	</lg>
</div></body></text></TEI>