<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <fileDesc>
   <titleStmt>
	<title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
	<title type="corpus">Le Rire des vers</title>
	<author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
	</author>
	<author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
	</author>
	<editor>Le Rire des vers, Université de Bâle</editor>
	<editor>
	 Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
	 <choice>
	  <abbr>CRISCO, Université de Caen Normandie</abbr>
	  <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	 </choice>
	 (EA 4255)
	</editor>
	<respStmt>
	 <resp>Encodage en XML (CRISCO, université de Caen)</resp>
	 <name id="KL">
	  <forename>Kedi</forename>
	  <surname>LI</surname>
	 </name>
	</respStmt>
	<respStmt>
	 <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
	 <name id="RR">
	  <forename>Richard</forename>
	  <surname>RENAULT</surname>
	 </name>
	</respStmt>
   </titleStmt>
   <extent>140 vers</extent>
   <publicationStmt>
	<publisher>
	 <orgname>
	  <choice>
	   <abbr>CRISCO, Université de Caen Normandie</abbr>
	   <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	  </choice>
	 </orgname>
	 <address>
	  <addrLine>Université de Caen</addrLine>
	  <addrLine>14032 CAEN CEDEX</addrLine>
	  <addrLine>FRANCE</addrLine>
	 </address>
	 <email>crisco.incipit@unicaen.fr</email>
	 <ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
	</publisher>
	<pubPlace>Caen</pubPlace>
	<date when="2022">2022</date>
	<idno type="local">SCB_2</idno>	
	<availability status="free">
		<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
		<p>
			Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
			CC = Licence Creative Commons
			BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
			sur les sources et sur les modifications introduites.
			NC = Pas d’utilisation commerciale.
			SA = Partage dans les mêmes conditions.
		</p>
	</availability>
   </publicationStmt>
   <sourceDesc>
	<biblFull>
	 <titleStmt>
	  <title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	  <author>SCRIBE ET BAYARD</author>
	 </titleStmt>
	 <publicationStmt>
	  <publisher>INTERNET ARCHIVE</publisher>
	  <idno type="URL">https://archive.org/details/camillaoulasoeur00scriuoft</idno>
	 </publicationStmt>
	 <sourceDesc>
	  <biblStruct>
	   <monogr>
		<repository>University of Toronto Libraries</repository>
		<idno type="URL">https://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991106543695706196</idno>
	   </monogr>
	  </biblStruct>                 
	 </sourceDesc>
	</biblFull> 
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <creation>
	<date when="1832">12 DÉCEMBRE 1832</date>
	<placeName>
	 <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
	</placeName>
   </creation>
  </profileDesc>
  <encodingDesc>
   <projectDesc>
	<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
   </projectDesc>
   <samplingDecl>
	<p>Les parties versifiées ont été prioritairement encodées.</p>
   </samplingDecl>
   <editorialDecl>
	<normalization>
	 <p>Les majuscules accentuées ont été restituées.</p>
	 <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
	 <p>La ponctuation a été normalisée.</p> 
	 <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
	 <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
	 <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
	</normalization>
   </editorialDecl>
  </encodingDesc>
 </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB19">
	<head type="tune">AIR du Piége.</head>
	<lg n="1">
	   <l n="1" num="1.1"><w n="1.1">C</w>'<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="1.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>squ<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="1.5">t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="1.7">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="1.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.9">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>,</l>
	   <l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="2.2">l</w>'<w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.7">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rsp<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ct<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
	   <l n="3" num="1.3"><w n="3.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="3.3">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w>, <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="3.5">pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>, <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="3.7">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>,</l>
	   <l n="4" num="1.4"><w n="4.1">C</w>'<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="4.4">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>  <w n="4.5">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.6">l</w>'<w n="4.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="4.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
	   <l n="5" num="1.5"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="5.2">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.3">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="5.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="5.6">ch<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="5.7">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w></l>
	   <l n="6" num="1.6"><w n="6.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w>, <w n="6.2">gr<seg phoneme="a" type="vs" value="1" rule="339">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.4">l</w>'<w n="6.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="6.6">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.7">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.8">b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
	   <l n="7" num="1.7"><w n="7.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.2">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="7.3">l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="7.5">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.6">n</w>'<w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="7.8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w></l>
	   <l n="8" num="1.8"><w n="8.1">R<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="8.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="8.3">m</w>'<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="8.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.7">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
	</lg>   
</div></body></text></TEI>