<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB14">
	<head type="tune">AIR de M. Alpb. Gilbert.</head>
		<div type="section" n="1">
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">C<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
				<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="2.2">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> :</l>
				<l n="3" num="1.3"><w n="3.1">J<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="3.3">f<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="3.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.6">c<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
				<l n="4" num="1.4"><w n="4.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.3">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.4">s</w>'<w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>mm<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>…</l>
				<l n="5" num="1.5"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="5.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="5.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w>, <w n="5.5">d</w>'<w n="5.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="5.7">ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>,</l>
				<l n="6" num="1.6"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="6.2">r<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="6.3">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.4">br<seg phoneme="y" type="vs" value="1" rule="444">û</seg>l<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w></l>
				<l n="7" num="1.7"><w n="7.1">C<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
			</lg>
		</div>
		<div type="section" n="2">
			<lg n="1">
			<head type="speaker">TÉLÉMAQUE.</head>
				<p>
					Ça demande une réponse. Permettez.<lb></lb>
				</p>
			<head type="tune">Même air.</head>
				<l n="8" num="1.1"><w n="8.1">C<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
				<l n="9" num="1.2"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="198">E</seg>st</w> <w n="9.2">j<seg phoneme="œ̃" type="vs" value="1" rule="394">eun</seg></w>', <w n="9.3">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="9.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="9.6">c<seg phoneme="e" type="vs" value="1" rule="271">æ</seg>t<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>…</l>
				<l n="10" num="1.3"><w n="10.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.2">dʼs<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="10.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.4">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.6">d<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
				<l n="11" num="1.4"><w n="11.1">L</w>'<w n="11.2"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll</w>'<w n="11.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>gn<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> ;</l>
				<l n="12" num="1.5"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="12.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.3">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>, <w n="12.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="12.5">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="12.6">ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>,</l>
				<l n="13" num="1.6"><w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="13.2">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="13.3">m<seg phoneme="œ" type="vs" value="1" rule="150">on</seg>s<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="396">eu</seg>r</w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ttr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w></l>
				<l n="14" num="1.7"><w n="14.1">C<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
			</lg>
		</div>
</div></body></text></TEI>