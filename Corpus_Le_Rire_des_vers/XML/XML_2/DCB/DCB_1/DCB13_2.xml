<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB13">
	<head type="tune">AIR Galopade du gentilhomme de la chambre.</head>
	<lg n="1">
		<stage>(à Télémaque.)</stage>
		<l n="1" num="1.1"><w n="1.1">C</w>'<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="1.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="1.5">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="1.6">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> !</l>
		<l n="2" num="1.2"><w n="2.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w> <w n="2.2">qu</w>'<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll</w>'<w n="2.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.5">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>, <w n="2.6">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rgu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="3" num="1.3"><w n="3.1">D</w>'<w n="3.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.3">p<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r</w>'<w n="3.4">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="3.5">l</w>'<w n="3.6">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w>,</l>
		<l n="4" num="1.4"><w n="4.1">V<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="4.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="4.3">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="4.4">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="5" num="1.5"><w n="5.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="5.2">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>.</l>
	</lg> 
	<lg n="2">
	<head type="speaker">MARGUERITE, à Télémaque.</head>
		<l n="6" num="2.1"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="6.3">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.4">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.5">s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="6.6"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w>…</l>
		<l n="7" num="2.2"><w n="7.1">D</w>'<w n="7.2">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="7.3">j</w>'<w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss</w>'<w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="7.6">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.7">b<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
		<l n="8" num="2.3"><w n="8.1">J</w>'<w n="8.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>tt</w>'<w n="8.3">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="8.5">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>…</l>
	</lg>
	<lg n="3">
	<head type="speaker">TÉLÉMAQUE.</head>
		<l n="9" num="3.1"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="9.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="9.3">g<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="9.4">pʼt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="9.5">ch<seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> !</l>
	</lg>
	<lg n="4">
	<head type="speaker">CHARLES, à Célestine qui lui tourne le dos.</head>
		<l n="10" num="4.1"><w n="10.1">V<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="10.2"><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="10.3">s</w>'<w n="10.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rnʼnt</w>… <w n="10.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> ?</l>
		<l n="11" num="4.2"><w n="11.1">N<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="11.3">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="11.5">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="12" num="4.3"><w n="12.1">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="12.2">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.3">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.4">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
	</lg>
	<lg n="5">
	<head type="speaker">CÉLESTINE, avec fierté.</head>
		<l n="13" num="5.1"><w n="13.1">Ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.2">n</w>'<w n="13.3">v<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="13.5">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="13.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="13.7">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>.</l>
	</lg> 
	<lg n="6">
	<head type="speaker">ENSEMBLE.</head>
		<l n="14" num="6.1"><w n="14.1">C</w>'<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="14.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="14.5">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="14.6">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> !</l>
		<l n="15" num="6.2"><w n="15.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w> <w n="15.2">qu</w>'<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll</w>'<w n="15.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.5">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>, <w n="15.6">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rgu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="16" num="6.3"><w n="16.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="16.2">d</w>'<w n="16.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w></l>
		<l n="17" num="6.4"><w n="17.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="17.2">d</w>'<w n="17.3">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="17.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="17.5">v<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="17.6">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l rhyme="none" n="18" num="6.5"><w n="18.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="18.2">d</w>'<w n="18.3">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w></l>
		<l n="19" num="6.6"><w n="19.1">D</w>'<w n="19.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="19.3">p<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r</w>'<w n="19.4">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.5">l</w>'<w n="19.6">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w>.</l>
	</lg>
</div></body></text></TEI>