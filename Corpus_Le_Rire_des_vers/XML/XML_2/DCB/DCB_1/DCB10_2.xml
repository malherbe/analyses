<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB10">
	<head type="tune">AIR de Julie.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.2">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>x</w>, <w n="1.3">l</w>'<w n="1.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>cr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tt<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>, <w n="1.5">l</w>'<w n="1.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="2" num="1.2"><w n="2.1">F<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="2.2">qu</w>'<w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="2.4">ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.5">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="2.7">l</w>'<w n="2.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> ;</l>
		<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="3.2">n</w>'<w n="3.3">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="3.5">t</w>'<w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>tr</w>' <w n="3.7">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.9">cl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.10"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="4" num="1.4"><w n="4.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="4.2">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="4.3">l</w>'<w n="4.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w> <w n="4.5">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rs<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w>.</l>
		<l n="5" num="1.5"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="5.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="5.3">l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="5.4">qu</w>'<w n="5.5">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.6">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="5.7">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="5.8"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w></l>
		<l n="6" num="1.6"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="6.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.3">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="6.5">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.6">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w></l>
		<l n="7" num="1.7"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="7.2">cr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w> <w n="7.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="7.4">g<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w>,</l>
		<l n="8" num="1.8"><w n="8.1">C</w>'<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="8.3">l</w>'<w n="8.4">g<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="8.5">qu</w>'<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="8.7"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="8.8">n<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>.</l>
	</lg>
</div></body></text></TEI>