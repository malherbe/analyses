<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC10">
	<head type="tune">AIR : Vaudeville des Scythes.</head>  
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.2">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.3">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="1.4">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>, <w n="1.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.6">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>, <w n="1.7">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.2">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.3">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="2.5">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm</w>'<w n="2.6">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ds</w> <w n="2.8"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>,</l>
		<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="3.2">s</w>'<w n="3.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="3.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr</w>'<w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="3.7">p<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="3.8">br<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="3.9">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.10">z<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="4" num="1.4"><w n="4.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.2">dʼm<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w>'<w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.4">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x</w>, <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>, <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.8">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>.</l>
		<l n="5" num="1.5"><w n="5.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.2">n</w>'<w n="5.3">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="5.5">d</w>' <w n="5.6">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x</w>, <w n="5.7">n<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="5.8">d</w>' <w n="5.9">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>, <w n="5.10">n<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="5.11">d</w>'<w n="5.12">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>.</l>
		<l n="6" num="1.6"><w n="6.1">B<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="6.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r</w>', <w n="6.4">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="6.5">j</w>'<w n="6.6"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ffr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.8">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.9"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>lt<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="7" num="1.7"><w n="7.1">Qu<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>'-<w n="7.2">c<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>q</w> <w n="7.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ls</w> <w n="7.4">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.5">j</w>'<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="7.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="7.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="7.9">g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> ;</l>
		<l n="8" num="1.8"><w n="8.1">J</w>'<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.3">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w>, <w n="8.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.5">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn</w>'<w n="8.6">p<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>'<w n="8.7">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="9" num="1.9"><w n="9.1">Qu</w>'<w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.3">n</w>'<w n="9.4">cr<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="9.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="9.8">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>il</w> <w n="9.9">b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>dg<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w>.</l>
	</lg>
</div></body></text></TEI>