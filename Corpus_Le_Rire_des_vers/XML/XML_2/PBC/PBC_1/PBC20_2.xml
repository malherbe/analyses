<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC20">
	<head type="tune">AIR d'Amour et Mystère.</head>
	<lg n="1">
	<head type="speaker">ENSEMBLE.</head>
		<l n="1" num="1.1"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> <w n="1.2">n</w>'<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w>-<w n="1.4">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.5">qu</w>'<w n="1.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="1.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ?</l>
		<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɔ" type="vs" value="1" rule="438">O</seg>bj<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.2">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.3">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> !</l>
		<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="3.2">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.3">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.4">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="3.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="4" num="1.4"><w n="4.1">P<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.2">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="4.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> !</l>
	</lg>
	<lg n="2">
	<head type="speaker">BRIND'AMOUR.</head>
		<l n="5" num="2.1"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="5.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.4">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.5">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="6" num="2.2"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="6.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="6.3">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ! <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.5">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>nn<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> ;</l>
		<l n="7" num="2.3"><w n="7.1">G<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="7.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg></w> <w n="7.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="8" num="2.4"><w n="8.1">L</w>'<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="8.3">d</w>'<w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="8.5">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="8.7">v<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>…</l>
		<l n="9" num="2.5"><w n="9.1">L</w>'<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="9.3">d</w>'<w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="9.5">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="9.6">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>.</l>
	</lg>
	<lg n="3">
	<head type="speaker">ENSEMBLE.</head>
		<l n="10" num="3.1"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>d<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> ! <w n="10.2">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.3">n</w>'<w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="10.5">qu</w>'<w n="10.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="10.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="11" num="3.2"><w n="11.1"><seg phoneme="ɔ" type="vs" value="1" rule="438">O</seg>bj<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.2">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.3">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> ! …</l>
		<l n="12" num="3.3"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="12.2">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.3">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.4">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="12.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="13" num="3.4"><w n="13.1">N<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.2">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>-<w n="13.3">t</w>-<w n="13.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="13.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> ?</l>
	</lg>
</div></body></text></TEI>