<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE IX.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC22">
					<head type="tune">AIR : J'arrivons de notʼ village.</head>
						<lg n="1">
							<head type="main">LE DEY ET ZULÉMA.</head>
							<l n="1" num="1.1"><w n="1.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="1.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.4">l</w>'<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>fr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>…</l>
							<l n="2" num="1.2"><w n="2.1">Ç<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.2">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="2.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>trʼ</w> <w n="2.5">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
							<l n="3" num="1.3"><space unit="char" quantity="2"></space><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="3.2">n</w>'<w n="3.3"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.5">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="3.6">dʼ</w> <w n="3.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w></l>
							<l n="4" num="1.4"><space unit="char" quantity="10"></space><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>.</l>
							<l n="5" num="1.5"><space unit="char" quantity="6"></space><w n="5.1">D<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="5.2">qu</w>'<w n="5.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w></l>
							<l n="6" num="1.6"><space unit="char" quantity="4"></space><w n="6.1">F<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="6.2">n<seg phoneme="o" type="vs" value="1" rule="443">o</seg>tʼ</w> <w n="6.3">b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
							<l n="7" num="1.7"><space unit="char" quantity="4"></space><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>,</l>
							<l n="8" num="1.8"><space unit="char" quantity="6"></space><w n="8.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="8.2">vʼl<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>, <w n="8.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="8.4">vʼl<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>,</l>
						</lg>
					</div></body></text></TEI>