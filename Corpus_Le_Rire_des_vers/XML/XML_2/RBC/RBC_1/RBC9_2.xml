<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE III.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC9">
				<head type="tune">Même air</head>
				<lg n="1">
					<head type="main">LA LIBERTÉ.</head>
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.3">C<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="1.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="1.6">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="1.7">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
					<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.2">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w> <w n="2.3">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.4">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<head type="main">CHARLES.</head>
					<l n="3" num="2.1"><w n="3.1">R<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.3">gr<seg phoneme="a" type="vs" value="1" rule="339">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="3.5">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="3.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s<seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<head type="main">LE RÉMOULEUR.</head>
					<l n="4" num="3.1"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.2">p<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="4.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="4.5">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="4.6"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>…</l>
					<l n="5" num="3.2"><space unit="char" quantity="12"></space><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="5.3"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>.</l>
				</lg>
				<p>
					LA LIBERTÉ. <lb></lb>
					Théâtre Montansier, Palais-Royal, à Paris, n. 3.<lb></lb>
				</p>
				<lg n="4">
					<head type="sub">LE RÉMOULEUR.</head>
					<l n="6" num="4.1"><w n="6.1">Gr<seg phoneme="a" type="vs" value="1" rule="339">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.3">Br<seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w>, <w n="6.4">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.5">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.6">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="6.7">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> !</l>
					<l n="7" num="4.2"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="7.2">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="7.3">l<seg phoneme="o" type="vs" value="1" rule="443">o</seg>c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w>… <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>h</w> ! <w n="7.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.6">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.7">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>f<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>r<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>…</l>
					<l n="8" num="4.3"><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="8.2"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="8.3">v<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.5">gr<seg phoneme="a" type="vs" value="1" rule="339">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="8.7">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>h<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> ;</l>
					<l n="9" num="4.4"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="9.2">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.3">gr<seg phoneme="a" type="vs" value="1" rule="339">â</seg>cʼs</w>-<w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>, <w n="9.5">f<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="9.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="9.7">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="9.8">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>…</l>
					<l n="10" num="4.5"><space unit="char" quantity="12"></space><w n="10.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="10.3"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>.</l>
				</lg>
			</div></body></text></TEI>