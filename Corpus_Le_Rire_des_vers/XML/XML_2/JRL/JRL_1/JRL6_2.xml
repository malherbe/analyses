<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHABERT</title>
				<title type="sub">HISTOIRE CONTEMPORAINE EN DEUX ACTES, MÊLÉE DE CHANT</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="ARJ" sort="1">
					<name>
						<forename>Jacques</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1790" to="1855">1790-1855</date>
				</author>
				<author key="LUR" sort="2">
					<name>
						<forename>Louis</forename>
						<surname>LURINE</surname>
					</name>
					<date from="1810" to="1860">1810-1860</date>
				</author>
				<editor>Le rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>110 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">JRL_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
				   </p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CHABERT</title>
						<author>JACQUES ARAGO ET LOUIS LURINE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=vldoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849877.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">2 JUILLET 1832</date>
				<placeName>
					<settlement>THÉÂTRE NATIONAL DU VAUDEVILLE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="JRL6">
	<head type="tune">AIR : Vaudeville des Frères de lait.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="1.3">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>, <w n="1.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="1.6">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.7">m</w>'<w n="1.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="2" num="1.2"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg></w> <w n="2.2">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.3">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="2.4">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.5">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ; </l>
		<l n="3" num="1.3"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="3.2">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="3.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="3.6"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xc<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="4" num="1.4"><w n="4.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="4.2">n<seg phoneme="y" type="vs" value="1" rule="449">u</seg>l</w> <w n="4.3">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="4.4">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.5">p<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="4.6">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.8">d<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>.</l>
		<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="5.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.3">tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t</w> <w n="5.4">qu</w>'<w n="5.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="5.6">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="5.7">m</w>'<w n="5.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="6" num="1.6"><w n="6.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.2">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="6.3">ch<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>, <w n="6.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="6.6">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="6.7">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w></l>
		<l n="7" num="1.7"><w n="7.1">J</w>'<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="7.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="7.4">l</w>'<w n="7.5"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>p<seg phoneme="y" type="vs" value="1" rule="449">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="7.7">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.8">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="8" num="1.8"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="8.3">l</w>'<w n="8.4">h<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.5">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w>.</l>
	</lg> 
</div></body></text></TEI>