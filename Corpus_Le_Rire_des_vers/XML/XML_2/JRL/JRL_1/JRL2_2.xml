<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHABERT</title>
				<title type="sub">HISTOIRE CONTEMPORAINE EN DEUX ACTES, MÊLÉE DE CHANT</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="ARJ" sort="1">
					<name>
						<forename>Jacques</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1790" to="1855">1790-1855</date>
				</author>
				<author key="LUR" sort="2">
					<name>
						<forename>Louis</forename>
						<surname>LURINE</surname>
					</name>
					<date from="1810" to="1860">1810-1860</date>
				</author>
				<editor>Le rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>110 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">JRL_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
				   </p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CHABERT</title>
						<author>JACQUES ARAGO ET LOUIS LURINE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=vldoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849877.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">2 JUILLET 1832</date>
				<placeName>
					<settlement>THÉÂTRE NATIONAL DU VAUDEVILLE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="JRL2">
	<head type="tune">AIR : Un page aimait la jeune Adèle.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.2">v<seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg></w> <w n="1.3">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.6">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>tr<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="2" num="1.2"><w n="2.1">C</w>'<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="2.3">R<seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt</w>, <w n="2.4">R<seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt</w> <w n="2.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.6">p<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> !</l>
		<l n="3" num="1.3"><w n="3.1">F<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="3.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>, <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="3.5">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="3.6">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.8">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="4.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.4">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="4.5">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rc<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>l<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="4.6">l</w>'<w n="4.7"><seg phoneme="e" type="vs" value="1" rule="169">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>.</l>
		<l n="5" num="1.5"><w n="5.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="5.2">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.3">f<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>, <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="5.5">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="5.6">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="6" num="1.6"><w n="6.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.2">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="300">ai</seg></w> <w n="6.3">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.4">n<seg phoneme="a" type="vs" value="1" rule="342">a</seg><seg phoneme="i" type="vs" value="1" rule="476">ï</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
		<l n="7" num="1.7"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="7.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="7.4">f<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>r</w> <w n="7.5">v<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="7.6">fr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="7.7">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.8">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="8" num="1.8"><w n="8.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.2">v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="8.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="8.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.7">m<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
		<l n="9" num="1.9"><w n="9.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.2">v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="9.3">R<seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt</w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="9.5">pr<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.7">m<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
	</lg> 
</div></body></text></TEI>