<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB26">
	<head type="main">CHŒUR.</head>
	<head type="tune">Air : Buvons à plein verre. ( Fra-Diavolo.)</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t</w> <w n="1.2">d</w>'<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>rg<seg phoneme="y" type="vs" value="1" rule="447">u</seg>s</w></l>
		<l n="2" num="1.2"><w n="2.1">S<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="3" num="1.3"><w n="3.1">B<seg phoneme="y" type="vs" value="1" rule="449">u</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="3.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg></w> <w n="3.4">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
		<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.2">d<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="4.3">qu</w>'<w n="4.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="4.5">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="5" num="1.5"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="5.2">c</w>'<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="5.4">B<seg phoneme="a" type="vs" value="1" rule="339">a</seg>cch<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>.</l>
	</lg> 
	<lg n="2">
	<head type="speaker">EDMOND.</head>
		<l n="6" num="2.1"><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="6.2">l</w>'<w n="6.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>lt<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="7" num="2.2"><w n="7.1">D</w>'<w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="7.3">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rqu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="8" num="2.3"><w n="8.1">Tr<seg phoneme="o" type="vs" value="1" rule="432">o</seg>p</w> <w n="8.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g</w>-<w n="8.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w> <w n="8.4">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="8.5">t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="9" num="2.4"><w n="9.1">N<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="9.2">v<seg phoneme="ø" type="vs" value="1" rule="247">œu</seg>x</w>, <w n="9.3">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="9.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>rs</w> ;</l>
		<l n="10" num="2.5"><w n="10.1">Gr<seg phoneme="a" type="vs" value="1" rule="339">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.4">R<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="11" num="2.6"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="11.2">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w> <w n="11.3">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="12" num="2.7"><w n="12.1">R<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="12.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="13" num="2.8"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.2">j<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>, <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>rs</w>.</l>
	</lg>
</div></body></text></TEI>