<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB24">
	<head type="tune">Air : Pourquoi me réveiller. ( Gentilhomme de la chambre.)</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1">Vr<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="1.2">c</w>'<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w>'<w n="1.4"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.5">h<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>,</l>
		<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.2">j</w>'<w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="3" num="1.3"><w n="3.1"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.2">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
		<l n="4" num="1.4"><w n="4.1">J</w>'<w n="4.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="4.3">d</w>'<w n="4.4">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w></l>
		<l n="5" num="1.5"><w n="5.1">C<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="5.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="5.3">s<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="5.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="5.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>g<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> !</l>
	</lg>
	<lg n="2">
		<l n="6" num="2.1"><w n="6.1">R<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.4">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w></l>
		<l n="7" num="2.2"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="7.2">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.3">f<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="7.5">r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="8" num="2.3"><w n="8.1">D<seg phoneme="e" type="vs" value="1" rule="408">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="8.2">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.3">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.4">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rç<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w></l>
		<l n="9" num="2.4"><w n="9.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.2">l</w>'<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="9.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="9.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> ;</l>
		<l n="10" num="2.5"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="10.2">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>h<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="10.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w>-<w n="10.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>-<w n="10.5">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p</w></l>
		<l n="11" num="2.6"><w n="11.1">M</w>'<w n="11.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="11.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="11.5">R<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
		<l n="12" num="2.7"><w n="12.1">C<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.2">n</w>'<w n="12.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="12.5">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="12.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w></l>
		<l n="13" num="2.8"><w n="13.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.2">ch<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="13.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.4">S<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t</w>-<w n="13.5">Cl<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>d</w>.</l>
		<l n="14" num="2.9"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w>, <w n="14.3">c<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ! …</l>
		<l n="15" num="2.10"><w n="15.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="15.2">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="15.4">v<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="16" num="2.11"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="16.2">m</w>'<w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="16.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
		<l n="17" num="2.12"><w n="17.1">R<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="17.2">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.3">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="17.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
		<l n="18" num="2.13"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="18.2">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rt</w> <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="18.4">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>squ<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.5">d</w>'<w n="18.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ccr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>.</l>
		<l n="19" num="2.14"><w n="19.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="19.2">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="19.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>,</l>
		<l n="20" num="2.15"><w n="20.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="20.2">c<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="20.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.5">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="21" num="2.16"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="21.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="21.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="21.4">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>,</l>
		<l n="22" num="2.17"><w n="22.1">S</w>'<w n="22.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="22.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="22.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="22.5">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>.,</l>
		<l n="23" num="2.18"><w n="23.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="23.2">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="o" type="vs" value="1" rule="317">au</seg>ds</w> <w n="23.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="23.4">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>,</l>
		<l n="24" num="2.19"><w n="24.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="24.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="24.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="24.4"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
		<l n="25" num="2.20"><w n="25.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="25.2">l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="25.3">d</w>'<w n="25.4"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="25.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>,</l>
		<l n="26" num="2.21"><w n="26.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="26.2">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d</w> <w n="26.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="26.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="26.5">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>…</l>
	</lg> 
	<p>
Il est vrai que j'étais pourpre de colère, les yeux en feu, <lb></lb>
le<lb></lb>
bonnet de travers ; mais ce n'est pas une raison… et j'aurais eu<lb></lb>
56<lb></lb>
du plaisir à les soumetter tous ; mais, de peur d'être reprise, <lb></lb>
j'attrappe un fiacre et je reviens ventre à terre chercher ici le<lb></lb>
traître qui m'a enlevée, et lui demander raison de sa con<lb></lb>
duite… car
	</p>
	<lg n="3">
		<l n="27" num="3.1"><w n="27.1">Vr<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="27.2">c</w>'<w n="27.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="27.4"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="27.5">h<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>,</l>
		<l n="28" num="3.2"><w n="28.1">Qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="28.2">j</w>'<w n="28.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="29" num="3.3"><w n="29.1"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="29.2">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="29.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
		<l n="30" num="3.4"><w n="30.1">J</w>'<w n="30.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="30.3">d</w>'<w n="30.4">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w></l>
		<l n="31" num="3.5"><w n="31.1">C<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="31.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="31.3">s<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="31.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="31.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>g<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>.</l>
	</lg>
</div></body></text></TEI>