<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">À PROPOS PATRIOTIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="VIL" sort="1">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="MAS" sort="2">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>273 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">VEM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>À propos patriotique</title>
						<author>VILLENEUVE ET MASSON</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=0FA6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>À propos patriotique</title>
								<author>VILLENEUVE ET MASSON</author>
								<repository>Bayerische Staatsbibliothek</repository>
								<idno type="URI">https://opacplus.bsb-muenchen.de/title/BV008031366</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_subpart">SCENE IV</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="VEM12">
				<head type="tune">AIR : Le cordon s'il vous plaît.</head>
				<lg n="1">
					<head type="main">Mitouflet.</head>
					<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.2">c</w>'<w n="1.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="1.4">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <subst reason="analysis" hand="LG" type="repetition"><del>(bis.)</del><add rend="hidden"><w n="1.5">Qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.6">c</w>'<w n="1.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="1.8">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w></add></subst> <w n="1.9">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="1.10">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.11">pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="2.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="2.4">nʼ</w> <w n="2.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="300">ai</seg>t</w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="2.7">f<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>r</w>,</l>
					<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="3.2">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="3.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="3.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.5">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="3.7">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="4.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="4.6">fr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>,</l>
					<l n="5" num="1.5"><space unit="char" quantity="4"></space><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="5.2">l</w>'<w n="5.3"><seg phoneme="e" type="vs" value="1" rule="169">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="5.4">s</w>'<w n="5.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="5.7">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> ;</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="6.3">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="6.4">lʼ</w> <w n="6.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="6.7">lʼ</w> <w n="6.8">b<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="7.2">n</w>'<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="7.4">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>f</w> <w n="7.5">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.6">dʼ</w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.8">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="8" num="1.8"><space unit="char" quantity="4"></space><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rchʼs</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="8.5">gʼn<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="8.6">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="8.7">s</w>'<w n="8.8"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> :</l>
					<l n="9" num="1.9"><space unit="char" quantity="12"></space><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="9.2">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w>, <w n="9.3">s</w>'<w n="9.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="9.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="9.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>t</w>, <del reason="analysis" hand="LG" type="repetition">(bis)</del></l>
					<l n="10" num="1.10"><space unit="char" quantity="12"></space><subst reason="analysis" hand="LG" type="repetition"><del> </del><add rend="hidden"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="10.2">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w>, <w n="10.3">s</w>'<w n="10.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="10.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="10.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>t</w>,</add></subst></l>
					<l n="11" num="1.11"><space unit="char" quantity="12"></space><w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="11.2">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w>, <w n="11.3">s</w>'<w n="11.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="11.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="11.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>t</w>,</l>
					<l n="12" num="1.12"><space unit="char" quantity="6"></space><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="12.2">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="12.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="12.5">lʼ</w> <w n="12.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>dʼ</w> <w n="12.7">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.8">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>,</l>
				</lg>
			</div></body></text></TEI>