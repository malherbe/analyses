<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">À PROPOS PATRIOTIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="VIL" sort="1">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="MAS" sort="2">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>273 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">VEM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>À propos patriotique</title>
						<author>VILLENEUVE ET MASSON</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=0FA6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>À propos patriotique</title>
								<author>VILLENEUVE ET MASSON</author>
								<repository>Bayerische Staatsbibliothek</repository>
								<idno type="URI">https://opacplus.bsb-muenchen.de/title/BV008031366</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_subpart">SCÈNE PREMIÈRE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="VEM2">
				<head type="tune">AIR : Elle aime à rire, elle aime à boire.</head>
				<lg n="1">
					<head type="main">Bertrand (montrant un drapeau tricolore qu'il cachait.)</head>
					<l n="1" num="1.1">« <w n="1.1">C<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.2">dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="1.3">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.6">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="2" num="1.2">« <w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="2.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="2.4">qu</w>'<w n="2.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.7">c<seg phoneme="u" type="vs" value="1" rule="424">oû</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
					<l n="3" num="1.3">« <w n="3.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="3.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg></w> <w n="3.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.6">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w></l>
					<l n="4" num="1.4">« <w n="4.1">N<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="4.2">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="4.3">j<seg phoneme="w" type="sc" value="0" rule="430">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="4.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
					<l n="5" num="1.5">« <w n="5.1">Qu</w>'<w n="5.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="5.3">pr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="5.6"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ppr<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ss<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w></l>
					<l n="6" num="1.6">« <w n="6.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="6.2">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.3">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="6.5">r<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
				</lg>
				<div type="section" n="1">
					<head type="main">Les chœurs</head>
					<lg n="1">
						<l n="7" num="1.1">« <w n="7.1"><seg phoneme="w" type="sc" value="0" rule="430">Ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.3">s<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>c<seg phoneme="u" type="vs" value="1" rule="424">oû</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ss<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="8" num="1.2">« <w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="8.2">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="8.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="8.5">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>.</l>
					</lg>
				</div>
			</div></body></text></TEI>