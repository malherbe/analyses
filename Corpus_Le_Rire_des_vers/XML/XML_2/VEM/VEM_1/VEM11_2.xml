<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">À PROPOS PATRIOTIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="VIL" sort="1">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="MAS" sort="2">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>273 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">VEM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>À propos patriotique</title>
						<author>VILLENEUVE ET MASSON</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=0FA6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>À propos patriotique</title>
								<author>VILLENEUVE ET MASSON</author>
								<repository>Bayerische Staatsbibliothek</repository>
								<idno type="URI">https://opacplus.bsb-muenchen.de/title/BV008031366</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_subpart">SCENE IV</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="VEM11">
				<head type="tune">MÊME AIR</head>
				<lg n="1">
					<head type="main">GACHEUX entrant, grotesquement vêtu.</head>
					<l n="1" num="1.1"><space unit="char" quantity="20"></space><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.2">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.3">ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.4">qu</w>'<w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="1.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w></l>
					<l n="2" num="1.2"><space unit="char" quantity="20"></space><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.3">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="2.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="2.6">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
					<l n="3" num="1.3"><space unit="char" quantity="20"></space><w n="3.1">L<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="3.2">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="3.3">ch<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="4.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="4.3">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="4.4">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.5">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="4.6">f<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="?" type="va" value="1" rule="33">er</seg></w>, <subst type="repetition" hand="LG" reason="analysis"><del>(bis.)</del><add rend="hidden"><w n="4.7"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="4.8">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="4.9">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="4.10">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.11">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="4.12">f<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="?" type="va" value="1" rule="33">er</seg></w>,</add></subst> <w n="4.13">d</w>'<w n="4.14"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>trʼ</w> <w n="4.15">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="362">en</seg></w> <w n="4.16">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w>.</l>
					<l n="5" num="1.5"><space unit="char" quantity="16"></space><w n="5.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.2">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="5.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="5.5">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="5.6">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.8">b<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="6" num="1.6"><space unit="char" quantity="14"></space><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="6.2">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.3">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="6.4">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="6.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.7">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p</w> <w n="6.8">d</w>'<w n="6.9"><seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w></l>
					<l n="7" num="1.7"><space unit="char" quantity="20"></space><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="7.2">p<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>squ</w>'<w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="7.4">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="7.5">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.6">jʼ</w> <w n="7.7">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="8" num="1.8"><space unit="char" quantity="16"></space><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="8.3">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="8.4">nʼ</w> <w n="8.5">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="8.6">qu</w>'<w n="8.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="8.8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.9"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="8.10">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w>.</l>
					<l n="9" num="1.9"><space unit="char" quantity="24"></space><w n="9.1">J</w>'<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="9.3">dʼ</w> <w n="9.4">l</w>'<w n="9.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="9.7">dʼ</w> <w n="9.8">l</w>'<w n="9.9"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="10" num="1.10"><space unit="char" quantity="24"></space><w n="10.1">G<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rʼ</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.3">t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="10.4">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>,</l>
					<l n="11" num="1.11"><space unit="char" quantity="24"></space><w n="11.1">C<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="11.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="11.3">v<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>lʼnt</w> <w n="11.4">qu</w>'<w n="11.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="11.7">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="12" num="1.12"><space unit="char" quantity="22"></space><w n="12.1">P<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="496">y</seg>ʼr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.3">p<seg phoneme="o" type="vs" value="1" rule="437">o</seg>ts</w> <w n="12.4">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>.</l>
					<p>(Ensemble en se donnant le bras.)</p>
					<l n="13" num="1.13"><space unit="char" quantity="20"></space><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.2">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.3">ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.4">qu</w>'<w n="13.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="13.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w>, <del type="alternative" hand="RR" reason="analysis">etc.</del></l>
					<l n="14" num="1.14"><space unit="char" quantity="20"></space><subst type="repetition" hand="LG" reason="analysis"><del></del><add rend="hidden"><w n="14.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="14.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.3">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="14.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="14.6">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, </add></subst></l>
					<l n="15" num="1.15"><space unit="char" quantity="20"></space><subst type="repetition" hand="LG" reason="analysis"><del></del><add rend="hidden"><w n="15.1">L<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="15.2">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="15.3">ch<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ! </add></subst></l>
					<l n="16" num="1.16"><subst type="repetition" hand="LG" reason="analysis"><del></del><add rend="hidden"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="16.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="16.3">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="16.4">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.5">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="16.6">f<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="?" type="va" value="1" rule="33">er</seg></w>, <del>(bis.)</del> <w n="16.7"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="16.8">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="16.9">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="16.10">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.11">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="16.12">f<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="?" type="va" value="1" rule="33">er</seg></w>, <w n="16.13">d</w>'<w n="16.14"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>trʼ</w> <w n="16.15">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="362">en</seg></w> <w n="16.16">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w>.</add></subst></l>
				</lg>
			</div></body></text></TEI>