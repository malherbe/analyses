<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">À PROPOS PATRIOTIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="VIL" sort="1">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="MAS" sort="2">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>273 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">VEM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>À propos patriotique</title>
						<author>VILLENEUVE ET MASSON</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=0FA6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>À propos patriotique</title>
								<author>VILLENEUVE ET MASSON</author>
								<repository>Bayerische Staatsbibliothek</repository>
								<idno type="URI">https://opacplus.bsb-muenchen.de/title/BV008031366</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_subpart">SCENE IV</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="VEM13">
				<head type="tune">AIR : Que d'établissemens nouveaux.</head>
				<lg n="1">
					<head type="main">Gacheux.</head>
					<l n="1" num="1.1"><w n="1.1">F<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="1.2">sʼ</w> <w n="1.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w></l>
					<l n="2" num="1.2"><w n="2.1">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>tʼ</w> <w n="2.2">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="2.3">s</w>'<w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="2.5">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="2.7">l</w>'<w n="2.8"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="3.4">n<seg phoneme="o" type="vs" value="1" rule="443">o</seg>tʼ</w> <w n="3.5">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p</w> <w n="3.6">d</w>'<w n="3.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w></l>
					<l n="4" num="1.4"><w n="4.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="4.2">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ff<seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="341">À</seg></w> <w n="5.2">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="5.3">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.4">f<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="5.5">dʼ</w> <w n="5.6">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="5.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w></l>
					<l n="6" num="1.6"><w n="6.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.2">gl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="6.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.5">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
					<l n="7" num="1.7"><w n="7.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.2">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="7.3">pr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="7.4">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w></l>
					<l n="8" num="1.8"><w n="8.1">Nʼ</w> <w n="8.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="8.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> <w n="8.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="8.6">m<seg phoneme="a" type="vs" value="1" rule="342">a</seg>n<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>