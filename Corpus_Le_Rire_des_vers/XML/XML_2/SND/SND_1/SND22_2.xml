<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE LIEUTENANT D'ARTILLERIE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="NSL" sort="2">
					<name>
						<forename>Jean-Pierre-Laurent</forename>
						<surname>DENOMBRET</surname>
						<addName type="pen_name">Charles NOMBRET SAINT-LAURENT</addName>
					</name>
					<date from="1791" to="1833">1791-1833</date>
				</author>
					<author key="DUV" sort="3">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>225 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SND_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=NWU:35556007830409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
								<repository>Northwestern university</repository>
								<idno type="URI">https://babel.hathitrust.org/cgi/pt?id=ien.35556007830409</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE XII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SND22">
				<head type="tune">AIR : Ce que j'éprouve en vous voyant.</head>
				<lg n="1">
					<head type="main">VICTORINE, avec dignité.</head>
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="w" type="sc" value="0" rule="430">Ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="1.2">c</w>'<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="1.4">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="1.5">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w> <w n="1.6">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.7">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.8">p<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="1.9"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xp<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ;</l>
					<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">C</w>'<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="2.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="2.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="2.6">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="2.7">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>-<w n="2.8"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
					<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.3">s<seg phoneme="ə" type="vi" value="1" rule="356">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.5">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="3.6">m<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="4.2">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="4.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
					<l n="5" num="1.5"><w n="5.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.2">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.3">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="5.4">cr<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="5.5">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.6">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="5.7">d</w>'<w n="5.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="5.9">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>sp<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>.</l>
					<l n="6" num="1.6"><space unit="char" quantity="4"></space><w n="6.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="6.2">l</w>'<w n="6.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="6.4">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.5">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.6">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
					<l n="7" num="1.7"><space unit="char" quantity="4"></space><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="7.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="7.3">j</w>'<w n="7.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>pl<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.5">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>,</l>
					<l n="8" num="1.8"><space unit="char" quantity="4"></space><w n="8.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w> <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w>'<w n="8.5">h<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>,</l>
					<l n="9" num="1.9"><space unit="char" quantity="4"></space><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="9.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="9.3">j</w>'<w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="9.5">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="9.6">s<seg phoneme="y" type="vs" value="1" rule="444">û</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
					<l n="10" num="1.10"><space unit="char" quantity="4"></space><w n="10.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="10.2">s</w>'<w n="10.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="10.4">m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rt</w>, <w n="10.5">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.6">m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="10.8">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>. <del hand="LG" reason="analysis" type="repetition">(bis.)</del></l>
					<l n="11" num="1.11"><space unit="char" quantity="4"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="11.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="11.2">s</w>'<w n="11.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="11.4">m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rt</w>, <w n="11.5">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.6">m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="11.8">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>. </add></subst></l>
				</lg>
			</div></body></text></TEI>