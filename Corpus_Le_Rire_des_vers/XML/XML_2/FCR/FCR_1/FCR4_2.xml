<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA MÉTEMPSYCOSE</title>
				<title type="sub">BÊTISE EN UN ACTE, MÊLÉE DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="COU" sort="1">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<author key="JAI" sort="2">
					<name>
						<forename>Ernest</forename>
						<surname>JAIME</surname>
					</name>
					<date from="1804" to="1884">1804-1884</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>282 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FCR_1</idno>	
				<availability status="free">
					<licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA MÉTEMPSYCOSE</title>
						<author>FRÉDÉRIC DE COURCY ET JAIME</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=c1RoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032819917.0x000001# ?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">7 FÉVRIER 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FCR4">
	<head type="tune">Air de Préville et Taconnet.</head>
	<lg n="1">
		<l n="1" num="1.1"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="1.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.3">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="1.4">j</w>'<w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="1.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>s</w>, <w n="1.7">m</w>'<w n="1.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
		<l n="2" num="1.2"><w n="2.1">M<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="2.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.3">b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>, <w n="2.4">c</w>'<w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="2.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.7">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.8"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="2.9">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</l>
		<l n="3" num="1.3"><w n="3.1">S</w>'<w n="3.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="3.4">qu</w>'<w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="3.6">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="3.7"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.8">m<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
		<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.2">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="4.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="444">û</seg>t</w> <w n="4.5">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.6">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
		<l n="5" num="1.5"><w n="5.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.2">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="5.3">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> : <w n="5.4">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="5.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> <w n="5.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w> <w n="5.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ?</l>
		<l n="6" num="1.6"><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="6.2">j</w>'<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="6.4">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="6.5">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.6">t</w>'<w n="6.7"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="6.8">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="6.9">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.10">v<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="7" num="1.7"><w n="7.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.2">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="7.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="7.5">t<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="7.6">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.7">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w>,</l>
		<l n="8" num="1.8"><w n="8.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="8.2">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.5">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="8.7">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="8.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w>…</l>
		<l n="9" num="1.9"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="9.2">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.4">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="9.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="9.7">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="9.8">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
		<l n="10" num="1.10"><w n="10.1">C</w>'<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="10.4">m<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="10.5">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="10.7">t</w>'<w n="10.8"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="10.9">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="10.10">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g</w>-<w n="10.11">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w>.</l>
	</lg>
</div></body></text></TEI>