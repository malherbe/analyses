<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB46" modus="cp" lm_max="10" metProfile="4+6, (8)" form="suite de strophes" schema="2[abab]">
	<head type="tune">AIR : Vaudeville du Colonel.</head>
	<lg n="1" type="regexp" rhyme="ababa">
		<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="1.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="M">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4" caesura="1">en</seg>ds</w><caesura></caesura> <w n="1.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="1.5" punct="vg:7">tr<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="vg" mp="F">e</seg></w>, <w n="1.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="1.7" punct="dp:10">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="M">an</seg>g<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></w> :</l>
		<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">j</w>'<w n="2.4"><seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>pr<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="2.5" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg">i</seg></w>, <w n="2.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="2.7">l</w>'<w n="2.8" punct="dp:10"><seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>pr<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>v<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="dp">ez</seg></rhyme></w> :</l>
		<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">s<seg phoneme="o" type="vs" value="1" rule="317" place="3" mp="M">au</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="3.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="C">ou</seg>s</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w> <w n="3.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="C">on</seg></w> <w n="3.7" punct="vg:10">c<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>r<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
		<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="4.3" punct="vg:4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="189" place="4" punct="vg" caesura="1">e</seg>ts</w>,<caesura></caesura> <w n="4.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6" punct="vg">in</seg></w>, <w n="4.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="C">ou</seg>s</w> <w n="4.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="4.7" punct="pt:10">s<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>v<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pt">ez</seg></rhyme></w>.</l>
		<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg></w> <w n="5.3" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="422" place="4" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="5.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="5.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="o" type="vs" value="1" rule="317" place="7" mp="C">au</seg></w> <w n="5.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>d</w> <w n="5.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="5.9">l</w>'<w n="5.10" punct="pt:10"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="10">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l> 
	</lg>
	<lg n="2" type="regexp" rhyme="">
	<head type="speaker">AMÉLIE.</head>
		<l part="I" n="6" num="2.1" lm="10" met="4+6"><w n="6.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="1">è</seg>s</w> <w n="6.2">d</w>'<w n="6.3"><seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4" punct="ps:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="481" place="4" punct="ps" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>…<caesura></caesura> </l>
	</lg>
	<lg n="3" type="regexp" rhyme="bab">
	<head type="speaker">HENRIETTE,</head>
		<l part="F" n="6" lm="10" met="4+6"><w n="6.5" punct="vg:7"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="5" mp="M">Im</seg>p<seg phoneme="o" type="vs" value="1" rule="434" place="6" mp="M">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="7" punct="vg">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="6.7" punct="dp:10">n<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="dp">ou</seg>s</rhyme></w> :</l>
		<l n="7" num="3.1" lm="10" met="4+6"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="C">ou</seg>s</w> <w n="7.2">lu<seg phoneme="i" type="vs" value="1" rule="490" place="2" mp="C">i</seg></w> <w n="7.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="M">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">ez</seg></w><caesura></caesura> <w n="7.4">tr<seg phoneme="o" type="vs" value="1" rule="432" place="5">o</seg>p</w> <w n="7.5">d</w>'<w n="7.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="vg">é</seg></w>, <w n="7.7" punct="vg:10">m<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>d<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
		<l n="8" num="3.2" lm="8"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="8.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">j</w>'<w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="8.6"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="8.7">p<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r</w> <w n="8.8" punct="pt:8">v<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">ou</seg>s</rhyme></w>.</l>
	</lg> 
</div></body></text></TEI>