<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONARDIN DANS LA LUNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="HON">
					<name>
						<forename>Charles-Honoré</forename>
						<surname>RÉMY</surname>
						<addName type="pen_name">HONORÉ</addName>
					</name>
					<date from="1793" to="1858">1793-1858</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>16 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HON_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>BONARDIN DANS LA LUNE, OU LA MONOMANIE ASTRONOMIQUE</title>
						<author>HONORÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=-DFMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>BONARDIN DANS LA LUNE, OU LA MONOMANIE ASTRONOMIQUE</title>
								<author>HONORÉ</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160357301</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les lettres ligaturées (œ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_subpart">SCÈNE PREMIÈRE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="HON1" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(ababcdcd)">
					<head type="tune">AIR : Femme voulez-vous éprouver.</head> 
					<p>(Sans accompagnement.)</p>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<head type="main">LE MARIÉ, avec prétention.</head>
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="1.3">h<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="1.4">d</w>'<w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>x</w> <w n="1.7">qu<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>d</rhyme></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="2.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>t</w> <w n="2.3">s</w>'<w n="2.4"><seg phoneme="y" type="vs" value="1" rule="452" place="3">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r</w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="2.6" punct="pv:8">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="3.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="3.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>rs</w> <w n="3.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="6">en</seg></w> <w n="3.5" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>t<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="ri_1" place="8" punct="vg">en</seg>t</rhyme></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="4.2">f<seg phoneme="a" type="vs" value="1" rule="192" place="2">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="4.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>g</w>-<w n="4.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">em</seg>ps</w> <w n="4.6" punct="pt:8">s<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="5.3">qu</w>'<w n="5.4"><seg phoneme="o" type="vs" value="1" rule="434" place="4">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="5.6">c<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="248" place="8">œu</seg>r</rhyme></w></l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="6.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3">qu</w>'<w n="6.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="6.6" punct="vg:8">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>d<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">S<seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="7.2">lu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="7.3">v<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>l<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>r</w> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.5">b<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>nh<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>r</rhyme></w></l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="8.2"><seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg></w> <hi rend="ital"><w n="8.4" punct="pt:8">h<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rn<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w></hi>.</l>
					</lg>
				</div></body></text></TEI>