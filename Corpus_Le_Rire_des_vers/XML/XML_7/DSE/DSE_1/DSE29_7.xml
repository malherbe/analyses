<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE29" modus="cp" lm_max="10" metProfile="4, 4+6" form="strophe unique" schema="1(aabb)">
	
			<head type="main">CHŒUR.</head>

 <head type="tune">Air : De monsieur Jean que le bonheur s’apprête ! (de Jean de Paris.)</head>

		   <lg n="1" type="quatrain" rhyme="aabb">
			   <l n="1" num="1.1" lm="4" met="4"><w n="1.1" punct="pe:4">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>du<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>s<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="pe">on</seg>s</rhyme></w> ! <del hand="LN" type="repetition" reason="analysis">(bis.)</del></l>
				<l n="2" num="1.2" lm="4" met="4"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">Em</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ss<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</rhyme></w></l>
				<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1" punct="vg:4">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" mp="M">on</seg>du<seg phoneme="i" type="vs" value="1" rule="490" place="3" mp="M">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg" caesura="1">on</seg>s</w>,<caesura></caesura> <w n="3.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.4" punct="pe:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>xp<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>r<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></w> !</l>
				<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2" mp="M">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="4.3">j<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>r</w><caesura></caesura> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="4.5">l</w>’<w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg></w> <w n="4.7">m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>l</w> <w n="4.8">hu<seg phoneme="i" type="vs" value="1" rule="490" place="8">i</seg>t</w> <w n="4.9">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="9">en</seg>t</w> <w n="4.10" punct="pt:10">tr<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
			</lg>
		</div></body></text></TEI>