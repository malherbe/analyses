<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE8" modus="cp" lm_max="10" metProfile="8, 4+6" form="suite de strophes" schema="1[ababa] 3[aa] 1[aaa]">			
	
	<head type="main">ZÉPHIRINE.</head>

			<p>Ah ! c’est charmant ; je raffole du bon temps : on avait de la religion alors.</p>

				
					<head type="main">L’AURÉOLE.</head>
					<head type="tune">Même air.</head>
				<lg n="1" type="regexp" rhyme="ababaaaaaaaaa"><l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="1.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="1.4" punct="vg:4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4" punct="vg">em</seg>ps</w>, <w n="1.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.6">d<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>s</w> <w n="1.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.8" punct="vg:8">cr<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="2.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="5">en</seg>t</w> <w n="2.4">ch<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="2.5" punct="pv:8">j<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pv">ou</seg>r</rhyme></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg></w> <w n="3.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>gn<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg></w> <w n="3.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="3.6">l</w>’<w n="3.7" punct="pv:8">h<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>st<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2" punct="vg">in</seg></w>, <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="4.3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.5" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="vg">ou</seg>r</rhyme></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">C<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="442" place="3">o</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.3" punct="vg:4"><seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">où</seg></w>, <w n="5.4">f<seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.6" punct="vg:8">gl<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>vi<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="6.4">m<seg phoneme="ɛ" type="vs" value="1" rule="351" place="5">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="6.6">l</w>’<w n="6.7" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pe">ou</seg>r</rhyme></w> !</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">si<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="5">en</seg>t</w> <w n="7.4" punct="pv:8">P<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">om</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>d<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pv">ou</seg>r</rhyme></w> ;</l>
					<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="8.2">c<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="M">ou</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4" caesura="1">an</seg>s</w><caesura></caesura> <w n="8.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="8.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fm">e</seg></w>-<w n="8.6" punct="vg:10">pu<seg phoneme="i" type="vs" value="1" rule="490" place="9" mp="M/mc">i</seg>ss<rhyme label="a" id="4" gender="f" type="a" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="9" num="1.9" lm="10" met="4+6"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="9.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="9.3">tr<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="M">u</seg>ff<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="9.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="C">i</seg>ls</w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>nt</w> <w n="9.6">d</w>’<w n="9.7" punct="dp:10"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>bst<seg phoneme="i" type="vs" value="1" rule="466" place="9" mp="M">i</seg>n<rhyme label="a" id="4" gender="f" type="e" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></w> :</l>
					<l n="10" num="1.10" lm="10" met="4+6"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">s<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" caesura="1">ain</seg></w><caesura></caesura> <w n="10.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>d</w> <w n="10.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="C">on</seg></w> <w n="10.5" punct="pv:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" mp="M">in</seg>s<seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>l<rhyme label="a" id="5" gender="f" type="a" stanza="4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="11" num="1.11" lm="10" met="4+6"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="11.2">cr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" caesura="1">in</seg></w><caesura></caesura> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="11.6">sc<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>ptr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="11.8" punct="pe:10">Fr<rhyme label="a" id="5" gender="f" type="e" stanza="4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					<l n="12" num="1.12" lm="10" met="4+6"><w n="12.1">Vi<seg phoneme="ɛ" type="vs" value="1" rule="365" place="1">e</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2" mp="C">un</seg></w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="12.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="12.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="12.6" punct="vg:10">pr<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>st<rhyme label="a" id="6" gender="m" type="a" stanza="5"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="10" punct="vg">an</seg>s</rhyme></w>,</l>
					<l n="13" num="1.13" lm="8" met="8"><w n="13.1">V<seg phoneme="wa" type="vs" value="1" rule="419" place="1">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="13.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="13.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="13.6" punct="pt:8">t<rhyme label="a" id="6" gender="m" type="e" stanza="5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8" punct="pt">em</seg>ps</rhyme></w>.</l></lg>

					<head type="main">ZÉPHIRINE et BIFFARD.</head>
					<lg n="2" type="regexp" rhyme="a"><l n="14" num="2.1" lm="8" met="8"><w n="14.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="14.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="14.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="14.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="14.6" punct="pe:8">t<rhyme label="a" id="6" gender="m" type="a" stanza="5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8" punct="pe">em</seg>ps</rhyme></w> !</l></lg>
			</div></body></text></TEI>