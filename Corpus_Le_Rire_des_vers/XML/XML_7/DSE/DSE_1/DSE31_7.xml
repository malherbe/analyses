<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE31" modus="sp" lm_max="8" metProfile="3, 4, 2, 6, 8" form="suite de strophes" schema="9[aa] 2[aaa]">			
	
	
	<head type="main">TOUS</head>

			<head type="tune">AIR : Grace au vent.</head>

		   <lg n="1" type="regexp" rhyme="aaaaaaaaaaaaaaa"><l n="1" num="1.1" lm="3" met="3"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="1.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>s</w> <w n="1.3" punct="vg:3">y<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ø" type="vs" value="1" rule="397" place="3" punct="vg">eu</seg>x</rhyme></w>,</l>
			<l n="2" num="1.2" lm="3" met="3"><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="2.3">d<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</rhyme></w></l>
			<l n="3" num="1.3" lm="4" met="4"><w n="3.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>x</w> <w n="3.3" punct="pe:4">ci<rhyme label="a" id="2" gender="m" type="a" stanza="2"><seg phoneme="ø" type="vs" value="1" rule="397" place="4" punct="pe">eu</seg>x</rhyme></w> !</l>
		   <l n="4" num="1.4" lm="2" met="2"> <w n="4.1">N<seg phoneme="o" type="vs" value="1" rule="437" place="1">o</seg>s</w> <w n="4.2" punct="vg:2">v<rhyme label="a" id="2" gender="m" type="e" stanza="2"><seg phoneme="ø" type="vs" value="1" rule="247" place="2" punct="vg">œu</seg>x</rhyme></w>,</l>
			<l n="5" num="1.5" lm="4" met="4"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="5.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>s</w> <w n="5.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>gr<rhyme label="a" id="3" gender="m" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="4">e</seg>ts</rhyme></w></l>
			<l n="6" num="1.6" lm="4" met="4"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="6.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>s</w> <w n="6.3" punct="vg:4">s<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>h<rhyme label="a" id="3" gender="m" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="vg">ai</seg>ts</rhyme></w>,</l>
			<l n="7" num="1.7" lm="4" met="4"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>pt<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w>-<w n="7.2" punct="vg:4">l<rhyme label="a" id="4" gender="m" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="160" place="4" punct="vg">e</seg>s</rhyme></w>,</l>
			<l n="8" num="1.8" lm="4" met="4"><w n="8.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="8.2" punct="vg:4">d<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rm<rhyme label="a" id="4" gender="m" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="vg">ai</seg>s</rhyme></w>,</l>
			<l n="9" num="1.9" lm="2" met="2"><w n="9.1">J<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>m<rhyme label="a" id="4" gender="m" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</rhyme></w></l>
			<l n="10" num="1.10" lm="4" met="4"><w n="10.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="10.2">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>nn<rhyme label="a" id="5" gender="f" type="a" stanza="5"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
			<l n="11" num="1.11" lm="6" met="6"><w n="11.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="11.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="11.4" punct="vg:6">d<seg phoneme="o" type="vs" value="1" rule="434" place="5">o</seg>nn<rhyme label="a" id="5" gender="f" type="e" stanza="5"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
			<l n="12" num="1.12" lm="4" met="4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="12.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>s</w> <w n="12.3" punct="vg:4">n<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<rhyme label="a" id="6" gender="m" type="a" stanza="6"><seg phoneme="ø" type="vs" value="1" rule="397" place="4" punct="vg">eu</seg>x</rhyme></w>,</l>
			<l n="13" num="1.13" lm="4" met="4"><w n="13.1" punct="vg:2">L<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="13.2" punct="pt:4">h<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r<rhyme label="a" id="6" gender="m" type="e" stanza="6"><seg phoneme="ø" type="vs" value="1" rule="397" place="4" punct="pt pt pt">eu</seg>x</rhyme></w> ...</l>
			<l n="14" num="1.14" lm="4" met="4"><w n="14.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="14.2" punct="vg:4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<rhyme label="a" id="7" gender="m" type="a" stanza="7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg">on</seg>t</rhyme></w>,</l>
			<l n="15" num="1.15" lm="4" met="4"><w n="15.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="15.2" punct="pe:4">b<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<rhyme label="a" id="7" gender="m" type="e" stanza="7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="pe">on</seg>t</rhyme></w> !</l></lg>

		   <lg n="2" type="regexp" rhyme="aaaa"><l n="16" num="2.1" lm="6" met="6"><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="16.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">v<seg phoneme="wa" type="vs" value="1" rule="439" place="3">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w>-<w n="16.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="16.5" punct="pi:6">l<rhyme label="a" id="8" gender="m" type="a" stanza="8"><seg phoneme="a" type="vs" value="1" rule="341" place="6" punct="pi">à</seg></rhyme></w> ?</l>
			<l n="17" num="2.2" lm="6" met="6"><w n="17.1" punct="pe:1">Qu<seg phoneme="wa" type="vs" value="1" rule="280" place="1" punct="pe pt pt pt">oi</seg></w> ! ... <w n="17.2">c</w>’<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="17.4">l</w>’<w n="17.5"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.6" punct="pt:6">d<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>j<rhyme label="a" id="8" gender="m" type="e" stanza="8"><seg phoneme="a" type="vs" value="1" rule="341" place="6" punct="pt pt pt pt">à</seg></rhyme></w> ....</l>
			<l n="18" num="2.3" lm="8" met="8"><w n="18.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="18.3" punct="pe:4">v<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="4" punct="pe pt pt pt">à</seg></w> ! ... <w n="18.4" punct="vg:5">ou<seg phoneme="i" type="vs" value="1" rule="490" place="5" punct="vg">i</seg></w>, <w n="18.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="18.6" punct="pe:8">v<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>l<rhyme label="a" id="9" gender="m" type="a" stanza="9"><seg phoneme="a" type="vs" value="1" rule="341" place="8" punct="pe">à</seg></rhyme></w> !</l>
			<l n="19" num="2.4" lm="8" met="8"><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="19.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="19.5">l</w>’<w n="19.6" punct="pi:8"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381" place="6">e</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="a" id="9" gender="m" type="e" stanza="9"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pi">a</seg></rhyme></w> ?</l></lg>

			<lg n="3" type="regexp" rhyme="aaaaa"><l n="20" num="3.1" lm="6" met="6"><w n="20.1" punct="vg:2">R<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>gn<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="vg">ez</seg></w>, <w n="20.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="3">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>nn<rhyme label="a" id="10" gender="f" type="a" stanza="10"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
			<l n="21" num="3.2" lm="6" met="6"><w n="21.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="21.2">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="21.3" punct="pv:6">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>st<seg phoneme="i" type="vs" value="1" rule="466" place="5">i</seg>n<rhyme label="a" id="10" gender="f" type="e" stanza="10"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></w> ;</l>
			<l n="22" num="3.3" lm="4" met="4"><w n="22.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="22.2">s<seg phoneme="ɔ" type="vs" value="1" rule="418" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="22.3">t<rhyme label="a" id="11" gender="m" type="a" stanza="11"><seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</rhyme></w></l>
			<l n="23" num="3.4" lm="4" met="4"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="23.2">v<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>s</w> <w n="23.3" punct="pe:4">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<rhyme label="a" id="11" gender="m" type="e" stanza="11"><seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="pe">ou</seg>x</rhyme></w> !</l>
			<l n="24" num="3.5" lm="8" met="8"><w n="24.1">R<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381" place="2">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w>-<w n="24.2" punct="pe:4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="pe">ou</seg>s</w> ! <w n="24.3">r<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381" place="6">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w>-<w n="24.4" punct="pe:8">v<rhyme label="a" id="11" gender="m" type="a" stanza="11"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pe">ou</seg>s</rhyme></w> !</l></lg></div></body></text></TEI>