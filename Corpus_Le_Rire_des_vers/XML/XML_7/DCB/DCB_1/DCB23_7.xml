<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB23" modus="sp" lm_max="7" metProfile="5, (4), (6), (7)" form="strophe unique" schema="1(ababccdde)">
	<head type="main">CHOEUR.</head>
	<head type="tune">AIR de la mazurck polonaise.</head>
	<lg n="1" type="neuvain" rhyme="ababccdde">
		<l n="1" num="1.1" lm="5" met="5"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">v<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>t</w>' <w n="1.3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="466" place="4">i</seg>n<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w></l>
		<l n="2" num="1.2" lm="5" met="5"><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="2.3"><seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg>s</rhyme></w></l>
		<l n="3" num="1.3" lm="5" met="5"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="3.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="3.4">h<seg phoneme="o" type="vs" value="1" rule="434" place="4">o</seg>mm<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w></l>
		<l n="4" num="1.4" lm="5" met="5"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>rr<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>vʼnt</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="4.3" punct="pt:5">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>mi<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="5" punct="pt">er</seg>s</rhyme></w>.</l>
		<l n="5" num="1.5" lm="5" met="5"><w n="5.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="5.2">v<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>vʼnt</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <hi rend="ital"><w n="5.4">j<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>m<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>s</rhyme></w></hi></l>
		<l n="6" num="1.6" lm="4"><w n="6.1">V<seg phoneme="o" type="vs" value="1" rule="437" place="1">o</seg>s</w> <w n="6.2">d<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>x</w> <hi rend="ital"><w n="6.3" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ttr<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="pe">é</seg>s</rhyme></w> !</hi></l>
		<l n="7" num="1.7" lm="6"><w n="7.1">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="7.3" punct="pv:6">f<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>c<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></w> ;</l>
		<l n="8" num="1.8" lm="7"><w n="8.1">C</w>'<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="8.3">c</w>'<w n="8.4">qu</w>'<w n="8.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="8.6">d<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>t</w> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="8.8">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>tr</w>' <w n="8.9" punct="vg:7">m<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>r<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
		<l n="9" num="1.9" lm="5" met="5"><w n="9.1">B<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2" punct="pe:5">M<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rgu<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<rhyme label="e" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></w> !</l>
	</lg>
</div></body></text></TEI>