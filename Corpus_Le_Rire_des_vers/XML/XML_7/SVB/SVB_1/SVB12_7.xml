<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB12" modus="cp" lm_max="9" metProfile="6, 3+6" form="suite de strophes" schema="5[aa] 2[abba] 1[aaa]">
	<head type="tune">Air : Que de mal, de tourment.</head>
	<lg n="1" type="regexp" rhyme="aaabbaaaaaaaaaaabbaaa">
		<l n="1" num="1.1" lm="6" met="6"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3" punct="vg:3">m<seg phoneme="o" type="vs" value="1" rule="317" place="3" punct="vg">au</seg>x</w>, <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.5">d</w>'<w n="1.6" punct="pe:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="358" place="5">en</seg>nu<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="490" place="6" punct="pe">i</seg>s</rhyme></w> !</l>
		<l n="2" num="1.2" lm="6" met="6"><w n="2.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>ls</w> <w n="2.2">j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rs</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="188" place="3">e</seg>t</w> <w n="2.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="2.5" punct="vg:6">nu<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="490" place="6" punct="vg">i</seg>ts</rhyme></w>,</l>
		<l n="3" num="1.3" lm="9" met="3+6"><w n="3.1">J</w>'<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="3" caesura="1">é</seg>s</w><caesura></caesura> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4" mp="C">au</seg></w> <w n="3.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>d</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="3.7">m<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="3.8" punct="vg:9">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>tr<rhyme label="a" id="2" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="9">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="vg" mp="F">e</seg></rhyme></w>,</l>
		<l n="4" num="1.4" lm="6" met="6"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="4.2">qu</w>'<w n="4.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>x</w> <w n="4.4" punct="vg:6">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">am</seg>b<rhyme label="b" id="3" gender="m" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="vg">é</seg></rhyme></w>,</l>
		<l n="5" num="1.5" lm="6" met="6"><w n="5.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">sc<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>t</w> <w n="5.3">d</w>'<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>bb<rhyme label="b" id="3" gender="m" type="e" stanza="2"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></rhyme></w></l>
		<l n="6" num="1.6" lm="9" met="3+6"><w n="6.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w> <w n="6.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="3" caesura="1">i</seg></w><caesura></caesura> <w n="6.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4" mp="P">an</seg>s</w> <w n="6.4" punct="vg:6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="M">am</seg>b<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="vg">ou</seg>r</w>, <w n="6.5">n<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="6.6" punct="pe:9">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="M">om</seg>p<rhyme label="a" id="2" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="9">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="pe" mp="F">e</seg></rhyme></w> !</l>
		<l n="7" num="1.7" lm="6" met="6"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="7.3">s<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>vi<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="7.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="7.5">d<rhyme label="a" id="4" gender="m" type="a" stanza="3"><seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</rhyme></w></l>
		<l n="8" num="1.8" lm="6" met="6"><w n="8.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="2">en</seg></w> <w n="8.2"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="8.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ffr<rhyme label="a" id="4" gender="m" type="e" stanza="3"><seg phoneme="ø" type="vs" value="1" rule="397" place="6" punct="vg">eu</seg>x</rhyme></w>,</l>
		<l n="9" num="1.9" lm="6" met="6"><w n="9.1">D</w>'<w n="9.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>r</w> <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>rs</w> <w n="9.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="5">ain</seg>s<rhyme label="a" id="5" gender="m" type="a" stanza="4"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></rhyme></w></l>
		<l n="10" num="1.10" lm="6" met="6"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="3">en</seg>t</w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r</w> <w n="10.4" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r<rhyme label="a" id="5" gender="m" type="e" stanza="4"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg">i</seg></rhyme></w>,</l>
		<l n="11" num="1.11" lm="6" met="6"><w n="11.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rsqu</w>'<w n="11.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="11.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>lh<rhyme label="a" id="6" gender="m" type="a" stanza="5"><seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>rs</rhyme></w></l>
		<l n="12" num="1.12" lm="6" met="6"><w n="12.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="12.2">j<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="2">oin</seg>t</w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>r</w> <w n="12.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="12.5" punct="pe:6">m<rhyme label="a" id="6" gender="m" type="e" stanza="5"><seg phoneme="œ" type="vs" value="1" rule="248" place="6" punct="pe">œu</seg>rs</rhyme></w> !</l>
		<l n="13" num="1.13" lm="6" met="6"><w n="13.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.2">l</w>'<w n="13.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="13.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="13.6" punct="pe:6">m<rhyme label="a" id="6" gender="m" type="a" stanza="5"><seg phoneme="œ" type="vs" value="1" rule="248" place="6" punct="pe">œu</seg>rs</rhyme></w> !</l>
		<l n="14" num="1.14" lm="6" met="6"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2" punct="vg:3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="490" place="3" punct="vg">i</seg>s</w>, <w n="14.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4" punct="vg:6">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>gr<rhyme label="a" id="7" gender="m" type="a" stanza="6"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg">i</seg>s</rhyme></w>,</l>
		<l n="15" num="1.15" lm="6" met="6"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="15.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="3">en</seg>s</w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="15.5">P<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r<rhyme label="a" id="7" gender="m" type="e" stanza="6"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>s</rhyme></w></l>
		<l n="16" num="1.16" lm="9" met="3+6"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="16.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="3" punct="vg" caesura="1">oi</seg>r</w>,<caesura></caesura> <w n="16.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="4" mp="C">u</seg></w> <w n="16.4" punct="vg:5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="5" punct="vg">oin</seg>s</w>, <w n="16.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6" mp="C">un</seg></w> <w n="16.6">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg></w> <w n="16.7" punct="pe:9">v<seg phoneme="ø" type="vs" value="1" rule="404" place="8" mp="M">eu</seg>v<rhyme label="a" id="8" gender="f" type="a" stanza="7"><seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="pe" mp="F">e</seg></rhyme></w> !</l>
		<l n="17" num="1.17" lm="6" met="6"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="17.3">qu</w>'<w n="17.4"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="17.5">s<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>t</w> <w n="17.6" punct="pe:6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>d<rhyme label="b" id="9" gender="m" type="a" stanza="7"><seg phoneme="y" type="vs" value="1" rule="449" place="6" punct="pe">u</seg></rhyme></w> !</l>
		<l n="18" num="1.18" lm="6" met="6"><w n="18.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r</w> <w n="18.3">m</w>'<w n="18.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="18.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="5">en</seg></w> <w n="18.6" punct="vg:6">d<rhyme label="b" id="9" gender="m" type="e" stanza="7"><seg phoneme="y" type="vs" value="1" rule="444" place="6" punct="vg">û</seg></rhyme></w>,</l>
		<l n="19" num="1.19" lm="9" met="3+6"><w n="19.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="19.2" punct="vg:3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="vg" caesura="1">a</seg></w>,<caesura></caesura> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg>s</w> <w n="19.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" mp="C">on</seg></w> <w n="19.5" punct="vg:9">m<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><rhyme label="a" id="8" gender="f" type="e" stanza="7"><seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="vg" mp="F">e</seg></rhyme></w>,</l>
		<l n="20" num="1.20" lm="6" met="6"><w n="20.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">s<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>l</w> <w n="20.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r</w> <w n="20.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">om</seg>pl<rhyme label="a" id="10" gender="m" type="a" stanza="8"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="6">e</seg>t</rhyme></w></l>
		<l n="21" num="1.21" lm="6" met="6"><w n="21.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">l</w>'<w n="21.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>bb<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg></w> <w n="21.4">m</w>'<w n="21.5"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="21.6" punct="pe:6">f<rhyme label="a" id="10" gender="m" type="e" stanza="8"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" punct="pe">ai</seg>t</rhyme></w> !</l>
	</lg>
</div></body></text></TEI>