<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB27" modus="cp" lm_max="10" metProfile="8, 4+6" form="strophe unique" schema="1(ababbcdcd)">
	<head type="tune">Air : Pour le trouver, je vais en Allemagne.</head>
	<lg n="1" type="neuvain" rhyme="ababbcdcd">
		<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="422" place="1" punct="vg">oi</seg></w>, <w n="1.2">l</w>'<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>r</w><caesura></caesura> <w n="1.4">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rt</w> <w n="1.5">p<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg></w> <w n="1.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.7">m</w>'<w n="1.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="8">en</seg></w> <w n="1.9" punct="pv:10"><seg phoneme="o" type="vs" value="1" rule="434" place="9" mp="M">o</seg>cc<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">C</w>'<w n="2.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>st</w> <w n="2.3">l</w>'<w n="2.4"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.6">n<seg phoneme="o" type="vs" value="1" rule="437" place="6">o</seg>s</w> <w n="2.7" punct="pt:8">n<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pt">eu</seg>x</rhyme></w>.</l>
		<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="3.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>gs</w> <w n="3.3">pr<seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="189" place="4" caesura="1">e</seg>ts</w><caesura></caesura> <w n="3.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="3.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="3.6">su<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg>s</w> <w n="3.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>s</w> <w n="3.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="3.9" punct="vg:10">d<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="4.3">m<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="4.5">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="4.7">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="4.8" punct="pv:8">v<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="247" place="8" punct="pv">œu</seg>x</rhyme></w> ;</l>
		<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">J<seg phoneme="wa" type="vs" value="1" rule="422" place="1">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="188" place="2">e</seg>t</w> <w n="5.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>rs</w><caesura></caesura> <w n="5.4">v<seg phoneme="wa" type="vs" value="1" rule="419" place="5" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="5.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="5.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="5.8" punct="pe:10">v<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="10" punct="pe">eu</seg>x</rhyme></w> !</l>
		<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="6.2">v<seg phoneme="i" type="vs" value="1" rule="481" place="2">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="P">à</seg></w> <w n="6.4">m<seg phoneme="wa" type="vs" value="1" rule="422" place="4" caesura="1">oi</seg></w><caesura></caesura> <w n="6.5">n</w>'<w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="6.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.8">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.9"><seg phoneme="e" type="vs" value="1" rule="188" place="8">e</seg>t</w> <w n="6.10" punct="vg:10">l<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>g<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
		<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" punct="vg" caesura="1">in</seg></w>,<caesura></caesura> <w n="7.4">s<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg></w> <w n="7.5">j<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>s</w> <w n="7.6">j</w>'<w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg></w> <w n="7.8" punct="vg:10">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg>s<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="vg">é</seg></rhyme></w>,</l>
		<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="P">À</seg></w> <w n="8.2">l</w>'<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>r</w><caesura></caesura> <w n="8.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>d</w> <w n="8.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" mp="C">on</seg></w> <w n="8.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7" mp="M">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>t</w> <w n="8.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="C">on</seg></w> <w n="8.8" punct="vg:10">v<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
		<l n="9" num="1.9" lm="8" met="8"><w n="9.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="188" place="1" punct="vg">E</seg>t</w>, <w n="9.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>d</w> <w n="9.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="9.5" punct="vg:5">v<seg phoneme="i" type="vs" value="1" rule="467" place="5" punct="vg">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg></w> <w n="9.7" punct="pe:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>ss<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pe">é</seg></rhyme></w> !</l>
	</lg>
</div></body></text></TEI>