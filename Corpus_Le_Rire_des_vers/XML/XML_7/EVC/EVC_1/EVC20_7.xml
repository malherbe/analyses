<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE V.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC20" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="2(abab)">
					<head type="tune">AIR de Marie.</head>
						<lg n="1" type="quatrain" rhyme="abab">
							<head type="main">CLINTON, LADY, JOB.</head>
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="1.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="1.5" punct="vg:8">pr<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>d<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
							<l n="2" num="1.2" lm="8" met="8"><w n="2.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="2.2">c<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w>-<w n="2.3">lu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="2.4">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.5" punct="pt:8">d<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>l<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pt">eu</seg>r</rhyme></w>.</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>ll<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <del hand="RR" reason="analysis" type="alternative">Venez </del><w n="3.2">pu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.3">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.4">pr<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>s<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>t</w> <w n="4.3">pl<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>s</w> <w n="4.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="4">en</seg></w> <w n="4.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="4.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="4.7" punct="pt:8">b<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>nh<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pt">eu</seg>r</rhyme></w>.</l>
						</lg>
						<lg n="2" type="quatrain" rhyme="abab">
							<head type="main">ANNA.</head>
							<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="5.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="5.5" punct="vg:8">pr<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>d<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
							<l n="6" num="2.2" lm="8" met="8"><w n="6.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="6.2">c<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ch<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w>-<w n="6.3">lu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="6.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.5" punct="pt:8">d<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>l<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pt">eu</seg>r</rhyme></w>.</l>
							<l n="7" num="2.3" lm="8" met="8"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="7.3" punct="vg:3">su<seg phoneme="i" type="vs" value="1" rule="490" place="3" punct="vg">i</seg>s</w>, <w n="7.4">pu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="7.6">pr<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>s<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
							<l n="8" num="2.4" lm="8" met="8"><w n="8.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>t</w> <w n="8.3">pl<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>s</w> <w n="8.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="4">en</seg></w> <w n="8.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="8.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="8.7" punct="pt:8">b<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>nh<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pt">eu</seg>r</rhyme></w>.</l>
						</lg>
					</div></body></text></TEI>