<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE XI.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC27" modus="cm" lm_max="10" metProfile="4+6" form="suite de strophes" schema="1[ababb] 1[abbacac]">
					<head type="tune">AIR : A soixante ans on ne doit pas remettre.</head>
						<lg n="1" type="regexp" rhyme="ababb">
							<head type="main">LA LIBERTÉ.</head>
							<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="1.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="1.3" punct="ps:4">d<seg phoneme="ø" type="vs" value="1" rule="397" place="4" punct="ps" caesura="1">eu</seg>x</w>…<caesura></caesura> <w n="1.4" punct="pe:5">di<seg phoneme="ø" type="vs" value="1" rule="397" place="5" punct="pe">eu</seg>x</w> ! <w n="1.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="1.6" punct="pe:10">d<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>r<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></w> !</l>
							<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="2.2">d<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="2.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="2.4">f<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" mp="F">e</seg>nt</w> <w n="2.5">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="2.6" punct="pv:10"><seg phoneme="e" type="vs" value="1" rule="169" place="8" mp="M">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pv">i</seg>s</rhyme></w> ;</l>
							<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="3.2">l</w>'<w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4" caesura="1">o</seg>r</w><caesura></caesura> <w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="3.6">l</w>'<w n="3.7"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" mp="M">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="343" place="7">ue</seg>il</w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="3.9">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="3.10" punct="vg:10">Fr<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">L</w>'<w n="4.2"><seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="4.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="4.6">h<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" mp="M">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r</w> <w n="4.7"><seg phoneme="o" type="vs" value="1" rule="317" place="8" mp="C">au</seg></w> <w n="4.8" punct="pt:10">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="9" mp="M">a</seg><rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="320" place="10" punct="pt">y</seg>s</rhyme></w>.<del type="repetition" hand="LG" reason="analysis">(bis)</del></l>
							<l n="5" num="1.5" lm="10" met="4+6"><subst type="repetition" hand="LG" reason="analysis"><del></del><add rend="hidden"><w n="5.1">L</w>'<w n="5.2"><seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="5.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="5.6">h<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" mp="M">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r</w> <w n="5.7"><seg phoneme="o" type="vs" value="1" rule="317" place="8" mp="C">au</seg></w> <w n="5.8" punct="pt:10">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="9" mp="M">a</seg><rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="320" place="10" punct="pt">y</seg>s</rhyme></w>.</add></subst></l>
						</lg>
						<lg n="2" type="regexp" rhyme="abb">
							<head type="main">LA VALEUR .</head>
							<l n="6" num="2.1" lm="10" met="4+6"><w n="6.1">Gu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1" mp="M">e</seg>rri<seg phoneme="e" type="vs" value="1" rule="346" place="2">er</seg></w> <w n="6.2" punct="vg:4"><seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="449" place="4" punct="vg" caesura="1">u</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="C">au</seg>x</w> <w n="6.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">am</seg>ps</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="6.7" punct="vg:10">v<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>ct<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="7" num="2.2" lm="10" met="4+6"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3" mp="M">ai</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="M">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg></w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="7.5" punct="pv:10">h<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>r<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="o" type="vs" value="1" rule="437" place="10" punct="pv">o</seg>s</rhyme></w> ;</l>
							<l n="8" num="2.3" lm="10" met="4+6"><w n="8.1">Lu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="8.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="P">ou</seg>r</w> <w n="8.3">c<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="8.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="C">i</seg>l</w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t</w> <w n="8.6">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="8.7" punct="ps:10">b<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>rr<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="o" type="vs" value="1" rule="314" place="10" punct="ps">eau</seg>x</rhyme></w>…</l>
						</lg>
						<div n="1" type="section">
							<head type="main">Bis en chœur {</head>
							<lg n="1" type="regexp" rhyme="acac">
								<l n="9" num="1.1" lm="10" met="4+6"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="9.2">l<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="9.3">p<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>t</w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7" mp="M">o</seg>pt<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w> <w n="9.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="9.6" punct="vg:10">gl<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
								<l n="10" num="1.2" lm="10" met="4+6"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="10.2">l<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="10.3">m<seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="M">au</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t</w> <w n="10.4">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="10.5" punct="pe:10"><seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>f<rhyme label="c" id="5" gender="m" type="a" stanza="2"><seg phoneme="o" type="vs" value="1" rule="317" place="10" punct="pe">au</seg>ds</rhyme></w> !</l>
								<l n="11" num="1.3" lm="10" met="4+6"><subst type="repetition" hand="LG" reason="analysis"><del></del><add rend="hidden"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="11.2">l<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="11.3">p<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>t</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7" mp="M">o</seg>pt<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w> <w n="11.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="11.6" punct="vg:10">gl<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</add></subst></l>
								<l n="12" num="1.4" lm="10" met="4+6"><subst type="repetition" hand="LG" reason="analysis"><del> </del><add rend="hidden"><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="12.2">l<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="12.3">m<seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="M">au</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t</w> <w n="12.4">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="12.5" punct="pe:10"><seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>f<rhyme label="c" id="5" gender="m" type="e" stanza="2"><seg phoneme="o" type="vs" value="1" rule="317" place="10" punct="pe">au</seg>ds</rhyme></w> !</add></subst></l>
							</lg>
						</div>
					</div></body></text></TEI>