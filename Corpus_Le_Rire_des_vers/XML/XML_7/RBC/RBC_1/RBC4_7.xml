<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE II.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC4" modus="sm" lm_max="6" metProfile="6" form="suite de strophes" schema="2[abab] 1[aa]">
			<head type="tune">AIR : Ça nʼdurʼra pas toujours.</head>
				<lg n="1" type="regexp" rhyme="abab">
					<head type="main">CHARLES.</head>
					<l n="1" num="1.1" lm="6" met="6"><w n="1.1">C</w>'<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="1.3"><seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.4">ch<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">in</seg>f<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="6" met="6"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381" place="4">e</seg>ils</w> <w n="2.4" punct="pe:6">d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>sc<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="pe">ou</seg>rs</rhyme></w> !</l>
					<l n="3" num="1.3" lm="6" met="6"><w n="3.1" punct="vg:2"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg></w>, <w n="3.2">gr<seg phoneme="a" type="vs" value="1" rule="339" place="3">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="3.4" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>d<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="6" met="6"><w n="4.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="4.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="4.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</w> <w n="4.4" punct="ps:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>c<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="ps">ou</seg>rs</rhyme></w>…</l>
				</lg>
				<lg n="2" type="regexp" rhyme="a">
					<head type="main">LA LIBERTÉ, dans la coulisse.</head>
					<l n="5" num="2.1" lm="6" met="6"><w n="5.1">Ç<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="5.2">nʼ</w> <w n="5.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>rʼr<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="5.5" punct="pt:6">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>j<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="pt">ou</seg>rs</rhyme></w>.</l>
				</lg>
				<lg n="3" type="regexp" rhyme="baba">
					<head type="main">CHARLES.</head>
					<l n="6" num="3.1" lm="6" met="6"><w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>rs</w> <w n="6.4" punct="vg:6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>gu<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>nt</rhyme></w>,</l>
					<l n="7" num="3.2" lm="6" met="6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4" punct="vg:6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>c<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="vg">ou</seg>rs</rhyme></w>,</l>
					<l n="8" num="3.3" lm="6" met="6"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="8.2">th<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg><seg phoneme="a" type="vs" value="1" rule="339" place="3">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="8.3" punct="pv:6">p<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>r<rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg>nt</rhyme></w> ;</l>
					<l n="9" num="3.4" lm="6" met="6"><w n="9.1" punct="vg:1">Br<seg phoneme="ɛ" type="vs" value="1" rule="345" place="1" punct="vg">e</seg>f</w>, <w n="9.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>t</w> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="9.5" punct="ps:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>b<rhyme label="a" id="5" gender="m" type="a" stanza="3"><seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="ps">ou</seg>rs</rhyme></w>…</l>
				</lg>
				<lg n="4" type="regexp" rhyme="a">
					<head type="main">LA LIBERTÉ.</head>
					<l n="10" num="4.1" lm="6" met="6"><w n="10.1">Ç<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="10.2">nʼ</w> <w n="10.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>rʼr<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="10.5" punct="pt:6">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>j<rhyme label="a" id="5" gender="m" type="e" stanza="3"><seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="pt">ou</seg>rs</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>