<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE II.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC2" modus="cp" lm_max="10" metProfile="6, (4+6), 4=5" form="suite de strophes" schema="2[abab]">
				<head type="tune">AIR : Rendez-moi mon écuelle de bois.</head>
				<lg n="1" type="regexp" rhyme="ab">
					<head type="main">CHARLES.</head>
					<l n="1" num="1.1" lm="10" met="4+6" met_alone="True"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="1.3" punct="vg:4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="M">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="vg" caesura="1">ai</seg>s</w>,<caesura></caesura> <w n="1.4">qu</w>'<w n="1.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="1.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="1.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="8">en</seg></w> <w n="1.9">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="1.10">j<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>r</rhyme></w></l>
					<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="8"></space><w n="2.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="2.2"><seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>vr<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3" punct="pt:6">s<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>bl<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="466" place="6">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="regexp" rhyme="ab">
					<head type="main">NOEL.</head>
					<l n="3" num="2.1" lm="9" met="5+4" mp4="C"><space unit="char" quantity="2"></space><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="3.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="422" place="2" punct="vg">oi</seg></w>, <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="P">ou</seg>r</w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="C">a</seg></w> <w n="3.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="5" caesura="1">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w><caesura></caesura> <w n="3.6" punct="pt:9">V<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="7" mp="M">en</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>d<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="u" type="vs" value="1" rule="424" place="9" punct="pt">ou</seg>r</rhyme></w>.</l>
					<l n="4" num="2.2" lm="6" met="6"><space unit="char" quantity="8"></space><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="4.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="4.4" punct="pt:6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>m<rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="466" place="6">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" type="regexp" rhyme="ab">
					<head type="main">CHARLES.</head>
					<l n="5" num="3.1" lm="9" met="5+4" mp4="M"><space unit="char" quantity="2"></space><w n="5.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="422" place="1" punct="vg">oi</seg></w>, <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="C">a</seg></w> <w n="5.3">tr<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>g<seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg>d<seg phoneme="i" type="vs" value="1" rule="481" place="5" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="341" place="6" mp="P">à</seg></w> <w n="5.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d</w> <w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>ff<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="9">e</seg>t</rhyme></w></l>
					<l n="6" num="3.2" lm="9" met="4−5" mp4="P" mp5="M/mc"><space unit="char" quantity="2"></space><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">l</w>'<w n="6.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="6.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="P" caesura="1">à</seg></w><caesura></caesura> <w n="6.6">l</w>'<w n="6.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="M/mc">Am</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="M/mc">i</seg>g<seg phoneme="y" type="vs" value="1" rule="447" place="7" mp="Lc">u</seg></w>-<w n="6.8" punct="pt:9">C<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M/mc">o</seg>m<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="4" type="regexp" rhyme="ab">
					<head type="main">NOEL.</head>
					<l n="7" num="4.1" lm="9" met="5+4" mp4="Mem"><space unit="char" quantity="2"></space><w n="7.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="422" place="1" punct="vg">oi</seg></w>, <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3">v<seg phoneme="o" type="vs" value="1" rule="317" place="3" mp="M">au</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="5" caesura="1">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w><caesura></caesura> <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.5">j</w>'<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg></w> <w n="7.7">f<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="9">ai</seg>t</rhyme></w></l>
					<l n="8" num="4.2" lm="6" met="6"><space unit="char" quantity="8"></space><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">C<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>rqu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>-<w n="8.4" punct="pt:6"><seg phoneme="o" type="vs" value="1" rule="443" place="4">O</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="493" place="5">ym</seg>p<rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>