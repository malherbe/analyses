<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE VII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC20" modus="cm" lm_max="10" metProfile="4+6" form="suite de strophes" schema="7[abab]">
					<head type="tune">AIR : Walse des Comédiens (de Miller).</head>
						<lg n="1" type="regexp" rhyme="abababababababababababababab">
							<head type="main">MUSICO</head>
							<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="1.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="1.3" punct="vg:4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="vg" caesura="1">an</seg>guʼ</w>,<caesura></caesura> <w n="1.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="1.5">dʼvi<seg phoneme="ɑ̃" type="vs" value="1" rule="377" place="6" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="1.6" punct="vg:10">g<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>r<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="C">ou</seg>s</w> <w n="2.2">n</w>'<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="2.5"><seg phoneme="i" type="vs" value="1" rule="466" place="5" mp="M">i</seg>m<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>g<seg phoneme="i" type="vs" value="1" rule="466" place="7" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></w> <w n="2.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="2.7" punct="pv:10">m<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="o" type="vs" value="1" rule="437" place="10" punct="pv">o</seg>ts</rhyme></w> ;</l>
							<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">on</seg></w> <w n="3.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="63" place="2">e</seg>r</w> <w n="3.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="3.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="3.6">m<seg phoneme="y" type="vs" value="1" rule="449" place="8" mp="M">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>c<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
							<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">D<seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2" mp="M">e</seg>ct<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="4.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="C">ou</seg>s</w> <w n="4.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="6">en</seg>t</w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="4.5" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="466" place="9" mp="M">i</seg>m<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="o" type="vs" value="1" rule="317" place="10" punct="pt">au</seg>x</rhyme></w>.</l>
							<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3" punct="vg:4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" punct="vg" caesura="1">in</seg></w>,<caesura></caesura> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="M">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t</w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="5.6" punct="vg:10">c<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="M">am</seg>p<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">F<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>t</w> <hi rend="ital"><w n="6.2" punct="pe:2">pi<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="pe">ou</seg></w> ! <w n="6.3" punct="pe:3">pi<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="pe">ou</seg></w> ! <w n="6.4" punct="pe:4">pi<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="pe" caesura="1">ou</seg></w> !<caesura></caesura></hi> <w n="6.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5" mp="P">an</seg>s</w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="6.7">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="6.8" punct="vg:10">s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9" mp="M">ai</seg>s<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" punct="vg">on</seg></rhyme></w>,</l>
							<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">S<seg phoneme="wa" type="vs" value="1" rule="439" place="1" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="7.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" caesura="1">ain</seg></w><caesura></caesura> <w n="7.3">qu</w>'<w n="7.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="7.5">d<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t</w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="P">à</seg></w> <w n="7.7">s<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="7.8" punct="dp:10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="M">om</seg>p<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></w> :</l>
							<l n="8" num="1.8" lm="10" met="4+6"><hi rend="ital"><w n="8.1" punct="pt:10">B<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="8.2">pʼt<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="Lc">i</seg>t</w>-<w n="8.3" punct="vg:4">f<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg" caesura="1">i</seg>ls</w>,<caesura></caesura> <w n="8.4">b<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="8.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>t</w> <w n="8.6">m<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>gn<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" punct="pt">on</seg></rhyme></w></hi>.</l>
							<l n="9" num="1.9" lm="10" met="4+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="9.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>d</w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.4" punct="vg:4">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="4" punct="vg" caesura="1">en</seg></w>,<caesura></caesura> <w n="9.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="P">ou</seg>r</w> <w n="9.6">r<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="9.7">l</w>'<w n="9.8" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>l<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="10" num="1.10" lm="10" met="4+6"><w n="10.1">F<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>t</w> <hi rend="ital"><w n="10.2" punct="pe:2">h<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="pe">ou</seg></w> ! <w n="10.3" punct="pe:3">h<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="pe">ou</seg></w> ! <w n="10.4" punct="pe:4">h<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="pe" caesura="1">ou</seg></w> !<caesura></caesura></hi> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="10.6">s</w>'<w n="10.7"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6" mp="M">e</seg>xpr<seg phoneme="i" type="vs" value="1" rule="466" place="7" mp="M">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>t</w> <w n="10.8" punct="vg:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="9" mp="M">ain</seg>s<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="vg">i</seg></rhyme></w>,</l>
							<l n="11" num="1.11" lm="10" met="4+6"><w n="11.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="11.2">cl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2" mp="M">ai</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.4">nʼ</w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="11.6">fʼr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t</w> <w n="11.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="8" mp="C">un</seg></w> <w n="11.8" punct="vg:10">g<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg>d<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="12" num="1.12" lm="10" met="4+6"><w n="12.1">C</w>'<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="12.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="2">en</seg></w> <w n="12.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="C">ou</seg>s</w> <w n="12.5" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="dp" caesura="1">i</seg>rʼ</w> :<caesura></caesura> <hi rend="ital"><w n="12.6" punct="pt:10">Y</w> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="12.8">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6" mp="C">e</seg>s</w> <w n="12.9">v<seg phoneme="o" type="vs" value="1" rule="443" place="7" mp="M">o</seg>l<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>rs</w> <w n="12.10"><seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>c<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pt">i</seg></rhyme></w></hi>.</l>
							<l n="13" num="1.13" lm="10" met="4+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="13.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>d</w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="C">a</seg></w> <w n="13.4" punct="vg:4">nu<seg phoneme="i" type="vs" value="1" rule="490" place="4" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="13.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="13.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>g</w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="13.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="13.9" punct="vg:10">g<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>ti<rhyme label="a" id="7" gender="f" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="14" num="1.14" lm="10" met="4+6"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="14.2">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>t</w> <w n="14.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" mp="M">en</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>blʼ</w><caesura></caesura> <w n="14.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <hi rend="ital"><w n="14.5" punct="pe:7">mi<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>h<seg phoneme="o" type="vs" value="1" rule="317" place="7" punct="pe">au</seg>t</w> ! <w n="14.6" punct="pe:10">m<seg phoneme="i" type="vs" value="1" rule="RBC20_3" place="8" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>h<rhyme label="b" id="8" gender="m" type="a" stanza="4"><seg phoneme="o" type="vs" value="1" rule="317" place="10" punct="pe">au</seg>t</rhyme></w> !</hi></l>
							<l n="15" num="1.15" lm="10" met="4+6"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="15.3">pr<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="15.4">qu</w>'<w n="15.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="P">à</seg></w> <w n="15.6">s<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="15.7">j<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>nʼ</w> <w n="15.8" punct="vg:10">m<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>g<rhyme label="a" id="7" gender="f" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="16" num="1.16" lm="10" met="4+6"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="16.2">d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t</w> <w n="16.3" punct="dp:4"><seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4" punct="dp" caesura="1">o</seg>rs</w> :<caesura></caesura> <hi rend="ital"><w n="16.4" punct="pi:10">V<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="Lp">a</seg>s</w>-<w n="16.5">t<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="16.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w> <w n="16.7">l<seg phoneme="a" type="vs" value="1" rule="341" place="9">à</seg></w> <w n="16.8">h<rhyme label="b" id="8" gender="m" type="e" stanza="4"><seg phoneme="o" type="vs" value="1" rule="317" place="10" punct="pi">au</seg>t</rhyme></w></hi> ?</l>
							<l n="17" num="1.17" lm="10" met="4+6"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="17.2">gl<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4" caesura="1">ai</seg></w><caesura></caesura> <w n="17.3">s<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="P">u</seg>r</w> <w n="17.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6" mp="C">e</seg>s</w> <w n="17.5">b<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="17.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="17.7" punct="vg:10">s<rhyme label="a" id="9" gender="f" type="a" stanza="5"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="10">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
							<l n="18" num="1.18" lm="10" met="4+6"><w n="18.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="P">u</seg>r</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="18.3">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rr<seg phoneme="y" type="vs" value="1" rule="449" place="4" caesura="1">u</seg>chʼs</w><caesura></caesura> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="18.5">s<seg phoneme="y" type="vs" value="1" rule="449" place="6" mp="P">u</seg>r</w> <w n="18.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="18.7" punct="pv:10">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>rr<seg phoneme="ɔ" type="vs" value="1" rule="442" place="9" mp="M">o</seg>qu<rhyme label="b" id="10" gender="m" type="a" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="10" punct="pv">e</seg>ts</rhyme></w> ;</l>
							<l n="19" num="1.19" lm="10" met="4+6"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="19.2">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2" mp="M">e</seg>rr<seg phoneme="ɔ" type="vs" value="1" rule="442" place="3" mp="M">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="189" place="4" caesura="1">e</seg>ts</w><caesura></caesura> <w n="19.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" mp="F">e</seg>nt</w> <w n="19.4">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="19.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="19.6" punct="vg:10">h<rhyme label="a" id="9" gender="f" type="e" stanza="5"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="10">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
							<l n="20" num="1.20" lm="10" met="4+6"><w n="20.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="20.2">dʼ</w> <w n="20.3">l<seg phoneme="œ" type="vs" value="1" rule="406" place="2" mp="C">eu</seg>r</w> <w n="20.4">c<seg phoneme="o" type="vs" value="1" rule="414" place="3" mp="M">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="20.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rlʼnt</w> <w n="20.6">c<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>mmʼ</w> <w n="20.7">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="20.8" punct="pt:10">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>rr<seg phoneme="ɔ" type="vs" value="1" rule="442" place="9" mp="M">o</seg>qu<rhyme label="b" id="10" gender="m" type="e" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="10" punct="pt">e</seg>ts</rhyme></w>.</l>
							<l n="21" num="1.21" lm="10" met="4+6"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="21.2">l<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="21.3">l</w>'<w n="21.4" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">â</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="21.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="21.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="21.7">m<seg phoneme="e" type="vs" value="1" rule="352" place="7" mp="M">e</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>t</w> <w n="21.8"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="P">à</seg></w> <w n="21.9" punct="vg:10">br<rhyme label="a" id="11" gender="f" type="a" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="22" num="1.22" lm="10" met="4+6"><w n="22.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="P">u</seg>r</w> <w n="22.2">lʼ</w> <w n="22.3">p<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="22.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="22.5"><seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">A</seg>rts</w><caesura></caesura> <w n="22.6">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="22.7">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6" mp="C">e</seg>s</w> <hi rend="ital"><w n="22.8" punct="pe:8">h<seg phoneme="i" type="vs" value="1" rule="dc-1" place="7" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="pe">an</seg>s</w> ! <w n="22.9" punct="pe:10">h<seg phoneme="i" type="vs" value="1" rule="dc-1" place="9" mp="M">i</seg><rhyme label="b" id="12" gender="m" type="a" stanza="6"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="10" punct="pe">an</seg>s</rhyme></w> !</hi></l>
							<l n="23" num="1.23" lm="10" met="4+6"><w n="23.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="23.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>t</w> <w n="23.3" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="dp" caesura="1">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> :<caesura></caesura> <w n="23.4" punct="vg:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">En</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="7" punct="vg">é</seg></w>, <w n="23.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="63" place="8">e</seg>r</w> <w n="23.6" punct="vg:10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="M">on</seg>fr<rhyme label="a" id="11" gender="f" type="e" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="24" num="1.24" lm="10" met="4+6"><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="24.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="24.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="M">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="24.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="24.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="24.6">n<seg phoneme="o" type="vs" value="1" rule="437" place="8" mp="C">o</seg>s</w> <w n="24.7" punct="pt:10">s<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>v<rhyme label="b" id="12" gender="m" type="e" stanza="6"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="10" punct="pt">an</seg>s</rhyme></w>.</l>
							<l n="25" num="1.25" lm="10" met="4+6"><w n="25.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="25.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>d</w> <w n="25.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="25.4" punct="vg:4">c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4" punct="vg" caesura="1">o</seg>q</w>,<caesura></caesura> <w n="25.5">d<seg phoneme="ɛ" type="vs" value="1" rule="409" place="5" mp="P">è</seg>s</w> <w n="25.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="25.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="25.8">j<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>r</w> <w n="25.9" punct="vg:10">c<seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>mm<rhyme label="a" id="13" gender="f" type="a" stanza="7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="26" num="1.26" lm="10" met="4+6"><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="26.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="2">ein</seg></w> <w n="26.3">g<seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>si<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="26.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <hi rend="ital"><w n="26.5" punct="pe:10">c<seg phoneme="o" type="vs" value="1" rule="443" place="7" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>c<seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>c<rhyme label="b" id="14" gender="m" type="a" stanza="7"><seg phoneme="o" type="vs" value="1" rule="443" place="10" punct="pe">o</seg></rhyme></w></hi> !</l>
							<l n="27" num="1.27" lm="10" met="4+6"><w n="27.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="27.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>t</w> <w n="27.3" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="dp" caesura="1">i</seg>rʼ</w> :<caesura></caesura> <hi rend="ital"><w n="27.4">Lʼv<seg phoneme="e" type="vs" value="1" rule="346" place="5" mp="Lp">ez</seg></w>-<w n="27.5" punct="vg:6">v<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="vg">ou</seg>s</w>, <w n="27.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8">an</seg>s</w> <w n="27.7">dʼ</w> <w n="27.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="27.9" punct="vg:10">Fr<rhyme label="a" id="13" gender="f" type="e" stanza="7"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</hi></l>
							<l n="28" num="1.28" lm="10" met="4+6"><hi rend="ital"><w n="28.1" punct="pe:10"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="28.2">s<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="M/mp">ou</seg>vʼn<seg phoneme="e" type="vs" value="1" rule="346" place="3" mp="Lp">ez</seg></w>-<w n="28.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>s</w><caesura></caesura> <w n="28.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="28.5">j<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>rs</w> <w n="28.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="28.7">M<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg>g<rhyme label="b" id="14" gender="m" type="e" stanza="7"><seg phoneme="o" type="vs" value="1" rule="443" place="10" punct="pe">o</seg></rhyme></w></hi> !</l>
						</lg>
					</div></body></text></TEI>