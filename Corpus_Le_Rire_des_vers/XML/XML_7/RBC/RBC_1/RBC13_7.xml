<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE IV.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC13" modus="cp" lm_max="10" metProfile="8, 4+6" form="strophe unique" schema="1(ababbcdcd)">
				<head type="tune">AIR : Vaudeville de Partie et Revanche</head>
				<lg n="1" type="neuvain" rhyme="ababbcdcd">
					<head type="main">COURBETTE.</head>
					<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="1.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rsqu</w>'<w n="1.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="1.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>d</w> <w n="1.5"><seg phoneme="y" type="vs" value="1" rule="452" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="1.6" punct="vg:8">f<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="192" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">on</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="2.3">d<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>t</w><caesura></caesura> <w n="2.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="M">u</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="2.5">l</w>'<w n="2.6" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="vg">é</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="3.1">S</w>'<w n="3.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>l</w> <w n="3.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="2">en</seg>t</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="3.5">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rdrʼ</w> <w n="3.6">l</w>'<w n="3.7"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>bj<seg phoneme="ɛ" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="3.8">dʼ</w> <w n="3.9">s<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="3.10" punct="vg:8">fl<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">É</seg>pr<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>s</w> <w n="4.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="3" mp="M">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="4" caesura="1">ô</seg>t</w><caesura></caesura> <w n="4.3">d</w>'<w n="4.4"><seg phoneme="y" type="vs" value="1" rule="462" place="5">u</seg>nʼ</w> <w n="4.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="4.6" punct="vg:10">b<seg phoneme="o" type="vs" value="1" rule="314" place="9" mp="M">eau</seg>t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="vg">é</seg></rhyme></w>,</l>
					<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="5.2">lu<seg phoneme="i" type="vs" value="1" rule="490" place="2" mp="C">i</seg></w> <w n="5.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="M">an</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4" caesura="1">o</seg>rtʼ</w><caesura></caesura> <w n="5.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>tʼ</w> <w n="5.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="5.6" punct="pt:10">f<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>d<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="pt">é</seg></rhyme></w>.</l>
					<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">f<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s</w> <w n="6.3">d</w>'<w n="6.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4" caesura="1">o</seg>rd</w><caesura></caesura> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="M">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>x</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="6.7">l</w>'<w n="6.8" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">em</seg>p<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">Pu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg>s</w> <w n="7.2">j</w>'<w n="7.3"><seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4" caesura="1">ai</seg></w><caesura></caesura> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="7.5" punct="vg:10">r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6" mp="M">e</seg>st<seg phoneme="o" type="vs" value="1" rule="317" place="7" mp="M">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" punct="vg">on</seg></rhyme></w>,</l>
					<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="8.3" punct="ps:4">v<seg phoneme="œ" type="vs" value="1" rule="406" place="4" punct="ps" caesura="1">eu</seg>f</w>…<caesura></caesura> <w n="8.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="C">on</seg></w> <w n="8.5">nʼ</w> <w n="8.6">p<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>t</w> <w n="8.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>s</w> <w n="8.8">m</w>'<w n="8.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" mp="M">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>rd<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
					<l n="9" num="1.9" lm="10" met="4+6"><w n="9.1">Lʼ</w> <w n="9.2">dr<seg phoneme="wa" type="vs" value="1" rule="419" place="1">oi</seg>t</w> <w n="9.3">dʼ</w> <w n="9.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>rʼ</w> <w n="9.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="C">a</seg></w> <w n="9.6">c<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>r</w><caesura></caesura> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="P">à</seg></w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="9.9" punct="pt:10">r<seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>v<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg>l<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="M">u</seg>ti<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" punct="pt">on</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>