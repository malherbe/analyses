<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHABERT</title>
				<title type="sub">HISTOIRE CONTEMPORAINE EN DEUX ACTES, MÊLÉE DE CHANT</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="ARJ" sort="1">
					<name>
						<forename>Jacques</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1790" to="1855">1790-1855</date>
				</author>
				<author key="LUR" sort="2">
					<name>
						<forename>Louis</forename>
						<surname>LURINE</surname>
					</name>
					<date from="1810" to="1860">1810-1860</date>
				</author>
				<editor>Le rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>110 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">JRL_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
				   </p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CHABERT</title>
						<author>JACQUES ARAGO ET LOUIS LURINE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=vldoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849877.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">2 JUILLET 1832</date>
				<placeName>
					<settlement>THÉÂTRE NATIONAL DU VAUDEVILLE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="JRL7" modus="cm" lm_max="10" metProfile="4+6" form="strophe unique" schema="1(ababcdcd)">
	<head type="tune">AIR : De votre bonté généreuse (de Fanchon)</head>
	<lg n="1" type="huitain" rhyme="ababcdcd">
		<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="C">ou</seg>s</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3" punct="vg:4">v<seg phoneme="wa" type="vs" value="1" rule="439" place="3" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="vg" caesura="1">ez</seg></w>,<caesura></caesura> <w n="1.4" punct="vg:7">m<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="vg" mp="F">e</seg></w>, <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="1.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9">em</seg>ps</w> <w n="1.7" punct="pv:10">pr<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="351" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
		<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1" mp="C">Un</seg></w> <w n="2.2">vi<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="2.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3" mp="M">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>t</w><caesura></caesura> <w n="2.4">g<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t</w> <w n="2.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="2.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="8">oin</seg></w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="2.8" punct="pt:10">n<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pt">ou</seg>s</rhyme></w>.</l>
		<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">D<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3" mp="C">un</seg></w> <w n="3.3" punct="vg:4">m<seg phoneme="o" type="vs" value="1" rule="437" place="4" punct="vg" caesura="1">o</seg>t</w>,<caesura></caesura> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5" mp="C">un</seg></w> <w n="3.5">s<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>l</w> <w n="3.6">m<seg phoneme="o" type="vs" value="1" rule="437" place="7">o</seg>t</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="3.8" punct="vg:10">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg>dr<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="351" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
		<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2" mp="P">an</seg>s</w> <w n="4.3">l</w>'<w n="4.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3" mp="M">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="4.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="C">i</seg>l</w> <w n="4.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="P">à</seg></w> <w n="4.8">v<seg phoneme="o" type="vs" value="1" rule="437" place="8" mp="C">o</seg>s</w> <w n="4.9" punct="pt:10">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pt">ou</seg>x</rhyme></w>.</l>
		<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="M/mp">on</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="2" mp="Lp">ez</seg></w>-<w n="5.2"><seg phoneme="i" type="vs" value="1" rule="496" place="3">y</seg></w> <w n="5.3" punct="vg:4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4" punct="vg" caesura="1">en</seg></w>,<caesura></caesura> <w n="5.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="C">i</seg>l</w> <w n="5.5">s</w>'<w n="5.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t</w> <w n="5.7">d</w>'<w n="5.8"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="8">un</seg></w> <w n="5.9">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>d</w> <w n="5.10" punct="vg:10">cr<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="466" place="10">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
		<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="6.2">s<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="C">a</seg></w> <w n="6.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" mp="M">en</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="310" place="4" caesura="1">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="6.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="6.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="7">en</seg>t</w> <w n="6.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="C">ou</seg>s</w> <w n="6.7"><seg phoneme="o" type="vs" value="1" rule="434" place="9" mp="M">o</seg>ffr<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>r</rhyme></w></l>
		<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg></w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg" caesura="1">on</seg></w>,<caesura></caesura> <w n="7.4">c<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r</w> <w n="7.5"><seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="C">i</seg>l</w> <w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="7.7" punct="vg:10">m<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>gn<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>n<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="466" place="10">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
		<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3" punct="vg:4">m<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="8.4">c<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r</w> <w n="8.5"><seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="C">i</seg>l</w> <w n="8.6">p<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>t</w> <w n="8.7">v<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="C">ou</seg>s</w> <w n="8.8" punct="pt:10">fl<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>tr<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pt">i</seg>r</rhyme></w>.</l>
	</lg>
</div></body></text></TEI>