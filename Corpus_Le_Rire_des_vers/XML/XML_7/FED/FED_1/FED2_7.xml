<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANDRÉ LE CHANSONNIER</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="FTN" sort="1">
					<name>
						<forename>Louis Marie</forename>
						<surname>FONTAN</surname>
					</name>
					<date from="1801" to="1839">1801-1839</date>
				</author>
				<author key="DNY" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>DESNOYER</surname>
					</name>
					<date from="1806" to="1858">1806-1858</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>239 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>André le chansonnier.</title>
						<author>FONTAN ET DESNOYER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=aN6oyNpF3cMC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>André le chansonnier.</title>
								<author>FONTAN ET DESNOYER</author>
								<repository>Biblioteca Casanatense</repository>
								<idno type="URI">http://opac.casanatense.it/Record.htm?idlist=3</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Ode et Wodon</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-15" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE I</head><head type="main_subpart">SCÈNE III</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FED2" modus="sm" lm_max="6" metProfile="6" form="suite périodique" schema="3(abba)">
				<head type="tune">(Reprenant l'air :)</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<head type="main">LOUISE.</head>
						<l n="1" num="1.1" lm="6" met="6"><w n="1.1">C<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="1.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>s</w> <w n="1.3">m</w>'<w n="1.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>t<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="vg">er</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="6" met="6"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">s<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="4">en</seg>s</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="2.5" punct="vg:6">p<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="384" place="6">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="6" met="6"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>ds</w> <w n="3.4" punct="vg:6">h<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>l<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="384" place="6">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="6" met="6"><w n="4.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg>s</w>, <w n="4.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="4.3" punct="dp:6">r<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>p<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>t<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="dp">er</seg></rhyme></w> :</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="6" met="6"><w n="5.1" punct="pe:2">S<seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="2" punct="pe">é</seg></w> ! <w n="5.2" punct="pe:4">s<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="pe">é</seg></w> ! <w n="5.3" punct="pe:6">s<seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>v<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="pe">é</seg></rhyme></w> !</l>
						<l n="6" num="2.2" lm="6" met="6"><w n="6.1">D<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="6.2">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="6.3">d</w>'<w n="6.4" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>l<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg>s</rhyme></w>.</l>
						<l n="7" num="2.3" lm="6" met="6"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>l</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="7.4">v<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="7.5">n<seg phoneme="o" type="vs" value="1" rule="437" place="5">o</seg>s</w> <w n="7.6" punct="pv:6">l<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg>s</rhyme></w> ;</l>
						<l n="8" num="2.4" lm="6" met="6"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="8.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="8.3">l</w>'<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="8.5" punct="pt:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rv<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="pt">é</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<head type="main">CHŒUR.</head>
						<l n="9" num="3.1" lm="6" met="6"><w n="9.1" punct="pe:2">S<seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="2" punct="pe">é</seg></w> ! <w n="9.2" punct="pe:4">s<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="pe">é</seg></w> ! <w n="9.3" punct="pe:6">s<seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>v<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="pe">é</seg></rhyme></w> !<del type="repetition" reason="analysis" hand="LG">etc.</del></l>
						<l n="10" num="3.2" lm="6" met="6"><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="10.1">D<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="10.2">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="10.3">d</w>'<w n="10.4" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>l<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg>s</rhyme></w>. </add></subst></l>
						<l n="11" num="3.3" lm="6" met="6"><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>l</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="11.4">v<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="11.5">n<seg phoneme="o" type="vs" value="1" rule="437" place="5">o</seg>s</w> <w n="11.6" punct="pv:6">l<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg>s</rhyme></w> ; </add></subst></l>
						<l n="12" num="3.4" lm="6" met="6"><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="12.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="12.3">l</w>'<w n="12.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="12.5" punct="pt:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rv<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="pt">é</seg></rhyme></w>. </add></subst></l>
					</lg>
					</div></body></text></TEI>