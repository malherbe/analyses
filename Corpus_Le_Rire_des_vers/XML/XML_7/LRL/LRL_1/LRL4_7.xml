<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL4" modus="sp" lm_max="6" metProfile="6, (4), (2)" form="suite de strophes" schema="1(abbab) 1(abab)">
	<head type="tune">AIR de Jeannot et Colin.</head>
	<lg n="1" type="quintil" rhyme="abbab">
		<l n="1" num="1.1" lm="6" met="6"><w n="1.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="1">e</seg>l</w> <w n="1.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r</w> <w n="1.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>pr<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
		<l n="2" num="1.2" lm="4"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="2.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ls</w> <w n="2.3">m<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="4">en</seg>s</rhyme></w></l>
		<l n="3" num="1.3" lm="2"><w n="3.1" punct="vg:2">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rm<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2" punct="vg">an</seg>s</rhyme></w>,</l>
		<l n="4" num="1.4" lm="6" met="6"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rs</w> <w n="4.2">qu</w>'<w n="4.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="4.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>tr<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
		<l n="5" num="1.5" lm="6" met="6"><w n="5.1"><seg phoneme="y" type="vs" value="1" rule="452" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">f<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>s</w> <w n="5.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="5.5" punct="pe:6"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6" punct="pe">an</seg>s</rhyme></w> !</l>
	</lg>
	<lg n="2" type="quatrain" rhyme="abab">
	<head type="speaker">LUDOVIC.</head>
		<l n="6" num="2.1" lm="6" met="6"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg></w> <w n="6.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4" punct="vg:6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>c<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
		<l n="7" num="2.2" lm="6" met="6"><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="7.2">j</w>'<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg></w> <w n="7.4">v<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="7.5">s</w>'<w n="7.6"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">om</seg>pl<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r</rhyme></w></l>
		<l n="8" num="2.3" lm="6" met="6"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="8.2">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="2">oin</seg>s</w> <w n="8.3">d</w>'<w n="8.4"><seg phoneme="y" type="vs" value="1" rule="452" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>c<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
		<l n="9" num="2.4" lm="6" met="6"><w n="9.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="9.2">pl<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4">en</seg>t</w> <w n="9.4" punct="pt:6">d<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>s<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="pt">i</seg>r</rhyme></w>.</l>
	</lg>
</div></body></text></TEI>