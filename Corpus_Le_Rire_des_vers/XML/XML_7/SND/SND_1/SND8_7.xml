<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE LIEUTENANT D'ARTILLERIE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="NSL" sort="2">
					<name>
						<forename>Jean-Pierre-Laurent</forename>
						<surname>DENOMBRET</surname>
						<addName type="pen_name">Charles NOMBRET SAINT-LAURENT</addName>
					</name>
					<date from="1791" to="1833">1791-1833</date>
				</author>
					<author key="DUV" sort="3">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>225 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SND_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=NWU:35556007830409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
								<repository>Northwestern university</repository>
								<idno type="URI">https://babel.hathitrust.org/cgi/pt?id=ien.35556007830409</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE VII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SND8" modus="cp" lm_max="10" metProfile="8, 4, (4+6)" form="suite de strophes" schema="1[ababb] 3[abab] 1[aa]">
				<head type="tune">AIR : Walse de Robin des Bois.</head>
					<lg n="1" type="regexp" rhyme="ababb">
						<head type="main">GERMILLY.</head>
						<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="1.5">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="1.6" punct="vg:8">t<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="2.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="2.2">d<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="2.3">z<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.6">l</w>'<w n="2.7" punct="ps:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rd<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="ps">eu</seg>r</rhyme></w> …</l>
						<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="3.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="3.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">l</w>'<w n="3.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="3.7">s</w>'<w n="3.8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>ppr<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="4" met="4"><space unit="char" quantity="12"></space><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="4.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="4.3">h<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nn<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</rhyme></w></l>
						<l n="5" num="1.5" lm="4" met="4"><space unit="char" quantity="12"></space><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="5.2" punct="pt:4">g<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rn<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="œ" type="vs" value="1" rule="406" place="4" punct="pt">eu</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="2" type="regexp" rhyme="ab">
						<head type="main">VICTORINE, à Delaunay.</head>
						<l n="6" num="2.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="6.2" punct="vg:2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2" punct="vg">ou</seg>s</w>, <w n="6.3" punct="vg:4">c<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" punct="vg">in</seg></w>, <w n="6.4">j</w>'<w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg></w> <w n="6.6" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="7" num="2.2" lm="10" met="4+6" met_alone="True"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="7.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="7.3" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406" place="4" punct="vg" caesura="1">eu</seg>rs</w>,<caesura></caesura> <w n="7.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" mp="Fm">e</seg>s</w>-<w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="7">e</seg></w> <w n="7.6" punct="pt:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pt">i</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="3" type="regexp" rhyme="ab">
						<head type="main">MAD. DU COLOMBIER, à Delaunay :</head>
						<l n="8" num="3.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">f<seg phoneme="i" type="vs" value="1" rule="481" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="8.5">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="442" place="7">o</seg>qu<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="9" num="3.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="9.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="9.2">s<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="9.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4">en</seg></w> <w n="9.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5" punct="pt:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rt<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pt">i</seg>r</rhyme></w>.</l>
					</lg>
					<div type="section" n="1">
						<head type="main">ENSEMBLE.</head>
						<lg n="1" type="regexp" rhyme="ababa">
							<head type="sub">CHŒUR</head>
							<l n="10" num="1.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="10.2">v<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="10.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="10.5">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="10.6" punct="vg:8">t<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
							<l n="11" num="1.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="11.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="11.2">d<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="11.3">z<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.6">l</w>'<w n="11.7" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rd<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pe">eu</seg>r</rhyme></w> !</l>
							<l n="12" num="1.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="12.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="12.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.5">l</w>'<w n="12.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="12.7">s</w>'<w n="12.8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>ppr<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
							<l n="13" num="1.4" lm="4" met="4"><space unit="char" quantity="12"></space><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="13.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="13.3">h<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nn<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</rhyme></w></l>
							<l n="14" num="1.5" lm="4" met="4"><space unit="char" quantity="12"></space><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="14.2" punct="pe:4">g<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rn<rhyme label="a" id="7" gender="m" type="a" stanza="4"><seg phoneme="œ" type="vs" value="1" rule="406" place="4" punct="pe">eu</seg>r</rhyme></w> !</l>
						</lg>
						<lg n="2" type="regexp" rhyme="babaa">
							<head type="main">GERMILLY.</head>
							<l n="15" num="2.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="15.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="15.5">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="15.6" punct="vg:8">t<rhyme label="b" id="8" gender="f" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
							<l n="16" num="2.2" lm="8" met="8"><space unit="char" quantity="4"></space><subst type="repetition" hand="LG" reason="analysis"><del>Etc., etc.</del><add rend="hidden"><w n="16.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="16.2">d<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="16.3">z<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="16.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.6">l</w>'<w n="16.7" punct="ps:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rd<rhyme label="a" id="7" gender="m" type="e" stanza="4"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="ps">eu</seg>r</rhyme></w> …</add></subst></l>
							<l n="17" num="2.3" lm="8" met="8"><space unit="char" quantity="4"></space><subst type="repetition" hand="LG" reason="analysis"><del> </del><add rend="hidden"><w n="17.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="17.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="17.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="17.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.5">l</w>'<w n="17.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="17.7">s</w>'<w n="17.8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>ppr<rhyme label="b" id="8" gender="f" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></add></subst></l>
							<l n="18" num="2.4" lm="4" met="4"><space unit="char" quantity="12"></space><subst type="repetition" hand="LG" reason="analysis"><del> </del><add rend="hidden"><w n="18.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="18.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="18.3">h<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nn<rhyme label="a" id="9" gender="m" type="a" stanza="5"><seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</rhyme></w></add></subst></l>
							<l n="19" num="2.5" lm="4" met="4"><space unit="char" quantity="12"></space><subst type="repetition" hand="LG" reason="analysis"><del> </del><add rend="hidden"><w n="19.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="19.2" punct="pt:4">g<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rn<rhyme label="a" id="9" gender="m" type="e" stanza="5"><seg phoneme="œ" type="vs" value="1" rule="406" place="4" punct="pt">eu</seg>r</rhyme></w>. </add></subst></l>
						</lg>
					</div>
				</div></body></text></TEI>