<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE LIEUTENANT D'ARTILLERIE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="NSL" sort="2">
					<name>
						<forename>Jean-Pierre-Laurent</forename>
						<surname>DENOMBRET</surname>
						<addName type="pen_name">Charles NOMBRET SAINT-LAURENT</addName>
					</name>
					<date from="1791" to="1833">1791-1833</date>
				</author>
					<author key="DUV" sort="3">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>225 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SND_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=NWU:35556007830409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
								<repository>Northwestern university</repository>
								<idno type="URI">https://babel.hathitrust.org/cgi/pt?id=ien.35556007830409</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE VIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SND9" modus="cm" lm_max="10" metProfile="4+6" form="strophe unique" schema="1(ababcdcd)">
				<head type="tune">Air des trois Couleurs. (de M. Vogel.)</head>
				<lg n="1" type="huitain" rhyme="ababcdcd">
					<head type="main">BONAPARTE.</head>
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2" mp="M">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="1.3"><seg phoneme="y" type="vs" value="1" rule="452" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fc">e</seg></w> <w n="1.4">v<seg phoneme="ɛ" type="vs" value="1" rule="304" place="7">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.5" punct="pi:10">c<seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>l<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">D</w>’<w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="2.3">f<seg phoneme="ɔ" type="vs" value="1" rule="442" place="2">o</seg>l</w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>r</w><caesura></caesura> <w n="2.5">n</w>'<w n="2.6"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="2.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>s</w> <w n="2.8">v<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="C">ou</seg>s</w> <w n="2.9" punct="pe:10">fl<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>tt<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pe">er</seg></rhyme></w> !</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="3.2" punct="ps:4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4" punct="ps" caesura="1">en</seg></w> …<caesura></caesura> <w n="3.3">c</w>'<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="3.6">fl<seg phoneme="o" type="vs" value="1" rule="437" place="7">o</seg>t</w> <w n="3.7" punct="vg:10">p<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="M">u</seg>l<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="P">à</seg></w> <w n="4.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="4.4">pr<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t</w> <w n="4.5">l</w>'<w n="4.6" punct="pe:10"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="9" mp="M">ê</seg>t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pe">er</seg></rhyme></w> !</l>
					<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="M/mp">on</seg>j<seg phoneme="y" type="vs" value="1" rule="449" place="2" mp="M/mp">u</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="Lp">on</seg>s</w>-<w n="5.2" punct="ps:4">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="4" punct="ps" caesura="1">e</seg></w> …<caesura></caesura> <w n="5.3">c</w>'<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="5.6">s<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="5.7" punct="vg:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>ss<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="6.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="2">en</seg>s</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="6.4">c<seg phoneme="œ" type="vs" value="1" rule="248" place="4" caesura="1">œu</seg>r</w><caesura></caesura> <w n="6.5">d<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" mp="F">e</seg>nt</w> <w n="6.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="6.7" punct="pv:10">r<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg><seg phoneme="y" type="vs" value="1" rule="452" place="9" mp="M">u</seg>n<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pv">i</seg>r</rhyme></w> ;</l>
					<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">S<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>ch<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="7.2">gu<seg phoneme="i" type="vs" value="1" rule="490" place="3" mp="M">i</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.4">t<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" mp="M">o</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="7">en</seg>t</w> <w n="7.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" mp="P">an</seg>s</w> <w n="7.6">s<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="7.7" punct="vg:10">c<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg></w> <w n="8.2">s<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="C">a</seg></w> <w n="8.3">f<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="M">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="406" place="4" caesura="1">eu</seg>r</w><caesura></caesura> <w n="8.4">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="5" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="8.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="C">ou</seg>s</w> <w n="8.6" punct="ps:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8" mp="M">en</seg>gl<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>t<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="ps">i</seg>r</rhyme></w> …</l>
				</lg>
			</div></body></text></TEI>