<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE LIEUTENANT D'ARTILLERIE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="NSL" sort="2">
					<name>
						<forename>Jean-Pierre-Laurent</forename>
						<surname>DENOMBRET</surname>
						<addName type="pen_name">Charles NOMBRET SAINT-LAURENT</addName>
					</name>
					<date from="1791" to="1833">1791-1833</date>
				</author>
					<author key="DUV" sort="3">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>225 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SND_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=NWU:35556007830409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
								<repository>Northwestern university</repository>
								<idno type="URI">https://babel.hathitrust.org/cgi/pt?id=ien.35556007830409</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE II.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SND3" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="1[abab] 1[abaab] 1[aaa]">
				<head type="tune">AIR : Et voilà comme tout s'arrange !</head>
				<lg n="1" type="regexp" rhyme="abababaabaaa">
					<head type="main">LORIS.</head>
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Qu</w>'<w n="1.2"><seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg></w> <w n="1.3">g<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="1.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w>-<w n="1.5">sc<seg phoneme="o" type="vs" value="1" rule="314" place="5">eau</seg>x</w> <w n="1.6">B<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>t<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8">in</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="2.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="y" type="vs" value="1" rule="452" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5" punct="pv:8"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>n<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="8">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Qu</w>'<w n="3.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="3.3" punct="vg:4">N<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ck<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="vg">er</seg></w>, <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5" punct="vg:8">p<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="vg">ain</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="4.2"><seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ts</w> <w n="4.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="4.5" punct="vg:8">r<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>f<rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="8">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.4">r<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>g<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>r<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></rhyme></w></l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">S<seg phoneme="wa" type="vs" value="1" rule="419" place="1">oi</seg>t</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>p<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="8">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>rv<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="7.2">qu</w>'<w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="7.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="7.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="7.8">gr<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></rhyme></w></l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">R<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="e" type="vs" value="1" rule="188" place="2">e</seg>t</w> <w n="8.3">b<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="8.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="8.6" punct="vg:8">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="ø" type="vs" value="1" rule="404" place="7">eu</seg>r<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="vg">é</seg></rhyme></w>,</l>
					<l n="9" num="1.9" lm="8" met="8"><w n="9.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="9.2" punct="vg:2">f<seg phoneme="wa" type="vs" value="1" rule="422" place="2" punct="vg">oi</seg></w>, <w n="9.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="9.4">r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.7">m<rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="8">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="10" num="1.10" lm="8" met="8"><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.3">M<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="i" type="vs" value="1" rule="481" place="5">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.4" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="339" place="6">A</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>c<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="8">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="11" num="1.11" lm="8" met="8"><w n="11.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="11.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="11.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>s</w>, <w n="11.4" punct="vg:5">ou<seg phoneme="i" type="vs" value="1" rule="490" place="5" punct="vg">i</seg></w>, <w n="11.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.6">m</w>'<w n="11.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="11.8">m<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="8">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="12" num="1.12" lm="8" met="8"><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3">M<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="i" type="vs" value="1" rule="481" place="5">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="12.4" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="339" place="6">A</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>c<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="8">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>