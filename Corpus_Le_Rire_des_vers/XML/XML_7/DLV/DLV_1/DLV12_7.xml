<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
				<title type="sub">INVRAISEMBLANCE EN TROIS PARTIES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
				  <name>
					<forename>Félix-Auguste</forename>
					<surname>DUVERT</surname>
				  </name>
				  <date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="DLV" sort="2">
					<name>
						<forename>Augustin Théodore</forename>
						<nameLink>de</nameLink>
						<surname>LAUZANNE DE VAUROUSSEL</surname>
						<addname type="other">Lauzanne</addname>
					</name>
					<date from="1805" to="1877">1805-1877</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>135 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
                    <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
						<author>F.-A. DUVERT EN SOCIÉTÉ AVEC M. LAUZANNE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?vid=UOM:39015076863409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>University of Michigan Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/mdp.39015076863409</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">16 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DLV12" modus="sp" lm_max="8" metProfile="6, (8)" form="strophe unique" schema="1(abba)">
	<head type="main">Reprise du chœur.</head>
	<lg n="1" type="quatrain" rhyme="abba">
		<l n="1" num="1.1" lm="8"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="1.2" punct="vg:4">J<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>th<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg>s</w>, <w n="1.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="5">e</seg>l</w> <w n="1.4">h<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>x</w> <w n="1.5" punct="pe:8">j<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pe">ou</seg>r</rhyme></w> ! </l>
		<l n="2" num="1.2" lm="6" met="6"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="2.2"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>bti<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="3">en</seg>t</w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="2.4" punct="pt:6">p<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>r<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="481" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></w>. </l>
		<l n="3" num="1.3" lm="6" met="6"><w n="3.1">M<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="3.3">s<seg phoneme="ɛ" type="vs" value="1" rule="383" place="4">ei</seg>gn<seg phoneme="ø" type="vs" value="1" rule="404" place="5">eu</seg>r<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="481" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
		<l n="4" num="1.4" lm="6" met="6"><w n="4.1">J<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="4.2"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg></w> <w n="4.3" punct="pt:6">L<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">em</seg>b<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="pt">ou</seg>rg</rhyme></w>.</l>
	</lg>
</div></body></text></TEI>