<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">27, 28 ET 29 JUILLET, TABLEAU ÉPISODIQUE DES TROIS JOURNÉES.</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ARA" sort="1">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<author key="DUV" sort="2">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>495 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
						<author>ARAGO ET DUVERT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=xjVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
								<author>ARAGO ET DUVERT</author>
								<repository>Österreichische Nationalbibliothek</repository>
								<idno type="URI">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886206</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : U+02BC MODIFIER LETTER APOSTROPHE) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<change when="2021-06-14" who="RR">Quelques corrections concernant la distribution du signe signe ʼ (UNICODE : ʼ).</change>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TROISIÈME JOURNÉE</head><head type="main_subpart">SCÈNE IV.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="AED23" modus="cp" lm_max="10" metProfile="8, 4+6" form="strophe unique" schema="1(ababcdcd)">
			<head type="tune">AIR : Un page aimait la jeune Adèle.</head>
				<lg n="1" type="huitain" rhyme="ababcdcd">
					<head type="main">RAIMOND, avec indignation.</head>
					<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="1.1">Qu</w>'<w n="1.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="1">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rtʼ</w> <w n="1.3">qu</w>'<w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="1.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4">e</seg>f</w> <w n="1.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.7" punct="pi:8">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>sh<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>n<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">C</w>'<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2" mp="C">un</seg></w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="353" place="3" mp="M">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4" caesura="1">em</seg>plʼ</w><caesura></caesura> <w n="2.5">qu</w>'<w n="2.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="2.7">nʼ</w> <w n="2.8">d<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>t</w> <w n="2.9">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>s</w> <w n="2.10" punct="pt:10"><seg phoneme="i" type="vs" value="1" rule="466" place="8" mp="M">i</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pt">er</seg></rhyme></w>.</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="3.2" punct="vg:4">W<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rl<seg phoneme="u" type="vs" value="1" rule="AED23_1" place="4" punct="vg" caesura="1">oo</seg></w>,<caesura></caesura> <w n="3.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="3.4">m</w>'<w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="3.6">s<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="M">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="8">en</seg>s</w> <w n="3.7" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg>c<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="10">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3" mp="C">un</seg></w> <w n="4.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4" caesura="1">e</seg>f</w><caesura></caesura> <w n="4.4">j</w>'<w n="4.5"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s</w> <w n="4.6">p<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="4.7" punct="pt:10">d<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>rt<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pt">er</seg></rhyme></w>.</l>
					<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="C">i</seg>l</w> <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg></w><caesura></caesura> <w n="5.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5" mp="P">an</seg>s</w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="5.6">l<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="169" place="8" mp="M">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="481" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
					<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="6.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2" mp="M">en</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="6.4">pr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>x</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="6.6">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="6.7" punct="vg:10">s<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" punct="vg">an</seg>g</rhyme></w>,</l>
					<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="4"></space><w n="7.1">S<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>l</w> <w n="7.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rt<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="7.4">l</w>'<w n="7.5" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">in</seg>f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="4"></space><w n="8.1">L</w>'<w n="8.2">h<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ø" type="vs" value="1" rule="404" place="4">eu</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="8.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.6" punct="pt:8">r<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pt">an</seg>g</rhyme></w>.</l>
				</lg>
		</div></body></text></TEI>