<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">27, 28 ET 29 JUILLET, TABLEAU ÉPISODIQUE DES TROIS JOURNÉES.</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ARA" sort="1">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<author key="DUV" sort="2">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>495 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
						<author>ARAGO ET DUVERT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=xjVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
								<author>ARAGO ET DUVERT</author>
								<repository>Österreichische Nationalbibliothek</repository>
								<idno type="URI">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886206</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : U+02BC MODIFIER LETTER APOSTROPHE) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<change when="2021-06-14" who="RR">Quelques corrections concernant la distribution du signe signe ʼ (UNICODE : ʼ).</change>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME JOURNÉE</head><head type="main_subpart">SCÈNE VII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="AED15" modus="sp" lm_max="8" metProfile="6, 3, 5, 8" form="suite périodique" schema="2(aabbccc) 1(aabb)">
			<head type="tune">Air : Au galop ! au galop !</head>
				<lg n="1" type="septain" rhyme="aabbccc">
					<head type="main">RAIMOND, à demi-voix.</head>
					<l n="1" num="1.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="1.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="1.2" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="pe">i</seg>s</w> ! <w n="1.3">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="1.4" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="pe">i</seg>s</rhyme></w> !</l>
					<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="2.1">Dʼ</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="2.3" punct="vg:3">pr<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" punct="vg">en</seg>cʼ</w>, <w n="2.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="4">oin</seg>t</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.6" punct="pe:6">cr<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="pe">i</seg>s</rhyme></w> !</l>
					<l n="3" num="1.3" lm="3" met="3"><space unit="char" quantity="10"></space><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="3.2">s<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="5" met="5"><space unit="char" quantity="6"></space><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="4.3">s</w>'<w n="4.4" punct="pt:5"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>v<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></w>.</l>
					<l n="5" num="1.5" lm="6" met="6"><space unit="char" quantity="4"></space><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="5.2" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="pe">i</seg>s</w> ! <w n="5.3">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="5.4" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="pe">i</seg>s</rhyme></w> !</l>
					<l n="6" num="1.6" lm="6" met="6"><space unit="char" quantity="4"></space><w n="6.1">Dʼ</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="6.3" punct="vg:3">pr<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" punct="vg">en</seg>cʼ</w>, <w n="6.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="4">oin</seg>t</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.6" punct="pe:6">cr<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="pe">i</seg>s</rhyme></w> !</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="7.2" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="pe">an</seg>t</w> ! <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.5" punct="pe:8">P<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>r<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pe">i</seg>s</rhyme></w> !</l>
				</lg>
				<p>(On entend le tambour pendant ce couplet ; tout le monde se met en bataille sur la gauche, le tambour à la droîte du peloton.)</p>
				<lg n="2" type="quatrain" rhyme="aabb">
					<l n="8" num="2.1" lm="6" met="6"><space unit="char" quantity="6"></space><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="8.2">pʼt<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ts</w> <w n="8.3">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>rʼ</w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="8.5" punct="pe:6">gr<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="pe">an</seg>ds</rhyme></w> !</l>
					<l n="9" num="2.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="9.2" punct="vg:2">dr<seg phoneme="wa" type="vs" value="1" rule="419" place="2" punct="vg">oi</seg>tʼ</w>, <w n="9.3">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="9.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="9.5" punct="pv:6">r<rhyme label="a" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="pv">an</seg>gs</rhyme></w> ;</l>
					<l n="10" num="2.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="10.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>d</w> <w n="10.3">l</w>'<w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="10.5">dʼ</w> <w n="10.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="10.7" punct="vg:6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">om</seg>bʼr<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="vg">a</seg></rhyme></w>,</l>
					<l n="11" num="2.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="11.1">L</w>'<w n="11.2"><seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>tʼ</w> <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.4" punct="pe:6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>pl<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="pe">a</seg></rhyme></w> !</l>
				</lg>
				<lg n="3" type="septain" rhyme="aabbccc">
					<head type="main">EN CHŒUR.</head>
					<l n="12" num="3.1" lm="6" met="6"><space unit="char" quantity="4"></space><subst hand="LG" type="repetition" reason="analysis"><del>Mes amis, etc.</del><add rend="hidden"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="12.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg>s</w>, <w n="12.3">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="12.4" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="pe">i</seg>s</rhyme></w> ! </add></subst></l>
					<l n="13" num="3.2" lm="6" met="6"><space unit="char" quantity="4"></space><subst hand="LG" type="repetition" reason="analysis"><del> </del><add rend="hidden"><w n="13.1">Dʼ</w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="13.3" punct="vg:3">pr<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" punct="vg">en</seg>cʼ</w>, <w n="13.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="4">oin</seg>t</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.6" punct="pe:6">cr<rhyme label="a" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="pe">i</seg>s</rhyme></w> ! </add></subst></l>
					<l n="14" num="3.3" lm="3" met="3"><space unit="char" quantity="10"></space><subst hand="LG" type="repetition" reason="analysis"><del> </del><add rend="hidden"><w n="14.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="14.2">s<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l<rhyme label="b" id="7" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></add></subst></l>
					<l n="15" num="3.4" lm="5" met="5"><space unit="char" quantity="6"></space><subst hand="LG" type="repetition" reason="analysis"><del> </del><add rend="hidden"><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="15.3">s</w>'<w n="15.4" punct="pt:5"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>v<rhyme label="b" id="7" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></w>. </add></subst></l>
					<l n="16" num="3.5" lm="6" met="6"><space unit="char" quantity="4"></space><subst hand="LG" type="repetition" reason="analysis"><del> </del><add rend="hidden"><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="16.2" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="pe">i</seg>s</w> ! <w n="16.3">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="16.4" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<rhyme label="c" id="8" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="pe">i</seg>s</rhyme></w> ! </add></subst></l>
					<l n="17" num="3.6" lm="6" met="6"><space unit="char" quantity="4"></space><subst hand="LG" type="repetition" reason="analysis"><del> </del><add rend="hidden"><w n="17.1">Dʼ</w> <w n="17.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="17.3" punct="vg:3">pr<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" punct="vg">en</seg>cʼ</w>, <w n="17.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="4">oin</seg>t</w> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.6" punct="pe:6">cr<rhyme label="c" id="8" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="pe">i</seg>s</rhyme></w> !</add></subst></l>
					<l n="18" num="3.7" lm="8" met="8"><subst hand="LG" type="repetition" reason="analysis"><del> </del><add rend="hidden"><w n="18.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="18.2" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="pe">an</seg>t</w> ! <w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="18.5" punct="pe:8">P<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>r<rhyme label="c" id="8" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pe">i</seg>s</rhyme></w> ! </add></subst></l>
				</lg>
		</div></body></text></TEI>