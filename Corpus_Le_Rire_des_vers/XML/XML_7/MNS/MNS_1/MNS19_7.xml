<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS19" modus="cp" lm_max="10" metProfile="8, (4+6), (9)" form="strophe unique" schema="1(ababcbbc)">
	<head type="tune">Air de Julie.</head>
	<lg n="1" type="huitain" rhyme="ababcbbc">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">m</w>'<w n="1.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="1.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="6">en</seg>s</w> <w n="1.7">qu</w>'<w n="1.8"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>l</w> <w n="1.9">m</w>'<w n="1.10" punct="pv:8">t<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ; </l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">su<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="2.3">s<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>l</w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="2.5">j</w>'<w n="2.6">pu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg>s</w> <w n="2.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="2.8" punct="vg:8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>g<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="vg">er</seg></rhyme></w>, </l>
		<l n="3" num="1.3" lm="9"><w n="3.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="3.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4" mp="M">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="418" place="5">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="3.4">n</w>' <w n="3.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="3.6" punct="vg:9">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>g<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="vg" mp="F">e</seg></rhyme></w>, </l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">P<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>tr<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="4.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</w> <w n="4.3">l</w>' <w n="4.4">g<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.5" punct="pv:8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>g<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pv">er</seg></rhyme></w> ; </l>
		<l n="5" num="1.5" lm="10" met="4+6" met_alone="True"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="5.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3" punct="vg:4">g<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">â</seg>t<seg phoneme="o" type="vs" value="1" rule="314" place="4" punct="vg" caesura="1">eau</seg></w>,<caesura></caesura> <w n="5.4">s<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>ch<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s</w> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="5.6" punct="vg:10">d<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>g<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>, </l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="6.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">om</seg>pr<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ttr</w>' <w n="6.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.5" punct="pv:8">b<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>rr<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pv">er</seg></rhyme></w> ; </l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="7.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3" punct="pe:4">j<seg phoneme="wa" type="vs" value="1" rule="422" place="4" punct="pe">oi</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> ! <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>v<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></rhyme></w></l>
		<l n="8" num="1.8" lm="8" met="8"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">d<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>x</w> <w n="8.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="8.6" punct="pe:8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>ge<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="310" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
	</lg>
</div></body></text></TEI>