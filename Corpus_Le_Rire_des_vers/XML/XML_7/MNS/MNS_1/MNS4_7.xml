<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS4" modus="cp" lm_max="9" metProfile="8, 4−5, (7)" form="suite de strophes" schema="2[abab]">
	<head type="tune">AIR :</head>
	<lg n="1" type="regexp" rhyme="ab">
		<l n="1" num="1.1" lm="9" met="4+5"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="C">ou</seg>s</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="1.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4" caesura="1">en</seg></w><caesura></caesura> <w n="1.4">s<seg phoneme="y" type="vs" value="1" rule="444" place="5">û</seg>r</w>' <w n="1.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.7">m<seg phoneme="wa" type="vs" value="1" rule="439" place="8" mp="M">o</seg>y<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="362" place="9">en</seg></rhyme></w></l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="2.2">d<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ssʼr<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="2.3">m<seg phoneme="a" type="vs" value="1" rule="144" place="6">a</seg>m</w>' <w n="2.4" punct="vg:8">R<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="7">ain</seg>v<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
	</lg>
	<lg n="2" type="regexp" rhyme="a">
		<head type="speaker">OCTAVIE</head>. 
		<l n="3" num="2.1" lm="8" met="8"><w n="3.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="3.2">c</w>'<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.5" punct="vg:5">m<seg phoneme="ɛ" type="vs" value="1" rule="381" place="4">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="406" place="5" punct="vg">eu</seg>r</w>, <w n="3.6">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>s</w>-<w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="7">e</seg></w> <w n="3.8" punct="pt:8">bi<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="8" punct="pt">en</seg></rhyme></w>.</l>
	</lg>
	<lg n="3" type="regexp" rhyme="babab">
		<head type="speaker">ANDRÉ</head>. 
		<l n="4" num="3.1" lm="8" met="8"><w n="4.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w>-<w n="4.3">t</w>-<w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.5">l</w>'<w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t</w> <w n="4.7" punct="pe:8">f<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>c<rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
		<l n="5" num="3.2" lm="8" met="8"><w n="5.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="5.2">m</w>'<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="5.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="5.5">b<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>c<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></rhyme></w></l>
		<l n="6" num="3.3" lm="8" met="8"><w n="6.1">Qu</w>' <w n="6.2">s<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="6.3">fʼs<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>x</w> <w n="6.5">d<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rbrʼs</w> <w n="6.7">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="6.8" punct="vg:8">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>c<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="y" type="vs" value="1" rule="452" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
		<l n="7" num="3.4" lm="9" met="4−5" mp4="P"><w n="7.1">J</w>'<w n="7.2">v<seg phoneme="ɔ" type="vs" value="1" rule="442" place="1">o</seg>l</w>' <w n="7.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="7.4">p<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>chʼs</w> <w n="7.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="4" mp="P" caesura="1">ou</seg>r</w><caesura></caesura> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="7.7">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="7.8" punct="vg:9">ch<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>ss<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="e" type="vs" value="1" rule="346" place="9" punct="vg">er</seg></rhyme></w>,</l>
		<l n="8" num="3.5" lm="7"><w n="8.1">Ç<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="8.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">sʼr<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="8.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="8.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="8.7" punct="pt:7">pr<rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg>s</rhyme></w>. <del reason="analysis" type="repetition" hand="KL">(bis)</del></l>
	</lg>
</div></body></text></TEI>