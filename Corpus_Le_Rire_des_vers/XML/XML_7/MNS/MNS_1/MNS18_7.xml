<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS18" modus="sp" lm_max="6" metProfile="6, 3, (4), (5)" form="suite de distiques" schema="4((aa))">
		<lg n="1" type="distiques" rhyme="aa…">
			<head type="speaker">LES PENSIONNAIRES.</head> 
			<l n="1" num="1.1" lm="4"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>rsu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w>-<w n="1.2" punct="vg:4">l<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg></rhyme></w>, </l>
			<l n="2" num="1.2" lm="6" met="6"><w n="2.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="2.3" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg">on</seg>s</w>, <w n="2.4">ou<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="2.5">d<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></rhyme></w></l>
			<l n="3" num="1.3" lm="3" met="3"><w n="3.1">Qu</w>'<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.3" punct="vg:3">t<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></w>, </l>
			<l n="4" num="1.4" lm="3" met="3"><w n="4.1">Qu</w>'<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.3" punct="pe:3">fr<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pe">e</seg></rhyme></w> ! </l>
			<l n="5" num="1.5" lm="4"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>rsu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w>-<w n="5.2" punct="vg:4">l<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg></rhyme></w>, </l>
			<l n="6" num="1.6" lm="6" met="6"><w n="6.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="6.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3" punct="vg:4">p<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg">on</seg>s</w>, <w n="6.4">ou<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="6.5" punct="vg:6">d<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="6" punct="vg">à</seg></rhyme></w>, </l>
			<l n="7" num="1.7" lm="3" met="3"><w n="7.1">C</w>'<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="7.3">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rt<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg></rhyme></w></l>
			<l n="8" num="1.8" lm="5"><w n="8.1">R<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>st<seg phoneme="e" type="vs" value="1" rule="346" place="2">er</seg></w> <w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="8.3" punct="pt:5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5" punct="pt">in</seg></rhyme></w>.</l> 
		</lg>
	</div></body></text></TEI>