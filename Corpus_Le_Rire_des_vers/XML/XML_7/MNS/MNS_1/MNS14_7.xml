<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS14" modus="sp" lm_max="8" metProfile="8, 4, 2" form="suite périodique" schema="2(abbacdcd)">
	<head type="tune">Air : Mon ami Pierre est mort ( de Scribe.)</head>
	<lg n="1" type="huitain" rhyme="abbacdcd">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">f<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s</w> <w n="1.3" punct="vg:4">c<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t</w>-<w n="1.5" punct="vg:8"><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg>l</rhyme></w>, </l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="2.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="2">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>t</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="2.6" punct="vg:8">gl<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>, </l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1" punct="vg:1">S<seg phoneme="œ" type="vs" value="1" rule="248" place="1" punct="vg">œu</seg>r</w>, <w n="3.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">m<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>s</w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="3.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="3.6" punct="vg:8">m<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>m<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,  </l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="4.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">cr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.6" punct="vg:8">p<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>r<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg>l</rhyme></w>, </l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="5">en</seg>s</w> <w n="5.5" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="vg">a</seg>s</w>, <w n="5.6">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="5.7" punct="vg:8">ch<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>, </l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="6.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="6.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">d<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t</w> <w n="6.6">d</w>'<w n="6.7" punct="dp:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>b<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="8" punct="dp">o</seg>rd</rhyme></w> : </l>
		<l n="7" num="1.7" lm="4" met="4"><w n="7.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="7.2">p<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">Pi<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w> <del reason="analysis" type="repetition" hand="KL">( bis)</del></l>
		<l n="8" num="1.8" lm="2" met="2"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w> <w n="8.2" punct="pt:2">m<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="2" punct="pt">o</seg>rt</rhyme></w>. </l>
	</lg>
	<lg n="2" type="huitain" rhyme="abbacdcd">
		<l n="9" num="2.1" lm="8" met="8"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">m</w>'<w n="9.4"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t</w> <w n="9.5" punct="ps:6">pl<seg phoneme="y" type="vs" value="1" rule="449" place="6" punct="ps">u</seg>s</w>… <w n="9.6" punct="pe:8">h<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>l<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pe">a</seg>s</rhyme></w> ! </l>
		<l n="10" num="2.2" lm="8" met="8"><w n="10.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="10.2"><seg phoneme="y" type="vs" value="1" rule="390" place="2">eu</seg>t</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">M<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>sc<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg></w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="10.6" punct="pv:8">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>v<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ; </l>
		<l n="11" num="2.3" lm="8" met="8"><w n="11.1">B<seg phoneme="o" type="vs" value="1" rule="314" place="1">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>p</w> <w n="11.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>s</w> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="11.4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.5">s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="11.6" punct="vg:8">b<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>, </l>
		<l n="12" num="2.4" lm="8" met="8"><w n="12.1" punct="ps:3">R<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="ps">e</seg>nt</w>… <w n="12.2">lu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="12.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>t</w> <w n="12.5" punct="pv:8">p<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pv">a</seg>s</rhyme></w> ; </l>
		<l n="13" num="2.5" lm="8" met="8"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rs</w> <w n="13.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>s</w> <w n="13.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="13.4">d<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>r</w> <w n="13.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<rhyme label="c" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>, </l>
		<l n="14" num="2.6" lm="8" met="8"><w n="14.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="14.2" punct="vg:3">pl<seg phoneme="ø" type="vs" value="1" rule="404" place="2">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">an</seg>t</w>, <w n="14.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>s</w> <w n="14.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t</w> <w n="14.6">d</w>'<w n="14.7" punct="dp:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>b<rhyme label="d" id="8" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="8" punct="dp">o</seg>rd</rhyme></w> : </l>
		<l n="15" num="2.7" lm="4" met="4"><w n="15.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="15.2">p<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.3">Pi<rhyme label="c" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
		<l n="16" num="2.8" lm="2" met="2"><w n="16.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w> <w n="16.2" punct="pt:2">m<rhyme label="d" id="8" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="2" punct="pt">o</seg>rt</rhyme></w>. <del reason="analysis" type="repetition" hand="KL">( bis.)</del></l>
	</lg>
</div></body></text></TEI>