<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC13" modus="sp" lm_max="7" metProfile="6, 3, 7" form="strophe unique" schema="1(ababcddc)">
	<head type="tune">AIR : O Pescator dell onda.</head>
	<lg n="1" type="huitain" rhyme="ababcddc">
		<l n="1" num="1.1" lm="6" met="6"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="1.2">cr<seg phoneme="ɔ" type="vs" value="1" rule="442" place="2">o</seg>k</w> <w n="1.3">m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>cr<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>k</w> <w n="1.4">b<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rl<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="6">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
		<l n="2" num="1.2" lm="3" met="3"><w n="2.1" punct="pe:3"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">am</seg>b<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="pe">ou</seg>rg</rhyme></w> !</l>
		<l n="3" num="1.3" lm="6" met="6"><w n="3.1">Cr<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="3.2">cr<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="3.3">fr<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="6">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
		<l n="4" num="1.4" lm="3" met="3"><w n="4.1" punct="pe:3">K<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rk<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="pe">ou</seg>r</rhyme></w> !</l>
		<l n="5" num="1.5" lm="6" met="6"><w n="5.1">Cr<seg phoneme="i" type="vs" value="1" rule="466" place="1">i</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="442" place="2">o</seg>k</w> <w n="5.2">cr<seg phoneme="i" type="vs" value="1" rule="466" place="3">i</seg>m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>k</w> <w n="5.3" punct="vg:6">cr<seg phoneme="i" type="vs" value="1" rule="466" place="5">i</seg>m<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="6" punct="vg">u</seg>k</rhyme></w>,</l>
		<l n="6" num="1.6" lm="6" met="6"><w n="6.1">Br<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>k</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="6.3" punct="vg:3">br<seg phoneme="ɛ" type="vs" value="1" rule="345" place="3" punct="vg">e</seg>k</w>, <w n="6.4">br<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>k</w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="6.6" punct="vg:6">br<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="6">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
		<l n="7" num="1.7" lm="7" met="7"><w n="7.1">Cr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>km<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="7.2">cr<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>k</w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="7.4" punct="vg:7">cr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>km<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>cr<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="7">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
		<l n="8" num="1.8" lm="7" met="7"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>sk<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>br<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>k</w> <w n="8.2">pl<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>k</w> <w n="8.3" punct="pt:7"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>sk<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>br<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="449" place="7" punct="pt">u</seg>k</rhyme></w>.</l>
	</lg> 
</div></body></text></TEI>