<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC29" modus="cp" lm_max="9" metProfile="(3), (5), (4), (9)" form="strophe unique" schema="1(abbaa)">
	<head type="tune">AIR : Dépéchons (du Fossé des Tuileries).</head>
	<lg n="1" type="quintil" rhyme="abbaa">
		<l n="1" num="1.1" lm="3"><w n="1.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="1.2" punct="vg:3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>l<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="3" punct="vg">à</seg></rhyme></w>, <del reason="analysis" type="repetition" hand="KL">(ter.)</del></l>
		<l n="2" num="1.2" lm="5"><w n="2.1">Pl<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="1">ein</seg>s</w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>sc<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="377" place="5">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w></l>
		<l n="3" num="1.3" lm="4"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="3.2">d</w>'<w n="3.3" punct="vg:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">in</seg>d<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>lg<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
		<l n="4" num="1.4" lm="3"><w n="4.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="4.2" punct="vg:3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>l<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="3" punct="vg">à</seg></rhyme></w>, <del reason="analysis" type="repetition" hand="KL">(ter.)</del></l>
		<l n="5" num="1.5" lm="9"><w n="5.1">S</w>'<w n="5.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>l</w> <w n="5.3">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="5.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="M">on</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>mn<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="5.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="C">ou</seg>s</w> <w n="5.6">s<seg phoneme="ɔ" type="vs" value="1" rule="418" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="Fm">e</seg>s</w>-<w n="5.7" punct="pe:9">l<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="9" punct="pe">à</seg></rhyme></w> !</l>
	</lg>
</div></body></text></TEI>