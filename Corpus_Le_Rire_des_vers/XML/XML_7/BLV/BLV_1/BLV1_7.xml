<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BANDEAU</title>
				<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="BLL" sort="1">
					<name>
						<forename>Jean-Nicolas</forename>
						<surname>BOUILLY</surname>
					</name>
					<date from="1763" to="1842">1763-1842</date>
				</author>
				<author key="VAN" sort="2">
					<name>
						<forename>Émile</forename>
						<surname>VANDERBURCH</surname>
						<addname type="other">Émile VANDERBURCH</addname>
						<addname type="other">Émile VANDER-BURCH</addname>
						<addname type="other">Émile VANDERBURCK</addname>
						<addname type="other">Émile VAN DER BURCK</addname>
					</name>
					<date from="1794" to="1862">1794-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>168 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE BANDEAU</title>
						<author>BOUILLY ET EM. VANDERBURCH</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GALLICA</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9782259n.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La Bibliothèque nationale de France</repository>
								<idno type="URL">https ://catalogue.bnf.fr/ark :/12148/cb31531335q</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">21 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BLV1" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(ababcdcd)">
	<head type="tune">AIR du Vaudeville l'Actrice.</head>
	<lg n="1" type="huitain" rhyme="ababcdcd">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">D</w>'<w n="1.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="1.3">c<seg phoneme="o" type="vs" value="1" rule="414" place="2">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg></w> <w n="1.4">c</w>'<w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="1.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="1.7">h<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="2.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="2.4">d<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="2.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="5">en</seg>t</w> <w n="2.6">m<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7" punct="pv:8"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>c<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="8" punct="pv">u</seg>s</rhyme></w> ;</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">C</w>'<w n="3.2" punct="vg:1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1" punct="vg">e</seg>st</w>, <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.4">l</w>'<w n="3.5" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">an</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="3.7">tr<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.8">v<seg phoneme="ø" type="vs" value="1" rule="404" place="7">eu</seg>v<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4">qu</w>'<w n="4.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="4.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.7">v<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>t</w> <w n="4.8" punct="pt:8">pl<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="449" place="8" punct="pt">u</seg>s</rhyme></w>.</l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="5.2" punct="vg:4">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" punct="vg">en</seg>t</w>, <w n="5.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="5.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.5" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="434" place="7">o</seg>nn<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1">N<seg phoneme="o" type="vs" value="1" rule="437" place="1">o</seg>s</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>s</w> <w n="6.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="6.5" punct="ps:8"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>st<seg phoneme="i" type="vs" value="1" rule="466" place="7">i</seg>m<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="ps">é</seg>s</rhyme></w>…</l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>ls</w> <w n="7.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="7.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>s</w> <w n="7.4" punct="vg:6">s<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="6" punct="vg">en</seg>t</w>, <w n="7.5">p<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>t</w>-<w n="7.6" punct="vg:8"><rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
		<l n="8" num="1.8" lm="8" met="8"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">s</w>'<w n="8.3"><seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="8.5">y<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</w> <w n="8.6" punct="pt:8">f<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rm<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pt">é</seg>s</rhyme></w>.</l>
	</lg>
</div></body></text></TEI>