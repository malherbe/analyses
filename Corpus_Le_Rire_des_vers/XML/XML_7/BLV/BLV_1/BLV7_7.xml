<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BANDEAU</title>
				<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="BLL" sort="1">
					<name>
						<forename>Jean-Nicolas</forename>
						<surname>BOUILLY</surname>
					</name>
					<date from="1763" to="1842">1763-1842</date>
				</author>
				<author key="VAN" sort="2">
					<name>
						<forename>Émile</forename>
						<surname>VANDERBURCH</surname>
						<addname type="other">Émile VANDERBURCH</addname>
						<addname type="other">Émile VANDER-BURCH</addname>
						<addname type="other">Émile VANDERBURCK</addname>
						<addname type="other">Émile VAN DER BURCK</addname>
					</name>
					<date from="1794" to="1862">1794-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>168 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE BANDEAU</title>
						<author>BOUILLY ET EM. VANDERBURCH</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GALLICA</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9782259n.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La Bibliothèque nationale de France</repository>
								<idno type="URL">https ://catalogue.bnf.fr/ark :/12148/cb31531335q</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">21 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BLV7" modus="cp" lm_max="10" metProfile="8, 4+6" form="strophe unique" schema="1(ababcdcd)">
	<head type="tune">AIR de Téniers.</head>
	<lg n="1" type="huitain" rhyme="ababcdcd">
		<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="1.2">l</w>'<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>r</w><caesura></caesura> <w n="1.4">d<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>t</w> <w n="1.5">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="6" mp="M">a</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="1.6">m<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="1.7" punct="vg:10">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg>dr<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="351" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="2.2">j<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>squ</w>'<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="2.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="2.5" punct="vg:8">s<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>p<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg>rs</rhyme></w>,</l>
		<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">D</w>'<w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="3.4">p<seg phoneme="y" type="vs" value="1" rule="449" place="4" caesura="1">u</seg>r</w><caesura></caesura> <w n="3.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="3.6">s<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>v<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.7">l</w>'<w n="3.8" punct="dp:10"><seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>vr<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="351" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></w> :</l>
		<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1" mp="C">Un</seg></w> <w n="4.2">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="4.3">h<seg phoneme="œ" type="vs" value="1" rule="406" place="3" mp="M">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397" place="4" caesura="1">eu</seg>x</w><caesura></caesura> <w n="4.4">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="5">en</seg>t</w> <w n="4.5">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6" mp="M">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="4.6">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="4.7" punct="pt:10">d<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>s<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pt">i</seg>rs</rhyme></w>.</l>
		<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="P">u</seg>r</w> <w n="5.2">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2" mp="C">e</seg>t</w> <w n="5.3" punct="vg:4">h<seg phoneme="i" type="vs" value="1" rule="496" place="3" mp="M">y</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="220" place="4" punct="vg" caesura="1">en</seg></w>,<caesura></caesura> <w n="5.4">pr<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>x</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="5.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t</w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="5.8" punct="vg:10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="M">on</seg>st<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="6.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="6.5" punct="pv:8">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pv">é</seg></rhyme></w> ;</l>
		<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="P">a</seg>r</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3" punct="vg:4">m<seg phoneme="i" type="vs" value="1" rule="492" place="3" mp="M">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4" punct="vg" caesura="1">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="5" mp="M">en</seg><seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="M">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg></w> <w n="7.5">d</w>'<w n="7.6" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>r<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
		<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="3" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="8.4"><seg phoneme="y" type="vs" value="1" rule="452" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fc">e</seg></w> <w n="8.5" punct="pt:10">d<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>v<seg phoneme="i" type="vs" value="1" rule="466" place="8" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>t<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="pt">é</seg></rhyme></w>.</l>
	</lg>
</div></body></text></TEI>