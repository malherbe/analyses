<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA MÉTEMPSYCOSE</title>
				<title type="sub">BÊTISE EN UN ACTE, MÊLÉE DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="COU" sort="1">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<author key="JAI" sort="2">
					<name>
						<forename>Ernest</forename>
						<surname>JAIME</surname>
					</name>
					<date from="1804" to="1884">1804-1884</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>282 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FCR_1</idno>	
				<availability status="free">
					<licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA MÉTEMPSYCOSE</title>
						<author>FRÉDÉRIC DE COURCY ET JAIME</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=c1RoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032819917.0x000001# ?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">7 FÉVRIER 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FCR18" modus="cm" lm_max="10" metProfile="4+6" form="suite de strophes" schema="2[abab]">
	<head type="main">Fragment du Husard de Felsheim.</head>
	<lg n="1" type="regexp" rhyme="ab">
	<head type="speaker">MADAME DUVAL, sans les voir.</head>
		<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="1.2" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg" caesura="1">on</seg>s</w>,<caesura></caesura> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="1.4">t<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7" mp="Lp">e</seg>st</w>-<w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.7" punct="pi:10">m<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
		<stage>( Les apercevant.)</stage>
		<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="2.2" punct="pe:2">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="2" punct="pe">eu</seg></w> ! <w n="2.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4">v<seg phoneme="wa" type="vs" value="1" rule="419" place="4" punct="pi" caesura="1">oi</seg>s</w>-<w n="2.5" punct="pi:4">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> ?<caesura></caesura> <w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w>-<w n="2.7">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="6" mp="F">e</seg></w> <w n="2.8">l</w>'<w n="2.9"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="7" mp="M">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>t</w> <w n="2.10" punct="pi:10">m<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>l<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" punct="pi">in</seg></rhyme></w> ?</l>
 	</lg>
 	<lg n="2" type="regexp" rhyme="a">
	<head type="speaker">ÉTIENNE, la retenant.</head>
		<l n="3" num="2.1" lm="10" met="4+6"><w n="3.1">N<seg phoneme="o" type="vs" value="1" rule="437" place="1">o</seg>t</w>' <w n="3.2">p<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>vr</w>' <w n="3.3" punct="vg:4">b<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>rge<seg phoneme="wa" type="vs" value="1" rule="419" place="4" punct="in vg" caesura="1">oi</seg>s</w>', <caesura></caesura><w n="3.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="3.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" mp="M">on</seg>ç<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>s</w> <w n="3.6">s<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="3.7" punct="pv:10">s<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="M">u</seg>rpr<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
		<l part="I" n="4" num="2.2" lm="10" met="4+6"><w n="4.1">Ç<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="4.2">c</w>'<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="4.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="C">on</seg></w> <w n="4.5" punct="pt:4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="pt" caesura="1">on</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>.<caesura></caesura> </l>
	</lg>
	<lg n="3" type="regexp" rhyme="b">
	<head type="speaker">CHRISTOPHE.</head>
		<l part="F" n="4" lm="10" met="4+6"><w n="4.6"><seg phoneme="e" type="vs" value="1" rule="188" place="5">E</seg>t</w> <w n="4.7" punct="vg:6">ç<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="vg">a</seg></w>, <w n="4.8">c</w>'<w n="4.9"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="4.10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="C">on</seg></w> <w n="4.11" punct="pt:10">c<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>s<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" punct="pt">in</seg></rhyme></w>.</l>
		<l part="I" n="5" num="3.1" lm="10" met="4+6"><w n="5.1">Vʼl<seg phoneme="a" type="vs" value="1" rule="341" place="1">à</seg></w> <w n="5.2">v<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>t</w>' <w n="5.3" punct="ps:4">d<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>f<seg phoneme="œ̃" type="vs" value="1" rule="451" place="4" punct="ps" caesura="1">un</seg>t</w>…<caesura></caesura> </l>
	</lg>
	<lg n="4" type="regexp" rhyme="">
	<head type="speaker">MADAME DUVAL.</head>
		<l part="M" n="5" lm="10" met="4+6"><w n="5.4">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="5">e</seg>l</w> <w n="5.5" punct="pe:7">h<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" mp="M">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406" place="7" punct="pe">eu</seg>r</w> ! </l>
	</lg>
	<lg n="5" type="regexp" rhyme="abab">
	<head type="speaker">CHRISTOPHE.</head>
		<l part="F" n="5" lm="10" met="4+6"><w n="5.6">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="5.7">v<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>s</w> <w n="5.8">l</w>'<w n="5.9" punct="pe:10">j<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></w> !</l>
		<l n="6" num="5.1" lm="10" met="4+6"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="6.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3" punct="vg:4">s<seg phoneme="o" type="vs" value="1" rule="317" place="3" mp="M">au</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="6.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="C">i</seg>l</w> <w n="6.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="7">en</seg>t</w> <w n="6.6"><seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg></w> <w n="6.7" punct="vg:10">b<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="vg">a</seg>s</rhyme></w>,</l>
		<l n="7" num="5.2" lm="10" met="4+6"><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="C">ou</seg>s</w> <w n="7.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="3" mp="M">en</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">ez</seg></w><caesura></caesura> <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">d</w>'<w n="7.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="7">è</seg>s</w> <w n="7.7">cʼt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="7.8" punct="vg:10">c<seg phoneme="wa" type="vs" value="1" rule="419" place="9" mp="M">oi</seg>ff<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
		<l n="8" num="5.3" lm="10" met="4+6"><w n="8.1">C</w>'<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="8.3">b<seg phoneme="ɛ̃" type="vs" value="1" rule="220" place="2">en</seg></w> <w n="8.4" punct="vg:4">f<seg phoneme="i" type="vs" value="1" rule="466" place="3" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="8.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="8.6">n</w>' <w n="8.7">v<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="C">ou</seg>s</w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>sʼr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="9">ai</seg></w> <w n="8.9" punct="pt:10">p<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="pt">a</seg>s</rhyme></w>.</l>
	</lg>
</div></body></text></TEI>