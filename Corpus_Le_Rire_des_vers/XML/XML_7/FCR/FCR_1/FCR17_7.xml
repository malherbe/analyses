<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA MÉTEMPSYCOSE</title>
				<title type="sub">BÊTISE EN UN ACTE, MÊLÉE DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="COU" sort="1">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<author key="JAI" sort="2">
					<name>
						<forename>Ernest</forename>
						<surname>JAIME</surname>
					</name>
					<date from="1804" to="1884">1804-1884</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>282 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FCR_1</idno>	
				<availability status="free">
					<licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA MÉTEMPSYCOSE</title>
						<author>FRÉDÉRIC DE COURCY ET JAIME</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=c1RoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032819917.0x000001# ?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">7 FÉVRIER 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FCR17" modus="cp" lm_max="9" metProfile="8, (7), (9)" form="strophe unique" schema="1(ababcdcd)">
	<head type="tune">AIR : Tenez, moi je suis un bon homme.</head>
	<lg n="1" type="huitain" rhyme="ababcdcd">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="vg">on</seg></w>, <w n="1.2">ç<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="1.3">n</w>'<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="1.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="1.8" punct="vg:8">n<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>t<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2" punct="vg:3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" punct="vg">en</seg>ds</w>, <w n="2.3">d</w>'<w n="2.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="5">è</seg>s</w> <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t</w> <w n="2.6">c</w>' <w n="2.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.8">j</w>' <w n="2.9" punct="vg:8">v<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="8" punct="vg">oi</seg>s</rhyme></w>,</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Qu</w>'<w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="3.3">pu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6" punct="vg:8">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>g<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="447" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">l</w>'<w n="4.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="4.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.5">pl<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>si<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>rs</w> <w n="4.6" punct="pt:8">f<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="8" punct="pt">oi</seg>s</rhyme></w>.</l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1" punct="vg">ai</seg>s</w>, <w n="5.2">s<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>t</w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="5.4" punct="vg:5">h<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="5.5">s<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>t</w> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="5.7" punct="vg:8">f<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="192" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">n</w>' <w n="6.3">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg></w> <w n="6.4">j<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s</w> <w n="6.5">qu</w>'<w n="6.6"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="6.7">b<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>s</rhyme></w></l>
		<l n="7" num="1.7" lm="7"><w n="7.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="7.2">qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg>qu</w>'<w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="7.4">pu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>r</w> <w n="7.6">d<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</w> <w n="7.7" punct="vg:7"><rhyme label="c" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></w>,</l>
		<l n="8" num="1.8" lm="9"><w n="8.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="8.2"><seg phoneme="i" type="vs" value="1" rule="496" place="2" mp="C">y</seg></w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="8.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t</w> <w n="8.6">qu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="8.7">n</w>'<w n="8.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="8.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>t</w> <w n="8.10" punct="pt:9">p<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="9" punct="pt">a</seg>s</rhyme></w>.</l>
	</lg>
</div></body></text></TEI>