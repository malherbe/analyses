<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">À PROPOS PATRIOTIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="VIL" sort="1">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="MAS" sort="2">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>273 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">VEM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>À propos patriotique</title>
						<author>VILLENEUVE ET MASSON</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=0FA6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>À propos patriotique</title>
								<author>VILLENEUVE ET MASSON</author>
								<repository>Bayerische Staatsbibliothek</repository>
								<idno type="URI">https://opacplus.bsb-muenchen.de/title/BV008031366</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_subpart">SCÈNE PREMIÈRE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="VEM5" modus="cp" lm_max="10" metProfile="8, 3−6, (4+6)" form="strophe unique" schema="1(ababcdcd)">
				<head type="tune">AIR : Ce magistrat irréprochable.</head>
				<lg n="1" type="huitain" rhyme="ababcdcd">
					<head type="main">Thérèse.</head>
					<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="1.3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>t</w> <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg></w> <w n="1.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="5">ein</seg></w> <w n="1.6">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="1.7">c<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rn<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="9" met="3+6"><space unit="char" quantity="2"></space><w n="2.1">R<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1" mp="M">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2" mp="M">e</seg>ct<seg phoneme="e" type="vs" value="1" rule="346" place="3" caesura="1">er</seg></w><caesura></caesura> <w n="2.2">ch<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="2.3" punct="vg:9">pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" mp="M">o</seg>pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="7" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="9" punct="vg">é</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="3.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="3.2">l</w>'<w n="3.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="3.4">d<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>t</w> <w n="3.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="3.6">l<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>r</w> <w n="3.7">c<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="10" met="4+6" met_alone="True"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="4.2">d<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>t</w> <w n="4.3">s<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="M">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>t</w><caesura></caesura> <w n="4.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="4.5">l<seg phoneme="œ" type="vs" value="1" rule="406" place="7" mp="C">eu</seg>r</w> <w n="4.6" punct="pt:10">pr<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="pt">é</seg></rhyme></w>.</l>
					<l n="5" num="1.5" lm="9" met="3+6"><space unit="char" quantity="2"></space><w n="5.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="1" mp="M">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" caesura="1">an</seg>t</w><caesura></caesura> <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="C">i</seg>ls</w> <w n="5.3">pr<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" mp="F">e</seg>nt</w> <w n="5.4">l<seg phoneme="œ" type="vs" value="1" rule="406" place="7" mp="C">eu</seg>r</w> <w n="5.5" punct="vg:9">v<seg phoneme="a" type="vs" value="1" rule="306" place="8" mp="M">a</seg>ill<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="6" num="1.6" lm="9" met="3−6" mp3="C"><space unit="char" quantity="2"></space><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg></w> <w n="6.2"><seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="C" caesura="1">i</seg>ls</w><caesura></caesura> <w n="6.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>t</w> <w n="6.4">l<seg phoneme="œ" type="vs" value="1" rule="406" place="7" mp="C">eu</seg>rs</w> <w n="6.5" punct="vg:9"><seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>t<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="9" punct="vg">i</seg>ls</rhyme></w>,</l>
					<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="4"></space><w n="7.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>l<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>r</w> <w n="7.3">d</w>'<w n="7.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.5">r<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">om</seg>p<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="4"></space><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">b<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">l<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>r</w> <w n="8.6" punct="pt:8">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="7">a</seg><rhyme label="d" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="320" place="8" punct="pt">y</seg>s</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>