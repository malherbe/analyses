<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">À PROPOS PATRIOTIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="VIL" sort="1">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="MAS" sort="2">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>273 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">VEM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>À propos patriotique</title>
						<author>VILLENEUVE ET MASSON</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=0FA6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>À propos patriotique</title>
								<author>VILLENEUVE ET MASSON</author>
								<repository>Bayerische Staatsbibliothek</repository>
								<idno type="URI">https://opacplus.bsb-muenchen.de/title/BV008031366</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_subpart">SCENE IV</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="VEM15" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(ababcdccd)">
				<head type="tune">AIR : Et voilà comme tout ça s'arrange.</head>
				<lg n="1" type="neuvain" rhyme="ababcdccd">
					<head type="main">Gacheux.</head>
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">C</w>'<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="1.3"><seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>cʼ</w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.6">l<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t</w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.8">rʼp<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="437" place="8">o</seg>s</rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="2.2">lʼqu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>l</w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg></w> <w n="2.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="2.7">ch<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="3.2">d<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r</w> <w n="3.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="3.4">s</w>'<w n="3.5"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>d</w> <w n="3.6">s<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>r</w> <w n="3.7">lʼ</w> <w n="3.8" punct="vg:8">d<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="437" place="8" punct="vg">o</seg>s</rhyme></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">C</w>'<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="4.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg></w> <w n="4.5">m<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.6">d</w>'<w n="4.7">h<seg phoneme="ɔ" type="vs" value="1" rule="418" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="4.9" punct="vg:8">pl<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="5.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="5.4">t<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rt</w> <w n="5.5">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>d<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>t</rhyme></w></l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">D</w>'<w n="6.2"><seg phoneme="i" type="vs" value="1" rule="496" place="1">y</seg></w> <w n="6.3">d<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r</w> <w n="6.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>gt<seg phoneme="ɛ" type="vs" value="1" rule="360" place="5">e</seg>ms</w> <w n="6.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="6.6" punct="vg:8">l<seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>mi<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="7.2">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="7.4">p<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>t</w> <w n="7.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="7.7">m<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>vʼm<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8">en</seg>t</rhyme></w></l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="8.2">r<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="188" place="3">e</seg>t</w> <w n="8.4">vʼl<seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="8.5">qu</w>'<w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="8.7">sʼ</w> <w n="8.8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="e" type="vs" value="1" rule="382" place="7">e</seg>ill<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>t</rhyme></w></l>
					<l n="9" num="1.9" lm="8" met="8"><w n="9.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="9.2">sʼ</w> <w n="9.3">tr<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>r</w> <w n="9.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.6">n<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>r</w> <w n="9.8" punct="pt:8">t<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>