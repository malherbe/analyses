<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L'HOMME QUI BAT SA FEMME</title>
				<title type="sub">TABLEAU POPULAIRE EN UN ACTE, MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DML" sort="1">
				  <name>
					<forename>Julien</forename>
					<nameLink>de</nameLink>
					<surname>MAILLAN</surname>
					<addname type="other">Julien</addname>
					<addname type="other">Julien de M.</addname>
				  </name>
				  <date from="1805" to="1851">1805-1851</date>
				</author>
				<author key="DNR" sort="2">
				  <name>
					<forename>Philippe-François</forename>
					<surname>PINEL</surname>
					<addname type="pen_name">DUMANOIR</addname>
					<addname type="other">Philippe Dumanoir</addname>
					<addname type="other">Philippe D.</addname>
					<addname type="other">Philippe D***</addname>
				  </name>
				  <date from="1806" to="1865">1806-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DMP_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
          <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L'HOMME QUI BAT SA FEMME</title>
						<author>JULIEN DE MALLIAN ET PHILIPPE DUMANOIR</author>
					</titleStmt>
					<publicationStmt>
						<publisher>INTERNET ARCHIVE</publisher>
						<idno type="URL">https ://archive.org/details/lhommequibatsafe00malluoft</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>University of Toronto Libraries</repository>
								<idno type="URL">https ://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991105986034006196</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">8 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DMP8" modus="sm" lm_max="6" metProfile="6" form="strophe unique" schema="1(abab)">
	<head type="main">CHŒUR.</head>
	<head type="tune">AIR de Wallace.</head>
	<lg n="1" type="quatrain" rhyme="abab">
		<l n="1" num="1.1" lm="6" met="6"><w n="1.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="1.2">t<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="1.3">d<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
		<l n="2" num="1.2" lm="6" met="6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1">É</seg>l<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w>-<w n="2.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="2.3">d</w>'<w n="2.4" punct="dp:6"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>c<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="dp">i</seg></rhyme></w> :</l>
		<l n="3" num="1.3" lm="6" met="6"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="3.2">qu</w>'<w n="3.3"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg></w> <w n="3.4">pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>ch<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg></w> <w n="3.5">t<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>p<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
		<l n="4" num="1.4" lm="6" met="6"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="4.3">s<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>t</w> <w n="4.4" punct="pt:6"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>rc<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="pt">i</seg></rhyme></w>.</l>
	</lg>
</div></body></text></TEI>