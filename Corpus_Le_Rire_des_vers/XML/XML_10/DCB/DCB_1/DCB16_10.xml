<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB16" modus="sp" lm_max="8" metProfile="7, 6, (8)" form="suite de strophes" schema="4[abab] 2[aa]">
	<head type="tune">AIR du Hussard de Felsheim.</head>
	<lg n="1" type="regexp" rhyme="abab">
		<l n="1" num="1.1" lm="7" met="7"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="1.2">t<seg phoneme="wa" type="vs" value="1" rule="422" place="2">oi</seg></w> <w n="1.3">j</w>'<w n="1.4"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg></w> <w n="1.5" punct="vg:7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
		<l n="2" num="1.2" lm="7" met="7"><w n="2.1">J<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="2.2">m<seg phoneme="wa" type="vs" value="1" rule="422" place="3">oi</seg></w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4">m</w>'<w n="2.5" punct="pv:7"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="e" type="vs" value="1" rule="346" place="7" punct="pv">er</seg></rhyme></w> ;</l>
		<l n="3" num="1.3" lm="7" met="7"><w n="3.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="3.2">l</w>'<w n="3.3" punct="vg:2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2" punct="vg">oi</seg>s</w>, <w n="3.4">j</w>'<w n="3.5">su<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>s</w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="3.7">t<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="3.8" punct="pv:7">pu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>ss<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></w> ;</l>
		<l n="4" num="1.4" lm="7" met="7"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="4.2">t<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="4.3">n</w>'<w n="4.4">p<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="4.6" punct="pt:7"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>b<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>s<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="e" type="vs" value="1" rule="346" place="7" punct="pt">er</seg></rhyme></w>.</l>
	</lg>
	<p>
		TÉLÉMAQUE, à part.<lb></lb>
		(parté.) V'là l'hic.<lb></lb>
	</p>
	<lg n="2" type="regexp" rhyme="ab">
	<head type="speaker">MARGUERITE.</head>
		<l n="5" num="2.1" lm="7" met="7"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">m</w>'<w n="5.3">m<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2">e</seg>ts</w> <w n="5.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>tt<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
		<l n="6" num="2.2" lm="7" met="7"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="6.2">l</w>'<w n="6.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>br<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="6.4">s<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="6.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="6.6" punct="pt:7">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>rm<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="7" punct="pt">en</seg>t</rhyme></w>.</l>
	</lg> 
	<lg n="3" type="regexp" rhyme="abaa">
	<head type="speaker">TÉLÉMAQUE.</head>
		<l n="7" num="3.1" lm="7" met="7"><w n="7.1">F<seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>t</w>-<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">j</w>'<w n="7.5">s<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>s</w> <w n="7.6">T<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>m<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
		<l n="8" num="3.2" lm="7" met="7"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="8.4">d<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>x</w> <w n="8.5" punct="pe:7">m<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>m<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="7" punct="pe">en</seg>t</rhyme></w> !</l>
		<l n="9" num="3.3" lm="6" met="6"><w n="9.1">C</w>'<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="9.3" punct="vg:3">m<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="vg">a</seg>l</w>, <w n="9.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>d</w>'<w n="9.5" punct="pv:6">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<rhyme label="a" id="5" gender="m" type="a" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6" punct="pv">an</seg></rhyme></w> ;</l>
		<l n="10" num="3.4" lm="7" met="7"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="10.2">c</w>'<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="10.5">f<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>m<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="10.6" punct="pe:7">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>rm<rhyme label="a" id="5" gender="m" type="e" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="7" punct="pe">en</seg>t</rhyme></w> !</l>
		<stage>(prenant les souliers qu'il a faits pour elle.)</stage>
	</lg>
	<lg n="4" type="regexp" rhyme="aba">
	<head type="tune">Même air.</head>
		<l n="11" num="4.1" lm="7" met="7"><w n="11.1">V<seg phoneme="wa" type="vs" value="1" rule="439" place="1">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="11.2">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="11.3" punct="vg:5">s<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>li<seg phoneme="e" type="vs" value="1" rule="346" place="5" punct="vg">er</seg>s</w>, <w n="11.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="11.5" punct="pv:7">ch<rhyme label="a" id="6" gender="f" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></w> ;</l>
		<stage>(Marguerite s'assied.)</stage>
		<l n="12" num="4.2" lm="7" met="7"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="12.2">m<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">c</w>'<w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="12.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="12.6" punct="pt:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">em</seg>pl<rhyme label="b" id="7" gender="m" type="a" stanza="4"><seg phoneme="wa" type="vs" value="1" rule="422" place="7" punct="pt">oi</seg></rhyme></w>.</l>
		<l n="13" num="4.3" lm="7" met="7"><w n="13.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="13.2">sʼr<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>s</w> <w n="13.3" punct="vg:5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="13.4">j</w>'<w n="13.5" punct="pv:7"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>sp<rhyme label="a" id="6" gender="f" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></w> ;</l>
		<stage>(lui prenant le pied.)</stage>
		<l part="I" n="14" num="4.4" lm="7" met="7"><w n="14.1">Qu</w>'<w n="14.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>l</w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="14.4" punct="pe:4">g<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="3">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="pe">i</seg>l</w> ! </l>
	</lg>
	<lg n="5" type="regexp" rhyme="b">
	<head type="speaker">MARGUERITE.</head>
		<l part="F" n="14" lm="7" met="7"><w n="14.5">C</w>'<w n="14.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="14.8" punct="pt:7">t<rhyme label="b" id="7" gender="m" type="e" stanza="4"><seg phoneme="wa" type="vs" value="1" rule="422" place="7" punct="pt">oi</seg></rhyme></w>.</l>
	</lg>
	<lg n="6" type="regexp" rhyme="a">
	<head type="speaker">TÉLÉMAQUE, après lui avoir mis les souliers.</head>
		<l n="15" num="6.1" lm="7" met="7"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="2">en</seg>s</w> <w n="15.3">qu</w>'<w n="15.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="15.5">r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="15.6">s</w>'<w n="15.7" punct="pt:7">d<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>tr<rhyme label="a" id="8" gender="f" type="a" stanza="5"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
	</lg>
	<lg n="7" type="regexp" rhyme="b">
	<head type="speaker">MARGUERITE, montrant son pied.</head>
		<l n="16" num="7.1" lm="8"><w n="16.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="16.2">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="16.3">ç<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="16.4">v<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="16.5" punct="pe:8">j<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>l<seg phoneme="i" type="vs" value="1" rule="466" place="7">i</seg>m<rhyme label="b" id="9" gender="m" type="a" stanza="5"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="pe">en</seg>t</rhyme></w> !</l>
	</lg>
	<lg n="8" type="regexp" rhyme="abaa">
	<head type="speaker">TÉLÉMAQUE, à part.</head>
		<l n="17" num="8.1" lm="7" met="7"><w n="17.1">F<seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>t</w>-<w n="17.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="17.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.4">j</w>'<w n="17.5">s<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>s</w> <w n="17.6">T<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>m<rhyme label="a" id="8" gender="f" type="e" stanza="5"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
		<l n="18" num="8.2" lm="7" met="7"><w n="18.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="18.2">v<seg phoneme="wa" type="vs" value="1" rule="439" place="2">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="18.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.4">pi<seg phoneme="e" type="vs" value="1" rule="240" place="5">e</seg>d</w> <w n="18.5" punct="pe:7">ch<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rm<rhyme label="b" id="9" gender="m" type="e" stanza="5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" punct="pe">an</seg>t</rhyme></w> !</l>
		<l n="19" num="8.3" lm="6" met="6"><w n="19.1">C</w>'<w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="19.3" punct="vg:3">m<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="vg">a</seg>l</w>, <w n="19.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>d</w>'<w n="19.5" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<rhyme label="a" id="10" gender="m" type="a" stanza="6"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6" punct="vg">an</seg></rhyme></w>,</l>
		<l n="20" num="8.4" lm="7" met="7"><w n="20.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="20.2">c</w>'<w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="20.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="20.5">f<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>m<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="20.6" punct="pe:7">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>rm<rhyme label="a" id="10" gender="m" type="e" stanza="6"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="7" punct="pe">en</seg>t</rhyme></w> !</l>
	</lg>
</div></body></text></TEI>