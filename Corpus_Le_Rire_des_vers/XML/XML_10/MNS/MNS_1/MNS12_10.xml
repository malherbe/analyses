<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">CHOEUR GÉNÉRAL.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS12" modus="cm" lm_max="10" metProfile="4+6" form="strophe unique" schema="1(abab)">
		<head type="tune">Air des Petits appartemens.</head>
		<lg n="1" type="quatrain" rhyme="abab">
			<head type="speaker">MADAME RAINVILLE, M. MAŽICOT, M. JOLYET, MADEMOISELLE BERNARD.</head>
			<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="C">Au</seg></w> <w n="1.2">d<seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>j<seg phoneme="ø" type="vs" value="1" rule="389" place="3" mp="M">eû</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="1.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" mp="F">e</seg>s</w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="Fc">e</seg>s</w> <w n="1.5">v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>t</w> <w n="1.6" punct="vg:10">v<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
			<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="2.2">pu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4" caesura="1">è</seg>s</w><caesura></caesura> <w n="2.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="2.5">l<seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="M">i</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t</w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="P">à</seg></w> <w n="2.7">l<seg phoneme="œ" type="vs" value="1" rule="406" place="9" mp="C">eu</seg>rs</w> <w n="2.8" punct="vg:10">j<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="10" punct="vg">eu</seg>x</rhyme></w>,</l>
			<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>r</w><caesura></caesura> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="3.5">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6" mp="C">e</seg>s</w> <w n="3.6">li<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>x</w> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="3.8" punct="vg:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9" mp="M">in</seg>v<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
			<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="Fc">e</seg>s</w> <w n="4.2">s<seg phoneme="o" type="vs" value="1" rule="317" place="3" mp="M">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" caesura="1">on</seg>t</w><caesura></caesura> <w n="4.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="5">en</seg></w> <w n="4.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="4.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="P">à</seg></w> <w n="4.7">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="4.8">v<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="247" place="10">œu</seg>x</rhyme></w></l>
		</lg>
	</div></body></text></TEI>