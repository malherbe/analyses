<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE VII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC19" modus="cm" lm_max="10" metProfile="4+6" form="suite de strophes" schema="1[ababb] 1[abbab]">
					<head type="tune">AIR : de Préville et Taconnet.</head>
						<lg n="1" type="regexp" rhyme="ab">
							<head type="main">MUSICO</head>
							<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="P">u</seg>r</w> <w n="1.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" mp="C">on</seg></w> <w n="1.3" punct="vg:4">s<seg phoneme="i" type="vs" value="1" rule="492" place="3" mp="M">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4" punct="vg" caesura="1">è</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="P">à</seg></w> <w n="1.5">l</w>'<w n="1.6"><seg phoneme="o" type="vs" value="1" rule="443" place="6" mp="M/mc">O</seg>p<seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M/mc">é</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="Lc">a</seg></w>-<w n="1.7" punct="vg:10">C<seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M/mc">o</seg>m<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="2.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="2">en</seg>t</w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="2.4">jou<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5" mp="C">un</seg></w> <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="443" place="6" mp="M">o</seg>p<seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w> <w n="2.7" punct="pt:10">n<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>v<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="o" type="vs" value="1" rule="314" place="10" punct="pt">eau</seg></rhyme></w>.</l>
						</lg>
						<lg n="2" type="regexp" rhyme="abbabb">
							<head type="main">LE REMOULEUR .</head>
							<l n="3" num="2.1" lm="10" met="4+6"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1" mp="C">Un</seg></w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="443" place="2" mp="M">o</seg>p<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg></w><caesura></caesura> <w n="3.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="3.4">r<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>lʼ</w> <w n="3.5">s<seg phoneme="y" type="vs" value="1" rule="449" place="7" mp="P">u</seg>r</w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="3.7" punct="vg:10">m<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="M">u</seg>s<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="4" num="2.2" lm="10" met="4+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">l</w>'<w n="4.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="4.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="4.7">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="6" mp="M">ou</seg><seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="4.8"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="P">à</seg></w> <w n="4.9" punct="pi:10">F<seg phoneme="ɛ" type="vs" value="1" rule="322" place="9" mp="M">ey</seg>d<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="o" type="vs" value="1" rule="314" place="10" punct="pi">eau</seg></rhyme></w> ?</l>
							<l n="5" num="2.3" lm="10" met="4+6"><w n="5.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="5.2" punct="vg:2">f<seg phoneme="wa" type="vs" value="1" rule="422" place="2" punct="vg">oi</seg></w>, <w n="5.3">c</w>'<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="5.5" punct="vg:4">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4" punct="vg" caesura="1">ai</seg></w>,<caesura></caesura> <w n="5.6">v<seg phoneme="wa" type="vs" value="1" rule="419" place="5" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="5.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="7">en</seg></w> <w n="5.8">d<seg phoneme="y" type="vs" value="1" rule="449" place="8" mp="C">u</seg></w> <w n="5.9" punct="pt:10">n<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>v<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="o" type="vs" value="1" rule="314" place="10" punct="pt">eau</seg></rhyme></w>.</l>
							<l n="6" num="2.4" lm="10" met="4+6"><w n="6.1">L</w>'<w n="6.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="M">i</seg>d<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="6.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="304" place="5" mp="M">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="6.5" punct="vg:10"><seg phoneme="o" type="vs" value="1" rule="443" place="7" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>g<seg phoneme="i" type="vs" value="1" rule="466" place="9" mp="M">i</seg>n<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="7" num="2.5" lm="10" met="4+6"><w n="7.1">C</w>'<w n="7.2"><seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="7.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="3">en</seg></w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="341" place="4" caesura="1">à</seg></w><caesura></caesura> <w n="7.5">qu</w>'<w n="7.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t</w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="7.9" punct="pv:10">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9" mp="M">o</seg>rt<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pv">er</seg></rhyme></w> ;</l>
							<l n="8" num="2.6" lm="10" met="4+6"><w n="8.1">C<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>mmʼ</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>ct<seg phoneme="œ" type="vs" value="1" rule="406" place="4" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="8.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="8.5">d<seg phoneme="y" type="vs" value="1" rule="444" place="6">û</seg></w> <w n="8.6">t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="8.7">l</w>'<w n="8.8" punct="ps:10"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9" mp="M">o</seg>pt<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="ps">er</seg></rhyme></w>…</l>
						</lg>
						<lg n="3" type="regexp" rhyme="ab">
							<head type="main">MUSICO.</head>
							<l n="9" num="3.1" lm="10" met="4+6"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1" mp="M">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">in</seg></w> <w n="9.2"><seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="C">i</seg>ls</w> <w n="9.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" caesura="1">on</seg>t</w><caesura></caesura> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="9.5">L<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="9.6" punct="vg:10">m<seg phoneme="y" type="vs" value="1" rule="449" place="8" mp="M">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>c<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="10" num="3.2" lm="10" met="4+6"><w n="10.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="10.2">ç<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="C">a</seg></w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="10.4">l<seg phoneme="œ" type="vs" value="1" rule="406" place="5" mp="C">eu</seg>r</w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="P">à</seg></w> <w n="10.7" punct="pe:10">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="M">an</seg>t<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pe">er</seg></rhyme></w> !</l>
						</lg>
				</div></body></text></TEI>