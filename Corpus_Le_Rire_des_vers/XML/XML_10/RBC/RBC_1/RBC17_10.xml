<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE VI.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC17" modus="sm" lm_max="6" metProfile="6" form="strophe unique" schema="1(ababcdcd)">
				<head type="tune">AIR : de l'Artiste.</head>
				<lg n="1" type="huitain" rhyme="ababcdcd">
					<head type="main">LA VALEUR.</head>
					<l n="1" num="1.1" lm="6" met="6"><w n="1.1">L</w>'<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="353" place="1">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">em</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="1.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="1.6" punct="vg:6">su<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="6" met="6"><w n="2.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="2.2">vʼn<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="2.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="2.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>g<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="vg">er</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="6" met="6"><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="3.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>rri<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="3.5">v<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="6" met="6"><w n="4.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="4.3">j<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>g</w> <w n="4.4" punct="pv:6"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>g<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="pv">er</seg></rhyme></w> ;</l>
					<l n="5" num="1.5" lm="6" met="6"><w n="5.1">C</w>'<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="5.3"><seg phoneme="y" type="vs" value="1" rule="462" place="2">u</seg>nʼ</w> <w n="5.4">vi<seg phoneme="ɛ" type="vs" value="1" rule="381" place="3">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="5.5">h<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="6" num="1.6" lm="6" met="6"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="6.3">nʼp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rdr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="6.4" punct="dp:6">j<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" punct="dp">ai</seg>s</rhyme></w> :</l>
					<l n="7" num="1.7" lm="6" met="6"><w n="7.1">L</w>'<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>r</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="7.5">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="8" num="1.8" lm="6" met="6"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w> <w n="8.2">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="345" place="3">e</seg>l</w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>x</w> <w n="8.4" punct="pt:6">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>ç<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" punct="pt">ai</seg>s</rhyme></w>.</l>
				</lg>
		</div></body></text></TEI>