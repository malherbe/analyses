<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE IX.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC23" modus="cp" lm_max="10" metProfile="8, 4+6" form="strophe unique" schema="1(ababcbcb)">
					<head type="tune">AIR : Vaudeville de la Petite Gouvernante.</head>
					<lg n="1" type="huitain" rhyme="ababcbcb">
						<head type="main">LE DEY.</head>
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="1.3">l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="1.4">s</w>'<w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>r</w> <w n="1.6">t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>tʼs</w> <w n="1.7">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="1.8" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>scl<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="2.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r</w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="2.5" punct="pv:8">l<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rt<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pv">é</seg></rhyme></w> ;</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="3.2">m</w>'<w n="3.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="3.4" punct="vg:4">r<seg phoneme="o" type="vs" value="1" rule="434" place="3" mp="M">o</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="3.5">l</w>'<w n="3.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="3.8">p<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></w> <w n="3.9">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="3.10" punct="vg:10">c<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="4.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rs</w> <w n="4.2"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg></w> <w n="4.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="4">om</seg></w> <w n="4.4">dʼ</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="4.6" punct="dp:8">l<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rt<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="dp">é</seg></rhyme></w> :</l>
						<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="4"></space><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="5.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="3">en</seg></w> <w n="5.4">j<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>stʼ</w> <w n="5.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.7">mʼ</w> <w n="5.8" punct="vg:8">d<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ss<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">N<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>l</w> <w n="6.2">n</w>'<w n="6.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="6.5">dr<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>t</w><caesura></caesura> <w n="6.6">d</w>'<w n="6.7"><seg phoneme="i" type="vs" value="1" rule="496" place="5">y</seg></w> <w n="6.8">tr<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="6.9"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="P">à</seg></w> <w n="6.10" punct="ps:10">bl<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">â</seg>m<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="ps">er</seg></rhyme></w>…</l>
						<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="7.2">l<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="7.3">nʼ</w> <w n="7.4">p<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>t</w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>s</w> <w n="7.6">m</w>'<w n="7.7"><seg phoneme="o" type="vs" value="1" rule="414" place="7" mp="M">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w> <w n="7.8">m<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="7.9" punct="vg:10">p<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="4"></space><w n="8.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="8.2">c</w>'<w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>llʼ</w> <w n="8.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="8.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.7">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="8.8" punct="pt:8">f<seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pt">er</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>