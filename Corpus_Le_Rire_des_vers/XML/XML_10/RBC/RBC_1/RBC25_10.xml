<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE X.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC25" modus="sm" lm_max="7" metProfile="7" form="suite de strophes" schema="3[abab]">
					<head type="tune">AIR : Je ne veux pas qu'on me prenne.</head>
					<lg n="1" type="regexp" rhyme="abababab">
						<head type="main">LA LIBERTÉ.</head>
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="1.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4">p<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.5" punct="vg:7">l<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="7" met="7"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">l<seg phoneme="i" type="vs" value="1" rule="466" place="2">i</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="2.4" punct="pv:7">p<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>v<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="wa" type="vs" value="1" rule="419" place="7" punct="pv">oi</seg>rs</rhyme></w> ;</l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">r<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>s</w> <w n="3.3">l</w>'<w n="3.4"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>qu<seg phoneme="i" type="vs" value="1" rule="486" place="6">i</seg>l<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="7" met="7"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="4.3" punct="vg:4">dr<seg phoneme="wa" type="vs" value="1" rule="419" place="4" punct="vg">oi</seg>ts</w>, <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="4.5" punct="pt:7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="wa" type="vs" value="1" rule="419" place="7" punct="pt">oi</seg>rs</rhyme></w>.</l>
						<l n="5" num="1.5" lm="7" met="7"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="5.3">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="5.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6" punct="vg:7">f<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="6" num="1.6" lm="7" met="7"><w n="6.1">J</w>'<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="6.4">r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="6.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="6.6" punct="pt:7">s<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>ti<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="7" punct="pt">en</seg></rhyme></w>.</l>
						<l n="7" num="1.7" lm="7" met="7"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="7.2">n</w>'<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="7.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.6">tr<seg phoneme="o" type="vs" value="1" rule="414" place="5">ô</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg></w> <w n="7.8">m<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="8" num="1.8" lm="7" met="7"><w n="8.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="8.2">s<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.5" punct="pt:7">mi<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="7" punct="pt">en</seg></rhyme></w>.</l>
					</lg>
					<div n="1" type="section">
						<head type="main">bis {</head>
						<lg n="1" type="regexp" rhyme="ab">
							<l n="9" num="1.1" lm="7" met="7"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="9.2">n</w>'<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.6">tr<seg phoneme="o" type="vs" value="1" rule="414" place="5">ô</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg></w> <w n="9.8">m<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
							<l n="10" num="1.2" lm="7" met="7"><w n="10.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="10.2">s<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.5" punct="pt:7">mi<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="7" punct="pt">en</seg></rhyme></w>.</l>
						</lg>
						<lg n="2" type="regexp" rhyme="ab">
							<l n="11" num="2.1" lm="7" met="7"><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="11.2">n</w>'<w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.6">tr<seg phoneme="o" type="vs" value="1" rule="414" place="5">ô</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg></w> <w n="11.8">m<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w> </add></subst></l>
							<l n="12" num="2.2" lm="7" met="7"><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="12.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="12.2">s<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.5" punct="pt:7">mi<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="7" punct="pt">en</seg></rhyme></w>.</add></subst></l>
						</lg>
					</div>
				</div></body></text></TEI>