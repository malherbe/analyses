<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE II.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC5" modus="sp" lm_max="8" metProfile="6, (8)" form="suite périodique" schema="3(ababc)">
				<head type="tune">AIR : Flon ! flon ! flon ! larira dondaine !</head>
				<lg n="1" type="quintil" rhyme="ababc">
					<head type="main">LE RÉMOULEUR.</head>
					<l n="1" num="1.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="1.1" punct="vg:2">C<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>s<seg phoneme="œ" type="vs" value="1" rule="406" place="2" punct="vg">eu</seg>rs</w>, <w n="1.2">g<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="1.4" punct="vg:6">p<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="2.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="2.2">n</w>'<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="2.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="2.5">d</w>'<w n="2.6" punct="pv:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">em</seg>pl<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="422" place="6" punct="pv">oi</seg></rhyme></w> ;</l>
					<l n="3" num="1.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="3.1">V<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="351" place="4">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="3.4" punct="vg:6">m<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="4.1"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">O</seg>rd<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="4.3" punct="pt:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>v<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="422" place="6" punct="pt">oi</seg></rhyme></w>.</l>
					<l n="5" num="1.5" lm="8"><w n="5.1" punct="pe:1">Fl<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="pe">on</seg></w> ! <w n="5.2" punct="pe:2">fl<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="pe">on</seg></w> ! <w n="5.3" punct="pe:3">fl<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="pe">on</seg></w> ! <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="5.5" punct="vg:8">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>d<rhyme label="c" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>, <del reason="analysis" hand="LG" type="repetition">etc.</del></l>
				</lg>
				<lg n="2" type="quintil" rhyme="ababc">
					<head type="main">CHARLES.</head>
					<l n="6" num="2.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="6.1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="1">oin</seg>t</w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">p<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">om</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="6.4" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="424" place="5">oû</seg>t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="402" place="6">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="7" num="2.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="7.2">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3" punct="pv:6">r<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>g<seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>d<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="pv">on</seg>s</rhyme></w> ;</l>
					<l n="8" num="2.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="8.2">b<seg phoneme="o" type="vs" value="1" rule="314" place="2">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>p</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">pl<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>r<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="402" place="6">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></w></l>
					<l n="9" num="2.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="9.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="9.3" punct="ps:6">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>l<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="ps">on</seg>s</rhyme></w>…</l>
					<l n="10" num="2.5" lm="8"><w n="10.1" punct="pe:1">Fl<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="pe">on</seg></w> ! <w n="10.2" punct="pe:2">fl<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="pe">on</seg></w> ! <w n="10.3" punct="pe:3">fl<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="pe">on</seg></w> ! <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="10.5" punct="vg:8">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>d<rhyme label="c" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>, <del reason="analysis" hand="LG" type="repetition">etc.</del></l>
				</lg>
				<lg n="3" type="quintil" rhyme="ababc">
					<head type="main">LE RÉMOULEUR.</head>
					<l n="11" num="3.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="11.1" punct="vg:2">C<seg phoneme="o" type="vs" value="1" rule="434" place="1">o</seg>ll<seg phoneme="e" type="vs" value="1" rule="408" place="2" punct="vg">é</seg></w>, <w n="11.2">P<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="11.4">d</w>'<w n="11.5" punct="vg:6"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="12" num="3.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="12.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="12.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s<seg phoneme="o" type="vs" value="1" rule="434" place="3">o</seg>nni<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg>s</w> <w n="12.3" punct="vg:6">d<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>mn<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="vg">é</seg>s</rhyme></w> ,</l>
					<l n="13" num="3.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="13.2">li<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg></w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="414" place="6">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</rhyme></w> ,</l>
					<l n="14" num="3.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="14.1">V<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="14.2">lu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="14.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="14.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg></w> <w n="14.5" punct="dp:6">n<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="dp">ez</seg></rhyme></w> :</l>
					<l n="15" num="3.5" lm="8"><w n="15.1" punct="pe:1">Fl<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="pe">on</seg></w> ! <w n="15.2" punct="pe:2">fl<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="pe">on</seg></w> ! <w n="15.3" punct="pe:3">fl<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="pe">on</seg></w> ! <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="15.5" punct="vg:8">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>d<rhyme label="c" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>, <del reason="analysis" hand="LG" type="repetition">etc.</del></l>
				</lg>
			</div></body></text></TEI>