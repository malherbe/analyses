<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE II.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC6" modus="sm" lm_max="8" metProfile="8" form="suite de distiques" schema="4((aa))">
			<head type="tune">AIR : On dit que je suis sans malice.</head>
				<lg n="1" type="distiques" rhyme="aa…">
					<head type="main">LE RÉMOULEUR.</head>
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J</w>'<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>gu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg></w> <w n="1.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="1.5" punct="vg:8">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>g<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">rʼp<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ssʼr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg></w> <w n="2.3">ch<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="2.5" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>ri<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Jʼ</w> <w n="3.2">r<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg></w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="3.4">h<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>chʼs</w> <w n="3.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="3.6">s<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>p<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>rs</rhyme></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">jʼ</w> <w n="4.3">d<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>nnʼr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg></w> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.5">f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="4.6"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>x</w> <w n="4.7" punct="pt:8">t<seg phoneme="a" type="vs" value="1" rule="306" place="7">a</seg>ill<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pt">eu</seg>rs</rhyme></w>.</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">C<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>mmʼ</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="5.3">th<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg><seg phoneme="a" type="vs" value="1" rule="339" place="4">â</seg>trʼs</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>tʼs</w> <w n="5.6" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>sp<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="6.2">c<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>p</w> <w n="6.3">dʼ</w> <w n="6.4">c<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>s<seg phoneme="o" type="vs" value="1" rule="314" place="4">eau</seg>x</w> <w n="6.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="6.6">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>rʼ</w> <w n="6.7">l<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>rs</w> <w n="6.8" punct="vg:8">pi<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">fʼs<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="7.3">pl<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>s</w> <w n="7.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="4">en</seg></w> <w n="7.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="7.7" punct="vg:8">c<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>s<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>rs</rhyme></w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">tr<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306" place="3">a</seg>illʼr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg></w> <w n="8.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="8.5" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>t<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pt">eu</seg>rs</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>