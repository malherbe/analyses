<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB50" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(ababcdcd)">
	<head type="tune">AIR : Que d'établissements nouveaux.</head>
	<lg n="1" type="huitain" rhyme="ababcdcd">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg></w> <w n="1.4">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="1.5">r<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>g</rhyme></w></l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="2.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="2">en</seg>s</w> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="2.4">t<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="5">en</seg>t</w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.6">pl<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>s</w> <w n="2.7" punct="vg:8">m<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D</w>'<w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="3.3">s<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>t</w> <w n="3.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="3.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="3.7" punct="vg:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">am</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>ll<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="vg">an</seg></rhyme></w>,</l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="4.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">f<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>t</w> <w n="4.4">qu</w>'<w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="4.6"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.7">d<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="4.8" punct="pt:8">pr<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="5.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="5.3" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="5.4">c</w>'<w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="5.6" punct="vg:8">d<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>r<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="8" punct="vg">en</seg>t</rhyme></w>,</l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1">C</w>'<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="6.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="2">oin</seg>s</w> <w n="6.4">f<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.5">qu</w>'<w n="6.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="6.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.8" punct="ps:8">p<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></w>…</l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="7.2" punct="ps:3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="3" punct="ps">eu</seg>rs</w>… <w n="7.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="7.4">g<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="5">en</seg>s</w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="7.6">t<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>l<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="8">en</seg>t</rhyme></w></l>
		<l n="8" num="1.8" lm="8" met="8"><w n="8.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>t</w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r</w> <w n="8.6" punct="pt:8"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>rd<seg phoneme="o" type="vs" value="1" rule="434" place="7">o</seg>nn<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
	</lg>
</div></body></text></TEI>