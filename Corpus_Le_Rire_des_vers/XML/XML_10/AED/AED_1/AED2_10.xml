<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">27, 28 ET 29 JUILLET, TABLEAU ÉPISODIQUE DES TROIS JOURNÉES.</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ARA" sort="1">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<author key="DUV" sort="2">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>495 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
						<author>ARAGO ET DUVERT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=xjVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
								<author>ARAGO ET DUVERT</author>
								<repository>Österreichische Nationalbibliothek</repository>
								<idno type="URI">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886206</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : U+02BC MODIFIER LETTER APOSTROPHE) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<change when="2021-06-14" who="RR">Quelques corrections concernant la distribution du signe signe ʼ (UNICODE : ʼ).</change>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE JOURNÉE.</head><head type="main_subpart">SCÈNE PREMIÈRE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="AED2" modus="cp" lm_max="10" metProfile="8, 4+6" form="suite de strophes" schema="1[ababb] 1[abbba]">
			<head type="tune">AIR : J'avais mis mon petit chapeau.</head>
			<lg n="1" type="regexp" rhyme="ababbabbba">
				<head type="main">COLOMBON.</head>
				<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="1.1">V<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>ge<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="1.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="3">o</seg>s</w> <w n="1.3" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="vg">a</seg>s</w>, <w n="1.4">n<seg phoneme="o" type="vs" value="1" rule="437" place="6">o</seg>s</w> <w n="1.5" punct="pe:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="pe">an</seg>s</rhyme></w> !</l>
				<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="2.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="M">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>ts</w><caesura></caesura> <w n="2.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">â</seg>chʼr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>s</w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="2.7" punct="pe:10">f<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="M">u</seg>t<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="306" place="10">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></w> !</l>
				<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="3.2">vi<seg phoneme="ɛ" type="vs" value="1" rule="383" place="2">ei</seg>llʼ</w> <w n="3.3" punct="vg:3">m<seg phoneme="ɛ" type="vs" value="1" rule="409" place="3" punct="vg">è</seg>rʼ</w>, <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg>s</w> <w n="3.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">in</seg>gt</w>-<w n="3.6">c<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>q</w> <w n="3.7" punct="vg:8"><rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="vg">an</seg>s</rhyme></w>,</l>
				<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">V<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>d</w> <w n="4.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="4.3">s<seg phoneme="o" type="vs" value="1" rule="317" place="3" mp="M">au</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>ssʼ</w><caesura></caesura> <w n="4.4">s<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="P">u</seg>r</w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="4.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg></w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="4.8">lʼ</w> <w n="4.9" punct="pv:10">F<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>rr<rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="306" place="10">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
				<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="5.2">vʼl<seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="5.3">M<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="M">an</seg>g<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" caesura="1">in</seg></w><caesura></caesura> <w n="5.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="5.5">nʼ</w> <w n="5.6">v<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>t</w> <w n="5.7">pl<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>s</w> <w n="5.8">qu</w>'<w n="5.9"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>llʼ</w> <w n="5.10" punct="pt:10">tr<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>v<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="306" place="10">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="4"></space><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="6.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="6.3" punct="vg:5">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>phi<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" punct="vg">on</seg>s</w>, <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>s</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg></rhyme></w></l>
				<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>llʼ</w> <w n="7.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="7.3">s<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="7.4">p<seg phoneme="wa" type="vs" value="1" rule="157" place="6">oê</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="7.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="C">on</seg></w> <w n="7.7" punct="pv:10">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>rv<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
				<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="4"></space><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>llʼ</w> <w n="8.2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="8.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>fr<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>rʼ</w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="8.5" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>c<rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
				<l n="9" num="1.9" lm="8" met="8"><space unit="char" quantity="4"></space><w n="9.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="9.2">qu</w>'<w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="9.4">d<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>mn<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></w> <w n="9.5">pr<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="9.6">dʼ</w> <w n="9.7">p<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>l<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
				<l n="10" num="1.10" lm="8" met="8"><space unit="char" quantity="4"></space><w n="10.1">Vi<seg phoneme="ɛ" type="vs" value="1" rule="365" place="1">e</seg>nnʼ</w> <w n="10.2">lu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="10.3">rʼt<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.5">p<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg></w> <w n="10.6">dʼ</w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="10.8" punct="pt:8">m<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pt">ain</seg></rhyme></w>.</l>
			</lg>
		</div></body></text></TEI>