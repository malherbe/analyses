<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">27, 28 ET 29 JUILLET, TABLEAU ÉPISODIQUE DES TROIS JOURNÉES.</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ARA" sort="1">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<author key="DUV" sort="2">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>495 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
						<author>ARAGO ET DUVERT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=xjVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
								<author>ARAGO ET DUVERT</author>
								<repository>Österreichische Nationalbibliothek</repository>
								<idno type="URI">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886206</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : U+02BC MODIFIER LETTER APOSTROPHE) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<change when="2021-06-14" who="RR">Quelques corrections concernant la distribution du signe signe ʼ (UNICODE : ʼ).</change>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TROISIÈME JOURNÉE</head><head type="main_subpart">SCÈNE V.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="AED24" modus="cp" lm_max="10" metProfile="8, 4+6" form="strophe unique" schema="1(ababcddc)">
			<head type="tune">AIR : J'en guette un petit de mon âge.</head>
				<lg n="1" type="huitain" rhyme="ababcddc">
					<head type="main">ADOLPHE.</head>
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>ls</w> <w n="1.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="1.4">dr<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>ts</w><caesura></caesura> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="P">à</seg></w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="1.7" punct="vg:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>c<seg phoneme="o" type="vs" value="1" rule="434" place="8" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9" mp="M">ai</seg>ss<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="2.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2" mp="P">an</seg>s</w> <w n="2.3">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="2.4">j<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="2.6">gl<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="2.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="2.9" punct="vg:10">d<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>l<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="vg">eu</seg>r</rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="3.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="5">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.5">Fr<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg>t</w> <w n="4.2">r<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>v<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>s<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.4" punct="pt:8">v<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>l<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pt">eu</seg>r</rhyme></w>.</l>
					<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="5.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" mp="C">on</seg></w> <w n="5.3">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="3" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="320" place="4" caesura="1">y</seg>s</w><caesura></caesura> <w n="5.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="C">i</seg>l</w> <w n="5.5">f<seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>t</w> <w n="5.6">qu</w>'<w n="5.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="5.8">t</w>'<w n="5.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="8">en</seg></w> <w n="5.10" punct="pe:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9" mp="M">in</seg>f<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="10">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="1">e</seg>l</w> <w n="6.2" punct="vg:2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" punct="vg">an</seg>g</w>, <w n="6.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ls</w> <w n="6.4">s<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="4" caesura="1">oin</seg>s</w><caesura></caesura> <w n="6.5">n</w>'<w n="6.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="Lp">on</seg>t</w>-<w n="6.7"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>ls</w> <w n="6.8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>s</w> <w n="6.9" punct="pv:10">pr<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>gu<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="pv">é</seg>s</rhyme></w> ;</l>
					<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="4"></space><w n="7.1">S</w>'<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>ls</w> <w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></w> <w n="7.5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="5">oin</seg>s</w> <w n="7.6">d<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>gu<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg>s</rhyme></w></l>
					<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="4"></space><w n="8.1">C</w>'<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="8.3">qu</w>'<w n="8.4"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ls</w> <w n="8.5">n</w>'<w n="8.6"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="8.8">d</w>'<w n="8.9" punct="pt:8"><seg phoneme="y" type="vs" value="1" rule="452" place="6">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>f<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="8">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
		</div></body></text></TEI>