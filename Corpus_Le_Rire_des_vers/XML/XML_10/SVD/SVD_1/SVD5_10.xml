<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">COMMÈRE JEANNETON</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>261 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE VIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD5" modus="cp" lm_max="9" metProfile="8, (9)" form="strophe unique" schema="1(ababccdede)">
				<head type="tune">Air : Dans mon village.</head>
				<lg n="1" type="dizain" rhyme="ababccdede">
					<head type="main">JEANNETON.</head>
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="1.2" punct="vg:4">R<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="3">ain</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <subst hand="LG" reason="analysis" type="repetition"><del>(bis)</del><add rend="hidden"> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5">À</seg></w> <w n="1.4" punct="vg:8">R<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="7">ain</seg>v<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</add></subst></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="2.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3" punct="vg:4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>du<seg phoneme="i" type="vs" value="1" rule="490" place="4" punct="vg">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="2.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="2.5">mʼ</w> <w n="2.6">d<seg phoneme="o" type="vs" value="1" rule="434" place="6">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t</w> <w n="2.7">lʼ</w> <w n="2.8" punct="vg:8">br<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg>s</rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="1">oin</seg></w> <w n="3.2">dʼ</w> <w n="3.3">m</w>'<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="3.5">m<seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>rʼ</w> <w n="3.6">qu</w>'<w n="3.7"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="3.8">b<seg phoneme="ɛ̃" type="vs" value="1" rule="220" place="6">en</seg></w> <w n="3.9" punct="vg:8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>qu<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="484" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>lli<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="4.3">c<seg phoneme="œ" type="vs" value="1" rule="344" place="4">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>r</w> <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="4.5" punct="pt:8">l<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>l<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pt">a</seg>s</rhyme></w>.</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="5.2" punct="vg:4">R<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="3">ain</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <subst hand="LG" reason="analysis" type="repetition"><del>(bis)</del><add rend="hidden"> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5">À</seg></w> <w n="5.4" punct="vg:8">R<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="7">ain</seg>v<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</add></subst></l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="6.2" punct="vg:4">R<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="3">ain</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <subst hand="LG" reason="analysis" type="repetition"><del>(bis)</del><add rend="hidden"> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5">À</seg></w> <w n="6.4" punct="vg:8">R<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="7">ain</seg>v<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</add></subst></l>
					<l n="7" num="1.7" lm="9"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="7.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3">mʼn<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="7.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4" mp="P">an</seg>s</w> <w n="7.5">pr<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>r</w> <w n="7.6">l</w>'<w n="7.7" punct="vg:9"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>n<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="9" punct="vg">i</seg>r</rhyme></w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="8.2">s</w>'<w n="8.3"><seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="8.5">s<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="8.6" punct="vg:8">f<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>c<rhyme label="e" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="9" num="1.9" lm="8" met="8"><w n="9.1">Jʼ</w> <w n="9.2">c<seg phoneme="o" type="vs" value="1" rule="434" place="1">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s</w> <w n="9.3">lʼ</w> <w n="9.4" punct="ps:4">b<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="4" punct="ps">eu</seg>r</w>… <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="9.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.7">rʼp<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>t<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>r</rhyme></w></l>
					<l n="10" num="1.10" lm="8" met="8"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="10.2" punct="vg:4">R<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="3">ain</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <subst hand="LG" reason="analysis" type="repetition"><del>(bis)</del><add rend="hidden"> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5">À</seg></w> <w n="10.4" punct="vg:8">R<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="7">ain</seg>v<rhyme label="e" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</add></subst></l>
				</lg>
			</div></body></text></TEI>