<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANGÉLIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIEME.</head><head type="main_subpart">SCÈNE PREMIÈRE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD27" modus="cp" lm_max="12" metProfile="5, 8, 12" form="strophe unique" schema="1(aabccb)">
			<lg n="1" type="sizain" rhyme="aabccb">
				<head type="main">CHŒUR.</head>
				<l n="1" num="1.1" lm="5" met="5"><space unit="char" quantity="14"></space><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="1.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="3">e</seg>l</w> <w n="1.4" punct="pe:5">b<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>nh<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="5" punct="pe">eu</seg>r</rhyme></w> ! <del hand="LG" reason="analysis" type="repetition">etc.</del></l>
				<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="2.5" punct="pe:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pe">eu</seg>r</rhyme></w> ! </add></subst></l>
				<l n="3" num="1.3" lm="12" mp7="Fc"><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M/mp">A</seg>m<seg phoneme="y" type="vs" value="1" rule="449" place="2" mp="M/mp">u</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="Lp">on</seg>s</w>-<w n="3.2" punct="vg:4">n<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg">ou</seg>s</w>, <w n="3.3">c<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r</w> <w n="3.4">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="Fc">e</seg></w> <w n="3.5">j<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="10">e</seg>st</w> <w n="3.7" punct="pv:12">s<seg phoneme="y" type="vs" value="1" rule="449" place="11" mp="M">u</seg>p<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="12">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ; </add></subst></l>
				<l n="4" num="1.4" lm="5" met="5"><space unit="char" quantity="14"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="4.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">f<seg phoneme="œ" type="vs" value="1" rule="405" place="2">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>p<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s</rhyme></w></add></subst></l>
				<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="8"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="5.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>d</w> <w n="5.2">l</w>'<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>r</w> <w n="5.4">p<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>r</w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.7">g<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="5.8" punct="pv:8">fr<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pv">ai</seg>s</rhyme></w> ; </add></subst></l>
				<l n="6" num="1.6" lm="12" mp6="Pem"><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r</w> <w n="6.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="5">è</seg>s</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="6.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="6.7"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="8">e</seg>st</w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>s</w> <w n="6.9">s<seg phoneme="y" type="vs" value="1" rule="449" place="11" mp="P">u</seg>r</w> <w n="6.10">l</w>'<w n="6.11" punct="pt:12">h<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="12">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>. </add></subst></l>
			</lg>
		</div></body></text></TEI>