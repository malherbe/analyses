<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANGÉLIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE VIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD24" modus="sp" lm_max="6" metProfile="6, 4" form="suite de strophes" schema="2[abab] 2[aa]">
				<head type="tune">Air : En ce tems-là (de la Fiancée.)</head>
				<lg n="1" type="regexp" rhyme="a">
					<head type="main">JEANNETON.</head>
					<l n="1" num="1.1" lm="6" met="6"><w n="1.1" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>c<seg phoneme="e" type="vs" value="1" rule="346" place="3" punct="vg">ez</seg></w>, <w n="1.2">j<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.3" punct="pe:6">f<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></w> !</l>
					</lg>
				<lg n="2" type="regexp" rhyme="b">
					<head type="main">GRATIEN.</head>
					<l n="2" num="2.1" lm="6" met="6"><w n="2.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>ls</w> <w n="2.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="2.3">v<seg phoneme="o" type="vs" value="1" rule="437" place="3">o</seg>s</w> <w n="2.4" punct="pi:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="y" type="vs" value="1" rule="449" place="6" punct="pi">u</seg>s</rhyme></w> ?</l>
					</lg>
				<lg n="3" type="regexp" rhyme="a">
					<head type="main">ANGÉLIQUE.</head>
					<l n="3" num="3.1" lm="6" met="6"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">n</w>'<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg></w> <w n="3.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="3">en</seg></w> <w n="3.5">qu</w>' <w n="3.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="3.7" punct="ps:6"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>g<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɥi" type="vs" value="1" rule="273" place="6">ui</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="ps">e</seg></rhyme></w> …</l>
					</lg>
				<lg n="4" type="regexp" rhyme="">
					<head type="main">JEANNETON.</head>
					<l part="I" n="4" num="4.1" lm="6" met="6"><w n="4.1">V<seg phoneme="o" type="vs" value="1" rule="437" place="1">o</seg>s</w> <w n="4.2" punct="pi:3">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="3" punct="pi">en</seg>s</w> ? </l>
					</lg>
				<lg n="5" type="regexp" rhyme="baba">
					<head type="main">ANGÉLIQUE.</head>
					<l part="F" n="4" lm="6" met="6">…<w n="4.3">Jʼ</w> <w n="4.4">n</w>'<w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg></w> <w n="4.7" punct="ps:6">pl<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="y" type="vs" value="1" rule="449" place="6" punct="ps">u</seg>s</rhyme></w>…</l>
					<l n="5" num="5.1" lm="6" met="6"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">n</w>'<w n="5.3"><seg phoneme="y" type="vs" value="1" rule="390" place="2">eu</seg>s</w> <w n="5.4">s<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>r</w> <w n="5.5">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.6">t<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="6" num="5.2" lm="6" met="6"><w n="6.1">Qu</w>'<w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="6.3">s<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>l</w> <w n="6.4">c<seg phoneme="œ" type="vs" value="1" rule="248" place="3">œu</seg>r</w> <w n="6.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="6.6">m</w>'<w n="6.7" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="5">ai</seg>m<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="vg">a</seg></rhyme></w>,</l>
					<l n="7" num="5.3" lm="6" met="6"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="2">o</seg>r</w> <w n="7.2">c</w>'<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="7.5" punct="pt:6">m<seg phoneme="i" type="vs" value="1" rule="492" place="5">y</seg>st<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
				<lg n="6" type="regexp" rhyme="b">
					<head type="main">JEANNETON.</head>
					<l n="8" num="6.1" lm="6" met="6"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>xpl<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>qu<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w>-<w n="8.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="8.3" punct="pi:6">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="pi">a</seg></rhyme></w> ?</l>
				</lg>

				<div n="1" type="section">
					<head type="main">(bis.) {</head>
					<lg n="1" type="regexp" rhyme="aaaa">
						<head type="main">ANGÉLIQUE, montrant Jules.</head>
						<l n="9" num="1.1" lm="4" met="4"><space unit="char" quantity="4"></space><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="9.3" punct="ps:4">l<rhyme label="a" id="5" gender="m" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="341" place="4" punct="ps">à</seg></rhyme></w> …</l>
						<l n="10" num="1.2" lm="6" met="6"><w n="10.1">M<seg phoneme="œ" type="vs" value="1" rule="150" place="1">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="2">eu</seg>r</w> <w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.4" punct="pe:6">d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>r<rhyme label="a" id="5" gender="m" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="pe">a</seg></rhyme></w> !</l>
						<l n="11" num="1.3" lm="4" met="4"><space unit="char" quantity="4"></space><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="11.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="11.3" punct="ps:4">l<rhyme label="a" id="6" gender="m" type="a" stanza="4"><seg phoneme="a" type="vs" value="1" rule="341" place="4" punct="ps">à</seg></rhyme></w> … </add></subst></l>
						<l n="12" num="1.4" lm="6" met="6"><subst reason="analysis" type="repetition" hand="LG"><del></del><add rend="hidden"><w n="12.1">M<seg phoneme="œ" type="vs" value="1" rule="150" place="1">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="2">eu</seg>r</w> <w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.4" punct="pe:6">d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>r<rhyme label="a" id="6" gender="m" type="e" stanza="4"><seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="pe">a</seg></rhyme></w> !</add></subst></l>
					</lg>
				</div>
			</div></body></text></TEI>