<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANGÉLIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIEME.</head><head type="main_subpart">SCÈNE II.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD28" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="1[abab] 2[aa]">
				<head type="tune">AIR nouveau.</head>
				<lg n="1" type="regexp" rhyme="a">
					<head type="main">JULES, se frappant le front.</head>
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.5">su<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg>s</w> <w n="1.6">s<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>r</w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="1.8" punct="pt:8">r<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="regexp" rhyme="b">
					<head type="main">ANGELIQUE.</head>
					<l n="2" num="2.1" lm="8" met="8"><w n="2.1">V<seg phoneme="wa" type="vs" value="1" rule="439" place="1">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="2.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">b<seg phoneme="o" type="vs" value="1" rule="314" place="4">eau</seg></w> <w n="2.4" punct="ps:6">m<seg phoneme="wa" type="vs" value="1" rule="439" place="5">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="362" place="6" punct="ps">en</seg></w> … <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="132" place="7">e</seg>h</w> <w n="2.6" punct="pe:8">qu<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="wa" type="vs" value="1" rule="280" place="8" punct="pe">oi</seg></rhyme></w> !</l>
					</lg>
				<lg n="3" type="regexp" rhyme="a">
					<head type="main">JULES.</head>
					<l n="3" num="3.1" lm="8" met="8"><w n="3.1">C</w>'<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="6">en</seg>t</w> <w n="3.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</w> <w n="3.6" punct="pe:8">d<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					</lg>
				<lg n="4" type="regexp" rhyme="b">
					<head type="main">ANGÉLIQUE.</head>
					<l n="4" num="4.1" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="132" place="1">E</seg>h</w> <w n="4.2" punct="ps:2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="2" punct="ps">en</seg></w> … <w n="4.3" punct="vg:4">m<seg phoneme="œ" type="vs" value="1" rule="150" place="3">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="4" punct="vg">eu</seg>r</w>, <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w>-<w n="4.5" punct="pe:8">m<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pe ps">oi</seg></rhyme></w> !…</l>
					</lg>
				<lg n="5" type="regexp" rhyme="a">
					<head type="main">JULES.</head>
					<l n="5" num="5.1" lm="8" met="8"><w n="5.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1" punct="vg">ai</seg>s</w>, <w n="5.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="5.3">dʼ</w> <w n="5.4">l</w>'<w n="5.5"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4">en</seg>t</w> <w n="5.6">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="5.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="5.8" punct="pt:8">v<seg phoneme="wa" type="vs" value="1" rule="439" place="7">o</seg>y<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
				<lg n="6" type="regexp" rhyme="a">
					<head type="main">ANGÉLIQUE.</head>
					<l n="6" num="6.1" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="6.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="422" place="2" punct="vg">oi</seg></w>, <w n="6.3">f<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>t</w> <w n="6.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5">jʼ</w> <w n="6.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>dʼ</w> <w n="6.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="6.8" punct="pt:8"><seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>vr<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
				<lg n="7" type="regexp" rhyme="aa">
					<head type="main">ANGÉLIQUE ET JULES.</head>
					<l n="7" num="7.1" lm="8" met="8"><w n="7.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="vg">on</seg></w>, <w n="7.2" punct="vg:2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg></w>, <w n="7.3" punct="vg:3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="vg">on</seg></w>, <w n="7.4" punct="vg:4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg">on</seg></w>, <w n="7.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.6">n</w>'<w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="7.8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>s</w> <w n="7.9" punct="vg:8">ç<rhyme label="a" id="4" gender="m" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg></rhyme></w>,</l>
					<l n="8" num="7.2" lm="8" met="8"><w n="8.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>r</w> <w n="8.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.4">m<seg phoneme="wa" type="vs" value="1" rule="439" place="6">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="362" place="7">en</seg></w>-<w n="8.5" punct="pt:8">l<rhyme label="a" id="4" gender="m" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="341" place="8" punct="pt">à</seg></rhyme></w>.</l>
			</lg>
		</div></body></text></TEI>