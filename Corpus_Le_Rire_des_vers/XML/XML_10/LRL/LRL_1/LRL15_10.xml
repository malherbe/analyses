<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL15" modus="cp" lm_max="10" metProfile="7, 6, (4+6), (8)" form="strophe unique" schema="1(ababcdcd)">
	<head type="tune">AIR : L'or est une chimère.</head>
	<lg n="1" type="huitain" rhyme="ababcdcd">
		<l n="1" num="1.1" lm="7" met="7"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="1.3">gr<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.4">st<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>t<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="456" place="7">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></w></l>
		<l n="2" num="1.2" lm="7" met="7"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="2.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>t</w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4">t<seg phoneme="i" type="vs" value="1" rule="492" place="4">y</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="2.6">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="2.7" punct="pv:7">li<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="7" punct="pv">eu</seg>x</rhyme></w> ;</l>
		<l n="3" num="1.3" lm="7" met="7"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="3.2">m<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>lʼs</w> <w n="3.3">c<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="3.5" punct="vg:7">r<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="456" place="7">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></w>,</l>
		<l n="4" num="1.4" lm="7" met="7"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">d<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="4.3">c<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>s</w> <w n="4.5" punct="pe:7"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="397" place="7" punct="pe">eu</seg>x</rhyme></w> !</l>
		<l n="5" num="1.5" lm="6" met="6"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="5.3">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="5.4">f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>g<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="447" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></w></l>
		<l n="6" num="1.6" lm="8"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>cc<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rt</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="6.3">f<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="6.5" punct="vg:8">b<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>d<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="317" place="8" punct="vg">au</seg>ds</rhyme></w>,</l>
		<l n="7" num="1.7" lm="6" met="6"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="7.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="7.3">d</w>'<w n="7.4">c<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>c<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>t<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></w></l>
		<l n="8" num="1.8" lm="10" met="4+6" met_alone="True"><w n="8.1">J</w>'<w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">en</seg></w> <w n="8.3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="8.4">d</w>'<w n="8.5">f<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>m<seg phoneme="ø" type="vs" value="1" rule="402" place="4" caesura="1">eu</seg>sʼs</w><caesura></caesura> <w n="8.6">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="8.7">s</w>'<w n="8.8"><seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg>tʼnt</w> <w n="8.9"><seg phoneme="o" type="vs" value="1" rule="317" place="8" mp="C">au</seg>x</w> <w n="8.10" punct="pt:10">c<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>rr<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="314" place="10" punct="pt">eau</seg>x</rhyme></w>.</l>
		<l ana="unanalyzable" n="9" num="1.9">De mes grotesques statues, etc.</l> 
	</lg>
</div></body></text></TEI>