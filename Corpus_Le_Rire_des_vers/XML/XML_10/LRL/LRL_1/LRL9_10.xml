<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL9" modus="sp" lm_max="8" metProfile="4, 6, 8">
	<head type="tune">AIR de Va-de-bon-Cœur.</head>
	<lg n="1">
	<head type="main">PREMIER COUPLET.</head> 
		<l n="1" num="1.1" lm="4" met="4"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="1.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="1.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w></l>
		<l n="2" num="1.2" lm="4" met="4"><w n="2.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="2.3">n</w>'<w n="2.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rt</w> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w></l>
		<l n="3" num="1.3" lm="6" met="6"><w n="3.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="3.2">qu</w>'<w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="3.4">br<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="3.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="3.6" punct="pv:6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></w> ;</l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3">s<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>l</w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="4.5" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>v<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg></w>,</l>
		<l n="5" num="1.5" lm="6" met="6"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="5.4">l</w>'<w n="5.5" punct="pv:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="358" place="5">en</seg>nʼm<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="pv">i</seg></w> ;</l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="6.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="y" type="vs" value="1" rule="452" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4">b<seg phoneme="ɔ" type="vs" value="1" rule="418" place="5">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg></w></l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg></w> <w n="7.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r</w> <w n="7.3"><seg phoneme="y" type="vs" value="1" rule="452" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4" punct="vg:8">s<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>scr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>pt<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="vg">on</seg></w>,</l>
		<l n="8" num="1.8" lm="6" met="6"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="8.3">s<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>r</w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="8.5" punct="vg:6">l<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
		<l n="9" num="1.9" lm="4" met="4"><w n="9.1">C</w>'<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="9.4" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></w> !</l>
	</lg>
	<lg n="2">
	<head type="main">DEUXIÈME COUPLET.</head>
		<l n="10" num="2.1" lm="8" met="8"><w n="10.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="10.3">p<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></w></l>
		<l n="11" num="2.2" lm="6" met="6"><w n="11.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="11.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg></w> <w n="11.3" punct="pv:6">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></w> ;</l>
		<l n="12" num="2.3" lm="8" met="8"><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="12.2">d<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="12.4">c<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>br<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></w></l>
		<l n="13" num="2.4" lm="6" met="6"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="13.3" punct="pv:6">d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="pv">é</seg></w> ;</l>
		<l n="14" num="2.5" lm="8" met="8"><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="14.2">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg></w> <w n="14.4">p<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>t<seg phoneme="o" type="vs" value="1" rule="434" place="7">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></w></l>
		<l n="15" num="2.6" lm="8" met="8"><w n="15.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="15.2">s<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>ts</w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4">e</seg>c</w> <w n="15.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="15.5">pi<seg phoneme="e" type="vs" value="1" rule="240" place="6">e</seg>d</w> <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.7" punct="vg:8">n<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="vg">é</seg></w>,</l>
		<l n="16" num="2.7" lm="6" met="6"><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="16.5" punct="vg:6">p<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
		<l n="17" num="2.8" lm="4" met="4"><w n="17.1">C</w>'<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="17.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="17.4" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></w> !</l>
	</lg>
</div></body></text></TEI>