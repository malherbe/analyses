<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL20" modus="cp" lm_max="10" metProfile="8, 4+6" form="strophe unique" schema="1(ababbcdcd)">
	<head type="tune">AIR : Ce Magistrat irréprochable.</head>
	<lg n="1" type="neuvain" rhyme="ababbcdcd">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>str<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>t</w> <w n="1.3" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>rr<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>ch<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L</w>'<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="169" place="1">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="2.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t</w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="2.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>b<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="8" punct="vg">u</seg>s</rhyme></w>,</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2" punct="vg:2">d<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg>t</w>, <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4">e</seg>c</w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>r</w> <w n="3.6" punct="dp:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>ff<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
		<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1" mp="M">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="4.2">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">â</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" caesura="1">on</seg>s</w><caesura></caesura> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="4.4">n</w>'<w n="4.5"><seg phoneme="i" type="vs" value="1" rule="496" place="6">y</seg></w> <w n="4.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="9">ez</seg></w> <w n="4.7" punct="pe:10">pl<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="449" place="10" punct="pe ps">u</seg>s</rhyme></w> ! …</l>
		<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg></w> <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="C">ou</seg>s</w> <w n="5.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">ai</seg>s</w><caesura></caesura> <w n="5.5">m<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="5.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg></w> <w n="5.8" punct="pe:10">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>ss<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="10" punct="pe">u</seg>s</rhyme></w> !</l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.2">M<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">in</seg></w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="6.4" punct="vg:8">d<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>v<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="466" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="7.2">h<seg phoneme="ɔ" type="vs" value="1" rule="418" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="7.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="7.6" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>gr<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>m<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="pv">en</seg>t</rhyme></w> ;</l>
		<l n="8" num="1.8" lm="8" met="8"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="8.3">t<seg phoneme="a" type="vs" value="1" rule="306" place="3">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <hi rend="ital"><w n="8.5">V<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>ct<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>r<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="466" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></hi></l>
		<l n="9" num="1.9" lm="8" met="8"><w n="9.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.3" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="8" punct="pt">en</seg>t</rhyme></w>. <del reason="analysis" type="repetition" hand="KL">(bis.)</del></l>
	</lg>
</div></body></text></TEI>