<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL16" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="1[abbaa] 1[abacbc]">
	<head type="tune">AIR : L'hymen est un lien charmant.</head>
	<lg n="1" type="regexp" rhyme="abba">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>spr<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t</w> <w n="1.2" punct="vg:4">l<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="vg">er</seg></w>, <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>r</w> <w n="1.4" punct="vg:8">f<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>c<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="vg">on</seg>d</rhyme></w>,</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="2.2">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="2">en</seg>s</w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>rd</w>'<w n="2.4" punct="vg:5">hu<seg phoneme="i" type="vs" value="1" rule="490" place="5" punct="vg">i</seg></w>, <w n="2.5">s<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>r</w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="2.7" punct="vg:8">sc<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">sc<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ptr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">M<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>lp<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>m<rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="4.3">gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="o" type="vs" value="1" rule="437" place="4">o</seg>ts</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="4.6" punct="pe:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="pe">on</seg></rhyme></w> !</l>
	</lg>
	<lg n="2" type="regexp" rhyme="aabacbc">
		<head type="speaker">GUSTAVE.</head>
		<l n="5" num="2.1" lm="8" met="8"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>l</w> <w n="5.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">m</w>'<w n="5.5"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="5.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="5">oin</seg>t</w> <w n="5.7">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="5.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.9" punct="pe:8">d<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="pe">on</seg></rhyme></w> !</l>
		<l n="6" num="2.2" lm="8" met="8"><w n="6.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1" punct="vg">ai</seg>s</w>, <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">S<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>ph<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.4"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg></w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.6" punct="vg:8">Pr<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>p<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
		<l n="7" num="2.3" lm="8" met="8"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="7.2">l</w>'<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t</w> <w n="7.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.5">m</w>'<w n="7.6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="7.7">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="6">oin</seg>t</w> <w n="7.8" punct="pv:8">cr<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg><rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pv">é</seg></rhyme></w> ;</l>
		<l n="8" num="2.4" lm="8" met="8"><w n="8.1" punct="vg:1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1" punct="vg">i</seg></w>, <w n="8.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="2">ein</seg></w> <w n="8.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="8.4">f<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg></w> <w n="8.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="8.6">qu</w>'<w n="8.7"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>l</w> <w n="8.8" punct="vg:8">v<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
		<l n="9" num="2.5" lm="8" met="8"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>p<seg phoneme="o" type="vs" value="1" rule="434" place="2">o</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="9.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s</w> <w n="9.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.5" punct="vg:8">b<rhyme label="c" id="5" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
		<l n="10" num="2.6" lm="8" met="8"><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="10.2">m<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="10.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.4">su<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg>s</w> <w n="10.5">l</w>'<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>gr<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg><rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></rhyme></w></l>
		<l n="11" num="2.7" lm="8" met="8"><w n="11.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="1">è</seg>s</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">tr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>b<seg phoneme="y" type="vs" value="1" rule="452" place="4">u</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>l</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.5" punct="pt:8">c<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>mm<rhyme label="c" id="5" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
	</lg>
</div></body></text></TEI>