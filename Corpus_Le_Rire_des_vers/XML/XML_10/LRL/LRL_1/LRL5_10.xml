<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL5" modus="sp" lm_max="8" metProfile="5, 8, (4)" form="suite de strophes" schema="1(aabbba) 1(aabccb)">
	<head type="tune">AIR de M. Sans-Gêne.</head>
	<lg n="1" type="sizain" rhyme="aabbba">
		<l n="1" num="1.1" lm="5" met="5"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="1.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="2">om</seg></w> <w n="1.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="1.4" punct="vg:5">c<seg phoneme="o" type="vs" value="1" rule="434" place="4">o</seg>ll<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></w>,</l>
		<l n="2" num="1.2" lm="5" met="5"><w n="2.1">C</w>'<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.4">pr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w></l>
		<l n="3" num="1.3" lm="5" met="5"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">v<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>s</w> <w n="3.3">vi<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="3.4" punct="pe:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>f<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5" punct="pe">an</seg>s</rhyme></w> !</l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="4.3">f<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="4.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="4.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="4.7">t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">em</seg>ps</rhyme></w></l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1">L<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>rs</w> <w n="5.2">c<seg phoneme="œ" type="vs" value="1" rule="248" place="2">œu</seg>rs</w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t</w> <w n="5.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>rs</w> <w n="5.5">qu<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><rhyme label="b" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8">an</seg>s</rhyme></w></l>
		<l n="6" num="1.6" lm="4"><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg></w> <w n="6.3" punct="pt:4">c<seg phoneme="o" type="vs" value="1" rule="434" place="3">o</seg>ll<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></rhyme></w>.</l>
	</lg> 
	<lg n="2" type="sizain" rhyme="aabccb">
	<head type="speaker">DUBOURG, attendri.</head>
	  <l n="7" num="2.1" lm="8" met="8"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="7.2">c<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>lt<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="7.3">v<seg phoneme="o" type="vs" value="1" rule="437" place="5">o</seg>s</w> <w n="7.4">j<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="7.5"><rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8">an</seg>s</rhyme></w></l>
	  <l n="8" num="2.2" lm="8" met="8"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="8.2">f<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>t</w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>b<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="8.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>g</w>-<w n="8.5" punct="pv:8">t<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8" punct="pv">em</seg>ps</rhyme></w> ;</l>
	  <l n="9" num="2.3" lm="8" met="8"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="9.2">b<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r</w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="9.4">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t</w> <w n="9.6" punct="vg:8">b<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
	  <l n="10" num="2.4" lm="8" met="8"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="10.2" punct="vg:2">s<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg></w>, <w n="10.3" punct="vg:4">j<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>s</w>, <w n="10.4">j</w>'<w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg></w> <w n="10.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="10.7" punct="vg:8">l<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>s<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg>r</rhyme></w>,</l>
	  <l n="11" num="2.5" lm="8" met="8"><w n="11.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg></w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="11.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">am</seg>ps</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">l</w>'<w n="11.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg>r</rhyme></w>,</l>
	  <l n="12" num="2.6" lm="8" met="8"><w n="12.1">C</w>'<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>rd</w>'<w n="12.4">hu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="12.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.7" punct="pt:8">m<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>ss<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
	</lg>
</div></body></text></TEI>