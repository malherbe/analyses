<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA MÉTEMPSYCOSE</title>
				<title type="sub">BÊTISE EN UN ACTE, MÊLÉE DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="COU" sort="1">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<author key="JAI" sort="2">
					<name>
						<forename>Ernest</forename>
						<surname>JAIME</surname>
					</name>
					<date from="1804" to="1884">1804-1884</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>282 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FCR_1</idno>	
				<availability status="free">
					<licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA MÉTEMPSYCOSE</title>
						<author>FRÉDÉRIC DE COURCY ET JAIME</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=c1RoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032819917.0x000001# ?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">7 FÉVRIER 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FCR5" modus="cp" lm_max="9" metProfile="8, (9)" form="strophe unique" schema="1(ababcdcd)">
	<head type="tune">AIR : Vaudeville du Traité de Paix.</head>
	<lg n="1" type="huitain" rhyme="ababcdcd">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3" punct="vg:4">m<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" punct="vg">en</seg>t</w>, <w n="1.4">ç<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="1.5">n</w>' <w n="1.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.7">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t</w> <w n="1.8" punct="pv:8">ri<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="8" punct="pv">en</seg></rhyme></w> ;</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="2.2">ç<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="2.3">m</w>' <w n="2.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>bl</w>' <w n="2.5">d</w>'<w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="2.7">m<seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s</w> <w n="2.8" punct="vg:8">pr<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>s<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w>-<w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="3.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="3.6">bi<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="8">en</seg></rhyme></w></l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">s<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="381" place="3">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="4.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="4.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>tr</w>' <w n="4.5" punct="pt:8">m<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>n<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">b<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="3">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="5.4">j</w>' <w n="5.5">v<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="5.6">l</w>' <w n="5.7" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>rn<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="vg">er</seg></rhyme></w>,</l>
		<l n="6" num="1.6" lm="9"><w n="6.1">Ç<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="6.2">m</w>' <w n="6.3">d<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>nn</w>' <w n="6.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="304" place="3" mp="M">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4">en</seg>t</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="6.6">l</w>'<w n="6.7" punct="ps:9"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6" mp="M">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="7" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>t<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="9">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="ps" mp="F">e</seg></rhyme></w>…</l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="7.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>t</w> <w n="7.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="7.5" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="304" place="7">aî</seg>n<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="vg">er</seg></rhyme></w>,</l>
		<l n="8" num="1.8" lm="8" met="8"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="8.3">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">l</w>'<w n="8.6" punct="pt:8">h<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
	</lg>
</div></body></text></TEI>