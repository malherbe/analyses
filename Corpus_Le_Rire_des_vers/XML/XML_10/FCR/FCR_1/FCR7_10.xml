<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA MÉTEMPSYCOSE</title>
				<title type="sub">BÊTISE EN UN ACTE, MÊLÉE DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="COU" sort="1">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<author key="JAI" sort="2">
					<name>
						<forename>Ernest</forename>
						<surname>JAIME</surname>
					</name>
					<date from="1804" to="1884">1804-1884</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>282 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FCR_1</idno>	
				<availability status="free">
					<licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA MÉTEMPSYCOSE</title>
						<author>FRÉDÉRIC DE COURCY ET JAIME</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=c1RoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032819917.0x000001# ?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">7 FÉVRIER 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FCR7" modus="cp" lm_max="10" metProfile="8, 4+6" form="strophe unique" schema="1(ababcdcd)">
	<head type="tune">AIR Du partage de la richesse.</head>
	<lg n="1" type="huitain" rhyme="ababcdcd">
		<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="C">Au</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">an</seg>g</w><caesura></caesura> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="1.5" punct="vg:7">s<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" punct="vg">an</seg>t</w>, <w n="1.6">l</w>'<w n="1.7" punct="vg:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="8" mp="M">im</seg>b<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>c<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
		<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">V<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="2.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">pl<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>c<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="P">a</seg>r</w> <w n="2.5">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="6" mp="C">e</seg>t</w> <w n="2.6" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="10" punct="pv">en</seg>t</rhyme></w> ;</l>
		<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="3.2">pl<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s</w> <w n="3.3">d</w>'<w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="3.5" punct="vg:4">s<seg phoneme="o" type="vs" value="1" rule="437" place="4" punct="vg" caesura="1">o</seg>t</w>,<caesura></caesura> <w n="3.6" punct="tc:5">tr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="5" punct="ti">è</seg>s</w>—<w n="3.7">c<seg phoneme="o" type="vs" value="1" rule="434" place="6" mp="M">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="3.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" mp="P">an</seg>s</w> <w n="3.9">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="3.10" punct="vg:10">v<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
		<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">Pr<seg phoneme="o" type="vs" value="1" rule="443" place="1" mp="M">o</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg></w><caesura></caesura> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="4.3">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="6" mp="C">e</seg>t</w> <w n="4.4" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="M">an</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="10" punct="pt">en</seg>t</rhyme></w>.</l>
		<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">L</w>'<w n="5.2">h<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="5.3">d</w>'<w n="5.4" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="5.5">s</w>'<w n="5.6"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>ff<seg phoneme="y" type="vs" value="1" rule="449" place="6" mp="M">u</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t</w> <w n="5.7">d</w>'<w n="5.8"><seg phoneme="y" type="vs" value="1" rule="452" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.9" punct="vg:10">t<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="10">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
		<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">V<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="6.2">s<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="P">ou</seg>s</w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="6.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="M">ou</seg>ff<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="6.6">s<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="6.7" punct="vg:10">g<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9" mp="M">aî</seg>t<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="vg">é</seg></rhyme></w>,</l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1" punct="vg:1">Br<seg phoneme="ɛ" type="vs" value="1" rule="345" place="1" punct="vg">e</seg>f</w>, <w n="7.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>t</w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="7.6">l</w>'<w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>r</w> <w n="7.8" punct="vg:8">b<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
		<l n="8" num="1.8" lm="8" met="8"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="8.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r</w> <w n="8.4">l</w>'<w n="8.5" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pt">é</seg></rhyme></w>. <del reason="analysis" type="repetition" hand="KL">( bis.)</del></l>
	</lg>
</div></body></text></TEI>