<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">À PROPOS PATRIOTIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="VIL" sort="1">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="MAS" sort="2">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>273 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">VEM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>À propos patriotique</title>
						<author>VILLENEUVE ET MASSON</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=0FA6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>À propos patriotique</title>
								<author>VILLENEUVE ET MASSON</author>
								<repository>Bayerische Staatsbibliothek</repository>
								<idno type="URI">https://opacplus.bsb-muenchen.de/title/BV008031366</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_subpart">SCÈNE PREMIÈRE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="VEM1" modus="sp" lm_max="6" metProfile="3, 6" form="strophe unique" schema="1(abbacddcc)">
				<head type="tune">AIR : Garde à vous.</head>
				<lg n="1" type="neuvain" rhyme="abbacddcc">
					<head type="main">AUGUSTE.</head>
					<l n="1" num="1.1" lm="3" met="3"><space unit="char" quantity="6"></space><w n="1.1">G<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="1.3" punct="vg:3">n<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="vg">ou</seg>s</rhyme></w>,</l>
					<l n="2" num="1.2" lm="6" met="6"><w n="2.1">Lʼ</w> <w n="2.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="2.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="2.5" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>t<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="6" met="6"><w n="3.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="3.2">dr<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>ts</w> <w n="3.3">qu</w>'<w n="3.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="3.5">v<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>t</w> <w n="3.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="3.7">pr<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="6" met="6"><w n="4.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="4.3">d<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s</w> <w n="4.4">t<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</rhyme></w></l>
					<l n="5" num="1.5" lm="3" met="3"><space unit="char" quantity="6"></space><w n="5.1">G<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="5.3" punct="pt:3">n<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="pt">ou</seg>s</rhyme></w>.</l>
					<l n="6" num="1.6" lm="6" met="6"><w n="6.1" punct="vg:2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2" punct="vg">ai</seg>s</w>, <w n="6.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="6.4" punct="pv:6"><rhyme label="d" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg>s</rhyme></w> ;</l>
					<l n="7" num="1.7" lm="6" met="6"><w n="7.1" punct="vg:2">F<seg phoneme="a" type="vs" value="1" rule="192" place="1">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="7.2">g<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="7.3">v<seg phoneme="o" type="vs" value="1" rule="437" place="5">o</seg>s</w> <w n="7.4">l<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></w></l>
					<l n="8" num="1.8" lm="6" met="6"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="8.2">pl<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="8.3">v<seg phoneme="o" type="vs" value="1" rule="437" place="4">o</seg>s</w> <w n="8.4" punct="ps:6"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>p<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="ps">ou</seg>x</rhyme></w> …</l>
					<l n="9" num="1.9" lm="6" met="6"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s</w> <w n="9.4" punct="pt:6">t<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="pt">ou</seg>s</rhyme></w>.</l>
				</lg>
				<p>(Le chœur reprend.)</p>
			</div></body></text></TEI>