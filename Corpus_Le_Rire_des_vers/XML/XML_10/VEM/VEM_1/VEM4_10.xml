<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">À PROPOS PATRIOTIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="VIL" sort="1">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="MAS" sort="2">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>273 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">VEM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>À propos patriotique</title>
						<author>VILLENEUVE ET MASSON</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=0FA6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>À propos patriotique</title>
								<author>VILLENEUVE ET MASSON</author>
								<repository>Bayerische Staatsbibliothek</repository>
								<idno type="URI">https://opacplus.bsb-muenchen.de/title/BV008031366</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_subpart">SCÈNE PREMIÈRE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="VEM4" modus="cp" lm_max="9" metProfile="8, 4÷5" form="strophe unique" schema="1(ababbcdcd)">
				<head type="tune">AIR : Restez, restez troupe jolie.</head>
				<lg n="1" type="neuvain" rhyme="ababbcdcd">
					<head type="main">Auguste.</head>
					<l n="1" num="1.1" lm="9" mp5="F" met="4−5"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1" mp="P">an</seg>s</w> <w n="1.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="2" mp="C">o</seg>s</w> <w n="1.3" punct="vg:5">c<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg" mp="F">e</seg>s</w>,<caesura></caesura> <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="317" place="6" mp="C">au</seg></w> <w n="1.5">li<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg></w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="1.7" punct="vg:9">b<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="2"></space><w n="2.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="2.2">m<seg phoneme="e" type="vs" value="1" rule="352" place="2">e</seg>tti<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="2.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t</w> <w n="2.5" punct="vg:8">r<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="vg">ain</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="2"></space><w n="3.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="3.2" punct="vg:4">c<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>c<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>r<seg phoneme="o" type="vs" value="1" rule="443" place="4" punct="vg">o</seg></w>, <w n="3.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="3.4" punct="vg:8">c<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="2"></space><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="4.2">ç<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="4.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="4.4">ch<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rgi<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s</w> <w n="4.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="4.6" punct="vg:8">tr<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="vg">ain</seg></rhyme></w>,</l>
					<l n="5" num="1.5" lm="9" met="4+5"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="P">a</seg>r</w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>t</w> <w n="5.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>s</w><caesura></caesura> <w n="5.4"><seg phoneme="i" type="vs" value="1" rule="496" place="5" mp="C">y</seg></w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>vi<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>s</w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="5.7" punct="pt:9">m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9" punct="pt">ain</seg></rhyme></w>.</l>
					<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="2"></space><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>pr<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="6.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="6.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.5">gu<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="2"></space><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="7.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="3">o</seg>s</w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="169" place="4">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>s</w> <w n="7.4" punct="pv:8">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">om</seg>br<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pv">eu</seg>x</rhyme></w> ;</l>
					<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="2"></space><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="8.2">jʼ</w> <w n="8.3">g<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>gʼ</w> <w n="8.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5">n<seg phoneme="o" type="vs" value="1" rule="437" place="5">o</seg>s</w> <w n="8.6">c<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>ct<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="9" num="1.9" lm="9" met="4+5"><w n="9.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="9.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="Lc">on</seg>g</w>-<w n="9.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4" caesura="1">em</seg>ps</w><caesura></caesura> <w n="9.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="5" mp="M">im</seg>pr<seg phoneme="i" type="vs" value="1" rule="466" place="6" mp="M">i</seg>m<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>s</w> <w n="9.5">s<seg phoneme="y" type="vs" value="1" rule="449" place="8" mp="P">u</seg>r</w> <w n="9.6" punct="pt:9"><rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="397" place="9" punct="pt">eu</seg>x</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>