<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE III.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC4" modus="cp" lm_max="11" metProfile="8, 4+6, (11), (9)" form="suite de strophes" schema="1[abab] 1[abaab]">
				<head type="tune">AIR de Turenne.</head>
					<lg n="1" type="regexp" rhyme="a">
						<head type="main">POLWARTH.</head>
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">C<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="1.4">j<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="1.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="1.7">l</w>'<w n="1.8" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>ppu<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="481" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<p>à Job.</p>
					<lg n="2" type="regexp" rhyme="">
						<head type="main">CLINTON,</head>
						<l part="I" n="2" num="2.1" lm="10" met="4+6"><w n="2.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="C">u</seg></w> <w n="2.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="2.3" punct="pt:4">r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>st<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="pt" caesura="1">er</seg></w>.<caesura></caesura> </l>
					</lg>
					<lg n="3" type="regexp" rhyme="baba">
						<head type="main">JOB.</head>
						<l part="F" n="2" lm="10" met="4+6"><w n="2.4">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="2.5">lʼ</w> <w n="2.6">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6" mp="M">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg></w> <w n="2.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>c</w> <w n="2.8" punct="pe:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg>f<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" punct="pe">in</seg></rhyme></w> !</l>
						<l n="3" num="3.1" lm="8" met="8"><space unit="char" quantity="6"></space><w n="3.1" punct="vg:3">G<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="vg">a</seg>l</w>, <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="3.4" punct="pt:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rc<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<p>(Il passe dans le coin à droite, tire un morceau de pain <lb></lb>d'un petit sac, et se met à manger.)</p>
						<l n="4" num="3.2" lm="10" met="4+6"><w n="4.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="341" place="1" punct="vg">à</seg></w>, <w n="4.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2" mp="P">an</seg>s</w> <w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3" mp="C">un</seg></w> <w n="4.4" punct="vg:4">c<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="4" punct="vg" caesura="1">oin</seg></w>,<caesura></caesura> <w n="4.5">j</w>' <w n="4.6">m</w>'<w n="4.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="4.8">v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s</w> <w n="4.9">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w> <w n="4.10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="C">on</seg></w> <w n="4.11" punct="pt:10">p<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10" punct="pt">ain</seg></rhyme></w>.</l>
						<l n="5" num="3.3" lm="11"><w n="5.1">J<seg phoneme="ɔ" type="vs" value="1" rule="442" place="1">o</seg>b</w> <w n="5.2">n</w>'<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="5.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="3">en</seg></w> <w n="5.5">pr<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>s</w> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="6">o</seg>r</w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="490" place="8">i</seg>s</w> <w n="5.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="5.9" punct="pt:11">m<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>t<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="11" punct="pt">in</seg></rhyme></w>.</l>
					</lg>
					<p>(Il passe dans le coin à droite, tire un morceau de pain <lb></lb>d'un petit sac, et se met à manger.)</p>
					<lg n="4" type="regexp" rhyme="b">
						<head type="main">POLWARTH.</head>
						<l n="6" num="4.1" lm="10" met="4+6"><w n="6.1" punct="pe:1">Qu<seg phoneme="wa" type="vs" value="1" rule="280" place="1" punct="pe">oi</seg></w> ! <w n="6.2">d<seg phoneme="y" type="vs" value="1" rule="449" place="2" mp="C">u</seg></w> <w n="6.3">p<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg></w> <w n="6.4">s<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4" caesura="1">e</seg>c</w><caesura></caesura> <w n="6.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="P">ou</seg>r</w> <w n="6.6">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="6.7" punct="pi:10">s<seg phoneme="y" type="vs" value="1" rule="449" place="8" mp="M">u</seg>bs<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>st<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					</lg>
					<lg n="5" type="regexp" rhyme="aab">
						<head type="main">JOB.</head>
						<l n="7" num="5.1" lm="10" met="4+6"><w n="7.1" punct="pe:1">D<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="pe">a</seg>mʼ</w> ! <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="M">An</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">ai</seg>s</w><caesura></caesura> <w n="7.4">d<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>nnʼnt</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="7.6">s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="7.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>ds</w> <w n="7.8" punct="vg:10">f<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>st<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" punct="vg">in</seg>s</rhyme></w>,</l>
						<l n="8" num="5.2" lm="9"><space unit="char" quantity="6"></space><w n="8.1">Qu</w>'<w n="8.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>l</w> <w n="8.3">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="8.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="3">en</seg></w> <w n="8.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">A</seg>m<seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>c<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="9">ain</seg>s</rhyme></w></l>
						<l n="9" num="5.3" lm="8" met="8"><space unit="char" quantity="6"></space><w n="9.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">p<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg></w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="9.5">d</w>'<w n="9.6" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>r<rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>