<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE IV.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC7" modus="cm" lm_max="10" metProfile="4+6" form="suite de strophes" schema="3[abab]">
				<head type="tune">AIR de la Couturière.</head>
					<lg n="1" type="regexp" rhyme="ab">
						<head type="main">POLWARTH.</head>
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">S<seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M/mp">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M/mp">a</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="Lp">on</seg>s</w>-<w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>s</w><caesura></caesura> <w n="1.3">pu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">em</seg>ps</w> <w n="1.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="C">ou</seg>s</w> <w n="1.7" punct="vg:10">pr<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="351" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="2.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="2.3">f<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4" caesura="1">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="2.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="C">i</seg>l</w> <w n="2.5">f<seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>t</w> <w n="2.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="C">ou</seg>s</w> <w n="2.7" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>ppr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="9" mp="M">ê</seg>t<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pt">er</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" type="regexp" rhyme="a">
						<head type="main">CLINTON, bas à Lady.</head>
						<l n="3" num="2.1" lm="10" met="4+6"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="3.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="481" place="4" punct="vg" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6">e</seg>c</w> <w n="3.5">lu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg></w> <w n="3.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.7">v<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="C">ou</seg>s</w> <w n="3.8" punct="pt:10">l<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						</lg>
					<lg n="3" type="regexp" rhyme="b">
						<head type="main">LADY.</head>
						<l n="4" num="3.1" lm="10" met="4+6"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="4.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.3">m<seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="4.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="4.5">s<seg phoneme="o" type="vs" value="1" rule="317" place="6" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg></w> <w n="4.6" punct="ps:10">pr<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>t<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="ps">er</seg></rhyme></w> …</l>
						</lg>
					<lg n="4" type="regexp" rhyme="ab">
						<head type="main">JOB, <p>bas à Arwed.</p></head>
						<l n="5" num="4.1" lm="10" met="4+6"><w n="5.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">on</seg></w> <w n="5.2" punct="vg:4">c<seg phoneme="o" type="vs" value="1" rule="443" place="2" mp="M">o</seg>l<seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4" punct="vg" caesura="1">e</seg>l</w>,<caesura></caesura> <w n="5.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="5.4">nʼ</w> <w n="5.5">s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s</w> <w n="5.6">cʼ</w> <w n="5.7">qu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg></w> <w n="5.8">sʼ</w> <w n="5.9" punct="pv:10">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>g<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="6" num="4.2" lm="10" met="4+6"><w n="6.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1" punct="vg">ai</seg>s</w>, <w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="6.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>t</w> <w n="6.4" punct="vg:4">c<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="6.5">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="6.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="7">en</seg></w> <w n="6.7">g<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="P">à</seg></w> <w n="6.9" punct="pt:10">v<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pt">ou</seg>s</rhyme></w>.</l>
						</lg>
					<lg n="5" type="regexp" rhyme="a">
						<head type="main">CLINTON, de même.</head>
						<l n="7" num="5.1" lm="10" met="4+6"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="7.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" caesura="1">in</seg></w><caesura></caesura> <w n="7.4">g<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="7.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="C">on</seg></w> <w n="7.6" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>ll<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						</lg>
					<lg n="6" type="regexp" rhyme="b">
						<head type="main">LADY.</head>
						<l n="8" num="6.1" lm="10" met="4+6"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="8.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="8.4">qu</w>'<w n="8.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="8.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="8.7">l</w>'<w n="8.8"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.9"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="P">à</seg></w> <w n="8.10" punct="pt:10">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pt">ou</seg>x</rhyme></w>.</l>
					</lg>
					<lg n="7" type="regexp" rhyme="abab">
					<head type="main">ENSEMBLE.</head>
						<l n="9" num="7.1" lm="10" met="4+6"><w n="9.1">S<seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M/mp">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M/mp">a</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="Lp">on</seg>s</w>-<w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>s</w><caesura></caesura> <w n="9.3">pu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="9.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="9.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">em</seg>ps</w> <w n="9.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="C">ou</seg>s</w> <w n="9.7" punct="pv:10">pr<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="351" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="10" num="7.2" lm="10" met="4+6"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="10.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="10.3">f<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4" caesura="1">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="10.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="C">i</seg>l</w> <w n="10.5">f<seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>t</w> <w n="10.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="C">ou</seg>s</w> <w n="10.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>ppr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="9" mp="M">ê</seg>t<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="vg">er</seg></rhyme></w>,</l>
						<l n="11" num="7.3" lm="10" met="4+6"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="11.3">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="11.4">li<seg phoneme="ø" type="vs" value="1" rule="397" place="4" caesura="1">eu</seg>x</w><caesura></caesura> <w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="11.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="11.7">v<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="C">ou</seg>s</w> <w n="11.8" punct="vg:10">l<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="12" num="7.4" lm="10" met="4+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="12.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="12.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="4" caesura="1">en</seg>s</w><caesura></caesura> <w n="12.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="P">ou</seg>r</w> <w n="12.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="12.6">pl<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>s</w> <w n="12.7">v<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="C">ou</seg>s</w> <w n="12.8" punct="pt:10">qu<seg phoneme="i" type="vs" value="1" rule="490" place="9" mp="M">i</seg>tt<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pt">er</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>