<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE V.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC8" modus="cm" lm_max="10" metProfile="4+6" form="strophe unique" schema="1(ababcdcd)">
				<head type="tune">AIR de Teniers.</head>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<head type="main">ARWED.</head>
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="vg" caesura="1">ai</seg>s</w>,<caesura></caesura> <w n="1.4">h<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="M">on</seg>t<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="1.6">m<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="1.7" punct="vg:10">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9" mp="M">ai</seg>bl<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="351" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="2.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg></w> <w n="2.3">l</w>'<w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>r</w><caesura></caesura> <w n="2.5">p<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>t</w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6" mp="M">en</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="304" place="7" mp="M">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w> <w n="2.7">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="2.8" punct="vg:10">p<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="vg">a</seg>s</rhyme></w>,</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">Br<seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="M">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="3.3">n<seg phoneme="ø" type="vs" value="1" rule="247" place="4" caesura="1">œu</seg>ds</w><caesura></caesura> <w n="3.4">d</w>'<w n="3.5"><seg phoneme="y" type="vs" value="1" rule="452" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6" mp="M">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg>dr<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="351" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">L</w>'<w n="4.2">h<seg phoneme="o" type="vs" value="1" rule="443" place="1" mp="M">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r</w> <w n="4.3">l</w>'<w n="4.4" punct="vg:4"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="3" mp="M">o</seg>rd<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4" punct="vg" caesura="1">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="4.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="4.7">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="o" type="vs" value="1" rule="317" place="8" mp="C">au</seg>x</w> <w n="4.9" punct="pt:10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="M">om</seg>b<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="pt">a</seg>ts</rhyme></w>.</l>
						<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="dp" caesura="1">ai</seg>s</w> :<caesura></caesura> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="C">Au</seg>x</w> <w n="5.5">y<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="5.7">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="5.8" punct="vg:10">b<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">on</seg></w> <w n="6.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="6.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="3" mp="M">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="4" caesura="1">ô</seg>t</w><caesura></caesura> <w n="6.4">v<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="6.5">br<seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="M">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="6.6">d</w>’<w n="6.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="8">un</seg></w> <w n="6.8" punct="pt:10">l<seg phoneme="o" type="vs" value="1" rule="317" place="9" mp="M">au</seg>ri<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pt">er</seg></rhyme></w>.</l>
						<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="1" mp="M">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r</w> <w n="7.2" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4" punct="vg" caesura="1">o</seg>rs</w>,<caesura></caesura> <w n="7.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="7" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg></w> <w n="7.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="9">è</seg>s</w> <w n="7.6">d</w>'<w n="7.7" punct="ps:10"><rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg></rhyme></w> …</l>
						<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">l</w>'<w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg></w> <w n="8.4" punct="vg:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="y" type="vs" value="1" rule="456" place="4" punct="vg" caesura="1">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="8.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="8.7">su<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg>s</w> <w n="8.8" punct="pt:10">pr<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>s<seg phoneme="o" type="vs" value="1" rule="434" place="9" mp="M">o</seg>nni<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pt">er</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>