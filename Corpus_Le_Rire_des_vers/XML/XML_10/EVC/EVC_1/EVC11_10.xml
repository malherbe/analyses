<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE VII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC11" modus="cp" lm_max="10" metProfile="8, 4+6" form="strophe unique" schema="1(ababcdcd)">
				<head type="tune">AIR nouveau de Doche.</head>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<head type="main">ANNA.</head>
						<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="1.1">J<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="1.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.3">n</w>'<w n="1.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="1.6">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>bl<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="351" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="2.2">l</w>'<w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2" mp="M">en</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="P">à</seg></w> <w n="2.5">tr<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r</w> <w n="2.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="C">on</seg></w> <w n="2.7" punct="pt:10">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="9" mp="M">a</seg><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="320" place="10" punct="pt">y</seg>s</rhyme></w>.</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1" punct="vg:2">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="1" mp="M">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="2" punct="vg">ô</seg>t</w>, <w n="3.2" punct="pe:4">h<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="pe" caesura="1">a</seg>s</w> !<caesura></caesura> <w n="3.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="C">on</seg></w> <w n="3.4">c<seg phoneme="œ" type="vs" value="1" rule="248" place="6">œu</seg>r</w> <w n="3.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="7">ein</seg></w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="3.7">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9" mp="M">o</seg>bl<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="351" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="4.1">M</w>'<w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">en</seg></w> <w n="4.3">p<seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r</w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.6" punct="pt:8">m<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>pr<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pt">i</seg>s</rhyme></w>.</l>
						<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="5.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="5.3" punct="vg:4">j<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="5.4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="5">oin</seg>s</w> <w n="5.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="P">à</seg></w> <w n="5.7">s<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="5.8" punct="vg:10">fl<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="6.2">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2" mp="C">e</seg>t</w> <w n="6.3">h<seg phoneme="i" type="vs" value="1" rule="496" place="3" mp="M">y</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="220" place="4" caesura="1">en</seg></w><caesura></caesura> <w n="6.4">j</w>'<w n="6.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="6.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="6.7" punct="vg:10">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="10" punct="vg">en</seg>s</rhyme></w>,</l>
						<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="7.3">qu</w>'<w n="7.4"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="7.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="C">au</seg></w> <w n="7.7">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="6">oin</seg>s</w> <w n="7.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.9">p<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="P">ou</seg>r</w> <w n="7.10">s<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="7.11">f<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="192" place="10">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="4"></space><w n="8.1">L</w>'<w n="8.2">h<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="8.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="8.7" punct="pt:8">bi<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="8" punct="pt">en</seg>s</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>