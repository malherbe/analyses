<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE X.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC13" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(ababcdcd)">
				<head type="tune">AIR du vaudeville de l'Actrice.</head>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<head type="main">POLWARTH.</head>
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rsqu</w>'<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="3">e</seg>c</w> <w n="1.3"><seg phoneme="y" type="vs" value="1" rule="452" place="4">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>r</w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>xtr<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">J</w>'<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>xc<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="2.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>s</w> <w n="2.5" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>ss<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="3.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3" punct="dp:4">r<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="dp">on</seg>d</w> : <w n="3.4">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="3.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w>-<w n="3.6" punct="pv:8">m<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="4.2">pl<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="4.4" punct="vg:4">l<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t</w> <w n="4.7" punct="pe:8">d<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pe">eu</seg>x</rhyme></w> !</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="5.2">d<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="5.3">r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="5.4">s<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>r</w> <w n="5.5">l<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>rs</w> <w n="5.6" punct="pv:8">ch<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></w> ;</l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="6.3" punct="vg:4">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4" punct="vg">an</seg>s</w>, <w n="6.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="6.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="6.6" punct="vg:8">v<seg phoneme="ɛ" type="vs" value="1" rule="354" place="7">e</seg>x<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="vg">er</seg></rhyme></w>,</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">D<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="7.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.3">c</w>'<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="7.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="304" place="6">aî</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>gl<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.5" punct="pt:8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pt">er</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>