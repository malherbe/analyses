<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE GENTILHOMME</title>
				<title type="sub">VAUDEVILLE ANECDOTIQUE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DPY" sort="1">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="COU" sort="2">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>135 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DDC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE GENTILHOMME</title>
						<author>DUPEUTY ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=blZoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032862369.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">21 MARS 1833</date>
				<placeName>
					<settlement>THÉÂTRE NATIONAL DU VAUDEVILLE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DDC11" modus="sp" lm_max="8" metProfile="5, 4, 6, 8" form="" schema="">
	<head type="main">( Ils reprennent tous le refrain en chœur. ‒ Au public.)</head> 
	<lg n="1" rhyme="None">
		<l n="1" num="1.1" lm="5" met="5"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="1.2" punct="vg:2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2" punct="vg">ou</seg>s</w>, <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>s</w> <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="1.5" punct="vg:5">l<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="5">o</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg>s</rhyme></w>,</l>
		<l n="2" num="1.2" lm="5" met="5"><w n="2.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">om</seg>bl<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="2.2">m<seg phoneme="wa" type="vs" value="1" rule="422" place="3">oi</seg></w> <w n="2.3">d</w>'<w n="2.4"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>l<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="5">o</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg>s</rhyme></w></l>
		<l n="3" num="1.3" lm="5" met="5"><w n="3.1">D<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>c<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ts</w> <w n="3.2"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="3.3" punct="pv:5">f<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5" punct="pv">in</seg>s</rhyme></w> ;</l>
		<l n="4" num="1.4" lm="5" met="5"><w n="4.1" punct="vg:1">T<seg phoneme="wa" type="vs" value="1" rule="422" place="1" punct="vg">oi</seg></w>, <w n="4.2">d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rt<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></w>,</l>
		<l n="5" num="1.5" lm="5" met="5"><w n="5.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="5.2">di<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg></w> <w n="5.3" punct="vg:5">t<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>l<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></w>,</l>
		<l n="6" num="1.6" lm="4" met="4"><w n="6.1"><seg phoneme="u" type="vs" value="1" rule="424" place="1">Ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="6.3" punct="ps:4">m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="4" punct="ps">ain</seg>s</rhyme></w>…</l>
		<p>
		( parlant.) Surtout, messieurs, que personne n'ait la malheu<lb></lb>
		reuse idée de faire entendre le signe de ralliement des voleurs :<lb></lb> 
		vous avez vu comme la police se fait à Paris, ct monsieur de<lb></lb>
		Sardine, que voici, ne manquerait pas de faire courir sus au dé<lb></lb>
		Jinquant… Néanmoins, néanmoinsse, vous avez une manière<lb></lb>
		toute simple de manifester votre opinion, manière dont mon<lb></lb>
		sieun de sardine, que voilà, serait lui-même fort satisfait…<lb></lb> 
		Voici tà manière : 
		</p>
		<l n="7" num="1.7" lm="6" met="6"><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="7.2" punct="vg:3">br<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="o" type="vs" value="1" rule="437" place="3" punct="vg">o</seg>s</w>, <w n="7.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="7.4" punct="vg:6">br<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="437" place="6" punct="vg">o</seg>s</rhyme></w>,</l>
		<l n="8" num="1.8" lm="4" met="4"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="8.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>s</w> <w n="8.3">d</w>'<w n="8.4" punct="pv:4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>tt<rhyme label="e" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pv">e</seg>s</rhyme></w> ;</l>
		<l n="9" num="1.9" lm="6" met="6"><w n="9.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="9.2" punct="vg:3">br<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="o" type="vs" value="1" rule="437" place="3" punct="vg">o</seg>s</w>, <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="9.4" punct="vg:6">br<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="437" place="6" punct="vg">o</seg>s</rhyme></w>,</l>
		<l n="10" num="1.10" lm="6" met="6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="10.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="10.3" punct="pe:6">br<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ss<seg phoneme="i" type="vs" value="1" rule="466" place="5">i</seg>m<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="437" place="6" punct="pe">o</seg>s</rhyme></w> !</l>
		<l n="11" num="1.11" lm="8" met="8"><w n="11.1">J</w>'<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="1">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="11.4" punct="vg:5">br<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>v<seg phoneme="o" type="vs" value="1" rule="437" place="5" punct="vg">o</seg>s</w>, <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="11.6" punct="pt:8">br<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="437" place="8" punct="pt">o</seg>s</rhyme></w>.</l>
		<l n="12" num="1.12" lm="5" met="5"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="12.2">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="12.4" punct="pv:5">cl<rhyme label="e" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg>s</rhyme></w> ;</l>
		<l n="13" num="1.13" lm="8" met="8"><w n="13.1">J</w>'<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="1">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="13.4" punct="vg:5">br<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>v<seg phoneme="o" type="vs" value="1" rule="437" place="5" punct="vg">o</seg>s</w>, <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="13.6">br<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<rhyme label="d" id="7" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="437" place="8">o</seg>s</rhyme></w></l>
		<l n="14" num="1.14" lm="6" met="6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="14.3" punct="pt:6">br<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ss<seg phoneme="i" type="vs" value="1" rule="466" place="5">i</seg>m<rhyme label="d" id="7" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="437" place="6" punct="pt">o</seg>s</rhyme></w>.</l>
		<l n="15" num="1.15" lm="4" met="4"><w n="15.1" punct="pe:2">Br<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>v<seg phoneme="o" type="vs" value="1" rule="443" place="2" punct="pe">o</seg></w> ! <w n="15.2" punct="pe:4">br<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<rhyme label="f" id="8" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="443" place="4" punct="pe">o</seg></rhyme></w> !</l>
		<l n="16" num="1.16" lm="4" met="4"><w n="16.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="16.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="16.3" punct="pe:4">cl<rhyme label="e" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg>s</rhyme></w> !</l>
		<l n="17" num="1.17" lm="4" met="4"><w n="17.1" punct="pe:2">Br<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>v<seg phoneme="o" type="vs" value="1" rule="443" place="2" punct="pe">o</seg></w> ! <w n="17.2" punct="pe:4">br<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<rhyme label="f" id="8" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="443" place="4" punct="pe">o</seg></rhyme></w> !</l>
		<l n="18" num="1.18" lm="4" met="4"><w n="18.1" punct="pe:4">Br<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ss<seg phoneme="i" type="vs" value="1" rule="466" place="3">i</seg>m<rhyme label="f" id="8" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="443" place="4" punct="pe">o</seg></rhyme></w> !</l>
	</lg>
</div></body></text></TEI>