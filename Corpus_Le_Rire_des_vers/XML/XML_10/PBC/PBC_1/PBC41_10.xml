<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC41" modus="cp" lm_max="9" metProfile="8, 3+6" form="strophe unique" schema="1(ababcdcd)">
	<head type="tune">AIR de Caroline.</head>
	<lg n="1" type="huitain" rhyme="ababcdcd">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:2">S<seg phoneme="ɛ" type="vs" value="1" rule="383" place="1">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="406" place="2" punct="vg">eu</seg>r</w>, <w n="1.2">c</w>'<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w>'<w n="1.5">f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.6">qu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg></w> <w n="1.7" punct="vg:8">d<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="8" punct="vg">oi</seg>t</rhyme></w>,</l>
		<l n="2" num="1.2" lm="9" met="3+6"><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="Lc">a</seg>rc</w>'<w n="2.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">c</w>'<w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3" caesura="1">e</seg>st</w><caesura></caesura> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4" mp="C">un</seg></w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="M">an</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="6" mp="M">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="7">en</seg></w> <w n="2.7" punct="vg:9"><seg phoneme="y" type="vs" value="1" rule="449" place="8" mp="M">u</seg>s<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="vg" mp="F">e</seg></rhyme></w>,</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>lqu</w>'<w n="3.2">f<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r</w> <w n="3.4" punct="vg:5">h<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s<seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="vg">a</seg>rd</w>, <w n="3.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="3.7" punct="vg:8">v<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="8" punct="vg">oi</seg>t</rhyme></w>,</l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="4.2">c</w>'<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="4.4">r<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r</w>'<w n="4.5">s<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>t</w> <w n="4.6"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg></w> <w n="4.7" punct="pt:8">v<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ll<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1">F<seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>t</w> <w n="5.2">n</w>'<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>r</w> <w n="5.4">j<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s</w> <w n="5.5"><seg phoneme="y" type="vs" value="1" rule="390" place="6">eu</seg></w> <w n="5.6">d</w>' <w n="5.7" punct="dp:8">g<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>l<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="dp">an</seg>s</rhyme></w> :</l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1">C</w>'<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="6.3" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ff<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="in vg">i</seg>l</w>', <w n="6.4">ç<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="6.5">n</w>'<w n="6.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.7">v<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>t</w> <w n="6.8" punct="dp:8">gu<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="7.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="7.4">n<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>mm</w>'<w n="7.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="7.7" punct="vg:8"><rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="vg">an</seg>s</rhyme></w>,</l>
		<l n="8" num="1.8" lm="9" met="3+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="8.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="3" caesura="1">à</seg></w><caesura></caesura> <w n="8.3">c</w>'<w n="8.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5">c</w>'<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="8.7">qu</w>'<w n="8.8"><seg phoneme="y" type="vs" value="1" rule="452" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="8.9" punct="pt:9">r<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg>si<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="pt" mp="F">e</seg></rhyme></w>.</l>
	</lg>
</div></body></text></TEI>