<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC2" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="1[ababb] 1[abab]">
	<head type="tune">AIR : Vaudeville du Baiser au Porteur.</head>
	<lg n="1" type="regexp" rhyme="a">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">V<seg phoneme="wa" type="vs" value="1" rule="419" place="1">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="1.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.4" punct="pt:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">in</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>c<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
	</lg>
	<lg n="2" type="regexp" rhyme="b">
	<head type="speaker">SERPENTINE.</head>
		<l n="2" num="2.1" lm="8" met="8"><w n="2.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="1">en</seg></w> <w n="2.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="2.3">r<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>s</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>t</w> <w n="2.6" punct="pt:8">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>s<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="8" punct="pt">oin</seg></rhyme></w>.</l>
	</lg>
	<lg n="3" type="regexp" rhyme="a">
	<head type="speaker">LE LUT1N.</head>
		<l n="3" num="3.1" lm="8" met="8"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="3.5" punct="pt:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">in</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>s<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
	</lg>
	<lg n="4" type="regexp" rhyme="b">
	<head type="speaker">SERPENTINE.</head>
		<l n="4" num="4.1" lm="8" met="8"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="4.3" punct="vg:4">c<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>ssi<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="vg">er</seg>s</w>, <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="338" place="5">a</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="4.6" punct="pt:8">s<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="8" punct="pt">oin</seg></rhyme></w>.</l>
	</lg>
	<lg n="5" type="regexp" rhyme="ba">
	<head type="speaker">LE LUTIN.</head>
		<l n="5" num="5.1" lm="8" met="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="5.2" punct="ps:3">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="3" punct="ps">ez</seg></w>… <w n="5.3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="5.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="5.6" punct="vg:8">c<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="8" punct="vg">oin</seg></rhyme></w>,</l>
		<l n="6" num="5.2" lm="8" met="8"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="6.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="304" place="2">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="6.3">qu</w>'<w n="6.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="6.6" punct="pt:8">pr<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>r<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
	</lg>
	<lg n="6" type="regexp" rhyme="b">
	<head type="speaker">SERPENTINE.</head>
		<l n="7" num="6.1" lm="8" met="8"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="7.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="7.3" punct="ps:4">ch<seg phoneme="ɛ" type="vs" value="1" rule="304" place="3">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="ps">e</seg>s</w>… <w n="7.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="7.5" punct="pi:6">qu<seg phoneme="i" type="vs" value="1" rule="490" place="6" punct="pi">i</seg></w> ? <w n="7.6" punct="pe:8">j<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pe">ai</seg>s</rhyme></w> !</l>
	</lg>
	<lg n="7" type="regexp" rhyme="a">
	<head type="speaker">LE LUTIN.</head>
		<l n="8" num="7.1" lm="8" met="8"><w n="8.1" punct="pe:2">V<seg phoneme="wa" type="vs" value="1" rule="439" place="1">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="pe">ez</seg></w> ! <w n="8.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="8.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="6">en</seg></w> <w n="8.6" punct="ps:8">d<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>r<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg>s</rhyme></w>…</l>
	</lg>
	<lg n="8" type="regexp" rhyme="b">
	<head type="speaker">SERPENTINE, à part, riant.</head>
		<l n="9" num="8.1" lm="8" met="8"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="9.2">l</w>'<w n="9.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4">en</seg>t</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="9.6" punct="pt:8">s<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>j<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="8" punct="pt">e</seg>ts</rhyme></w>.</l>
	</lg>
</div></body></text></TEI>