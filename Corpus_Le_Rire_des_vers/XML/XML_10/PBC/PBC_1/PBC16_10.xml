<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC16" modus="sp" lm_max="8" metProfile="8, 6">
	<head type="tune">AIR de Fernand Cortez.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="1.2" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="pe">an</seg>t</w> ! <w n="1.3">su<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s</w>-<w n="1.4" punct="vg:6">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="6" punct="vg">e</seg></w>, <w n="1.5" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="vg">on</seg>s</w>,</l>
		<l n="2" num="1.2" lm="6" met="6"><w n="2.1">C<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="2.4" punct="pv:6">j<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">am</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg>s</w> ;</l>
		<l n="3" num="1.3" lm="6" met="6"><w n="3.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="3.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="3.3">tr<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="3.4" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="vg">on</seg>s</w>,</l>
		<l n="4" num="1.4" lm="6" met="6"><w n="4.1" punct="vg:2">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="4.2" punct="vg:4">c<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg">on</seg>s</w>, <w n="4.3" punct="pt:6">v<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="pt">on</seg>s</w>.</l>
  </lg>   
	<lg n="2">
	<head type="speaker">TURLUBEK.</head> 
		<l n="5" num="2.1" lm="6" met="6"><w n="5.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="5.2" punct="vg:2">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="2" punct="vg">eu</seg></w>, <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="338" place="3">a</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="5.4" punct="pe:6">p<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="pe ps">é</seg></w> ! …</l>
		<l n="6" num="2.2" lm="8" met="8"><w n="6.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="6.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="3">oi</seg></w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="6.5">vʼl<seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="6.6">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="6.7" punct="pe:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">am</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</w> !</l>
		<l n="7" num="2.3" lm="6" met="6"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="7.2" punct="pe:3">b<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="pe">e</seg>s</w> ! <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r</w> <w n="7.4" punct="vg:6">p<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="vg">é</seg></w>,</l>
		<l n="8" num="2.4" lm="6" met="6"><w n="8.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">m</w>' <w n="8.3">d<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="8.5">d</w>' <w n="8.6">c<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>ps</w> <w n="8.7">d</w>' <w n="8.8" punct="pe:6">pi<seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="pe">é</seg></w> !</l>
  </lg>    
	<lg n="3">
	<head type="speaker">LES BOTTES.</head> 
		<l n="9" num="3.1" lm="6" met="6"><w n="9.1" punct="vg:2">C<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="9.2" punct="vg:4">v<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg">on</seg>s</w>, <w n="9.3" punct="pe:6">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="pe">on</seg>s</w> !</l>
		<l n="10" num="3.2" lm="8" met="8"><w n="10.1" punct="vg:2">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="10.2">c<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="10.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="10.5" punct="pt:8">j<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">am</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
  </lg>
</div></body></text></TEI>