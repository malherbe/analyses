<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANDRÉ LE CHANSONNIER</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="FTN" sort="1">
					<name>
						<forename>Louis Marie</forename>
						<surname>FONTAN</surname>
					</name>
					<date from="1801" to="1839">1801-1839</date>
				</author>
				<author key="DNY" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>DESNOYER</surname>
					</name>
					<date from="1806" to="1858">1806-1858</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>239 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>André le chansonnier.</title>
						<author>FONTAN ET DESNOYER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=aN6oyNpF3cMC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>André le chansonnier.</title>
								<author>FONTAN ET DESNOYER</author>
								<repository>Biblioteca Casanatense</repository>
								<idno type="URI">http://opac.casanatense.it/Record.htm?idlist=3</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Ode et Wodon</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-15" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE I</head><head type="main_subpart">SCÈNE VIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FED6" modus="sp" lm_max="6" metProfile="6, (5), (4)" form="suite de strophes" schema="2[abab]">
				<head type="tune">Même air.</head>
					<lg n="1" type="regexp" rhyme="abababa">
						<head type="main">ÉMILIE.</head>
						<l n="1" num="1.1" lm="5"><space unit="char" quantity="2"></space><w n="1.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="1.2">c<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="1.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="1.5" punct="vg:5">l<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="2" num="1.2" lm="6" met="6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="2.2">fu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="2.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="3">oin</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.5">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="2.6" punct="dp:6">li<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ø" type="vs" value="1" rule="397" place="6" punct="dp">eu</seg>x</rhyme></w> :</l>
						<l n="3" num="1.3" lm="6" met="6"><w n="3.1">V<seg phoneme="wa" type="vs" value="1" rule="419" place="1">oi</seg>s</w> <w n="3.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="3.3" punct="vg:3">m<seg phoneme="o" type="vs" value="1" rule="317" place="3" punct="vg">au</seg>x</w>, <w n="3.4">v<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>s</w> <w n="3.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="3.6">l<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></w></l>
						<l n="4" num="1.4" lm="6" met="6"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="4.2">c<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="4.5" punct="pt:6">y<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ø" type="vs" value="1" rule="397" place="6" punct="pt">eu</seg>x</rhyme></w>.</l>
						<l n="5" num="1.5" lm="6" met="6"><w n="5.1">N<seg phoneme="o" type="vs" value="1" rule="437" place="1">o</seg>s</w> <w n="5.2">pl<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>rs</w> <w n="5.3">v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>t</w> <w n="5.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.5" punct="dp:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>f<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></rhyme></w> :</l>
						<l n="6" num="1.6" lm="6" met="6"><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="6.2">t<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="6.3" punct="vg:3">m<seg phoneme="œ" type="vs" value="1" rule="406" place="3" punct="vg">eu</seg>rs</w>, <w n="6.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5" punct="ps:6">m<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>rr<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6" punct="ps">ai</seg></rhyme></w> …</l>
						<l n="7" num="1.7" lm="6" met="6"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rr<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w>-<w n="7.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="7.4" punct="pi:6">r<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>p<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pi">e</seg></rhyme></w> ?</l>
					</lg>
					<lg n="2" type="regexp" rhyme="b">
						<head type="main">ANDRÉ.</head>
						<l n="8" num="2.1" lm="4"><space unit="char" quantity="4"></space><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2" punct="pt:4">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4" punct="pt">ai</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>