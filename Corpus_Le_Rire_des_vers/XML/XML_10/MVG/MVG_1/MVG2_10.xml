<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER TABLEAU.</head><head type="main_subpart">SCÈNE PREMIÈRE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG2" modus="cp" lm_max="10" metProfile="8, 4+6" form="strophe unique" schema="1(ababcdccd)">
						<head type="tune">AIR : Garrick.</head>
						<lg n="1" type="neuvain" rhyme="ababcdccd">
							<head type="main">ÆGIDIUS.</head>
							<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="1.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="M">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="1.4">l<seg phoneme="œ" type="vs" value="1" rule="406" place="5" mp="C">eu</seg>rs</w> <w n="1.5">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6" mp="M">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>s</w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="188" place="8">e</seg>t</w> <w n="1.7">l<seg phoneme="œ" type="vs" value="1" rule="406" place="9" mp="C">eu</seg>rs</w> <w n="1.8" punct="pv:10">m<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="248" place="10" punct="pv">œu</seg>rs</rhyme></w> ;</l>
							<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2" punct="dp:4">r<seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>pl<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4" punct="dp" caesura="1">ai</seg></w> :<caesura></caesura> <w n="2.3">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5" mp="P">an</seg>s</w> <w n="2.4" punct="vg:6">d<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="vg">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="2.5"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="C">i</seg>ls</w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="9">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="2.7" punct="pv:10">br<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg>s</rhyme></w> ;</l>
							<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="3.3" punct="vg:4">R<seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="4" punct="vg" caesura="1">ain</seg>s</w>,<caesura></caesura> <w n="3.4">d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" mp="F">e</seg>nt</w> <w n="3.5">n<seg phoneme="o" type="vs" value="1" rule="437" place="7" mp="C">o</seg>s</w> <w n="3.6">vi<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg>x</w> <w n="3.7" punct="vg:10"><seg phoneme="o" type="vs" value="1" rule="317" place="9" mp="M">au</seg>t<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="vg">eu</seg>rs</rhyme></w>,</l>
							<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">N</w>'<w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">en</seg></w> <w n="4.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="M">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="4.5">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="4.6">p<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="P">ou</seg>r</w> <w n="4.7">l<seg phoneme="œ" type="vs" value="1" rule="406" place="8" mp="C">eu</seg>rs</w> <w n="4.8" punct="pt:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>scl<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
							<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="5.4">m<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4" caesura="1">e</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="5.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg></w> <w n="5.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7" mp="P">an</seg>s</w> <w n="5.8">l</w>'<w n="5.9" punct="pv:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8" mp="M">em</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>rr<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="pv">a</seg>s</rhyme></w> ;</l>
							<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="6.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">r<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" caesura="1">on</seg>d</w><caesura></caesura> <w n="6.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" punct="vg">o</seg>rs</w>, <w n="6.5">d</w>'<w n="6.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="6.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg></w> <w n="6.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="6.9" punct="vg:10">m<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="4"></space><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">s<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="7.4" punct="vg:5">R<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="5" punct="vg">ain</seg>s</w>, <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="7.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.7" punct="vg:8">c<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg>s</rhyme></w>,</l>
							<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="4"></space><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="8.3">n</w>'<w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="8.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.6" punct="vg:8">p<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg>s</rhyme></w>,</l>
							<l n="9" num="1.9" lm="8" met="8"><space unit="char" quantity="4"></space><w n="9.1">C</w>'<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="9.3">qu</w>'<w n="9.4"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ls</w> <w n="9.5">n</w>'<w n="9.6"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="9.8">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>ts</w> <w n="9.9">p<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r</w> <w n="9.10">l</w>'<w n="9.11" punct="pt:8"><rhyme label="d" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						</lg>
					</div></body></text></TEI>