<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME TABLEAU.</head><head type="main_subpart">SCÈNE XIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG16" modus="sp" lm_max="8" metProfile="4, 8" form="suite de strophes" schema="1[aa] 1[abbaa] 1[aaa]">
						<head type="tune">AIR : Comme il m'aimait.</head>
						<lg n="1" type="regexp" rhyme="aaabbaaaaa">
							<head type="main">BONAPARTE.</head>
							<l n="1" num="1.1" lm="4" met="4"><space unit="char" quantity="8"></space><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">su<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="1.3" punct="pv:4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>ç<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="pv">ai</seg>s</rhyme></w> ; <del hand="LG" type="repetition" reason="analysis">(bis.)</del></l>
							<l n="2" num="1.2" lm="4" met="4"><space unit="char" quantity="8"></space><subst hand="LG" type="repetition" reason="analysis"><del></del><add rend="hidden"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">su<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="2.3" punct="pv:4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>ç<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="pv">ai</seg>s</rhyme></w> ;</add></subst></l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="3.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="3.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="3.5">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.6" punct="pt:8">ch<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>r<rhyme label="a" id="2" gender="f" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="4.2" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="4.3">s<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="4.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>tt<rhyme label="b" id="3" gender="m" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="vg">ai</seg>s</rhyme></w>,</l>
							<l n="5" num="1.5" lm="8" met="8"><w n="5.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="5.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r</w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg></w> <w n="5.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6" punct="pt:8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="7">ain</seg>cr<rhyme label="b" id="3" gender="m" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pt">ai</seg>s</rhyme></w>.</l>
							<l n="6" num="1.6" lm="8" met="8"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="6.3">qu</w>'<w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="6.5">j<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r</w> <w n="6.6">ch<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="6.7">s</w>'<w n="6.8" punct="dp:8"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>cr<rhyme label="a" id="2" gender="f" type="e" stanza="2"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
							<l n="7" num="1.7" lm="8" met="8"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="7.3">gl<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="7.6" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>tr<rhyme label="a" id="2" gender="f" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
							<l n="8" num="1.8" lm="4" met="4"><space unit="char" quantity="8"></space><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="8.2">f<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>t</w> <w n="8.3" punct="pe:4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>ç<rhyme label="a" id="4" gender="m" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="pe">ai</seg>s</rhyme></w> ! <del hand="LG" type="repetition" reason="analysis">(bis.)</del></l>
							<l n="9" num="1.9" lm="4" met="4"><space unit="char" quantity="8"></space><subst hand="LG" type="repetition" reason="analysis"><del></del><add rend="hidden"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="9.2">f<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>t</w> <w n="9.3" punct="pe:4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>ç<rhyme label="a" id="4" gender="m" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="pe">ai</seg>s</rhyme></w> !</add></subst></l>
							<l n="10" num="1.10" lm="8" met="8"><w n="10.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="10.2">B<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="10.4" punct="pe:8">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>ç<rhyme label="a" id="4" gender="m" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pe">ai</seg>s</rhyme></w> !</l>
						</lg>
					</div></body></text></TEI>