<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER TABLEAU.</head><head type="main_subpart">SCÈNE V.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG10" modus="cp" lm_max="10" metProfile="8, 4+6" form="suite périodique" schema="1(abbacdcd) 2(abba)">
							<head type="tune">AIR : de la Lune de Miel.</head>
							<lg n="1" type="huitain" rhyme="abbacdcd">
								<head type="main">DARBEL.</head>
								<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="1.2" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="1.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="1.5">b<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>s</w> <w n="1.6" punct="pt:10">pl<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="10" punct="pt">u</seg>s</rhyme></w>.</l>
								<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="2.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg></w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="2.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" caesura="1">ain</seg></w><caesura></caesura> <w n="2.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.8" punct="pt:10">p<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
								<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="3.2">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="3.3" punct="vg:4">s<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="M">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="1">ou</seg>t</w>,<caesura></caesura> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6" mp="M">en</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t</w> <w n="3.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" mp="P">an</seg>s</w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="3.8" punct="vg:10">cl<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
								<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">G<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2" mp="C">au</seg>x</w> <hi rend="ital"><w n="4.3">p<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3" mp="M">in</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="450" place="4" caesura="1">u</seg>ms</w></hi><caesura></caesura> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="C">u</seg></w> <w n="4.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6" punct="pt:10"><seg phoneme="e" type="vs" value="1" rule="271" place="7" mp="M">Æ</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>d<seg phoneme="i" type="vs" value="1" rule="dc-1" place="9" mp="M">i</seg><rhyme label="a" id="1" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="449" place="10" punct="pt">u</seg>s</rhyme></w>.</l>
								<p>(A Bonaparte.)</p>
								<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">su<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="5.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>t<seg phoneme="?" type="va" value="1" rule="161" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="5.6" punct="dp:10">p<seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" punct="dp">on</seg></rhyme></w> :</l>
								<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="4"></space><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">n</w>'<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="6.6">s<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg></w> <w n="6.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>d</w> <w n="6.8" punct="vg:8">pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>bl<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
								<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="3">en</seg>s</w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="341" place="4" caesura="1">à</seg></w><caesura></caesura> <w n="7.5">qu</w>'<w n="7.6"><seg phoneme="y" type="vs" value="1" rule="452" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="7.7">b<seg phoneme="ɔ" type="vs" value="1" rule="418" place="7">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg></rhyme></w></l>
								<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="4"></space><w n="8.1">C</w>'<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="8.3">pl<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>gr<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.5">qu</w>'<w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="8.7" punct="pt:8">th<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
							</lg>
							<div type="section" n="1">
								<head type="main">ENSEMBLE.</head>
								<lg n="1" type="quatrain" rhyme="abba">
									<head type="main">CHŒUR.</head>
									<l n="9" num="1.1" lm="10" met="4+6"><w n="9.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="9.2" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="9.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="9.5">b<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>s</w> <w n="9.6" punct="pt:10">pl<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="10" punct="pt">u</seg>s</rhyme></w>. <del hand="LG" reason="analysis" type="repetition">etc.</del></l>
									<l n="10" num="1.2" lm="10" met="4+6"><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="10.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg></w> <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="10.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" caesura="1">ain</seg></w><caesura></caesura> <w n="10.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="10.8" punct="pt:10">p<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</add></subst></l>
									<l n="11" num="1.3" lm="10" met="4+6"><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="11.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="11.2">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="11.3" punct="vg:4">s<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="M">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="1">ou</seg>t</w>,<caesura></caesura> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6" mp="M">en</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t</w> <w n="11.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" mp="P">an</seg>s</w> <w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="11.8" punct="vg:10">cl<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</add></subst></l>
									<l n="12" num="1.4" lm="10" met="4+6"><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="12.1">G<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2" mp="C">au</seg>x</w> <w n="12.3">p<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3" mp="M">in</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="450" place="4" caesura="1">u</seg>ms</w><caesura></caesura> <w n="12.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="C">u</seg></w> <w n="12.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6" punct="pt:10"><seg phoneme="e" type="vs" value="1" rule="271" place="7" mp="M">Æ</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>d<seg phoneme="i" type="vs" value="1" rule="dc-1" place="9" mp="M">i</seg><rhyme label="a" id="5" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="449" place="10" punct="pt">u</seg>s</rhyme></w>.</add></subst></l>
								</lg>
								<lg n="2" type="quatrain" rhyme="abba">
									<head type="main">MOREL, JOSÉPHINE.</head>
									<l n="13" num="2.1" lm="10" met="4+6"><w n="13.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="13.2" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="vg" caesura="1">ez</seg></w>,<caesura></caesura> <subst hand="LG" reason="analysis" type="repetition"><del>etc.</del><add rend="hidden"><w n="13.3"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="13.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="13.5">b<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>s</w> <w n="13.6" punct="pt:10">pl<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="10" punct="pt">u</seg>s</rhyme></w>.</add></subst></l>
									<l n="14" num="2.2" lm="10" met="4+6"><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="14.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg></w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="14.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" caesura="1">ain</seg></w><caesura></caesura> <w n="14.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="14.7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="14.8" punct="pt:10">p<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</add></subst></l>
									<l n="15" num="2.3" lm="10" met="4+6"><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="15.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="15.2">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="15.3" punct="vg:4">s<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="M">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="1">ou</seg>t</w>,<caesura></caesura> <w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6" mp="M">en</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t</w> <w n="15.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" mp="P">an</seg>s</w> <w n="15.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="15.8" punct="vg:10">cl<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</add></subst></l>
									<l n="16" num="2.4" lm="10" met="4+6"><subst hand="LG" reason="analysis" type="repetition"><del></del><add rend="hidden"><w n="16.1">G<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2" mp="C">au</seg>x</w> <w n="16.3">p<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3" mp="M">in</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="450" place="4" caesura="1">u</seg>ms</w><caesura></caesura> <w n="16.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="C">u</seg></w> <w n="16.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6" punct="pt:10"><seg phoneme="e" type="vs" value="1" rule="271" place="7" mp="M">Æ</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>d<seg phoneme="i" type="vs" value="1" rule="dc-1" place="9" mp="M">i</seg><rhyme label="a" id="7" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="449" place="10" punct="pt">u</seg>s</rhyme></w>.</add></subst></l>
								</lg>
							</div>
						</div></body></text></TEI>