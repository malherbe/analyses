<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TROISIÈME TABLEAU.</head><head type="main_subpart">SCÈNE XIX.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG25" modus="cp" lm_max="10" metProfile="8, 4+6" form="strophe unique" schema="1(ababcdccd)">
						<head type="tune">AIR de la Sentinelle.</head>
						<lg n="1" type="neuvain" rhyme="ababcdccd">
							<head type="main">LESTRADE, accourant.</head>
							<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="1.6">t</w>'<w n="1.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7" mp="M">em</seg>br<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.8" punct="pv:10"><seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>c<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pv">i</seg></rhyme></w> ;</l>
							<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="2.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">m<seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="2.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="5">en</seg></w> <w n="2.5">n</w>'<w n="2.6"><seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.7">t<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="2.8" punct="pt:10">gl<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
							<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="P">a</seg>r</w> <w n="3.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="3.3">t<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="4" caesura="1">en</seg>s</w><caesura></caesura> <w n="3.4">B<seg phoneme="o" type="vs" value="1" rule="443" place="5" mp="M">o</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="317" place="8" mp="M/mc">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="Lc">ou</seg>rd</w>'<w n="3.6">hu<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="490" place="10">i</seg></rhyme></w></l>
							<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>t</w> <w n="4.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2" mp="M">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="4.3"><seg phoneme="y" type="vs" value="1" rule="452" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fc">e</seg></w> <w n="4.4">d<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="4.5" punct="dp:10">v<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>ct<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></w> :</l>
							<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="5.2" punct="vg:2">cl<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="vg">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3" mp="C">au</seg></w> <w n="5.4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">am</seg>p</w><caesura></caesura> <w n="5.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="C">on</seg></w> <w n="5.6">m<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="8">e</seg>st</w> <w n="5.8" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>g<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="vg">a</seg>l</rhyme></w>,</l>
							<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" caesura="1">in</seg></w><caesura></caesura> <w n="6.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="6.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>rs</w> <w n="6.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="6.7">pr<seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>t<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
							<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="4"></space><w n="7.1">D<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg></w> <w n="7.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t</w> <w n="7.4">c<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>p<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>r<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>l</rhyme></w></l>
							<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="4"></space><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="8.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">g<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>r<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>l</rhyme></w></l>
							<l n="9" num="1.9" lm="8" met="8"><space unit="char" quantity="4"></space><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="9.3">c<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="9.5" punct="pt:8">c<seg phoneme="o" type="vs" value="1" rule="434" place="7">o</seg>ll<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						</lg>
					</div></body></text></TEI>