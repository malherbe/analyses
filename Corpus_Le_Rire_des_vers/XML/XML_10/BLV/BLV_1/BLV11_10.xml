<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BANDEAU</title>
				<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="BLL" sort="1">
					<name>
						<forename>Jean-Nicolas</forename>
						<surname>BOUILLY</surname>
					</name>
					<date from="1763" to="1842">1763-1842</date>
				</author>
				<author key="VAN" sort="2">
					<name>
						<forename>Émile</forename>
						<surname>VANDERBURCH</surname>
						<addname type="other">Émile VANDERBURCH</addname>
						<addname type="other">Émile VANDER-BURCH</addname>
						<addname type="other">Émile VANDERBURCK</addname>
						<addname type="other">Émile VAN DER BURCK</addname>
					</name>
					<date from="1794" to="1862">1794-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>168 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE BANDEAU</title>
						<author>BOUILLY ET EM. VANDERBURCH</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GALLICA</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9782259n.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La Bibliothèque nationale de France</repository>
								<idno type="URL">https ://catalogue.bnf.fr/ark :/12148/cb31531335q</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">21 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BLV11" modus="cp" lm_max="12" metProfile="8, 6+6, 4+6" form="suite de strophes" schema="3[abba] 1[abbacca]">
	<lg n="1" type="regexp" rhyme="abbaabbaabbaabbacca">
	<head type="speaker">AMÉLIE.</head>
	<p>
		Écoutez. (Elle lit.)<lb></lb>
		Vous me demandez mon portrait…<lb></lb>
 (s'interrompant.) Nous étions convenus de nous faire chacun<lb></lb>
le nôtre ; mais par écrit seulement. La peinture eût détruit tout<lb></lb>
le piquant, tout le charme de notre situation. ( reprenant.)<lb></lb>
	</p>
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="1.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w> <w n="1.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="1.5" punct="vg:8">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rtr<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="vg">ai</seg>t</rhyme></w>,</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">C</w>'<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="2.3">m</w>'<w n="2.4"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rd<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="2.5">d</w>'<w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.7" punct="pv:8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>c<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
		<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1" punct="vg">ai</seg>s</w>, <w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="3.3" punct="vg:6"><seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>b<seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="3.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="3.5">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="8">ain</seg>s</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="3.7">v<seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>s</w> <w n="3.8" punct="dp:12">d<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>pl<rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></w> :</l>
		<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1" mp="M">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">in</seg></w> <w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="C">ou</seg>s</w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="4.4" punct="pv:6">v<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="M">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="pv" caesura="1">ez</seg></w> ;<caesura></caesura> <w n="4.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="4.6">v<seg phoneme="wa" type="vs" value="1" rule="419" place="8" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg></w> <w n="4.7">tr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>t</w> <w n="4.8">p<seg phoneme="u" type="vs" value="1" rule="424" place="11" mp="P">ou</seg>r</w> <w n="4.9" punct="dp:12">tr<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="12" punct="dp">ai</seg>t</rhyme></w> :</l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">su<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="5.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>s</w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="5.5">v<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>gu<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>r</w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.7">l</w>'<w n="5.8"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="8">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="6.2">n</w>'<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg></w> <w n="6.4">v<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="6.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.7" punct="pv:8">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>t<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8" punct="pv">em</seg>ps</rhyme></w> ;</l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="7.2" punct="vg:2">y<seg phoneme="ø" type="vs" value="1" rule="397" place="2" punct="vg">eu</seg>x</w>, <w n="7.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4">c<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="7.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="vg">an</seg>s</rhyme></w>,</l>
		<l n="8" num="1.8" lm="8" met="8"><w n="8.1" punct="vg:2">Br<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>nt</w>, <w n="8.2">cr<seg phoneme="wa" type="vs" value="1" rule="439" place="3">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="8.3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>r</w> <w n="8.4">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.5" punct="ps:8"><seg phoneme="i" type="vs" value="1" rule="466" place="7">i</seg>m<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></w>…</l>
	<p>
		(s'interrompant.) Je le vois d'ici.<lb></lb>
	</p>
		<l n="9" num="1.9" lm="10" met="4+6"><w n="9.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">on</seg></w> <w n="9.2">t<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="2">ein</seg>t</w> <w n="9.3">br<seg phoneme="y" type="vs" value="1" rule="452" place="3" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="9.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5" mp="P">an</seg>s</w> <w n="9.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6" mp="C">e</seg>s</w> <w n="9.6" punct="vg:7">c<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" punct="vg">am</seg>ps</w>, <w n="9.7"><seg phoneme="o" type="vs" value="1" rule="317" place="8" mp="C">au</seg></w> <w n="9.8" punct="vg:10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="M">om</seg>b<rhyme label="a" id="5" gender="m" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="vg">a</seg>t</rhyme></w>,</l>
		<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">R<seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>d</w> <w n="10.2">s<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="P">u</seg>r</w> <w n="10.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="C">a</seg></w> <w n="10.4">f<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>g<seg phoneme="y" type="vs" value="1" rule="447" place="6" caesura="1">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="10.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg></w> <w n="10.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="9" mp="C">un</seg></w> <w n="10.8">p<seg phoneme="ø" type="vs" value="1" rule="397" place="10">eu</seg></w> <w n="10.9">s<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>v<rhyme label="b" id="6" gender="f" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
		<l n="11" num="1.11" lm="8" met="8"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="11.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>vi<seg phoneme="?" type="va" value="1" rule="161" place="3">en</seg>t</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="11.4">l</w>'<w n="11.5">h<seg phoneme="ɔ" type="vs" value="1" rule="418" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.7" punct="pv:8">gu<rhyme label="b" id="6" gender="f" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
		<l n="12" num="1.12" lm="8" met="8"><w n="12.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="12.2">c</w>'<w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="12.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.6">f<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rd</w> <w n="12.7">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="12.8" punct="pt:8">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>ld<rhyme label="a" id="5" gender="m" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pt">a</seg>t</rhyme></w>.</l>
		<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="13.3">l</w>'<w n="13.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>v<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="M">oû</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6" punct="vg" caesura="1">ai</seg></w>,<caesura></caesura> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="13.7">m<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="M">an</seg>c<seg phoneme="o" type="vs" value="1" rule="443" place="11" mp="M">o</seg>l<rhyme label="a" id="7" gender="f" type="a" stanza="4"><seg phoneme="i" type="vs" value="1" rule="481" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
		<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1" mp="M">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>s</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="14.3">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="5" mp="M">u</seg><seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="14.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="M">om</seg>br<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="300" place="9">ai</seg>t</w> <w n="14.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="10" mp="C">e</seg>s</w> <w n="14.6">b<seg phoneme="o" type="vs" value="1" rule="314" place="11">eau</seg>x</w> <w n="14.7" punct="pv:12">j<rhyme label="b" id="8" gender="m" type="a" stanza="4"><seg phoneme="u" type="vs" value="1" rule="424" place="12" punct="pv">ou</seg>rs</rhyme></w> ;</l>
		<l n="15" num="1.15" lm="10" met="4+6"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="15.2">j<seg phoneme="y" type="vs" value="1" rule="449" place="2" mp="Lc">u</seg>squ</w>'<w n="15.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M/mc">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="15.4">d<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" mp="M">ai</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t</w> <w n="15.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="15.6" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>m<rhyme label="b" id="8" gender="m" type="e" stanza="4"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="vg">ou</seg>rs</rhyme></w>,</l>
		<l n="16" num="1.16" lm="8" met="8"><w n="16.1">D</w>'<w n="16.2"><seg phoneme="y" type="vs" value="1" rule="452" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="16.4">m<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>thr<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>p<rhyme label="a" id="7" gender="f" type="e" stanza="4"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
		<l n="17" num="1.17" lm="12" met="6+6"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="17.2">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2" mp="M">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="17.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4" mp="P">an</seg>s</w> <w n="17.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="C">on</seg></w> <w n="17.5">c<seg phoneme="œ" type="vs" value="1" rule="248" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="17.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="17.7">p<seg phoneme="wa" type="vs" value="1" rule="419" place="8" mp="M">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg></w> <w n="17.8" punct="ps:12">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10" mp="M">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="357" place="11" mp="M">e</seg>rm<rhyme label="c" id="9" gender="m" type="a" stanza="4"><seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="ps">é</seg></rhyme></w>…</l>
		<l n="18" num="1.18" lm="12" met="6+6"><w n="18.1">C</w>'<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="18.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.4">j</w>'<w n="18.5"><seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>s</w> <w n="18.6">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="6" caesura="1">oin</seg></w><caesura></caesura> <w n="18.7">d</w>'<w n="18.8"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="7" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w> <w n="18.9"><seg phoneme="e" type="vs" value="1" rule="188" place="9">e</seg>t</w> <w n="18.10">d</w>'<w n="18.11"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="10">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.12" punct="dp:12"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="11" mp="M">ai</seg>m<rhyme label="c" id="9" gender="m" type="e" stanza="4"><seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="dp">é</seg></rhyme></w> :</l>
		<l n="19" num="1.19" lm="8" met="8"><w n="19.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="19.2">m</w>'<w n="19.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="19.4">gu<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg></w> <w n="19.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>r</w> <w n="19.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="19.7" punct="pt:8">v<rhyme label="a" id="7" gender="f" type="a" stanza="4"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
	</lg> 
</div></body></text></TEI>