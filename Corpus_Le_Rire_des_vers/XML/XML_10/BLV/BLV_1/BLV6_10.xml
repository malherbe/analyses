<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BANDEAU</title>
				<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="BLL" sort="1">
					<name>
						<forename>Jean-Nicolas</forename>
						<surname>BOUILLY</surname>
					</name>
					<date from="1763" to="1842">1763-1842</date>
				</author>
				<author key="VAN" sort="2">
					<name>
						<forename>Émile</forename>
						<surname>VANDERBURCH</surname>
						<addname type="other">Émile VANDERBURCH</addname>
						<addname type="other">Émile VANDER-BURCH</addname>
						<addname type="other">Émile VANDERBURCK</addname>
						<addname type="other">Émile VAN DER BURCK</addname>
					</name>
					<date from="1794" to="1862">1794-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>168 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE BANDEAU</title>
						<author>BOUILLY ET EM. VANDERBURCH</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GALLICA</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9782259n.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La Bibliothèque nationale de France</repository>
								<idno type="URL">https ://catalogue.bnf.fr/ark :/12148/cb31531335q</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">21 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BLV6" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="2(abab)">
	<head type="tune">AIR du Siége de Corinthe.</head>
	<lg n="1" type="quatrain" rhyme="abab"> 
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="1.3" punct="pv:4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4" punct="pv">en</seg>ds</w> ; <w n="1.4" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="vg">ez</seg></w>, <w n="1.5" punct="pv:8">n<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>t<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="ø" type="vs" value="1" rule="397" place="1">eu</seg>x</w> <w n="2.2">f<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="2.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">om</seg>pt<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="2.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.6" punct="vg:8">j<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="vg">ou</seg>r</rhyme></w>,</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4">d<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>p<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="4.3" punct="vg:4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>tr<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg>t</w>, <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="4.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">ou</seg>r</rhyme></w>.</l>
	</lg>
	<lg n="2" rhyme="None">
	<head type="speaker">ENSEMBLE.</head>
	<l ana="unanalyzable" n="5" num="2.1">Je vous attends ; courez, notaire, etc.</l>
	</lg> 
	<lg n="3" type="quatrain" rhyme="abab">
	<head type="speaker">LAPLACE.</head>
		<l n="6" num="3.1" lm="8" met="8"><w n="6.1">D<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>scr<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="188" place="3">e</seg>t</w> <w n="6.3">d<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="6">en</seg>t</w> <w n="6.4" punct="vg:8">n<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
		<l n="7" num="3.2" lm="8" met="8"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="7.3">pr<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="7.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.6" punct="vg:8">j<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="vg">ou</seg>r</rhyme></w>,</l>
		<l n="8" num="3.3" lm="8" met="8"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rs</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="8.3">l</w>'<w n="8.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="8.6" punct="vg:8">m<seg phoneme="i" type="vs" value="1" rule="492" place="7">y</seg>st<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
		<l n="9" num="3.4" lm="8" met="8"><w n="9.1">V<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>tr<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>t</w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="9.4">v<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">ou</seg>r</rhyme></w>.</l>
	</lg> 
</div></body></text></TEI>