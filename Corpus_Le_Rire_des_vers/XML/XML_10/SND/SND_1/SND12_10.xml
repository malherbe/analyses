<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE LIEUTENANT D'ARTILLERIE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="NSL" sort="2">
					<name>
						<forename>Jean-Pierre-Laurent</forename>
						<surname>DENOMBRET</surname>
						<addName type="pen_name">Charles NOMBRET SAINT-LAURENT</addName>
					</name>
					<date from="1791" to="1833">1791-1833</date>
				</author>
					<author key="DUV" sort="3">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>225 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SND_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=NWU:35556007830409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
								<repository>Northwestern university</repository>
								<idno type="URI">https://babel.hathitrust.org/cgi/pt?id=ien.35556007830409</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE XIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SND12" modus="cp" lm_max="10" metProfile="8, 4+6" form="strophe unique" schema="1(ababbcdcd)">
				<head type="tune">Air du Magistrat irréprochable.</head>
				<lg n="1" type="neuvain" rhyme="ababbcdcd">
					<head type="main">BONAPARTE, se tournant vers lui, et avec <lb></lb>l'accent de la plus profonde conviction.</head>
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3">s<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>l<seg phoneme="y" type="vs" value="1" rule="449" place="4" caesura="1">u</seg>t</w><caesura></caesura> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="1.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.7" punct="vg:10">f<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>m<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>bj<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.4" punct="pv:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>p<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="437" place="8" punct="pv">o</seg>s</rhyme></w> ;</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" caesura="1">on</seg></w><caesura></caesura> <w n="3.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="3.5">br<seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="M">i</seg>s<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="3.7" punct="vg:10">B<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>st<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="4.1">Tr<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>t</w> <w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="4.4" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>ch<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="437" place="8" punct="pt">o</seg>s</rhyme></w>. <del hand="LG" type="repetition" reason="analysis">(bis.)</del></l>
					<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="4"></space><subst hand="LG" type="repetition" reason="analysis"><del> </del><add rend="hidden"><w n="5.1">Tr<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>t</w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="5.4" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>ch<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="437" place="8" punct="pt">o</seg>s</rhyme></w>.</add></subst></l>
					<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="4"></space><w n="6.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="1">è</seg>s</w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3" punct="vg:3">n<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="vg">ou</seg>s</w>, <w n="6.4">l</w>'<w n="6.5"><seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.6">qu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg></w> <w n="6.7" punct="vg:8">gr<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="4"></space><w n="7.1">V<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="7.2">r<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381" place="3">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.4">g<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="7.5" punct="pv:8">h<seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>m<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pv">ain</seg></rhyme></w> ;</l>
					<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="8.2">l<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="8.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>r</w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="C">u</seg></w> <w n="8.7" punct="vg:10">m<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="9" num="1.9" lm="10" met="4+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="C">a</seg></w> <w n="9.3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="3" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="4" caesura="1">à</seg></w><caesura></caesura> <w n="9.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="9.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="9.6">m<seg phoneme="ɛ" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="8">en</seg></w> <w n="9.8" punct="pt:10">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" punct="pt">in</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>