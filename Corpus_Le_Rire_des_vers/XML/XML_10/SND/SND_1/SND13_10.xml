<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE LIEUTENANT D'ARTILLERIE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="NSL" sort="2">
					<name>
						<forename>Jean-Pierre-Laurent</forename>
						<surname>DENOMBRET</surname>
						<addName type="pen_name">Charles NOMBRET SAINT-LAURENT</addName>
					</name>
					<date from="1791" to="1833">1791-1833</date>
				</author>
					<author key="DUV" sort="3">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>225 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SND_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=NWU:35556007830409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
								<repository>Northwestern university</repository>
								<idno type="URI">https://babel.hathitrust.org/cgi/pt?id=ien.35556007830409</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE XIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SND13" modus="cp" lm_max="15" metProfile="8, (6+6), (11)" form="suite de strophes" schema="2[abab] 1[ababab]">
				<head type="tune">AIR de M. Doche.</head>
				<lg n="1" type="regexp" rhyme="aba">
					<head type="main">GERMILLY.</head>
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">V<seg phoneme="wa" type="vs" value="1" rule="439" place="1">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="1.4">b<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="1.5">s<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>r</w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="1.7" punct="vg:8">pl<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="2.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">p<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="2.5">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="2.6" punct="pi:8">b<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306" place="7">a</seg>ill<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="pi">on</seg>s</rhyme></w> ?</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">p<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">v<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="3.5" punct="pt:8">gr<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="339" place="8">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="regexp" rhyme="b">
					<head type="main">LES OFFICIERS.</head>
					<l n="4" num="2.1" lm="8" met="8"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="4.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="4.3" punct="ps:4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="ps">a</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> … <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="132" place="5">E</seg>h</w> <w n="4.5" punct="pe:6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="6" punct="pe">en</seg></w> ! <w n="4.6" punct="pe:8">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rch<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="pe">on</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="3" type="regexp" rhyme="a">
					<head type="main">GERMILLY et COURVOLLE.</head>
					<l n="5" num="3.1" lm="15"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M/mp">A</seg>rm<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" mp="Lp">on</seg>s</w>-<w n="5.2" punct="pt:3">n<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="pt">ou</seg>s</w>. <subst hand="LG" reason="analysis" type="repetition"><del>(bis.)</del><add rend="hidden"><w n="5.3"><seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M/mp">A</seg>rm<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="Lp">on</seg>s</w>-<w n="5.4" punct="pt:6">n<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="pt">ou</seg>s</w>.</add></subst> <w n="5.5">Gu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="P">à</seg></w> <w n="5.7">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9">o</seg>rt</w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="P">à</seg></w> <w n="5.9">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="5.10">p<seg phoneme="œ" type="vs" value="1" rule="406" place="12">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="13" mp="F">e</seg></w> <w n="5.11" punct="pe:15">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="14" mp="Mem">e</seg>b<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="15">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="16" punct="pe" mp="F">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="4" type="regexp" rhyme="b">
					<head type="main">BONAPARTE et DELAUNAY, à part.</head>
					<l n="6" num="4.1" lm="12" met="6+6" met_alone="True"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>ls</w> <w n="6.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="6.3" punct="dp:3">d<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="dp">i</seg>t</w> : <w n="6.4">gu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="C">au</seg></w> <w n="6.6" punct="ps:6">p<seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="ps" caesura="1">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> …<caesura></caesura> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="132" place="7">e</seg>h</w> <w n="6.8" punct="pe:8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="8" punct="pe">en</seg></w> ! <w n="6.9">gu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.10"><seg phoneme="o" type="vs" value="1" rule="317" place="10" mp="C">au</seg>x</w> <w n="6.11" punct="pe:12">t<seg phoneme="i" type="vs" value="1" rule="492" place="11" mp="M">y</seg>r<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="12" punct="pe">an</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="5" type="regexp" rhyme="">
					<head type="main">LES OFFICIERS, tirant leurs épées.</head>
					<l part="I" rhyme="none" n="7" num="5.1" lm="11"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="C">Au</seg>x</w> <w n="7.2" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="pe" mp="F">e</seg>s</w> ! <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4" mp="C">au</seg>x</w> <w n="7.4" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" punct="pe" mp="F">e</seg>s</w> ! <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="317" place="7" mp="C">au</seg>x</w> <w n="7.6" punct="pe:9"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" punct="pe" mp="F">e</seg>s</w> ! </l>
				</lg>
				<lg n="6" type="regexp" rhyme="ab">
					<head type="main">BONAPARTE et DELAUNAY, bas.</head>
					<l part="F" rhyme="none" n="7" lm="11"><w n="7.7"><seg phoneme="o" type="vs" value="1" rule="317" place="10" mp="C">Au</seg>x</w> <w n="7.8" punct="pe:11"><seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="pe" mp="F">e</seg>s</w> !</l>
					<l n="8" num="6.1" lm="8" met="8"><w n="8.1" punct="pe:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="pe">i</seg></w> ! <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.3">p<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="8.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>pp<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="9" num="6.2" lm="8" met="8"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>rm<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w>-<w n="9.2" punct="vg:3">n<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="vg">ou</seg>s</w>, <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="9.4">gu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>x</w> <w n="9.6" punct="pe:8">t<seg phoneme="i" type="vs" value="1" rule="492" place="7">y</seg>r<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="pe">an</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="7" type="regexp" rhyme="ab">
					<head type="main">CHŒUR,</head>
					<l n="10" num="7.1" lm="8" met="8"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="10.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg></w> <w n="10.4">r<seg phoneme="wa" type="vs" value="1" rule="422" place="5">oi</seg></w> <w n="10.5">s<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>t</w> <w n="10.6" punct="vg:8">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>d<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="11" num="7.2" lm="8" met="8"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>rm<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w>-<w n="11.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="11.4">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s</w> <w n="11.5">n<seg phoneme="o" type="vs" value="1" rule="437" place="7">o</seg>s</w> <w n="11.6" punct="pt:8">r<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pt">an</seg>gs</rhyme></w>.</l>
				</lg>
				<div n="1" type="section">
					<head type="main">ENSEMBLE.</head>
					<lg n="1" type="regexp" rhyme="ab">
						<head type="main">BONAPARTE et DELAUNAY.</head>
						<l n="12" num="1.1" lm="8" met="8"><w n="12.1" punct="pe:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="pe">i</seg></w> ! <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3">p<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="12.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>pp<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="13" num="1.2" lm="8" met="8"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>rm<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w>-<w n="13.2" punct="vg:3">n<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="vg">ou</seg>s</w>, <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="13.4">gu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>x</w> <w n="13.6" punct="pe:8">t<seg phoneme="i" type="vs" value="1" rule="492" place="7">y</seg>r<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="pe">an</seg>s</rhyme></w> !</l>
					</lg>
					<lg n="2" type="regexp" rhyme="ab">
						<head type="main">CHŒUR.</head>
						<l n="14" num="2.1" lm="8" met="8"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg></w> <w n="14.4">r<seg phoneme="wa" type="vs" value="1" rule="422" place="5">oi</seg></w> <w n="14.5">s<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>t</w> <w n="14.6" punct="pe:8">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>d<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
						<l n="15" num="2.2" lm="8" met="8"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>rm<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w>-<w n="15.2" punct="vg:3">n<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="vg">ou</seg>s</w>, <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="15.4">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s</w> <w n="15.5">n<seg phoneme="o" type="vs" value="1" rule="437" place="7">o</seg>s</w> <w n="15.6" punct="pe:8">r<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pe">an</seg>gs</rhyme></w> !</l>
					</lg>
				</div>
			</div></body></text></TEI>