<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE LIEUTENANT D'ARTILLERIE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="NSL" sort="2">
					<name>
						<forename>Jean-Pierre-Laurent</forename>
						<surname>DENOMBRET</surname>
						<addName type="pen_name">Charles NOMBRET SAINT-LAURENT</addName>
					</name>
					<date from="1791" to="1833">1791-1833</date>
				</author>
					<author key="DUV" sort="3">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>225 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SND_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=NWU:35556007830409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
								<repository>Northwestern university</repository>
								<idno type="URI">https://babel.hathitrust.org/cgi/pt?id=ien.35556007830409</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE PREMIÈRE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SND1" modus="cp" lm_max="10" metProfile="8, 4+6" form="strophe unique" schema="1(ababcdcd)">
				<head type="tune">Air du Vaudeville de la Somnambule.</head>
				<lg n="1" type="huitain" rhyme="ababcdcd">
					<head type="main">GERMILLY, avec humeur.</head>
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="1.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>l</w> <w n="1.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>r</w><caesura></caesura> <w n="1.4"><seg phoneme="i" type="vs" value="1" rule="496" place="5" mp="C">y</seg></w> <w n="1.5">tr<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="M/mp">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="7" mp="Lp">ez</seg></w>-<w n="1.6" punct="vg:8">v<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="vg">ou</seg>s</w>, <w n="1.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="C">on</seg></w> <w n="1.8" punct="pi:10">fr<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1" punct="vg:1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" punct="vg">an</seg>d</w>, <w n="2.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="2.4" punct="vg:4">j<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="1">ou</seg>rs</w>,<caesura></caesura> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5" mp="C">un</seg></w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.7">n<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>v<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="314" place="10">eau</seg></rhyme></w></l>
					<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="3.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="1">en</seg>t</w> <w n="3.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>p<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="3.3">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.4">c<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>l<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="4.2">N<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>ck<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="4.3"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg></w> <w n="4.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="4.5" punct="pt:10">M<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>b<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="314" place="10" punct="pt">eau</seg></rhyme></w>.</l>
					<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="1">en</seg></w> <w n="5.2">mi<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4" punct="vg:4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="5.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="5.6">s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s</w> <w n="5.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>c</w> <w n="5.8">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="5.9" punct="pi:10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="M">on</seg>du<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="490" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="M">Au</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r</w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="6.3" punct="vg:4">n<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="6.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>d</w> <w n="6.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t</w> <w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="6.7" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="352" place="8" mp="M">e</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="338" place="9" mp="M">a</seg>y<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" punct="vg">an</seg>t</rhyme></w>,</l>
					<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1" mp="P">an</seg>s</w> <w n="7.2">l</w>'<w n="7.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg" caesura="1">i</seg>r</w>,<caesura></caesura> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="132" place="5">e</seg>h</w> <w n="7.5" punct="pe:6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="6" punct="pe">en</seg></w> ! <w n="7.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="P">à</seg></w> <w n="7.9" punct="vg:10">l<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="4"></space><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="8.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>l<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="8.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="8.5" punct="pt:8">pr<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>s<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="8" punct="pt">en</seg>t</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>