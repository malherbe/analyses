<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB22" modus="sp" lm_max="5" metProfile="5, 4, (3)">
	<head type="tune">AIR de la Servante justifiée.</head>
	<lg n="1">
	<head type="main">CHŒUR.</head>
		<l n="1" num="1.1" lm="5" met="5"><w n="1.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg>s</w>, <w n="1.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="1.4" punct="vg:5">nu<seg phoneme="i" type="vs" value="1" rule="490" place="5" punct="vg">i</seg>t</w>,</l>
		<l n="2" num="1.2" lm="4" met="4"><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="2.2" punct="vg:2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2" punct="vg">ou</seg>s</w>, <w n="2.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>s</w> <w n="2.4" punct="vg:4">bru<seg phoneme="i" type="vs" value="1" rule="490" place="4" punct="vg">i</seg>t</w>,</l>
		<l n="3" num="1.3" lm="4" met="4"><w n="3.1">S<seg phoneme="wa" type="vs" value="1" rule="419" place="1">oi</seg>t</w> <w n="3.2" punct="pe:4"><seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="338" place="3">a</seg>y<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></w> !</l>
		<l n="4" num="1.4" lm="5" met="5"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="4.2">l</w>'<w n="4.3">h<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="4.4" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424" place="5" punct="vg">ou</seg>x</w>,</l>
		<l n="5" num="1.5" lm="4" met="4"><w n="5.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>du<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w></l>
		<l n="6" num="1.6" lm="4" met="4"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="6.2" punct="pe:4">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></w> !</l>
	</lg>
	<lg n="2">
	<head type="speaker">PELLEYIN.</head>
		<l n="7" num="2.1" lm="3"><w n="7.1">S<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>ls</w> <w n="7.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="7.3" punct="pe:3">d<seg phoneme="ø" type="vs" value="1" rule="397" place="3" punct="pe">eu</seg>x</w> !</l>
		<l n="8" num="2.2" lm="4" met="4"><w n="8.1">C</w>'<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="8.3" punct="pe:4">sc<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>d<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>l<seg phoneme="ø" type="vs" value="1" rule="397" place="4" punct="pe">eu</seg>x</w> !</l>
		<l n="9" num="2.3" lm="4" met="4"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="9.2" punct="pe:4">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="377" place="4">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></w> !</l>
		<l n="10" num="2.4" lm="5" met="5"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="10.2" punct="vg:2">nu<seg phoneme="i" type="vs" value="1" rule="490" place="2" punct="vg">i</seg>t</w>, <w n="10.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>x</w> <w n="10.4" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5" punct="vg">an</seg>s</w>,</l>
		<l n="11" num="2.5" lm="4" met="4"><w n="11.1" punct="vg:2">L<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="11.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.3" punct="vg:4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></w>,</l>
		<l n="12" num="2.6" lm="4" met="4"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3" punct="pe:4">t<seg phoneme="ɛ" type="vs" value="1" rule="360" place="4" punct="pe">e</seg>ms</w> !</l>
	</lg> 
</div></body></text></TEI>