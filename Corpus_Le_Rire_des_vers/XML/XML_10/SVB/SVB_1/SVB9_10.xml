<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB9" modus="cp" lm_max="10" metProfile="8, 4+6" form="suite de strophes" schema="1[abab] 1[aababbcbc] 1[aa]">
	<head type="tune">Air : Fragment du premier final de la Fiancée.</head>
	<head type="main">CHŒUR.</head>
	<lg n="1" type="regexp" rhyme="abab">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="1.2">l</w>'<w n="1.3" punct="vg:4">h<seg phoneme="i" type="vs" value="1" rule="496" place="3">y</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="220" place="4" punct="vg">en</seg></w>, <w n="1.4">s<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="1.5">d<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.6" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>vr<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="351" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="1">e</seg>l</w> <w n="2.2">b<seg phoneme="o" type="vs" value="1" rule="314" place="2">eau</seg></w> <w n="2.3" punct="pe:4">m<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" punct="pe">en</seg>t</w> ! <w n="2.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="5">e</seg>l</w> <w n="2.5">j<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>r</w> <w n="2.6" punct="pe:8">h<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pe">eu</seg>x</rhyme></w> !</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Qu</w>'<w n="3.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">à</seg></w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="3.4">f<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="3.5">ch<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="3.6">s</w>'<w n="3.7" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">em</seg>pr<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="351" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="4.2">l<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r</w> <w n="4.3">b<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="4.4">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rm<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s</w> <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="4.6" punct="pt:8">v<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ø" type="vs" value="1" rule="247" place="8" punct="pt">œu</seg>x</rhyme></w>.</l>
	</lg>
	<lg n="2" type="regexp" rhyme="aa">
	<head type="speaker">PELLEVIN.</head>
		<l n="5" num="2.1" lm="8" met="8"><w n="5.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="5.2" punct="vg:3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg></w>, <w n="5.3">l</w>'<w n="5.4">h<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="5.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>pp<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
		<l n="6" num="2.2" lm="8" met="8"><w n="6.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="6.2">v<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="6.5" punct="pt:8">ch<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>p<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
	</lg>
	<lg n="3" type="regexp" rhyme="">
	<head type="speaker">MIMI, pleurant.</head>
		<l part="I" n="7" num="3.1" lm="8" met="8"><w n="7.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="7.2">n</w>'<w n="7.3"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="7.4" punct="pt:4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="pt">a</seg>s</w>. </l>
	</lg>
	<lg n="4" type="regexp" rhyme="b">
	<head type="speaker">TORS.</head>
		<l part="F" n="7" lm="8" met="8"><w n="7.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5">I</seg>l</w> <w n="7.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.7" punct="pi:8">p<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>rr<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pi">ai</seg>t</rhyme></w> ?</l>
	</lg>
	<lg n="5" type="regexp" rhyme="">
	<head type="speaker">MIMI, de même.</head>
		<l part="I" n="8" num="5.1" lm="8" met="8"><w n="8.1">M<seg phoneme="œ" type="vs" value="1" rule="150" place="1">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="2">eu</seg>r</w> <w n="8.2" punct="ps:4">D<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>pu<seg phoneme="i" type="vs" value="1" rule="490" place="4" punct="ps">i</seg>s</w>… </l>
	</lg>
	<lg n="6" type="regexp" rhyme="a">
	<head type="speaker">PELLEVIN.</head>
		<l part="F" n="8" lm="8" met="8"><w n="8.3" punct="pi:5">H<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="5" punct="pi">ein</seg></w> ? <w n="8.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.5">d<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t</w>-<w n="8.6" punct="pi:8"><rhyme label="a" id="5" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
	</lg>
	<lg n="7" type="regexp" rhyme="bb">
	<head type="speaker">Mme GERVAIS.</head>
		<l n="9" num="7.1" lm="10" met="4+6"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="9.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>s</w> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="8">en</seg></w> <w n="9.6" punct="pt:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>ff<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="10" punct="pt">e</seg>t</rhyme></w>.</l>
		<l n="10" num="7.2" lm="10" met="4+6"><w n="10.1">M<seg phoneme="œ" type="vs" value="1" rule="150" place="1" mp="M">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="2">eu</seg>r</w> <w n="10.2">D<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="M">u</seg>pu<seg phoneme="i" type="vs" value="1" rule="490" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="10.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="C">ou</seg>s</w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t</w> <w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="8" mp="C">un</seg></w> <w n="10.6">b<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>ll<rhyme label="b" id="6" gender="m" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="10">e</seg>t</rhyme></w></l>
		<l part="I" n="11" num="7.3" lm="8" met="8"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="11.2">s</w>'<w n="11.3" punct="pt:4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>xc<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="pt">er</seg></w>. </l>
	</lg>
	<lg n="8" type="regexp" rhyme="cbc">
	<head type="speaker">MIMI,aux invités.</head>
		<l part="F" n="11" lm="8" met="8"><w n="11.4">D<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>r</w> <w n="11.5" punct="pe:8">cr<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg><rhyme label="c" id="5" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
		<l n="12" num="8.1" lm="10" met="4+6"><w n="12.1">J</w>'<w n="12.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="12.3">d<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341" place="4" caesura="1">à</seg></w><caesura></caesura> <w n="12.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="12.5" punct="vg:7">ch<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314" place="7" punct="vg">eau</seg></w>, <w n="12.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="12.7" punct="vg:10">b<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>qu<rhyme label="b" id="6" gender="m" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="10" punct="vg">e</seg>t</rhyme></w>,</l>
		<l n="13" num="8.2" lm="8" met="8"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="13.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="13.3">t<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="13.5">s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="13.6" punct="pe:8">b<rhyme label="c" id="5" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
	</lg> 
	<lg n="9" type="regexp" rhyme="">
	<head type="speaker">PELLEVIN.</head>
		<l part="I" n="14" num="9.1" lm="10" met="4+6"><w n="14.1" punct="vg:2">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1" mp="M">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="2" punct="vg">en</seg>t</w>, <w n="14.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="C">on</seg></w> <w n="14.3" punct="pi:4">g<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4" punct="pi ps" caesura="1">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ?<caesura></caesura> … </l>
	</lg>
	<lg n="10" type="regexp" rhyme="a">
	<head type="speaker">Mme GERVAIS.</head>
		<l part="F" n="14" lm="10" met="4+6"><w n="14.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="C">I</seg>l</w> <w n="14.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="14.6">p<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>t</w> <w n="14.7">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="14.8" punct="vg:10">nu<rhyme label="a" id="7" gender="m" type="a" stanza="3"><seg phoneme="i" type="vs" value="1" rule="490" place="10" punct="vg">i</seg>t</rhyme></w>,</l>
	</lg> 
	<lg n="11" type="regexp" rhyme="">
	<head type="speaker">PELLEVIN</head>
	<l part="I" n="15" num="11.1" lm="10" met="4+6"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="C">a</seg></w> <w n="15.3" punct="pe:4">r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="pe" caesura="1">on</seg></w> !<caesura></caesura> </l>
	</lg>
	<lg n="12" type="regexp" rhyme="a">
	<head type="speaker">Mme GERVAIS, elle lui donne la lettre.</head>
	<l part="F" n="15" lm="10" met="4+6"><w n="15.4">V<seg phoneme="wa" type="vs" value="1" rule="439" place="5" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="15.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="15.6">qu</w>'<w n="15.7"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>l</w> <w n="15.8" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>cr<rhyme label="a" id="7" gender="m" type="e" stanza="3"><seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="vg">i</seg>t</rhyme></w>,</l>
	</lg>
</div></body></text></TEI>