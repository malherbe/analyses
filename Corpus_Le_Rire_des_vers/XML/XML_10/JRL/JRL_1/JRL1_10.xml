<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHABERT</title>
				<title type="sub">HISTOIRE CONTEMPORAINE EN DEUX ACTES, MÊLÉE DE CHANT</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="ARJ" sort="1">
					<name>
						<forename>Jacques</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1790" to="1855">1790-1855</date>
				</author>
				<author key="LUR" sort="2">
					<name>
						<forename>Louis</forename>
						<surname>LURINE</surname>
					</name>
					<date from="1810" to="1860">1810-1860</date>
				</author>
				<editor>Le rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>110 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">JRL_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
				   </p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CHABERT</title>
						<author>JACQUES ARAGO ET LOUIS LURINE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=vldoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849877.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">2 JUILLET 1832</date>
				<placeName>
					<settlement>THÉÂTRE NATIONAL DU VAUDEVILLE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="JRL1" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(ababcdcd)">
	<head type="tune">AIR : Qu'il est flatteur d'épouser celle ( du Fandango).</head>
	<lg n="1" type="huitain" rhyme="ababcdcd">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">em</seg>bl<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg></w> <w n="1.3" punct="vg:5">h<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg></w>, <w n="1.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="1.5">m<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">r<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381" place="3">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>d</w> <w n="2.4">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="6">en</seg>t</w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="2.6" punct="pt:8">nu<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="490" place="8" punct="pt">i</seg>t</rhyme></w>.</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="409" place="1">è</seg>s</w> <w n="3.2">qu</w>'<w n="3.3"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg></w> <w n="3.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="3">e</seg>l</w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.6">j<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="3.7">v<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="3.8" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>r<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="1">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="2">ô</seg>t</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3">s<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="381" place="5">e</seg>il</w> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.5" punct="pt:8">p<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>rsu<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="490" place="8" punct="pt">i</seg>t</rhyme></w>.</l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">d<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>s</w> <w n="5.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="5.6">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="6">ain</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7" punct="pv:8"><seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>c<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="452" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="6.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="6.4">n</w>'<w n="6.5"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="6.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="6.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="6.8" punct="pv:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>r<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="381" place="8" punct="pv">e</seg>il</rhyme></w> ;</l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="7.2">d<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="7.3">s<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="381" place="4">e</seg>il</w> <w n="7.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="7.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="7.6">s<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="7.7">l<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="452" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
		<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="8.4">l<seg phoneme="y" type="vs" value="1" rule="452" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="8.6" punct="pt:8">s<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>l<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="381" place="8" punct="pt">e</seg>il</rhyme></w>.</l>
	</lg>
</div></body></text></TEI>