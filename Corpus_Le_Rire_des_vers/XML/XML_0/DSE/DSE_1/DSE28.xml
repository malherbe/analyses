<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email/>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE28"> 
			<head type="main">LA LIBERTÉ POLITIQUE.</head>

			<head type="tune">AIR : Merveilleuse dans ses vertus.</head>

		 <lg n="1"> <l n="1" num="1.1">Peuples, accourez sans retard,</l>
			<l n="2" num="1.2">Ralliez-vous à ma bannière :</l>
			<l n="3" num="1.3">Un jour verra l’Europe entière</l>
			<l n="4" num="1.4">Marcher sous le même étendard ! ..</l></lg>.

			<p>(au Belge.)</p>
			<lg n="2"><l n="5" num="2.1">Déjà la noble Belgique</l>
			<l n="6" num="2.2">A vu s’accomplir son sort ...</l>
			<p>(au Polonais.) </p>
			<l n="7" num="2.3">Et vous, d’un joug despotique</l>
			<l n="8" num="2.4">Triomphez, Français du Nord !</l></lg>

			<lg n="3"><l n="9" num="3.1">Partout, de canton en canton,</l>
			<l n="10" num="3.2">On me proclame en Helvétie,</l>
			<l n="11" num="3.3">Et l’Allemagne et la Russie</l>
			<l n="12" num="3.4">Tout bas ont répété mon nom ! ...</l></lg>

			<lg n="4"><l n="13" num="4.1">Oui, vers ce peuple d’esclaves</l>
			<l n="14" num="4.2">Je prendrai bientôt mon vol ;</l>
			<l n="15" num="4.3">La cendre de nos vieux braves</l>
			<l n="16" num="4.4">A fertilisé leur sol ...</l></lg>

			<lg n="5"><l n="17" num="5.1">En Espagne bientôt j’irai,</l>
			<l n="18" num="5.2">Et foulant aux pieds ses reliques,</l>
			<l n="19" num="5.3">Je veux par des lauriers civiques</l>
			<l n="20" num="5.4">Anoblir son front tonsuré.</l></lg>

			<lg n="6"><l n="21" num="6.1">Oui, l’Italie elle-même,</l>
			<l n="22" num="6.2">Retrouvant ses anciens dieux,</l>
			<l n="23" num="6.3">Posera son diadème</l>
			<l n="24" num="6.4">Sur mon front victorieux !</l></lg>

			<lg n="7"><l n="25" num="7.1">À Naple on entend aujourd’hui</l>
			<l n="26" num="7.2">Mugir le Vésuve en colère ;</l>
			<l n="27" num="7.3">Mais là, le volcan populaire</l>
			<l n="28" num="7.4">Pourrait éclater avant lui ...</l></lg>
			
		   <lg n="8"> <l n="29" num="8.1">Brisant de saintes entraves,</l>
			<l n="30" num="8.2">Peut-être un jour ce volcan</l>
			<l n="31" num="8.3">Ira répandre ses laves</l>
			<l n="32" num="8.4">Jusqu’au pied du Vatican.</l></lg>
			
			<lg n="9"><l n="33" num="9.1">Première arme du citoyen,</l>
			<l n="34" num="9.2">Déjà, dit-on, dans chaque rue,</l>
			<l n="35" num="9.3">À Milan, le pavé remue</l>
			<l n="36" num="9.4">Sous les pas de l’Autrichien !</l></lg>

		   <lg n="10"> <l n="37" num="10.1">La liberté protectrice,</l>
			<l n="38" num="10.2">À l’abri de sages lois,</l>
			<l n="39" num="10.3">Doit devenir la tutrice</l>
			<l n="40" num="10.4">Et des peuples et des rois !</l></lg>

			<lg n="11"><l n="41" num="11.1">Peuples, accourez sans retard,</l>
			<l n="42" num="11.2">Ralliez-vous à ma bannière ;</l>
			<l n="43" num="11.3">Un jour verra l’Europe entière</l>
		   <l n="44" num="11.4"> Marcher sous le même étendard. <del hand="LN" type="repetition" reason="analysis">(3 fois.)</del></l></lg>
</div></body></text></TEI>