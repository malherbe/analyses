<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email/>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE7">
				
		<head type="main">BIFFARD.</head>

			<p>Et bon époux.</p>
			
		<head type="tune">Air de la Vieille.</head>

				<lg n="1"><l n="1" num="1.1">Ah ! quel ménage délectable !</l>
					<head type="main">ZÉPHIRINE.</head>

					<l n="2" num="1.2">Chacun pourra vivre à son gré.</l>
					<head type="main">BIFFARD.</head>

					<l n="3" num="1.3">C’est entendu .... ! femme adorable !</l>
					<head type="main">ZÉPHIRINE.</head>

					<l part="I" n="4" num="1.4"> Heureux époux ! ...</l>
					<head type="main">BIFFARD.</head>

					<l part="F" n="4" num="1.4">Je le serai.</l>
					<head type="main">ZÉPHIRINE.</head>

					<l part="I" n="5" num="1.5">Liberté pleine !</l>
					<head type="main">BIFFARD.</head>

					<l part="F" n="5" num="1.5">Et bonne table.</l>
					<head type="main">ZÉPHIRINE.</head>

					<l n="6" num="1.6">Un appartement séparé.<del hand="LN" type="repetition" reason="analysis">(bis.)</del></l>
					<l n="7" num="1.7">Quel sort heureux bientôt sera le nôtre !</l>
					<l n="8" num="1.8">La liberté ! c’est mon vœu, c’est le vôtre.</l>
					<head type="main">BIFFARD.</head>

					<l n="9" num="1.9">Oui nous vivrons tous les deux l’un pour l’autre.</l>
					<head type="main">ZÉPHIRINE.</head>

					<l part="I" n="10" num="1.10">Chacun chez nous ...</l>
					<head type="main">BIFFARD.</head>

					<l part="F" n="10" num="1.10">Mon désir est le vôtre.</l>
				<head type="main">ENSEMBLE.</head>

					<l n="11" num="1.11">C’est le bonheur ainsi que je l’entends,</l>
					<l n="12" num="1.12">C’est un ménage du bon temps !</l></lg>	
		</div></body></text></TEI>