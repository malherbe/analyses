<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email/>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE5">
		
			<head type="main">BIFFARD.</head>

			<p>Comment ! il n’y a pas de quoi ? Seriez-vous au nombre de ces esprits endurcis dans l’erreur qui nient les services immenses que nous rendons tous les jours à la monarchie ? détrompez-vous, monsieur.</p>
		
		<head type="tune">AIR : Moi, je flâne.</head>

				<lg n="1"><l n="1" num="1.1">La censure</l>
					<l n="2" num="1.2">Seule est sûre</l>
					<l n="3" num="1.3">Pour gouverner, je l’assure ;</l>
					<l n="4" num="1.4">La censure</l>
					<l n="5" num="1.5">Sans blessure</l>
					<l n="6" num="1.6">Guérit le mal</l>
					<l n="7" num="1.7">Libéral.</l></lg>

				<lg n="2"><l n="8" num="2.1">Pour la France quel danger !</l>
					<l n="9" num="2.2">Sans sa surveillance utile,</l>
					<l n="10" num="2.3">On chantʼrait au Vaudeville</l>
					<l n="11" num="2.4">Jusqu’aux rʼfrains de Bérenger ;</l>
					<l n="12" num="2.5">Et foulant toutes les règles,</l>
					<l n="13" num="2.6">En raillant les fleurs de lis,</l>
					<l n="14" num="2.7">Vous parleriez de vos aigles</l>
					<l n="15" num="2.8">Et des soldats d’Austerlitz.</l>
				</lg>
				
				<lg type="refrain" n="3">
					<l ana="unanalyzable" n="16" num="3.1">La censure, etc.</l>
				</lg>

				<lg n="4"><l n="17" num="4.1">Faut-il dans ses fonctions</l>
					<l n="18" num="4.2">Tourmenter un ministère,</l>
					<l n="19" num="4.3">Et laisser faire au parterre</l>
					<l n="20" num="4.4">D’insolentʼs allusions ?</l>
					<l n="21" num="4.5">Chacun se sert de ses armes ;</l>
					<l n="22" num="4.6">Protectric’ des droits sacrés,</l>
					<l n="23" num="4.7">C’est l’égide des gendarmes,</l>
					<l n="24" num="4.8">Des préfets et des curés.</l>
				</lg>

				<lg n="5">
					<l ana="unanalyzable" n="25" num="5.1">La censure, etc.</l>
				</lg>

				<lg n="6"><l n="26" num="6.1">C’est un énorme attentat !</l>
					<l n="27" num="6.2">Quoi ! ... vos esprits indociles</l>
					<l n="28" num="6.3">Signalent des imbéciles</l>
					<l n="29" num="6.4">Jusques au conseil d’état !</l>
					<l n="30" num="6.5">Pour répondre à cette ligue,</l>
					<l n="31" num="6.6">Faut-il écrir’ tous les jours ?</l>
					<l n="32" num="6.7">Une plume se fatigue ;</l>
					<l n="33" num="6.8">Des ciseaux, ça va toujours.</l>
				</lg>

				<lg n="7">
					<l ana="unanalyzable" n="34" num="7.1">La censure, etc.</l>
				</lg>
	</div></body></text></TEI>