<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">À PROPOS PATRIOTIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="VIL" sort="1">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="MAS" sort="2">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>273 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">VEM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>À propos patriotique</title>
						<author>VILLENEUVE ET MASSON</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=0FA6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>À propos patriotique</title>
								<author>VILLENEUVE ET MASSON</author>
								<repository>Bayerische Staatsbibliothek</repository>
								<idno type="URI">https://opacplus.bsb-muenchen.de/title/BV008031366</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="VEM18">
			<head type="main">LA MARSEILLAISE</head>
			<head type="sub">PAR M. ROUGET DE LISLE.</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="8"/>Allons, enfans de la Patrie !</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"/>Le jour de gloire est arrivé,</l>
					<l n="3" num="1.3"><space unit="char" quantity="8"/>Contre nous de la tyrannie</l>
					<l n="4" num="1.4"><space unit="char" quantity="8"/>L'étendard sanglant est levé.</l>
					<l n="5" num="1.5"><space unit="char" quantity="8"/>Entendez-vous dans les campagnes</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"/>Mugir ces féroces soldats ?</l>
					<l n="7" num="1.7"><space unit="char" quantity="8"/>Ils viennent jusque dans vos bras</l>
					<l n="8" num="1.8"><space unit="char" quantity="8"/>Égorger vos fils, vos compagnes !</l>
					<l n="9" num="1.9">Aux armes, citoyens ! formez vos bataillons,</l>
					<l n="10" num="1.10">Marchons !… qu'un sang impur abreuve nos sillons.</l>
				</lg>
				<lg n="2">
					<l n="11" num="2.1"><space unit="char" quantity="8"/>Que veut cette horde d'esclaves,</l>
					<l n="12" num="2.2"><space unit="char" quantity="8"/>De traîtres, de rois conjurés ?</l>
					<l n="13" num="2.3"><space unit="char" quantity="8"/>Pour qui ces ignobles entraves,</l>
					<l n="14" num="2.4"><space unit="char" quantity="8"/>Ces fers dès long-temps préparés ?</l>
					<l n="15" num="2.5"><space unit="char" quantity="8"/>Français ! pour nous, ah ! quel outrage !</l>
					<l n="16" num="2.6"><space unit="char" quantity="8"/>Quels transports il doit exciter !</l>
					<l n="17" num="2.7"><space unit="char" quantity="8"/>C'est nous qu'on ose méditer</l>
					<l n="18" num="2.8"><space unit="char" quantity="8"/>De rendre à l'antique esclavage …,</l>
					<l n="19" num="2.9">Aux armes, citoyens ! <subst hand="LG" reason="analysis" type="repetition"><del>etc.</del><add rend="hidden">formez vos bataillons,</add></subst></l>
					<l n="20" num="2.10"><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden">Marchons !… qu'un sang impur abreuve nos sillons.</add></subst></l>
				</lg>
				<lg n="3">
					<l n="21" num="3.1"><space unit="char" quantity="8"/>Quoi ! des cohortes étrangères</l>
					<l n="22" num="3.2"><space unit="char" quantity="8"/>Feraient la loi dans nos foyers !</l>
					<l n="23" num="3.3"><space unit="char" quantity="8"/>Quoi ! ces phalanges mercenaires</l>
					<l n="24" num="3.4"><space unit="char" quantity="8"/>Terrasseraient nos fiers guerriers !</l>
					<l n="25" num="3.5"><space unit="char" quantity="8"/>Grand Dieu ! par des mains enchaînées ,</l>
					<l n="26" num="3.6"><space unit="char" quantity="8"/>Nos fronts sous le joug se ploiraient !</l>
					<l n="27" num="3.7"><space unit="char" quantity="8"/>De vils despotes deviendraient</l>
					<l n="28" num="3.8"><space unit="char" quantity="8"/>Les moteurs de nos destinés !…</l>
					<l n="29" num="3.9">Aux armes, citoyens ! <subst hand="LG" reason="analysis" type="repetition"><del>etc.</del><add rend="hidden">formez vos bataillons,</add></subst></l>
					<l n="30" num="3.10"><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden">Marchons !… qu'un sang impur abreuve nos sillons.</add></subst></l>
				</lg>
				<lg n="4">
					<l n="31" num="4.1"><space unit="char" quantity="8"/>Tremblez, tyrans ! et vous , perfides,</l>
					<l n="32" num="4.2"><space unit="char" quantity="8"/>L'opprobre de tous les partis,</l>
					<l n="33" num="4.3"><space unit="char" quantity="8"/>Tremblez ! … Vos projets parricides</l>
					<l n="34" num="4.4"><space unit="char" quantity="8"/>Vont enfin recevoir leur prix.</l>
					<l n="35" num="4.5"><space unit="char" quantity="8"/>Tout est soldat pour vous combattre.</l>
					<l n="36" num="4.6"><space unit="char" quantity="8"/>S'ils tombent nos jeunes héros,</l>
					<l n="37" num="4.7"><space unit="char" quantity="8"/>La terre en produit de nouveaux</l>
					<l n="38" num="4.8"><space unit="char" quantity="8"/>Contre vous tout prêts à se battre !…</l>
					<l n="39" num="4.9">Aux armes, citoyens ! <subst hand="LG" reason="analysis" type="repetition"><del>etc.</del><add rend="hidden">formez vos bataillons,</add></subst></l>
					<l n="40" num="4.10"><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden">Marchons !… qu'un sang impur abreuve nos sillons.</add></subst></l>
				</lg>
				<lg n="5">
					<l n="41" num="5.1"><space unit="char" quantity="8"/>Français, en guerriers magnanimes</l>
					<l n="42" num="5.2"><space unit="char" quantity="8"/>Portez, ou retenez vos coups :</l>
					<l n="43" num="5.3"><space unit="char" quantity="8"/>Épargnez ces tristes victimes</l>
					<l n="44" num="5.4"><space unit="char" quantity="8"/>À regret s'armant contre nous.</l>
					<l n="45" num="5.5"><space unit="char" quantity="8"/>Mais le despote sanguinaire,</l>
					<l n="46" num="5.6"><space unit="char" quantity="8"/>Mais les complices des Bouillé,</l>
					<l n="47" num="5.7"><space unit="char" quantity="8"/>Tous ces tigres qui,sans pitié,</l>
					<l n="48" num="5.8"><space unit="char" quantity="8"/>Déchirent le sein de leur mère !</l>
					<l n="49" num="5.9">Aux armes, citoyens ! <subst hand="LG" reason="analysis" type="repetition"><del>etc.</del><add rend="hidden">formez vos bataillons,</add></subst></l>
					<l n="50" num="5.10"><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden">Marchons !… qu'un sang impur abreuve nos sillons.</add></subst></l>
				</lg>
				<lg n="6">
					<l n="51" num="6.1"><space unit="char" quantity="8"/>Amour sacré de la patrie,</l>
					<l n="52" num="6.2"><space unit="char" quantity="8"/>Conduis, soutiens nos bras vengeurs !</l>
					<l n="53" num="6.3"><space unit="char" quantity="8"/>Liberté, Liberté chérie,</l>
					<l n="54" num="6.4"><space unit="char" quantity="8"/>Combats avec tes défenseurs.</l>
					<l n="55" num="6.5"><space unit="char" quantity="8"/>Sous nos drapeaux que la victoire</l>
					<l n="56" num="6.6"><space unit="char" quantity="8"/>Accoure à tes mâles accens ;</l>
					<l n="57" num="6.7"><space unit="char" quantity="8"/>Que tes ennemis expirans</l>
					<l n="58" num="6.8"><space unit="char" quantity="8"/>Voient ton triomphe et notre gloire.</l>
					<l n="59" num="6.9">Aux armes, citoyens ! <subst hand="LG" reason="analysis" type="repetition"><del>etc.</del><add rend="hidden">formez vos bataillons,</add></subst></l>
					<l n="60" num="6.10"><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden">Marchons !… qu'un sang impur abreuve nos sillons.</add></subst></l>
				</lg>
			</div></body></text></TEI>