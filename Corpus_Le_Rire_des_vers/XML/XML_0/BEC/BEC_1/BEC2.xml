<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES DEMOISELLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="BRA" sort="1">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="2">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LH">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML, application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>54 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BEC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
					<biblFull>
						<titleStmt>
						<title type="main">Les demoiselles</title>
					<author>BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=KF7AyuNs2gAC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque municipale de Lyon</repository>
								<idno type="URL">https://numelyo.bm-lyon.fr/f_view/BML:BML_00GOO0100137001100337604#</idno>
							</monogr>
						</biblStruct>                 
			</sourceDesc>
			</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : U+02BC MODIFIER LETTER APOSTROPHE) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">DEUXIEME ACTE</head><head type="main_subpart">SCÈNE XIV.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BEC2">
					<head type="main">VAUDEVILLE</head>
					<head type="tune">Air : Vaudeville du Diner de garçons.</head>
					<lg n="1">
						<head type="main">MADAME BICHONNET.</head>
						<l n="1" num="1.1">À quel danger vous exposa</l>
						<l n="2" num="1.2">La légèreté de votre âme,</l>
						<l n="3" num="1.3">Vous pouviez perdre, à ce jeu-là,</l>
						<l n="4" num="1.4">Votre recette et votre femme.</l>
						<l n="5" num="1.5">Vous n'êtes plus un Cupidon,</l>
						<l n="6" num="1.6">Le temps vous a rogné les ailes ;</l>
						<l n="7" num="1.7">Souvenez-vous de la leçon,</l>
						<l n="8" num="1.8">Et n'allez plus, vieux papillons</l>
						<l n="9" num="1.9">Voltiger près des demoiselles.</l>
					</lg>
					<lg n="2">
						<head type="main">COQUENARD.</head>
						<l n="10" num="2.1">J'aime mieux un verre de vin</l>
						<l n="11" num="2.2">Q'une fille au gentil sourire ;</l>
						<l n="12" num="2.3">Un' chopin' me met plus en train</l>
						<l n="13" num="2.4">Que le plus beau pied qu'on admire.</l>
						<l n="14" num="2.5">Un' bouteille pleine est, je croi,</l>
						<l n="15" num="2.6">Préférable aux taillʼs les plus belles.</l>
						<l n="16" num="2.7">Un' pinte vaut dix femmʼs pour moi,</l>
						<l n="17" num="2.8">Et pour un' dam' jeanne, ma foi,</l>
						<l n="18" num="2.9">Je donn'rais toutes les demoiselles.</l>
					</lg>
					<lg n="3">
						<head type="main">COLIN.</head>
						<l n="19" num="3.1">Hier au soir, on m'avertit,</l>
						<l n="20" num="3.2">Qu'un brigand dans le bois se montre.</l>
						<l n="21" num="3.3">« C'en est une… me suis-je dit.</l>
						<l n="22" num="3.4">« Courons bien vite à sa rencontre. »</l>
						<l n="23" num="3.5">Je m'y transporte avec ardeur ;</l>
						<l n="24" num="3.6">Je cherche à déployer mon zèle ;</l>
						<l n="25" num="3.7">Mais, que vois-je… au lieu d'un voleur</l>
						<l n="26" num="3.8">C'était mon voisin le <hi rend="ital">paveur</hi>,</l>
						<l n="27" num="3.9">Qui rentrait avec sa <hi rend="ital">d'moiselle</hi>.</l>
					</lg>
					<lg n="4">
						<head type="main">SIMONNE, au public.</head>
						<l n="28" num="4.1">La ptʼite laitière ce soir</l>
						<l n="29" num="4.2">À son mariage vous invite ;</l>
						<l n="30" num="4.3">Nous désirons souvent vous voir,</l>
						<l n="31" num="4.4">Venez donc nous rendre visite.</l>
						<l n="32" num="4.5">Daignez accueillir sans courroux</l>
						<l n="33" num="4.6">Mes invitations nouvelles,</l>
						<l n="34" num="4.7">Messieurs, soyez galans pour nous ;</l>
						<l n="35" num="4.8">Ne manquez pas au rendez-vous</l>
						<l n="36" num="4.9">Que vous donnent les demoiselles.</l>
					</lg>
				</div></body></text></TEI>