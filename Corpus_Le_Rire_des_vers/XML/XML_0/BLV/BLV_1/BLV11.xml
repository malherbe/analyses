<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BANDEAU</title>
				<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="BLL" sort="1">
					<name>
						<forename>Jean-Nicolas</forename>
						<surname>BOUILLY</surname>
					</name>
					<date from="1763" to="1842">1763-1842</date>
				</author>
				<author key="VAN" sort="2">
					<name>
						<forename>Émile</forename>
						<surname>VANDERBURCH</surname>
						<addname type="other">Émile VANDERBURCH</addname>
						<addname type="other">Émile VANDER-BURCH</addname>
						<addname type="other">Émile VANDERBURCK</addname>
						<addname type="other">Émile VAN DER BURCK</addname>
					</name>
					<date from="1794" to="1862">1794-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>168 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE BANDEAU</title>
						<author>BOUILLY ET EM. VANDERBURCH</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GALLICA</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9782259n.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La Bibliothèque nationale de France</repository>
								<idno type="URL">https ://catalogue.bnf.fr/ark :/12148/cb31531335q</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">21 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BLV11">
	<lg n="1">
	<head type="speaker">AMÉLIE.</head>
	<p>
		Écoutez. (Elle lit.)<lb/>
		Vous me demandez mon portrait…<lb/>
 (s'interrompant.) Nous étions convenus de nous faire chacun<lb/>
le nôtre ; mais par écrit seulement. La peinture eût détruit tout<lb/>
le piquant, tout le charme de notre situation. ( reprenant.)<lb/>
	</p>
		<l n="1" num="1.1">Vous me demandez mon portrait,</l>
		<l n="2" num="1.2">C'est m'ordonner d'être sincère ;</l>
		<l n="3" num="1.3">Mais, en obéissant, je crains de vous déplaire :</l>
		<l n="4" num="1.4">Enfin vous le voulez ; me voici trait pour trait :</l>
		<l n="5" num="1.5">Je suis dans la vigueur de l'âge</l>
		<l n="6" num="1.6">Et n'ai vu que trente printemps ;</l>
		<l n="7" num="1.7">Mes yeux, comme ceux des amans,</l>
		<l n="8" num="1.8">Brillent, croyant voir votre image…</l>
	<p>
		(s'interrompant.) Je le vois d'ici.<lb/>
	</p>
		<l n="9" num="1.9">Mon teint bruni dans les camps, au combat,</l>
		<l n="10" num="1.10">Répand sur ma figure un ton un peu sévère</l>
		<l n="11" num="1.11">Qui convient à l'homme de guerre ;</l>
		<l n="12" num="1.12">Car c'est là le fard du soldat.</l>
		<l n="13" num="1.13">Pourtant je l'avoûrai, de la mélancolie</l>
		<l n="14" num="1.14">Quelquefois le nuage ombrageait les beaux jours ;</l>
		<l n="15" num="1.15">Et jusqu'ici dédaignant les amours,</l>
		<l n="16" num="1.16">D'une sombre misanthropie</l>
		<l n="17" num="1.17">Je portais dans mon cœur le poison renfermé…</l>
		<l n="18" num="1.18">C'est que j'avais besoin d'aimer et d'être aimé :</l>
		<l n="19" num="1.19">Vous m'avez guéri pour la vie.</l>
	</lg> 
</div></body></text></TEI>