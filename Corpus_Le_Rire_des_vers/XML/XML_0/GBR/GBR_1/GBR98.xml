<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poèmes et chansons</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Une édition électronique</title>
				<author key="GBR">
					<name>
						<forename>Georges</forename>
						<surname>BRASSENS</surname>
					</name>
					<date from="1921" to="1981">1921-1981</date>
				</author>
				<respStmt>
					<resp>Text preparation, XML encoding (Comic Verse, University of Basel)</resp>
					<name id="NC">
						<forename>Nils</forename>
						<surname>Couturier</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>8159 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email/>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">GBR_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
		  <sourceDesc>
				<biblStruct>
					<monogr>
						<title>Poèmes et chansons</title>
						<author>Georges Brassens</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Seuil</publisher>
							<date when="1993">1993</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1993">1993</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<editorialDecl>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="GBR98">
				<head type="main">La ballade des gens qui sont nés quelque part</head>
				<opener>
					<dateline>
						<date when="1972"> 1972 </date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">C'est vrai qu'ils sont plaisants, tous ces petits villages,</l>
					<l n="2" num="1.2">Tous ces bourgs, ces hameaux, ces lieux-dits, ces cités,</l>
					<l n="3" num="1.3">Avec leurs châteaux forts, leurs églises, leurs plages,</l>
					<l n="4" num="1.4">Ils n'ont qu'un seul point faible et c'est d'être habités,</l>
					<l n="5" num="1.5">Et c'est d'être habités par des gens qui regardent</l>
					<l n="6" num="1.6">Le reste avec mépris du haut de leurs remparts,</l>
					<l n="7" num="1.7">La race des chauvins, des porteurs de cocardes,</l>
					<l n="8" num="1.8">Les imbécilʼs heureux qui sont nés quelque part.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Maudits soient ces enfants de leur mère patrie</l>
					<l n="10" num="2.2">Empalés une fois pour toutʼs sur leur clocher,</l>
					<l n="11" num="2.3">Qui vous montrent leurs tours, leurs muséʼs, leur mairie,</l>
					<l n="12" num="2.4">Vous font voir du pays natal jusqu'à loucher.</l>
					<l n="13" num="2.5">Qu'ils sortent de Paris, ou de Rome, ou de Sète,</l>
					<l n="14" num="2.6">Ou du diable vauvert ou bien de Zanzibar,</l>
					<l n="15" num="2.7">Ou même de Montcuq, ils s'en flattent, mazette,</l>
					<l n="16" num="2.8">Les imbécilʼs heureux qui sont nés quelque part.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Le sable dans lequel, douillettes, leurs autruches</l>
					<l n="18" num="3.2">Enfouissent la tête, on trouve pas plus fin,</l>
					<l n="19" num="3.3">Quant à l'air qu'ils emploient pour gonfler leurs baudruches,</l>
					<l n="20" num="3.4">Leurs bulles de savon, c'est du souffle divin.</l>
					<l n="21" num="3.5">Et, petit à petit, les voilà qui se montent</l>
					<l n="22" num="3.6">Le cou jusqu'à penser que le crottin fait par</l>
					<l n="23" num="3.7">Leurs chevaux, même en bois, rend jaloux tout le monde,</l>
					<l n="24" num="3.8">Les imbécilʼs heureux qui sont nés quelque part.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">C'est pas un lieu commun celui de leur naissance,</l>
					<l n="26" num="4.2">Ils plaignent de tout coeur les pauvres malchanceux,</l>
					<l n="27" num="4.3">Les petits maladroits qui n'eurʼnt pas la présence,</l>
					<l n="28" num="4.4">La présence d'esprit de voir le jour chez eux.</l>
					<l n="29" num="4.5">Quand sonne le tocsin sur leur bonheur précaire,</l>
					<l n="30" num="4.6">Contre les étrangers tous plus ou moins barbares,</l>
					<l n="31" num="4.7">Ils sortent de leur trou pour mourir à la guerre,</l>
					<l n="32" num="4.8">Les imbécilʼs heureux qui sont nés quelque part.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1">Mon Dieu, qu'il ferait bon sur la terre des hommes</l>
					<l n="34" num="5.2">Si l'on n'y rencontrait cette race incongru',</l>
					<l n="35" num="5.3">Cette race importune et qui partout foisonne :</l>
					<l n="36" num="5.4">La race des gens du terroir, des gens du cru.</l>
					<l n="37" num="5.5">Que la viʼserait belle en toutes circonstances</l>
					<l n="38" num="5.6">Si vous n'aviez tiré du néant ces jobards,</l>
					<l n="39" num="5.7">Preuve, peut-être bien, de votre inexistence :</l>
					<l n="40" num="5.8">Les imbécil'heureux qui sont nés quelque part.</l>
				</lg>
			</div></body></text></TEI>