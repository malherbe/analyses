<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poèmes et chansons</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Une édition électronique</title>
				<author key="GBR">
					<name>
						<forename>Georges</forename>
						<surname>BRASSENS</surname>
					</name>
					<date from="1921" to="1981">1921-1981</date>
				</author>
				<respStmt>
					<resp>Text preparation, XML encoding (Comic Verse, University of Basel)</resp>
					<name id="NC">
						<forename>Nils</forename>
						<surname>Couturier</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>8159 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email/>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">GBR_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
		  <sourceDesc>
				<biblStruct>
					<monogr>
						<title>Poèmes et chansons</title>
						<author>Georges Brassens</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Seuil</publisher>
							<date when="1993">1993</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1993">1993</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<editorialDecl>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="GBR95">
				<head type="main">Sale petit bonhomme</head>
				<opener>
					<dateline>
						<date when="1969"> 1969 </date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Sale petit bonhomme, il ne portait plus d'ailes,</l>
					<l n="2" num="1.2">Plus de bandeau sur l'oeil et, d'un huissier modèle,</l>
					<l n="3" num="1.3">Arborait les sombres habits.</l>
					<l n="4" num="1.4">Dès qu'il avait connu le krach, la banqueroute</l>
					<l n="5" num="1.5">De nos affairʼs de coeur, il s'était mis en route</l>
					<l n="6" num="1.6">Pour recouvrer tout son fourbi.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Pas plus tôt descendu de sa noire calèche,</l>
					<l n="8" num="2.2">Il nous a dit : «Je viens récupérer mes flèches</l>
					<l n="9" num="2.3">Maintenant pour vous superfluʼs.»</l>
					<l n="10" num="2.4">Sans une ombre de peine ou de mélancolie,</l>
					<l n="11" num="2.5">On l'a vu remballer la vaine panoplie</l>
					<l n="12" num="2.6">Des amoureux qui ne jouent plus.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Avisant, oublié', la pauvre marguerite</l>
					<l n="14" num="3.2">Qu'on avait effeuillé', jadis, selon le rite,</l>
					<l n="15" num="3.3">Quand on s'aimait un peu, beaucoup,</l>
					<l n="16" num="3.4">L'un après l'autre, en place, il remit les pétales ;</l>
					<l n="17" num="3.5">La veille encore, on aurait crié au scandale,</l>
					<l n="18" num="3.6">On lui aurait tordu le cou.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Il brûla nos trophéʼs, il brûla nos reliques,</l>
					<l n="20" num="4.2">Nos gages, nos portraits, nos lettres idylliques,</l>
					<l n="21" num="4.3">Bien belle fut la part du feu.</l>
					<l n="22" num="4.4">Et je n'ai pas bronché, pas eu la mort dans l'âme,</l>
					<l n="23" num="4.5">Quand, avec tout le reste, il passa par les flammes</l>
					<l n="24" num="4.6">Une boucle de vos cheveux.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Enfin, pour bien montrer qu'il faisait table rase,</l>
					<l n="26" num="5.2">Il effaça du mur l'indélébile phrase :</l>
					<l n="27" num="5.3">«Paul est épris de Virginie.»</l>
					<l n="28" num="5.4">De Virgini', d'Hortense ou bien de Caroline,</l>
					<l n="29" num="5.5">J'oubli'presque toujours le nom de l'héroïne</l>
					<l n="30" num="5.6">Quand la comédie est finie.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">«Faut voir à pas confondre amour et bagatelle,</l>
					<l n="32" num="6.2">À pas trop mélanger la rose et l'immortelle,</l>
					<l n="33" num="6.3">Qu'il nous a dit en se sauvant,</l>
					<l n="34" num="6.4">À pas traiter comme une affaire capitale</l>
					<l n="35" num="6.5">Une petite fantaisi' sentimentale,</l>
					<l n="36" num="6.6">Plus de crédit dorénavant.»</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">Ma mi', ne prenez pas ma complainte au tragique.</l>
					<l n="38" num="7.2">Les raisons qui, ce soir, m'ont rendu nostalgique,</l>
					<l n="39" num="7.3">Sont les moins nobles des raisons,</l>
					<l n="40" num="7.4">Et j'aurais sans nul doute enterré cette histoire</l>
					<l n="41" num="7.5">Si, pour renouveler un peu mon répertoire</l>
					<l n="42" num="7.6">Je n'avais besoin de chansons.</l>
				</lg>
			</div></body></text></TEI>