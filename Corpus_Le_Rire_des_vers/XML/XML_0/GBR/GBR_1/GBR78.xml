<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poèmes et chansons</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Une édition électronique</title>
				<author key="GBR">
					<name>
						<forename>Georges</forename>
						<surname>BRASSENS</surname>
					</name>
					<date from="1921" to="1981">1921-1981</date>
				</author>
				<respStmt>
					<resp>Text preparation, XML encoding (Comic Verse, University of Basel)</resp>
					<name id="NC">
						<forename>Nils</forename>
						<surname>Couturier</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>8159 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email/>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">GBR_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
		  <sourceDesc>
				<biblStruct>
					<monogr>
						<title>Poèmes et chansons</title>
						<author>Georges Brassens</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Seuil</publisher>
							<date when="1993">1993</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1993">1993</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<editorialDecl>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="GBR78">
				<head type="main">Supplique pour être enterré à la plage de Sète</head>
				<opener>
					<dateline>
						<date when="1966"> 1966 </date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">La camarde, qui ne m'a jamais pardonné</l>
					<l n="2" num="1.2">D'avoir semé des fleurs dans les trous de son nez,</l>
					<l n="3" num="1.3">Me poursuit d'un zèle imbécile.</l>
					<l n="4" num="1.4">Alors, cerné de près par les enterrements,</l>
					<l n="5" num="1.5">J'ai cru bon de remettre à jour mon testament,</l>
					<l n="6" num="1.6">De me payer un codicille.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Trempe, dans l'encre bleue du golfe du Lion,</l>
					<l n="8" num="2.2">Trempe, trempe ta plume, ô mon vieux tabellion,</l>
					<l n="9" num="2.3">Et, de ta plus belle écriture,</l>
					<l n="10" num="2.4">Note ce qu'il faudrait qu'il advînt de mon corps,</l>
					<l n="11" num="2.5">Lorsque mon âme et lui ne seront plus d'accord</l>
					<l n="12" num="2.6">Que sur un seul point : la rupture.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Quand mon âme aura pris son vol à l'horizon</l>
					<l n="14" num="3.2">Vers celles de Gavroche et de Mimi Pinson,</l>
					<l n="15" num="3.3">Celles des titis, des grisettes,</l>
					<l n="16" num="3.4">Que vers le sol natal mon corps soit ramené</l>
					<l n="17" num="3.5">Dans un sleeping du «Paris-Méditerranée»,</l>
					<l n="18" num="3.6">Terminus en gare de Sète.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Mon caveau de famille, hélas ! n'est pas tout neuf.</l>
					<l n="20" num="4.2">Vulgairement parlant, il est plein comme un oeuf,</l>
					<l n="21" num="4.3">Et, d'ici que quelqu'un n'en sorte,</l>
					<l n="22" num="4.4">Il risque de se faire tard et je ne peux</l>
					<l n="23" num="4.5">Dire à ces braves gens «Poussez-vous donc un peu !»</l>
					<l n="24" num="4.6">Place aux jeunes en quelque sorte.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Juste au bord de la mer, à deux pas des flots bleus,</l>
					<l n="26" num="5.2">Creusez, si c'est possible, un petit trou moelleux,</l>
					<l n="27" num="5.3">Une bonne petite niche,</l>
					<l n="28" num="5.4">Auprès de mes amis d'enfance, les dauphins,</l>
					<l n="29" num="5.5">Le long de cette grève où le sable est si fin,</l>
					<l n="30" num="5.6">Sur la plage de la Corniche.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">C'est une plage où, même à ses moments furieux,</l>
					<l n="32" num="6.2">Neptune ne se prend jamais trop au sérieux,</l>
					<l n="33" num="6.3">Où, quand un bateau fait naufrage,</l>
					<l n="34" num="6.4">Le capitaine crie : «Je suis le maître à bord !</l>
					<l n="35" num="6.5">Sauve qui peut ! Le vin et le pastis d'abord !</l>
					<l n="36" num="6.6">Chacun sa bonbonne et courage !»</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">Et c'est là que, jadis, à quinze ans révolus,</l>
					<l n="38" num="7.2">À l'âge où s'amuser tout seul ne suffit plus,</l>
					<l n="39" num="7.3">Je connus la prime amourette.</l>
					<l n="40" num="7.4">Auprès d'une sirène, une femme-poisson,</l>
					<l n="41" num="7.5">Je reçus de l'amour la première leçon,</l>
					<l n="42" num="7.6">Avalai la première arête.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1">Déférence gardée envers Paul Valéry,</l>
					<l n="44" num="8.2">Moi, l'humble troubadour, sur lui je renchéris,</l>
					<l n="45" num="8.3">Le bon maître me le pardonne,</l>
					<l n="46" num="8.4">Et qu'au moins, si ses vers valent mieux que les miens,</l>
					<l n="47" num="8.5">Mon cimetière soit plus marin que le sien,</l>
					<l n="48" num="8.6">Et n'en déplaise aux autochtones.</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1">Cette tombe en sandwich, entre le ciel et l'eau,</l>
					<l n="50" num="9.2">Ne donnera pas une ombre triste au tableau,</l>
					<l n="51" num="9.3">Mais un charme indéfinissable.</l>
					<l n="52" num="9.4">Les baigneuses s'en serviront de paravent</l>
					<l n="53" num="9.5">Pour changer de tenue, et les petits enfants</l>
					<l n="54" num="9.6">Diront : «Chouette ! un château de sable !»</l>
				</lg>
				<lg n="10">
					<l n="55" num="10.1">Est-ce trop demander…! Sur mon petit lopin,</l>
					<l n="56" num="10.2">Plantez, je vous prie, une espèce de pin</l>
					<l n="57" num="10.3">Pin parasol, de préférence,</l>
					<l n="58" num="10.4">Qui saura prémunir contre l'insolation</l>
					<l n="59" num="10.5">Les bons amis venus fair'sur ma concession</l>
					<l n="60" num="10.6">D'affectueuses révérences.</l>
				</lg>
				<lg n="11">
					<l n="61" num="11.1">Tantôt venant d'Espagne et tantôt d'Italie,</l>
					<l n="62" num="11.2">Tout chargés de parfums, de musiques jolies,</l>
					<l n="63" num="11.3">Le mistral et la tramontane</l>
					<l n="64" num="11.4">Sur mon dernier sommeil verseront les échos,</l>
					<l n="65" num="11.5">De villanelle un jour, un jour de fandango,</l>
					<l n="66" num="11.6">De tarentelle, de sardane…</l>
				</lg>
				<lg n="12">
					<l n="67" num="12.1">Et quand, prenant ma butte en guise d'oreiller,</l>
					<l n="68" num="12.2">Une ondine viendra gentiment sommeiller</l>
					<l n="69" num="12.3">Avec moins que rien de costume,</l>
					<l n="70" num="12.4">J'en demande pardon par avance à Jésus,</l>
					<l n="71" num="12.5">Si l'ombre de ma croix s'y couche un peu dessus</l>
					<l n="72" num="12.6">Pour un petit bonheur posthume.</l>
				</lg>
				<lg n="13">
					<l n="73" num="13.1">Pauvres rois, pharaons ! Pauvre Napoléon !</l>
					<l n="74" num="13.2">Pauvres grands disparus gisant au Panthéon !</l>
					<l n="75" num="13.3">Pauvres cendres de conséquence !</l>
					<l n="76" num="13.4">Vous envierez un peu l'éternel estivant,</l>
					<l n="77" num="13.5">Qui fait du pédalo sur la vague en rêvant,</l>
					<l n="78" num="13.6">Qui passe sa mort en vacances…</l>
				</lg>
			</div></body></text></TEI>