<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poèmes et chansons</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Une édition électronique</title>
				<author key="GBR">
					<name>
						<forename>Georges</forename>
						<surname>BRASSENS</surname>
					</name>
					<date from="1921" to="1981">1921-1981</date>
				</author>
				<respStmt>
					<resp>Text preparation, XML encoding (Comic Verse, University of Basel)</resp>
					<name id="NC">
						<forename>Nils</forename>
						<surname>Couturier</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>8159 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email/>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">GBR_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
		  <sourceDesc>
				<biblStruct>
					<monogr>
						<title>Poèmes et chansons</title>
						<author>Georges Brassens</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Seuil</publisher>
							<date when="1993">1993</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1993">1993</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<editorialDecl>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="GBR53">
				<head type="main">L'orage</head>
				<opener>
					<dateline>
						<date when="1960"> 1960 </date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Parlez-moi de la pluie et non pas du beau temps,</l>
					<l n="2" num="1.2">Le beau temps me dégoûte et m'fait grincer les dents,</l>
					<l n="3" num="1.3">Le bel azur me met en rage,</l>
					<l n="4" num="1.4">Car le plus grand amour qui m'fut donné sur terr'</l>
					<l n="5" num="1.5">Je l'dois au mauvais temps, je l'dois à Jupiter,</l>
					<l n="6" num="1.6">Il me tomba d'un ciel d'orage.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Par un soir de novembre, à cheval sur les toits,</l>
					<l n="8" num="2.2">Un vrai tonnerr'de Brest, avec des cris d'putois,</l>
					<l n="9" num="2.3">Allumait ses feux d'artifice.</l>
					<l n="10" num="2.4">Bondissant de sa couche en costume de nuit,</l>
					<l n="11" num="2.5">Ma voisine affolé'vint cogner à mon huis</l>
					<l n="12" num="2.6">En réclamant mes bons offices.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">«Je suis seule et j'ai peur, ouvrez-moi, par pitié,</l>
					<l n="14" num="3.2">Mon époux vient d'partir faire son dur métier,</l>
					<l n="15" num="3.3">Pauvre malheureux mercenaire,</l>
					<l n="16" num="3.4">Contraint d'coucher dehors quand il fait mauvais temps,</l>
					<l n="17" num="3.5">Pour la bonne raison qu'il est représentant</l>
					<l n="18" num="3.6">D'un'maison de paratonnerres.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">En bénissant le nom de Benjamin Franklin,</l>
					<l n="20" num="4.2">Je l'ai mise en lieu sûr entre mes bras câlins,</l>
					<l n="21" num="4.3">Et puis l'amour a fait le reste !</l>
					<l n="22" num="4.4">Toi qui sèmes des paratonnerre'à foison,</l>
					<l n="23" num="4.5">Que n'en as-tu planté sur ta propre maison ?</l>
					<l n="24" num="4.6">Erreur on ne peut plus funeste.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Quand Jupiter alla se faire entendre ailleurs,</l>
					<l n="26" num="5.2">La belle, ayant enfin conjuré sa frayeur</l>
					<l n="27" num="5.3">Et recouvré tout son courage,</l>
					<l n="28" num="5.4">Rentra dans ses foyers fair'sécher son mari</l>
					<l n="29" num="5.5">En m'donnant rendez-vous les jours d'intempéri',</l>
					<l n="30" num="5.6">Rendez-vous au prochain orage.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">À partir de ce jour j'n'ai plus baissé les yeux,</l>
					<l n="32" num="6.2">J'ai consacré mon temps à contempler les cieux,</l>
					<l n="33" num="6.3">À regarder passer les nues,</l>
					<l n="34" num="6.4">À guetter les stratus, à lorgner les nimbus,</l>
					<l n="35" num="6.5">À faire les yeux doux aux moindres cumulus,</l>
					<l n="36" num="6.6">Mais elle n'est pas revenue.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">Son bonhomm'de mari avait tant fait d'affairʼs,</l>
					<l n="38" num="7.2">Tant vendu ce soir-là de petits bouts de fer,</l>
					<l n="39" num="7.3">Qu'il était dev'nu millionnaire</l>
					<l n="40" num="7.4">Et l'avait emmené'vers des cieux toujours bleus,</l>
					<l n="41" num="7.5">Des pays imbécile'où jamais il ne pleut,</l>
					<l n="42" num="7.6">Où l'on ne sait rien du tonnerre.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1">Dieu fass'que ma complainte aille, tambour battant,</l>
					<l n="44" num="8.2">Lui parler de la plui', lui parler du gros temps</l>
					<l n="45" num="8.3">Auxquels on a tʼnu tête ensemble,</l>
					<l n="46" num="8.4">Lui conter qu'un certain coup de foudre assassin</l>
					<l n="47" num="8.5">Dans le mill'de mon coeur a laissé le dessin</l>
					<l n="48" num="8.6">D'un'petit'fleur qui lui ressemble.</l>
				</lg>
			</div></body></text></TEI>