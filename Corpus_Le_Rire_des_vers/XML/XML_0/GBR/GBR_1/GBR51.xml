<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poèmes et chansons</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Une édition électronique</title>
				<author key="GBR">
					<name>
						<forename>Georges</forename>
						<surname>BRASSENS</surname>
					</name>
					<date from="1921" to="1981">1921-1981</date>
				</author>
				<respStmt>
					<resp>Text preparation, XML encoding (Comic Verse, University of Basel)</resp>
					<name id="NC">
						<forename>Nils</forename>
						<surname>Couturier</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>8159 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email/>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">GBR_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
		  <sourceDesc>
				<biblStruct>
					<monogr>
						<title>Poèmes et chansons</title>
						<author>Georges Brassens</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Seuil</publisher>
							<date when="1993">1993</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1993">1993</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<editorialDecl>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="GBR51">
				<head type="main">La ballade des cimetières</head>
				<opener>
					<dateline>
						<date when="1962"> 1962 </date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">J'ai des tombeaux en abondance,</l>
					<l n="2" num="1.2">Des sépulturʼs à discrétion,</l>
					<l n="3" num="1.3">Dans tout cimʼtièr'd'quelque importance</l>
					<l n="4" num="1.4">J'ai ma petite concession.</l>
					<l n="5" num="1.5">De l'humble terre au mausolée,</l>
					<l n="6" num="1.6">Avec toujours quelqu'un dedans,</l>
					<l n="7" num="1.7">J'ai des pʼtitʼs bossʼs plein les allées,</l>
					<l n="8" num="1.8">Et je suis triste cependant…</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Car j'n'en ai pas, et ça m'agace,</l>
					<l n="10" num="2.2">Et ça défrise mon blason,</l>
					<l n="11" num="2.3">Au cimetièr'du Montparnasse,</l>
					<l n="12" num="2.4">À quatre pas de ma maison.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">J'en possède au Père-Lachaise,</l>
					<l n="14" num="3.2">à Bagneux, à Thiais, à Pantin,</l>
					<l n="15" num="3.3">Et jusque, ne vous en déplaise,</l>
					<l n="16" num="3.4">Au fond du cimetièr'marin,</l>
					<l n="17" num="3.5">À la vil'comm'à la campagne,</l>
					<l n="18" num="3.6">Partout où l'on peut faire un trou,</l>
					<l n="19" num="3.7">J'ai mêm'des tombeaux en Espagne</l>
					<l n="20" num="3.8">Qu'on me jalouse peu ou prou…</l>
					<l n="21" num="3.9">Mais j'n'en ai pas la moindre trace,</l>
					<l n="22" num="3.10">Le plus humble petit soupçon,</l>
					<l n="23" num="3.11">Au cimetièr'du Montparnasse,</l>
					<l n="24" num="3.12">À quatre pas de ma maison.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">Le jour des morts, je cours, je vole,</l>
					<l n="26" num="4.2">Je vais, infatigablement,</l>
					<l n="27" num="4.3">De nécropole en nécropole,</l>
					<l n="28" num="4.4">De pierr'tombale en monument.</l>
					<l n="29" num="4.5">On m'entrevoit sous un'couronne</l>
					<l n="30" num="4.6">D'immortelles à Champerret,</l>
					<l n="31" num="4.7">Un peu plus tard, c'est à Charonne</l>
					<l n="32" num="4.8">Qu'on m'aperçoit sous un cyprès…</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1">Mais, seul, un fourbe aura l'audace</l>
					<l n="34" num="5.2">De dir'«J'l'ai vu à l'horizon</l>
					<l n="35" num="5.3">Du cimetièr'du Montparnasse,</l>
					<l n="36" num="5.4">À quatre pas de sa maison.»</l>
				</lg>
				<lg n="6">
					<l n="37" num="6.1">Devant l'château de ma grand-tante</l>
					<l n="38" num="6.2">La marquise de Carabas,</l>
					<l n="39" num="6.3">Ma saint'famille languit d'attente :</l>
					<l n="40" num="6.4">Mourra-t-ell', mourra-t-elle pas ?</l>
					<l n="41" num="6.5">L'un veut son or, l'autre ses meubles,</l>
					<l n="42" num="6.6">Qui ses bijoux, qui ses bibʼlots,</l>
					<l n="43" num="6.7">Qui ses forêts, qui ses immeubles,</l>
					<l n="44" num="6.8">Qui ses tapis, qui ses tableaux…</l>
				</lg>
				<lg n="7">
					<l n="45" num="7.1">Moi, je n'implore qu'une grâce,</l>
					<l n="46" num="7.2">C'est qu'ell'pass'la morte-saison</l>
					<l n="47" num="7.3">Au cimetièr'du Montparnasse,</l>
					<l n="48" num="7.4">À quatre pas de ma maison.</l>
				</lg>
				<lg n="8">
					<l n="49" num="8.1">Ainsi chantait, la mort dans l'âme,</l>
					<l n="50" num="8.2">Un jeun'homm'de bonne tenue,</l>
					<l n="51" num="8.3">En train de ranimer la flamme</l>
					<l n="52" num="8.4">Du soldat qui lui'était connu,</l>
					<l n="53" num="8.5">Or, il advint qu'le ciel eut marr'de</l>
					<l n="54" num="8.6">L'entendre parler d'ses caveaux.</l>
					<l n="55" num="8.7">Et Dieu fit signe à la camarde</l>
					<l n="56" num="8.8">De l'expédier ru'Froidevaux…</l>
				</lg>
				<lg n="9">
					<l n="57" num="9.1">Mais les croquʼ-morts, qui'étaient de Chartre',</l>
					<l n="58" num="9.2">Funeste erreur de livraison,</l>
					<l n="59" num="9.3">Menèrʼnt sa dépouille à Montparnasse,</l>
					<l n="60" num="9.4">De l'autr'côté de sa maison.</l>
				</lg>
			</div></body></text></TEI>