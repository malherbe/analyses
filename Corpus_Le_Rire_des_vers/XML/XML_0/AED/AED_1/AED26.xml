<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">27, 28 ET 29 JUILLET, TABLEAU ÉPISODIQUE DES TROIS JOURNÉES.</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ARA" sort="1">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<author key="DUV" sort="2">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>495 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
						<author>ARAGO ET DUVERT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=xjVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
								<author>ARAGO ET DUVERT</author>
								<repository>Österreichische Nationalbibliothek</repository>
								<idno type="URI">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886206</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : U+02BC MODIFIER LETTER APOSTROPHE) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<change when="2021-06-14" who="RR">Quelques corrections concernant la distribution du signe signe ʼ (UNICODE : ʼ).</change>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TROISIÈME JOURNÉE</head><head type="main_subpart">SCÈNE VI.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="AED26">
			<head type="tune">AIR : Gai, gai, mariez-vous.</head>
				<lg n="1">
					<head type="main">COLOMBON.</head>
					<l n="1" num="1.1"><space unit="char" quantity="4"/>Non, non, donnons-nous lʼ bras,</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"/>Qu' notrʼ alliance</l>
					<l n="3" num="1.3"><space unit="char" quantity="10"/>Sauvʼ la France ;</l>
					<l n="4" num="1.4"><space unit="char" quantity="4"/>Non, non, donnons-nous lʼ bras</l>
					<l n="5" num="1.5"><space unit="char" quantity="2"/>Les Jésuitʼs ne rʼviendront pas.</l>
				</lg>
				
				<lg n="2">
					<head type="main">EN CHŒUR.</head>
					<l n="6" num="2.1"><space unit="char" quantity="4"/>Non, non, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden">donnons-nous lʼ bras,</add></subst></l>
					<l n="7" num="2.2"><space unit="char" quantity="8"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Qu' notrʼ alliance</add></subst></l>
					<l n="8" num="2.3"><space unit="char" quantity="10"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Sauvʼ la France ; </add></subst></l>
					<l n="9" num="2.4"><space unit="char" quantity="4"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Non, non, donnons-nous lʼ bras</add></subst></l>
					<l n="10" num="2.5"><space unit="char" quantity="2"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Les Jésuitʼs ne rʼviendront pas. </add></subst></l>
				</lg>
				
				<lg n="3">
					<head type="main">COLOMBON.</head>
					<l n="11" num="3.1"><space unit="char" quantity="2"/>Après nos glorieux travaux,</l>
					<l n="12" num="3.2"><space unit="char" quantity="2"/>S'ils revenaient, ces transfuges,</l>
					<l n="13" num="3.3"><space unit="char" quantity="2"/>Nous aurions au lieu de juges</l>
					<l n="14" num="3.4"><space unit="char" quantity="2"/>Des prévôts et des bourreaux ?</l>
					<l n="15" num="3.5"><space unit="char" quantity="4"/>Non, non, donnons-nous lʼ bras !</l>
					<l n="16" num="3.6"><space unit="char" quantity="10"/>Qu'on bénisse</l>
					<l n="17" num="3.7"><space unit="char" quantity="10"/>La justice ;</l>
					<l n="18" num="3.8"><space unit="char" quantity="4"/>Non, non, donnons-nous lʼ bras !</l>
					<l n="19" num="3.9"><space unit="char" quantity="2"/>Les prévôts ne rʼviendront pas.</l>
				</lg>
				
				<lg n="4">
					<head type="main">EN CHŒUR.</head>
					<l n="20" num="4.1"><space unit="char" quantity="4"/>Non, non, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden">donnons-nous lʼ bras,</add></subst></l>
					<l n="21" num="4.2"><space unit="char" quantity="8"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Qu' notrʼ alliance</add></subst></l>
					<l n="22" num="4.3"><space unit="char" quantity="10"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Sauvʼ la France ; </add></subst></l>
					<l n="23" num="4.4"><space unit="char" quantity="4"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Non, non, donnons-nous lʼ bras</add></subst></l>
					<l n="24" num="4.5"><space unit="char" quantity="2"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Les Jésuitʼs ne rʼviendront pas. </add></subst></l>
					</lg>
				
				<lg n="5">
					<head type="main">RAIMOND.</head>
					<l n="25" num="5.1"><space unit="char" quantity="2"/>Nous rʼverrions ces noirs Judas</l>
					<l n="26" num="5.2"><space unit="char" quantity="2"/>Envahir les ministères</l>
					<l n="27" num="5.3"><space unit="char" quantity="2"/>Et doter les séminaires</l>
					<l n="28" num="5.4"><space unit="char" quantity="2"/>Aux dépens dʼ nos vieux soldats ?</l>
					<l n="29" num="5.5"><space unit="char" quantity="4"/>Non, non, donnons-nous lʼ bras !</l>
					<l n="30" num="5.6"><space unit="char" quantity="8"/>Plus dʼ ces ministres</l>
					<l n="31" num="5.7"><space unit="char" quantity="12"/>Sinistres !</l>
					<l n="32" num="5.8"><space unit="char" quantity="4"/>Non, non, donnons-nous lʼ bras !</l>
					<l n="33" num="5.9"><space unit="char" quantity="2"/>Les traîtres ne rʼviendront pas.</l>
				</lg>
				
				<lg n="6">
					<head type="main">EN CHŒUR.</head>
					<l n="34" num="6.1"><space unit="char" quantity="4"/>Non, non, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden">donnons-nous lʼ bras,</add></subst></l>
					<l n="35" num="6.2"><space unit="char" quantity="8"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Qu' notrʼ alliance</add></subst></l>
					<l n="36" num="6.3"><space unit="char" quantity="10"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Sauvʼ la France ; </add></subst></l>
					<l n="37" num="6.4"><space unit="char" quantity="4"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Non, non, donnons-nous lʼ bras</add></subst></l>
					<l n="38" num="6.5"><space unit="char" quantity="2"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Les Jésuitʼs ne rʼviendront pas. </add></subst></l>
					</lg>
				
				<lg n="7">
					<head type="main">LE GARDE NATIONAL.</head>
					<l n="39" num="7.1"><space unit="char" quantity="2"/>Quoi ! nous rʼverrions désormais</l>
					<l n="40" num="7.2"><space unit="char" quantity="2"/>Ces gendarmʼs patibulaires</l>
					<l n="41" num="7.3"><space unit="char" quantity="2"/>Dont les sabres mercenaires</l>
					<l n="42" num="7.4"><space unit="char" quantity="2"/>N'ont versé qu' du sang français ?</l>
					<l n="43" num="7.5"><space unit="char" quantity="4"/>Non, non, donnons nous lʼ bras !</l>
					<l n="44" num="7.6"><space unit="char" quantity="10"/>Vils gendarmes !</l>
					<l n="45" num="7.7"><space unit="char" quantity="10"/>Bas les armes !</l>
					<l n="46" num="7.8"><space unit="char" quantity="4"/>Non, non, donnons-nous lʼ bras !</l>
					<l n="47" num="7.9"><space unit="char" quantity="2"/>Les gendarmʼs ne rʼviendront pas.</l>
				</lg>
				
				<lg n="8">
					<head type="main">EN CHŒUR.</head>
					<l n="48" num="8.1"><space unit="char" quantity="4"/>Non, non, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden">donnons-nous lʼ bras,</add></subst></l>
					<l n="49" num="8.2"><space unit="char" quantity="8"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Qu' notrʼ alliance</add></subst></l>
					<l n="50" num="8.3"><space unit="char" quantity="10"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Sauvʼ la France ; </add></subst></l>
					<l n="51" num="8.4"><space unit="char" quantity="4"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Non, non, donnons-nous lʼ bras</add></subst></l>
					<l n="52" num="8.5"><space unit="char" quantity="2"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Les Jésuitʼs ne rʼviendront pas. </add></subst></l>
					</lg>
				
				<lg n="9">
					<head type="main">ATKINSON.</head>
					<l n="53" num="9.1"><space unit="char" quantity="2"/>Pour chaqu' croyancʼ plus dʼ tracas,</l>
					<l n="54" num="9.2"><space unit="char" quantity="2"/>Et la procession qui passe</l>
					<l n="55" num="9.3"><space unit="char" quantity="2"/>Ne forcʼra plus sur la place</l>
					<l n="56" num="9.4"><space unit="char" quantity="2"/>Le Juif à mettrʼ chapeau bas.</l>
					<l n="57" num="9.5"><space unit="char" quantity="4"/>Non, non, donnez-vous lʼ bras,</l>
					<l n="58" num="9.6"><space unit="char" quantity="8"/>Qu' la tolérance</l>
					<l n="59" num="9.7"><space unit="char" quantity="10"/>Règne en France ;</l>
					<l n="60" num="9.8"><space unit="char" quantity="4"/>Non, non, donnez-vous lʼ bras !</l>
					<l n="61" num="9.9"><space unit="char" quantity="2"/>Les tartuffʼs ne rʼviendront pas.</l>
				</lg>
				
				<lg n="10">
					<head type="main">EN CHŒUR.</head>
					<l n="62" num="10.1"><space unit="char" quantity="4"/>Non, non, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden">donnons-nous lʼ bras,</add></subst></l>
					<l n="63" num="10.2"><space unit="char" quantity="8"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Qu' notrʼ alliance</add></subst></l>
					<l n="64" num="10.3"><space unit="char" quantity="10"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Sauvʼ la France ; </add></subst></l>
					<l n="65" num="10.4"><space unit="char" quantity="4"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Non, non, donnons-nous lʼ bras</add></subst></l>
					<l n="66" num="10.5"><space unit="char" quantity="2"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Les Jésuitʼs ne rʼviendront pas. </add></subst></l>
				</lg>
				
				<lg n="11">
					<head type="main">ADOLPHE.</head>
					<l n="67" num="11.1"><space unit="char" quantity="2"/>Plus de censeurs infernaux,</l>
					<l n="68" num="11.2"><space unit="char" quantity="2"/>Gendarmes de la pensée,</l>
					<l n="69" num="11.3"><space unit="char" quantity="2"/>Que leur horde soit chassée !</l>
					<l n="70" num="11.4"><space unit="char" quantity="2"/>Plus dʼ baillons pour les journaux.</l>
					<l n="71" num="11.5"><space unit="char" quantity="4"/>Non, non, donnons-nous lʼ bras,</l>
					<l n="72" num="11.6"><space unit="char" quantity="10"/>L'imprimʼrie</l>
					<l n="73" num="11.7"><space unit="char" quantity="8"/>Est affranchie ;</l>
					<l n="74" num="11.8"><space unit="char" quantity="4"/>Non, non, donnons-nous lʼ bras</l>
					<l n="75" num="11.9"><space unit="char" quantity="2"/>Les censeurs ne rʼviendront pas.</l>
				</lg>
				
				<lg n="12">
					<head type="main">EN CHŒUR.</head>
					<l n="76" num="12.1"><space unit="char" quantity="4"/>Non, non, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden">donnons-nous lʼ bras,</add></subst></l>
					<l n="77" num="12.2"><space unit="char" quantity="8"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Qu' notrʼ alliance</add></subst></l>
					<l n="78" num="12.3"><space unit="char" quantity="10"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Sauvʼ la France ; </add></subst></l>
					<l n="79" num="12.4"><space unit="char" quantity="4"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Non, non, donnons-nous lʼ bras</add></subst></l>
					<l n="80" num="12.5"><space unit="char" quantity="2"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Les Jésuitʼs ne rʼviendront pas. </add></subst></l>
				</lg>
				
				<lg n="13">
					<head type="main">L'APPRENTI.</head>
					<l n="81" num="13.1"><space unit="char" quantity="2"/>Aux sottisʼs du spirituel</l>
					<l n="82" num="13.2"><space unit="char" quantity="2"/>La raison va mettʼ des bornes,</l>
					<l n="83" num="13.3"><space unit="char" quantity="2"/>Et les frères à trois cornes,</l>
					<l n="84" num="13.4"><space unit="char" quantity="2"/>N'enfoncʼront plus lʼ mutuel.</l>
					<l n="85" num="13.5"><space unit="char" quantity="4"/>Non, non, donnons-nous lʼ bras,</l>
					<l n="86" num="13.6"><space unit="char" quantity="10"/>Plus dʼ férule</l>
					<l n="87" num="13.7"><space unit="char" quantity="10"/>Ridicule,</l>
					<l n="88" num="13.8"><space unit="char" quantity="4"/>Non, non, donnons-nous lʼ bras !</l>
					<l n="89" num="13.9"><space unit="char" quantity="2"/>Les fouetteurs ne rʼviendront pas.</l>
				</lg>
				
				<lg n="14">
					<head type="main">EN CHŒUR.</head>
					<l n="90" num="14.1"><space unit="char" quantity="4"/>Non, non, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden">donnons-nous lʼ bras,</add></subst></l>
					<l n="91" num="14.2"><space unit="char" quantity="8"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Qu' notrʼ alliance</add></subst></l>
					<l n="92" num="14.3"><space unit="char" quantity="10"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Sauvʼ la France ; </add></subst></l>
					<l n="93" num="14.4"><space unit="char" quantity="4"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Non, non, donnons-nous lʼ bras</add></subst></l>
					<l n="94" num="14.5"><space unit="char" quantity="2"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Les Jésuitʼs ne rʼviendront pas. </add></subst></l>
				</lg>
				
				<lg n="15">
					<head type="main">UN JEUNE BOURGEOIS.</head>
					<l n="95" num="15.1"><space unit="char" quantity="2"/>Gens de la congrégation,</l>
					<l n="96" num="15.2"><space unit="char" quantity="2"/>Dans votʼ systèmʼ déplorable,</l>
					<l n="97" num="15.3"><space unit="char" quantity="2"/>Molièr sʼrait un misérable ?</l>
					<l n="98" num="15.4"><space unit="char" quantity="2"/>Talma sʼrait un histrion ?</l>
					<l n="99" num="15.5"><space unit="char" quantity="4"/>Non, non, donnons-nous lʼ bras !</l>
					<l n="100" num="15.6"><space unit="char" quantity="10"/>Qu'on les traîne</l>
					<l n="101" num="15.7"><space unit="char" quantity="10"/>Sur la scène !</l>
					<l n="102" num="15.8"><space unit="char" quantity="4"/>Non, non, donnons-nous lʼ bras !</l>
					<l n="103" num="15.9"><space unit="char" quantity="2"/>Les Boudet ne rʼviendront pas.</l>
				</lg>
				
				<lg n="16">
					<head type="main">EN CHŒUR.</head>
					<l n="104" num="16.1"><space unit="char" quantity="4"/>Non, non, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden">donnons-nous lʼ bras,</add></subst></l>
					<l n="105" num="16.2"><space unit="char" quantity="8"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Qu' notrʼ alliance</add></subst></l>
					<l n="106" num="16.3"><space unit="char" quantity="10"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Sauvʼ la France ; </add></subst></l>
					<l n="107" num="16.4"><space unit="char" quantity="4"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Non, non, donnons-nous lʼ bras</add></subst></l>
					<l n="108" num="16.5"><space unit="char" quantity="2"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Les Jésuitʼs ne rʼviendront pas. </add></subst></l>
				</lg>
				
				<lg n="17">
					<head type="main">LOUISE.</head>
					<l n="109" num="17.1"><space unit="char" quantity="2"/>Chansonniers dʼ tous les partis !</l>
					<l n="110" num="17.2"><space unit="char" quantity="2"/>Fidèlʼs à votre principe,</l>
					<l n="111" num="17.3"><space unit="char" quantity="2"/>Chantʼrez-vous la Saint-Philippe,</l>
					<l n="112" num="17.4"><space unit="char" quantity="2"/>Vous qui chantiez la Saint-Louis ?</l>
					<l n="113" num="17.5"><space unit="char" quantity="4"/>Non, non, donnons-nous lʼ bras !</l>
					<l n="114" num="17.6"><space unit="char" quantity="10"/>Plus d'hommage</l>
					<l n="115" num="17.7"><space unit="char" quantity="8"/>A tant la page.</l>
					<l n="116" num="17.8"><space unit="char" quantity="4"/>Non, non, donnons-nous lʼ bras ?</l>
					<l n="117" num="17.9">Les girouettʼs ne reviendront pas.</l>
				</lg>
				
				<lg n="18">
					<head type="main">EN CHŒUR.</head>
					<l n="118" num="18.1"><space unit="char" quantity="4"/>Non, non, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden">donnons-nous lʼ bras,</add></subst></l>
					<l n="119" num="18.2"><space unit="char" quantity="8"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Qu' notrʼ alliance</add></subst></l>
					<l n="120" num="18.3"><space unit="char" quantity="10"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Sauvʼ la France ; </add></subst></l>
					<l n="121" num="18.4"><space unit="char" quantity="4"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Non, non, donnons-nous lʼ bras</add></subst></l>
					<l n="122" num="18.5"><space unit="char" quantity="2"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Les Jésuitʼs ne rʼviendront pas. </add></subst></l>
				</lg>
			
				<lg n="19">
						<head type="main">PRUNEAU.</head>
					<l n="123" num="19.1"><space unit="char" quantity="2"/>Si lʼ trônʼ courait quelqu' danger,</l>
					<l n="124" num="19.2">Le défendʼ c'est nous qu' ça regarde ;</l>
					<l n="125" num="19.3"><space unit="char" quantity="2"/>Nous sommʼs sa meilleure garde,</l>
					<l n="126" num="19.4"><space unit="char" quantity="2"/>Plus d'uniforme étranger.</l>
					<l n="127" num="19.5"><space unit="char" quantity="4"/>Non, non, donnons-nous lʼ bras,</l>
					<l n="128" num="19.6"><space unit="char" quantity="10"/>Qu'on les berne</l>
					<l n="129" num="19.7"><space unit="char" quantity="10"/>Jusqu'à Berne !</l>
					<l n="130" num="19.8"><space unit="char" quantity="4"/>Non, non, donnons-nous lʼ bras !</l>
					<l n="131" num="19.9">Non ! les Suissʼs ne reviendront pas.</l>
				</lg>
				
				<lg n="20">
					<head type="main">EN CHŒUR.</head>
					<l n="132" num="20.1"><space unit="char" quantity="4"/>Non, non, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden">donnons-nous lʼ bras,</add></subst></l>
					<l n="133" num="20.2"><space unit="char" quantity="8"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Qu' notrʼ alliance</add></subst></l>
					<l n="134" num="20.3"><space unit="char" quantity="10"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Sauvʼ la France ; </add></subst></l>
					<l n="135" num="20.4"><space unit="char" quantity="4"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Non, non, donnons-nous lʼ bras</add></subst></l>
					<l n="136" num="20.5"><space unit="char" quantity="2"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Les Jésuitʼs ne rʼviendront pas. </add></subst></l>
				</lg>
				
				<lg n="21">
					<head type="main">JULIEN.</head>
					<l n="137" num="21.1"><space unit="char" quantity="2"/>Amis, partout j'ai couru,</l>
					<l n="138" num="21.2"><space unit="char" quantity="2"/>Je n'ai pas vu dʼ robe noire ;</l>
					<l n="139" num="21.3"><space unit="char" quantity="2"/>Depuis que lʼ coq chantʼ victoire,</l>
					<l n="140" num="21.4"><space unit="char" quantity="2"/>Les dindons ont disparu.</l>
					<l n="141" num="21.5"><space unit="char" quantity="4"/>Bon, bon, donnons-nous lʼ bras,</l>
					<l n="142" num="21.6"><space unit="char" quantity="8"/>Qu'on les escorte</l>
					<l n="143" num="21.7"><space unit="char" quantity="10"/>A la porte !</l>
					<l n="144" num="21.8"><space unit="char" quantity="4"/>Bon ! bon ! donnons-nous lʼ bras,</l>
					<l n="145" num="21.9"><space unit="char" quantity="2"/>Les dindons ne rʼviendront pas.</l>
				</lg>
				
				<lg n="22">
					<head type="main">EN CHŒUR,</head>
					<l n="146" num="22.1"><space unit="char" quantity="4"/>Non, non, <subst type="repetition" reason="analysis" hand="LG"><del>etc…</del><add rend="hidden">donnons-nous lʼ bras,</add></subst></l>
					<l n="147" num="22.2"><space unit="char" quantity="8"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Qu' notrʼ alliance</add></subst></l>
					<l n="148" num="22.3"><space unit="char" quantity="10"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Sauvʼ la France ; </add></subst></l>
					<l n="149" num="22.4"><space unit="char" quantity="4"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Non, non, donnons-nous lʼ bras</add></subst></l>
					<l n="150" num="22.5"><space unit="char" quantity="2"/><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden">Les Jésuitʼs ne rʼviendront pas. </add></subst></l>
				</lg>
		</div></body></text></TEI>