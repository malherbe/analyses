<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">27, 28 ET 29 JUILLET, TABLEAU ÉPISODIQUE DES TROIS JOURNÉES.</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ARA" sort="1">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<author key="DUV" sort="2">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>495 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
						<author>ARAGO ET DUVERT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=xjVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
								<author>ARAGO ET DUVERT</author>
								<repository>Österreichische Nationalbibliothek</repository>
								<idno type="URI">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886206</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : U+02BC MODIFIER LETTER APOSTROPHE) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<change when="2021-06-14" who="RR">Quelques corrections concernant la distribution du signe signe ʼ (UNICODE : ʼ).</change>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE JOURNÉE.</head><head type="main_subpart">SCÈNE VI.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="AED6">
			<head type="tune">AIR. Merveilleuse dans ses vertus.</head>
			<lg n="1">
				<head type="main">JULIEN.</head>
				<l n="1" num="1.1"><space unit="char" quantity="2"/>Ah ! mon pèrʼ quel événement !</l>
				<l n="2" num="1.2"><space unit="char" quantity="2"/>On prépare une tragédie ;</l>
				<l n="3" num="1.3"><space unit="char" quantity="2"/>Ils ont allumé l'incendie !</l>
				<l n="4" num="1.4"><space unit="char" quantity="2"/>Qui pourra l'éteindre à présent !</l>
				<l n="5" num="1.5"><space unit="char" quantity="4"/>La lecturʼ de l'ordonnance</l>
				<l n="6" num="1.6"><space unit="char" quantity="4"/>A soulevé tous les cœurs !</l>
				<l n="7" num="1.7"><space unit="char" quantity="4"/>Par bonheur, la noble France</l>
				<l n="8" num="1.8"><space unit="char" quantity="4"/>Ne manQ pas de défenseurs !</l>
				<l n="9" num="1.9"><space unit="char" quantity="2"/>Lʼ National et lʼ Temps à la fois,</l>
				<l n="10" num="1.10"><space unit="char" quantity="2"/>Lʼ Figaro, leur auxiliaire,</l>
				<l n="11" num="1.11"><space unit="char" quantity="2"/>Ont aux sabres de l'arbitraire</l>
				<l n="12" num="1.12"><space unit="char" quantity="2"/>Opposé lʼ bouclier des lois.</l>
				<l n="13" num="1.13"><space unit="char" quantity="4"/>Chaque feuille courageuse,</l>
				<l n="14" num="1.14"><space unit="char" quantity="4"/>Par un serment solennel,</l>
				<l n="15" num="1.15"><space unit="char" quantity="4"/>A d'unʼ nation généreuse</l>
				<l n="16" num="1.16"><space unit="char" quantity="4"/>Proclamé lʼ droit éternel !</l>
				<l n="17" num="1.17"><space unit="char" quantity="2"/>Mais, toi ! vilʼ Gazette du soir ;</l>
				<l n="18" num="1.18"><space unit="char" quantity="2"/>Toi, Quotidiennʼ ! journal des traîtres !</l>
				<l n="19" num="1.19"><space unit="char" quantity="2"/>Vous reniez déjà vos maîtres,</l>
				<l n="20" num="1.20"><space unit="char" quantity="2"/>Lâches sicaires du pouvoir !</l>
				<l n="21" num="1.21"><space unit="char" quantity="4"/>Dans cʼ moment un long murmure</l>
				<l n="22" num="1.22"><space unit="char" quantity="4"/>Accueillʼ le nouvel édit !</l>
				<l n="23" num="1.23"><space unit="char" quantity="4"/>Et frémissant dʼ son injure,</l>
				<l n="24" num="1.24"><space unit="char" quantity="2"/>Oui, chaquʼ Parisien se dit :</l>
				<l n="25" num="1.25"><space unit="char" quantity="2"/>En vain lʼ despotisme voudra</l>
				<l n="26" num="1.26"><space unit="char" quantity="2"/>Qu' son étendard de sang se lève !</l>
				<l n="27" num="1.27"><space unit="char" quantity="2"/>C'est l'enfant jouant avec un glaive :</l>
				<l n="28" num="1.28"><space unit="char" quantity="2"/>Bientôt lui-même il se tuera.</l>
				<l n="29" num="1.29"><space unit="char" quantity="4"/>L'artisan quittʼ son ouvrage ;</l>
				<l n="30" num="1.30"><space unit="char" quantity="4"/>Lʼ villageois désertʼ son champ :</l>
				<l n="31" num="1.31"><space unit="char" quantity="4"/>Paris n'offre plus l'image</l>
				<l n="32" num="1.32"><space unit="char" quantity="4"/>Qu' d'unʼ placʼ de guerre ou d'un camp.</l>
				<l n="33" num="1.33"><space unit="char" quantity="2"/>On s'agite, et de toutes parts</l>
				<l n="34" num="1.34"><space unit="char" quantity="2"/>Voyez ces bandes accourues ;</l>
				<l n="35" num="1.35"><space unit="char" quantity="2"/>Voyez-les dépaver les rues</l>
				<l n="36" num="1.36"><space unit="char" quantity="2"/>Pour former de nouveaux remparts.</l>
				<l n="37" num="1.37"><space unit="char" quantity="4"/>A ce beau nom de patrie,</l>
				<l n="38" num="1.38"><space unit="char" quantity="4"/>Chacun s'arme, chacun sort ;</l>
				<l n="39" num="1.39"><space unit="char" quantity="4"/>Le fer mêmʼ de l'industrie</l>
				<l n="40" num="1.40"><space unit="char" quantity="4"/>Forme un instrument de mort.</l>
				<l n="41" num="1.41"><space unit="char" quantity="2"/>De l'espoir d'un affreux succès</l>
				<l n="42" num="1.42">On dit que nos ennʼmis sont bien aises ;</l>
				<l n="43" num="1.43"><space unit="char" quantity="2"/>On dit que des balles françaises</l>
				<l n="44" num="1.44"><space unit="char" quantity="2"/>Doivent percer des cœurs français.</l>
				<l n="45" num="1.45"><space unit="char" quantity="4"/>Au milieu de tant d'alarmes,</l>
				<l n="46" num="1.46"><space unit="char" quantity="4"/>Moi, jʼ ne rʼssens aucun effroi.</l>
				<l n="47" num="1.47"><space unit="char" quantity="4"/>Ici, jʼ viens chercher des armes !</l>
				<l n="48" num="1.48"><space unit="char" quantity="4"/>Au nom du ciel ! armez-moi !</l>
				<l n="49" num="1.49"><space unit="char" quantity="2"/>Si je péris dans ces combats,</l>
				<l n="50" num="1.50"><space unit="char" quantity="2"/>Eh bien ! j'aurai payé ma dette.</l>
				<l n="51" num="1.51"><space unit="char" quantity="2"/>A la France il restʼ Lafayette :</l>
				<l n="52" num="1.52"><space unit="char" quantity="2"/>La liberté nʼ périra pas !</l>
			</lg>
			<lg n="2">
				<head type="main">ENSEMBLE</head>
				<head type="sub">RAIMOND, allant vers l'armoire.</head>
				<l n="53" num="2.1"><space unit="char" quantity="2"/>Tiens, mon fils, tiens, vole aux combats ;</l>
				<l n="54" num="2.2"><space unit="char" quantity="2"/>Au pays va payer ta dette :</l>
				<l n="55" num="2.3"><space unit="char" quantity="2"/>A la Francʼ il restʼ Lafayette,</l>
				<l n="56" num="2.4"><space unit="char" quantity="2"/>La liberté nʼ périra pas.</l>
			</lg>
			<lg n="3">
				<head type="main">ADOLPHE, tirant son épée.</head>
				<l n="57" num="3.1"><space unit="char" quantity="2"/>Je vais vous guider aux combats ;</l>
				<l n="58" num="3.2"><space unit="char" quantity="2"/>Je veux aussi payer ma dette :</l>
				<l n="59" num="3.3"><space unit="char" quantity="2"/>Sous le drapeau de Lafayette</l>
				<l n="60" num="3.4"><space unit="char" quantity="2"/>La liberté ne périt pas.</l>
			</lg>
			<lg n="4">
				<head type="main">LOUISE, avec effroi.</head>
				<l n="61" num="4.1"><space unit="char" quantity="2"/>Ah ! grand dieu ! quels tristes combats !</l>
				<l n="62" num="4.2"><space unit="char" quantity="2"/>Quel affreux carnage s'apprête !</l>
				<l n="63" num="4.3"><space unit="char" quantity="2"/>La foudre gronde sur la tête</l>
				<l n="64" num="4.4"><space unit="char" quantity="2"/>Des citoyens et des soldats.</l>
			</lg>
		</div></body></text></TEI>