<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE PREMIÈRE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC1">
				<head type="tune">AIR : Alerte ! alerte !</head>
				<lg n="1">
					<head type="main">LA CENSURE.</head>
					<l n="1" num="1.1"><space unit="char" quantity="12"/>Je coupe, <del hand="LG" reason="analysis" type="repetition">(bis)</del></l>
					<l n="2" num="1.2"><space unit="char" quantity="12"/><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden">Je coupe</add></subst></l>
					<l n="3" num="1.3">Je taille et rogne des deux mains,</l>
					<l n="4" num="1.4"><space unit="char" quantity="12"/>La troupe <del hand="LG" reason="analysis" type="repetition">(bis)</del></l>
					<l n="5" num="1.5"><space unit="char" quantity="12"/><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"> La troupe</add></subst></l>
					<l n="6" num="1.6"><space unit="char" quantity="8"/>Des écrivains.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Mes ciseaux ! je m'impatiente !</l>
				</lg>
				<lg n="3">
				<head type="main">LE RÉMOULEUR.</head>
					<l n="8" num="3.1">Mon Dieu, vous n'êtʼs jamais contente.</l>
				</lg>
				<lg n="4">
					<head type="main">LA CENSURE.</head>
					<l n="9" num="4.1">Coupons les couplets les meilleurs.</l>
				</lg>
				<lg n="5">
					<head type="main">LE RÉMOULEUR, à part.</head>
					<l n="10" num="5.1">Ellʼ fait aller les pauvrʼs auteurs</l>
					<l n="11" num="5.2"><space unit="char" quantity="4"/>Comme des rémouleurs.</l>
				</lg>
				<div type="section" n="1">
					<head type="main">ENSEMBLE</head>
					<lg n="1">
						<head type="main">LA CENSURE.</head>
						<l n="12" num="1.1"><space unit="char" quantity="12"/>Je coupe, <del hand="LG" reason="analysis" type="repetition">etc.</del></l>
						<l n="13" num="1.2"><space unit="char" quantity="12"/><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden">Je coupe</add></subst></l>
						<l n="14" num="1.3">Je taille et rogne des deux mains,</l>
						<l n="15" num="1.4"><space unit="char" quantity="12"/>La troupe <del hand="LG" reason="analysis" type="repetition">(bis)</del></l>
						<l n="16" num="1.5"><space unit="char" quantity="12"/><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"> La troupe</add></subst></l>
						<l n="17" num="1.6"><space unit="char" quantity="8"/>Des écrivains.</l>
					</lg>
					<lg n="2">
						<head type="main">LE RÉMOULEUR.</head>
						<l n="18" num="2.1"><space unit="char" quantity="12"/>Ellʼ coupe, <del hand="LG" reason="analysis" type="repetition">etc.</del></l>
						<l n="19" num="2.2"><space unit="char" quantity="12"/><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden">Ellʼ coupe</add></subst></l>
						<l n="20" num="2.3">Ellʼ taille et rogne des deux mains,</l>
						<l n="21" num="2.4"><space unit="char" quantity="12"/><subst hand="LG" reason="analysis" type="repetition"><del/><add rend="hidden">La troupe </add></subst></l>
						<l n="22" num="2.5"><space unit="char" quantity="12"/> <subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden">La troupe</add></subst></l>
						<l n="23" num="2.6"><space unit="char" quantity="8"/><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden">Des écrivains.</add></subst></l>
					</lg>
				</div>
			</div></body></text></TEI>