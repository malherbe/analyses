<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <fileDesc>
   <titleStmt>
	<title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
	<title type="corpus">Le Rire des vers</title>
	<author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
	</author>
	<author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
	</author>
	<editor>Le Rire des vers, Université de Bâle</editor>
	<editor>
	 Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
	 <choice>
	  <abbr>CRISCO, Université de Caen Normandie</abbr>
	  <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	 </choice>
	 (EA 4255)
	</editor>
	<respStmt>
	 <resp>Encodage en XML (CRISCO, université de Caen)</resp>
	 <name id="KL">
	  <forename>Kedi</forename>
	  <surname>LI</surname>
	 </name>
	</respStmt>
	<respStmt>
	 <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
	 <name id="RR">
	  <forename>Richard</forename>
	  <surname>RENAULT</surname>
	 </name>
	</respStmt>
   </titleStmt>
   <extent>140 vers</extent>
   <publicationStmt>
	<publisher>
	 <orgname>
	  <choice>
	   <abbr>CRISCO, Université de Caen Normandie</abbr>
	   <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	  </choice>
	 </orgname>
	 <address>
	  <addrLine>Université de Caen</addrLine>
	  <addrLine>14032 CAEN CEDEX</addrLine>
	  <addrLine>FRANCE</addrLine>
	 </address>
	 <email>crisco.incipit@unicaen.fr</email>
	 <ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
	</publisher>
	<pubPlace>Caen</pubPlace>
	<date when="2022">2022</date>
	<idno type="local">SCB_2</idno>	
	<availability status="free">
		<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
		<p>
			Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
			CC = Licence Creative Commons
			BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
			sur les sources et sur les modifications introduites.
			NC = Pas d’utilisation commerciale.
			SA = Partage dans les mêmes conditions.
		</p>
	</availability>
   </publicationStmt>
   <sourceDesc>
	<biblFull>
	 <titleStmt>
	  <title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	  <author>SCRIBE ET BAYARD</author>
	 </titleStmt>
	 <publicationStmt>
	  <publisher>INTERNET ARCHIVE</publisher>
	  <idno type="URL">https://archive.org/details/camillaoulasoeur00scriuoft</idno>
	 </publicationStmt>
	 <sourceDesc>
	  <biblStruct>
	   <monogr>
		<repository>University of Toronto Libraries</repository>
		<idno type="URL">https://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991106543695706196</idno>
	   </monogr>
	  </biblStruct>                 
	 </sourceDesc>
	</biblFull> 
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <creation>
	<date when="1832">12 DÉCEMBRE 1832</date>
	<placeName>
	 <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
	</placeName>
   </creation>
  </profileDesc>
  <encodingDesc>
   <projectDesc>
	<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
   </projectDesc>
   <samplingDecl>
	<p>Les parties versifiées ont été prioritairement encodées.</p>
   </samplingDecl>
   <editorialDecl>
	<normalization>
	 <p>Les majuscules accentuées ont été restituées.</p>
	 <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
	 <p>La ponctuation a été normalisée.</p> 
	 <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
	 <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
	 <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
	</normalization>
   </editorialDecl>
  </encodingDesc>
 </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB25">
	<head type="tune">AIR : Un homme pour faire un tableau.</head>
	<lg n="1">
		<l n="1" num="1.1">Il est fort aimable… à part lui…</l>
		<l n="2" num="1.2">Il faut qu'alors il se trahisse…</l>
		<l n="3" num="1.3">D'abord il est comme aujourd'hui</l>
		<l n="4" num="1.4">Taciturne au premier service ;</l>
		<l n="5" num="1.5">Au second il est plus ouvert,</l>
		<l n="6" num="1.6">Et, lorsque la gaîté nous gagne,</l>
		<l n="7" num="1.7">Son esprit s'échauffe au dessert,</l>
		<l n="8" num="1.8">Et s'échappe avec le champagne.</l>
	</lg>
</div></body></text></TEI>