<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANGÉLIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE VIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD25">
			<head type="tune">Air de madame Stockhausen.</head>
				<lg n="1">
					<head type="main">GRATIEN.</head>
					<l n="1" num="1.1">Suivez-nous loin de ces lieux,</l>
					<l n="2" num="1.2">Obéissez, je le veux.</l>
				</lg>
				<lg n="2">
					<head type="main">JEANNETON.</head>
					<l n="3" num="2.1">Mon enfant, c'est pour ton bien.</l>
				</lg>
				<lg n="3">
					<head type="main">JULES.</head>
					<l n="4" num="3.1">C'est possiblʼ, mais jʼ n'en crois rien.</l>
					<l n="5" num="3.2">Malgré mes pleurs, mes regrets,</l>
					<l n="6" num="3.3">On nous séparʼ pour jamais.</l>
				</lg>
				<lg n="4">
					<head type="main">ANGÉLIQUE.</head>
					<l n="7" num="4.1">Faut-il perdre tout espoir</l>
					<l n="8" num="4.2"><space unit="char" quantity="4"/>Et ne plus le rʼvoir ?</l>
					<p>Ils vont pour se tendre les mains, Gratien les sépare. )</p>
				</lg>
				<div type="section" n="1">
					<head type="main">ENSEMBLE |</head>
				<lg n="1">
					<head type="main">GRATIEN, JEANNETON.</head>
					<l n="9" num="1.1">Sortez, sortez, suivez-nous,</l>
					<l n="10" num="1.2"><space unit="char" quantity="2"/>Ou craignez mon courroux,</l>
					<l n="11" num="1.3">Et d'une autre, malgré vous,</l>
					<l n="12" num="1.4"><space unit="char" quantity="4"/>Vous serez l'époux.</l>
				</lg>
				<lg n="2">
					<head type="main">ANGÉLIQUE, JULES.</head>
					<l n="13" num="2.1">Quoi ! rompre des nœuds si doux !</l>
					<l n="14" num="2.2"><space unit="char" quantity="2"/>Redoutons leur courroux,</l>
					<l n="15" num="2.3">Ah ! plus de bonheur pour nous,</l>
					<l n="16" num="2.4"><space unit="char" quantity="4"/>Nous nʼ sʼrons pas époux.</l>
					</lg>
				<lg n="3">
					<head type="main">DELAUNAY, MAGLOIRE.</head>
					<l n="17" num="3.1">Rompez des nœuds aussi doux,</l>
					<l n="18" num="3.2"><space unit="char" quantity="2"/>Redoutez leur courroux,</l>
					<l n="19" num="3.3">Hélas, d'un autre que vous,</l>
					<l n="20" num="3.4"><space unit="char" quantity="4"/>Il sera l'époux.</l>
				</lg>
			</div>
<p>
(Angelique rentre chez elle, Gratien sort avec Jules, Jeanneton et Groom.)</p>
		</div></body></text></TEI>