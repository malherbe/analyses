<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANDRÉ LE CHANSONNIER</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="FTN" sort="1">
					<name>
						<forename>Louis Marie</forename>
						<surname>FONTAN</surname>
					</name>
					<date from="1801" to="1839">1801-1839</date>
				</author>
				<author key="DNY" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>DESNOYER</surname>
					</name>
					<date from="1806" to="1858">1806-1858</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>239 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>André le chansonnier.</title>
						<author>FONTAN ET DESNOYER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=aN6oyNpF3cMC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>André le chansonnier.</title>
								<author>FONTAN ET DESNOYER</author>
								<repository>Biblioteca Casanatense</repository>
								<idno type="URI">http://opac.casanatense.it/Record.htm?idlist=3</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Ode et Wodon</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-15" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE I</head><head type="main_subpart">SCÈNE VI.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FED4">
				<head type="tune">AIR : Aux armes, janissaires ! de M.Herold. ( Missolonghi. )</head>
					<p>(A voix basse.)</p>
					<lg n="1">
						<head type="main">ANDRÉ.</head>
						<l n="1" num="1.1"><space unit="char" quantity="8"/>Alerte, et du silence !</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"/>Je l'ai vu ! Par ici</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"/>Marchons avec prudence,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"/>Suivez-moi, le voici.</l>
					</lg>
					<p>Changeant de ton, et plus bas encore.)</p>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="10"/>Cherchez, cherchez bien !</l>
						<l n="6" num="2.2"><space unit="char" quantity="6"/>À votre œil le téméraire</l>
						<l n="7" num="2.3"><space unit="char" quantity="10"/>Saura se soustraire,</l>
						<l n="8" num="2.4"><space unit="char" quantity="6"/>Et vous ne trouverez rien.</l>
					</lg>
					<p>Nouveau changement de ton.)</p>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="8"/>Sentinelle attentive,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"/>En joue, et feu sur lui !</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"/>Gare qu'il ne s'esquive</l>
						<l n="12" num="3.4"><space unit="char" quantity="10"/>Encore aujourd'hui.</l>
						<l n="13" num="3.5"><space unit="char" quantity="16"/>C'est lui,</l>
						<l n="14" num="3.6"><space unit="char" quantity="16"/>C'est lui,</l>
						<l n="15" num="3.7"><space unit="char" quantity="14"/>C'est bien lui !</l>
						<l n="16" num="3.8"><space unit="char" quantity="18"/>Oui !</l>
					</lg>
					<p>Très bas, et ayant l'air de se cacher.)</p>
					<lg n="4">
						<l n="17" num="4.1">Dieu des proscrits, toi que toujours j'implore,</l>
						<l n="18" num="4.2">Toujours aussi propice à mes malheurs,</l>
						<l n="19" num="4.3">Ferme les yeux de mes persécuteurs,</l>
						<l n="20" num="4.4">Que, grace à toi, je leur échappe encore.</l>
						<l n="21" num="4.5">Tout bas, tout bas, j'ose te supplier,</l>
						<l n="22" num="4.6">Dieu ! sois en aide au pauvre chansonnier !</l>
						<l n="23" num="4.7">Ah ! sois en aide, <subst type="repetition" reason="analysis" hand="LG"><del>etc.</del><add rend="hidden">au pauvre chansonnier !</add></subst></l>
					</lg>
					<p>Nouveau changement de ton.)</p>
					<lg n="5">
						<l n="24" num="5.1"><space unit="char" quantity="8"/>Alerte, et du silence !</l>
						<l n="25" num="5.2"><space unit="char" quantity="8"/>Par ici, je reviens,</l>
						<l n="26" num="5.3"><space unit="char" quantity="8"/>Marchons avec prudence,</l>
						<l n="27" num="5.4"><space unit="char" quantity="8"/>Cette fois, je le tiens.</l>
					</lg>
					<p>Très fort et très gaîment.)</p>
					<lg n="6">
						<l n="28" num="6.1"><space unit="char" quantity="10"/>Cherchez, cherchez bien !</l>
						<l n="29" num="6.2"><space unit="char" quantity="6"/>A votre œil, le téméraire</l>
						<l n="30" num="6.3"><space unit="char" quantity="10"/>Saura se soustraire,</l>
						<l n="31" num="6.4"><space unit="char" quantity="6"/>Et vous ne trouverez rien.</l>
					</lg>
				</div></body></text></TEI>