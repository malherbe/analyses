<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB16">
	<head type="tune">AIR du Hussard de Felsheim.</head>
	<lg n="1">
		<l n="1" num="1.1">En toi j'aurai confiance,</l>
		<l n="2" num="1.2">Jure-moi de m'épouser ;</l>
		<l n="3" num="1.3">Tu l'vois, j'suis en ta puissance ;</l>
		<l n="4" num="1.4">Mais tu n'peux en abuser.</l>
	</lg>
	<p>
		TÉLÉMAQUE, à part.<lb/>
		(parté.) V'là l'hic.<lb/>
	</p>
	<lg n="2">
	<head type="speaker">MARGUERITE.</head>
		<l n="5" num="2.1">Je m'mets contre toute attaque</l>
		<l n="6" num="2.2">À l'abri sous ton serment.</l>
	</lg> 
	<lg n="3">
	<head type="speaker">TÉLÉMAQUE.</head>
		<l n="7" num="3.1">Faut-il que j'sois Télémaque</l>
		<l n="8" num="3.2">Dans un aussi doux moment !</l>
		<l n="9" num="3.3">C'est moral, grand'maman ;</l>
		<l n="10" num="3.4">Mais c'est un fameux tourment !</l>
		<stage>(prenant les souliers qu'il a faits pour elle.)</stage>
	</lg>
	<lg n="4">
	<head type="tune">Même air.</head>
		<l n="11" num="4.1">Voyons tes souliers, ma chère ;</l>
		<stage>(Marguerite s'assied.)</stage>
		<l n="12" num="4.2">Les mettre c'est mon emploi.</l>
		<l n="13" num="4.3">Tu sʼras contente, j'espère ;</l>
		<stage>(lui prenant le pied.)</stage>
		<l part="I" n="14" num="4.4">Qu'il est gentil !</l>
	</lg>
	<lg n="5">
	<head type="speaker">MARGUERITE.</head>
		<l part="F" n="14">C'est à toi.</l>
	</lg>
	<lg n="6">
	<head type="speaker">TÉLÉMAQUE, après lui avoir mis les souliers.</head>
		<l n="15" num="6.1">Je sens qu'ma raison s'détraque.</l>
	</lg>
	<lg n="7">
	<head type="speaker">MARGUERITE, montrant son pied.</head>
		<l n="16" num="7.1">Ah ! comme ça va joliment !</l>
	</lg>
	<lg n="8">
	<head type="speaker">TÉLÉMAQUE, à part.</head>
		<l n="17" num="8.1">Faut-il que j'sois Télémaque</l>
		<l n="18" num="8.2">En voyant ce pied charmant !</l>
		<l n="19" num="8.3">C'est moral, grand'maman,</l>
		<l n="20" num="8.4">Mais c'est un fameux tourment !</l>
	</lg>
</div></body></text></TEI>