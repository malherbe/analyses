<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL17">
	<head type="main">LA MONTAGNE EN TRAVAIL.</head>
		<p>
			LUDOVIC. <lb/>
			Quelle montagne ? la butte Montmartre ?<lb/>
		</p>
		 <p>
			GUSTAVE. <lb/>
			 Non, la montagne de Ménilmontant. ( Il déclame.)<lb/>
		 </p>
	<lg n="1">
		<l n="1" num="1.1">O fuyez les cités, venez à la campagne,</l>
		<l n="2" num="1.2">Venez-y savourer le bonheur des élus ;</l>
		<l n="3" num="1.3">Saint-Simon vous appelle à la sainte montagne…</l>
		<l n="4" num="1.4">On y va par les Omnibus !</l>
	</lg>
	<lg n="2">
		<l n="5" num="2.1">Vous y verrez, vainqueurs de préjugés gothiques,</l>
		<l n="6" num="2.2">Vers sa mission noble avançant d'un pas sûr,</l>
		<l n="7" num="2.3">L'homme libre, occupé de travaux domestiques,</l>
		<l n="8" num="2.4">Les mains sales et le cœur pur !</l>
	</lg>
	<lg n="3">
		<l n="9" num="3.1">Car, dans notre maison, chacun avec courage</l>
		<l n="10" num="3.2">Se livre, sans orgueil, aux soins les plus grossiers :</l>
		<l n="11" num="3.3">C'est un baron qui met la main à l'éclairage</l>
		<l n="12" num="3.4">Et récure les chandeliers !</l>
	</lg>
	<lg n="4">
		<l n="13" num="4.1">Un savant avocat, qui d'esprit étincelle,</l>
		<l n="14" num="4.2">Écume la marmite, hache les épinards ;</l>
		<l n="15" num="4.3">Ce sont deux sous-préfets qui lavent la vaisselle ;</l>
		<l n="16" num="4.4">Un banquier plume les canards !</l>
	</lg>
	<lg n="5">
		<l n="17" num="5.1">Un major de dragons brode et fait des reprises,</l>
		<l n="18" num="5.2">Un tendre soprano scie et monte le bois,</l>
		<l n="19" num="5.3">Un président de cour savonne nos chemises</l>
		<l n="20" num="5.4">Et met nos faux cols à l'empois !</l>
	</lg> 
	<lg n="6">
		<l n="21" num="6.1">Un enfant d'Apollon nous décrotte nos bottes</l>
		<l n="22" num="6.2">Un ancien auditeur a soin de nous brosser ;</l>
		<l n="23" num="6.3">Un duc et pair cultive oignons, poireaux, carottes,</l>
		<l n="24" num="6.4">Et mène les poules… coucher ! ! !</l>
	</lg>
</div></body></text></TEI>