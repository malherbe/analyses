<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
				<title type="sub">INVRAISEMBLANCE EN TROIS PARTIES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
				  <name>
					<forename>Félix-Auguste</forename>
					<surname>DUVERT</surname>
				  </name>
				  <date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="DLV" sort="2">
					<name>
						<forename>Augustin Théodore</forename>
						<nameLink>de</nameLink>
						<surname>LAUZANNE DE VAUROUSSEL</surname>
						<addname type="other">Lauzanne</addname>
					</name>
					<date from="1805" to="1877">1805-1877</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>135 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
                    <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
						<author>F.-A. DUVERT EN SOCIÉTÉ AVEC M. LAUZANNE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?vid=UOM:39015076863409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>University of Michigan Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/mdp.39015076863409</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">16 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DLV3" modus="cp" lm_max="11" metProfile="8, (4+6), (11)" form="strophe unique" schema="1(abbacdeedc)" er_moy="0.0" er_max="0" er_min="0" er_mode="0(5/5)" er_moy_et="0.0">
	<head type="tune">AIR : Ce que j'éprouve en vous voyant.</head>
	<lg n="1" type="dizain" rhyme="abbacdeedc">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J</w>'<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="1.3">t<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rt</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.5">l</w>'<w n="1.6" punct="ps:4">d<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="in ps">i</seg>r</w>'… <w n="1.7">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s</w> <w n="1.8"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>l</w> <w n="1.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.10" punct="vg:8">f<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="317" place="8" punct="vg">au</seg>t</rhyme></pgtc></w>, </l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">V<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="2.3">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>gr<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="2.4"><seg phoneme="u" type="vs" value="1" rule="425" place="5">où</seg></w> <w n="2.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.6" punct="pv:8">v<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>g<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ; </l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="3.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">r<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ppʼl<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w> <w n="3.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="3.6" punct="vg:8">M<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>n<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>, </l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="2">en</seg>s</w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">j</w>'<w n="4.5">su<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg>s</w> <w n="4.6">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>rs</w> <w n="4.7" punct="pe:8">P<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>g<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="437" place="8" punct="pe">o</seg>t</rhyme></pgtc></w> ! </l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1">L</w>'<w n="5.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">in</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="452" place="3">u</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></w> <w n="5.3">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="6">en</seg>t</w> <w n="5.4" punct="pe:8">P<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>g<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="437" place="8" punct="pe">o</seg>t</rhyme></pgtc></w> ! </l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="6.2">c<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>lm<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="6.3">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>tt</w>' <w n="6.4">fl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.5" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>, </l>
		<l n="7" num="1.7" lm="10" met="4+6" met_alone="True"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="7.2">pr<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t</w> <w n="7.3">n<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3" mp="M">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">an</seg>c</w>' <caesura></caesura><w n="7.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5" mp="P">an</seg>s</w> <w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6" mp="C">un</seg></w> <w n="7.6">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="7" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="320" place="8">y</seg>s</w> <w n="7.7">s<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg></w> <w n="7.8" punct="vg:10">ch<pgtc id="3" weight="0" schema="R"><rhyme label="e" id="3" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="317" place="10" punct="vg">au</seg>d</rhyme></pgtc></w>, </l>
		<l n="8" num="1.8" lm="11"><w n="8.1">N</w>'<w n="8.2"><seg phoneme="i" type="vs" value="1" rule="496" place="1">y</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="8.4">qu</w>'<w n="8.5">d<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="8.6" punct="vg:5">m<seg phoneme="wa" type="vs" value="1" rule="439" place="4" mp="M">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="362" place="5" punct="vg">en</seg>s</w>, <w n="8.7">ch<seg phoneme="wa" type="vs" value="1" rule="419" place="6" mp="M">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="8">ez</seg></w> <w n="8.8"><seg phoneme="o" type="vs" value="1" rule="317" place="9" mp="C">au</seg></w> <w n="8.9">pl<seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg>s</w> <w n="8.10" punct="dp:11">t<pgtc id="5" weight="0" schema="R"><rhyme label="e" id="5" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="414" place="11" punct="dp">ô</seg>t</rhyme></pgtc></w> : </l>
		<l n="9" num="1.9" lm="8" met="8"><w n="9.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="9.2">v<seg phoneme="o" type="vs" value="1" rule="437" place="3">o</seg>t</w>' <w n="9.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>g<seg phoneme="y" type="vs" value="1" rule="447" place="5">u</seg>r</w>' <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.5" punct="vg:8">M<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>n<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>, </l>
		<l n="10" num="1.10" lm="8" met="8"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg></w> <w n="10.2">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="10.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="10.4">c<seg phoneme="œ" type="vs" value="1" rule="248" place="5">œu</seg>r</w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.6" punct="pe:8">P<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>g<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="437" place="8" punct="pe">o</seg>t</rhyme></pgtc></w> ! </l>
	</lg>
</div></body></text></TEI>