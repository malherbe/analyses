<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHABERT</title>
				<title type="sub">HISTOIRE CONTEMPORAINE EN DEUX ACTES, MÊLÉE DE CHANT</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="ARJ" sort="1">
					<name>
						<forename>Jacques</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1790" to="1855">1790-1855</date>
				</author>
				<author key="LUR" sort="2">
					<name>
						<forename>Louis</forename>
						<surname>LURINE</surname>
					</name>
					<date from="1810" to="1860">1810-1860</date>
				</author>
				<editor>Le rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>110 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">JRL_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
				   </p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CHABERT</title>
						<author>JACQUES ARAGO ET LOUIS LURINE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=vldoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849877.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">2 JUILLET 1832</date>
				<placeName>
					<settlement>THÉÂTRE NATIONAL DU VAUDEVILLE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="JRL4" modus="cm" lm_max="10" metProfile="4+6" form="strophe unique" schema="1(ababcdcd)" er_moy="0.0" er_max="0" er_min="0" er_mode="0(4/4)" er_moy_et="0.0">
	<head type="tune">AIR : Aux temps heureux de la chevalerie.</head>
	<lg n="1" type="huitain" rhyme="ababcdcd">
		<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="Fm">e</seg>s</w>-<w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="341" place="4" caesura="1">à</seg></w><caesura></caesura> <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" mp="F">e</seg>nt</w> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="317" place="7" mp="C">au</seg></w> <w n="1.6">b<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>f<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
		<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="2.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406" place="3" mp="M">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397" place="4" caesura="1">eu</seg>x</w><caesura></caesura> <w n="2.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="2.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="C">u</seg></w> <w n="2.6" punct="pt:10">p<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10" punct="pt">ain</seg></rhyme></pgtc></w>.</l>
		<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="3.2" punct="vg:2"><seg phoneme="ø" type="vs" value="1" rule="397" place="2" punct="vg">eu</seg>x</w>, <w n="3.3" punct="vg:4">m<seg phoneme="œ" type="vs" value="1" rule="150" place="3" mp="M">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="4" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="3.4">l</w>'<w n="3.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5" mp="M">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="M">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="8">e</seg>st</w> <w n="3.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="9" mp="C">un</seg></w> <w n="3.8" punct="pv:10">v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
		<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">c</w>'<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="4.4">f<seg phoneme="a" type="vs" value="1" rule="306" place="3" mp="M">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>r</w><caesura></caesura> <w n="4.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="4.7">r<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">â</seg>l<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w> <w n="4.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="4.9" punct="pt:10">f<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10" punct="pt">aim</seg></rhyme></pgtc></w>.</l>
		<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">H<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="5.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3" mp="P">an</seg>s</w> <w n="5.3" punct="vg:4">c<seg phoneme="œ" type="vs" value="1" rule="248" place="4" punct="vg" caesura="1">œu</seg>r</w>,<caesura></caesura> <w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5" mp="C">un</seg></w> <w n="5.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r</w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="5.7" punct="dp:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="9" mp="M">en</seg><pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
		<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">Pu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg>s</w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2" mp="C">au</seg></w> <w n="6.3">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="4" caesura="1">oin</seg></w><caesura></caesura> <w n="6.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="C">i</seg>ls</w> <w n="6.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="6" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="C">ou</seg>s</w> <w n="6.7"><seg phoneme="o" type="vs" value="1" rule="434" place="9" mp="M">o</seg>ffr<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>r</rhyme></pgtc></w></l>
		<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">M<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="7.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="M">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="381" place="4" caesura="1">e</seg>ils</w><caesura></caesura> <w n="7.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="P">ou</seg>r</w> <w n="7.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="P">à</seg></w> <w n="7.7" punct="vg:10">v<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
		<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>s</w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2" mp="C">un</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>c<seg phoneme="y" type="vs" value="1" rule="449" place="4" caesura="1">u</seg></w><caesura></caesura> <w n="8.4">s</w>'<w n="8.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ls</w> <w n="8.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="C">ou</seg>s</w> <w n="8.7">v<seg phoneme="wa" type="vs" value="1" rule="439" place="7" mp="M">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.8" punct="pt:10">m<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>r<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pt">i</seg>r</rhyme></pgtc></w>.</l>
	</lg>
</div></body></text></TEI>