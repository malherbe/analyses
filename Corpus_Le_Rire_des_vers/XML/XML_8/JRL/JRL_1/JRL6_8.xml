<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHABERT</title>
				<title type="sub">HISTOIRE CONTEMPORAINE EN DEUX ACTES, MÊLÉE DE CHANT</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="ARJ" sort="1">
					<name>
						<forename>Jacques</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1790" to="1855">1790-1855</date>
				</author>
				<author key="LUR" sort="2">
					<name>
						<forename>Louis</forename>
						<surname>LURINE</surname>
					</name>
					<date from="1810" to="1860">1810-1860</date>
				</author>
				<editor>Le rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>110 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">JRL_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
				   </p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CHABERT</title>
						<author>JACQUES ARAGO ET LOUIS LURINE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=vldoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849877.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">2 JUILLET 1832</date>
				<placeName>
					<settlement>THÉÂTRE NATIONAL DU VAUDEVILLE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="JRL6" modus="cm" lm_max="10" metProfile="4+6" form="strophe unique" schema="1(ababcdcd)" er_moy="4.0" er_max="8" er_min="0" er_mode="0(2/4)" er_moy_et="4.0">
	<head type="tune">AIR : Vaudeville des Frères de lait.</head>
	<lg n="1" type="huitain" rhyme="ababcdcd">
		<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="C">ou</seg>s</w> <w n="1.3" punct="vg:4">c<seg phoneme="o" type="vs" value="1" rule="434" place="3" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="vg" caesura="1">ai</seg>s</w>,<caesura></caesura> <w n="1.4" punct="vg:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="M">om</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="351" place="6" punct="vg">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="1.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="1.7">m</w>'<w n="1.8" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
		<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg></w> <w n="2.2">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="2.3">c<seg phoneme="œ" type="vs" value="1" rule="248" place="4" caesura="1">œu</seg>r</w><caesura></caesura> <w n="2.4">s<seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="M">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="2.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="2.6" punct="pv:10">p<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>r<pgtc id="2" weight="8" schema="CVCR">d<seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>nn<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pv">er</seg></rhyme></pgtc></w> ; </l>
		<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="3.2">s<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="3.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="C">ou</seg>s</w> <w n="3.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="M">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w> <w n="3.6"><seg phoneme="y" type="vs" value="1" rule="452" place="8" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>xc<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
		<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1" mp="P">an</seg>s</w> <w n="4.2">n<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>l</w> <w n="4.3">d<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>r</w><caesura></caesura> <w n="4.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="4.5">pu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>s</w> <w n="4.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="C">ou</seg>s</w> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="4.8" punct="pt:10"><pgtc id="2" weight="8" schema="CVCR">d<seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>nn<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pt">er</seg></rhyme></pgtc></w>.</l>
		<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1" mp="P">an</seg>s</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">tr<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>b<seg phoneme="y" type="vs" value="1" rule="449" place="4" caesura="1">u</seg>t</w><caesura></caesura> <w n="5.4">qu</w>'<w n="5.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="5.6">ch<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="8">un</seg></w> <w n="5.7"><pgtc id="3" weight="8" schema="[C[VCR" part="1">m</pgtc></w>'<w n="5.8" punct="vg:10"><pgtc id="3" weight="8" schema="[C[VCR" part="2"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>pp<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="10">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
		<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">d<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="6.3" punct="vg:4">ch<seg phoneme="wa" type="vs" value="1" rule="419" place="3" mp="M">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg" caesura="1">i</seg>r</w>,<caesura></caesura> <w n="6.4" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="6.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="8">en</seg></w> <w n="6.7">s<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>v<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="10">en</seg>t</rhyme></pgtc></w></l>
		<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">J</w>'<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="7.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" mp="M">on</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>gn<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="7.4">l</w>'<w n="7.5"><seg phoneme="o" type="vs" value="1" rule="443" place="5" mp="M">o</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="6" mp="M">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="P">à</seg></w> <w n="7.7"><pgtc id="3" weight="8" schema="[CV[CR" part="1">m<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></pgtc></w> <w n="7.8"><pgtc id="3" weight="8" schema="[CV[CR" part="2">p<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="10">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
		<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="344" place="3" mp="M">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>r</w><caesura></caesura> <w n="8.3">l</w>'<w n="8.4">h<seg phoneme="o" type="vs" value="1" rule="434" place="5" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="411" place="6">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="8.5">h<seg phoneme="ɔ" type="vs" value="1" rule="418" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.6" punct="pt:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" mp="M">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>g<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="10" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
	</lg> 
</div></body></text></TEI>