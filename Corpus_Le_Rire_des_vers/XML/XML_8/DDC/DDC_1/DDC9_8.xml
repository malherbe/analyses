<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE GENTILHOMME</title>
				<title type="sub">VAUDEVILLE ANECDOTIQUE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DPY" sort="1">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="COU" sort="2">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>135 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DDC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE GENTILHOMME</title>
						<author>DUPEUTY ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=blZoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032862369.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">21 MARS 1833</date>
				<placeName>
					<settlement>THÉÂTRE NATIONAL DU VAUDEVILLE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DDC9" modus="sp" lm_max="7" metProfile="6, 4, (3), (7)" form="suite de strophes" schema="2[abab] 2[ababa] 4[aa] 2[aaa]" er_moy="1.0" er_max="7" er_min="0" er_mode="0(11/18)" er_moy_et="1.73">
	<head type="tune">AIR du morceau d'ensemble…</head>
		<div type="section" n="1">
		<lg n="1" type="regexp" rhyme="a">
			<l n="1" num="1.1" lm="6" met="6"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="1.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="1.3" punct="vg:3">nu<seg phoneme="i" type="vs" value="1" rule="490" place="3" punct="vg">i</seg>t</w>, <w n="1.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.5" punct="pt:6">s<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>mm<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="381" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
		</lg>
		<lg n="2" type="regexp" rhyme="b">
		<head type="speaker">LE CHEVAU-LÉGER.</head>
			<l n="2" num="2.1" lm="6" met="6"><w n="2.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.3">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>s</w> <w n="2.4" punct="ps:6">v<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="ps">oi</seg>r</rhyme></pgtc></w>…</l>
		</lg> 
		<lg n="3" type="regexp" rhyme="ab">
		<head type="speaker">TIMOTHÉE, à part.</head>
			<l n="3" num="3.1" lm="6" met="6"><w n="3.1">F<seg phoneme="œ" type="vs" value="1" rule="303" place="1">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="3.3">s<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.4" punct="ps:6"><seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>r<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="381" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="ps">e</seg></rhyme></pgtc></w>…</l>
			<stage>( haut.)</stage>
			<l n="4" num="3.2" lm="6" met="6"><w n="4.1" punct="pv:2">M<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="pv">i</seg></w> ; <w n="4.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="3">en</seg></w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4" punct="pt:6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
		</lg>
		<lg n="4" type="regexp" rhyme="a">
		<head type="speaker">LE CHEVAU-LÉGER.</head>
			<l n="5" num="4.1" lm="6" met="6"><w n="5.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1" punct="vg">ai</seg>s</w>, <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.5" punct="ps:6">r<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>p<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="ps">e</seg></rhyme></pgtc></w>…</l>
		</lg>
		<lg n="5" type="regexp" rhyme="ba">
		<head type="speaker">TIMOTHÉE.</head>
			<l n="6" num="5.1" lm="6" met="6"><w n="6.1">Pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="6.2" punct="vg:3">g<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="vg">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="6.4" punct="vg:6">d<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rm<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
			<l n="7" num="5.2" lm="6" met="6"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="7.3">c<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>gn<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="7.5" punct="pt:6">t<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="6">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
		</lg> 
		<lg n="6" type="regexp" rhyme="ba">
		<head type="speaker">LE CHEVAU-LÉGER.</head>
			<l n="8" num="6.1" lm="6" met="6"><w n="8.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="8.4" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>n<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
			<l n="9" num="6.2" lm="6" met="6"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="9.2" punct="vg:2">s<seg phoneme="o" type="vs" value="1" rule="437" place="2" punct="vg">o</seg>t</w>, <w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="9.4" punct="pe:6">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>lh<seg phoneme="o" type="vs" value="1" rule="434" place="5">o</seg>nn<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="6">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></pgtc></w> !</l>
		</lg>
		<lg n="7" type="regexp" rhyme="aaaaaaa">
		<head type="speaker">TIMOTHÉE, feignant toujours de ne pas entendre.</head>
			<l n="10" num="7.1" lm="6" met="6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="10.3" punct="pt:6">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381" place="4">e</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg><pgtc id="5" weight="2" schema="CR">m<rhyme label="a" id="5" gender="m" type="a" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="6" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
			<stage>( venant sur le devant de la scène.)</stage>
			<l n="11" num="7.2" lm="4" met="4"><w n="11.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="11.2">c</w>'<w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="11.4" punct="pe:4">ch<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r<pgtc id="5" weight="2" schema="CR">m<rhyme label="a" id="5" gender="m" type="e" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="pe">an</seg>t</rhyme></pgtc></w> !</l>
			<l n="12" num="7.3" lm="4" met="4"><w n="12.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="12.2">c</w>'<w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="12.4" punct="pe:4">ch<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rm<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="a" stanza="4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="pe">an</seg>t</rhyme></pgtc></w> !</l>
			<l n="13" num="7.4" lm="6" met="6"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="13.2">m</w>'<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.4" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>n<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="e" stanza="4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
			<l n="14" num="7.5" lm="4" met="4"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>pr<pgtc id="7" weight="7" schema="V[CGR" part="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>ds</pgtc></w> <w n="14.3" punct="vg:4"><pgtc id="7" weight="7" schema="V[CGR" part="2">bi<rhyme label="a" id="7" gender="m" type="a" stanza="5"><seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4" punct="vg">en</seg></rhyme></pgtc></w>,</l>
			<l n="15" num="7.6" lm="3"><w n="15.1">J</w>'<w n="15.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>t<pgtc id="7" weight="7" schema="V[CGR" part="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>ds</pgtc></w> <w n="15.3" punct="vg:3"><pgtc id="7" weight="7" schema="V[CGR" part="2">bi<rhyme label="a" id="7" gender="m" type="e" stanza="5"><seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="3" punct="vg">en</seg></rhyme></pgtc></w>,</l>
			<l n="16" num="7.7" lm="6" met="6"><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="16.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">n</w>'<w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg></w> <w n="16.5">l</w>'<w n="16.6"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>r</w> <w n="16.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.8" punct="pt:6">r<pgtc id="7" weight="1" schema="GR" part="1">i<rhyme label="a" id="7" gender="m" type="a" stanza="5"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="6" punct="pt">en</seg></rhyme></pgtc></w>.</l>
		</lg> 
	</div>
	<p>
 ( On frappe à la porte de gauche.)<lb></lb>
TIMOTHÉE. <lb></lb>
Allons, à l'autre à présent. <lb></lb>
Mon voisin ? … <lb></lb>
LUCINDE, en dehors. <lb></lb>
TIMOTHÉE, à part. <lb></lb>
C'est la comédienne ! ( haut.) Qu'est-ce que vous me voulez, <lb></lb>
ma voisine ? <lb></lb>
LUCINDE. <lb></lb>
Mon voisin, j'ai peur. <lb></lb>
TIMOTHÉE, à lui-même. <lb></lb>
Parbleu, et moi aussi. <lb></lb>
LUCINDE. <lb></lb>
J'ai peur de ce jeune chevau-léger. <lb></lb>
TIMOTHÉE. <lb></lb>
Vous n'en aviez pas peur dans la diligence. <lb></lb>
LUCINDE. <lb></lb>
Jaloux ! …, c'était le jour… mais la nuit… Ma porte est si<lb></lb>
facile à ouvrir ! <lb></lb>
TIMOTHÉE. <lb></lb>
Il faut la fermer à double-tour. (Il va fermer le verrou.)<lb></lb>
LUCINDE. <lb></lb>
Și j'avais près de moi un défenseur… <lb></lb>
TIMOTHÉE. <lb></lb>
Un avocat ! ( a part.) Je cherche à rompre les chiens. <lb></lb>
LUCINDE. <lb></lb>
Vous ne m'entendez pas ? … <lb></lb>
TIMOTHÉE. <lb></lb>
Non, non, pas très bien. ( à part.) Gaussons-nous d'elle. <lb></lb>
Le Gentilhomme. 
	</p>
	<div type="section" n="2">
		<head type="tune">Même air.</head>
		<lg n="1" type="regexp" rhyme="a">
			<l n="17" num="1.1" lm="6" met="6"><w n="17.1">J</w>'<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="17.3">l</w>'<w n="17.4"><seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381" place="3">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="17.6">p<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg></w> <w n="17.7" punct="pt:6">d<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="a" id="8" gender="f" type="a" stanza="6"><seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l> 
		</lg>
		<lg n="2" type="regexp" rhyme="bab">
		<head type="speaker">LUCINDE.</head>
			<l n="18" num="2.1" lm="6" met="6"><w n="18.1">P<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="418" place="2">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>s</w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.4" punct="vg:6">m<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="b" id="9" gender="m" type="a" stanza="6"><seg phoneme="wa" type="vs" value="1" rule="422" place="6" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
			<l n="19" num="2.2" lm="6" met="6"><w n="19.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="19.3">nu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg>t</w> <w n="19.4" punct="pe:6"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>bsc<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="a" id="8" gender="f" type="e" stanza="6"><seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></pgtc></w> !</l>
			<l n="20" num="2.3" lm="6" met="6"><w n="20.1" punct="vg:2">Vr<seg phoneme="ɛ" type="vs" value="1" rule="304" place="1">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="2" punct="vg">en</seg>t</w>, <w n="20.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.3">m<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>rs</w> <w n="20.4">d</w>'<w n="20.5" punct="pt:6"><seg phoneme="e" type="vs" value="1" rule="352" place="5">e</seg>ffr<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="b" id="9" gender="m" type="e" stanza="6"><seg phoneme="wa" type="vs" value="1" rule="422" place="6" punct="pt">oi</seg></rhyme></pgtc></w>.</l>
		</lg>
		<lg n="3" type="regexp" rhyme="a">
		<head type="speaker">TIMOTHÉE…</head>
			<l n="21" num="3.1" lm="6" met="6"><w n="21.1" punct="vg:2">M<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg></w>, <w n="21.2">j</w>'<w n="21.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="21.4">su<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg>s</w> <w n="21.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="5">en</seg></w> <w n="21.6" punct="pt:6"><pgtc id="10" weight="0" schema="[R" part="1"><rhyme label="a" id="10" gender="f" type="a" stanza="7"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
		</lg> 
		<lg n="4" type="regexp" rhyme="b">
		<head type="speaker">LUCINDE.</head>
			<l n="22" num="4.1" lm="6" met="6"><w n="22.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="22.3">p<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg></w> <w n="22.4" punct="pt:6">g<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>l<pgtc id="11" weight="0" schema="R" part="1"><rhyme label="b" id="11" gender="m" type="a" stanza="7"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="pt">an</seg>t</rhyme></pgtc></w>.</l>
		</lg> 
		<lg n="5" type="regexp" rhyme="a">
		<head type="speaker">TIMOTHÉE.</head>
			<l n="23" num="5.1" lm="6" met="6"><w n="23.1" punct="vg:2">B<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>s<seg phoneme="wa" type="vs" value="1" rule="419" place="2" punct="vg">oi</seg>r</w>, <w n="23.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="23.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="23.4" punct="pt:6">d<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>pl<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="a" id="10" gender="f" type="e" stanza="7"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
		</lg>
		<lg n="6" type="regexp" rhyme="ba">
		<head type="speaker">LUCINDE.</head>
			<l n="24" num="6.1" lm="6" met="6"><w n="24.1" punct="vg:2">V<seg phoneme="wa" type="vs" value="1" rule="419" place="1">oi</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2" punct="vg">in</seg></w>, <w n="24.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>s</w> <w n="24.3" punct="vg:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="466" place="5">i</seg>m<pgtc id="11" weight="0" schema="R" part="1"><rhyme label="b" id="11" gender="m" type="e" stanza="7"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
			<l n="25" num="6.2" lm="6" met="6"><w n="25.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="25.2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="25.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="25.4" punct="pt:6">N<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>c<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="a" id="10" gender="f" type="a" stanza="7"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
		</lg>
		<lg n="7" type="regexp" rhyme="aaaaaaa">
		<head type="speaker">TIMOTHÉE.</head>
			<l n="26" num="7.1" lm="6" met="6"><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="26.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="26.3" punct="pt:6">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381" place="4">e</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg><pgtc id="12" weight="2" schema="CR" part="1">m<rhyme label="a" id="12" gender="m" type="a" stanza="8"><seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="6" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
			<stage>( sur le devant de la scène.)</stage>
			<l n="27" num="7.2" lm="4" met="4"><w n="27.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="27.2">c</w>'<w n="27.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="27.4" punct="pe:4">ch<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r<pgtc id="12" weight="2" schema="CR" part="1">m<rhyme label="a" id="12" gender="m" type="e" stanza="8"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="pe">an</seg>t</rhyme></pgtc></w> !</l>
			<l n="28" num="7.3" lm="4" met="4"><w n="28.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="28.2">c</w>'<w n="28.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="28.4" punct="pe:4">ch<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r<pgtc id="13" weight="2" schema="CR" part="1">m<rhyme label="a" id="13" gender="m" type="a" stanza="9"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="pe">an</seg>t</rhyme></pgtc></w> !</l>
			<l n="29" num="7.4" lm="7"><w n="29.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="29.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="29.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="29.4" punct="pt:7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="5">en</seg>t<seg phoneme="i" type="vs" value="1" rule="466" place="6">i</seg><pgtc id="13" weight="2" schema="CR" part="1">m<rhyme label="a" id="13" gender="m" type="e" stanza="9"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="7" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
			<l n="30" num="7.5" lm="4" met="4"><w n="30.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="30.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>ds</w> <w n="30.3" punct="vg:4"><pgtc id="14" weight="3" schema="[CGR" part="1">bi<rhyme label="a" id="14" gender="m" type="a" stanza="10"><seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4" punct="vg">en</seg></rhyme></pgtc></w>,</l>
			<l n="31" num="7.6" lm="4" met="4"><w n="31.1">J</w>'<w n="31.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>ds</w> <w n="31.3">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rt</w> <w n="31.4" punct="vg:4"><pgtc id="14" weight="3" schema="[CGR" part="1">bi<rhyme label="a" id="14" gender="m" type="e" stanza="10"><seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4" punct="vg">en</seg></rhyme></pgtc></w>,</l>
			<l n="32" num="7.7" lm="6" met="6"><w n="32.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="32.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="32.3">n</w>'<w n="32.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg></w> <w n="32.5">l</w>'<w n="32.6"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>r</w> <w n="32.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="32.8" punct="pt:6">r<pgtc id="14" weight="1" schema="GR" part="1">i<rhyme label="a" id="14" gender="m" type="a" stanza="10"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="6" punct="pt">en</seg></rhyme></pgtc></w>.</l>
		</lg>
	</div>
</div></body></text></TEI>