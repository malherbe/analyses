<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE GENTILHOMME</title>
				<title type="sub">VAUDEVILLE ANECDOTIQUE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DPY" sort="1">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="COU" sort="2">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>135 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DDC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE GENTILHOMME</title>
						<author>DUPEUTY ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=blZoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032862369.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">21 MARS 1833</date>
				<placeName>
					<settlement>THÉÂTRE NATIONAL DU VAUDEVILLE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DDC2" modus="sp" lm_max="8" metProfile="8, (6), (4)" form="suite de strophes" schema="1[abab] 1[abaab] 1[aa]" er_moy="3.67" er_max="20" er_min="0" er_mode="0(4/6)" er_moy_et="7.34">
	<head type="tune">AIR : Je suis vilain.</head>
	<lg n="1" type="regexp" rhyme="abababaabaa">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="1.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>t</w> <w n="1.3">j<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="1.4">s<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>r</w> <w n="1.5">l</w>'<w n="1.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>r<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="2.2">l</w>'<w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="2.4"><seg phoneme="y" type="vs" value="1" rule="390" place="3">eu</seg>t</w> <w n="2.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.6" punct="vg:8">b<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>s<seg phoneme="a" type="vs" value="1" rule="342" place="7">a</seg>ï<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>l</rhyme></pgtc></w>,</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="3.3">r<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>t<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.4"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="3.6">n<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>ss<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="i" type="vs" value="1" rule="466" place="3">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg></w> <w n="4.4">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="4.5">c<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>p</w> <w n="4.6">d</w>'<w n="4.7" punct="pt:8"><pgtc id="2" weight="0" schema="[R"><rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="œ" type="vs" value="1" rule="285" place="8" punct="pt">œ</seg>il</rhyme></pgtc></w>.</l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="5.2">ph<seg phoneme="i" type="vs" value="1" rule="492" place="2">y</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="5.4">d</w>'<w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="5.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="5.7" punct="pv:8"><seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>g<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="y" type="vs" value="1" rule="447" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="6.2">l</w>'<w n="6.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="6.4" punct="vg:3">d<seg phoneme="wa" type="vs" value="1" rule="419" place="3" punct="vg">oi</seg>t</w>, <w n="6.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</w> <w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.7" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg><pgtc id="4" weight="2" schema="CR">l<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" punct="vg">in</seg></rhyme></pgtc></w>,</l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2" punct="vg:2">d<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="7.4">v<seg phoneme="wa" type="vs" value="1" rule="439" place="4">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t</w> <w n="7.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="7.6" punct="vg:8">t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>rn<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="8" num="1.8" lm="6"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="8.2">v<seg phoneme="wa" type="vs" value="1" rule="439" place="2">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="8.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="8.4" punct="dp:6">f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>g<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="y" type="vs" value="1" rule="447" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></rhyme></pgtc></w> :</l>
		<l n="9" num="1.9" lm="8" met="8"> « <w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">m<seg phoneme="œ" type="vs" value="1" rule="150" place="2">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="3">eu</seg>r</w>-<w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="9.4">n</w>'<w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="9.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>s</w> <w n="9.7" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="4" weight="2" schema="CR">l<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="vg">ain</seg></rhyme></pgtc></w>, <del reason="analysis" type="repetition" hand="KL">( bis.)</del></l>
		<l n="10" num="1.10" lm="4"> « <w n="10.1"><pgtc id="5" weight="20" schema="C[V[CV[CVCR" part="1">N</pgtc></w>'<w n="10.2"><pgtc id="5" weight="20" schema="C[V[CV[CVCR" part="2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</pgtc></w> <w n="10.3"><pgtc id="5" weight="20" schema="C[V[CV[CVCR" part="3">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>s</pgtc></w> <w n="10.4" punct="vg:4"><pgtc id="5" weight="20" schema="C[V[CV[CVCR" part="4">v<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l<rhyme label="a" id="5" gender="m" type="a" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" punct="vg">ain</seg></rhyme></pgtc></w>,</l>
		<l n="11" num="1.11" lm="8" met="8"> « <w n="11.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="11.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="2">en</seg></w> <w n="11.3">s<seg phoneme="y" type="vs" value="1" rule="444" place="3">û</seg>r</w> <w n="11.4"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l</w> <w n="11.5"><pgtc id="5" weight="20" schema="[C[V[CV[CVCR" part="1">n</pgtc></w>'<w n="11.6"><pgtc id="5" weight="20" schema="[C[V[CV[CVCR" part="2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</pgtc></w> <w n="11.7"><pgtc id="5" weight="20" schema="[C[V[CV[CVCR" part="3">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>s</pgtc></w> <w n="11.8" punct="pe:8"><pgtc id="5" weight="20" schema="[C[V[CV[CVCR" part="4">v<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>l<rhyme label="a" id="5" gender="m" type="e" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pe">ain</seg></rhyme></pgtc></w> ! » <del reason="analysis" type="repetition" hand="KL">(bis.)</del></l>
 	</lg>
</div></body></text></TEI>