<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE GENTILHOMME</title>
				<title type="sub">VAUDEVILLE ANECDOTIQUE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DPY" sort="1">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="COU" sort="2">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>135 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DDC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE GENTILHOMME</title>
						<author>DUPEUTY ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=blZoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032862369.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">21 MARS 1833</date>
				<placeName>
					<settlement>THÉÂTRE NATIONAL DU VAUDEVILLE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DDC3" modus="cp" lm_max="10" metProfile="8, 4+6" form="strophe unique" schema="1(ababcdcdcd)" er_moy="2.0" er_max="8" er_min="0" er_mode="0(3/6)" er_moy_et="2.83">
	<head type="tune">AIR : Ah ! vous avez des droits superbes !</head>
	<lg n="1" type="dizain" rhyme="ababcdcdcd">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">D<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rd</w> <w n="1.2"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="1.3" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" punct="vg">en</seg>d</w>, <w n="1.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="1.5"><pgtc id="1" weight="8" schema="[C[VCR" part="1">l</pgtc></w>'<w n="1.6" punct="dp:8"><pgtc id="1" weight="8" schema="[C[VCR" part="2"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>pp<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
		<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="2.2">b<seg phoneme="y" type="vs" value="1" rule="444" place="2">û</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="2.4" punct="vg:4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" punct="vg" caesura="1">ain</seg></w>,<caesura></caesura> <w n="2.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="C">i</seg>l</w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="2.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>d</w> <w n="2.9" punct="pv:10">s<seg phoneme="ɛ" type="vs" value="1" rule="383" place="9" mp="M">ei</seg>gn<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="pv">eu</seg>r</rhyme></pgtc></w> ;</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>su<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="3.3">lu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="3.4">d<seg phoneme="ɔ" type="vs" value="1" rule="418" place="5">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.5"><pgtc id="1" weight="8" schema="[CV[CR" part="1">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></pgtc></w> <w n="3.6" punct="ps:8"><pgtc id="1" weight="8" schema="[CV[CR" part="2">p<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></pgtc></w>…</l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">pu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="4.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="4.4">p<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="4.5">d</w>'<w n="4.6" punct="pt:8">h<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>nn<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pt">eu</seg>r</rhyme></pgtc></w>.</l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2" punct="vg">in</seg></w>, <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="5.3">f<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.4" punct="vg:8">pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>r<pgtc id="3" weight="2" schema="CR" part="1">n<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">L</w>'<w n="6.2">h<seg phoneme="œ" type="vs" value="1" rule="406" place="1" mp="M">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="6.3" punct="vg:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3" mp="M">in</seg>tr<seg phoneme="y" type="vs" value="1" rule="449" place="4" punct="vg" caesura="1">u</seg>s</w>,<caesura></caesura> <w n="6.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="P">ou</seg>r</w> <w n="6.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="6.7">l</w>'<w n="6.8" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">em</seg>pl<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="d" id="4" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="422" place="10" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1" punct="vg:2">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="419" place="2" punct="vg">oi</seg>t</w>, <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="7.4" punct="vg:8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="i" type="vs" value="1" rule="466" place="7">i</seg><pgtc id="3" weight="2" schema="CR" part="1">n<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="8.2">s<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>ffl<seg phoneme="ɛ" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="8.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg></w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="8.7" punct="vg:8">r<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="d" id="4" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
		<l n="9" num="1.9" lm="8" met="8"><w n="9.1" punct="vg:2">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="419" place="2" punct="vg">oi</seg>t</w>, <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="9.4" punct="vg:8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="i" type="vs" value="1" rule="466" place="7">i</seg>n<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="c" id="3" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="10" num="1.10" lm="8" met="8"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="10.2">j<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="10.3">s<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>ffl<seg phoneme="ɛ" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="10.6" punct="pe:8"><pgtc id="4" weight="2" schema="[CR" part="1">r<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pe ps">oi</seg></rhyme></pgtc></w> ! …</l>
	</lg>
</div></body></text></TEI>