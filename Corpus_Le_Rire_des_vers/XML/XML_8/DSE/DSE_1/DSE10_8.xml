<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE10" modus="cp" lm_max="10" metProfile="8, 6, 4, (4+6)" form="suite de strophes" schema="1[abba] 2[aaa] 2[aa]" er_moy="1.25" er_max="8" er_min="0" er_mode="0(6/8)" er_moy_et="2.63">
	
			<head type="main">PLUMEVILE.</head>

			<p>Réfléchissez ... vous voyez que je puis vous aider dans la composition de l’histoire.
</p>
			<head type="tune">Air : Tout comme a fait mon père.</head>

				<lg n="1" type="regexp" rhyme="abbaaaaaaaaaaa"><l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="1.3" punct="vg:4">f<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>rn<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>s</w>, <w n="1.4">c</w>’<w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="1.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="vg">é</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="6" met="6"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="2.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="2.3">pr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>x</w> <w n="2.4" punct="vg:6">r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>s<seg phoneme="o" type="vs" value="1" rule="434" place="5">o</seg>nn<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="6" met="6"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="3.3">m<seg phoneme="o" type="vs" value="1" rule="437" place="3">o</seg>ts</w> <w n="3.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rqu<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="4.2">v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="4.5" punct="vg:8">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>st<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="vg">é</seg></rhyme></pgtc></w>,</l>
					<l n="5" num="1.5" lm="4" met="4"><w n="5.1">M<seg phoneme="o" type="vs" value="1" rule="437" place="1">o</seg>ts</w> <w n="5.2">p<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>tt<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>r<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg>s</rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="4" met="4"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="6.2" punct="vg:4">g<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="7" num="1.7" lm="10" met="4+6" met_alone="True"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="7.3">s<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="M">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>t</w><caesura></caesura> <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="7.5">m<seg phoneme="o" type="vs" value="1" rule="437" place="6">o</seg>ts</w> <w n="7.6" punct="vg:10">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>v<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>r<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="8.2">m<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>ts</w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>m<seg phoneme="i" type="vs" value="1" rule="466" place="4">i</seg>n<seg phoneme="a" type="vs" value="1" rule="364" place="5">e</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="6">en</seg>t</w> <w n="8.4" punct="pv:8">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>ç<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pv">ai</seg>s</rhyme></pgtc></w> ;</l>
					<l n="9" num="1.9" lm="8" met="8"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1">É</seg>cr<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="9.2">d</w>’<w n="9.3" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" punct="vg">o</seg>rd</w>, <w n="9.4">pu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>s</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>pr<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>s</rhyme></pgtc></w></l>
					<l n="10" num="1.10" lm="4" met="4"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="10.2">v<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>s</w> <w n="10.3">h<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>r<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="a" stanza="4"><seg phoneme="o" type="vs" value="1" rule="437" place="4">o</seg>s</rhyme></pgtc></w></l>
					<l n="11" num="1.11" lm="6" met="6"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">f<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rn<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg></w> <w n="11.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="11.4" punct="dp:6">m<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="e" stanza="4"><seg phoneme="o" type="vs" value="1" rule="437" place="6" punct="dp">o</seg>ts</rhyme></pgtc></w> :</l>
					<l n="12" num="1.12" lm="6" met="6"><w n="12.1" punct="vg:2">V<seg phoneme="wa" type="vs" value="1" rule="419" place="1">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="2" punct="vg">à</seg></w>, <w n="12.2">v<seg phoneme="œ" type="vs" value="1" rule="405" place="3">eu</seg>ill<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="12.3">m</w>’<w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="12.5" punct="vg:6">cr<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="a" stanza="5"><seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="13" num="1.13" lm="6" met="6"><w n="13.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.2">l</w>’<w n="13.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="13.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="13.5">l</w>’<w n="13.6" punct="vg:6">h<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>st<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="e" stanza="5"><seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="1.14" lm="8" met="8"><w n="14.1">V<seg phoneme="wa" type="vs" value="1" rule="419" place="1">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <del hand="LN" type="repetition" reason="analysis">(bis.)</del> <w n="14.2">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t</w> <w n="14.5"><pgtc id="6" weight="8" schema="[[CVCR" part="1">l</pgtc></w>’<w n="14.6" punct="pt:8"><pgtc id="6" weight="8" schema="[[CVCR" part="2">h<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>st<rhyme label="a" id="6" gender="f" type="a" stanza="5"><seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l></lg>
			</div></body></text></TEI>