<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE17" modus="sp" lm_max="8" metProfile="7, 8, 3, 6" form="suite périodique" schema="1(abbacddcd) 2(ababcdcd) 1(abbacddc) 1(abba)" er_moy="0.84" er_max="2" er_min="0" er_mode="0(11/19)" er_moy_et="0.99">
	
			<head type="main">MATHIEU.</head>

			<head type="tune">Air : Oui, toujours, toujours, etc.</head>

			
				<lg n="1" type="neuvain" rhyme="abbacddcd"><l n="1" num="1.1" lm="7" met="7"><w n="1.1">N<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>str<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="1.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="6">en</seg></w> <w n="1.4" punct="pe:7">b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="pe">a</seg>s</rhyme></pgtc></w> !</l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1">L</w>’<w n="2.2">M<seg phoneme="e" type="vs" value="1" rule="352" place="1">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="2.3">B<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>t<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="2.4" punct="vg:7"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>xp<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="3.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="3.5">s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="3.6">l<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">N</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>st<seg phoneme="i" type="vs" value="1" rule="466" place="2">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="4.5" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>lm<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>n<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pe">a</seg>chs</rhyme></pgtc></w> !</l>
					<l n="5" num="1.5" lm="3" met="3"><w n="5.1" punct="pe:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">In</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg><pgtc id="3" weight="2" schema="CR">s<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="3" punct="pe">é</seg>s</rhyme></pgtc></w> !</l>
					<l n="6" num="1.6" lm="3" met="3"><w n="6.1">C</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="6.3" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg><pgtc id="3" weight="2" schema="CR">ss<rhyme label="d" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="3" punct="pe">ez</seg></rhyme></pgtc></w> !</l>
					<l n="7" num="1.7" lm="6" met="6"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="7.3" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>cl<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>p<pgtc id="4" weight="2" schema="CR">s<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="vg">é</seg>s</rhyme></pgtc></w>,</l>
					<l n="8" num="1.8" lm="6" met="6"><w n="8.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>ll<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="vg">ez</seg></w>, <w n="8.2" punct="vg:6">d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>sp<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg><pgtc id="4" weight="2" schema="CR">ss<rhyme label="c" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="vg">ez</seg></rhyme></pgtc></w>,</l>
					<l n="9" num="1.9" lm="6" met="6"><w n="9.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="9.3" punct="pe:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg><pgtc id="4" weight="2" schema="CR">c<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="pe">é</seg>s</rhyme></pgtc></w> !</l></lg>

				<lg n="2" type="huitain" rhyme="ababcdcd"><l n="10" num="2.1" lm="8" met="8"><w n="10.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg></w> <w n="10.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="10.3">s<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="10.4" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg><pgtc id="5" weight="2" schema="CR">t<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="11" num="2.2" lm="8" met="8"><w n="11.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg></w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="4">om</seg></w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="11.6" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg><pgtc id="6" weight="2" schema="CR">t<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>rs</rhyme></pgtc></w>,</l>
					<l n="12" num="2.3" lm="8" met="8"><w n="12.1">L</w>’<w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>nn<seg phoneme="y" type="vs" value="1" rule="d-3" place="2">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="12.4">L<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="5" weight="2" schema="CR">t<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="13" num="2.4" lm="8" met="8"><w n="13.1">N</w>’<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>s</w> <w n="13.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="13.5">s<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>l</w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.7">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="13.8" punct="pv:8">l<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>c<pgtc id="6" weight="2" schema="CR">t<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pv">eu</seg>rs</rhyme></pgtc></w> ;</l>
					<l n="14" num="2.5" lm="6" met="6"><w n="14.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="14.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="2">om</seg></w> <w n="14.3">s<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>l</w> <w n="14.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.5" punct="vg:6">pr<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>t<pgtc id="7" weight="0" schema="R"><rhyme label="c" id="7" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="15" num="2.6" lm="6" met="6"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="3">ain</seg>s</w> <w n="15.4">n<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>l</w> <w n="15.5" punct="pv:6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg><pgtc id="8" weight="2" schema="CR">g<rhyme label="d" id="8" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="pv">er</seg></rhyme></pgtc></w> ;</l>
					<l n="16" num="2.7" lm="6" met="6"><w n="16.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">A</seg>lm<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ch</w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.5">Li<pgtc id="7" weight="0" schema="R"><rhyme label="c" id="7" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="17" num="2.8" lm="6" met="6"><w n="17.1">D<seg phoneme="wa" type="vs" value="1" rule="419" place="1">oi</seg>t</w> <w n="17.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>rs</w> <w n="17.3" punct="pt:6">s<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>rn<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg><pgtc id="8" weight="2" schema="CR">g<rhyme label="d" id="8" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="pt">er</seg></rhyme></pgtc></w>.</l></lg>

				<lg n="3" type="huitain" rhyme="abbacddc"><l n="18" num="3.1" lm="7" met="7"><w n="18.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>r</w> <w n="18.3">d</w>’<w n="18.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="18.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>c<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>rs</rhyme></pgtc></w></l>
					<l n="19" num="3.2" lm="7" met="7"><w n="19.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="19.3">sc<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="377" place="4">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.4" punct="vg:7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">in</seg>f<seg phoneme="i" type="vs" value="1" rule="466" place="6">i</seg>n<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="481" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="20" num="3.3" lm="7" met="7"><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="20.3">c<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="20.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="20.5">l</w>’<w n="20.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="20.7">p<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>bl<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
					<l n="21" num="3.4" lm="8" met="8"><w n="21.1">M<seg phoneme="wa" type="vs" value="1" rule="422" place="1">oi</seg></w> <w n="21.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>ph<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg></w> <w n="21.4" punct="pe:8">t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>j<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pe">ou</seg>rs</rhyme></pgtc></w> !</l>
					<l n="22" num="3.5" lm="6" met="6"><w n="22.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="22.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg></w> <w n="22.3">l</w>’<w n="22.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="22.5">r<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>d<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg><pgtc id="11" weight="2" schema="CR">t<rhyme label="c" id="11" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></rhyme></pgtc></w></l>
					<l n="23" num="3.6" lm="6" met="6"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="23.2">l</w>’<w n="23.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="23.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="23.5" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>b<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg><pgtc id="11" weight="2" schema="CR">t<rhyme label="d" id="11" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="vg">a</seg></rhyme></pgtc></w>,</l>
					<l n="24" num="3.7" lm="6" met="6"><w n="24.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="24.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="3">en</seg></w> <w n="24.4">d<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>g<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg><pgtc id="12" weight="2" schema="CR">t<rhyme label="d" id="12" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></rhyme></pgtc></w></l>
					<l n="25" num="3.8" lm="6" met="6"><w n="25.1">L</w>’<w n="25.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>lm<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ch</w> <w n="25.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="25.4" punct="pe:6">G<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg><pgtc id="12" weight="2" schema="CR">th<rhyme label="c" id="12" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="pe">a</seg></rhyme></pgtc></w> !</l></lg>

				<lg n="4" type="huitain" rhyme="ababcdcd"><l n="26" num="4.1" lm="8" met="8"><w n="26.1">L</w>’<w n="26.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>lm<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ch</w> <w n="26.3">R<seg phoneme="wa" type="vs" value="1" rule="439" place="4">o</seg>y<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>l</w> <w n="26.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="26.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="26.6" punct="vg:8">b<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="27" num="4.2" lm="8" met="8"><w n="27.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="27.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="27.3">p<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>t</w> <w n="27.4">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="27.6">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="27.7" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rr<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8" punct="pv">ê</seg>ts</rhyme></pgtc></w> ;</l>
					<l n="28" num="4.3" lm="8" met="8"><w n="28.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="28.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="28.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="3">ain</seg>ts</w> <w n="28.4"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l</w> <w n="28.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="28.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</w> <w n="28.7" punct="vg:8">c<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="351" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="29" num="4.4" lm="8" met="8"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="29.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="29.3">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="3">en</seg>s</w> <w n="29.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="29.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="29.6" punct="pe:8">j<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pe">ai</seg>s</rhyme></pgtc></w> !</l>
					<l n="30" num="4.5" lm="6" met="6"><w n="30.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="30.2">st<seg phoneme="i" type="vs" value="1" rule="492" place="2">y</seg>l</w>’ <w n="30.3">d</w>’<w n="30.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="339" place="3">A</seg>p<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>c<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>l<pgtc id="15" weight="0" schema="R"><rhyme label="c" id="15" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="492" place="6">y</seg>ps<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="31" num="4.6" lm="6" met="6"><w n="31.1">L</w>’<w n="31.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg></w> <w n="31.3" punct="vg:3">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346" place="3" punct="vg">er</seg></w>, <w n="31.4">j</w>’<w n="31.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg></w> <w n="31.6">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>t</w> <w n="31.7">b<pgtc id="16" weight="0" schema="R"><rhyme label="d" id="16" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>s</rhyme></pgtc></w></l>
					<l n="32" num="4.7" lm="6" met="6"><w n="32.1">Pr<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t</w> <w n="32.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="304" place="4">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.3"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>cl<pgtc id="15" weight="0" schema="R"><rhyme label="c" id="15" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>ps<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="33" num="4.8" lm="6" met="6"><w n="33.1">Qu</w>’<w n="33.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>l</w> <w n="33.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="33.4">pr<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>v<seg phoneme="wa" type="vs" value="1" rule="439" place="4">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="33.5" punct="pe:6">p<pgtc id="16" weight="0" schema="R"><rhyme label="d" id="16" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="pe">a</seg>s</rhyme></pgtc></w> !</l></lg>

				<lg n="5" type="quatrain" rhyme="abba"><l n="34" num="5.1" lm="7" met="7"><w n="34.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="34.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="34.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">in</seg>gt</w> <w n="34.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>lgu<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>z<pgtc id="17" weight="0" schema="R"><rhyme label="a" id="17" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ls</rhyme></pgtc></w></l>
					<l n="35" num="5.2" lm="7" met="7"><w n="35.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="35.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="35.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="35.4">m<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="35.6" punct="pv:7">c<pgtc id="18" weight="0" schema="R"><rhyme label="b" id="18" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="36" num="5.3" lm="7" met="7"><w n="36.1"><seg phoneme="e" type="vs" value="1" rule="132" place="1">E</seg>h</w> <w n="36.2" punct="pe:2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="2" punct="pe">en</seg></w> ! <w n="36.3">j</w>’<w n="36.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg></w> <w n="36.5">br<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg></w> <w n="36.6">l</w>’<w n="36.7" punct="vg:7"><seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>r<pgtc id="18" weight="0" schema="R"><rhyme label="b" id="18" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="37" num="5.4" lm="8" met="8"><w n="37.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="37.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="37.3" punct="pt:5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>b<seg phoneme="u" type="vs" value="1" rule="424" place="5" punct="pt pt pt">ou</seg>t</w> ... <w n="37.4"><seg phoneme="u" type="vs" value="1" rule="425" place="6">où</seg></w> <w n="37.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>t</w>-<w n="37.6" punct="pi:8"><pgtc id="17" weight="0" schema="[R"><rhyme label="a" id="17" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pi">i</seg>ls</rhyme></pgtc></w> ?</l>
					<l ana="unanalyzable" n="38" num="5.5">Insensés !</l>
					<l ana="unanalyzable" n="39" num="5.6">C’est assez,</l>
					<l ana="unanalyzable" n="40" num="5.7">etc., etc.</l></lg>
			</div></body></text></TEI>