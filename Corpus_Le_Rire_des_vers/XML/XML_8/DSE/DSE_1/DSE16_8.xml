<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE16" modus="sp" lm_max="8" metProfile="6, 8" form="suite périodique" schema="2(abab)" er_moy="0.0" er_max="0" er_min="0" er_mode="0(4/4)" er_moy_et="0.0">
		
			<head type="tune">AIR : Marche de la Muette.</head>
		
				<head type="main">CHŒUR.</head>

				<lg n="1" type="quatrain" rhyme="abab"><l n="1" num="1.1" lm="6" met="6"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>bj<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="1.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>l<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="6" met="6"><w n="2.1">S<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="2.2">d</w>’<w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="2.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>g</w> <w n="2.5" punct="pv:6">s<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>mm<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="381" place="6" punct="pv">e</seg>il</rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="6" met="6"><w n="3.1" punct="vg:2">C<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="3.2">c<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>x</w> <w n="3.4" punct="vg:6"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="4.2">d<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="4.3">p<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4">c</w>’<w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.7" punct="pt:8">r<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>v<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="381" place="8" punct="pt">e</seg>il</rhyme></pgtc></w>.</l></lg>

				<head type="main">FIGARO.</head>

				<lg n="2" type="quatrain" rhyme="abab"><l n="5" num="2.1" lm="8" met="8"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.6">ju<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="5.7">qu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg></w> <w n="5.8" punct="pt:8">br<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2" punct="vg:2">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2" punct="vg">e</seg>l</w>, <w n="6.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="6.4">pr<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="6.5">n<seg phoneme="o" type="vs" value="1" rule="437" place="7">o</seg>s</w> <w n="6.6" punct="vg:8">f<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg>ls</rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="7.2">d<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>nn</w>’ <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">s<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="381" place="5">e</seg>il</w> <w n="7.5">d</w>’<w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="7.7">B<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>st<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">vi<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="8.4">dr<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314" place="5">eau</seg></w> <w n="8.5">d</w>’<w n="8.6" punct="pe:8"><seg phoneme="o" type="vs" value="1" rule="317" place="6">Au</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rl<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pe">i</seg>tz</rhyme></pgtc></w> !</l></lg>

				<head type="main">CHŒUR.</head>

				<lg n="3" rhyme="None"><l ana="unanalyzable" n="9" num="3.1">Abjurons les alarmes, etc., etc.</l></lg>
			</div></body></text></TEI>