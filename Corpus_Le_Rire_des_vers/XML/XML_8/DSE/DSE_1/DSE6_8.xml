<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE6" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(ababcdcd)" er_moy="2.0" er_max="6" er_min="0" er_mode="0(2/4)" er_moy_et="2.45">
	
			<head type="main">ZÉPHIRINE.</head>

		<head type="tune">AIR : De cet amour vif et soudain</head>.

				<lg n="1" type="huitain" rhyme="ababcdcd"><l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>s</w> ! <w n="1.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="1.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4">d<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>vr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="1" weight="2" schema="CR">r<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>t</w> <w n="2.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="2.5">m</w>’<w n="2.6" punct="pi:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="6">im</seg>p<pgtc id="2" weight="6" schema="VCR"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rt<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="452" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="2">en</seg>t</w> <w n="3.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>c</w> <w n="3.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="3.5">l</w>’<w n="3.6"><seg phoneme="o" type="vs" value="1" rule="443" place="6">O</seg>p<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg><pgtc id="1" weight="2" schema="CR">r<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="4.3">s<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>l<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>t</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="4.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="4.6" punct="pi:8">f<pgtc id="2" weight="6" schema="VCR"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rt<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="452" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="1">e</seg>l</w> <w n="5.2" punct="pe:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">em</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="pe pt pt pt">a</seg>s</w> ! ... <w n="5.3">v<seg phoneme="wa" type="vs" value="1" rule="439" place="5">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="5.5" punct="vg:8">p<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="vg">eu</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="6.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="6.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">f<seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>t</w> <w n="6.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="6.7" punct="vg:8">M<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1">É</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="7.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.4">v<seg phoneme="ɛ" type="vs" value="1" rule="63" place="7">e</seg>rs</w> <w n="7.5">Di<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="8.2">f<seg phoneme="œ" type="vs" value="1" rule="303" place="2">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="8.3"><seg phoneme="y" type="vs" value="1" rule="452" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.4" punct="pi:8">p<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r<seg phoneme="u" type="vs" value="1" rule="d-2" place="7">ou</seg><pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></pgtc></w> ?</l></lg>
			</div></body></text></TEI>