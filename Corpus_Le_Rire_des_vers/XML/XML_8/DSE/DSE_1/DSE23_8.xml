<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE23" modus="cp" lm_max="11" metProfile="4, 6, 4+6, 5+6, (9)" form="suite de strophes" schema="1[ababcbcc] 2[abab] 1[abaaabab]" er_moy="0.86" er_max="2" er_min="0" er_mode="0(8/14)" er_moy_et="0.99">
	
			<head type="main">LE POSTILLON.</head>

			<p>Oh ! moi, je n’ai pas donné dans le menu voyageur ; j’ai été tout-à-fait messagerie royale, car je n’ai voituré que des rois, oui, des rois, et nous ne flânions pas sur la route ; ils étaient pressés, car ils voyageaient par ordonnance du peuple ... changement de domicile pour raison de santé ... Il faut avouer qu’il y a des positions de conducteurs qui sont cocasses.
</p>
			<head type="tune">AIR de la Monaco.</head>

				<lg n="1" type="regexp" rhyme="ababcbccabab"><l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="1.3">j<seg phoneme="y" type="vs" value="1" rule="449" place="2" mp="Lc">u</seg>squ</w>’<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="P">à</seg></w> <w n="1.5">G<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">an</seg>d</w><caesura></caesura> <w n="1.6">mʼn<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg></w> <w n="1.7">l</w>’<w n="1.8"><seg phoneme="o" type="vs" value="1" rule="317" place="6" mp="M">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r</w> <w n="1.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="1.10">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="1.11" punct="vg:10">Ch<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="9"><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="2.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="C">i</seg>l</w> <w n="2.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t</w> <w n="2.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" mp="C">on</seg></w> <w n="2.5">v<seg phoneme="wa" type="vs" value="1" rule="439" place="5" mp="M">o</seg>y<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>g</w>’ <w n="2.6">d</w>’<w n="2.7" punct="pv:9"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>gr<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg><pgtc id="2" weight="2" schema="CR">m<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="9" punct="pv">en</seg>t</rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">J<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="Lc">u</seg>squ</w>’<w n="3.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="P">à</seg></w> <w n="3.3">R<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3" mp="M">o</seg>chʼf<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4" caesura="1">o</seg>rt</w><caesura></caesura> <w n="3.4">j</w>’<w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg></w> <w n="3.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" mp="M">on</seg>du<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg>t</w> <w n="3.7" punct="pv:10">B<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>p<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">Pu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg>s</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="P">à</seg></w> <w n="4.3" punct="vg:4">Ch<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rb<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="1">ou</seg>rg</w>,<caesura></caesura> <w n="4.4">l</w>’<w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="M">an</seg>ci<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="6">en</seg></w> <w n="4.6" punct="pt:10">g<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>rn<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg><pgtc id="2" weight="2" schema="CR">m<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="10" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="4" met="4"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="5.3">s<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>ppl<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="4" met="4"><w n="6.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="6.2">r<seg phoneme="wa" type="vs" value="1" rule="422" place="2">oi</seg></w> <w n="6.3" punct="vg:4">fl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg><pgtc id="2" weight="2" schema="CR">m<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="vg">an</seg>d</rhyme></pgtc></w>,</l>
					<l n="7" num="1.7" lm="4" met="4"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="7.3">d</w>’<w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="7.5">B<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>lg<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="6" met="6"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">Em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg></w> <w n="8.2">s<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="8.3" punct="pv:6">b<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>t<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="9" num="1.9" lm="4" met="4"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2" punct="vg:4">V<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rs<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>v<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="481" place="4">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="1.10" lm="4" met="4"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="10.2">b<seg phoneme="o" type="vs" value="1" rule="314" place="2">eau</seg></w> <w n="10.3" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg><pgtc id="5" weight="2" schema="CR">t<rhyme label="b" id="5" gender="m" type="a" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" punct="vg">in</seg></rhyme></pgtc></w>,</l>
					<l n="11" num="1.11" lm="4" met="4"><w n="11.1">J<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>squ</w>’<w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="11.3">R<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>ss<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="e" stanza="2"><seg phoneme="i" type="vs" value="1" rule="481" place="4">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
					<l n="12" num="1.12" lm="6" met="6"><w n="12.1">J</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>sc<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg></w> <w n="12.3" punct="pe:6">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg><pgtc id="5" weight="2" schema="CR">t<rhyme label="b" id="5" gender="m" type="e" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6" punct="pe">in</seg></rhyme></pgtc></w> !</l></lg>

				<lg n="2" type="regexp" rhyme="abababaaabab"><l n="13" num="2.1" lm="11" met="5+6"><w n="13.1">D</w>’<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">A</seg>lg<seg phoneme="e" type="vs" value="1" rule="346" place="2">er</seg></w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="P">à</seg></w> <w n="13.4" punct="vg:5">L<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>v<seg phoneme="u" type="vs" value="1" rule="424" place="5" punct="vg" caesura="1">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="13.6">d</w>’<w n="13.7">L<seg phoneme="ɛ" type="vs" value="1" rule="383" place="7" mp="M">ei</seg>ps<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>k</w> <w n="13.8"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="P">à</seg></w> <w n="13.9" punct="vg:11">M<seg phoneme="ɛ" type="vs" value="1" rule="338" place="10" mp="M">a</seg>y<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="a" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="2.2" lm="11" met="5+6"><w n="14.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg></w>-<w n="14.3">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="3" mp="Fm">e</seg></w> <w n="14.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" mp="M">on</seg>du<seg phoneme="i" type="vs" value="1" rule="490" place="5" caesura="1">i</seg>t</w><caesura></caesura> <w n="14.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6" mp="C">e</seg>s</w> <w n="14.6">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>cʼs</w> <w n="14.7" punct="pi:11">d<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="9" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="320" place="10" mp="M">y</seg><pgtc id="7" weight="2" schema="CR">s<rhyme label="b" id="7" gender="m" type="a" stanza="3"><seg phoneme="e" type="vs" value="1" rule="408" place="11" punct="pi">é</seg>s</rhyme></pgtc></w> ?</l>
					<l n="15" num="2.3" lm="10" met="4+6"><w n="15.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="15.2">r<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" mp="M">en</seg>trʼ<seg phoneme="ø" type="vs" value="1" rule="397" place="4" caesura="1">eu</seg>x</w><caesura></caesura> <w n="15.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="15.5"><seg phoneme="y" type="vs" value="1" rule="452" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="Fc">e</seg></w> <w n="15.6" punct="vg:10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="M">on</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>d<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="e" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="16" num="2.4" lm="10" met="4+6"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="16.2">l</w>’<w n="16.3">p<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="16.5" punct="vg:4">l<seg phoneme="a" type="vs" value="1" rule="341" place="4" punct="vg" caesura="1">à</seg></w>,<caesura></caesura> <w n="16.6">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="16.7" punct="dp:6">d<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="dp">i</seg>t</w> : <w n="16.8">ch<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M/mc">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="8" mp="Lc">ez</seg></w>-<w n="16.9" punct="pe:10">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="9" mp="M/mc">oi</seg><pgtc id="7" weight="2" schema="CR">s<rhyme label="b" id="7" gender="m" type="e" stanza="3"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pe">ez</seg></rhyme></pgtc></w> !</l>
					<l n="17" num="2.5" lm="4" met="4"><w n="17.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2" punct="vg">in</seg></w>, <w n="17.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="17.3" punct="vg:4">s<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="f" type="a" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="18" num="2.6" lm="4" met="4"><w n="18.1">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>qu</w>’ <w n="18.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>s<pgtc id="9" weight="2" schema="CR">t<rhyme label="b" id="9" gender="m" type="a" stanza="4"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></rhyme></pgtc></w></l>
					<l n="19" num="2.7" lm="4" met="4"><w n="19.1">M</w>’<w n="19.2">v<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>t</w> <w n="19.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>s</w> <w n="19.4">d</w>’<w n="19.5" punct="pi:3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" punct="in pi">e</seg>ll</w>’ ? <w n="19.6">c<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="f" type="e" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
					<l n="20" num="2.8" lm="6" met="6"><w n="20.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="20.2">f<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="345" place="3">e</seg>l</w>’ <w n="20.3" punct="dp:6">g<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>lh<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="f" type="a" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="6">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></rhyme></pgtc></w> :</l>
					<l n="21" num="2.9" lm="4" met="4"><w n="21.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">Dr<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>sd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="21.4">R<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="f" type="e" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="440" place="4">o</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
					<l n="22" num="2.10" lm="6" met="6"><w n="22.1">J</w>’<w n="22.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">en</seg></w> <w n="22.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg></w> <w n="22.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="22.5" punct="vg:6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>r<pgtc id="9" weight="2" schema="CR">t<rhyme label="b" id="9" gender="m" type="e" stanza="4"><seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="vg">é</seg></rhyme></pgtc></w>,</l>
					<l n="23" num="2.11" lm="4" met="4"><w n="23.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">l</w>’<w n="23.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="23.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="23.5">n<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="f" type="a" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
					<l n="24" num="2.12" lm="6" met="6"><w n="24.1">Cr<seg phoneme="ɔ" type="vs" value="1" rule="442" place="1">o</seg>qu</w>’ <w n="24.2">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rt</w> <w n="24.3">d</w>’<w n="24.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="24.5" punct="pe:6">r<seg phoneme="wa" type="vs" value="1" rule="439" place="4">o</seg>y<seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg><pgtc id="9" weight="2" schema="CR">t<rhyme label="b" id="9" gender="m" type="a" stanza="4"><seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="pe">é</seg></rhyme></pgtc></w> !</l>
					<l ana="unanalyzable" n="25" num="2.13">Tra, déri, déra, la la.</l></lg>
			</div></body></text></TEI>