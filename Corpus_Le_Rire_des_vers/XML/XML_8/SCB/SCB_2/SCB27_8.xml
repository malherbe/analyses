<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <fileDesc>
   <titleStmt>
	<title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
	<title type="corpus">Le Rire des vers</title>
	<author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
	</author>
	<author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
	</author>
	<editor>Le Rire des vers, Université de Bâle</editor>
	<editor>
	 Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
	 <choice>
	  <abbr>CRISCO, Université de Caen Normandie</abbr>
	  <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	 </choice>
	 (EA 4255)
	</editor>
	<respStmt>
	 <resp>Encodage en XML (CRISCO, université de Caen)</resp>
	 <name id="KL">
	  <forename>Kedi</forename>
	  <surname>LI</surname>
	 </name>
	</respStmt>
	<respStmt>
	 <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
	 <name id="RR">
	  <forename>Richard</forename>
	  <surname>RENAULT</surname>
	 </name>
	</respStmt>
   </titleStmt>
   <extent>140 vers</extent>
   <publicationStmt>
	<publisher>
	 <orgname>
	  <choice>
	   <abbr>CRISCO, Université de Caen Normandie</abbr>
	   <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	  </choice>
	 </orgname>
	 <address>
	  <addrLine>Université de Caen</addrLine>
	  <addrLine>14032 CAEN CEDEX</addrLine>
	  <addrLine>FRANCE</addrLine>
	 </address>
	 <email>crisco.incipit@unicaen.fr</email>
	 <ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
	</publisher>
	<pubPlace>Caen</pubPlace>
	<date when="2022">2022</date>
	<idno type="local">SCB_2</idno>	
	<availability status="free">
		<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
		<p>
			Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
			CC = Licence Creative Commons
			BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
			sur les sources et sur les modifications introduites.
			NC = Pas d’utilisation commerciale.
			SA = Partage dans les mêmes conditions.
		</p>
	</availability>
   </publicationStmt>
   <sourceDesc>
	<biblFull>
	 <titleStmt>
	  <title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	  <author>SCRIBE ET BAYARD</author>
	 </titleStmt>
	 <publicationStmt>
	  <publisher>INTERNET ARCHIVE</publisher>
	  <idno type="URL">https://archive.org/details/camillaoulasoeur00scriuoft</idno>
	 </publicationStmt>
	 <sourceDesc>
	  <biblStruct>
	   <monogr>
		<repository>University of Toronto Libraries</repository>
		<idno type="URL">https://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991106543695706196</idno>
	   </monogr>
	  </biblStruct>                 
	 </sourceDesc>
	</biblFull> 
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <creation>
	<date when="1832">12 DÉCEMBRE 1832</date>
	<placeName>
	 <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
	</placeName>
   </creation>
  </profileDesc>
  <encodingDesc>
   <projectDesc>
	<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
   </projectDesc>
   <samplingDecl>
	<p>Les parties versifiées ont été prioritairement encodées.</p>
   </samplingDecl>
   <editorialDecl>
	<normalization>
	 <p>Les majuscules accentuées ont été restituées.</p>
	 <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
	 <p>La ponctuation a été normalisée.</p> 
	 <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
	 <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
	 <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
	</normalization>
   </editorialDecl>
  </encodingDesc>
 </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB27" modus="cp" lm_max="10" metProfile="8, 4+6" form="suite de strophes" schema="2[abab]" er_moy="0.0" er_max="0" er_min="0" er_mode="0(4/4)" er_moy_et="0.0">
	<head type="tune">AIR du Partage de la Richesse.</head>
	<lg n="1" type="regexp" rhyme="ababab">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="1.3" punct="vg:4">f<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="1.6">f<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>l<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
		<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">t</w>'<w n="2.3" punct="ps:4"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>cc<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="M">u</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="ps" caesura="1">ai</seg>s</w>…<caesura></caesura> <w n="2.4">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5">t<seg phoneme="y" type="vs" value="1" rule="449" place="6" mp="C">u</seg></w> <w n="2.6">d<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>s</w> <w n="2.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="2.8" punct="pe:10">h<seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="M">a</seg><pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="476" place="10" punct="pe">ï</seg>r</rhyme></pgtc></w> !</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">M<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="3.3" punct="vg:5">s<seg phoneme="œ" type="vs" value="1" rule="248" place="5" punct="vg">œu</seg>rs</w>, <w n="3.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="3.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
		<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="C">u</seg></w> <w n="4.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.3">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">ai</seg>s</w><caesura></caesura> <w n="4.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="P">ou</seg>r</w> <w n="4.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="4.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>s</w> <w n="4.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="4.8" punct="pt:10">tr<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>h<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pt">i</seg>r</rhyme></pgtc></w>.</l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="5.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3" punct="vg:4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="3">ain</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="5.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="5.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>f<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="6.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="6.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="6.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5" punct="vg:8">r<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>gn<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="vg">er</seg></rhyme></pgtc></w>,</l>
		<l part="I" n="7" num="1.7" lm="10" met="4+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">c</w>'<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="7.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="P">ou</seg>r</w> <w n="7.5" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="422" place="4" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> </l>
	</lg>
	<lg n="2" type="regexp" rhyme="a">
	<head type="speaker">CAMILLA.</head>
		<l part="F" n="7" lm="10" met="4+6"><w n="7.6">P<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="M/mp">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s</w>-<w n="7.7">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="7" mp="Fm">e</seg></w> <w n="7.8">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="7.9">l</w>'<w n="7.10" punct="pi:10"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>ppr<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></rhyme></pgtc></w> ?</l>
	</lg>
	<lg n="3" type="regexp" rhyme="b">
	<head type="speaker">LIONEL.</head>
		<l n="8" num="3.1" lm="8" met="8"><w n="8.1" punct="pe:1">M<seg phoneme="wa" type="vs" value="1" rule="422" place="1" punct="pe">oi</seg></w> ! <w n="8.2">j</w>'<w n="8.3"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="8.4">d<seg phoneme="y" type="vs" value="1" rule="444" place="4">û</seg></w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.6" punct="pt:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="i" type="vs" value="1" rule="466" place="7">i</seg>n<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pt">er</seg></rhyme></pgtc></w>.</l>
	</lg>
</div></body></text></TEI>