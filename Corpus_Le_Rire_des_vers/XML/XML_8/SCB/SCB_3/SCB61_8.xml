<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB61" modus="sp" lm_max="6" metProfile="4, 6" form="strophe unique" schema="1(ababccdd)" er_moy="0.0" er_max="0" er_min="0" er_mode="0(4/4)" er_moy_et="0.0">
	<head type="tune">AIR : Fleuve du Tage.</head>
	<lg n="1" type="huitain" rhyme="ababccdd">
	<head type="speaker">ENSEMBLE.</head>
		<stage>(Montrant Rodolphe.)</stage>
		<l n="1" num="1.1" lm="4" met="4"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="1.2">lu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="1.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4" punct="vg:4">tr<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="2" num="1.2" lm="6" met="6"><w n="2.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="2.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="2.3"><seg phoneme="y" type="vs" value="1" rule="390" place="3">eu</seg>t</w> <w n="2.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="2.5">d</w>'<w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="2.7" punct="pv:6">t<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" punct="pv">o</seg>rt</rhyme></pgtc></w> ;</l>
		<l n="3" num="1.3" lm="4" met="4"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="3.2">l<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rsqu</w>'<w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
		<l n="4" num="1.4" lm="6" met="6"><w n="4.1">Tr<seg phoneme="wa" type="vs" value="1" rule="419" place="1">oi</seg>s</w> <w n="4.2">f<seg phoneme="a" type="vs" value="1" rule="192" place="2">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="4.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t</w> <w n="4.4">d</w>'<w n="4.5" punct="ps:6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>cc<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" punct="ps">o</seg>rd</rhyme></pgtc></w>…</l>
		<l n="5" num="1.5" lm="6" met="6"><w n="5.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rsqu</w>'<w n="5.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">in</seg>d<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>lg<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="5.4" punct="vg:6">b<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="6" num="1.6" lm="6" met="6"><w n="6.1">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>c<seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.2"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="6.3" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rd<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="7" num="1.7" lm="4" met="4"><w n="7.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="7.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w>-<w n="7.3">v<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</rhyme></pgtc></w></l>
		<l n="8" num="1.8" lm="6" met="6"><w n="8.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="8.2">s<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="8.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.4" punct="pi:6">n<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="pi">ou</seg>s</rhyme></pgtc></w> ?</l>
	</lg>
</div></body></text></TEI>