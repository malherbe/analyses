<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS10" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(ababcbcb)" er_moy="0.5" er_max="2" er_min="0" er_mode="0(3/4)" er_moy_et="0.87">
	<head type="tune">Air : En naissant premis à Thalie.</head>
	<lg n="1" type="huitain" rhyme="ababcbcb">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="1.3" punct="vg:4">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="1.6" punct="vg:8">br<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="452" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>, </l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="2.2">l</w>'<w n="2.3">h<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r</w> <w n="2.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="2.5">d<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>t</w> <w n="2.6" punct="pt:8">h<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pt">er</seg></rhyme></pgtc></w>. </l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">ê</seg>t</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="3.3">d<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">pr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>x</w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="341" place="7">à</seg></w> <w n="3.7">l</w>'<w n="3.8" punct="vg:8"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="452" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>, </l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="4.2">l</w>'<w n="4.3"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="4.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="5">en</seg>t</w> <w n="4.7" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pv">er</seg></rhyme></pgtc></w> ; </l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="422" place="1" punct="vg">oi</seg></w>, <w n="5.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">di<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg></w> <w n="5.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg></w> <w n="5.6">g<seg phoneme="u" type="vs" value="1" rule="424" place="6">oû</seg>t</w> <w n="5.7" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="6.2">n</w>'<w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="6.6" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="vg">é</seg></rhyme></pgtc></w>, </l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="7.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>sp<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="7.6" punct="vg:8">p<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>, </l>
		<l n="8" num="1.8" lm="8" met="8"><w n="8.1">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>c<seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="8.5" punct="pt:8">m<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>ti<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pt">é</seg></rhyme></pgtc></w>. <del reason="analysis" type="repetition" hand="KL">( Bis.)</del></l>
	</lg>
</div></body></text></TEI>