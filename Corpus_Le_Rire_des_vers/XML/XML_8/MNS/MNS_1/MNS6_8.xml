<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS6" modus="sp" lm_max="8" metProfile="7, 6, 8, 5, 4" form="suite de strophes" schema="3[aa] 1[abba] 1[aaa]" er_moy="0.57" er_max="2" er_min="0" er_mode="0(5/7)" er_moy_et="0.9">
	<head type="tune">Air. : Une deux, trois, etc.</head>
	<lg n="1" type="regexp" rhyme="a">
		<l n="1" num="1.1" lm="7" met="7"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2" punct="vg:4">c<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="1.3">c</w>'<w n="1.4" punct="ps:5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5" punct="ps">e</seg>st</w>… <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="1.6" punct="pv:7"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="7" punct="pv">I</seg></rhyme></pgtc></w> ;</l>
	</lg>
	<lg n="2" type="regexp" rhyme="a">
		<head type="speaker">MISS SARA.</head>
		<l n="2" num="2.1" lm="7" met="7"><w n="2.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="422" place="1" punct="vg">oi</seg></w>, <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">v<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6" punct="pt:7"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>ss<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="7" punct="pt">i</seg></rhyme></pgtc></w>.</l>
		<l part="I" n="3" num="2.2" lm="6" met="6"><w n="3.1"><seg phoneme="y" type="vs" value="1" rule="452" place="1">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <subst hand="RR" reason="analysis" type="phonemization"><del>S</del><add rend="hidden"><w n="3.2" punct="pt:2"><seg phoneme="ɛ" type="vs" value="1" rule="49" place="2" punct="pt">è</seg>s</w></add></subst>. </l>  
	</lg>
	<lg n="3" type="regexp" rhyme="a">
		<head type="speaker">TOUTES.</head>
		<l part="F" n="3" lm="6" met="6"><w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">E</seg>st</w>-<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.5" punct="dp:6">b<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="6">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></rhyme></pgtc></w> :</l> 
	</lg>
	<lg n="4" type="regexp" rhyme="bb">
		<head type="speaker">HÉLÈNE.</head>
		<l n="4" num="4.1" lm="7" met="7"><w n="4.1">C</w>'<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="4.4" punct="ps:3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="3" punct="vg ps">oi</seg></w>, … <w n="4.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.6" punct="ps:5">t<seg phoneme="i" type="vs" value="1" rule="467" place="5" punct="ps">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… <w n="4.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <subst hand="RR" reason="analysis" type="phonemization"><del>B</del><add rend="hidden"><w n="4.8" punct="pt:7"><pgtc id="3" weight="2" schema="[CR">b<rhyme label="b" id="3" gender="m" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="408" place="7" punct="pt">é</seg></rhyme></pgtc></w></add></subst>.</l>
		<l n="5" num="4.2" lm="7" met="7"><w n="5.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="1">e</seg>l</w> <w n="5.2" punct="pe:3">b<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="3" punct="pe">eu</seg>r</w> ! <w n="5.3">j</w>'<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg></w> <w n="5.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="5">en</seg></w> <w n="5.6" punct="pt:7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">om</seg><pgtc id="3" weight="2" schema="CR">b<rhyme label="b" id="3" gender="m" type="e" stanza="2"><seg phoneme="e" type="vs" value="1" rule="408" place="7" punct="pt">é</seg></rhyme></pgtc></w>.</l>  
	</lg>
	<lg n="5" type="regexp" rhyme="a">
		<head type="speaker">TOUTES.</head>
		<l n="6" num="5.1" lm="6" met="6"><w n="6.1">C</w>'<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="6.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="6.5" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg">ou</seg>r</w>, <w n="6.6" punct="pt:6">N<seg phoneme="i" type="vs" value="1" rule="466" place="5">i</seg>n<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l> 
	</lg>
	<lg n="6" type="regexp" rhyme="aaa">
		<head type="speaker">NINETTE.</head> 
		<l n="7" num="6.1" lm="8" met="8"><w n="7.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="vg">on</seg></w>, <w n="7.2">c</w>'<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="7.5" punct="vg:5">Cl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="7.6" punct="ps:6">p<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" punct="ps">ai</seg>x</w>… <w n="7.7" punct="pe:8">br<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg><pgtc id="4" weight="2" schema="CR">v<rhyme label="a" id="4" gender="m" type="a" stanza="3"><seg phoneme="o" type="vs" value="1" rule="443" place="8" punct="pe">o</seg></rhyme></pgtc></w> !</l>
		<l n="8" num="6.2" lm="5" met="5"><w n="8.1">V<seg phoneme="wa" type="vs" value="1" rule="419" place="1">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="8.2">d<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="8.3" punct="vg:5">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg><pgtc id="4" weight="2" schema="CR">v<rhyme label="a" id="4" gender="m" type="e" stanza="3"><seg phoneme="o" type="vs" value="1" rule="314" place="5" punct="vg">eau</seg></rhyme></pgtc></w>,</l>
		<l n="9" num="6.3" lm="5" met="5"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="9.4" punct="pt:5"><pgtc id="4" weight="0" schema="[R"><rhyme label="a" id="4" gender="m" type="a" stanza="3"><seg phoneme="o" type="vs" value="1" rule="443" place="5" punct="pt">O</seg></rhyme></pgtc></w>.</l>
	</lg>
	<lg n="7" type="regexp" rhyme="a">
	<head type="speaker">HÉLÈNE à Ninette.</head>
		<l n="10" num="7.1" lm="8" met="8"><w n="10.1">C</w>'<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="10.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="2">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="10.5" punct="vg:6">t<seg phoneme="wa" type="vs" value="1" rule="422" place="6" punct="vg">oi</seg></w>, <w n="10.6">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="10.7" punct="pt:8">ch<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l> 
	</lg>
	<lg n="8" type="regexp" rhyme="aaa">
		<head type="speaker">NINETTE,</head>
		<l n="11" num="8.1" lm="8" met="8"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="11.3">l</w>'<w n="11.4" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="346" place="5" punct="vg">er</seg></w>, <w n="11.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.6">l</w>'<w n="11.7" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>sp<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="12" num="8.2" lm="4" met="4"><w n="12.1" punct="ps:3">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="346" place="3" punct="ps">ez</seg></w>… <w n="12.2" punct="pe:4"><pgtc id="6" weight="0" schema="[R"><rhyme label="a" id="6" gender="m" type="a" stanza="5"><seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="pe">a</seg>h</rhyme></pgtc></w> !</l>
		<l n="13" num="8.3" lm="4" met="4"><w n="13.1">C</w>'<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="13.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="2">en</seg></w> <w n="13.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="13.5" punct="pt:4"><pgtc id="6" weight="0" schema="[R"><rhyme label="a" id="6" gender="m" type="e" stanza="5"><seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="pt">A</seg></rhyme></pgtc></w>.</l> 
	</lg>
</div></body></text></TEI>