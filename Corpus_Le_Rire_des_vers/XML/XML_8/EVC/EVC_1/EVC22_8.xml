<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE VI.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC22" modus="cp" lm_max="10" metProfile="4+6, 3−6, (8)" form="strophe unique" schema="1(ababcdcd)" er_moy="0.5" er_max="2" er_min="0" er_mode="0(3/4)" er_moy_et="0.87">
					<head type="tune">AIR : Au brave hussard du deuxième.</head>
						<lg n="1" type="huitain" rhyme="ababcdcd">
							<head type="main">BUTLER.</head>
							<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="1.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>rs</w><caesura></caesura> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5" mp="C">un</seg></w> <w n="1.5">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" mp="M">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>t</w> <w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="8">e</seg>st</w> <w n="1.7">l</w>'<w n="1.8" punct="pv:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>scl<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
							<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="2.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="2.3">s<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="443" place="5" mp="M">o</seg>b<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r</w> <w n="2.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg></w> <w n="2.6" punct="pv:10">n<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pv">ou</seg>s</rhyme></pgtc></w> ;</l>
							<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="3.2">s</w>'<w n="3.3"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="3.4">s</w>'<w n="3.5"><seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="3.7">f<seg phoneme="y" type="vs" value="1" rule="449" place="6" mp="M">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w> <w n="3.8"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="9" mp="C">un</seg></w> <w n="3.9" punct="vg:10">br<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="4.2">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2" mp="C">e</seg>t</w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" mp="M">em</seg>pl<seg phoneme="wa" type="vs" value="1" rule="422" place="4" caesura="1">oi</seg></w><caesura></caesura> <w n="4.4">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5" mp="M">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="418" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="4.5">n</w>'<w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="8">e</seg>st</w> <w n="4.7" punct="pt:10">j<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>l<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pt">ou</seg>x</rhyme></pgtc></w>.</l>
							<l n="5" num="1.5" lm="8"><space unit="char" quantity="4"></space><w n="5.1">C</w>'<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="5.3"><seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.4">f<seg phoneme="y" type="vs" value="1" rule="452" place="4">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.5" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ct<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>b</w> <w n="6.3">f<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>l</w><caesura></caesura> <w n="6.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="6.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" mp="C">on</seg></w> <w n="6.6">fr<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="8" mp="C">un</seg></w> <w n="6.8" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pv">i</seg></rhyme></pgtc></w> ;</l>
							<l n="7" num="1.7" lm="9" met="3+6"><w n="7.1">C</w>'<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="317" place="2" mp="M">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" caesura="1">an</seg>t</w><caesura></caesura> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="7.5">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5" mp="M">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="7.6">p<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="P">ou</seg>r</w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="7.8" punct="vg:9">gl<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="9">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="8" num="1.8" lm="9" met="3−6" mp3="Pem"><space unit="char" quantity="4"></space><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="M">Au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem" caesura="1">e</seg></w><caesura></caesura> <w n="8.3">g<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg></w> <w n="8.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="P">ou</seg>r</w> <w n="8.5">l</w>'<w n="8.6" punct="pt:9"><seg phoneme="e" type="vs" value="1" rule="169" place="7" mp="M">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="9" punct="pt">i</seg></rhyme></pgtc></w>.</l>
						</lg>
					</div></body></text></TEI>