<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE VIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC24" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(ababcdcd)" er_moy="1.0" er_max="2" er_min="0" er_mode="0(2/4)" er_moy_et="1.0">
					<head type="tune">AIR : Restez, troupe jolie.</head>
						<lg n="1" type="huitain" rhyme="ababcdcd">
							<head type="main">ARWED.</head>
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="1.2" punct="vg:2">ch<seg phoneme="ɛ" type="vs" value="1" rule="63" place="2" punct="vg">e</seg>r</w>, <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4">tr<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="1.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="1.6">m<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg><pgtc id="1" weight="2" schema="CR">ss<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="2.2">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ts</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="2.5" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>l<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>n<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" punct="vg">e</seg>ls</rhyme></pgtc></w>,</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="3.2">b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="3.3">n</w>'<w n="3.4"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rgn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.5" punct="vg:8">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>r<pgtc id="1" weight="2" schema="CR">s<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>d</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>g<seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>x</w> <w n="4.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="4.6" punct="pt:8">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rt<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" punct="pt">e</seg>ls</rhyme></pgtc></w>.</l>
							<l n="5" num="1.5" lm="8" met="8"><w n="5.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="5.2" punct="vg:2">tr<seg phoneme="wa" type="vs" value="1" rule="419" place="2" punct="vg">oi</seg>s</w>, <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="5.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.5">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="5.6" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="7">ai</seg>m<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="407" place="2">e</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w>-<w n="6.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6">e</seg>c</w> <w n="6.4" punct="pv:8">g<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">aî</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pv">é</seg></rhyme></pgtc></w> ;</l>
							<l n="7" num="1.7" lm="8" met="8"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r</w> <w n="7.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="7.5">d<seg phoneme="ɔ" type="vs" value="1" rule="418" place="6">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="341" place="7">à</seg></w> <w n="7.7">t<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="y" type="vs" value="1" rule="452" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="8.3">d</w>'<w n="8.4" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
						</lg>
					</div></body></text></TEI>