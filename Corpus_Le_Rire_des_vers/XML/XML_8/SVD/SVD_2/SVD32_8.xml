<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANGÉLIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIEME.</head><head type="main_subpart">SCÈNE V.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD32" modus="cp" lm_max="9" metProfile="4, 8, 4=5, (7)" form="suite de strophes" schema="6[aa] 1[aaa]" er_moy="9.62" er_max="21" er_min="0" er_mode="0(2/8)" er_moy_et="9.29">
				<head type="tune">Même air.</head>
				<lg n="1" type="regexp" rhyme="aaaaaaaaaaaaaaa">
					<head type="main">MÈRE GIBOU.</head>
						<l n="1" num="1.1" lm="4" met="4"><space unit="char" quantity="10"></space><w n="1.1"><pgtc id="1" weight="21" schema="CV[C[VCV[CGR" part="1">Ç<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></pgtc></w> <w n="1.2"><pgtc id="1" weight="21" schema="CV[C[VCV[CGR" part="2">t</pgtc></w>'<w n="1.3"><pgtc id="1" weight="21" schema="CV[C[VCV[CGR" part="3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</pgtc></w> <w n="1.4"><pgtc id="1" weight="21" schema="CV[C[VCV[CGR" part="4">bi<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4">en</seg></rhyme></pgtc></w> <del hand="LG" reason="analysis" type="repetition">(bis)</del></l>
						<l n="2" num="1.2" lm="4" met="4"><space unit="char" quantity="10"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="2.1"><pgtc id="1" weight="21" schema="CV[C[VCV[CGR" part="1">Ç<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></pgtc></w> <w n="2.2"><pgtc id="1" weight="21" schema="CV[C[VCV[CGR" part="2">t</pgtc></w>'<w n="2.3"><pgtc id="1" weight="21" schema="CV[C[VCV[CGR" part="3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</pgtc></w> <w n="2.4"><pgtc id="1" weight="21" schema="CV[C[VCV[CGR" part="4">bi<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4">en</seg></rhyme></pgtc></w></add></subst></l>
						<l n="3" num="1.3" lm="9" met="4+5"><w n="3.1">T<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="3.2">y<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="3.3" punct="ps:4">b<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="ps" caesura="1">é</seg>s</w> …<caesura></caesura> <w n="3.4">t<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="3.5">pʼt<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="3.6" punct="vg:9">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><pgtc id="2" weight="0" schema="R" part="1"><rhyme label="a" id="2" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="9" met="5+4" mp4="Fc"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="y" type="vs" value="1" rule="452" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="Fc">e</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" caesura="1">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w><caesura></caesura> <w n="4.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="P">ou</seg>r</w> <w n="4.6">t<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="4.7" punct="vg:9">m<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="a" id="2" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">Lʼ</w> <w n="5.2">j<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.4">t<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="5.5">n<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6">e</seg>c</w> <w n="5.7" punct="vg:8">Gr<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>t<pgtc id="3" weight="1" schema="GR" part="1">i<rhyme label="a" id="3" gender="m" type="a" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="8" punct="vg">en</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="1.6" lm="4" met="4"><space unit="char" quantity="10"></space><w n="6.1">Ç<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="6.2">t</w>'<w n="6.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="6.4">b<pgtc id="3" weight="1" schema="GR" part="1">i<rhyme label="a" id="3" gender="m" type="e" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4">en</seg></rhyme></pgtc></w> <del hand="LG" reason="analysis" type="repetition">(bis)</del></l>
						<l n="7" num="1.7" lm="4" met="4"><space unit="char" quantity="10"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="7.1"><pgtc id="3" weight="21" schema="CV[C[VCV[CGR" part="1">Ç<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></pgtc></w> <w n="7.2"><pgtc id="3" weight="21" schema="CV[C[VCV[CGR" part="2">t</pgtc></w>'<w n="7.3"><pgtc id="3" weight="21" schema="CV[C[VCV[CGR" part="3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</pgtc></w> <w n="7.4"><pgtc id="3" weight="21" schema="CV[C[VCV[CGR" part="4">bi<rhyme label="a" id="3" gender="m" type="a" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4">en</seg></rhyme></pgtc></w></add></subst></l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="8.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="2">ain</seg>tʼn<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="8.3">qu</w>' <w n="8.4">t<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="8.5">f<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ssʼ</w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="8.7">s<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>v<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="a" id="4" gender="f" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="9" num="1.9" lm="8" met="8"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="9.2"><seg phoneme="y" type="vs" value="1" rule="462" place="2">u</seg>nʼ</w> <w n="9.3">j<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="351" place="4">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="9.5">p<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg></w> <w n="9.6">l<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>g<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="a" id="4" gender="f" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="10" num="1.10" lm="8" met="8"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="10.2">n</w>'<w n="10.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="10.5">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>mʼ</w> <w n="10.6">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="10.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="10.8">f<seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>x</w> <w n="10.9" punct="ps:8"><pgtc id="5" weight="2" schema="[CR" part="1">p<rhyme label="a" id="5" gender="m" type="a" stanza="5"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="ps">a</seg>s</rhyme></pgtc></w>…</l>
						<l n="11" num="1.11" lm="4" met="4"><space unit="char" quantity="10"></space><w n="11.1">Ç<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="11.2">nʼ</w> <w n="11.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.4">v<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="11.5" punct="pt:4"><pgtc id="5" weight="2" schema="[CR" part="1">p<rhyme label="a" id="5" gender="m" type="e" stanza="5"><seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="pt">a</seg>s</rhyme></pgtc></w>. <del hand="LG" reason="analysis" type="repetition">(bis)</del></l>
						<l n="12" num="1.12" lm="4" met="4"><space unit="char" quantity="10"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="12.1">Ç<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="12.2">nʼt<pgtc id="6" weight="12" schema="V[CV[CR" part="1"><seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></pgtc></w> <w n="12.3"><pgtc id="6" weight="12" schema="V[CV[CR" part="2">v<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></pgtc></w> <w n="12.4" punct="pt:4"><pgtc id="6" weight="12" schema="V[CV[CR" part="3">p<rhyme label="a" id="6" gender="m" type="a" stanza="6"><seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="pt">a</seg>s</rhyme></pgtc></w>.</add></subst></l>
						<l n="13" num="1.13" lm="7"><space unit="char" quantity="4"></space><w n="13.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="vg">on</seg></w>, <w n="13.2" punct="vg:2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg></w>, <w n="13.3" punct="ps:3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="ps">on</seg></w> … <w n="13.4">ç<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="13.5">nʼ</w> <w n="13.6">t<pgtc id="6" weight="12" schema="V[CV[CR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></pgtc></w> <w n="13.7"><pgtc id="6" weight="12" schema="V[CV[CR" part="2">v<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></pgtc></w> <w n="13.8" punct="pv:7"><pgtc id="6" weight="12" schema="V[CV[CR" part="3">p<rhyme label="a" id="6" gender="m" type="e" stanza="6"><seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="pv">a</seg>s</rhyme></pgtc></w> ;</l>
						<l n="14" num="1.14" lm="8" met="8"><w n="14.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="vg">on</seg></w>, <w n="14.2" punct="vg:2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg></w>, <w n="14.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="14.4" punct="vg:4">f<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>llʼ</w>, <w n="14.5"><pgtc id="7" weight="20" schema="[CV[[CV[CV[CR" part="1">ç<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></pgtc></w> <w n="14.6"><pgtc id="7" weight="20" schema="[CV[[CV[CV[CR" part="2">nʼ</pgtc></w> <w n="14.7"><pgtc id="7" weight="20" schema="[CV[[CV[CV[CR" part="3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></pgtc></w> <w n="14.8"><pgtc id="7" weight="20" schema="[CV[[CV[CV[CR" part="4">v<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></pgtc></w> <w n="14.9" punct="vg:8"><pgtc id="7" weight="20" schema="[CV[[CV[CV[CR" part="5">p<rhyme label="a" id="7" gender="m" type="a" stanza="7"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg>s</rhyme></pgtc></w>,</l>
						<l n="15" num="1.15" lm="8" met="8"><w n="15.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="vg">on</seg></w>, <w n="15.2" punct="vg:2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg></w>, <w n="15.3" punct="vg:3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="vg">on</seg></w>, <w n="15.4" punct="ps:4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="ps">on</seg></w> … <w n="15.5"><pgtc id="7" weight="20" schema="[CV[[CV[CV[CR" part="1">ç<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></pgtc></w> <w n="15.6"><pgtc id="7" weight="20" schema="[CV[[CV[CV[CR" part="2">nʼ</pgtc></w> <w n="15.7"><pgtc id="7" weight="20" schema="[CV[[CV[CV[CR" part="3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></pgtc></w> <w n="15.8"><pgtc id="7" weight="20" schema="[CV[[CV[CV[CR" part="4">v<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></pgtc></w> <w n="15.9" punct="pe:8"><pgtc id="7" weight="20" schema="[CV[[CV[CV[CR" part="5">p<rhyme label="a" id="7" gender="m" type="e" stanza="7"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pe">a</seg>s</rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>