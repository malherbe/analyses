<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANGÉLIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE III.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD19" modus="cm" lm_max="10" metProfile="4+6" form="strophe unique avec vers clausule" schema="1(abab) 1(b)" er_moy="2.67" er_max="8" er_min="0" er_mode="0(2/3)" er_moy_et="3.77">
				<head type="tune">Air : De la Villageoise somnambule.</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<head type="main">DELAUNAY</head>
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">J<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>s</w> <w n="1.2">l</w>'<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>r</w><caesura></caesura> <w n="1.4">ch<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="317" place="7" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg></w> <w n="1.6">m<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="1.7" punct="pv:10">v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="481" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">lu<seg phoneme="i" type="vs" value="1" rule="490" place="3" mp="C">i</seg></w> <w n="2.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="4" caesura="1">u</seg>s</w><caesura></caesura> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="2.6">pl<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>rs</w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="2.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="2.9">b<seg phoneme="o" type="vs" value="1" rule="314" place="9">eau</seg>x</w> <w n="2.10" punct="pe:10">j<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pe">ou</seg>rs</rhyme></pgtc></w> !</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="C">ou</seg>s</w> <w n="3.4" punct="ps:4">fu<seg phoneme="i" type="vs" value="1" rule="490" place="4" punct="ps" caesura="1">i</seg>s</w> …<caesura></caesura> <w n="3.5">c<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r</w> <w n="3.6">m<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="3.7">t<seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.8"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="8">e</seg>st</w> <w n="3.9" punct="vg:10">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="M">an</seg>ch<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="481" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="4.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="4.3">gr<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="4.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="C">on</seg></w> <w n="4.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="4.6">p<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r</w> <w n="4.7"><seg phoneme="o" type="vs" value="1" rule="317" place="8" mp="C">au</seg>x</w> <w n="4.8" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pt">ou</seg>rs</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="vers clausule" rhyme="b">
					<head type="main">DELAUNAY ET MAGLOIRE.</head>
				<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1" punct="pe:2">Cr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1" mp="M">ai</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="pe">on</seg>s</w> ! <subst hand="LG" reason="analysis" type="repetition"><del>(bis)</del><add rend="hidden"> <w n="5.2" punct="pe:4">Cr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3" mp="M">ai</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="pe" caesura="1">on</seg>s</w> !<caesura></caesura></add></subst> <w n="5.3">d</w>'<w n="5.4"><seg phoneme="e" type="vs" value="1" rule="352" place="5" mp="M">e</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="338" place="6" mp="M">a</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg><pgtc id="2" weight="8" schema="C[VCR" part="1">s</pgtc></w> <w n="5.6" punct="pt:10"><pgtc id="2" weight="8" schema="C[VCR" part="2"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pt">ou</seg>rs</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>