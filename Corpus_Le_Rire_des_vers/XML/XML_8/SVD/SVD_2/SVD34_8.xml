<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANGÉLIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIEME.</head><head type="main_subpart">SCÈNE VIII</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD34" modus="sp" lm_max="6" metProfile="4, 2, 6" form="suite périodique" schema="2(aabbccc) 1(abab)" er_moy="0.8" er_max="2" er_min="0" er_mode="0(6/10)" er_moy_et="0.98">
					<lg n="1" type="septain" rhyme="aabbccc">
						<head type="main">CHŒUR, en dehors.</head>
						<l n="1" num="1.1" lm="4" met="4"><space unit="char" quantity="4"></space><w n="1.1">Qu</w>'<w n="1.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="1.4">h<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="4" met="4"><space unit="char" quantity="4"></space><w n="2.1">Qu</w>'<w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="2.4" punct="ps:4">j<seg phoneme="wa" type="vs" value="1" rule="439" place="3">o</seg>y<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="397" place="4" punct="ps">eu</seg>x</rhyme></pgtc></w> …</l>
						<l n="3" num="1.3" lm="2" met="2"><space unit="char" quantity="8"></space><w n="3.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>qu<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="484" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="4" met="4"><space unit="char" quantity="4"></space><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="4.2" punct="vg:4">R<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="3">ain</seg>v<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="5" num="1.5" lm="4" met="4"><space unit="char" quantity="4"></space><w n="5.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="5.2">b<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="5.3">ch<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r<pgtc id="3" weight="2" schema="CR">m<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</rhyme></pgtc></w></l>
						<l n="6" num="1.6" lm="4" met="4"><space unit="char" quantity="4"></space><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg><pgtc id="3" weight="2" schema="CR">m<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</rhyme></pgtc></w></l>
						<l n="7" num="1.7" lm="6" met="6"><w n="7.1"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">O</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="7.2">m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>gr<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg><pgtc id="3" weight="2" schema="CR">m<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="6" punct="pe">en</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<head type="main">GRATIEN.</head>
						<l n="8" num="2.1" lm="6" met="6"><w n="8.1">V<seg phoneme="wa" type="vs" value="1" rule="419" place="1">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="8.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>nn<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="9" num="2.2" lm="6" met="6"><w n="9.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="9.3">m<seg phoneme="e" type="vs" value="1" rule="352" place="3">e</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="9.5" punct="pv:6">tr<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" punct="pv">ain</seg></rhyme></pgtc></w> ;</l>
						<l n="10" num="2.3" lm="6" met="6"><w n="10.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="10.2" punct="vg:4">J<seg phoneme="a" type="vs" value="1" rule="309" place="2">ea</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="10.3">r<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>p<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="11" num="2.4" lm="6" met="6"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="11.2">m<seg phoneme="wa" type="vs" value="1" rule="422" place="3">oi</seg></w> <w n="11.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.4" punct="ps:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>fr<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" punct="ps">ain</seg></rhyme></pgtc></w> …</l>
					</lg>
					<lg n="3" type="septain" rhyme="aabbccc">
						<head type="main">TOUS DEUX.</head>
						<l n="12" num="3.1" lm="4" met="4"><space unit="char" quantity="4"></space><w n="12.1">Qu</w>'<w n="12.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="12.4" punct="vg:4">h<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="4" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="13" num="3.2" lm="4" met="4"><space unit="char" quantity="4"></space><w n="13.1">Qu</w>'<w n="13.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="13.4" punct="vg:4">j<seg phoneme="wa" type="vs" value="1" rule="439" place="3">o</seg>y<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="397" place="4" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="14" num="3.3" lm="2" met="2"><space unit="char" quantity="8"></space><subst type="repetition" hand="LG" reason="analysis"><del>etc. , etc. </del><add rend="hidden"><w n="14.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>qu<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="484" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></rhyme></pgtc></w></add></subst></l>
						<l n="15" num="3.4" lm="4" met="4"><space unit="char" quantity="4"></space><subst type="repetition" hand="LG" reason="analysis"><del> </del><add rend="hidden"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="15.2" punct="vg:4">R<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="3">ain</seg>v<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>, </add></subst></l>
						<l n="16" num="3.5" lm="4" met="4"><space unit="char" quantity="4"></space><subst type="repetition" hand="LG" reason="analysis"><del> </del><add rend="hidden"><w n="16.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="16.2">b<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="16.3">ch<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r<pgtc id="8" weight="2" schema="CR">m<rhyme label="c" id="8" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</rhyme></pgtc></w></add></subst></l>
						<l n="17" num="3.6" lm="4" met="4"><space unit="char" quantity="4"></space><subst type="repetition" hand="LG" reason="analysis"><del> </del><add rend="hidden"><w n="17.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg><pgtc id="8" weight="2" schema="CR">m<rhyme label="c" id="8" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</rhyme></pgtc></w></add></subst></l>
						<l n="18" num="3.7" lm="6" met="6"><subst type="repetition" hand="LG" reason="analysis"><del> </del><add rend="hidden"><w n="18.1"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">O</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="18.2">m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>gr<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg><pgtc id="8" weight="2" schema="CR">m<rhyme label="c" id="8" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="6" punct="pe">en</seg>s</rhyme></pgtc></w> ! </add></subst></l>
					</lg>
				</div></body></text></TEI>