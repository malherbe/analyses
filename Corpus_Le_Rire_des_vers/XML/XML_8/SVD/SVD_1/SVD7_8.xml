<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">COMMÈRE JEANNETON</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>261 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE XI.SCÈNE XII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD7" modus="sp" lm_max="8" metProfile="8, 6, 4" form="suite de strophes" schema="7[abba] 4[aa] 1[aaa]" er_moy="0.4" er_max="2" er_min="0" er_mode="0(16/20)" er_moy_et="0.8">
				<head type="tune">Air du Tictac (de Marie).</head>
				<lg n="1" type="regexp" rhyme="abba">
				<head type="main">DELAUNAY, à Jeanneton.</head>
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="pe:1"><seg phoneme="e" type="vs" value="1" rule="132" place="1" punct="pe">E</seg>h</w> ! <w n="1.2" punct="vg:2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="2" punct="vg">en</seg></w>, <w n="1.3">d<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w>-<w n="1.4">m<seg phoneme="wa" type="vs" value="1" rule="422" place="5">oi</seg></w> <w n="1.5">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="1.6" punct="vg:8">m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="vg">ain</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="2.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="2.3" punct="pt:6">c<seg phoneme="o" type="vs" value="1" rule="434" place="5">o</seg>mm<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<p>(A Gratien.)</p>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="3.2" punct="vg:2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2" punct="vg">ou</seg>s</w>, <w n="3.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w>-<w n="3.4" punct="vg:5">l<seg phoneme="a" type="vs" value="1" rule="341" place="5" punct="vg">à</seg></w>, <w n="3.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="3.6" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">om</seg>p<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">C</w>'<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="4.3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="2">oi</seg></w> <w n="4.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="4.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg></w> <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.7" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rr<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pt">ain</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="regexp" rhyme="aaaa">
					<head type="main">TOUS.</head>
					<l n="5" num="2.1" lm="4" met="4"><space unit="char" quantity="8"></space><w n="5.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="1">e</seg>l</w> <w n="5.2">j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r</w> <w n="5.3" punct="pe:4">pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg><pgtc id="3" weight="2" schema="CR">sp<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="6" num="2.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="6.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="6.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">l</w>'<w n="6.4" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg><pgtc id="3" weight="2" schema="CR">sp<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="4" met="4"><space unit="char" quantity="8"></space><w n="7.1">C</w>'<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="7.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="7.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a" stanza="3"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r</rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="8.2">l</w>'<w n="8.3" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="e" stanza="3"><seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="pe">i</seg>r</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="3" type="regexp" rhyme="a">
					<head type="main">GRATIEN.</head>
					<l n="9" num="3.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="9.2">v<seg phoneme="wa" type="vs" value="1" rule="439" place="2">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="9.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="9.4">j</w>'<w n="9.5"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg><pgtc id="5" weight="2" schema="CR">r<rhyme label="a" id="5" gender="m" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg></rhyme></pgtc></w></l>
					<l rhyme="none" n="10" num="3.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="10.3" punct="pt:4">c<seg phoneme="o" type="vs" value="1" rule="434" place="3">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="4" type="regexp" rhyme="aa">
					<head type="main">DELAUNAY.</head>
					<l n="11" num="4.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="11.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="11.2">t<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rd</w> <w n="11.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="11.5">d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg><pgtc id="5" weight="2" schema="CR">r<rhyme label="a" id="5" gender="m" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg></rhyme></pgtc></w></l>
					<l n="12" num="4.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="12.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3" punct="pt:4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>dr<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4" punct="pt">ai</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="5" type="regexp" rhyme="abba">
					<head type="main">GRATIEN</head>
					<l n="13" num="5.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="13.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="13.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r</w> <w n="13.3" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="3" punct="vg">oi</seg></w>, <w n="13.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>t</w> <w n="13.5">d<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>j<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="a" stanza="5"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></rhyme></pgtc></w></l>
					<l n="14" num="5.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="14.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w> <w n="14.2">d</w>'<w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="14.4">h<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="14.5" punct="pe:6">pr<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>s<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="f" type="a" stanza="5"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="15" num="5.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="15.3">lʼ</w> <w n="15.4" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ri<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="f" type="e" stanza="5"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="16" num="5.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="16.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">vʼl<seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="16.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>rs</w> <w n="16.4" punct="pt:6">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>p<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="e" stanza="5"><seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="pt">a</seg></rhyme></pgtc></w>.</l>
				</lg>
				<div type="section" n="1">
					<head type="main">ENSEMBLE. |</head>
					<lg n="1" type="regexp" rhyme="abba">
						<head type="main">DELAUNAY.</head>
						<l n="17" num="1.1" lm="8" met="8"><w n="17.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="17.2">d<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w>-<w n="17.3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="5">oi</seg></w> <w n="17.4">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="17.5" punct="vg:8">m<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="m" type="a" stanza="6"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="vg">ain</seg></rhyme></pgtc></w>, <del reason="analysis" type="repetition" hand="LG">(etc.)</del></l>
						<l n="18" num="1.2" lm="6" met="6"><space unit="char" quantity="4"></space><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="18.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="18.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="18.3" punct="pt:6">c<seg phoneme="o" type="vs" value="1" rule="434" place="5">o</seg>mm<pgtc id="9" weight="0" schema="R"><rhyme label="b" id="9" gender="f" type="a" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>. </add></subst></l>
						<p>(A Gratien.)</p>
						<l n="19" num="1.3" lm="8" met="8"><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="19.2" punct="vg:2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2" punct="vg">ou</seg>s</w>, <w n="19.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w>-<w n="19.4" punct="vg:5">l<seg phoneme="a" type="vs" value="1" rule="341" place="5" punct="vg">à</seg></w>, <w n="19.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="19.6" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">om</seg>p<pgtc id="9" weight="0" schema="R"><rhyme label="b" id="9" gender="f" type="e" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>, </add></subst></l>
						<l n="20" num="1.4" lm="8" met="8"><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="20.1">C</w>'<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="20.3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="2">oi</seg></w> <w n="20.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="20.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg></w> <w n="20.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.7" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rr<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="m" type="e" stanza="6"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pt">ain</seg></rhyme></pgtc></w>.</add></subst></l>
					</lg>
					<lg n="2" type="regexp" rhyme="abba">
						<head type="main">GRATIEN.</head>
						<l n="21" num="2.1" lm="8" met="8"><w n="21.1" punct="vg:2">T<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="vg">ez</seg></w>, <w n="21.2" punct="vg:4">m<seg phoneme="œ" type="vs" value="1" rule="150" place="3">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="4" punct="vg">eu</seg>r</w>, <w n="21.3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="21.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="21.5" punct="vg:8">m<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="m" type="a" stanza="7"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="vg">ain</seg></rhyme></pgtc></w>,</l>
						<l n="22" num="2.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="22.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="22.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="22.3" punct="pt:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">om</seg><pgtc id="11" weight="2" schema="CR">p<rhyme label="b" id="11" gender="f" type="a" stanza="7"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="23" num="2.3" lm="8" met="8"><w n="23.1">Dʼ</w> <w n="23.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="23.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="23.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="23.5">sʼr<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w> <w n="23.6">lʼs<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>d</w> <w n="23.7" punct="pv:8"><pgtc id="11" weight="2" schema="[CR">p<rhyme label="b" id="11" gender="f" type="e" stanza="7"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="24" num="2.4" lm="8" met="8"><w n="24.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="24.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="24.3">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-8" place="4">e</seg>r</w> <w n="24.4">d</w>'<w n="24.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="24.6">t<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6">e</seg>l</w> <w n="24.7" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rr<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="m" type="e" stanza="7"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pt">ain</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="regexp" rhyme="abba">
						<head type="main">JEANNETON.</head>
						<l n="25" num="3.1" lm="8" met="8"><w n="25.1" punct="vg:2">T<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="vg">ez</seg></w>, <w n="25.2" punct="vg:4">m<seg phoneme="œ" type="vs" value="1" rule="150" place="3">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="4" punct="vg">eu</seg>r</w>, <w n="25.3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="25.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="25.5" punct="vg:8">m<pgtc id="12" weight="0" schema="R"><rhyme label="a" id="12" gender="m" type="a" stanza="8"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="vg">ain</seg></rhyme></pgtc></w>,</l>
						<l n="26" num="3.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="26.1">N<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="26.2">m<seg phoneme="wa" type="vs" value="1" rule="422" place="3">oi</seg></w> <w n="26.3">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>trʼ</w> <w n="26.4" punct="pt:6">c<seg phoneme="o" type="vs" value="1" rule="434" place="5">o</seg>mm<pgtc id="13" weight="0" schema="R"><rhyme label="b" id="13" gender="f" type="a" stanza="8"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="27" num="3.3" lm="8" met="8"><w n="27.1">Dʼ</w> <w n="27.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="27.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="27.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="27.5">sʼr<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w> <w n="27.6">lʼ</w> <w n="27.7">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>d</w> <w n="27.8" punct="pv:8">p<pgtc id="13" weight="0" schema="R"><rhyme label="b" id="13" gender="f" type="e" stanza="8"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="28" num="3.4" lm="8" met="8"><w n="28.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="28.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="28.3">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-8" place="4">e</seg>r</w> <w n="28.4">d</w>'<w n="28.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="28.6">t<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6">e</seg>l</w> <w n="28.7" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rr<pgtc id="12" weight="0" schema="R"><rhyme label="a" id="12" gender="m" type="e" stanza="8"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pt">ain</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="regexp" rhyme="abba">
						<head type="main">MADAME TRÉDIER.</head>
						<l n="29" num="4.1" lm="8" met="8"><w n="29.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="29.2">d<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w>-<w n="29.3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="5">oi</seg></w> <w n="29.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="29.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="29.6" punct="vg:8">m<pgtc id="14" weight="0" schema="R"><rhyme label="a" id="14" gender="m" type="a" stanza="9"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="vg">ain</seg></rhyme></pgtc></w>,</l>
						<l n="30" num="4.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="30.1">Lʼ</w> <w n="30.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">om</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.3"><seg phoneme="e" type="vs" value="1" rule="188" place="3">e</seg>t</w> <w n="30.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="30.5" punct="pt:6">c<seg phoneme="o" type="vs" value="1" rule="434" place="5">o</seg>mm<pgtc id="15" weight="0" schema="R"><rhyme label="b" id="15" gender="f" type="a" stanza="9"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="31" num="4.3" lm="8" met="8"><w n="31.1">Dʼ</w> <w n="31.2">l</w>'<w n="31.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="31.4">m<seg phoneme="œ" type="vs" value="1" rule="150" place="3">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="4">eu</seg>r</w> <w n="31.5">sʼr<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="31.6">lʼ</w> <w n="31.7">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>d</w> <w n="31.8" punct="pv:8">p<pgtc id="15" weight="0" schema="R"><rhyme label="b" id="15" gender="f" type="e" stanza="9"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="32" num="4.4" lm="8" met="8"><w n="32.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="32.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="32.3">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-8" place="4">e</seg>r</w> <w n="32.4">d</w>'<w n="32.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="32.6">t<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6">e</seg>l</w> <w n="32.7" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rr<pgtc id="14" weight="0" schema="R"><rhyme label="a" id="14" gender="m" type="e" stanza="9"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pt">ain</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="5" type="regexp" rhyme="abba">
						<head type="main">MAGLOIRE.</head>
						<l n="33" num="5.1" lm="8" met="8"><w n="33.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="33.2">d<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w>-<w n="33.3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="5">oi</seg></w> <w n="33.4">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="33.5" punct="vg:8">m<pgtc id="16" weight="0" schema="R"><rhyme label="a" id="16" gender="m" type="a" stanza="10"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="vg">ain</seg></rhyme></pgtc></w>, <del reason="analysis" type="repetition" hand="LG">(etc.)</del></l>
						<l n="34" num="5.2" lm="6" met="6"><space unit="char" quantity="4"></space><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="34.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="34.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="34.3" punct="pt:6">c<seg phoneme="o" type="vs" value="1" rule="434" place="5">o</seg>mm<pgtc id="17" weight="0" schema="R"><rhyme label="b" id="17" gender="f" type="a" stanza="10"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>. </add></subst></l>
						<p>(A Gratien.)</p>
						<l n="35" num="5.3" lm="8" met="8"><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="35.2" punct="vg:2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2" punct="vg">ou</seg>s</w>, <w n="35.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w>-<w n="35.4" punct="vg:5">l<seg phoneme="a" type="vs" value="1" rule="341" place="5" punct="vg">à</seg></w>, <w n="35.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="35.6" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">om</seg>p<pgtc id="17" weight="0" schema="R"><rhyme label="b" id="17" gender="f" type="e" stanza="10"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>, </add></subst></l>
						<l n="36" num="5.4" lm="8" met="8"><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="36.1">C</w>'<w n="36.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="36.3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="2">oi</seg></w> <w n="36.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="36.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg></w> <w n="36.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="36.7" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rr<pgtc id="16" weight="0" schema="R"><rhyme label="a" id="16" gender="m" type="e" stanza="10"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pt">ain</seg></rhyme></pgtc></w>.</add></subst></l>
					</lg>
					<lg n="6" type="regexp" rhyme="aaaa">
						<head type="main">TOUS.</head>
						<l n="37" num="6.1" lm="4" met="4"><space unit="char" quantity="8"></space><w n="37.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="1">e</seg>l</w> <w n="37.2">j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r</w> <w n="37.3" punct="pe:4">pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg><pgtc id="18" weight="2" schema="CR">sp<rhyme label="a" id="18" gender="f" type="a" stanza="11"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="38" num="6.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="38.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="38.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="38.3">l</w>'<w n="38.4" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg><pgtc id="18" weight="2" schema="CR">sp<rhyme label="a" id="18" gender="f" type="e" stanza="11"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="39" num="6.3" lm="4" met="4"><space unit="char" quantity="8"></space><w n="39.1">C</w>'<w n="39.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="39.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="39.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s<pgtc id="19" weight="0" schema="R"><rhyme label="a" id="19" gender="m" type="a" stanza="12"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r</rhyme></pgtc></w></l>
						<l n="40" num="6.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="40.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="40.2">l</w>'<w n="40.3" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<pgtc id="19" weight="0" schema="R"><rhyme label="a" id="19" gender="m" type="e" stanza="12"><seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="pe">i</seg>r</rhyme></pgtc></w> !</l>
					</lg>
				</div>
			</div></body></text></TEI>