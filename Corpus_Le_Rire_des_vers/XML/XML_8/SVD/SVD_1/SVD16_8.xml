<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">COMMÈRE JEANNETON</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>261 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIEME</head><head type="main_subpart">SCÈNE VIII.SCÈNE IX.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD16" modus="sm" lm_max="7" metProfile="7" form="suite périodique" schema="2(abab)" er_moy="0.0" er_max="0" er_min="0" er_mode="0(4/4)" er_moy_et="0.0">
			<head type="tune">AIR : Vive ! vive l'Italie.</head>
			<lg n="1" type="quatrain" rhyme="abab">
				<head type="main">CHŒUR.</head>
				<l n="1" num="1.1" lm="7" met="7"><w n="1.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="1.3">c<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>r<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>m<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>n<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="481" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
				<l n="2" num="1.2" lm="7" met="7"><w n="2.1">Qu</w>' <w n="2.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="2.3">c<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>br<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="2.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.6" punct="vg:7">j<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="7" punct="vg">ou</seg>r</rhyme></pgtc></w>,</l>
				<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="3.2">pl<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s</w> <w n="3.3">t<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rd</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="3.6">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>r<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="481" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
				<l n="4" num="1.4" lm="7" met="7"><w n="4.1">Lʼ</w> <w n="4.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="4.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="4.5" punct="pt:7">t<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="7" punct="pt">ou</seg>r</rhyme></pgtc></w>.</l>
			</lg>
			<p>
			(Pendant ce qui suit la musique continue en sourdine jusqu'à la reprise du chœur.)<lb></lb>
			<lb></lb>
			MÈRE GIBOU.<lb></lb> 
			Allons ! mes petits enfans, fleurissez-vous, v'là d' la jonquille, dl' la r'noncule
			et du pois de senteur ; je veux que nous embaumions comme des parfumeurs. <lb></lb><lb></lb>
			DELAUNAY, lui présentant la corbeille. <lb></lb>
			Commère Gibou, voulez-vous accepter le cadeau du parrain ? <lb></lb><lb></lb>
			MÈRE GIBOU. <lb></lb>
			Comment, c'est vous qui êtes mon compère ? c'est qu'il est gentil tout d' même ! c'
			garçon-là. (Examinant la corbeille. ) Tout ça brille comme un soleil ; vive les gens
			comme il faut pour faire des cadeaux ! embrasse-moi, mon poulot. <lb></lb><lb></lb>
			DELAUNAY. <lb></lb>
			Volontiers, commère. (N l'embrasse. )<lb></lb><lb></lb>
			MAGLOIRE. <lb></lb>
			Respectable madame Gibou, si vous voulez me permettre. <lb></lb><lb></lb>
			MÈRE GIBOU. <lb></lb>
			Comment, toi aussi, mon petit, allons viens-y, bouffi. (Elle l'embrasse. ) Quant à
			toi, Jeannelon, j'te tiens quitte de tout, car tu l' sauras peut-être un jour comme
			moi, y a tant de plaisir à pardonner à son enfant ! <lb></lb><lb></lb>
			GRATIEN. <lb></lb>
			Maintenant, que chacun prenne sa chacune, et en avant, marche ! ...<lb></lb>
			</p>
			<lg n="2" type="quatrain" rhyme="abab">
				<head type="main">CHŒUR.</head>
				<l n="5" num="2.1" lm="7" met="7"><w n="5.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="5.3" punct="vg:7">c<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>r<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>m<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>n<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="481" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>, <del reason="analysis" type="repetition" hand="LG">etc., etc.</del></l>
				<l n="6" num="2.2" lm="7" met="7"><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="6.1">Qu</w>' <w n="6.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="6.3">c<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>br<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="6.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.6" punct="vg:7">j<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="7" punct="vg">ou</seg>r</rhyme></pgtc></w>,</add></subst></l>
				<l n="7" num="2.3" lm="7" met="7"><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">pl<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s</w> <w n="7.3">t<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rd</w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="7.6">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>r<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="481" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></add></subst></l>
				<l n="8" num="2.4" lm="7" met="7"><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="8.1">Lʼ</w> <w n="8.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="8.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="8.5" punct="pt:7">t<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="7" punct="pt">ou</seg>r</rhyme></pgtc></w>. </add></subst></l>
			</lg>
		</div></body></text></TEI>