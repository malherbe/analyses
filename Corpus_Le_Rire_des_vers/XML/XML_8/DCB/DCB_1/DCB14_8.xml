<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB14" modus="sp" lm_max="8" metProfile="3, 8" form="suite périodique" schema="2(ababbba)" er_moy="2.0" er_max="8" er_min="0" er_mode="0(5/7)" er_moy_et="3.21">
	<head type="tune">AIR de M. Alpb. Gilbert.</head>
		<div type="section" n="1">
			<lg n="1" type="septain" rhyme="ababbba">
				<l n="1" num="1.1" lm="3" met="3"><w n="1.1">C<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>st<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="466" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></pgtc></w></l>
				<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="2.2">br<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>rs</w> <w n="2.4" punct="dp:8"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="6">ai</seg><pgtc id="2" weight="8" schema="CVCR">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="dp">a</seg></rhyme></pgtc></w> :</l>
				<l n="3" num="1.3" lm="8" met="8"><w n="3.1">J<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg></w> <w n="3.3">f<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg></w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="3.6">cu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg>s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="466" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
				<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">s</w>'<w n="4.5" punct="ps:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg><pgtc id="2" weight="8" schema="CVCR">mm<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="ps">a</seg></rhyme></pgtc></w>…</l>
				<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="5.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2">an</seg>s</w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="5.4" punct="vg:5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" punct="vg">em</seg>ps</w>, <w n="5.5">d</w>'<w n="5.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="7">è</seg>s</w> <w n="5.7" punct="vg:8">ç<pgtc id="3" weight="0" schema="R"><rhyme label="b" id="3" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg></rhyme></pgtc></w>,</l>
				<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="6.2">r<seg phoneme="o" type="vs" value="1" rule="414" place="2">ô</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ss<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="6.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.4">br<seg phoneme="y" type="vs" value="1" rule="444" place="6">û</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<pgtc id="3" weight="0" schema="R"><rhyme label="b" id="3" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></rhyme></pgtc></w></l>
				<l n="7" num="1.7" lm="3" met="3"><w n="7.1" punct="pt:3">C<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>st<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="466" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></rhyme></pgtc></w>.</l>
			</lg>
		</div>
		<div type="section" n="2">
			<lg n="1" type="septain" rhyme="ababbba">
			<head type="speaker">TÉLÉMAQUE.</head>
				<p>
					Ça demande une réponse. Permettez.<lb></lb>
				</p>
			<head type="tune">Même air.</head>
				<l n="8" num="1.1" lm="3" met="3"><w n="8.1">C<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>st<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="466" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></pgtc></w></l>
				<l n="9" num="1.2" lm="8" met="8"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w> <w n="9.2" punct="vg:2">j<seg phoneme="œ̃" type="vs" value="1" rule="394" place="2" punct="in vg">eun</seg></w>', <w n="9.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="3">en</seg></w> <w n="9.4" punct="vg:4">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="vg">ai</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="9.6" punct="ps:8">c<seg phoneme="e" type="vs" value="1" rule="271" place="6">æ</seg>t<pgtc id="5" weight="6" schema="VCR"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="ps">a</seg></rhyme></pgtc></w>…</l>
				<l n="10" num="1.3" lm="8" met="8"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">dʼs<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>r</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">br<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="10.6" punct="vg:8">d<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>m<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="466" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				<l n="11" num="1.4" lm="8" met="8"><w n="11.1">L</w>'<w n="11.2"><seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>ll</w>'<w n="11.4" punct="pv:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>gn<pgtc id="5" weight="6" schema="VCR"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pv">a</seg></rhyme></pgtc></w> ;</l>
				<l n="12" num="1.5" lm="8" met="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="12.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3" punct="vg:4">j<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg">ou</seg>r</w>, <w n="12.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="12.5">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w> <w n="12.6" punct="vg:8">ç<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg></rhyme></pgtc></w>,</l>
				<l n="13" num="1.6" lm="8" met="8"><w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="13.2">b<seg phoneme="o" type="vs" value="1" rule="314" place="2">eau</seg></w> <w n="13.3">m<seg phoneme="œ" type="vs" value="1" rule="150" place="3">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="4">eu</seg>r</w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ttr<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></rhyme></pgtc></w></l>
				<l n="14" num="1.7" lm="3" met="3"><w n="14.1" punct="pt:3">C<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>st<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="466" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></rhyme></pgtc></w>.</l>
			</lg>
		</div>
</div></body></text></TEI>