<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB24" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(ababcdcd)" er_moy="2.0" er_max="6" er_min="0" er_mode="0(2/4)" er_moy_et="2.45">
	<head type="tune">AIR du Piége.</head>
	<lg n="1" type="huitain" rhyme="ababcdcd">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">D</w>'<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rd</w> <w n="1.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>t</w> <w n="1.4">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-3" place="4">e</seg>r</w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.6">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="1.7" punct="vg:8">spl<pgtc id="1" weight="6" schema="VCR"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>d<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="2.2">cr<seg phoneme="wa" type="vs" value="1" rule="439" place="2">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg><seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>r</w> <w n="2.4">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5" punct="vg:8"><pgtc id="2" weight="0" schema="[R" part="1"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">J</w>'<w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="3.3" punct="vg:2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2" punct="vg">ai</seg></w>, <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="3.5">h<seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>t</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.7">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="3.8" punct="vg:8">gr<pgtc id="1" weight="6" schema="VCR" part="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Pr<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>p<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>s<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg></w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.3">dʼv<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r</w> <w n="4.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="4.5" punct="pt:8">f<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="192" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="5.2" punct="vg:2">pʼt<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg>t</w>, <w n="5.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="4">en</seg>s</w> <w n="5.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="5.6">rʼpr<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg><pgtc id="3" weight="2" schema="CR" part="1">t<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></rhyme></pgtc></w></l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1">Cʼt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="1">e</seg></w> <w n="6.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg></w> <w n="6.3">qu</w>'<w n="6.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w> <w n="6.6">rʼf<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>s<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg></w> <w n="6.7">d</w>'<w n="6.8" punct="pt:8">pr<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="d" id="4" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
		<stage>(se mettant d ses genoux.)</stage>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="7.2">n</w>'<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="7.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="7.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="7.6" punct="vg:8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg><pgtc id="3" weight="2" schema="CR" part="1">t<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="vg">er</seg></rhyme></pgtc></w>,</l>
		<l n="8" num="1.8" lm="8" met="8"><w n="8.1">Pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="8.3">p<seg phoneme="ɛ" type="vs" value="1" rule="384" place="4">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.5" punct="pt:8">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>sc<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="d" id="4" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
	</lg> 
</div></body></text></TEI>