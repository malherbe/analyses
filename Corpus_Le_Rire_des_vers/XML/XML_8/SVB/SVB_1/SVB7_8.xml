<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB7" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="2[abab]" er_moy="0.5" er_max="2" er_min="0" er_mode="0(3/4)" er_moy_et="0.87">
	<head type="tune">Air du Premier prix.</head>
	<lg n="1" type="regexp" rhyme="ab">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="1.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="1.3" punct="vg:4">v<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="189" place="4" punct="vg">e</seg>t</w>, <w n="1.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="1.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="1.6" punct="vg:8">v<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>t<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="2.3">d<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="2.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>tr</w>'<w n="2.5"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.6" punct="pt:8"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="2" weight="2" schema="CR">c<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pt">i</seg></rhyme></pgtc></w>.</l>
	</lg>
	<lg n="2" type="regexp" rhyme="a">
	<head type="speaker">Mme GERVAIS,</head>
		<l n="3" num="2.1" lm="8" met="8"><w n="3.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="3.2" punct="vg:3">r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>st<seg phoneme="e" type="vs" value="1" rule="346" place="3" punct="vg">ez</seg></w>, <w n="3.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="3.6" punct="pe:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>j<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
	</lg>
	<lg n="3" type="regexp" rhyme="">
	<head type="speaker">DESŒILLETS</head>
		<l part="I" n="4" num="3.1" lm="8" met="8"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">su<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="4.4" punct="pe:4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="pe">ou</seg>s</w> ! </l>
	</lg>
  <stage>Il sont.</stage>
	<lg n="4" type="regexp" rhyme="b">
	<head type="speaker">HENRI, bas, d Mimi.</head>
		<l part="F" n="4" lm="8" met="8"><w n="4.5">S<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="4.6" punct="pe:8"><seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg><pgtc id="2" weight="2" schema="CR">ss<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pe">i</seg></rhyme></pgtc></w> !</l>
	</lg>
	<lg n="5" type="regexp" rhyme="abab">
	<head type="speaker">MIMI.</head>
		<l n="5" num="5.1" lm="8" met="8"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="5.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>c</w> <w n="5.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="5.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="5.6" punct="pe:8">c<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
		<l n="6" num="5.2" lm="8" met="8"><w n="6.1">J</w>'<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="6.3">cr<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="6.4">qu</w>'<w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.6" punct="vg:8">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="vg">ai</seg>t</rhyme></pgtc></w>,</l>
		<l n="7" num="5.3" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">qu</w>'<w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="7.4" punct="ps:3"><seg phoneme="o" type="vs" value="1" rule="317" place="3" punct="ps">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="7.6">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="7.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.8" punct="dp:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rd<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg dp">e</seg></rhyme></pgtc></w>, :</l>
		<l n="8" num="5.4" lm="8" met="8"><w n="8.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="1">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="8.2">d<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="8.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="8.4" punct="pt:8">b<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>qu<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="8" punct="pt">e</seg>t</rhyme></pgtc></w>.</l>
	</lg>
</div></body></text></TEI>