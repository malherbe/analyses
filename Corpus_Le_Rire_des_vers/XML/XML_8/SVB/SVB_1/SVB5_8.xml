<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB5" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="1[abba] 1[abab]" er_moy="0.5" er_max="2" er_min="0" er_mode="0(3/4)" er_moy_et="0.87">
	<head type="tune">Air des Gascons.</head>
	<lg n="1" type="regexp" rhyme="abba">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J</w>'<w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>ds</w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="1.4">f<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="1" weight="2" schema="CR">v<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="vg">er</seg></rhyme></pgtc></w>,</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="2.2" punct="pe:3">v<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="pe ps">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! … <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">En</seg></w> <w n="2.4">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.5" punct="vg:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">N</w>'<w n="3.2"><seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="3.4" punct="vg:5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" punct="vg">e</seg></w>, <w n="3.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="3.6"><seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="3.7" punct="vg:8">h<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="4.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="4.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="4.4">d<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="4.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="4.6" punct="pt:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg><pgtc id="1" weight="2" schema="CR">v<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pt">er</seg></rhyme></pgtc></w>.</l>
	</lg>
	<lg n="2" type="regexp" rhyme="ab">
	<head type="speaker">HENRI, d Dupuis.</head>
		<l n="5" num="2.1" lm="8" met="8"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">M<seg phoneme="i" type="vs" value="1" rule="466" place="2">i</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="5.3">r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.4">l</w>'<w n="5.5" punct="vg:8">h<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>nn<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
		<l n="6" num="2.2" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="6.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w>-<w n="6.3">t<seg phoneme="wa" type="vs" value="1" rule="422" place="5">oi</seg></w> <w n="6.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="6.5" punct="vg:8">sc<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
	</lg>
	<lg n="3" type="regexp" rhyme="ab">
	<head type="speaker">DUPUIS.</head>
		<l n="7" num="3.1" lm="8" met="8"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="7.2"><seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.4">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="383" place="7">ei</seg>gn<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>r</rhyme></pgtc></w></l>
		<l n="8" num="3.2" lm="8" met="8"><w n="8.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="8.3">mi<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="8.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="8.6" punct="pe:8">m<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>r<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
	</lg> 
</div></body></text></TEI>