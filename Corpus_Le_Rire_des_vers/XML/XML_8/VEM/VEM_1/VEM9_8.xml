<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">À PROPOS PATRIOTIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="VIL" sort="1">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="MAS" sort="2">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>273 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">VEM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>À propos patriotique</title>
						<author>VILLENEUVE ET MASSON</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=0FA6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>À propos patriotique</title>
								<author>VILLENEUVE ET MASSON</author>
								<repository>Bayerische Staatsbibliothek</repository>
								<idno type="URI">https://opacplus.bsb-muenchen.de/title/BV008031366</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_subpart">SCÈNE III.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="VEM9" modus="cp" lm_max="10" metProfile="6, 7, 8, 4+6" form="suite périodique" schema="3(abab)" er_moy="1.33" er_max="2" er_min="0" er_mode="2(4/6)" er_moy_et="0.94">
				<head type="tune">AIR : De la Muette,</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<head type="main">CHŒUR DE FEMMES.</head>
					<l n="1" num="1.1" lm="6" met="6"><space unit="char" quantity="8"></space><w n="1.1">Nʼ</w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="1.3">l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg><pgtc id="1" weight="2" schema="CR">b<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="7" met="7"><space unit="char" quantity="6"></space><w n="2.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="2.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="2.3">c<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>ri<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5" punct="vg:7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg><pgtc id="2" weight="2" schema="CR">g<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="346" place="7" punct="vg">er</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="6" met="6"><space unit="char" quantity="8"></space><w n="3.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="3.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="3.3">nʼ</w> <w n="3.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="3.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">om</seg><pgtc id="1" weight="2" schema="CR">b<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="4.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="C">ou</seg>s</w> <w n="4.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" caesura="1">on</seg>s</w><caesura></caesura> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="C">u</seg></w> <w n="4.5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="6">oin</seg>s</w> <w n="4.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="C">ou</seg>s</w> <w n="4.7" punct="pt:10">s<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg><pgtc id="2" weight="2" schema="CR">g<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pt">er</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<head type="main">THÉRÈSE.</head>
					<l n="5" num="2.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="5.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w>-<w n="5.2" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="422" place="4" punct="vg">oi</seg></w>, <w n="5.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="5.6" punct="vg:8">pr<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="6.1">N<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>trʼ</w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>r</w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="6.4">d</w>'<w n="6.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>r</w> <w n="6.6">s<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="7">oin</seg></w> <w n="6.7">d</w>'<w n="6.8" punct="pv:8"><pgtc id="4" weight="0" schema="[R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pv">eu</seg>x</rhyme></pgtc></w> ;</l>
					<l n="7" num="2.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>g</w> <w n="7.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="7.4">c<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>lʼ</w> <w n="7.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="7.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>tr<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w> <w n="8.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>rs</w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="8.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>g</w> <w n="8.5" punct="pe:8">pr<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pe">eu</seg>x</rhyme></pgtc></w> !</l>
				</lg>
				<p>Bertrand. ‒ C'est ça, conduisons-les là dedans, tout est préparé ... Venez, enfans. </p>
				<lg n="3" type="quatrain" rhyme="abab">
					<head type="main">REPRISE DU CHŒUR.</head>
					<l n="9" num="3.1" lm="6" met="6"><space unit="char" quantity="8"></space><w n="9.1">Nʼ</w> <w n="9.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="9.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg><pgtc id="5" weight="2" schema="CR">b<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>, <del type="repetition" hand="LG" reason="analysis">etc</del></l>
					<l n="10" num="3.2" lm="7" met="7"><space unit="char" quantity="6"></space><subst type="repetition" hand="LG" reason="analysis"><del> </del><add rend="hidden"><w n="10.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="10.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="10.3">c<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>ri<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.5" punct="vg:7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg><pgtc id="6" weight="2" schema="CR">g<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="346" place="7" punct="vg">er</seg></rhyme></pgtc></w>, </add></subst></l>
					<l n="11" num="3.3" lm="6" met="6"><space unit="char" quantity="8"></space><subst type="repetition" hand="LG" reason="analysis"><del> </del><add rend="hidden"><w n="11.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="11.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="11.3">nʼ</w> <w n="11.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="11.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">om</seg><pgtc id="5" weight="2" schema="CR">b<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></add></subst></l>
					<l n="12" num="3.4" lm="10" met="4+6"><subst type="repetition" hand="LG" reason="analysis"><del> </del><add rend="hidden"><w n="12.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="12.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="C">ou</seg>s</w> <w n="12.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" caesura="1">on</seg>s</w><caesura></caesura> <w n="12.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="C">u</seg></w> <w n="12.5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="6">oin</seg>s</w> <w n="12.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="C">ou</seg>s</w> <w n="12.7" punct="pt:10">s<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg><pgtc id="6" weight="2" schema="CR">g<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pt">er</seg></rhyme></pgtc></w>. </add></subst></l>
				</lg>
				<p>(Ils rentrent par la droite.)</p>
			</div></body></text></TEI>