<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">À PROPOS PATRIOTIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="VIL" sort="1">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="MAS" sort="2">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>273 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">VEM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>À propos patriotique</title>
						<author>VILLENEUVE ET MASSON</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=0FA6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>À propos patriotique</title>
								<author>VILLENEUVE ET MASSON</author>
								<repository>Bayerische Staatsbibliothek</repository>
								<idno type="URI">https://opacplus.bsb-muenchen.de/title/BV008031366</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_subpart">SCÈNE II.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="VEM7" modus="sp" lm_max="8" metProfile="6, 7, (8)" form="suite périodique avec alternance de type 2" schema="2{1(abab) 1(aabb)} 1(abab)" er_moy="0.0" er_max="0" er_min="0" er_mode="0(10/10)" er_moy_et="0.0">
				<head type="tune">AIR : Trou la la.</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<head type="main">Auguste (prenant le fusil et la giberne.)</head>
					<l n="1" num="1.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="1.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="1.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg>s</w>, <w n="1.3">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="1.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg">i</seg>s</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="7" met="7"><space unit="char" quantity="2"></space><w n="2.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>vʼnt</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="2.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">in</seg>s</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5" punct="pv:7">P<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<pgtc id="1" weight="0" schema="R"><rhyme label="b" id="1" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="7" punct="pv">i</seg>s</rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="3.2" punct="vg:3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg>ts</w>, <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="3.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>ts</rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="7" met="7"><space unit="char" quantity="2"></space><w n="4.1">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="4.2">v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="4.4" punct="pt:7">P<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="7" punct="pt">i</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="aabb">
					<l n="5" num="2.1" lm="7" met="7"><space unit="char" quantity="2"></space><w n="5.1">Hi<seg phoneme="ɛ" type="vs" value="1" rule="63" place="1">e</seg>r</w> <w n="5.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2">an</seg>s</w> <w n="5.3">r<seg phoneme="i" type="vs" value="1" rule="466" place="3">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.4">n<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg></w> <w n="5.5">r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="7" met="7"><space unit="char" quantity="2"></space><w n="6.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="6.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="6.3">cr<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="6.5" punct="pv:7">c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>n<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" punct="pv">on</seg></rhyme></pgtc></w> ;</l>
					<l n="7" num="2.3" lm="8"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="7.3">qu</w>'<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w>-<w n="7.5">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="5">e</seg></w> <w n="7.6">qu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="7.7">l</w>'<w n="7.8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="7.9" punct="pi:8">pr<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pi">i</seg>s</rhyme></pgtc></w> ?</l>
					<l n="8" num="2.4" lm="7" met="7"><space unit="char" quantity="2"></space><w n="8.1">C</w>'<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="8.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">in</seg></w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.6" punct="pt:7">P<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="7" punct="pt">i</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="9.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg>s</w>, <w n="9.3">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="9.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg">i</seg>s</rhyme></pgtc></w>, <del reason="analysis" type="repetition" hand="LG">etc.</del></l>
					<l n="10" num="3.2" lm="7" met="7"><space unit="char" quantity="2"></space><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="10.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>vʼnt</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="10.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">in</seg>s</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.5" punct="pv:7">P<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="7" punct="pv">i</seg>s</rhyme></pgtc></w> ; </add></subst></l>
					<l n="11" num="3.3" lm="6" met="6"><space unit="char" quantity="4"></space><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="11.2" punct="vg:3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg>ts</w>, <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="11.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>ts</rhyme></pgtc></w></add></subst></l>
					<l n="12" num="3.4" lm="7" met="7"><space unit="char" quantity="2"></space><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="12.1">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="12.2">v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="12.4" punct="pt:7">P<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="7" punct="pt">i</seg>s</rhyme></pgtc></w>. </add></subst></l>
				</lg>
				<lg n="4" type="quatrain" rhyme="aabb">
					<l n="13" num="4.1" lm="7" met="7"><space unit="char" quantity="2"></space><w n="13.1">J</w>'<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="13.3" punct="vg:2">v<seg phoneme="y" type="vs" value="1" rule="449" place="2" punct="vg">u</seg></w>, <w n="13.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="13.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>lʼ</w> <w n="13.6">d</w>'<w n="13.7" punct="vg:7">h<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>nn<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="7" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
					<l n="14" num="4.2" lm="7" met="7"><space unit="char" quantity="2"></space><w n="14.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">Em</seg>p<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>gn<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="14.3" punct="vg:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">em</seg>p<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>gn<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="7" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
					<l n="15" num="4.3" lm="7" met="7"><space unit="char" quantity="2"></space><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="15.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="15.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="15.4">l</w>'<w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">em</seg>p<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>gn<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="7" met="7"><space unit="char" quantity="2"></space><w n="16.1">N</w>'<w n="16.2"><seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="16.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="16.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="16.5">h<seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>t</w> <w n="16.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.7" punct="pt:7">ç<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="pt">a</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="17.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg>s</w>, <w n="17.3">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="17.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg">i</seg>s</rhyme></pgtc></w>, <del reason="analysis" type="repetition" hand="LG">etc.</del></l>
					<l n="18" num="5.2" lm="7" met="7"><space unit="char" quantity="2"></space><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="18.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>vʼnt</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="18.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">in</seg>s</w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.5" punct="pv:7">P<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<pgtc id="9" weight="0" schema="R"><rhyme label="b" id="9" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="7" punct="pv">i</seg>s</rhyme></pgtc></w> ; </add></subst></l>
					<l n="19" num="5.3" lm="6" met="6"><space unit="char" quantity="4"></space><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="19.2" punct="vg:3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg>ts</w>, <w n="19.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="19.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>ts</rhyme></pgtc></w></add></subst></l>
					<l n="20" num="5.4" lm="7" met="7"><space unit="char" quantity="2"></space><subst reason="analysis" type="repetition" hand="LG"><del> </del><add rend="hidden"><w n="20.1">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="20.2">v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="20.4" punct="pt:7">P<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="7" punct="pt">i</seg>s</rhyme></pgtc></w>. </add></subst></l>
				</lg>
			</div></body></text></TEI>