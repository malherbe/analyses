<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BANDEAU</title>
				<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="BLL" sort="1">
					<name>
						<forename>Jean-Nicolas</forename>
						<surname>BOUILLY</surname>
					</name>
					<date from="1763" to="1842">1763-1842</date>
				</author>
				<author key="VAN" sort="2">
					<name>
						<forename>Émile</forename>
						<surname>VANDERBURCH</surname>
						<addname type="other">Émile VANDERBURCH</addname>
						<addname type="other">Émile VANDER-BURCH</addname>
						<addname type="other">Émile VANDERBURCK</addname>
						<addname type="other">Émile VAN DER BURCK</addname>
					</name>
					<date from="1794" to="1862">1794-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>168 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE BANDEAU</title>
						<author>BOUILLY ET EM. VANDERBURCH</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GALLICA</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9782259n.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La Bibliothèque nationale de France</repository>
								<idno type="URL">https ://catalogue.bnf.fr/ark :/12148/cb31531335q</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">21 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BLV3" modus="cp" lm_max="10" metProfile="8, 4+6" form="strophe unique" schema="1(ababcddc)" er_moy="1.5" er_max="2" er_min="0" er_mode="2(3/4)" er_moy_et="0.87">
	<head type="tune">AIR de Julie.</head>
	<lg n="1" type="huitain" rhyme="ababcddc">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.5">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L</w>'<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>x</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="2.4">p<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg></w> <w n="2.5" punct="pv:8">s<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>s<pgtc id="2" weight="2" schema="CR">f<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pv">ai</seg>t</rhyme></pgtc></w> ;</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="3.2">f<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>t<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="3.4">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>rt</w> <w n="3.5">g<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="7">en</seg>t<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="4.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="4.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="7">en</seg></w> <w n="4.6" punct="pt:8"><pgtc id="2" weight="2" schema="CR">f<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pt">ai</seg>t</rhyme></pgtc></w>.</l>
		<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">L</w>'<w n="5.2" punct="vg:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="vg">a</seg>rt</w>, <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="P">a</seg>r</w> <w n="5.4" punct="vg:4">b<seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="4" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="5.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">em</seg>pl<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>ç<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="5.7" punct="pv:10">n<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
		<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">f<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s</w> <w n="6.3">d<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>scr<seg phoneme="ɛ" type="vs" value="1" rule="189" place="4" caesura="1">e</seg>t</w><caesura></caesura> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="6.5">s<seg phoneme="y" type="vs" value="1" rule="449" place="6" mp="M">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>t</w> <w n="6.6">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="8">o</seg>rt</w> <w n="6.7" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg><pgtc id="4" weight="2" schema="CR">dr<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="10" punct="pv">oi</seg>t</rhyme></pgtc></w> ;</l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">b<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>ti<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.5">l</w>'<w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg><pgtc id="4" weight="2" schema="CR">dr<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>t</rhyme></pgtc></w></l>
		<l n="8" num="1.8" lm="8" met="8"><w n="8.1">F<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>t</w> <w n="8.2">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="8.5" punct="pt:8">f<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>rn<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
	</lg>
</div></body></text></TEI>