<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BANDEAU</title>
				<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="BLL" sort="1">
					<name>
						<forename>Jean-Nicolas</forename>
						<surname>BOUILLY</surname>
					</name>
					<date from="1763" to="1842">1763-1842</date>
				</author>
				<author key="VAN" sort="2">
					<name>
						<forename>Émile</forename>
						<surname>VANDERBURCH</surname>
						<addname type="other">Émile VANDERBURCH</addname>
						<addname type="other">Émile VANDER-BURCH</addname>
						<addname type="other">Émile VANDERBURCK</addname>
						<addname type="other">Émile VAN DER BURCK</addname>
					</name>
					<date from="1794" to="1862">1794-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>168 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE BANDEAU</title>
						<author>BOUILLY ET EM. VANDERBURCH</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GALLICA</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9782259n.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La Bibliothèque nationale de France</repository>
								<idno type="URL">https ://catalogue.bnf.fr/ark :/12148/cb31531335q</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">21 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BLV14" modus="cp" lm_max="12" metProfile="4+6, (6+6), (8)" form="suite de strophes" schema="2[abab]" er_moy="2.0" er_max="6" er_min="0" er_mode="0(2/4)" er_moy_et="2.45">
	<head type="tune">AIR de Henri IV en famille.</head>
	<lg n="1" type="regexp" rhyme="abab">
		<l n="1" num="1.1" lm="8"><w n="1.1">L</w>'<w n="1.2">h<seg phoneme="i" type="vs" value="1" rule="496" place="1">y</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="220" place="2">en</seg></w> <w n="1.3">n</w>'<w n="1.4"><seg phoneme="y" type="vs" value="1" rule="390" place="3">eu</seg>t</w> <w n="1.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r</w> <w n="1.6">m<seg phoneme="wa" type="vs" value="1" rule="422" place="5">oi</seg></w> <w n="1.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.8">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="1.9" punct="vg:8"><pgtc id="1" weight="2" schema="[CR">f<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="63" place="8" punct="vg">e</seg>rs</rhyme></pgtc></w>,</l>
		<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">L</w>'<w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r</w> <w n="2.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="2.4">v<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="2.5">v<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="466" place="6">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.6" punct="pt:10"><seg phoneme="o" type="vs" value="1" rule="443" place="7" mp="M">o</seg>b<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg><pgtc id="2" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>ss<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
		<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="3.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="P">ou</seg>r</w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="3.4">m<seg phoneme="o" type="vs" value="1" rule="317" place="4" caesura="1">au</seg>x</w><caesura></caesura> <w n="3.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="C">ou</seg>s</w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>s</w> <w n="3.8">s<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg><pgtc id="1" weight="2" schema="CR">ff<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>rts</rhyme></pgtc></w></l>
		<l n="4" num="1.4" lm="12" met="6+6" met_alone="True"><w n="4.1">N<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="4.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="4.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="4.7">pl<seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>s</w> <w n="4.8" punct="pt:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="M">om</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>t<pgtc id="2" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>ss<rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
	</lg>
	<lg n="2" type="regexp" rhyme="">
	<head type="speaker">SAINT-VALERY.</head>
		<l part="I" n="5" num="2.1" lm="10" met="4+6"><w n="5.1" punct="pe:1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1" punct="pe">ou</seg>s</w> ! <w n="5.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">p<seg phoneme="ø" type="vs" value="1" rule="397" place="3" mp="Lp">eu</seg>t</w>-<w n="5.4" punct="pi:4"><seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="pi" caesura="1">i</seg>l</w> ?<caesura></caesura> </l> 
	</lg>
	<lg n="3" type="regexp" rhyme="ab">
	<head type="speaker">AMÉLIE.</head>
		<l part="F" n="5" lm="10" met="4+6"><w n="5.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="C">On</seg></w> <w n="5.6">s<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="5.7">pl<seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>s</w> <w n="5.8">d</w>'<w n="5.9"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="9">un</seg></w> <w n="5.10" punct="pt:10">j<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pt">ou</seg>r</rhyme></pgtc></w>.</l>
		<l n="6" num="3.1" lm="10" met="4+6"><w n="6.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>s</w> ! <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="6.3">c<seg phoneme="œ" type="vs" value="1" rule="248" place="4" caesura="1">œu</seg>r</w><caesura></caesura> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="6.5">tr<seg phoneme="o" type="vs" value="1" rule="432" place="6">o</seg>p</w> <w n="6.6">b<seg phoneme="ɔ" type="vs" value="1" rule="418" place="7">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="6.7" punct="pt:10">m<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>m<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
	</lg>
	<lg n="4" type="regexp" rhyme="ab">
	<head type="speaker">SAINT-VALERY, s'oubliant.</head>
		<l n="7" num="4.1" lm="10" met="4+6"><w n="7.1">Tr<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>h<seg phoneme="i" type="vs" value="1" rule="481" place="2">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="3" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="P">a</seg>r</w> <w n="7.4">l</w>'<w n="7.5">h<seg phoneme="i" type="vs" value="1" rule="496" place="6" mp="M">y</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="220" place="7">en</seg></w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="188" place="8">e</seg>t</w> <w n="7.7">l</w>'<w n="7.8" punct="pe:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>m<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pe">ou</seg>r</rhyme></pgtc></w> !</l>
		<l n="8" num="4.2" lm="10" met="4+6"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="8.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="8.3">v<seg phoneme="wa" type="vs" value="1" rule="439" place="3" mp="M">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="8.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="C">on</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="8.6">p<seg phoneme="ɛ" type="vs" value="1" rule="384" place="7">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="P">à</seg></w> <w n="8.8">v<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>s</w> <w n="8.9" punct="pt:10">cr<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
	</lg>
</div></body></text></TEI>