<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BANDEAU</title>
				<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="BLL" sort="1">
					<name>
						<forename>Jean-Nicolas</forename>
						<surname>BOUILLY</surname>
					</name>
					<date from="1763" to="1842">1763-1842</date>
				</author>
				<author key="VAN" sort="2">
					<name>
						<forename>Émile</forename>
						<surname>VANDERBURCH</surname>
						<addname type="other">Émile VANDERBURCH</addname>
						<addname type="other">Émile VANDER-BURCH</addname>
						<addname type="other">Émile VANDERBURCK</addname>
						<addname type="other">Émile VAN DER BURCK</addname>
					</name>
					<date from="1794" to="1862">1794-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>168 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE BANDEAU</title>
						<author>BOUILLY ET EM. VANDERBURCH</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GALLICA</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9782259n.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La Bibliothèque nationale de France</repository>
								<idno type="URL">https ://catalogue.bnf.fr/ark :/12148/cb31531335q</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">21 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BLV8" modus="cp" lm_max="10" metProfile="6, 8, (4+6)" form="suite périodique" schema="1(ababcdcd) 3(abab)" er_moy="2.0" er_max="6" er_min="0" er_mode="2(7/10)" er_moy_et="1.55">
	<head type="tune">AIR : O troupe fantastique.</head>
		<div type="section" n="1">
			<lg n="1" type="huitain" rhyme="ababcdcd">
				<l n="1" num="1.1" lm="6" met="6"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="1.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.5">l</w>'<w n="1.6"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg><pgtc id="1" weight="2" schema="CR">v<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
				<l n="2" num="1.2" lm="6" met="6"><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="2.2">j</w>'<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>rs</w> <w n="2.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r</w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="2.6" punct="pv:6"><pgtc id="2" weight="2" schema="[CR">v<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="pv">oi</seg>r</rhyme></pgtc></w> ;</l>
				<l n="3" num="1.3" lm="6" met="6"><w n="3.1">N</w>'<w n="3.2" punct="vg:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="1">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s</w> <w n="3.4" punct="vg:6"><pgtc id="1" weight="2" schema="[CR">v<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				<l n="4" num="1.4" lm="6" met="6"><w n="4.1">Pu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">c</w>'<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="4.5" punct="pt:6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg><pgtc id="2" weight="2" schema="CR">v<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
				<l n="5" num="1.5" lm="8" met="8"><w n="5.1" punct="vg:3">N<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="5.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w>-<w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="5.4" punct="vg:8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>s<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				<l n="6" num="1.6" lm="8" met="8"><w n="6.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">pr<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="6.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="6.5" punct="pv:8">t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r<pgtc id="4" weight="2" schema="CR">m<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="pv">en</seg>t</rhyme></pgtc></w> ;</l>
				<l n="7" num="1.7" lm="8" met="8"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>str<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>t</w> <w n="7.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">in</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="354" place="7">e</seg>x<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
				<l n="8" num="1.8" lm="10" met="4+6" met_alone="True"><w n="8.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="365" place="1">e</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2" mp="M/mc">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="Lc">ou</seg>rd</w>'<w n="8.3">hu<seg phoneme="i" type="vs" value="1" rule="490" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="8.5">pl<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="8.7">l</w>'<w n="8.8" punct="pe:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" punct="pe">an</seg>t</rhyme></pgtc></w> !</l>
			</lg>
		</div>
		<div type="section" n="2">
			<lg n="1" type="quatrain" rhyme="abab">
			<head type="speaker">ENSEMBLE.</head>
				<l n="9" num="1.1" lm="6" met="6"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="9.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="9.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.5">l</w>'<w n="9.6"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg><pgtc id="5" weight="2" schema="CR">v<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
				<l n="10" num="1.2" lm="6" met="6"><w n="10.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="10.2">j</w>'<w n="10.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>rs</w> <w n="10.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r</w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="10.6" punct="pv:6"><pgtc id="6" weight="2" schema="[CR">v<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="pv">oi</seg>r</rhyme></pgtc></w> ;</l>
				<l n="11" num="1.3" lm="6" met="6"><w n="11.1">N</w>'<w n="11.2" punct="vg:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="1">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s</w> <w n="11.4" punct="vg:6"><pgtc id="5" weight="2" schema="[CR">v<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				<l n="12" num="1.4" lm="6" met="6"><w n="12.1">Pu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">c</w>'<w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="12.5" punct="pt:6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg><pgtc id="6" weight="2" schema="CR">v<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
			</lg>
			<lg n="2" type="quatrain" rhyme="abab">
			<head type="speaker">LAPLACE.</head>
				<l n="13" num="2.1" lm="6" met="6"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="13.2">fu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>r</w> <w n="13.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="13.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">in</seg><pgtc id="7" weight="2" schema="CR">v<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
				<l n="14" num="2.2" lm="6" met="6"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="14.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="14.5" punct="pv:6"><pgtc id="8" weight="2" schema="[CR">v<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="pv">oi</seg>r</rhyme></pgtc></w> ;</l>
				<l n="15" num="2.3" lm="6" met="6"><w n="15.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="15.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="15.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg></w> <w n="15.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>s</w> <w n="15.5"><pgtc id="7" weight="2" schema="[CR">v<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
				<l n="16" num="2.4" lm="6" met="6"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="16.2">c<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="16.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg></w> <w n="16.4" punct="pt:6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg><pgtc id="8" weight="2" schema="CR">v<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
			</lg>
			<lg n="3" type="quatrain" rhyme="abab">
			<head type="speaker">JOSEPH.</head>
				<l n="17" num="3.1" lm="6" met="6"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="17.2">fu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>r</w> <w n="17.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="17.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="17.5" punct="pv:6"><pgtc id="9" weight="6" schema="[VCR"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">in</seg>v<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
				<l n="18" num="3.2" lm="6" met="6"><w n="18.1">L<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="18.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="18.4">l</w>'<w n="18.5" punct="dp:6"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>sp<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="dp">oi</seg>r</rhyme></pgtc></w> :</l>
				<l n="19" num="3.3" lm="6" met="6"><w n="19.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="19.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="3">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="19.3">bi<pgtc id="9" weight="6" schema="V[CR" part="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="5">en</seg></pgtc></w> <w n="19.4"><pgtc id="9" weight="6" schema="V[CR" part="2">v<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
				<l n="20" num="3.4" lm="6" met="6"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.3">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>d<seg phoneme="o" type="vs" value="1" rule="314" place="5">eau</seg></w> <w n="20.4" punct="pt:6">n<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
			</lg> 
		</div>
</div></body></text></TEI>