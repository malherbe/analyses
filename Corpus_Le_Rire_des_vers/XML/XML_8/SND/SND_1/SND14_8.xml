<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE LIEUTENANT D'ARTILLERIE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="NSL" sort="2">
					<name>
						<forename>Jean-Pierre-Laurent</forename>
						<surname>DENOMBRET</surname>
						<addName type="pen_name">Charles NOMBRET SAINT-LAURENT</addName>
					</name>
					<date from="1791" to="1833">1791-1833</date>
				</author>
					<author key="DUV" sort="3">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>225 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SND_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=NWU:35556007830409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
								<repository>Northwestern university</repository>
								<idno type="URI">https://babel.hathitrust.org/cgi/pt?id=ien.35556007830409</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE II.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SND14" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(ababbcddc)" er_moy="9.2" er_max="44" er_min="0" er_mode="0(3/5)" er_moy_et="17.42">
				<head type="tune">Air de l'Ecu de six francs.</head>
				<lg n="1" type="neuvain" rhyme="ababbcddc">
					<head type="main">Michel, à part.</head>
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="2">ô</seg>t</w> <w n="1.2">c</w>'<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="1.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="1.5">y<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="1.6">qu</w>'<w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>llʼ</w> <w n="1.8" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="2">ô</seg>t</w> <w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3">s</w>'<w n="2.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ppu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>ʼ</w> <w n="2.5">s<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>r</w> <w n="2.6" punct="pv:8">m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pv">oi</seg></rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="3.3">d<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="3.4">ch<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="3.5"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>l</w> <w n="3.6">r<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Qu</w>' <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.3">nʼ</w> <w n="4.4">s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="4.5">pl<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>s</w> <w n="4.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4">e</seg>l</w> <w n="4.7"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="4.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="4.9" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">em</seg>pl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pt">oi</seg></rhyme></pgtc></w>.<del type="repetition" reason="analysis" hand="LG">(bis.)</del></l>
					<l n="5" num="1.5" lm="8" met="8"><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="5.1"><pgtc id="2" weight="44" schema="[CV[[CV[CV[CVC[V[CVc[VCR" part="1">Qu</pgtc></w>' <w n="5.2"><pgtc id="2" weight="44" schema="[CV[[CV[CV[CVC[V[CVc[VCR" part="2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></pgtc></w> <w n="5.3"><pgtc id="2" weight="44" schema="[CV[[CV[CV[CVC[V[CVc[VCR" part="3">nʼ</pgtc></w> <w n="5.4"><pgtc id="2" weight="44" schema="[CV[[CV[CV[CVC[V[CVc[VCR" part="4">s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</pgtc></w> <w n="5.5"><pgtc id="2" weight="44" schema="[CV[[CV[CV[CVC[V[CVc[VCR" part="5">pl<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>s</pgtc></w> <w n="5.6"><pgtc id="2" weight="44" schema="[CV[[CV[CV[CVC[V[CVc[VCR" part="6">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4">e</seg>l</pgtc></w> <w n="5.7"><pgtc id="2" weight="44" schema="[CV[[CV[CV[CVC[V[CVc[VCR" part="7"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</pgtc></w> <w n="5.8"><pgtc id="2" weight="44" schema="[CV[[CV[CV[CVC[V[CVc[VCR" part="8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></pgtc></w> <w n="5.9" punct="pt:8"><pgtc id="2" weight="44" schema="[CV[[CV[CV[CVC[V[CVc[VCR" part="9"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">em</seg>pl<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pt">oi</seg></rhyme></pgtc></w>. </add></subst></l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">C<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>rtʼ</w> <w n="6.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ts</w> <w n="6.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>t</w> <w n="6.6" punct="pv:8">h<seg phoneme="o" type="vs" value="1" rule="434" place="7">o</seg><pgtc id="3" weight="2" schema="CR" part="1">nn<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></pgtc></w> ;</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="7.2">jʼ</w> <w n="7.3">v<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="7.4" punct="vg:4">s<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="4" punct="vg">oi</seg>r</w>, <w n="7.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="7.7" punct="vg:8">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>s<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="vg">on</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="8.2">jʼ</w> <w n="8.3">su<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="8.4">r</w>’<w n="8.5">g<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></w> <w n="8.6">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="8.8" punct="vg:8">b<seg phoneme="a" type="vs" value="1" rule="339" place="7">â</seg>t<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="vg">on</seg></rhyme></pgtc></w>,</l>
					<l n="9" num="1.9" lm="8" met="8"><w n="9.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg></w> <w n="9.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="2">en</seg></w> <w n="9.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="y" type="vs" value="1" rule="462" place="4">u</seg>nʼ</w> <w n="9.5">p<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>rʼ</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.7" punct="pt:8">l<seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg><pgtc id="3" weight="2" schema="CR" part="1">n<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>