<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE LIEUTENANT D'ARTILLERIE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="NSL" sort="2">
					<name>
						<forename>Jean-Pierre-Laurent</forename>
						<surname>DENOMBRET</surname>
						<addName type="pen_name">Charles NOMBRET SAINT-LAURENT</addName>
					</name>
					<date from="1791" to="1833">1791-1833</date>
				</author>
					<author key="DUV" sort="3">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>225 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SND_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=NWU:35556007830409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
								<repository>Northwestern university</repository>
								<idno type="URI">https://babel.hathitrust.org/cgi/pt?id=ien.35556007830409</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE III.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SND4" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="1[ababb] 1[abab]" er_moy="7.6" er_max="38" er_min="0" er_mode="0(4/5)" er_moy_et="15.2">
				<head type="tune">Air du Vaudeville de Partie et Revanche.</head>
				<lg n="1" type="regexp" rhyme="aba">
					<head type="main">MAD. DU COLOMBIER.</head>
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="1.2">n</w>'<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="1.4" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg>s</w>, <w n="1.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="1.7" punct="vg:8">pr<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="2.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg>s</w> <w n="2.3">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="2.4">d</w>'<w n="2.5" punct="pv:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>ch<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pv">a</seg>ts</rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="3.2" punct="vg:3">t<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="3" punct="vg">en</seg>t</w>, <w n="3.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4">l</w>'<w n="3.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="3.7" punct="pt:8">m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="regexp" rhyme="b">
					<head type="main">LORIS.</head>
					<l n="4" num="2.1" lm="8" met="8"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="4.2">s<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="4.3">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.5">l</w>'<w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="4.7" punct="pt:8">p<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="regexp" rhyme="b">
					<head type="main">COURVOLLE.</head>
					<l n="5" num="3.1" lm="8" met="8"><w n="5.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="vg">on</seg></w>, <w n="5.2"><pgtc id="2" weight="38" schema="[CV[CVCVCV[CV[C[V[CR" part="1">s<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></pgtc></w> <w n="5.3"><pgtc id="2" weight="38" schema="[CV[CVCVCV[CV[C[V[CR" part="2">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></pgtc></w> <w n="5.4"><pgtc id="2" weight="38" schema="[CV[CVCVCV[CV[C[V[CR" part="3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></pgtc></w> <w n="5.5"><pgtc id="2" weight="38" schema="[CV[CVCVCV[CV[C[V[CR" part="4">l</pgtc></w>'<w n="5.6"><pgtc id="2" weight="38" schema="[CV[CVCVCV[CV[C[V[CR" part="5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</pgtc></w> <w n="5.7" punct="pt:8"><pgtc id="2" weight="38" schema="[CV[CVCVCV[CV[C[V[CR" part="6">p<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="regexp" rhyme="abab">
					<head type="main">MAD. DU COLOMBIER.</head>
					<l n="6" num="4.1" lm="8" met="8"><w n="6.1">C</w>'<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="6.3"><seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.4" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg>j<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="6.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.6" punct="pv:8">p<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="7" num="4.2" lm="8" met="8"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>s</w> <w n="7.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="4">en</seg>t</w> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r</w> <w n="7.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="7.5">br<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="o" type="vs" value="1" rule="437" place="8">o</seg>s</rhyme></pgtc></w></l>
					<l n="8" num="4.3" lm="8" met="8"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="8.2">pl<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s</w> <w n="8.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>ds</w> <w n="8.4">pr<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ss<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>rs</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.6" punct="vg:8">d<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="9" num="4.4" lm="8" met="8"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="9.3">n</w>'<w n="9.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="9.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="9.7">pl<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>s</w> <w n="9.8" punct="pt:8">gr<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="o" type="vs" value="1" rule="437" place="8" punct="pt">o</seg>s</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>