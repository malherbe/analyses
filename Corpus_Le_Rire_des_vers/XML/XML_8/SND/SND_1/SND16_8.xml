<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE LIEUTENANT D'ARTILLERIE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="NSL" sort="2">
					<name>
						<forename>Jean-Pierre-Laurent</forename>
						<surname>DENOMBRET</surname>
						<addName type="pen_name">Charles NOMBRET SAINT-LAURENT</addName>
					</name>
					<date from="1791" to="1833">1791-1833</date>
				</author>
					<author key="DUV" sort="3">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>225 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SND_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=NWU:35556007830409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
								<repository>Northwestern university</repository>
								<idno type="URI">https://babel.hathitrust.org/cgi/pt?id=ien.35556007830409</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE III.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SND16" modus="cm" lm_max="10" metProfile="4+6" form="strophe unique" schema="1(ababcdcd)" er_moy="0.5" er_max="2" er_min="0" er_mode="0(3/4)" er_moy_et="0.87">
				<head type="tune">AIR : Le choix que fait tout le village.</head>
				<lg n="1" type="huitain" rhyme="ababcdcd">
					<head type="main">DELAUNAY, au désespoir.</head>
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="1.2">l</w>'<w n="1.3"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.4" punct="vg:4">j<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="C">au</seg></w> <w n="1.6">m<seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="M">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg></w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="1.8">n<seg phoneme="o" type="vs" value="1" rule="437" place="9" mp="C">o</seg>s</w> <w n="1.9" punct="vg:10">fr<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">J</w>'<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="2.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="C">a</seg></w> <w n="2.5">v<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>x</w><caesura></caesura> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>pp<seg phoneme="ɥi" type="vs" value="1" rule="461" place="6" mp="M">u</seg>y<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg></w> <w n="2.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="2.8" punct="pv:10">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg><pgtc id="2" weight="2" schema="CR">ss<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="10" punct="pv">ein</seg></rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="3.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="3.3" punct="vg:4">n<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="3.4">r<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="6" mp="M">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="8">ain</seg>s</w> <w n="3.5" punct="vg:10">s<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="4.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2" mp="M">e</seg>rch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="4.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg></w> <w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7" mp="C">un</seg></w> <w n="4.6" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg><pgtc id="2" weight="2" schema="CR">ss<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" punct="pt">in</seg></rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="M">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>r</w><caesura></caesura> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="5.4">m<seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="M">i</seg>ss<seg phoneme="i" type="vs" value="1" rule="d-1" place="7" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg></w> <w n="5.5" punct="vg:10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="M">an</seg>gl<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="6.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2" mp="M">in</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="442" place="3" mp="M">o</seg>qu<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg></w><caesura></caesura> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="6.4">d<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="8" mp="C">u</seg></w> <w n="6.6" punct="pv:10">h<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>s<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="pv">a</seg>rd</rhyme></pgtc></w> ;</l>
					<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">c</w>'<w n="7.3" punct="ps:2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2" punct="ps">e</seg>st</w> … <w n="7.4" punct="pe:4">fr<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="pe ps" caesura="1">i</seg>s</w> !<caesura></caesura> … <w n="7.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5" mp="P">an</seg>s</w> <w n="7.6">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="Fc">e</seg></w> <w n="7.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg></w> <w n="7.8" punct="vg:10">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">em</seg>bl<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" caesura="1">in</seg></w><caesura></caesura> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="8.5">pl<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>c<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg></w> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="8.7" punct="pt:10">p<seg phoneme="wa" type="vs" value="1" rule="419" place="9" mp="M">oi</seg>gn<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="pt">a</seg>rd</rhyme></pgtc></w>.</l>
					<l ana="unanalyzable" n="9" num="1.9">Oui, c'est, hélas ! dans, etc.</l>
				</lg>
			</div></body></text></TEI>