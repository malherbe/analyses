<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC46" modus="cp" lm_max="10" metProfile="8, 4+6" form="suite de strophes" schema="2[abab]" er_moy="0.75" er_max="2" er_min="0" er_mode="0(2/4)" er_moy_et="0.83">
	<head type="tune">AIR : Vaudeville de Psyché.</head>
	<lg n="1" type="regexp" rhyme="ab">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="1.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="1.3">p<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg></w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="2.2">l</w>'<w n="2.3"><seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>pr<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5">cr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w> <w n="2.6" punct="pt:8">r<pgtc id="2" weight="1" schema="GR">i<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="8" punct="pt">en</seg></rhyme></pgtc></w>.</l>
	</lg>
	<lg n="2" type="regexp" rhyme="a">
	<head type="speaker">AZOLIN, à part.</head>
		<l n="3" num="2.1" lm="10" met="4+6"><w n="3.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="3.2">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2" mp="C">e</seg>t</w> <w n="3.3" punct="ps:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>nn<seg phoneme="o" type="vs" value="1" rule="314" place="4" punct="ps" caesura="1">eau</seg></w>…<caesura></caesura> <w n="3.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="3.5">fr<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>s</w> <w n="3.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>d</w> <w n="3.7">j</w>'<w n="3.8"><seg phoneme="i" type="vs" value="1" rule="496" place="9">y</seg></w> <w n="3.9" punct="pt:10">p<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
	</lg>
	<lg n="3" type="regexp" rhyme="b">
	<head type="speaker">BRIND'AMOUR, à part, en riant.</head>
		<l n="4" num="3.1" lm="8" met="8"><w n="4.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="4.2">s</w>'<w n="4.3"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="4.4">s<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="4.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.6">c</w>'<w n="4.7"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="4.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.9" punct="pe:8">m<pgtc id="2" weight="1" schema="GR">i<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="8" punct="pe">en</seg></rhyme></pgtc></w> !</l>
	</lg>
	<lg n="4" type="regexp" rhyme="a">
	<head type="speaker">AZOLIN, à part.</head>
		<l n="5" num="4.1" lm="10" met="4+6"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2" punct="ps:2">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="2" punct="ps">ain</seg>s</w>… <del reason="analysis" type="irrelevant" hand="KL">(Haut.)</del> <w n="5.3">J</w>'<w n="5.4" punct="ps:4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4" punct="ps" caesura="1">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>…<caesura></caesura> <w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" mp="Fm">e</seg>s</w>-<w n="5.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="5.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="8">en</seg></w> <w n="5.8">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>rt<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="10">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
		<l part="I" n="6" num="4.2" lm="10" met="4+6"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="6.3">b<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="6.4" punct="ps:6"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="ps">a</seg></w>… </l>
	</lg>
	<lg n="5" type="regexp" rhyme="bab">
	<head type="speaker">BRIND'AMOUR, mettant la bague.</head>
		<l part="F" n="6" lm="10" met="4+6"><w n="6.5">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7" mp="P">an</seg>s</w> <w n="6.6" punct="vg:10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="M">on</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg><pgtc id="4" weight="2" schema="CR">d<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="vg">i</seg>t</rhyme></pgtc></w>,</l>
		<l n="7" num="5.1" lm="10" met="4+6"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">s<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="7.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4" caesura="1">en</seg></w><caesura></caesura> <w n="7.4">qu</w>'<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6" mp="M">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>t</w> <w n="7.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="9" mp="P">an</seg>s</w> <w n="7.8" punct="vg:10">p<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="384" place="10">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
		<l n="8" num="5.2" lm="8" met="8"><w n="8.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="8.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t</w> <w n="8.3">d<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>gt</w> <w n="8.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">l</w>'<w n="8.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t</w> <w n="8.7" punct="pt:8"><pgtc id="4" weight="2" schema="[CR">d<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pt">i</seg>t</rhyme></pgtc></w>.</l>
	</lg>   
</div></body></text></TEI>