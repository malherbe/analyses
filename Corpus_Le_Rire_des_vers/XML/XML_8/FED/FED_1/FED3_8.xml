<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANDRÉ LE CHANSONNIER</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="FTN" sort="1">
					<name>
						<forename>Louis Marie</forename>
						<surname>FONTAN</surname>
					</name>
					<date from="1801" to="1839">1801-1839</date>
				</author>
				<author key="DNY" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>DESNOYER</surname>
					</name>
					<date from="1806" to="1858">1806-1858</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>239 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>André le chansonnier.</title>
						<author>FONTAN ET DESNOYER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=aN6oyNpF3cMC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>André le chansonnier.</title>
								<author>FONTAN ET DESNOYER</author>
								<repository>Biblioteca Casanatense</repository>
								<idno type="URI">http://opac.casanatense.it/Record.htm?idlist=3</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Ode et Wodon</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-15" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE I</head><head type="main_subpart">SCÈNE V</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FED3" modus="sp" lm_max="7" metProfile="5, 7" form="strophe unique" schema="1(abba)" er_moy="0.5" er_max="1" er_min="0" er_mode="0(1/2)" er_moy_et="0.5">
					<p>On entend André Bernard chanter dans la coulisse.)</p>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="5" met="5"><space unit="char" quantity="4"></space><w n="1.1" punct="vg:2">Ch<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="vg">ez</seg></w>, <w n="1.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="1.3" punct="pe:5">b<pgtc id="1" weight="1" schema="GR">i<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="5" punct="pe">en</seg></rhyme></pgtc></w> !</l>
						<l n="2" num="1.2" lm="7" met="7"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="2.2">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3" punct="vg:3"><seg phoneme="œ" type="vs" value="1" rule="285" place="3" punct="vg">œ</seg>il</w>, <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.5">t<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>m<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>r<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
						<l n="3" num="1.3" lm="5" met="5"><space unit="char" quantity="4"></space><w n="3.1">S<seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="3.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3" punct="vg:5">s<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>str<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="7" met="7"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="4.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">tr<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="4.5" punct="pt:7">r<pgtc id="1" weight="1" schema="GR">i<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="7" punct="pt">en</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>