<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL12" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="2[abab]" er_moy="0.0" er_max="0" er_min="0" er_mode="0(4/4)" er_moy_et="0.0">
	<head type="tune">AIR du premier Prix.</head>
	<lg n="1" type="regexp" rhyme="">
		<l part="I" n="1" num="1.1" lm="8" met="8"><hi rend="ital"><w n="1.1" punct="pe:3">M<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>li<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe ps">e</seg></w></hi> ! … </l>
	</lg>
	<lg n="2" type="regexp" rhyme="ab">
	<head type="speaker">COURTOIS.</head>
		<l part="F" n="1" lm="8" met="8"><w n="1.2">V<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>d</w>-<w n="1.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="1.4">s<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="1.5">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>g<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="y" type="vs" value="1" rule="447" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
		<l n="2" num="2.1" lm="8" met="8"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>x</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>rs</w> <w n="2.3">d</w>'<w n="2.4" punct="pi:8">H<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>n<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pi ps">i</seg></rhyme></pgtc></w> ? …</l>
	</lg>
	<lg n="3" type="regexp" rhyme="">
	<head type="speaker">LUDOVIC.</head>
		<l part="I" n="3" num="3.1" lm="8" met="8"><hi rend="ital"><w n="3.1" punct="pe:2">D<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="pe ps">i</seg>d</w></hi> ! … </l>
	</lg>
	<lg n="4" type="regexp" rhyme="ab">
	<head type="speaker">COURTOIS.</head>
		<l part="F" n="3" lm="8" met="8"><w n="3.2">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>d</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="3.4">c<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>c<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>t<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
		<l n="4" num="4.1" lm="8" met="8"><w n="4.1"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">O</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="3">o</seg>s</w> <w n="4.3">s<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s</w> <w n="4.4">d</w>'<w n="4.5"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>rd</w>'<w n="4.6" punct="ps:8">hu<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="490" place="8" punct="ps">i</seg></rhyme></pgtc></w>…</l>
	</lg>
	<lg n="5" type="regexp" rhyme="">
	<head type="speaker">LUDOVIC.</head>
		<l part="I" n="5" num="5.1" lm="8" met="8"><hi rend="ital"><w n="5.1" punct="pe:2">C<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>vi<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="pe ps">er</seg></w></hi> ! … </l>
	</lg>
	<lg n="6" type="regexp" rhyme="ab">
	<head type="speaker">COURTOIS.</head>
		<l part="F" n="5" lm="8" met="8"><w n="5.2">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>d</w> <w n="5.3">l</w>'<w n="5.4"><seg phoneme="y" type="vs" value="1" rule="452" place="4">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.5">sc<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="377" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
		<l n="6" num="6.1" lm="8" met="8"><w n="6.1">N</w>'<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="6.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.6">p<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>r</w> <w n="6.7" punct="ps:8"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>bj<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="8" punct="ps">e</seg>t</rhyme></pgtc></w>…</l>
	</lg>
	<lg n="7" type="regexp" rhyme="">
	<head type="speaker">LUDOVIC.</head>
		<l part="I" n="7" num="7.1" lm="8" met="8"><hi rend="ital"><w n="7.1" punct="pe:3">M<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>b<seg phoneme="o" type="vs" value="1" rule="314" place="3" punct="pe ps">eau</seg></w></hi> ! … </l>
	</lg>
	<lg n="8" type="regexp" rhyme="ab">
	<head type="speaker">COURTOIS.</head>
		<l part="F" n="7" lm="8" met="8"><w n="7.2">L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.3">l</w>'<w n="7.4"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="442" place="7">o</seg>qu<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
		<l n="8" num="8.1" lm="8" met="8"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="8.3">j<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>rs</w> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="8.5">pr<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg></w> <w n="8.7" punct="pt:8">c<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>ll<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="8" punct="pt">e</seg>t</rhyme></pgtc></w>. <del reason="analysis" type="repetition" hand="KL">( bis.)</del></l>
	</lg>
</div></body></text></TEI>