<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
				<title type="sub">ROMAN IMAGINAIRE MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DLU" sort="1">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
					</name>
					<date from="1803" to="1889">1803-1889</date>
				</author>
				<author key="LAN" sort="2">
					<name>
						<forename>Joseph</forename>
						<surname>LANGLOIS</surname>
						<addname type="pen_name">Ferdinand LANGLÉ</addname>
					</name>
				  <date from="1798" to="1867">1798-1867</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>452 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">LRL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA:
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FÉE AUX MIETTES, OU LES CAMARADES DE CLASSE</title>
						<author>GABRIEL ET F. LANGLÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?id=r1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032855184.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">17 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU PALAIS-ROYAL</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="LRL14" modus="sp" lm_max="7" metProfile="7, 6, 2" form="suite périodique" schema="4(abaab)" er_moy="1.5" er_max="8" er_min="0" er_mode="0(8/12)" er_moy_et="2.6">
	<head type="tune">AIR : Des Fraises.</head>
	<lg n="1" type="quintil" rhyme="abaab">
	<head type="main">PREMIER COUPLET.</head>
		<l n="1" num="1.1" lm="7" met="7"><w n="1.1">C<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">br<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.3">Phr<seg phoneme="i" type="vs" value="1" rule="496" place="6">y</seg><pgtc id="1" weight="2" schema="CR">n<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg></rhyme></pgtc></w></l>
		<l n="2" num="1.2" lm="6" met="6"><w n="2.1">D</w>'<w n="2.2"><seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>g<seg phoneme="i" type="vs" value="1" rule="466" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3" punct="vg:6">m<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>l<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="6">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="3" num="1.3" lm="7" met="7"><w n="3.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="3.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>t</w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="3.4" punct="vg:7">b<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ge<seg phoneme="o" type="vs" value="1" rule="434" place="6">o</seg><pgtc id="1" weight="2" schema="CR">nn<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="7" punct="vg">é</seg></rhyme></pgtc></w>,</l>
		<l n="4" num="1.4" lm="7" met="7"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2" punct="vg:2">br<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="vg">a</seg>s</w>, <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t</w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.7" punct="ps:7"><pgtc id="1" weight="2" schema="[CR">n<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="7" punct="ps">é</seg></rhyme></pgtc></w>…</l>
		<l n="5" num="1.5" lm="2" met="2"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="5.2" punct="pe:2">pl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="2">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pe">e</seg></rhyme></pgtc></w> ! <del reason="analysis" type="repetition" hand="KL">(ter.)</del></l>
	</lg>
	<lg n="2" type="quintil" rhyme="abaab">
	<head type="main">DEUXIÈME COUPLET.</head>
		<l n="6" num="2.1" lm="7" met="7"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2" punct="vg:3">h<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437" place="3" punct="vg">o</seg>s</w>, <w n="6.3">qu</w>'<w n="6.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="6.5" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="vg">a</seg></rhyme></pgtc></w>,</l>
		<l n="7" num="2.2" lm="6" met="6"><w n="7.1">Qu</w>'<w n="7.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">à</seg></w> <w n="7.3">L<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="7.5" punct="pe:6"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>d<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>l<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="6">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></pgtc></w> !</l>
		<l n="8" num="2.3" lm="7" met="7"><w n="8.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="8.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="5">om</seg></w> <w n="8.5" punct="vg:7">v<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>vr<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="vg">a</seg></rhyme></pgtc></w>,</l>
		<l n="9" num="2.4" lm="7" met="7"><w n="9.1">Qu</w>'<w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">en</seg></w> <w n="9.3">br<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="9.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.6" punct="ps:7">c<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="ps">a</seg></rhyme></pgtc></w>…</l>
		<l n="10" num="2.5" lm="2" met="2"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="10.2" punct="pe:2">pl<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="2">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pe">e</seg></rhyme></pgtc></w> ! <del reason="analysis" type="repetition" hand="KL">(ter.)</del></l>
	</lg>
	<lg n="3" type="quintil" rhyme="abaab">
	<head type="main">TROISIÈME COUPLET.</head>
		<l n="11" num="3.1" lm="7" met="7"><w n="11.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2" punct="vg:3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="3" punct="vg">eu</seg>r</w>, <w n="11.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>s</w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="11.5" punct="vg:7">r<pgtc id="5" weight="6" schema="VCR"><seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>s<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="7" punct="vg">é</seg></rhyme></pgtc></w>,</l>
		<l n="12" num="3.2" lm="6" met="6"><w n="12.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>s</w> <w n="12.3">qu</w>'<w n="12.4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="12.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="12.6">th<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg><pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="6">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
		<l n="13" num="3.3" lm="7" met="7"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">sp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ct<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="13.4" punct="vg:7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">om</seg>p<pgtc id="5" weight="6" schema="VCR"><seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>s<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="7" punct="vg">é</seg></rhyme></pgtc></w>,</l>
		<l n="14" num="3.4" lm="7" met="7"><w n="14.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w> <w n="14.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>rs</w> <w n="14.3" punct="ps:7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>s<pgtc id="5" weight="8" schema="CVCR">p<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>s<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="7" punct="ps">é</seg></rhyme></pgtc></w>…</l>
		<l n="15" num="3.5" lm="2" met="2"><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="15.2" punct="pe:2">pl<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="2">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pe">e</seg></rhyme></pgtc></w> ! <del reason="analysis" type="repetition" hand="KL">( ter.)</del></l>
	</lg>
	<lg n="4" type="quintil" rhyme="abaab">
	<head type="main">QUATRIÈME COUPLET.</head>
		<l n="16" num="4.1" lm="7" met="7"><w n="16.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2" punct="vg:3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="vg">a</seg>rd</w>, <w n="16.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>x</w> <w n="16.4">pi<seg phoneme="e" type="vs" value="1" rule="240" place="5">e</seg>ds</w> <w n="16.5" punct="vg:7">cl<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>ch<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7" punct="vg">an</seg>s</rhyme></pgtc></w>, </l>
		<l n="17" num="4.2" lm="6" met="6"><w n="17.1">Vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="17.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg></w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.4" punct="vg:6">th<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg><pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="6">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
		<l n="18" num="4.3" lm="7" met="7"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="18.2" punct="vg:5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>pr<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="18.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="18.4" punct="vg:7">d<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="7" punct="vg">en</seg>ts</rhyme></pgtc></w>,</l>
		<l n="19" num="4.4" lm="7" met="7"><w n="19.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="19.2">j<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="19.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="19.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="6">en</seg>t</w> <w n="19.6" punct="pe:7"><pgtc id="7" weight="0" schema="[R"><rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7" punct="pe ps">an</seg>s</rhyme></pgtc></w> ! …</l>
		<l n="20" num="4.5" lm="2" met="2"><w n="20.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="20.2" punct="pe:2">pl<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="2">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pe">e</seg></rhyme></pgtc></w> ! <del reason="analysis" type="repetition" hand="KL">( ter.)</del></l>
	</lg>
</div></body></text></TEI>