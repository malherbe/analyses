<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
				<title type="sub">INVRAISEMBLANCE EN TROIS PARTIES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
				  <name>
					<forename>Félix-Auguste</forename>
					<surname>DUVERT</surname>
				  </name>
				  <date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="DLV" sort="2">
					<name>
						<forename>Augustin Théodore</forename>
						<nameLink>de</nameLink>
						<surname>LAUZANNE DE VAUROUSSEL</surname>
						<addname type="other">Lauzanne</addname>
					</name>
					<date from="1805" to="1877">1805-1877</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>135 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
                    <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
						<author>F.-A. DUVERT EN SOCIÉTÉ AVEC M. LAUZANNE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?vid=UOM:39015076863409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>University of Michigan Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/mdp.39015076863409</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">16 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DLV4" modus="cp" lm_max="9" metProfile="8, (9)">
	<head type="tune">AIR : On dit que je suis sans malice.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">V<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">scr<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="1.5" punct="vg:8"><seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>tr<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>, </l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">z</w>'<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>s</w> <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>t</w> <w n="2.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.6" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rt<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>, </l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="3.3">pr<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="3.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="5">en</seg></w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r</w> <w n="3.6">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg></w></l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="4.3">n</w>'<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="4.5">j<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s</w> <w n="4.6" punct="vg:8">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg></w>, </l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="5.2" punct="vg:4">M<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="5.4">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="5.5" punct="vg:8">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>, </l>
		<l n="6" num="1.6" lm="9"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="P">À</seg></w> <w n="6.2">ch<seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="6.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>t</w> <w n="6.4">l</w>'<w n="6.5">qu<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M/mc">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-27" place="7" mp="Fm">e</seg></w>-<w n="6.6" punct="vg:9">hu<seg phoneme="i" type="vs" value="1" rule="490" place="8" mp="M/mc">i</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="409" place="9">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="vg" mp="F">e</seg></w>, </l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="2">en</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="7.4">d<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>bs<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>l<seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="vg">en</seg>t</w>, </l>
		<l n="8" num="1.8" lm="8" met="8"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="409" place="1">è</seg>s</w> <w n="8.2">qu</w>'<w n="8.3">ç<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="8.4">n</w>'<w n="8.5">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rt</w> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="8.7">d<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg></w> <w n="8.8" punct="vg:8">r<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>g<seg phoneme="i" type="vs" value="1" rule="466" place="7">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="vg">en</seg>t</w>,</l> 
	</lg>
</div></body></text></TEI>