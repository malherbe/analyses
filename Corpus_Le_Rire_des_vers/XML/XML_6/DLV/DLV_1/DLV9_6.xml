<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
				<title type="sub">INVRAISEMBLANCE EN TROIS PARTIES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
				  <name>
					<forename>Félix-Auguste</forename>
					<surname>DUVERT</surname>
				  </name>
				  <date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="DLV" sort="2">
					<name>
						<forename>Augustin Théodore</forename>
						<nameLink>de</nameLink>
						<surname>LAUZANNE DE VAUROUSSEL</surname>
						<addname type="other">Lauzanne</addname>
					</name>
					<date from="1805" to="1877">1805-1877</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>135 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
                    <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE MARCHAND DE PEAUX DE LAPIN OU LE RÊVE</title>
						<author>F.-A. DUVERT EN SOCIÉTÉ AVEC M. LAUZANNE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books?vid=UOM:39015076863409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>University of Michigan Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/mdp.39015076863409</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">16 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DLV9" modus="sm" lm_max="8" metProfile="8">
	<head type="tune">AIR : Au marché qui vient de s'ouvrir.</head>
		<div type="section" n="1">
			<lg n="1">
				<l n="1" num="1.1" lm="8" met="8"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="1.2" punct="vg:4">J<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>th<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg>s</w>, <w n="1.3" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="pe">a</seg>h</w> ! <w n="1.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6">e</seg>l</w> <w n="1.5" punct="pe:8">h<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pe">eu</seg>r</w> ! </l>
				<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="2.3" punct="vg:4">p<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="vg">ai</seg>r</w>, <w n="2.4" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="pe">a</seg>h</w> ! <w n="2.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6">e</seg>l</w> <w n="2.6" punct="pe:8">b<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pe">eu</seg>r</w> ! </l>
				<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="3.2">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="452" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.3">lu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="3.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>d</w> <w n="3.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="3.6" punct="pv:8">br<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pv">a</seg>s</w> ; </l>
				<l n="4" num="1.4" lm="8" met="8"><w n="4.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.4" punct="pe:8">J<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>th<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pe">a</seg>s</w> ! </l>
			</lg>
		</div>
		<p>
			SCÈNE V<lb></lb>
			LA VICOMTESSE, PINGOT, DOROTHÉE, LE SECRÉTAIRE, <lb></lb>
			DOMESTIQUES, <lb></lb>
			Dorothée a son premier costume de la première partie.-Dorothée et Pingot<lb></lb>
			sont entrés en scène pendant le chour, au moment où Jonathas entre dans<lb></lb>
			le cabinet. Ils l'ont aperçu. <lb></lb>
		</p>
		<div type="section" n="2">
			<head type="speaker">DOROTHÉE, menaçant Jonathas, qui a disparu.</head>
			<head type="tune">Suite de l'air.</head>
			<lg n="1">
				<l n="5" num="1.1" lm="8" met="8"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>d</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.4">p<seg phoneme="o" type="vs" value="1" rule="314" place="6">eau</seg>x</w> <w n="5.5">d</w>'<w n="5.6" punct="vg:8">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" punct="vg">in</seg></w>, </l>
				<l n="6" num="1.2" lm="8" met="8"><w n="6.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="6.2">n</w>'<w n="6.3">p<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="6.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.6">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="6.7" punct="pe:8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pe">ain</seg></w> ! </l>
				<l n="7" num="1.3" lm="8" met="8"><w n="7.1">J</w>'<w n="7.2">t</w>'<w n="7.3"><seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>gl<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>s</w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6">e</seg>c</w> <w n="7.5" punct="ps:8">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="ps">i</seg>r</w>… </l>
			</lg>
			<lg n="2">
			<head type="speaker">PINGOT, à part.</head>
				<l n="8" num="2.1" lm="8" met="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="8.2">m<seg phoneme="wa" type="vs" value="1" rule="422" place="2">oi</seg></w> <w n="8.3">j</w>'<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>r</w>' <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.6" punct="pt:8">r<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg><seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pt">i</seg>r</w>. </l>
			</lg>
		</div>
</div></body></text></TEI>