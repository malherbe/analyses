<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">27, 28 ET 29 JUILLET, TABLEAU ÉPISODIQUE DES TROIS JOURNÉES.</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ARA" sort="1">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<author key="DUV" sort="2">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>495 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
						<author>ARAGO ET DUVERT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=xjVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
								<author>ARAGO ET DUVERT</author>
								<repository>Österreichische Nationalbibliothek</repository>
								<idno type="URI">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886206</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : U+02BC MODIFIER LETTER APOSTROPHE) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<change when="2021-06-14" who="RR">Quelques corrections concernant la distribution du signe signe ʼ (UNICODE : ʼ).</change>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME JOURNÉE</head><head type="main_subpart">SCÈNE V.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="AED12" modus="cm" lm_max="10" metProfile="4+6">
			<head type="tune">AIR : Du château perdu.</head>
				<lg n="1">
					<head type="main">RAIMOND.</head>
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">C</w>'<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="1.3">j<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="1.4">l</w>'<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>ttr<seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="M">i</seg>b<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>t</w> <w n="1.6">dʼ</w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="1.8" punct="vg:10">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9" mp="M">o</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="351" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="2.2">gl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>vʼ</w> <w n="2.3" punct="vg:4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4" punct="vg" caesura="1">an</seg>s</w>,<caesura></caesura> <w n="2.4">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="2.5" punct="vg:6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg">an</seg>cʼ</w>, <w n="2.6">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="2.7" punct="pv:10">b<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>cl<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pv">er</seg>s</w> ;</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="434" place="2" mp="M">o</seg>ppr<seg phoneme="i" type="vs" value="1" rule="466" place="3" mp="M">i</seg>m<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="3.4">fr<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="3.6" punct="vg:10">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9" mp="M">ai</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="351" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>ls</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.3">lʼ</w> <w n="4.4">br<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="4.6">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6" mp="C">e</seg>s</w> <w n="4.7">pr<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>x</w> <w n="4.8" punct="pt:10">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>li<seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="pt">er</seg>s</w>.</l>
					<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">S<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1" mp="M/mp">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M/mp">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="3" mp="Lp">ez</seg></w>-<w n="5.2" punct="dp:4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4" punct="dp" caesura="1">e</seg>s</w> :<caesura></caesura> <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="P">a</seg>r</w> <w n="5.4">v<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>tʼ</w> <w n="5.5" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>sm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">l<seg phoneme="œ" type="vs" value="1" rule="406" place="2" mp="C">eu</seg>r</w> <w n="6.3">h<seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="4" caesura="1">eu</seg>r</w><caesura></caesura> <w n="6.4">s<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>t</w> <w n="6.5" punct="pv:10">r<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>h<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="pv">é</seg></w> ;</l>
					<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="C">u</seg></w> <w n="7.4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="4" caesura="1">oin</seg>s</w><caesura></caesura> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.6">f<seg phoneme="ɛ" type="vs" value="1" rule="63" place="6">e</seg>r</w> <w n="7.7">d<seg phoneme="y" type="vs" value="1" rule="449" place="7" mp="C">u</seg></w> <w n="7.8">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>sp<seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>sm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
					<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="8.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3">b<seg phoneme="o" type="vs" value="1" rule="314" place="3">eau</seg></w> <w n="8.4">j<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>r</w><caesura></caesura> <w n="8.5">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="8.7" punct="pt:10">l<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="pt">é</seg></w>.</l>
				</lg>
		</div></body></text></TEI>