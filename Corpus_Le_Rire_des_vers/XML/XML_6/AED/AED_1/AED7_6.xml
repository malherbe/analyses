<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">27, 28 ET 29 JUILLET, TABLEAU ÉPISODIQUE DES TROIS JOURNÉES.</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ARA" sort="1">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<author key="DUV" sort="2">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>495 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
						<author>ARAGO ET DUVERT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=xjVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>27, 28 et 29 juillet, tableau épisodique des trois journées</title>
								<author>ARAGO ET DUVERT</author>
								<repository>Österreichische Nationalbibliothek</repository>
								<idno type="URI">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886206</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : U+02BC MODIFIER LETTER APOSTROPHE) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<change when="2021-06-14" who="RR">Quelques corrections concernant la distribution du signe signe ʼ (UNICODE : ʼ).</change>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE JOURNÉE.</head><head type="main_subpart">SCÈNE VII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="AED7" modus="cp" lm_max="10" metProfile="8, 4+6">
			<head type="tune">AIR : Du magistrat irréprochable.</head>
			<lg n="1">
				<head type="main">RAIMOND.</head>
				<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1" punct="pe:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="pe">on</seg></w> ! <w n="1.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="C">i</seg>l</w> <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="1.4" punct="vg:4">f<seg phoneme="o" type="vs" value="1" rule="317" place="4" punct="vg" caesura="1">au</seg>t</w>,<caesura></caesura> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="1.6">v<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>x</w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="1.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="1.9">p<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
				<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="C">ou</seg>s</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>llʼ</w> <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>s</w><caesura></caesura> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="P">à</seg></w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="2.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="M">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8">an</seg>s</w> <w n="2.7" punct="pt:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>xpl<seg phoneme="wa" type="vs" value="1" rule="419" place="10" punct="pt">oi</seg>ts</w>.</l>
				<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1" mp="M/mp">Em</seg>br<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M/mp">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="Lp">on</seg>s</w>-<w n="3.2" punct="vg:4">n<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="414" place="5">ô</seg></w> <w n="3.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="3.5">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.6" punct="pe:10">ch<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="481" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></w> !</l>
				<p>(À part.)</p>
				<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">P<seg phoneme="ø" type="vs" value="1" rule="397" place="1" mp="Lc">eu</seg>t</w>-<w n="4.2" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="4.3">c</w>'<w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4" caesura="1">e</seg>st</w><caesura></caesura> <w n="4.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="P">ou</seg>r</w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7" mp="M">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.8" punct="pe:10">f<seg phoneme="wa" type="vs" value="1" rule="419" place="10" punct="pe">oi</seg>s</w> !</l>
				<p>(Haut.)</p>
				<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1" mp="M/mp">Em</seg>br<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M/mp">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="Lp">on</seg>s</w>-<w n="5.2" punct="vg:4">n<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M/mp">em</seg>br<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M/mp">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="Lp">on</seg>s</w>-<w n="5.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>s</w> <w n="5.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>s</w> <w n="5.6" punct="ps:10">tr<seg phoneme="wa" type="vs" value="1" rule="419" place="10" punct="ps">oi</seg>s</w> …</l>
			</lg>
			<p>(Raimond, Julien et Louise tombent dans les bras l'un de l'autre. Raimond cherche à contenir son émotion, se dégage des bras de ses enfans, et dit à part :) </p>
			<lg n="2">
				<l n="6" num="2.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="6.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="6.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2">an</seg>s</w> <w n="6.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>t</w> <w n="6.5" punct="vg:8">s<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
				<l n="7" num="2.2" lm="10" met="4+6"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="C">ou</seg>s</w> <w n="7.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="M">om</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" caesura="1">on</seg>s</w><caesura></caesura> <w n="7.4">s<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="P">ou</seg>s</w> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6" mp="C">e</seg>s</w> <w n="7.6">c<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>ps</w> <w n="7.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" punct="vg">in</seg>s</w>,</l>
				<l n="8" num="2.3" lm="10" met="4+6"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="414" place="1">Ô</seg></w> <w n="8.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" mp="C">on</seg></w> <w n="8.3" punct="pe:4">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="3" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="320" place="4" punct="pe" caesura="1">y</seg>s</w> !<caesura></caesura> <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="414" place="5">ô</seg></w> <w n="8.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" mp="C">on</seg></w> <w n="8.6" punct="pe:8">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="7" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="320" place="8" punct="pe">y</seg>s</w> ! <w n="8.7">pr<seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
				<l n="9" num="2.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="9.2">v<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="9.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="9.5" punct="pe:8"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>rph<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" punct="pe">in</seg>s</w> !</l>
			</lg>
		</div></body></text></TEI>