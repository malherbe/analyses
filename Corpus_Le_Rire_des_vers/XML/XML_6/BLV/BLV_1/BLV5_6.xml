<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BANDEAU</title>
				<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="BLL" sort="1">
					<name>
						<forename>Jean-Nicolas</forename>
						<surname>BOUILLY</surname>
					</name>
					<date from="1763" to="1842">1763-1842</date>
				</author>
				<author key="VAN" sort="2">
					<name>
						<forename>Émile</forename>
						<surname>VANDERBURCH</surname>
						<addname type="other">Émile VANDERBURCH</addname>
						<addname type="other">Émile VANDER-BURCH</addname>
						<addname type="other">Émile VANDERBURCK</addname>
						<addname type="other">Émile VAN DER BURCK</addname>
					</name>
					<date from="1794" to="1862">1794-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>168 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLV_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE BANDEAU</title>
						<author>BOUILLY ET EM. VANDERBURCH</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GALLICA</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9782259n.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La Bibliothèque nationale de France</repository>
								<idno type="URL">https ://catalogue.bnf.fr/ark :/12148/cb31531335q</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">21 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="BLV5" modus="cm" lm_max="10" metProfile="4+6">
	<head type="tune">AIR de Préville et Taconnet.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1" punct="vg:2">S<seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">é</seg>du<seg phoneme="i" type="vs" value="1" rule="490" place="2" punct="vg">i</seg>t</w>, <w n="1.2" punct="vg:4">ch<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="1.3">j</w>'<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="5">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="1.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="1.8" punct="pv:10">m<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="481" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></w> ;</l>
		<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="2.2">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2" mp="C">e</seg>t</w> <w n="2.3">h<seg phoneme="i" type="vs" value="1" rule="496" place="3" mp="M">y</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="220" place="4" caesura="1">en</seg></w><caesura></caesura> <w n="2.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="2.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s</w> <w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="2.7">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="8">oin</seg>t</w> <w n="2.8">d</w>'<w n="2.9" punct="dp:10">h<seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="dp">eu</seg>r</w> :</l>
		<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="3.2" punct="vg:2">t<seg phoneme="o" type="vs" value="1" rule="414" place="2" punct="vg">ô</seg>t</w>, <w n="3.3">pl<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>s</w> <w n="3.4" punct="vg:4">t<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg" caesura="1">a</seg>rd</w>,<caesura></caesura> <w n="3.5"><seg phoneme="i" type="vs" value="1" rule="496" place="5" mp="C">y</seg></w> <w n="3.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" mp="M">on</seg>s<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>cr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>t</w> <w n="3.7">s<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="3.8" punct="vg:10">v<seg phoneme="i" type="vs" value="1" rule="481" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
		<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">L</w>'<w n="4.2" punct="vg:1">h<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1" punct="vg">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="4.3"><seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M/mc">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="Lc">i</seg></w>-<w n="4.4" punct="vg:4">b<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="4.5">c<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>rt</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="7">è</seg>s</w> <w n="4.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="4.8" punct="pt:10">b<seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="pt">eu</seg>r</w>.</l>
		<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">L</w>'<w n="5.2">h<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="5.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="5.4">c<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>rt</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="7">è</seg>s</w> <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="5.7" punct="pt:10">b<seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="pt">eu</seg>r</w>.</l>
		<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="6.2">tr<seg phoneme="o" type="vs" value="1" rule="432" place="2">o</seg>p</w> <w n="6.3">s<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6" mp="M">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg></w> <w n="6.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="C">ou</seg>s</w> <w n="6.7" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>pr<seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
		<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">b<seg phoneme="o" type="vs" value="1" rule="443" place="2" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r</w> <w n="7.3">fu<seg phoneme="i" type="vs" value="1" rule="490" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="7.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>d</w> <w n="7.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="7.6">l</w>'<w n="7.7" punct="pv:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7" mp="M">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="439" place="9" mp="M">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" punct="pv">on</seg>s</w> ;</l>
		<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="8.2">fu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>t</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4" caesura="1">o</seg>rs</w><caesura></caesura> <w n="8.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="8.6">l</w>'<w n="8.7" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" punct="pt">on</seg>s</w>.</l>
		<l n="9" num="1.9" lm="10" met="4+6"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="9.2">y<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="9.3" punct="vg:4"><seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4" punct="vg" caesura="1">e</seg>rts</w>,<caesura></caesura> <w n="9.4">r<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="7">en</seg>t</w> <w n="9.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="C">on</seg></w> <w n="9.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="9.7" punct="dp:10">tr<seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></w> :</l>
		<l n="10" num="1.10" lm="10" met="4+6"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="M">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="10.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4" caesura="1">en</seg></w><caesura></caesura> <w n="10.4">l</w>'<w n="10.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>ttr<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>p<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="P">à</seg></w> <w n="10.7" punct="pt:10">t<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" punct="pt">on</seg>s</w>.</l>
	</lg>
</div></body></text></TEI>