<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB11" modus="sm" lm_max="8" metProfile="8">
	<head type="tune">Air de Catinat</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="1.2">s</w>'<w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="1.4">v<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="1.5">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.6"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="1.8" punct="vg:8">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="y" type="vs" value="1" rule="449" place="8" punct="vg">u</seg></w>,</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>s</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="2.3" punct="vg:5">n<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.6" punct="pv:8">su<seg phoneme="i" type="vs" value="1" rule="490" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="3.2" punct="pe:2">f<seg phoneme="wa" type="vs" value="1" rule="422" place="2" punct="pe">oi</seg></w> ! <w n="3.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">n</w>'<w n="3.5"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s</w> <w n="3.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>s</w> <w n="3.7" punct="vg:8">v<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="449" place="8" punct="vg">u</seg></w>,</l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="4.2">s<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="4.3" punct="vg:4">pl<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r</w> <w n="4.5">s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="4.6" punct="pt:8">v<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="5.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="2">en</seg></w> <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">l</w>'<w n="5.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="5.6">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="5">en</seg>t</w> <w n="5.7">d</w>'<w n="5.8" punct="vg:8"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>bt<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg>r</w>,</l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>s</w> <w n="6.5" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">s<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>r</w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">b<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>r</w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="7.6" punct="vg:8">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg>r</w>,</l>
		<l n="8" num="1.8" lm="8" met="8"><w n="8.1">J</w>'<w n="8.2"><seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="8.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="8.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="8.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rrh<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
	</lg>
</div></body></text></TEI>