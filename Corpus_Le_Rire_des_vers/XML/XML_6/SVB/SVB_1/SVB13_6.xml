<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB13" modus="sp" lm_max="5" metProfile="2, 5">
	<head type="tune">Air du proces du Baiser.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="2" met="2"><w n="1.1" punct="vg:2">Gl<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="vg">ez</seg></w>,</l>
		<l n="2" num="1.2" lm="2" met="2"><w n="2.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="vg">ez</seg></w>,</l>
		<l n="3" num="1.3" lm="5" met="5"><w n="3.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="3.3" punct="pv:5">c<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></w> ;</l>
		<l n="4" num="1.4" lm="2" met="2"><w n="4.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="1">en</seg></w> <w n="4.2">n</w>'<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w></l>
		<l n="5" num="1.5" lm="2" met="2"><w n="5.1">C<seg phoneme="ɔ" type="vs" value="1" rule="442" place="1">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2">e</seg>t</w></l>
		<l n="6" num="1.6" lm="5" met="5"><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="6.3" punct="dp:5">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="y" type="vs" value="1" rule="d-3" place="4">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="189" place="5" punct="dp">e</seg>t</w> :</l>
		<l n="7" num="1.7" lm="2" met="2"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="7.2" punct="vg:2">g<seg phoneme="u" type="vs" value="1" rule="424" place="2" punct="vg">oû</seg>t</w>,</l>
		<l n="8" num="1.8" lm="2" met="2"><w n="8.1" punct="vg:2">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="2" punct="vg">ou</seg>t</w>,</l>
		<l n="9" num="1.9" lm="5" met="5"><w n="9.1">J</w>'<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="1">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.3">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.4" punct="vg:5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
		<l n="10" num="1.10" lm="2" met="2"><w n="10.1">S<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="10.2" punct="vg:2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="vg">a</seg>s</w>,</l>
		<l n="11" num="1.11" lm="2" met="2"><w n="11.1">S<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>cs</w></l>
		<l n="12" num="1.12" lm="5" met="5"><w n="12.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="12.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>s</w> <w n="12.3">d</w>'<w n="12.4" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="pe">a</seg>s</w> !</l>
		<stage>( pleurant.)</stage>
		<l n="13" num="1.13" lm="5" met="5"><w n="13.1">F<seg phoneme="a" type="vs" value="1" rule="192" place="1">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>d<seg phoneme="o" type="vs" value="1" rule="434" place="4">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
		<l n="14" num="1.14" lm="5" met="5"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>x</w> <w n="14.2">pl<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>rs</w> <w n="14.3" punct="pe:5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>mn<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg pt pe">e</seg></w>, . !</l>
		<l n="15" num="1.15" lm="5" met="5"><w n="15.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="15.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="15.3" punct="vg:5">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>st<seg phoneme="i" type="vs" value="1" rule="466" place="4">i</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
		<l n="16" num="1.16" lm="5" met="5"><w n="16.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">j</w>'<w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="16.4" punct="pe:5">r<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="5" punct="pe">i</seg>s</w> !</l>
		<stage>( Gaiment.)</stage>
		<l n="17" num="1.17" lm="5" met="5"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="17.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="17.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="310" place="5">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
		<l n="18" num="1.18" lm="5" met="5"><w n="18.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="18.3">l</w>'<w n="18.4" punct="pv:5"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></w> ;</l>
		<l n="19" num="1.19" lm="5" met="5"><w n="19.1">H<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="402" place="2">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="19.2">d</w>'<w n="19.3" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
		<l n="20" num="1.20" lm="5" met="5"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="20.2">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg></w> <w n="20.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="20.4" punct="dp:5">d<seg phoneme="i" type="vs" value="1" rule="467" place="5" punct="dp">i</seg>s</w> :</l>
	</lg>
	<lg n="2">
		<l n="21" num="2.1" lm="2" met="2"><w n="21.1" punct="pv:2">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="pv">ez</seg></w> ;</l>
		<l n="22" num="2.2" lm="2" met="2"><w n="22.1" punct="vg:2">Gl<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="vg">ez</seg></w>,</l>
		<l n="23" num="2.3" lm="5" met="5"><w n="23.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="23.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="23.3" punct="pv:5">c<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></w> ;</l>
		<l n="24" num="2.4" lm="2" met="2"><w n="24.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="1">en</seg></w> <w n="24.2">n</w>'<w n="24.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w></l>
		<l ana="unanalyzable" n="25" num="2.5">Coquet, etc.</l>
	</lg>
</div></body></text></TEI>