<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB23" modus="sp" lm_max="8" metProfile="8, 5, (4), (1)">
	<head type="form">Chanson du Régent.</head>
	<head type="tune">Air nouveau de M. Paris.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="pe:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">In</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>s<seg phoneme="e" type="vs" value="1" rule="408" place="3" punct="pe">é</seg>s</w> ! <w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="1.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.4">v<seg phoneme="wa" type="vs" value="1" rule="439" place="6">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>s</w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>s</w></l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="2.2">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg>s</w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="2.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg>ts</w>,</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="3.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="3.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="3.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.6">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="2">en</seg>t</w> <w n="4.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="4.4">d</w>'<w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="4.6" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="317" place="8">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="5.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">si<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="4">en</seg></w> <w n="5.5">d<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">aî</seg>t</w> <w n="5.6" punct="pv:8"><seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pv">an</seg>t</w> ;</l>
		<l n="6" num="1.6" lm="4"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="6.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="6.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
		<l n="7" num="1.7" lm="1"><w n="7.1">L</w>'<w n="7.2">h<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="2">e</seg></w></l>
		<l n="8" num="1.8" lm="5" met="5"><w n="8.1">N</w>'<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="8.3">j<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="8.4" punct="pe:*">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t<seg phoneme="?" type="va" value="1" rule="161" place="5" punct="pe">en</seg>t</w> !</l>
	</lg> 
	<p>
* TOUS. 
Et voilà conime, etc. <lb></lb>
On danse. Pendant le refrain du couplet, Madame Gervais, <lb></lb>
conduite par des dames, entre dans la chambre à gauche. <lb></lb>
PELLEVIN. <lb></lb>
Chut ! silence…, la mariée a disparu. <lb></lb>
MIMI, <lb></lb>
Quand je pense que ce devrait être moi. <lb></lb>
Quel sacrifice ! … elle est si jolie…'<lb></lb>
DESOEILLETS, d part. <lb></lb>
Pellevin, à Desvillets, bas, <lb></lb>
Dites donc, à vous… ( Haut.) Second couplet ! <lb></lb>
	</p>
	<lg n="2">
		<l n="9" num="2.1" lm="8" met="8"><w n="9.1">L</w>'<w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="9.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r</w> <w n="9.5" punct="vg:8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="vg">an</seg>d</w>,</l>
		<l n="10" num="2.2" lm="8" met="8"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">vi<seg phoneme="e" type="vs" value="1" rule="382" place="2">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rd</w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>d<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="8" punct="vg">en</seg>t</w>,</l>
		<l n="11" num="2.3" lm="8" met="8"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="11.2">f<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.4">f<seg phoneme="a" type="vs" value="1" rule="192" place="5">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="11.6">pu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg>s</w> <w n="11.7" punct="vg:8">v<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="12" num="2.4" lm="8" met="8"><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="12.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.4">d<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="12.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r</w> <w n="12.6" punct="vg:8">n<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="13" num="2.5" lm="8" met="8"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="13.2">vi<seg phoneme="ɛ" type="vs" value="1" rule="381" place="2">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>x<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="13.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="13.5" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pv">an</seg>t</w> ;</l>
		<l n="14" num="2.6" lm="4"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="14.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="14.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
		<l n="15" num="2.7" lm="1"><w n="15.1">L</w>'<w n="15.2">h<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="2">e</seg></w></l>
		<l n="16" num="2.8" lm="5" met="5"><w n="16.1">N</w>'<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="16.3">j<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="16.4" punct="pt:*">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t<seg phoneme="?" type="va" value="1" rule="161" place="5" punct="pt">en</seg>t</w>.</l>
	</lg>
</div></body></text></TEI>