<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB14" modus="cp" lm_max="10" metProfile="8, 4+6">
	<head type="tune">Air : Voilà trois ans qu'en co village. ( Léocadie.)</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>s</w> ! <w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="1.4" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="2.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="2.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4">n</w>'<w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="2.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>s</w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.8" punct="pe:8">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="8" punct="pe">en</seg></w> !</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">J</w>'<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="3.3">s<seg phoneme="ɛ" type="vs" value="1" rule="383" place="2">ei</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4" punct="ps:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3" punct="ps">an</seg>s</w>… <w n="3.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="3.6">d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t</w> <w n="3.7">qu</w>'<w n="3.8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="3.9">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.10" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="8">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="4.2">d<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>t</w> <w n="4.3">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="4.5">d<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>x</w> <w n="4.6" punct="vg:8">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="8" punct="vg">en</seg></w>,</l>
		<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">J</w>'<w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">en</seg></w> <w n="5.3">pl<seg phoneme="œ" type="vs" value="1" rule="406" place="2" mp="M">eu</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">ai</seg>s</w><caesura></caesura> <w n="5.4">s<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg></w> <w n="5.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="5.6">n</w>'<w n="5.7"><seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>t</w> <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="5.9" punct="pe:10">si<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="10" punct="pe">en</seg></w> !</l>
		<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="6.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="6.3" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="dp" caesura="1">i</seg>t</w> :<caesura></caesura> <w n="6.4">V<seg phoneme="wa" type="vs" value="1" rule="419" place="5" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="6.6">j<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1">C</w>'<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="7.4">R<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.7">b<seg phoneme="o" type="vs" value="1" rule="314" place="7">eau</seg></w> <w n="7.8" punct="ps:8">j<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="ps">ou</seg>r</w>…</l>
		<l n="8" num="1.8" lm="8" met="8"><w n="8.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">n</w>'<w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="8.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.7">s<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>s</w> <w n="8.8" punct="pv:8">j<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>l<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
		<l n="9" num="1.9" lm="8" met="8"><w n="9.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1" punct="vg">ai</seg>s</w>, <w n="9.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="9.3" punct="vg:3">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="3" punct="vg">eu</seg></w>, <w n="9.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>d</w> <w n="9.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="5">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="9.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="9.7" punct="pi:8">t<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pi">ou</seg>r</w> ?</l>
	</lg>
	<lg n="2">
		<l n="10" num="2.1" lm="8" met="8"><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>t</w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">v<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="10.5" punct="pv:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
		<l n="11" num="2.2" lm="8" met="8"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">b<seg phoneme="a" type="vs" value="1" rule="306" place="2">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="11.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="11.4">d<seg phoneme="ɔ" type="vs" value="1" rule="418" place="5">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="11.6" punct="pv:8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pv">ain</seg></w> ;</l>
		<l n="12" num="2.3" lm="8" met="8"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2" punct="vg:2">su<seg phoneme="i" type="vs" value="1" rule="490" place="2" punct="vg">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4">e</seg>c</w> <w n="12.4">s<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="12.5" punct="vg:8">h<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="13" num="2.4" lm="8" met="8"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="13.2" punct="vg:3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="vg">ou</seg>s</w>, <w n="13.3"><seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.5" punct="pv:8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" punct="pv">in</seg></w> ;</l>
		<l n="14" num="2.5" lm="10" met="4+6"><w n="14.1">Vi<seg phoneme="ɛ" type="vs" value="1" rule="365" place="1">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4" caesura="1">è</seg>s</w><caesura></caesura> <w n="14.3">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>d<seg phoneme="o" type="vs" value="1" rule="314" place="6">eau</seg>x</w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="14.5" punct="pt:10">s<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10" punct="pt">ain</seg></w>.</l>
		<l n="15" num="2.6" lm="8" met="8"><w n="15.1">L</w>'<w n="15.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">aî</seg>t</w> <w n="15.4">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.5">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="15.7" punct="dp:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
		<l n="16" num="2.7" lm="8" met="8"><w n="16.1">B<seg phoneme="o" type="vs" value="1" rule="314" place="1">eau</seg></w> <w n="16.2" punct="vg:3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="16.3">g<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>ts</w> <w n="16.4" punct="vg:5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" punct="vg">an</seg>cs</w>, <w n="16.5">r<seg phoneme="ɔ" type="vs" value="1" rule="442" place="6">o</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="a" type="vs" value="1" rule="341" place="7">à</seg></w> <w n="16.7" punct="pv:8">j<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pv">ou</seg>r</w> ;</l>
		<l n="17" num="2.8" lm="8" met="8"><w n="17.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">ç<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="17.3">v<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="17.4" punct="vg:4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4" punct="vg">en</seg></w>, <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="17.6">fl<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>r</w> <w n="17.7">d</w>'<w n="17.8" punct="pe:8"><seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ps">e</seg></w> ! …</l>
		<l n="18" num="2.9" lm="8" met="8"><w n="18.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="18.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="18.3" punct="vg:3">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="3" punct="vg">eu</seg></w>, <w n="18.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>d</w> <w n="18.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="5">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="18.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="18.7" punct="pe:8">t<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pe">ou</seg>r</w> !</l>
	</lg>
</div></body></text></TEI>