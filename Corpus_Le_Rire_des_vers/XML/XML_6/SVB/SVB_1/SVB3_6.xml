<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ROUÉS</title>
				<title type="sub">COMÉDIE HISTORIQUE, MÊLÉE DE CHANTS, EN TROIS ACTES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="SAU" sort="1">
				  <name>
					<forename>Thomas</forename>
					<surname>SAUVAGE</surname>
				  </name>
				  <date from="1794" to="1877">1794-1877</date>
				</author>
				<author key="BYD" sort="2">
				  <name>
					<forename>Jean-François-Alfred</forename>
					<surname>BAYARD</surname>
				  </name>
				  <date from="1796" to="1853">1796-1853</date>
				</author>
					<editor>Le Rire des vers, Université de Bâle</editor>
					<editor>
						Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
						(EA 4255)
					</editor>
					<respStmt>
						<resp>Encodage en XML (CRISCO, université de Caen)</resp>
						<name id="KL">
							<forename>Kedi</forename>
							<surname>LI</surname>
						</name>
					</respStmt>
					<respStmt>
						<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
						<name id="RR">
							<forename>Richard</forename>
							<surname>RENAULT</surname>
						</name>
					</respStmt>
			</titleStmt>
			<extent>458 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">SVB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES ROUÉS</title>
						<author>SAUVAGE ET BAYARD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books ?vid=BL:A0021461620</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034256747.0x000001</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">10 SEPTEMBRE 1833</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVB3" modus="sm" lm_max="8" metProfile="8">
	<head type="tune">Air de Masaniello.</head>
		<div type="section" n="1">
			<lg n="1">
				<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="1.2">c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="2">o</seg>qu<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg></w> <w n="1.3">d</w>'<w n="1.4" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>bb<seg phoneme="e" type="vs" value="1" rule="408" place="5" punct="vg">é</seg></w>, <w n="1.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.6">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg></w> <w n="1.7" punct="vg:8">d<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
				<l n="2" num="1.2" lm="8" met="8"><w n="2.1">F<seg phoneme="œ" type="vs" value="1" rule="303" place="1">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="2.2">c<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="2.4" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pv">ou</seg>rs</w> ;</l>
				<l n="3" num="1.3" lm="8" met="8"><w n="3.1">C</w>'<w n="3.2" punct="vg:1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1" punct="vg">e</seg>st</w>, <w n="3.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.4" punct="vg:3">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="3" punct="vg">oi</seg>s</w>, <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.6">di<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="3.8" punct="vg:8">s<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
				<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="4.3">d<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="4.6" punct="pt:8">j<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">ou</seg>rs</w>.</l>
				<l n="5" num="1.5" lm="8" met="8"><w n="5.1">C</w>'<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="5.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.5">qu</w>'<w n="5.6"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>l</w> <w n="5.7">f<seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>t</w> <w n="5.8" punct="vg:8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
				<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="6.2">d<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t</w> <w n="6.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="6.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t</w> <w n="6.5" punct="pt:8">c<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="189" place="8" punct="pt">e</seg>t</w>.</l>
			</lg>
			<lg n="2">
			<head type="speaker">HENRI, riant.</head>
			<p>
				Bravo ! (4 Desœillets.) C'est pour toi. 
			</p>
				<l n="7" num="2.1" lm="8" met="8"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">n</w>'<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w>-<w n="7.4"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="7.5" punct="vg:4">l<seg phoneme="a" type="vs" value="1" rule="341" place="4" punct="vg">à</seg></w>, <w n="7.6">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="7.7">v<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="7.8" punct="pe:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
				<l n="8" num="2.2" lm="8" met="8"><w n="8.1">P<seg phoneme="ø" type="vs" value="1" rule="397" place="1">eu</seg>t</w>-<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="8.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5" punct="pt:8">c<seg phoneme="o" type="vs" value="1" rule="434" place="5">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pt">ai</seg>t</w>.</l>
			</lg>
		</div>
		<p>
			DESOEILLETS. <lb></lb>
Ah ça ! et le prince n'a-t-il rien mérité ? <lb></lb>
PELLEVIN. <lb></lb>
Eb ! eh ! je ne dis pas.<lb></lb>
		</p>
			

		<div type="section" n="2">
			<head type="tune">Même air.</head>
			<lg n="1">
				<l n="9" num="1.1" lm="8" met="8"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="9.2">d<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>b<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>ch<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></w> <w n="9.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="9.4" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>sc<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="377" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
				<l n="10" num="1.2" lm="8" met="8"><w n="10.1">N</w>'<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="338" place="1">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="10.4">l<seg phoneme="wa" type="vs" value="1" rule="422" place="4">oi</seg></w> <w n="10.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.6">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="10.7" punct="pe:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>f<seg phoneme="o" type="vs" value="1" rule="317" place="8" punct="pe">au</seg>ts</w> !</l>
				<l n="11" num="1.3" lm="8" met="8"><w n="11.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="11.2">s<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="11.3">j<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>s</w> <w n="11.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="11.5">r<seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="11.7" punct="vg:8">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
				<l n="12" num="1.4" lm="8" met="8"><w n="12.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.4">R<seg phoneme="wa" type="vs" value="1" rule="422" place="5">oi</seg></w> <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="12.6" punct="vg:8">r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>b<seg phoneme="o" type="vs" value="1" rule="317" place="8" punct="vg">au</seg>ds</w>,</l>
				<l n="13" num="1.5" lm="8" met="8"><w n="13.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="13.2">l</w>'<w n="13.3"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="345" place="3">e</seg>l</w> <w n="13.4" punct="vg:4">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4" punct="vg">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="13.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="13.6"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t</w> <w n="13.7">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
				<l n="14" num="1.6" lm="8" met="8"><w n="14.1"><seg phoneme="y" type="vs" value="1" rule="452" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">f<seg phoneme="a" type="vs" value="1" rule="192" place="3">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="14.4">lu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="14.5" punct="ps:8">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="ps">ai</seg>t</w>…</l>
			</lg>
			<lg n="2">
			<head type="speaker">DESOEILLETS, bas à Henri.</head>
			<p>
				Bravo ! c'est à vous.
			</p>
				<l n="15" num="2.1" lm="8" met="8"><w n="15.1">S</w>'<w n="15.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>l</w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="15.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="15.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="15.7" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
				<l n="16" num="2.2" lm="8" met="8"><w n="16.1">P<seg phoneme="ø" type="vs" value="1" rule="397" place="1">eu</seg>t</w>-<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="16.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="16.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.5" punct="pt:8">c<seg phoneme="o" type="vs" value="1" rule="434" place="5">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pt">ai</seg>t</w>.</l>
			</lg> 
		</div>
</div></body></text></TEI>