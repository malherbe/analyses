<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE PREMIÈRE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC1" modus="cm" lm_max="10" metProfile="4+6">
				<head type="tune">AIR : Walse de Robin.</head>
					<lg n="1">
						<head type="main">CLINTON.</head>
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="P">À</seg></w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="1.3" punct="vg:4">h<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">â</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="1.4" punct="vg:6">M<seg phoneme="e" type="vs" value="1" rule="352" place="5" mp="M">e</seg>ssi<seg phoneme="ø" type="vs" value="1" rule="396" place="6" punct="vg">eu</seg>rs</w>, <w n="1.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="C">ou</seg>s</w> <w n="1.7" punct="vg:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9" mp="M">in</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1" mp="C">Un</seg></w> <w n="2.2">d<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>x</w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>r</w><caesura></caesura> <w n="2.4">d<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>t</w> <w n="2.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="C">ou</seg>s</w> <w n="2.6">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pv">i</seg>r</w> ;</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="3.3">B<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3" mp="M">o</seg>st<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" caesura="1">on</seg></w><caesura></caesura> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="3.5">b<seg phoneme="o" type="vs" value="1" rule="314" place="6" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>s</w> <w n="3.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="3.7">l</w>'<w n="3.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg></w> <w n="3.9" punct="vg:10">c<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2" punct="vg:2">s<seg phoneme="wa" type="vs" value="1" rule="419" place="2" punct="vg">oi</seg>r</w>, <w n="4.3">ch<seg phoneme="e" type="vs" value="1" rule="346" place="3" mp="P">ez</seg></w> <w n="4.4" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="422" place="4" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="4.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="5" mp="M">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>t</w> <w n="4.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="4.7" punct="pt:10">r<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg><seg phoneme="y" type="vs" value="1" rule="452" place="9" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pt">i</seg>r</w>.</l>
						<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">R<seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>st<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="5.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4" caesura="1">en</seg></w><caesura></caesura> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="P">à</seg></w> <w n="5.4">l</w>'<w n="5.5"><seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="M">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="351" place="7">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="5.6">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>rf<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="6.2">l<seg phoneme="œ" type="vs" value="1" rule="406" place="2" mp="C">eu</seg>rs</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">ai</seg>ts</w><caesura></caesura> <w n="6.4">v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="6.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6" mp="M">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r</w> <w n="6.6">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="6.7" punct="dp:10">c<seg phoneme="œ" type="vs" value="1" rule="248" place="10" punct="dp">œu</seg>r</w> :</l>
						<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="7.2">d</w>'<w n="7.3"><seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="7.4">f<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>s</w><caesura></caesura> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.6">p<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="M">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>r</w> <w n="7.7">d</w>'<w n="7.8"><seg phoneme="y" type="vs" value="1" rule="452" place="8">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.9"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">A</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="8.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2" mp="P">an</seg>s</w> <w n="8.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="8.4">f<seg phoneme="ɛ" type="vs" value="1" rule="63" place="4" caesura="1">e</seg>rs</w><caesura></caesura> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="6" mp="M">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg></w> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="8.7" punct="pt:10">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="9" mp="M">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="pt">eu</seg>r</w>.</l>
					</lg>
					<lg n="2">
						<head type="main">ENSEMBLE.</head>
						<l n="9" num="2.1" lm="10" met="4+6"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="P">À</seg></w> <w n="9.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="9.3" punct="vg:4">h<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">â</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <subst hand="LG" reason="analysis" type="repetition"><del>etc.</del><add rend="hidden"><w n="9.4" punct="vg:6">M<seg phoneme="e" type="vs" value="1" rule="352" place="5" mp="M">e</seg>ssi<seg phoneme="ø" type="vs" value="1" rule="396" place="6" punct="vg">eu</seg>rs</w>, <w n="9.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="9.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="C">ou</seg>s</w> <w n="9.7" punct="vg:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9" mp="M">in</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</add></subst></l>
						<l n="10" num="2.2" lm="10" met="4+6"><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1" mp="C">Un</seg></w> <w n="10.2">d<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>x</w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>r</w><caesura></caesura> <w n="10.4">d<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>t</w> <w n="10.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="C">ou</seg>s</w> <w n="10.6">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pv">i</seg>r</w> ; </add></subst></l>
						<l n="11" num="2.3" lm="10" met="4+6"><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="11.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="11.3">B<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3" mp="M">o</seg>st<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" caesura="1">on</seg></w><caesura></caesura> <w n="11.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="11.5">b<seg phoneme="o" type="vs" value="1" rule="314" place="6" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>s</w> <w n="11.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="11.7">l</w>'<w n="11.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg></w> <w n="11.9" punct="vg:10">c<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>, </add></subst></l>
						<l n="12" num="2.4" lm="10" met="4+6"><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="12.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="12.2" punct="vg:2">s<seg phoneme="wa" type="vs" value="1" rule="419" place="2" punct="vg">oi</seg>r</w>, <w n="12.3">ch<seg phoneme="e" type="vs" value="1" rule="346" place="3" mp="P">ez</seg></w> <w n="12.4" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="422" place="4" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="12.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="5" mp="M">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>t</w> <w n="12.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="12.7" punct="pt:10">r<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg><seg phoneme="y" type="vs" value="1" rule="452" place="9" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pt">i</seg>r</w>. </add></subst></l>
						<l n="13" num="2.5" lm="10" met="4+6"><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="13.1">R<seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>st<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="13.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4" caesura="1">en</seg></w><caesura></caesura> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="P">à</seg></w> <w n="13.4">l</w>'<w n="13.5"><seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="M">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="351" place="7">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="13.6">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>rf<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></add></subst></l>
						<l n="14" num="2.6" lm="10" met="4+6"><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="14.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="14.2">l<seg phoneme="œ" type="vs" value="1" rule="406" place="2" mp="C">eu</seg>rs</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">ai</seg>ts</w><caesura></caesura> <w n="14.4">v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="14.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6" mp="M">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r</w> <w n="14.6">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="14.7" punct="dp:10">c<seg phoneme="œ" type="vs" value="1" rule="248" place="10" punct="dp">œu</seg>r</w> : </add></subst></l>
						<l n="15" num="2.7" lm="10" met="4+6"><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="15.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="15.2">d</w>'<w n="15.3"><seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="15.4">f<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>s</w><caesura></caesura> <w n="15.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="15.6">p<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="M">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>r</w> <w n="15.7">d</w>'<w n="15.8"><seg phoneme="y" type="vs" value="1" rule="452" place="8">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.9"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">A</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></add></subst></l>
						<l n="16" num="2.8" lm="10" met="4+6"><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="16.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2" mp="P">an</seg>s</w> <w n="16.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="16.4">f<seg phoneme="ɛ" type="vs" value="1" rule="63" place="4" caesura="1">e</seg>rs</w><caesura></caesura> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="6" mp="M">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg></w> <w n="16.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="16.7" punct="pt:10">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="9" mp="M">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="pt">eu</seg>r</w>. </add></subst></l>
					</lg>
					<lg n="3">
						<head type="main">TOUS.</head>
						<l n="17" num="3.1" lm="10" met="4+6"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="P">À</seg></w> <w n="17.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="17.3">h<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">â</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="17.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="17.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>t</w> <w n="17.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="C">ou</seg>s</w> <w n="17.7" punct="vg:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9" mp="M">in</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="18" num="3.2" lm="10" met="4+6"><w n="18.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1" mp="C">Un</seg></w> <w n="18.2">d<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>x</w> <w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>r</w><caesura></caesura> <w n="18.4">d<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>t</w> <w n="18.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="C">ou</seg>s</w> <w n="18.6">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="vg">i</seg>r</w>,</l>
						<l n="19" num="3.3" lm="10" met="4+6"><w n="19.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="19.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="19.3">B<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3" mp="M">o</seg>st<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" caesura="1">on</seg></w><caesura></caesura> <w n="19.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="19.5">b<seg phoneme="o" type="vs" value="1" rule="314" place="6" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>s</w> <w n="19.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="19.7">l</w>'<w n="19.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg></w> <w n="19.9" punct="vg:10">c<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="20" num="3.4" lm="10" met="4+6"><w n="20.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="20.2" punct="vg:2">s<seg phoneme="wa" type="vs" value="1" rule="419" place="2" punct="vg">oi</seg>r</w>, <w n="20.3">ch<seg phoneme="e" type="vs" value="1" rule="346" place="3" mp="P">ez</seg></w> <w n="20.4" punct="vg:4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="20.5">vi<seg phoneme="ɛ" type="vs" value="1" rule="365" place="5">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" mp="F">e</seg>nt</w> <w n="20.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="20.7" punct="pt:10">r<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg><seg phoneme="y" type="vs" value="1" rule="452" place="9" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pt">i</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>