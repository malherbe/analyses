<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE X.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC27" modus="cp" lm_max="10" metProfile="8, 4+6">
					<head type="tune">AIR de la Vieille.</head>
						<lg n="1">
							<head type="main">ARWED, lui remettant un anneau.</head>
							<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="1.1">V<seg phoneme="wa" type="vs" value="1" rule="419" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.3">g<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="351" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="2.4">j<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="2.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="2.6" punct="pv:8"><seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pv">i</seg>r</w> ;</l>
							<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="3.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="3.2">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>nn<seg phoneme="o" type="vs" value="1" rule="314" place="4">eau</seg></w> <w n="3.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6">lu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg></w> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="4.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="4.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="4.5" punct="pt:8">s<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pt">i</seg>r</w>.</l>
							<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="4"></space><w n="5.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>s</w> ! <w n="5.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3">g<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="351" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="4"></space><w n="6.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="6.3">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="6.4">qu</w>'<w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="6.6" punct="pe:8">s<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pe">i</seg>r</w> ! <del hand="LG" reason="analysis" type="repetition">(Bis.)</del></l>
							<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="4"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="7.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="7.3">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="7.4">qu</w>'<w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="7.6" punct="pe:8">s<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pe">i</seg>r</w> !</add></subst></l>
							<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="C">a</seg></w> <w n="8.4" punct="ps:4">v<seg phoneme="wa" type="vs" value="1" rule="419" place="4" punct="ps" caesura="1">oi</seg>s</w> …<caesura></caesura> <w n="8.5">c</w>'<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="8.8">qu<seg phoneme="i" type="vs" value="1" rule="490" place="8">i</seg></w> <w n="8.9">s</w>'<w n="8.10" punct="ps:10"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg></w>…</l>
							<l n="9" num="1.9" lm="10" met="4+6"><w n="9.1" punct="pe:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="397" place="2" punct="pe">eu</seg></w> ! <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.3">d<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>s</w><caesura></caesura> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="9.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="9.6" punct="vg:10">pr<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
							<l n="10" num="1.10" lm="10" met="4+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M/mp">É</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M/mp">a</seg>rgn<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="Lp">on</seg>s</w>-<w n="10.2">lu<seg phoneme="i" type="vs" value="1" rule="490" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="10.3">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="5" mp="C">e</seg>t</w> <w n="10.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6" mp="M">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t</w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="10.6" punct="pe:10">s<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></w> !</l>
							</lg>
						<lg n="2">
							<head type="main">LAD.</head>
							<l n="11" num="2.1" lm="10" met="4+6"><w n="11.1" punct="pe:2">P<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="pe">ez</seg></w> ! <w n="11.2" punct="pe:4">p<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="pe" caesura="1">ez</seg></w> !<caesura></caesura> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="11.4">v<seg phoneme="wa" type="vs" value="1" rule="419" place="6" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="7">à</seg></w> <w n="11.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="8">i</seg></w> <w n="11.6">s</w>'<w n="11.7" punct="pe:10"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></w> !</l>
						</lg>
						<lg n="3">
							<head type="main">ARWED.</head>
							<l n="12" num="3.1" lm="10" met="4+6"><w n="12.1">R<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="12.3" punct="vg:4">gl<seg phoneme="wa" type="vs" value="1" rule="419" place="4" punct="vg" caesura="1">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5" mp="M">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="12.5">d</w>'<w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>r</w></l>
							<l n="13" num="3.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="13.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="13.2">m</w>'<w n="13.3"><seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="13.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="13.6" punct="pe:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pe">ou</seg>r</w> !</l>
							<l n="14" num="3.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="14.3" punct="vg:3">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" punct="vg">e</seg>rds</w>, <w n="14.4" punct="pe:5">h<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="pe">a</seg>s</w> ! <w n="14.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="14.6" punct="pe:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pe">ou</seg>r</w> !</l>
						</lg>
						<lg n="4">
							<head type="main">POLWARTH, LADY.</head>
							<l n="15" num="4.1" lm="10" met="4+6"><w n="15.1">R<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="15.3" punct="vg:4">gl<seg phoneme="wa" type="vs" value="1" rule="419" place="4" punct="vg" caesura="1">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5" mp="M">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="15.5">d</w>'<w n="15.6"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>r</w></l>
							<l n="16" num="4.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="16.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="16.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="16.6" punct="pe:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pe">ou</seg>r</w> !</l>
							<l n="17" num="4.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="17.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="17.3" punct="vg:3">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" punct="vg">e</seg>rd</w>, <w n="17.4" punct="pe:5">h<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="pe">a</seg>s</w> ! <w n="17.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="17.6" punct="pe:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pe">ou</seg>r</w> !</l>
						</lg>
					</div></body></text></TEI>