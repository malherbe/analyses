<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE III.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC16" modus="sm" lm_max="8" metProfile="8">
					<head type="tune">AIR du Rêve du Soldat.</head>
						<lg n="1">
							<head type="main">ANNA.</head>
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w>-<w n="1.2">lu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="1.3">g<seg phoneme="u" type="vs" value="1" rule="424" place="4">oû</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="1.5">s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">c<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>p<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>r</w> <w n="2.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="2.5" punct="pv:8">s<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="381" place="8" punct="pv">e</seg>il</w> ;</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1">P<seg phoneme="ø" type="vs" value="1" rule="397" place="1">eu</seg>t</w>-<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="3.4">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.5">l</w>'<w n="3.6" punct="ps:8"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w> …</l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r</w> <w n="4.3">l</w>'<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>d</w> <w n="4.5"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg></w> <w n="4.6" punct="pt:8">r<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381" place="8" punct="pt">e</seg>il</w>.</l>
							<l n="5" num="1.5" lm="8" met="8"><w n="5.1">Gr<seg phoneme="a" type="vs" value="1" rule="339" place="1">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="5.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.4">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>t</w> <w n="5.5" punct="vg:8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="6.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>t</w> <w n="6.4" punct="ps:5">h<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397" place="5" punct="ps">eu</seg>x</w> … <w n="6.5" punct="vg:6">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" punct="vg">ai</seg>s</w>, <w n="6.6" punct="pe:8">h<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pe">a</seg>s</w> !</l>
							<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="7.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">p<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>t</w> <w n="7.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="7.5">l</w>'<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.7">qu</w>'<w n="7.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="7.9" punct="pt:8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
							<l n="8" num="1.8" lm="8" met="8"><w n="8.1">T<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w>-<w n="8.2" punct="vg:3">n<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="vg">ou</seg>s</w>, <w n="8.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">l</w>'<w n="8.5"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>v<seg phoneme="e" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>s</w> <w n="8.6" punct="pe:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pe">a</seg>s</w> !</l>
						</lg>
					</div></body></text></TEI>