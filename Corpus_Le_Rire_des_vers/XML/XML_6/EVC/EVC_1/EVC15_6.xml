<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ARWED, OU LES REPRÉSAILLES</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="ETI" sort="1">
					<name>
						<forename>Charles-Guillaume</forename>
						<surname>ÉTIENNE</surname>
					</name>
					<date from="1777" to="1845">1777-1845</date>
				</author>
				<author key="VRN" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>VOIRIN</surname>
						<addName type="pen_name">VARIN</addName>
					</name>
					<date from="1798" to="1869">1798-1869</date>
				</author>
				<author key="DVR" sort="3">
					<name>
						<forename>Lucien</forename>
						<surname>DESVERGERS</surname>
					</name>
					<date from="1794" to="1851">1794-1851</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EVC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ARWED, OU LES REPRÉSAILLES</title>
						<author>ÉTIENNE, VARIN ET DESVERGERS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=g1doAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ARWED, OU LES REPRÉSAILLES,</title>
							<author>ÉTIENNE, VARIN ET DESVERGERS</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849613.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Seules les parties versifiées du texte ont été encodées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<p>
					L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.
				</p>
			<normalization>
				<p>
					La ponctuation a été normalisée (espace devant un signe de ponctuation double).
				</p>
				<p>
					Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).
				</p>
				<p>
					Les majuscules accentuées ont été restituées.
				</p>
				<p>
					Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
				</p>
				<p>
					Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.
				</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIÈME.</head><head type="main_subpart">SCÈNE PREMIÈRE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="EVC15" modus="cm" lm_max="10" metProfile="4+6">
					<head type="tune">AIR du Château perdu.</head>
						<lg n="1">
							<head type="main">ARWED.</head>
							<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="1.2">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3" punct="vg:4">j<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="1.5">j<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="1.8">f<seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="481" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
							<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">D</w>'<w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="2.3">d<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>x</w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>r</w><caesura></caesura> <w n="2.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="C">ou</seg>s</w> <w n="2.6">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>nt</w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="8">en</seg></w> <w n="2.8" punct="pt:10">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" punct="pt">an</seg>t</w>.</l>
							<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">Gr<seg phoneme="a" type="vs" value="1" rule="339" place="1">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="P">à</seg></w> <w n="3.3">l</w>'<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406" place="4" caesura="1">eu</seg>r</w><caesura></caesura> <w n="3.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="3.6"><seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="C">i</seg>ls</w> <w n="3.7">ch<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="3.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="3.9" punct="vg:10">v<seg phoneme="i" type="vs" value="1" rule="481" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
							<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="4.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.3">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>t</w> <w n="4.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4" caesura="1">u</seg>s</w><caesura></caesura> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="4.6">b<seg phoneme="o" type="vs" value="1" rule="443" place="6" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r</w> <w n="4.7" punct="pt:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" mp="M">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="M">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" punct="pt">an</seg>t</w>.</l>
							<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="5.2">v<seg phoneme="ɛ" type="vs" value="1" rule="304" place="2" mp="M">ai</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="5.4">m<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.5" punct="vg:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" mp="M">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
							<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="P">u</seg>r</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="6.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6" mp="P">à</seg></w> <w n="6.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="6.7" punct="pv:10"><seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>rd<seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pv">i</seg>r</w> ;</l>
							<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1" punct="vg:2">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="1" mp="M">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="2" punct="vg">ô</seg>t</w>, <w n="7.2" punct="pe:4">h<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="pe" caesura="1">a</seg>s</w> !<caesura></caesura> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.4">pr<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="7">en</seg>t</w> <w n="7.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="C">ou</seg>s</w> <w n="7.6">r<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
							<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="8.3">ch<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" caesura="1">in</seg>s</w><caesura></caesura> <w n="8.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="8.5"><seg phoneme="o" type="vs" value="1" rule="317" place="6" mp="M">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="7">è</seg>s</w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="449" place="8" mp="C">u</seg></w> <w n="8.7" punct="pt:10">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pt">i</seg>r</w>.</l>
						</lg>
					</div></body></text></TEI>