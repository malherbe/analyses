<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DUV" sort="1">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>Duvert</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<author key="SAI" sort="2">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>Boniface</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="ARA" sort="3">
					<name>
						<forename>Étienne</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1802" to="1892">1802-1892</date>
				</author>
				<respStmt>
					<resp>Correction de l'OCR, encodage en XML</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp> Application des programmes de traitement automatique</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>570 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname> Le Rire des vers</orgname>
					<address>
						<addrLine>University of Basel</addrLine>
					</address>
					<email></email>
					<ref type="URL">https://slw-comicverse.dslw.unibas.ch/index.php?lang=fr</ref>
				</publisher>
				<pubPlace>Bâle</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CAGOTISME ET LIBERTÉ OU LES DEUX SEMESTRES</title>
						<author>Duvert, Ernest et Étienne</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=8jVMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository> Österreichische Nationalbibliothek</repository>
								<idno type="URL">http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160886905</idno>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>
					Les parties versifiées ont été prioritairement balisées.
				</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>
						La ponctuation a été normalisée.
					</p>
					<p>
						Les accents sur les majuscules ont été restitués, ainsi que les o-e liés (œ).
					</p>
					<p>
						Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DSE28" modus="sp" lm_max="8" metProfile="8, 7"> 
			<head type="main">LA LIBERTÉ POLITIQUE.</head>

			<head type="tune">AIR : Merveilleuse dans ses vertus.</head>

		 <lg n="1"> <l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:2">P<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w> <w n="1.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="1.4" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg>rd</w>,</l>
			<l n="2" num="1.2" lm="8" met="8"><w n="2.1">R<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>ll<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w>-<w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="2.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="2.5" punct="dp:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>nni<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
			<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="3.2">j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r</w> <w n="3.3">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="3.4">l</w>’<w n="3.5"><seg phoneme="ø" type="vs" value="1" rule="404" place="5">Eu</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="442" place="6">o</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
			<l n="4" num="1.4" lm="8" met="8"><w n="4.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="346" place="2">er</seg></w> <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>d<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pe pt pt">a</seg>rd</w> ! ..</l></lg>.

			<p>(au Belge.)</p>
			<lg n="2"><l n="5" num="2.1" lm="7" met="7"><w n="5.1">D<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="5.3">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.4">B<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>lg<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
			<l n="6" num="2.2" lm="7" met="7"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="6.2">v<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="6.3">s</w>’<w n="6.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>r</w> <w n="6.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="6.6" punct="pt:7">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7" punct="pt pt pt">o</seg>rt</w> ...</l>
			<p>(au Polonais.) </p>
			<l n="7" num="2.3" lm="7" met="7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2" punct="vg:2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2" punct="vg">ou</seg>s</w>, <w n="7.3">d</w>’<w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="7.5">j<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>g</w> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>sp<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
			<l n="8" num="2.4" lm="7" met="7"><w n="8.1" punct="vg:3">Tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>ph<seg phoneme="e" type="vs" value="1" rule="346" place="3" punct="vg">ez</seg></w>, <w n="8.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s</w> <w n="8.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="8.4" punct="pe:7">N<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7" punct="pe">o</seg>rd</w> !</l></lg>

			<lg n="3"><l n="9" num="3.1" lm="8" met="8"><w n="9.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="2" punct="vg">ou</seg>t</w>, <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="9.5" punct="vg:8">c<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="vg">on</seg></w>,</l>
			<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="10.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="10.5" punct="vg:8">H<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>lv<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>t<seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
			<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="11.6">R<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>ss<seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
			<l n="12" num="3.4" lm="8" met="8"><w n="12.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>t</w> <w n="12.2">b<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>s</w> <w n="12.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>t</w> <w n="12.4">r<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>p<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="12.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="12.6" punct="pe:8">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="8" punct="pe pt pt pt">om</seg></w> ! ...</l></lg>

			<lg n="4"><l n="13" num="4.1" lm="7" met="7"><w n="13.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="13.2">v<seg phoneme="ɛ" type="vs" value="1" rule="63" place="2">e</seg>rs</w> <w n="13.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">p<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.5">d</w>’<w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</w></l>
			<l n="14" num="4.2" lm="7" met="7"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg></w> <w n="14.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="5">ô</seg>t</w> <w n="14.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="14.5" punct="pv:7">v<seg phoneme="ɔ" type="vs" value="1" rule="442" place="7" punct="pv">o</seg>l</w> ;</l>
			<l n="15" num="4.3" lm="7" met="7"><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="15.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.4">n<seg phoneme="o" type="vs" value="1" rule="437" place="5">o</seg>s</w> <w n="15.5">vi<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</w> <w n="15.6">br<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</w></l>
			<l n="16" num="4.4" lm="7" met="7"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="16.2">f<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>s<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg></w> <w n="16.3">l<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>r</w> <w n="16.4" punct="pt:7">s<seg phoneme="ɔ" type="vs" value="1" rule="442" place="7" punct="pt pt pt">o</seg>l</w> ...</l></lg>

			<lg n="5"><l n="17" num="5.1" lm="8" met="8"><w n="17.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">E</seg>sp<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="5">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="6">ô</seg>t</w> <w n="17.4">j</w>’<w n="17.5" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8" punct="vg">ai</seg></w>,</l>
			<l n="18" num="5.2" lm="8" met="8"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="18.2">f<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>x</w> <w n="18.4">pi<seg phoneme="e" type="vs" value="1" rule="240" place="5">e</seg>ds</w> <w n="18.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="18.6" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
			<l n="19" num="5.3" lm="8" met="8"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="19.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r</w> <w n="19.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="19.5">l<seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg>s</w> <w n="19.6">c<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
			<l n="20" num="5.4" lm="8" met="8"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r</w> <w n="20.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="20.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="20.4" punct="pt:8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>r<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pt">é</seg></w>.</l></lg>

			<lg n="6"><l n="21" num="6.1" lm="7" met="7"><w n="21.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="21.2">l</w>’<w n="21.3"><seg phoneme="i" type="vs" value="1" rule="467" place="2">I</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>l<seg phoneme="i" type="vs" value="1" rule="481" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="21.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w>-<w n="21.5" punct="vg:7">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
			<l n="22" num="6.2" lm="7" met="7"><w n="22.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="22.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="22.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>ci<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="6">en</seg>s</w> <w n="22.4" punct="vg:7">di<seg phoneme="ø" type="vs" value="1" rule="397" place="7" punct="vg">eu</seg>x</w>,</l>
			<l n="23" num="6.3" lm="7" met="7"><w n="23.1">P<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="23.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="23.3">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="409" place="7">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
			<l n="24" num="6.4" lm="7" met="7"><w n="24.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="24.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="24.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>t</w> <w n="24.4" punct="pe:7">v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ct<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="ø" type="vs" value="1" rule="397" place="7" punct="pe">eu</seg>x</w> !</l></lg>

			<lg n="7"><l n="25" num="7.1" lm="8" met="8"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="25.2">N<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="25.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="25.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>d</w> <w n="25.5"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>rd</w>’<w n="25.6">hu<seg phoneme="i" type="vs" value="1" rule="490" place="8">i</seg></w></l>
			<l n="26" num="7.2" lm="8" met="8"><w n="26.1">M<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>r</w> <w n="26.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="26.3">V<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>s<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="26.5" punct="pv:8">c<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
			<l n="27" num="7.3" lm="8" met="8"><w n="27.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="27.2" punct="vg:2">l<seg phoneme="a" type="vs" value="1" rule="341" place="2" punct="vg">à</seg></w>, <w n="27.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="27.4">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>lc<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg></w> <w n="27.5">p<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
			<l n="28" num="7.4" lm="8" met="8"><w n="28.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="28.2"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="28.3"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t</w> <w n="28.4" punct="pt:8">lu<seg phoneme="i" type="vs" value="1" rule="490" place="8" punct="pt pt pt">i</seg></w> ...</l></lg>
			
		   <lg n="8"> <l n="29" num="8.1" lm="7" met="7"><w n="29.1">Br<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="29.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="29.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="4">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="29.4" punct="vg:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>tr<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</w>,</l>
			<l n="30" num="8.2" lm="7" met="7"><w n="30.1">P<seg phoneme="ø" type="vs" value="1" rule="397" place="1">eu</seg>t</w>-<w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="30.4">j<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r</w> <w n="30.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="30.6">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>lc<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg></w></l>
			<l n="31" num="8.3" lm="7" met="7"><w n="31.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="31.2">r<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="31.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="31.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</w></l>
			<l n="32" num="8.4" lm="7" met="7"><w n="32.1">J<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>squ</w>’<w n="32.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg></w> <w n="32.3">pi<seg phoneme="e" type="vs" value="1" rule="240" place="3">e</seg>d</w> <w n="32.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="32.5" punct="pt:7">V<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7" punct="pt">an</seg></w>.</l></lg>
			
			<lg n="9"><l n="33" num="9.1" lm="8" met="8"><w n="33.1">Pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.2"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="33.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg></w> <w n="33.4" punct="vg:8">c<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="439" place="7">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="362" place="8" punct="vg">en</seg></w>,</l>
			<l n="34" num="9.2" lm="8" met="8"><w n="34.1" punct="vg:2">D<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341" place="2" punct="vg">à</seg></w>, <w n="34.2">d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t</w>-<w n="34.3" punct="vg:4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg">on</seg></w>, <w n="34.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="34.5">ch<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="34.6" punct="vg:8">r<seg phoneme="y" type="vs" value="1" rule="456" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
			<l n="35" num="9.3" lm="8" met="8"><w n="35.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="35.2" punct="vg:3">M<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3" punct="vg">an</seg></w>, <w n="35.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="35.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="35.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="y" type="vs" value="1" rule="456" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
			<l n="36" num="9.4" lm="7" met="7"><w n="36.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="36.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="36.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="36.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="36.5">l</w>’<w n="36.6" punct="pe:7"><seg phoneme="o" type="vs" value="1" rule="317" place="5">Au</seg>tr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>chi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="7" punct="pe">en</seg></w> !</l></lg>

		   <lg n="10"> <l n="37" num="10.1" lm="7" met="7"><w n="37.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="37.2">l<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></w> <w n="37.3" punct="vg:7">pr<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ctr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
			<l n="38" num="10.2" lm="7" met="7"><w n="38.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="38.2">l</w>’<w n="38.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>br<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="38.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="38.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="38.6" punct="vg:7">l<seg phoneme="wa" type="vs" value="1" rule="419" place="7" punct="vg">oi</seg>s</w>,</l>
			<l n="39" num="10.3" lm="7" met="7"><w n="39.1">D<seg phoneme="wa" type="vs" value="1" rule="419" place="1">oi</seg>t</w> <w n="39.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r</w> <w n="39.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="39.4">t<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>tr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
			<l n="40" num="10.4" lm="7" met="7"><w n="40.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="40.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="40.3">p<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="40.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="40.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="40.6" punct="pe:7">r<seg phoneme="wa" type="vs" value="1" rule="419" place="7" punct="pe">oi</seg>s</w> !</l></lg>

			<lg n="11"><l n="41" num="11.1" lm="8" met="8"><w n="41.1" punct="vg:2">P<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="41.2"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w> <w n="41.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="41.4" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg>rd</w>,</l>
			<l n="42" num="11.2" lm="8" met="8"><w n="42.1">R<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>ll<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w>-<w n="42.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="42.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="42.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="42.5" punct="pv:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>nni<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
			<l n="43" num="11.3" lm="8" met="8"><w n="43.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="43.2">j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r</w> <w n="43.3">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="43.4">l</w>’<w n="43.5"><seg phoneme="ø" type="vs" value="1" rule="404" place="5">Eu</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="442" place="6">o</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="43.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
		   <l n="44" num="11.4" lm="8" met="8"> <w n="44.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="346" place="2">er</seg></w> <w n="44.2">s<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="44.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="44.4">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="44.5" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>d<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pt">a</seg>rd</w>. <del hand="LN" type="repetition" reason="analysis">(3 fois.)</del></l></lg>
</div></body></text></TEI>