<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB38" modus="sm" lm_max="8" metProfile="8">
	<head type="tune">Air : Un homme pour faire un tableau.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="1.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>rs</w> <w n="1.5">d</w>'<w n="1.6"><seg phoneme="o" type="vs" value="1" rule="443" place="6">O</seg>p<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w></l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="2.2">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3">j<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.4" punct="vg:8">s<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l part="I" n="3" num="1.3" lm="8" met="8"><w n="3.1">N<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>d</w>-<w n="3.3" punct="ps:4">d<seg phoneme="y" type="vs" value="1" rule="449" place="4" punct="ps">u</seg>c</w>… </l>
	</lg>
	<lg n="2">
	<head type="speaker">AUGUSTA.</head>
		<l part="F" n="3" lm="8" met="8"><w n="3.4">Mi<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="3.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6" punct="vg:8">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg></w>,</l>
		<l n="4" num="2.1" lm="8" met="8"><w n="4.1">C</w>'<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.5">p<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>c</w> <w n="4.6">lu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg></w>-<w n="4.7" punct="ps:8">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
		<l n="5" num="2.2" lm="8" met="8"><w n="5.1">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="5.2" punct="vg:3">s<seg phoneme="ɛ" type="vs" value="1" rule="383" place="2">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="406" place="3" punct="vg">eu</seg>r</w>, <w n="5.3">qu</w>'<w n="5.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="5.5">d<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>t</w> <w n="5.6" punct="vg:8">r<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="vg">er</seg></w>,</l>
		<l n="6" num="2.3" lm="8" met="8"><w n="6.1">J<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ff<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="6.4" punct="pv:8">s<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>rpr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
		<l n="7" num="2.4" lm="8" met="8"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="7.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="7.4">s<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="5">en</seg>t</w> <w n="7.5" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="vg">er</seg></w>,</l>
		<l n="8" num="2.5" lm="8" met="8"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="8.2">qu</w>'<w n="8.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="8.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="8.6">j<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s</w> <w n="8.7" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
	</lg>
</div></body></text></TEI>