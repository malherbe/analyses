<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB32" modus="sm" lm_max="8" metProfile="8">
	<head type="tune">AIR : De cet amour vif et soudain ( de CAROLINE).</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="1.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="1.3">t<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rts</w> <w n="1.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t</w> <w n="1.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.7">d<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>ds</w></l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="2.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="2.4">m</w>'<w n="2.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>cc<seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="3.2">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="3.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="3.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>ds</w></l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">P<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="4.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="4.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>r</w> <w n="4.5" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>xc<seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l> 
	</lg>
	<lg n="2">
	<head type="speaker">LE GRAND-DUC, apercevant Henriette.</head>
		<l n="5" num="2.1" lm="8" met="8"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">m</w>'<w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="5.5">s<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s</w> <w n="5.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="8" punct="vg">o</seg>r</w>,</l>
		<l n="6" num="2.2" lm="8" met="8"><w n="6.1" punct="vg:2">T<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="vg">ez</seg></w>, <w n="6.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.5">l</w>'<w n="6.6" punct="pv:8"><seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
		<l n="7" num="2.3" lm="8" met="8"><w n="7.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="2">en</seg>t</w> <w n="7.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.3">r<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="7.5" punct="vg:8">t<seg phoneme="ɔ" type="vs" value="1" rule="438" place="8" punct="vg">o</seg>rt</w>,</l>
		<l n="8" num="2.4" lm="8" met="8"><w n="8.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">l</w>'<w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>xc<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="8.5">s<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="8.6" punct="pi:8">j<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>l<seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l> 
	</lg>
</div></body></text></TEI>