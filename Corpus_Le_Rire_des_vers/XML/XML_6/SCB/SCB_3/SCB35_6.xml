<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <fileDesc>
       <titleStmt>
         <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
         <title type="sub">COMÉDIE-VAUDEVILLE EN DEUX ACTES</title>
         <title type="corpus">Le Rire des vers</title>
         <author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
        </author>
         <author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
        </author>
         <editor>Le Rire des vers, Université de Bâle</editor>
         <editor>
           Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
           <choice>
             <abbr>CRISCO, Université de Caen Normandie</abbr>
             <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
          </choice>
           (EA 4255)
        </editor>
         <respStmt>
           <resp>Encodage en XML (CRISCO, université de Caen)</resp>
           <name id="KL">
             <forename>Kedi</forename>
             <surname>LI</surname>
          </name>
        </respStmt>
         <respStmt>
           <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
           <name id="RR">
             <forename>Richard</forename>
             <surname>RENAULT</surname>
          </name>
        </respStmt>
      </titleStmt>
       <extent>400 vers</extent>
       <publicationStmt>
         <publisher>
           <orgname>
             <choice>
               <abbr>CRISCO, Université de Caen Normandie</abbr>
               <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
            </choice>
          </orgname>
           <address>
             <addrLine>Université de Caen</addrLine>
             <addrLine>14032 CAEN CEDEX</addrLine>
             <addrLine>FRANCE</addrLine>
          </address>
           <email>crisco.incipit@unicaen.fr</email>
           <ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
        </publisher>
         <pubPlace>Caen</pubPlace>
         <date when="2022">2022</date>
         <idno type="local">SCB_3</idno>
         <availability status="free">
					 <licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					 <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
        </availability>
      </publicationStmt>
       <sourceDesc>
         <biblFull>
           <titleStmt>
             <title type="main">LES TROIS MAITRESSES, OU UNE COUR D'ALLEMAGNE</title>
             <author>BAYARD EN SOCIÉTÉ AVEC M. SCRIBE</author>
          </titleStmt>
           <publicationStmt>
             <publisher>Google Books</publisher>
             <idno type="URL">https ://books.google.ch/books ?id=6fssAAAAYAAJ</idno>
          </publicationStmt>
           <sourceDesc>
             <biblStruct>
               <monogr>
                 <repository>Harvard University Library</repository>
                 <idno type="URL">http ://id.lib.harvard.edu/alma/990026763330203941/catalog</idno>
              </monogr>
            </biblStruct>         
          </sourceDesc>
        </biblFull> 
      </sourceDesc>
    </fileDesc>
     <profileDesc>
       <creation>
         <date when="1831">24 JANVIER 1831</date>
         <placeName>
           <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
        </placeName>
      </creation>
    </profileDesc>
     <encodingDesc>
       <projectDesc>
         <p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
      </projectDesc>
       <samplingDecl>
         <p>Les parties versifiées ont été prioritairement encodées.</p>
      </samplingDecl>
       <editorialDecl>
         <normalization>
           <p>Les majuscules accentuées ont été restituées.</p>
           <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
           <p>La ponctuation a été normalisée.</p> 
           <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
           <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
           <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
        </normalization>
      </editorialDecl>
    </encodingDesc>
  </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB35" modus="sp" lm_max="8" metProfile="3, 6, 8">
	<head type="tune">AIR : Garde à vous ( de la FIANCÉE).</head>
		<div type="section" n="1">
			<lg n="1">
				<l n="1" num="1.1" lm="3" met="3"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="1.2" punct="pe:3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="3" punct="pe">oi</seg>r</w> !</l>
				<l n="2" num="1.2" lm="6" met="6"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="2.2" punct="vg:2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="2" punct="vg">eu</seg>t</w>, <w n="2.3" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
				<l n="3" num="1.3" lm="6" met="6"><w n="3.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="346" place="2">er</seg></w> <w n="3.2">s<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>r</w> <w n="3.3">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4" punct="pi:6">z<seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pi">e</seg></w> ?</l>
			</lg>
			<lg n="2">
			<head type="speaker">HENRIETTE.</head>
				<l n="4" num="2.1" lm="6" met="6"><w n="4.1" punct="vg:2">M<seg phoneme="œ" type="vs" value="1" rule="150" place="1">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="2" punct="vg">eu</seg>r</w>, <w n="4.2">c</w>'<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="4.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="4.5" punct="pt:6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="pt">oi</seg>r</w>.</l>
			</lg> 
			<lg n="3">
			<head type="speaker">LE GRAND-DUC.</head>
				<l n="5" num="3.1" lm="6" met="6"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="5.2" punct="vg:3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="3" punct="vg">oi</seg>r</w>, <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="5.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5" punct="pt:6">s<seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="pt">oi</seg>r</w>.</l>
			</lg>
			<lg n="4">
			<head type="speaker">HENRIETTE.</head>
				<l n="6" num="4.1" lm="3" met="3"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="6.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3" punct="pe:3">s<seg phoneme="wa" type="vs" value="1" rule="419" place="3" punct="pe">oi</seg>r</w> !</l>
			</lg>
			<lg n="5">
			<head type="speaker">LE GRAND-DUC.</head>
				<l n="7" num="5.1" lm="6" met="6"><w n="7.1">J</w>'<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="7.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="7.4" punct="vg:4">pr<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="189" place="4" punct="vg">e</seg>ts</w>, <w n="7.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="7.6" punct="pv:6">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></w> ;</l>
				<l n="8" num="5.2" lm="6" met="6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="8.2">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="8.4">f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
				<l n="9" num="5.3" lm="6" met="6"><w n="9.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="9.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="9.4" punct="pt:6">s<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="pt">oi</seg>r</w>.</l>
				<l n="10" num="5.4" lm="3" met="3"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="10.2" punct="pt:3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="3" punct="pt">oi</seg>r</w>.</l>
			</lg>
			<lg n="6">
			<head type="speaker">HENRIETTE.</head>
				<l n="11" num="6.1" lm="3" met="3"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="11.2" punct="pt:3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="3" punct="pt">oi</seg>r</w>.</l>
			</lg>
		</div>
		<div type="section" n="2">
			<head type="main">ENSEMBLE.</head>
			<lg n="1">
			<head type="speaker">HENRIETTE.</head>
				<l n="12" num="1.1" lm="3" met="3"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="12.2" punct="vg:3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="3" punct="vg">oi</seg>r</w>,</l>
				<l n="13" num="1.2" lm="3" met="3"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="13.2" punct="vg:3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="3" punct="vg">oi</seg>r</w>,</l>
				<l n="14" num="1.3" lm="3" met="3"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="14.2" punct="pt:3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="3" punct="pt">oi</seg>r</w>.</l> 
			</lg>
			<lg n="2">
			<head type="speaker">LE GRAND-DUC.</head>
				<l n="15" num="2.1" lm="6" met="6"><w n="15.1">J</w>'<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="15.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="15.4" punct="vg:4">pr<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="189" place="4" punct="vg">e</seg>ts</w>, <w n="15.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="15.6" punct="vg:6">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
				<l n="16" num="2.2" lm="6" met="6"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="16.2">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="16.4">f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
				<l n="17" num="2.3" lm="6" met="6"><w n="17.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="17.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="17.4" punct="pt:6">s<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="pt">oi</seg>r</w>.</l>
				<l n="18" num="2.4" lm="3" met="3"><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="18.2" punct="pt:3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="3" punct="pt">oi</seg>r</w>.</l>
			</lg>
			<lg n="3">
			<head type="speaker">LE SURINTENDANT, à part.</head>
				<l n="19" num="3.1" lm="8" met="8"><w n="19.1">S<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>rv<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="19.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">in</seg>tr<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="19.4" punct="pv:8">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
				<l n="20" num="3.2" lm="8" met="8"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="20.3">pr<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="189" place="4">e</seg>ts</w> <w n="20.4">qu</w>'<w n="20.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="20.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="20.7">s<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>r</w> <w n="20.8" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
				<l n="21" num="3.3" lm="6" met="6"><w n="21.1">V<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="21.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>bl<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="21.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="21.4" punct="pt:6"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="pt">oi</seg>r</w>.</l>
				<stage>( Haut.)</stage>
				<l n="22" num="3.4" lm="3" met="3"><w n="22.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="22.2" punct="pe:3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="3" punct="pe">oi</seg>r</w> !</l>
			</lg> 
		</div>
</div></body></text></TEI>