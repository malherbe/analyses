<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <fileDesc>
   <titleStmt>
	<title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
	<title type="corpus">Le Rire des vers</title>
	<author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
	</author>
	<author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
	</author>
	<editor>Le Rire des vers, Université de Bâle</editor>
	<editor>
	 Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
	 <choice>
	  <abbr>CRISCO, Université de Caen Normandie</abbr>
	  <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	 </choice>
	 (EA 4255)
	</editor>
	<respStmt>
	 <resp>Encodage en XML (CRISCO, université de Caen)</resp>
	 <name id="KL">
	  <forename>Kedi</forename>
	  <surname>LI</surname>
	 </name>
	</respStmt>
	<respStmt>
	 <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
	 <name id="RR">
	  <forename>Richard</forename>
	  <surname>RENAULT</surname>
	 </name>
	</respStmt>
   </titleStmt>
   <extent>140 vers</extent>
   <publicationStmt>
	<publisher>
	 <orgname>
	  <choice>
	   <abbr>CRISCO, Université de Caen Normandie</abbr>
	   <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	  </choice>
	 </orgname>
	 <address>
	  <addrLine>Université de Caen</addrLine>
	  <addrLine>14032 CAEN CEDEX</addrLine>
	  <addrLine>FRANCE</addrLine>
	 </address>
	 <email>crisco.incipit@unicaen.fr</email>
	 <ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
	</publisher>
	<pubPlace>Caen</pubPlace>
	<date when="2022">2022</date>
	<idno type="local">SCB_2</idno>	
	<availability status="free">
		<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
		<p>
			Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
			CC = Licence Creative Commons
			BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
			sur les sources et sur les modifications introduites.
			NC = Pas d’utilisation commerciale.
			SA = Partage dans les mêmes conditions.
		</p>
	</availability>
   </publicationStmt>
   <sourceDesc>
	<biblFull>
	 <titleStmt>
	  <title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	  <author>SCRIBE ET BAYARD</author>
	 </titleStmt>
	 <publicationStmt>
	  <publisher>INTERNET ARCHIVE</publisher>
	  <idno type="URL">https://archive.org/details/camillaoulasoeur00scriuoft</idno>
	 </publicationStmt>
	 <sourceDesc>
	  <biblStruct>
	   <monogr>
		<repository>University of Toronto Libraries</repository>
		<idno type="URL">https://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991106543695706196</idno>
	   </monogr>
	  </biblStruct>                 
	 </sourceDesc>
	</biblFull> 
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <creation>
	<date when="1832">12 DÉCEMBRE 1832</date>
	<placeName>
	 <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
	</placeName>
   </creation>
  </profileDesc>
  <encodingDesc>
   <projectDesc>
	<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
   </projectDesc>
   <samplingDecl>
	<p>Les parties versifiées ont été prioritairement encodées.</p>
   </samplingDecl>
   <editorialDecl>
	<normalization>
	 <p>Les majuscules accentuées ont été restituées.</p>
	 <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
	 <p>La ponctuation a été normalisée.</p> 
	 <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
	 <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
	 <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
	</normalization>
   </editorialDecl>
  </encodingDesc>
 </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB17" modus="sm" lm_max="8" metProfile="8">
	<head type="tune">Air du vaudeville de la Petite Sœur.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="1.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="422" place="2" punct="vg">oi</seg></w>, <w n="1.3">s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="1.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.6">tr<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w> <w n="1.7" punct="vg:8">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="vg">on</seg></w>,</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="2.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="2.3" punct="vg:4"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg></w>, <w n="2.4" punct="vg:8">m<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>sd<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">F<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="3.3" punct="vg:4">c<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg">ou</seg>r</w>, <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="3.5" punct="ps:8">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg>s</w>…</l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">m<seg phoneme="wa" type="vs" value="1" rule="422" place="2">oi</seg></w> <w n="4.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4" punct="vg:5">pr<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="189" place="5" punct="vg">e</seg>ts</w>, <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="4.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="4.7" punct="vg:8">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="8" punct="vg">om</seg></w>,</l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1">D</w>'<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="1">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="5.6">pl<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>s</w> <w n="5.7" punct="pe:8">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</w> !</l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">pr<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="189" place="3">e</seg>ts</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4">su<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.5">v<seg phoneme="o" type="vs" value="1" rule="437" place="7">o</seg>s</w> <w n="6.6" punct="vg:8">g<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="vg">oû</seg>ts</w>,</l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1">D</w>'<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="1">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="7.4">m<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.6" punct="pe:8">s<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="351" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ps">e</seg></w> ! …</l>
	</lg>
	<lg n="2">
		<head type="speaker">PRETTY.</head>
		<l n="8" num="2.1" lm="8" met="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="8.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>r</w> <w n="8.3">b<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="8.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">n</w>'<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="8.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>s</w> <w n="8.8">v<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>s</w></l>
		<l n="9" num="2.2" lm="8" met="8"><w n="9.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="9.3">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>r</w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="9.5" punct="pt:8">pr<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="351" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>. </l>
	</lg>
</div></body></text></TEI>