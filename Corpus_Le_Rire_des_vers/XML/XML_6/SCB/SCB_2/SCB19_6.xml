<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <fileDesc>
   <titleStmt>
	<title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	<title type="sub">COMÉDIE-VAUDEVILLE EN UN ACTE</title>
	<title type="corpus">Le Rire des vers</title>
	<author key="SCR" sort="1">
          <name>
            <forename>Eugène</forename>
            <surname>SCRIBE</surname>
          </name>
          <date from="1791" to="1861">1791-1861</date>
	</author>
	<author key="BYD" sort="2">
          <name>
            <forename>Jean-François-Alfred</forename>
            <surname>BAYARD</surname>
          </name>
          <date from="1796" to="1853">1796-1853</date>
	</author>
	<editor>Le Rire des vers, Université de Bâle</editor>
	<editor>
	 Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
	 <choice>
	  <abbr>CRISCO, Université de Caen Normandie</abbr>
	  <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	 </choice>
	 (EA 4255)
	</editor>
	<respStmt>
	 <resp>Encodage en XML (CRISCO, université de Caen)</resp>
	 <name id="KL">
	  <forename>Kedi</forename>
	  <surname>LI</surname>
	 </name>
	</respStmt>
	<respStmt>
	 <resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
	 <name id="RR">
	  <forename>Richard</forename>
	  <surname>RENAULT</surname>
	 </name>
	</respStmt>
   </titleStmt>
   <extent>140 vers</extent>
   <publicationStmt>
	<publisher>
	 <orgname>
	  <choice>
	   <abbr>CRISCO, Université de Caen Normandie</abbr>
	   <expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
	  </choice>
	 </orgname>
	 <address>
	  <addrLine>Université de Caen</addrLine>
	  <addrLine>14032 CAEN CEDEX</addrLine>
	  <addrLine>FRANCE</addrLine>
	 </address>
	 <email>crisco.incipit@unicaen.fr</email>
	 <ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
	</publisher>
	<pubPlace>Caen</pubPlace>
	<date when="2022">2022</date>
	<idno type="local">SCB_2</idno>	
	<availability status="free">
		<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
		<p>
			Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
			CC = Licence Creative Commons
			BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
			sur les sources et sur les modifications introduites.
			NC = Pas d’utilisation commerciale.
			SA = Partage dans les mêmes conditions.
		</p>
	</availability>
   </publicationStmt>
   <sourceDesc>
	<biblFull>
	 <titleStmt>
	  <title type="main">CAMILLA, OU LA SŒUR ET LE FRÈRE</title>
	  <author>SCRIBE ET BAYARD</author>
	 </titleStmt>
	 <publicationStmt>
	  <publisher>INTERNET ARCHIVE</publisher>
	  <idno type="URL">https://archive.org/details/camillaoulasoeur00scriuoft</idno>
	 </publicationStmt>
	 <sourceDesc>
	  <biblStruct>
	   <monogr>
		<repository>University of Toronto Libraries</repository>
		<idno type="URL">https://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991106543695706196</idno>
	   </monogr>
	  </biblStruct>                 
	 </sourceDesc>
	</biblFull> 
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <creation>
	<date when="1832">12 DÉCEMBRE 1832</date>
	<placeName>
	 <settlement>THÉÂTRE DU GYMNASE DRAMATIQUE</settlement>
	</placeName>
   </creation>
  </profileDesc>
  <encodingDesc>
   <projectDesc>
	<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
   </projectDesc>
   <samplingDecl>
	<p>Les parties versifiées ont été prioritairement encodées.</p>
   </samplingDecl>
   <editorialDecl>
	<normalization>
	 <p>Les majuscules accentuées ont été restituées.</p>
	 <p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
	 <p>La ponctuation a été normalisée.</p> 
	 <p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
	 <p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
	 <p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
	</normalization>
   </editorialDecl>
  </encodingDesc>
 </teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SCB19" modus="cp" lm_max="10" metProfile="8, 4+6">
	<head type="tune">AIR du Piége.</head>
	<lg n="1">
	   <l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">C</w>'<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="1.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3" mp="C">un</seg></w> <w n="1.5">t<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="P">à</seg></w> <w n="1.7">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="1.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="1.9" punct="vg:10">f<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="vg">eu</seg>rs</w>,</l>
	   <l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="2.2">l</w>'<w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>t</w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="2.7" punct="vg:8">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>rsp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>ct<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
	   <l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="P">à</seg></w> <w n="3.3" punct="vg:4">pr<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="C">au</seg>x</w> <w n="3.5" punct="vg:7">pl<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" punct="vg" mp="F">e</seg>s</w>, <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="317" place="8" mp="C">au</seg>x</w> <w n="3.7" punct="vg:10">h<seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="vg">eu</seg>rs</w>,</l>
	   <l n="4" num="1.4" lm="8" met="8"><w n="4.1">C</w>'<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="4.4">c<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w>  <w n="4.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.6">l</w>'<w n="4.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="4.8" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
	   <l n="5" num="1.5" lm="10" met="4+6"><w n="5.1" punct="vg:2"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="M">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg></w>, <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="5.3">d<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>s</w><caesura></caesura> <w n="5.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6" mp="C">un</seg></w> <w n="5.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8">in</seg></w> <w n="5.7">br<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>t</w></l>
	   <l n="6" num="1.6" lm="8" met="8"><w n="6.1" punct="vg:1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="vg">a</seg>r</w>, <w n="6.2">gr<seg phoneme="a" type="vs" value="1" rule="339" place="2">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="6.4">l</w>'<w n="6.5"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>t</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.7">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="6.8" punct="vg:8">b<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
	   <l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">su<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="7.3" punct="vg:4">l<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="7.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="7.6">n</w>'<w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg></w> <w n="7.8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="8" mp="M">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>t</w></l>
	   <l n="8" num="1.8" lm="8" met="8"><w n="8.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="1">en</seg></w> <w n="8.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="8.3">m</w>'<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="8.6">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="8.7" punct="pe:8">c<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
	</lg>   
</div></body></text></TEI>