<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME TABLEAU.</head><head type="main_subpart">SCÈNE XIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG19" modus="cp" lm_max="10" metProfile="8, 4+6">
						<head type="tune">Même air.</head>
						<lg n="1">
							<head type="main">BONAPARTE.</head>
							<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">n</w>'<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="2">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="1.5">l</w>'<w n="1.6" punct="dp:8"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>bs<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>l<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>sm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
							<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rs</w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg></w> <w n="2.3">p<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="2.5">f<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>t</w> <w n="2.6" punct="pt:8">f<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pt">a</seg>l</w>.</l>
							<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">d<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.3">l</w>'<w n="3.4" punct="vg:8"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>sm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="P">à</seg></w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="C">a</seg></w> <w n="4.4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="4.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="C">i</seg>l</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="4.7">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t</w> <w n="4.8">tr<seg phoneme="o" type="vs" value="1" rule="432" place="8">o</seg>p</w> <w n="4.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="4.10" punct="pt:10">m<seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="pt">a</seg>l</w>.</l>
							<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="P">À</seg></w> <w n="5.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" mp="C">on</seg></w> <w n="5.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>r</w><caesura></caesura> <w n="5.4">f<seg phoneme="œ" type="vs" value="1" rule="303" place="5" mp="M">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>t</w> <w n="5.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>rs</w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="5.7" punct="vg:10">gu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
							<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="6.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="351" place="2">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3" mp="C">un</seg></w> <w n="6.4" punct="vg:4">j<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="6.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="310" place="6">an</seg>t</w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="6.7" punct="vg:10">v<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="vg">é</seg></w>,</l>
							<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="4"></space><w n="7.1">R<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>dr<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="7.2">s<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>r</w> <w n="7.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="7.5">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="4"></space><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="8.2">l<seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="8.5" punct="pt:8">l<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pt">é</seg></w>. <del reason="analysis" hand="LG" type="repetition">(bis.)</del></l>
							<l n="9" num="1.9" lm="8" met="8"><space unit="char" quantity="4"></space><subst reason="analysis" hand="LG" type="repetition"><del></del><add rend="hidden"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="9.2">l<seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="9.5" punct="pt:8">l<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pt">é</seg></w>.</add></subst></l>
						</lg>
					</div></body></text></TEI>