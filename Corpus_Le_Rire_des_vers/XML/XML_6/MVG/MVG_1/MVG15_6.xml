<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE À L'ÉCOLE DE BRIENNE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="MAS" sort="1">
					<name>
						<forename>Michel</forename>
						<surname>MASSON</surname>
					</name>
					<date from="1800" to="1883">1800-1883</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="GAB" sort="3">
					<name>
						<forename>Gabriel</forename>
						<nameLink>de</nameLink>
						<surname>LURIEU</surname>
						<addName type="other">GABRIEL</addName>
					</name>
					<date from="1799" to="1889">1799-1889</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>378 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">MVG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte à l'école de Brienne</title>
						<author>Gabriel, Masson et Villeneuve</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=MbdoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bonaparte à l'école de Brienne</title>
								<author>Gabriel, Masson et Villeneuve</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100034261572.0x000001#?</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Barba</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME TABLEAU.</head><head type="main_subpart">SCÈNE XI.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MVG15" modus="cm" lm_max="10" metProfile="4+6">
						<head type="tune">AIR : Des Amazones et des Scythes.</head>
						<lg n="1">
							<head type="main">BONAPARTE.</head>
							<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="1.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3">s<seg phoneme="o" type="vs" value="1" rule="317" place="3" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="1.4">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="M">om</seg>p<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="1.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="1.6" punct="pv:10">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>st<seg phoneme="i" type="vs" value="1" rule="466" place="9" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></w> ;</l>
							<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="2.2">j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rs</w> <w n="2.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="2.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="2.5">d<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>nt</w> <w n="2.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>s</w> <w n="2.7" punct="pt:10">f<seg phoneme="i" type="vs" value="1" rule="466" place="9" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pt">i</seg>r</w>.</l>
							<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="P">À</seg></w> <w n="3.2">s</w>'<w n="3.3"><seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="M">u</seg>str<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="3.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="3.5">v<seg phoneme="i" type="vs" value="1" rule="481" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="3.7" punct="pv:10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="M">on</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>mn<seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></w> ;</l>
							<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">L</w>'<w n="4.2"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="1" mp="M">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="449" place="2" mp="M">u</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="4.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="4.4">p<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>t</w> <w n="4.5">lu<seg phoneme="i" type="vs" value="1" rule="490" place="7" mp="C">i</seg></w> <w n="4.6" punct="pt:10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="M">on</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pt">i</seg>r</w>.</l>
							<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="C">Au</seg></w> <w n="5.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="5.3" punct="vg:4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="vg" caesura="1">an</seg>g</w>,<caesura></caesura> <w n="5.4" punct="vg:5">ou<seg phoneme="i" type="vs" value="1" rule="490" place="5" punct="vg">i</seg></w>, <w n="5.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="5.6">v<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>x</w> <w n="5.7" punct="pt:10">p<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>rv<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pt">i</seg>r</w>.</l>
							<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="6.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="C">on</seg></w> <w n="6.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="4" caesura="1">om</seg></w><caesura></caesura> <w n="6.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>c<seg phoneme="œ" type="vs" value="1" rule="344" place="6" mp="M">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="6.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="P">a</seg>r</w> <w n="6.7">l</w>'<w n="6.8">h<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
							<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="P">à</seg></w> <w n="7.3">j<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">ai</seg>s</w><caesura></caesura> <w n="7.4">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6" mp="C">un</seg></w> <w n="7.6">b<seg phoneme="o" type="vs" value="1" rule="314" place="7">eau</seg></w> <w n="7.7" punct="pt:10">s<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pt pt">i</seg>r</w>..</l>
							<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="8.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="C">on</seg></w> <w n="8.3">c<seg phoneme="œ" type="vs" value="1" rule="248" place="4" caesura="1">œu</seg>r</w><caesura></caesura> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="8.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>t</w> <w n="8.6">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="8">oin</seg></w> <w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="8.8" punct="vg:10">gl<seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
							<l n="9" num="1.9" lm="10" met="4+6"><w n="9.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2" mp="M/mp">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="3" mp="Lp">ez</seg></w>-<w n="9.3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="4" caesura="1">oi</seg></w><caesura></caesura> <w n="9.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="M">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="9.5">s<seg phoneme="y" type="vs" value="1" rule="449" place="7" mp="P">u</seg>r</w> <w n="9.6">l</w>'<w n="9.7" punct="pe:10"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pe">i</seg>r</w> !</l>
						</lg>
					</div></body></text></TEI>