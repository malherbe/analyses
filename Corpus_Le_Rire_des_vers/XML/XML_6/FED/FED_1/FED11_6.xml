<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANDRÉ LE CHANSONNIER</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="FTN" sort="1">
					<name>
						<forename>Louis Marie</forename>
						<surname>FONTAN</surname>
					</name>
					<date from="1801" to="1839">1801-1839</date>
				</author>
				<author key="DNY" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>DESNOYER</surname>
					</name>
					<date from="1806" to="1858">1806-1858</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>239 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>André le chansonnier.</title>
						<author>FONTAN ET DESNOYER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=aN6oyNpF3cMC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>André le chansonnier.</title>
								<author>FONTAN ET DESNOYER</author>
								<repository>Biblioteca Casanatense</repository>
								<idno type="URI">http://opac.casanatense.it/Record.htm?idlist=3</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Ode et Wodon</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-15" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE I</head><head type="main_subpart">SCÈNE XIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FED11" rhyme="none" modus="sp" lm_max="7" metProfile="7, (6), (5)">
				<head type="tune">Même air.</head>
					<lg n="1">
						<head type="main">ANDRÉ, à voix basse, en s'adressant aux deux <lb></lb>sentinelles qui lui tournent le dos.</head>
						<l n="1" num="1.1" lm="6"><space unit="char" quantity="2"></space><w n="1.1">V<seg phoneme="wa" type="vs" value="1" rule="439" place="1">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="1.5" punct="vg:6">pl<seg phoneme="ɛ" type="vs" value="1" rule="304" place="6">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="7" met="7"><w n="2.1">L</w>'<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="169" place="1">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="2.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="2.5" punct="pi:7">ch<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" punct="pi">on</seg>s</w> ?</l>
						<p>L'orchestre seul répète l'air de ces deux vers.)</p>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1">J<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>squ</w>'<w n="3.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="3.3" punct="vg:3">lu<seg phoneme="i" type="vs" value="1" rule="490" place="3" punct="vg">i</seg></w>, <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>t</w> <w n="3.5">d</w>'<w n="3.6"><seg phoneme="y" type="vs" value="1" rule="452" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="3.7" punct="vg:7">h<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="384" place="7">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="5"><space unit="char" quantity="4"></space><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="4.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="4.3" punct="pt:5"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="5" punct="pt">er</seg></w>.</l>
						<p>(L'orchestre seul.)</p>
						<l n="5" num="1.5" lm="7" met="7"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="5.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="422" place="2" punct="vg">oi</seg></w>, <w n="5.3">p<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="5.5" punct="vg:7">s<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="6" num="1.6" lm="7" met="7"><w n="6.1">M<seg phoneme="wa" type="vs" value="1" rule="422" place="1">oi</seg></w> <w n="6.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="6.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="6.5">c<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="5">oin</seg></w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.7" punct="vg:7">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="7" num="1.7" lm="7" met="7"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>rri<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="7.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4" punct="pi:7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>p<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="7" punct="pi">er</seg></w> ?</l>
						<l n="8" num="1.8" lm="7" met="7"><w n="8.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="1">en</seg>t<seg phoneme="i" type="vs" value="1" rule="466" place="2">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="8.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.3">j</w>'<w n="8.4" punct="vg:7"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>squ<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="9" num="1.9" lm="7" met="7"><w n="9.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="9.4" punct="dp:5">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="dp">a</seg>s</w> : <w n="9.5">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="9.6" punct="pe:7">v<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
						<l n="10" num="1.10" lm="7" met="7"><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.3">pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>scr<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t</w> <w n="10.4" punct="pt:7">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="7" punct="pt">er</seg></w>.</l>
					</lg>
				</div></body></text></TEI>