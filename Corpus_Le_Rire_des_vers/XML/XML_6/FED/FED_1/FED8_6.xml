<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANDRÉ LE CHANSONNIER</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="FTN" sort="1">
					<name>
						<forename>Louis Marie</forename>
						<surname>FONTAN</surname>
					</name>
					<date from="1801" to="1839">1801-1839</date>
				</author>
				<author key="DNY" sort="2">
					<name>
						<forename>Charles</forename>
						<surname>DESNOYER</surname>
					</name>
					<date from="1806" to="1858">1806-1858</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>239 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FED_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>André le chansonnier.</title>
						<author>FONTAN ET DESNOYER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=aN6oyNpF3cMC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>André le chansonnier.</title>
								<author>FONTAN ET DESNOYER</author>
								<repository>Biblioteca Casanatense</repository>
								<idno type="URI">http://opac.casanatense.it/Record.htm?idlist=3</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Ode et Wodon</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-15" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE I</head><head type="main_subpart">SCÈNE X.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FED8" modus="cp" lm_max="10" metProfile="8, 4+6, (6)">
				<head type="tune">AIR du Dieu des Bonnes gens.</head>
					<lg n="1">
						<head type="main">ANDRÉ.</head>
						<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="169" place="1">E</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="1.4">n<seg phoneme="o" type="vs" value="1" rule="437" place="7">o</seg>s</w> <w n="1.5" punct="vg:8">gl<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg></w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="2.4">n<seg phoneme="o" type="vs" value="1" rule="437" place="6">o</seg>s</w> <w n="2.5" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>rs</w>,</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="C">u</seg></w> <w n="3.2">n</w>'<w n="3.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>s</w> <w n="3.4">j<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">ai</seg>s</w><caesura></caesura> <w n="3.5">pr<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>s</w> <w n="3.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rt</w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="P">à</seg></w> <w n="3.8">n<seg phoneme="o" type="vs" value="1" rule="437" place="8" mp="C">o</seg>s</w> <w n="3.9" punct="dp:10">v<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg>s</w> :</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="Fm">e</seg>s</w>-<w n="4.2">t<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="4.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4" caesura="1">en</seg></w><caesura></caesura> <w n="4.4">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="4.6">tr<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>s</w> <w n="4.7" punct="pi:10">c<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="pi">eu</seg>rs</w> ?</l>
						<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">C<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="5.2" punct="vg:4">c<seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>c<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg" caesura="1">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="5.4">s<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="5.5">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="188" place="8">e</seg>t</w> <w n="5.7">s<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg></w> <w n="5.8" punct="vg:10">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="P">ou</seg>r</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="6.4">l</w>'<w n="6.5"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="4" caesura="1">o</seg>r</w><caesura></caesura> <w n="6.6"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="C">au</seg>x</w> <w n="6.7">pi<seg phoneme="e" type="vs" value="1" rule="240" place="6">e</seg>ds</w> <w n="6.8">t<seg phoneme="y" type="vs" value="1" rule="449" place="7" mp="C">u</seg></w> <w n="6.9" punct="vg:10">f<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10" punct="vg">ai</seg>s</w>,</l>
						<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1" mp="P">an</seg>s</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3">r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4" caesura="1">e</seg>ct</w><caesura></caesura> <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="7.6">g<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="7.7">p<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="P">ou</seg>r</w> <w n="7.8" punct="ps:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg></w> …</l>
						<l n="8" num="1.8" lm="6"><space unit="char" quantity="8"></space><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">l</w>'<w n="8.4" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" punct="pt">ai</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>