<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA MÉTEMPSYCOSE</title>
				<title type="sub">BÊTISE EN UN ACTE, MÊLÉE DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="COU" sort="1">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1796" to="1862">1796-1862</date>
				</author>
				<author key="JAI" sort="2">
					<name>
						<forename>Ernest</forename>
						<surname>JAIME</surname>
					</name>
					<date from="1804" to="1884">1804-1884</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>282 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http ://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FCR_1</idno>	
				<availability status="free">
					<licence target="https ://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA MÉTEMPSYCOSE</title>
						<author>FRÉDÉRIC DE COURCY ET JAIME</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https ://books.google.ch/books ?id=c1RoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http ://access.bl.uk/item/viewer/ark :/81055/vdc_100032819917.0x000001# ?c=0</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">7 FÉVRIER 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE L'AMBIGU-COMIQUE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="FCR9" modus="sm" lm_max="8" metProfile="8">
	<head type="tune">AIR : Que d'établissemens nouveaux,</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J</w>'<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg></w> <w n="1.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="1.4">p<seg phoneme="ɛ" type="vs" value="1" rule="409" place="5">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="1.6">b<seg phoneme="o" type="vs" value="1" rule="314" place="7">eau</seg></w> <w n="1.7" punct="vg:8">j<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="vg">ou</seg>r</w>,</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="2.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg></w> <w n="2.3">j</w>'<w n="2.4"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg></w> <w n="2.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="2.6" punct="pv:8">m<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="3.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="3.3">c<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>ps</w> <w n="3.4">j</w>'<w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg></w> <w n="3.6">v<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg></w> <w n="3.7">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>r</w> <w n="3.8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">à</seg></w> <w n="3.9">t<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>r</w></l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>b<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="4.2">s<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="4.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
		<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="5.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="5.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="5.4"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l</w> <w n="5.5">v<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="5.7" punct="vg:8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="vg">er</seg></w>,</l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="466" place="2">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="6.4" punct="pe:8">t<seg phoneme="i" type="vs" value="1" rule="492" place="6">y</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>nn<seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rd</w>'<w n="7.2">hu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="7.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s</w> <w n="7.5">fr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>c<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w></l>
		<l n="8" num="1.8" lm="8" met="8"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="8.5" punct="pt:8">d<seg phoneme="i" type="vs" value="1" rule="496" place="6">y</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>st<seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
	</lg> 
</div></body></text></TEI>