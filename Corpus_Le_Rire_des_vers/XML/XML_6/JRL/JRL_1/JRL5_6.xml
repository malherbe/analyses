<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHABERT</title>
				<title type="sub">HISTOIRE CONTEMPORAINE EN DEUX ACTES, MÊLÉE DE CHANT</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="ARJ" sort="1">
					<name>
						<forename>Jacques</forename>
						<surname>ARAGO</surname>
					</name>
					<date from="1790" to="1855">1790-1855</date>
				</author>
				<author key="LUR" sort="2">
					<name>
						<forename>Louis</forename>
						<surname>LURINE</surname>
					</name>
					<date from="1810" to="1860">1810-1860</date>
				</author>
				<editor>Le rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>110 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">JRL_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
				   </p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">CHABERT</title>
						<author>JACQUES ARAGO ET LOUIS LURINE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=vldoAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>The British Library</repository>
								<idno type="URL">http://access.bl.uk/item/viewer/ark:/81055/vdc_100032849877.0x000001#?c=0</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">2 JUILLET 1832</date>
				<placeName>
					<settlement>THÉÂTRE NATIONAL DU VAUDEVILLE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="JRL5" modus="cp" lm_max="11" metProfile="4+6, (11)">
	<head type="tune">AIR : Depuis long-temps j'aimais Adèle.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1" punct="vg:2">M<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1" mp="M">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg></w>, <w n="1.2" punct="pv:4">m<seg phoneme="œ" type="vs" value="1" rule="150" place="3" mp="M">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="4" punct="pv" caesura="1">eu</seg>r</w> ;<caesura></caesura> <w n="1.3">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s</w> <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="317" place="6" mp="M/mc">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="Lc">ou</seg>rd</w>'<w n="1.5" punct="vg:8">hu<seg phoneme="i" type="vs" value="1" rule="490" place="8" punct="vg">i</seg></w>, <w n="1.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="P">a</seg>r</w> <w n="1.7" punct="vg:10">gr<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
		<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">N</w>'<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="353" place="1" mp="M">e</seg>x<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5" mp="C">un</seg></w> <w n="2.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>g</w> <w n="2.6" punct="pt:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="466" place="9" mp="M">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10" punct="pt">en</seg>t</w>.</l>
		<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="3.3" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="3.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="3.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="3.8" punct="pt:10">gl<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
		<l n="4" num="1.4" lm="11"><w n="4.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2" mp="M/mp">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="3" mp="Lp">ez</seg></w>-<w n="4.3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="4">oi</seg></w> <w n="4.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6" mp="M">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="d-1" place="7" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w> <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="9">en</seg></w> <w n="4.6" punct="pt:11">pl<seg phoneme="ø" type="vs" value="1" rule="404" place="10" mp="M">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" punct="pt">an</seg>t</w>.</l>
	</lg>
	<lg n="2">
		<head type="speaker">DERVILLE, à part.</head>
		<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w>-<w n="5.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2" mp="F">e</seg></w> <w n="5.3" punct="vg:4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" mp="M">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg" caesura="1">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w>-<w n="5.5">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="6" mp="F">e</seg></w> <w n="5.6" punct="pi:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>c<seg phoneme="o" type="vs" value="1" rule="434" place="8" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9" mp="M">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></w> ?</l>
		<l n="6" num="2.2" lm="10" met="4+6"><w n="6.1">C</w>'<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2" mp="C">un</seg></w> <w n="6.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3" mp="M">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>t</w><caesura></caesura> <w n="6.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="6.6" punct="vg:6">pl<seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="vg">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="6.8">j</w>'<w n="6.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8" mp="M">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>s</w></l>
		<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="7.2">pr<seg phoneme="o" type="vs" value="1" rule="443" place="2" mp="M">o</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="7.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5" mp="P">an</seg>s</w> <w n="7.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="7.5">tr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="7.6" punct="pv:10">s<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></w> ;</l>
		<l n="8" num="2.4" lm="10" met="4+6"><w n="8.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2" mp="C">un</seg></w> <w n="8.3">fr<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" caesura="1">on</seg></w><caesura></caesura> <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="8.5"><seg phoneme="y" type="vs" value="1" rule="390" place="7">eu</seg></w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="8.8" punct="pt:10">v<seg phoneme="wa" type="vs" value="1" rule="419" place="10" punct="pt">oi</seg>x</w>.</l>
		<l n="9" num="2.5" lm="10" met="4+6"><w n="9.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="9.3">fr<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" caesura="1">on</seg>s</w><caesura></caesura> <w n="9.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="9.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>rs</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="9.8" punct="pt:10">v<seg phoneme="wa" type="vs" value="1" rule="419" place="10" punct="pt">oi</seg>x</w>.</l>
	</lg> 
</div></body></text></TEI>