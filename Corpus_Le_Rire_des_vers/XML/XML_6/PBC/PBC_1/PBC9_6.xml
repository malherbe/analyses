<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC9" modus="cp" lm_max="9" metProfile="7, 8, 4, (3), (9)">
	<head type="tune">AIR : L'autre jour la pʼtite Isabelle.</head> 
	<lg n="1">
		<l n="1" num="1.1" lm="7" met="7"><w n="1.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="1.2" punct="vg:3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">am</seg>z<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" punct="in vg">e</seg>ll</w>', <w n="1.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="6">en</seg>t<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="2.2">m</w>'<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg></w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg>s</w> <w n="2.6" punct="pe:8">v<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pe">ou</seg>s</w> !</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="3.2">cʼt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.3">v<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg></w>'<w n="3.4">f<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">ê</seg>t</w> <w n="3.5" punct="vg:8"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="4" num="1.4" lm="8" met="8"><w n="4.1">J</w>'<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="4.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></w> <w n="4.4">d<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="4.5">l<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>ps</w> <w n="4.6" punct="pt:8">g<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>r<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">ou</seg>s</w>.</l>
		<l rhyme="none" n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="5.2" punct="vg:3"><seg phoneme="ø" type="vs" value="1" rule="397" place="3" punct="vg">eu</seg>x</w>, <w n="5.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.4">m</w>'<w n="5.5">su<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg>s</w> <w n="5.6">pr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>s</w> <w n="5.7">d</w>'<w n="5.8" punct="vg:8">qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="6.2">c</w>'<w n="6.3">qu</w>'<w n="6.4"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ls</w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="6.7" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="pt">an</seg>s</w>.</l>
		<l n="7" num="1.7" lm="4" met="4"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="7.2">cʼt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.3" punct="vg:4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></w>,</l>
		<l n="8" num="1.8" lm="4" met="4"><w n="8.1">J</w>'<w n="8.2">d<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>s</w> <w n="8.3">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="8.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.5">j</w>'<w n="8.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
		<l n="9" num="1.9" lm="3"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="9.2">gr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>ssʼs</w> <w n="9.3" punct="pv:3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="3" punct="pv">en</seg>ts</w> ;</l>
		<l n="10" num="1.10" lm="7" met="7"><w n="10.1">J</w>'<w n="10.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="10.5" punct="pe:5">g<seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="pe ps">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! … <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="10.7">j</w>'<w n="10.8" punct="pe:7">cr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe ps">e</seg></w> ! …</l>
	<p>(Il parle très-vite). V'là que tout à coup, par un effet ma
		gique, la forêt se remplit d'animaux des quatre parties du
		monde, et je me vois entouré de perroquets qui parlaient, 
		d'chauves-souris qui sifflaient, de corbeaux qui croassaient, 
		d'chats-huans qui me huaient… d'éléphans qui me chatouil
		laient avec leurs trompes, de vautours qui me lançaient des
		coups de bec, d'escargots qui me faisaient les cornes, de
		serpens qui me piquaient les mollets, j'avais beau vouloir
		me sauver, ils me poursuivaient, ils me barraient le chemin.
		(Il chante.)
	</p> 
		<l n="11" num="1.11" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="11.2">j</w>'<w n="11.3">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="11.4">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="11.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>g<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="vg">é</seg></w>,</l>
		<l n="12" num="1.12" lm="9"><w n="12.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="12.2">j</w>'<w n="12.3"><seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="12.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4" mp="P">an</seg>s</w> <w n="12.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w>' <w n="12.6">m<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>r<seg phoneme="i" type="vs" value="1" rule="481" place="9">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" mp="F">e</seg></w></l>
		<l n="13" num="1.13" lm="8" met="8"><w n="13.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Où</seg></w> <w n="13.2">l</w>'<w n="13.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="13.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.5">m</w>'<w n="13.6"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="13.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="13.8" punct="pt:8">m<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>g<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pt">é</seg></w>.</l>
  </lg>	
</div></body></text></TEI>