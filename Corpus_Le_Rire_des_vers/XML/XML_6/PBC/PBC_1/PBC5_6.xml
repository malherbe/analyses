<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC5" modus="cp" lm_max="10" metProfile="8">
	<head type="tune">AIR : Elle était heureuse au village.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="1.2">n</w>'<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="1.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r</w> <w n="1.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="1.6">n<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>l</w> <w n="1.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="vg">ai</seg>t</w>,</l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">b<seg phoneme="o" type="vs" value="1" rule="314" place="2">eau</seg></w> <w n="2.3" punct="vg:3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">an</seg>g</w>, <w n="2.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.5">t<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.7" punct="pi:8">r<seg phoneme="ɛ" type="vs" value="1" rule="384" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
		<l n="3" num="1.3" lm="8" met="8"><w n="3.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">b<seg phoneme="o" type="vs" value="1" rule="314" place="2">eau</seg></w> <w n="3.3">p<seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="3.5" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pt">ai</seg>t</w>.</l>
	</lg>
	<lg n="2">
	<head type="speaker">BRIND'AMOUR.</head>
		<l n="4" num="2.1" lm="8" met="8"><w n="4.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="443" place="1" punct="pe">O</seg>h</w> ! <w n="4.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r</w> <w n="4.3">lu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="4.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.5">n</w>'<w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg></w> <w n="4.7">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="6">oin</seg>t</w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.9" punct="dp:8">h<seg phoneme="ɛ" type="vs" value="1" rule="304" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
		<l n="5" num="2.2" lm="8" met="8"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="5.2">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="5.3" punct="vg:4">h<seg phoneme="i" type="vs" value="1" rule="496" place="3">y</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="220" place="4" punct="vg">en</seg></w>, <w n="5.4">s<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg></w> <w n="5.5">p<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg></w> <w n="5.6" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="434" place="7">o</seg>mm<seg phoneme="œ̃" type="vs" value="1" rule="451" place="8" punct="vg">un</seg></w>,</l>
		<l n="6" num="2.3" lm="8" met="8"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="6.2">m<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="6.4">n</w>'<w n="6.5"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="6.6">s<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="6.7" punct="pt:8">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
	</lg>
	<lg n="3">
	<head type="speaker">SERPENTINE.</head>
		<l ana="unanalyzable" n="7" num="3.1">Pourquoi ?</l>
	</lg>
	<lg n="4">
		<head type="speaker">BRIND'AMOUR.</head>
		<l n="8" num="4.1" lm="10" met="10" mp4="F"><w n="8.1"><seg phoneme="y" type="vs" value="1" rule="452" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="8.2">j<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="8.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>rs</w> <w n="8.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="Lc">e</seg>lqu</w>'<w n="8.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="10">un</seg></w></l>
		<l n="9" num="4.2" lm="8" met="8"><w n="9.1">Qu</w>'<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="2">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="9.5">p<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg></w> <w n="9.6">pl<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>s</w> <w n="9.7">qu</w>'<w n="9.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="9.9">b<seg phoneme="o" type="vs" value="1" rule="314" place="7">eau</seg></w>-<w n="9.10" punct="vg:8">p<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,<del reason="analysis" type="repetition" hand="KL">(bis.)</del></l>
	</lg>
</div></body></text></TEI>