<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC39" modus="sp" lm_max="8" metProfile="7, 3, (4), (8)">
	<head type="tune">AIR : Pour un curé patriote.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="7" met="7"><w n="1.1" punct="vg:1">Ç<seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="vg">a</seg></w>, <w n="1.2">c<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="1.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="1.4" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>pr<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</w>,</l>
		<l n="2" num="1.2" lm="7" met="7"><w n="2.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="2.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>c</w> <w n="2.4">v<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>r</w> <w n="2.5"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w></l>
		<l n="3" num="1.3" lm="7" met="7"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="3.2">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>s</w> <w n="3.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>t</w>-<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="3.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="3.6" punct="vg:7">n<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</w>,</l>
		<l n="4" num="1.4" lm="7" met="7"><w n="4.1">S</w>'<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>l</w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="4.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</w> <w n="4.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">em</seg>ps</w>-<w n="4.8" punct="pt:7">c<seg phoneme="i" type="vs" value="1" rule="467" place="7" punct="pt">i</seg></w>.</l>
		<l n="5" num="1.5" lm="7" met="7"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="5.2">l</w>'<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>str<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5" punct="vg:7">v<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="7" punct="vg">à</seg></w>,</l>
		<l n="6" num="1.6" lm="7" met="7"><w n="6.1">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3" punct="dp:7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="dp">a</seg></w> :</l>
		<l n="7" num="1.7" lm="3" met="3"><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="7.2" punct="vg:3">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="vg">a</seg></w>,</l>
		<l n="8" num="1.8" lm="3" met="3"><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="8.2">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w></l>
		<l n="9" num="1.9" lm="7" met="7"><w n="9.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="9.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>l</w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>r</w> <w n="9.4">ç<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="9.5" punct="pt:7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="pt">a</seg></w>.</l> 
	</lg>
	<lg n="2">
		<l n="10" num="2.1" lm="7" met="7"><w n="10.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">j<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="10.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="10.5" punct="vg:7">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
		<l n="11" num="2.2" lm="7" met="7"><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="11.2">v<seg phoneme="wa" type="vs" value="1" rule="439" place="2">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="11.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="11.5">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="6">a</seg><seg phoneme="i" type="vs" value="1" rule="320" place="7">y</seg>s</w></l>
		<l n="12" num="2.3" lm="7" met="7"><w n="12.1">S</w>'<w n="12.2"><seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r</w> <w n="12.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381" place="5">e</seg>il</w> <w n="12.4" punct="vg:7"><seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>s<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
		<l n="13" num="2.4" lm="7" met="7"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="2">ain</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="13.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4">en</seg></w> <w n="13.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="13.5" punct="vg:7">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="7" punct="vg">i</seg>s</w>,</l>
		<l n="14" num="2.5" lm="7" met="7"><w n="14.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="1">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">f<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">em</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t</w></l>
		<l n="15" num="2.6" lm="7" met="7"><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>r</w> <w n="15.3">tr<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r</w> <w n="15.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="15.5" punct="pt:7">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="189" place="7" punct="pt">e</seg>t</w>.</l>
		<l n="16" num="2.7" lm="3" met="3"><w n="16.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="16.2" punct="vg:3">r<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3" punct="vg">ai</seg>t</w>,</l>
		<l n="17" num="2.8" lm="4"><w n="17.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="17.3">r<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w></l>
		<l n="18" num="2.9" lm="7" met="7"><w n="18.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="18.3">qu</w>'<w n="18.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="18.5" punct="vg:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7" punct="vg">ai</seg>t</w>,</l>
		<l n="19" num="2.10" lm="8"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="19.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="19.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>g<seg phoneme="y" type="vs" value="1" rule="447" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="19.4">qu</w>'<w n="19.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="19.6" punct="pe:8">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pe">ai</seg>t</w> !</l>
	</lg>
</div></body></text></TEI>