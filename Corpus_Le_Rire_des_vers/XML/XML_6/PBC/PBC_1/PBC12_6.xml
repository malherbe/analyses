<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE PETIT HOMME ROUGE</title>
				<title type="sub">FOLIE-FÉERIE-ROMANTIQUE EN QUATRE ACTES ET EN VAUDEVILLES,</title>
				<title type="sub_1">IMITÉE DU GENRE ANGLAIS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="PIX" sort="1">
					<name>
						<forename>René-Charles</forename>
						<surname>GUILBERT DE PIXÉRÉCOURT</surname>                 
					</name>
					<date from="1773" to="1844">1773-1844</date>
				</author>
				<author key="BRA" sort="2">
				  <name>
					<forename>Nicolas</forename>
					<surname>BRAZIER</surname>
				  </name>
				  <date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="CCH" sort="3">
					<name>
						<forename>Pierre-François-Adolphe</forename>
						<surname>CARMOUCHE</surname>
					</name>
					<date from="1797" to="1868">1797-1868</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>470 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">PBC_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE PETIT HOMME ROUGE</title>
						<author>G. DE PIXERÉCOURT, BRAZIER ET CARMOUCHE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=N3pLAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Bibliothèque nationale d'Autriche</repository>
								<idno type="URL">https://onb.digital/result/102D1F17</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">19 MARS 1832</date>
				<placeName>
					<settlement>THÉÂTRE DE LA GAITÉ</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="PBC12" modus="sm" lm_max="6" metProfile="6">
	<head type="tune">AIR : Eh ! vogue ma nacelle.</head>  
	<lg n="1">
		<l n="1" num="1.1" lm="6" met="6"><w n="1.1">Pu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="1.3">h<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="402" place="4">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
		<l n="2" num="1.2" lm="6" met="6"><w n="2.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="2.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="2.3">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="2.4" punct="pe:6">v<seg phoneme="ø" type="vs" value="1" rule="247" place="6" punct="pe ps">œu</seg>x</w> !…</l>
		<l n="3" num="1.3" lm="6" met="6"><w n="3.1">D<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="3.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4" punct="vg:6">v<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
		<l n="4" num="1.4" lm="6" met="6"><w n="4.1">D<seg phoneme="i" type="vs" value="1" rule="466" place="1">i</seg>m<seg phoneme="i" type="vs" value="1" rule="466" place="2">i</seg>n<seg phoneme="y" type="vs" value="1" rule="456" place="3">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="4.3">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="4.4" punct="pt:6">y<seg phoneme="ø" type="vs" value="1" rule="397" place="6" punct="pt">eu</seg>x</w>.</l>
		<l n="5" num="1.5" lm="6" met="6"><w n="5.1">V<seg phoneme="ɛ" type="vs" value="1" rule="63" place="1">e</seg>rs</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="5.3">b<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rds</w> <w n="5.4"><seg phoneme="u" type="vs" value="1" rule="425" place="4">où</seg></w> <w n="5.5">r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
		<l n="6" num="1.6" lm="6" met="6"><w n="6.1">L</w>'<w n="6.2"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>bj<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="6.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="vg">ou</seg>rs</w>,</l>
		<l n="7" num="1.7" lm="6" met="6"><w n="7.1" punct="pe:1">Fu<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="pe">i</seg>s</w> ! <w n="7.2"><seg phoneme="e" type="vs" value="1" rule="188" place="2">e</seg>t</w> <w n="7.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4" punct="vg:6">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="vg">ou</seg>rs</w>,<del reason="analysis" type="repetition" hand="KL">(bis.)</del></l>
		<l n="8" num="1.8" lm="6" met="6"><w n="8.1" punct="vg:2">V<seg phoneme="ɔ" type="vs" value="1" rule="442" place="1">o</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="8.2">l<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="8.3" punct="vg:6">n<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
		<l n="9" num="1.9" lm="6" met="6"><w n="9.1">V<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="9.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="9.3">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="9.4" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="pe">ou</seg>rs</w> !</l>
	</lg>
</div></body></text></TEI>