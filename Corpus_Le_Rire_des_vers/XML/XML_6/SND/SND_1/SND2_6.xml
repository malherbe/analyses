<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BONAPARTE LIEUTENANT D'ARTILLERIE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="NSL" sort="2">
					<name>
						<forename>Jean-Pierre-Laurent</forename>
						<surname>DENOMBRET</surname>
						<addName type="pen_name">Charles NOMBRET SAINT-LAURENT</addName>
					</name>
					<date from="1791" to="1833">1791-1833</date>
				</author>
					<author key="DUV" sort="3">
					<name>
						<forename>Félix-Auguste</forename>
						<surname>DUVERT</surname>
					</name>
					<date from="1795" to="1876">1795-1876</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>225 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SND_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=NWU:35556007830409</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
						<title>Bonaparte lieutenant d'artillerie</title>
						<author>XAVIER, DUVERT ET SAINT-LAURENT</author>
								<repository>Northwestern university</repository>
								<idno type="URI">https://babel.hathitrust.org/cgi/pt?id=ien.35556007830409</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BEZOU</publisher>
									<date when="1830">1830</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE II.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SND2" modus="sp" lm_max="7" metProfile="7, 4">
				<head type="tune">AIR : Eh ! lon lan la, landerirette.</head>
				<lg n="1">
					<head type="main">LORIS, entre gaîment.</head>
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">l</w>'<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">A</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.6" punct="vg:7">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">am</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="353" place="1">E</seg>x<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="2.2">s<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>ls</w> <w n="2.3">n<seg phoneme="o" type="vs" value="1" rule="437" place="5">o</seg>s</w> <w n="2.4" punct="pe:7"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="467" place="7" punct="pe">i</seg>ts</w> !</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="3.2">b<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>tt<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="3.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="3.5" punct="vg:7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">om</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="7" met="7"><w n="4.1">Gr<seg phoneme="a" type="vs" value="1" rule="339" place="1">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>x</w> <w n="4.3">j<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>rn<seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>x</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5" punct="pe:7">P<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="7" punct="pe">i</seg>s</w> !</l>
					<l n="5" num="1.5" lm="4" met="4"><space unit="char" quantity="6"></space><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="5.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="5.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg></w> <w n="5.4" punct="vg:4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg></w>,</l>
					<l n="6" num="1.6" lm="4" met="4"><space unit="char" quantity="6"></space><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="6.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.3" punct="vg:4">g<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></w>,</l>
					<l n="7" num="1.7" lm="4" met="4"><space unit="char" quantity="6"></space><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="7.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg></w> <w n="7.4" punct="vg:4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg></w>,</l>
					<l n="8" num="1.8" lm="4" met="4"><space unit="char" quantity="6"></space><w n="8.1">R<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>st<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="8.3" punct="pe:4">l<seg phoneme="a" type="vs" value="1" rule="341" place="4" punct="pe">à</seg></w> !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1" lm="7" met="7"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="9.2">g<seg phoneme="ɛ" type="vs" value="1" rule="304" place="3">aî</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4">en</seg>t</w> <w n="9.3">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.4" punct="pe:7">v<seg phoneme="i" type="vs" value="1" rule="481" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
					<l n="10" num="2.2" lm="7" met="7"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">tr<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="10.5">s<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="10.6" punct="pe:7">c<seg phoneme="u" type="vs" value="1" rule="424" place="7" punct="pe">ou</seg>rt</w> !</l>
					<l n="11" num="2.3" lm="7" met="7"><w n="11.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.3" punct="vg:4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" punct="vg">in</seg></w>, <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="11.5" punct="pe:7">f<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>l<seg phoneme="i" type="vs" value="1" rule="481" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
					<l n="12" num="2.4" lm="7" met="7"><w n="12.1">D<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="12.2">f<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg></w> <w n="12.3">l</w>'<w n="12.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>bb<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg></w> <w n="12.5" punct="pt:7">Gr<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="7" punct="pt">ou</seg>rt</w>.</l>
					<l n="13" num="2.5" lm="4" met="4"><space unit="char" quantity="6"></space><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="13.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="13.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg></w> <w n="13.4" punct="vg:4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg></w>,</l>
					<l n="14" num="2.6" lm="4" met="4"><space unit="char" quantity="6"></space><w n="14.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="14.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="481" place="4">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></w>,</l>
					<l n="15" num="2.7" lm="4" met="4"><space unit="char" quantity="6"></space><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="15.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="15.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg></w> <w n="15.4" punct="vg:4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg></w>,</l>
					<l n="16" num="2.8" lm="4" met="4"><space unit="char" quantity="6"></space><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">nʼ</w> <w n="16.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rs</w> <w n="16.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="16.5">dʼ</w> <w n="16.6" punct="pt:4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="pt">a</seg></w>.</l>
				</lg>
			</div></body></text></TEI>