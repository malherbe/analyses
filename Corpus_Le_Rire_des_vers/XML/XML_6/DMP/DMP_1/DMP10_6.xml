<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L'HOMME QUI BAT SA FEMME</title>
				<title type="sub">TABLEAU POPULAIRE EN UN ACTE, MÊLÉ DE COUPLETS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DML" sort="1">
				  <name>
					<forename>Julien</forename>
					<nameLink>de</nameLink>
					<surname>MAILLAN</surname>
					<addname type="other">Julien</addname>
					<addname type="other">Julien de M.</addname>
				  </name>
				  <date from="1805" to="1851">1805-1851</date>
				</author>
				<author key="DNR" sort="2">
				  <name>
					<forename>Philippe-François</forename>
					<surname>PINEL</surname>
					<addname type="pen_name">DUMANOIR</addname>
					<addname type="other">Philippe Dumanoir</addname>
					<addname type="other">Philippe D.</addname>
					<addname type="other">Philippe D***</addname>
				  </name>
				  <date from="1806" to="1865">1806-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DMP_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons: CC-BY-NC-SA</licence>
          <p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L'HOMME QUI BAT SA FEMME</title>
						<author>JULIEN DE MALLIAN ET PHILIPPE DUMANOIR</author>
					</titleStmt>
					<publicationStmt>
						<publisher>INTERNET ARCHIVE</publisher>
						<idno type="URL">https ://archive.org/details/lhommequibatsafe00malluoft</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>University of Toronto Libraries</repository>
								<idno type="URL">https ://librarysearch.library.utoronto.ca/permalink/01UTORONTO_INST/14bjeso/alma991105986034006196</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">8 MAI 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DMP10" modus="sp" lm_max="8" metProfile="2, 4, 8, 6">
	<head type="tune">AIR : Alerte.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="2" met="2"><w n="1.1" punct="pe:2">T<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pe">e</seg></w> ! <del reason="analysis" type="repetition" hand="KL">(bis)</del></l>
		<l n="2" num="1.2" lm="4" met="4"><w n="2.1" punct="vg:2">H<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="2" punct="vg">eu</seg>r</w>, <w n="2.2">h<seg phoneme="o" type="vs" value="1" rule="434" place="3">o</seg>mm<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
		<l n="3" num="1.3" lm="4" met="4"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="3.2" punct="pe:4">t<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>g<seg phoneme="œ" type="vs" value="1" rule="406" place="4" punct="pe">eu</seg>r</w> !</l>
		<l n="4" num="1.4" lm="2" met="2"><w n="4.1" punct="pe:2">T<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pe">e</seg></w> ! <del reason="analysis" type="repetition" hand="KL">(bis)</del> </l>
		<l n="5" num="1.5" lm="4" met="4"><w n="5.1">V<seg phoneme="wa" type="vs" value="1" rule="419" place="1">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="5.2">l</w>'<w n="5.3" punct="pe:4">b<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="4" punct="pe">eu</seg>r</w> !</l>
		<l n="6" num="1.6" lm="8" met="8"><w n="6.1">Ch<seg phoneme="e" type="vs" value="1" rule="346" place="1">ez</seg></w> <w n="6.2" punct="vg:4">D<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>sn<seg phoneme="wa" type="vs" value="1" rule="439" place="3">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="vg">er</seg>s</w>, <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg></w> <w n="6.4">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>t</w>-<w n="6.5">P<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rn<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
		<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="7.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3" punct="vg:4">pr<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4" punct="vg">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="7.5">l</w>'<w n="7.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="7.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.8" punct="pv:8">pl<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
		<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="8.2">b<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>t</w> <w n="8.3">d</w>'<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rd</w> <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="8.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="8.7">g<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rç<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg></w></l>
		<l n="9" num="1.9" lm="8" met="8"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="9.3">d<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>f<seg phoneme="i" type="vs" value="1" rule="481" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="9.5">b<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306" place="7">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg></w></l>
		<l n="10" num="1.10" lm="6" met="6"><w n="10.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="10.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="10.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="10.5" punct="pt:6">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="pt">on</seg></w>.</l>
	</lg>
	<lg n="2">
	<head type="main">CHŒUR.</head>
		<l ana="unanalyzable" n="11" num="2.1">Tapage ! etc. (bis.)</l>
	</lg>
	<lg n="3">
	<head type="speaker">PICHARD.</head>
	<head type="main">DEUXIÈME COUPLET.</head>
		<l n="12" num="3.1" lm="8" met="8"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="12.2">d</w>'<w n="12.3">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="12.4" punct="vg:4">m<seg phoneme="e" type="vs" value="1" rule="352" place="3">e</seg>ssi<seg phoneme="ø" type="vs" value="1" rule="396" place="4" punct="vg">eu</seg>rs</w>, <w n="12.5">c<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="12.7" punct="vg:8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="484" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="13" num="3.2" lm="8" met="8"><w n="13.1">S</w>'<w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">en</seg></w> <w n="13.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="3">en</seg>t</w> <w n="13.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</w> <w n="13.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="13.6">b<seg phoneme="ɔ" type="vs" value="1" rule="418" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="13.7" punct="pt:8">v<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
		<l n="14" num="3.3" lm="8" met="8"><w n="14.1">Vʼl<seg phoneme="a" type="vs" value="1" rule="341" place="1">à</seg></w> <w n="14.2">qu</w>'<w n="14.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="14.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="14.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="14.6">r<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">em</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="8">en</seg>t</w></l>
		<l n="15" num="3.4" lm="8" met="8"><w n="15.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="15.2">ch<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="15.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="15.4">d<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="8">en</seg>t</w></l>
		<l n="16" num="3.5" lm="6" met="6"><w n="16.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2" punct="vg">in</seg></w>, <w n="16.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>t</w> <w n="16.3">l</w>'<w n="16.4" punct="pe:6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">em</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="6" punct="pe">en</seg>t</w> !</l>
	</lg>
</div></body></text></TEI>