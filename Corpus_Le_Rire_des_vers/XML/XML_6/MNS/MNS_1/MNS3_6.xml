<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS3" modus="sm" lm_max="8" metProfile="8">
	<head type="tune">Air du Vaudev. des Maris ont tort.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J</w>'<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="1.3">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="1.5">ch<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.6" punct="pt:8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l> 
	</lg>
	<lg n="2">
		<head type="speaker">ANDRÉ</head>. 
		<l n="2" num="2.1" lm="8" met="8"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="2.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="2.3">pr<seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w>'<w n="2.4">s</w> <w n="2.5">f<seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>t</w>-<w n="2.6"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="2.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>t</w> <w n="2.8" punct="pi:8">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pi">er</seg></w> ?</l>
		<l n="3" num="2.2" lm="8" met="8"><w n="3.1">s<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="3.2">j</w>'<w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="3.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>g</w>' <w n="3.5">qu<seg phoneme="ø" type="vs" value="1" rule="404" place="4">eu</seg>qu</w>' <w n="3.6">f<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>s</w> <w n="3.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="3.8" punct="vg:8">c<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="4" num="2.3" lm="8" met="8"><w n="4.1">C</w>'<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="4.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r</w> <w n="4.4">m</w>'<w n="4.5">r<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>fr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">aî</seg>ch<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>r</w> <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.7" punct="vg:8">g<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>si<seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="vg">er</seg></w>, <del reason="analysis" type="repetition" hand="KL">( bis.)</del></l>
		<l n="5" num="2.4" lm="8" met="8"><w n="5.1">J</w>' <w n="5.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>ds</w> <w n="5.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="5.4" punct="vg:3">fru<seg phoneme="i" type="vs" value="1" rule="490" place="3" punct="vg">i</seg>ts</w>, <w n="5.5">c</w>'<w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="5.7" punct="vg:5">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5" punct="vg">ai</seg></w>, <w n="5.8">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s</w> <w n="5.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="5.10">z<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
		<l n="6" num="2.5" lm="8" met="8"><w n="6.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>d</w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="6.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">ê</seg>t</w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>r</w> <w n="6.6" punct="pv:8">g<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pv">oû</seg>t</w> ;</l>
		<l n="7" num="2.6" lm="8" met="8"><w n="7.1">Ç<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="7.2">v<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="7.3" punct="vg:3">mi<seg phoneme="ø" type="vs" value="1" rule="397" place="3" punct="vg">eu</seg>x</w>, <w n="7.4">n</w>'<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w>-<w n="7.6">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="5">e</seg></w> <w n="7.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>s</w> <w n="7.8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">am</seg>z<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
		<l n="8" num="2.7" lm="8" met="8"><w n="8.1">Qu</w>' <w n="8.2">s<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="8.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.4">n</w>' <w n="8.5">lu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="8.6">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s</w> <w n="8.7">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="6">en</seg></w> <w n="8.8">d<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="8.9" punct="pi:8">t<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pi">ou</seg>t</w> ?</l>  
	</lg>
</div></body></text></TEI>