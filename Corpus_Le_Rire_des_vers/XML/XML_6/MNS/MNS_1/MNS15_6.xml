<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRUNE ET BLONDE</title>
				<title type="sub">TABLEAU EN UN ACTE, MÊLÉ DE CHANTS</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="MNS">
					<name>
						<forename>Constant</forename>
						<surname>MÉNISSIER</surname>
					</name>
					<date from="1793" to="1878">1793-1878</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>338 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">MNS_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">BRUNE ET BLONDE</title>
						<author>M. MÉNIFSIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?id=qkc6AAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>La bibliothèque de l'État de Bavière</repository>
								<idno type="URL">http://opacplus.bsb-muenchen.de/title/BV001619840/ft/bsb10102964?page=5</idno>
							</monogr>
						</biblStruct>                 
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">11 AVRIL 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES JEUNES ARTISTES DE M. COMTE</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="MNS15" modus="sp" lm_max="8" metProfile="2, 3, 6, 8, (4), (5)">
	<head type="main">CHOEUR.</head>
	<head type="tune">Air de la Tarentèle.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="2" met="2"><w n="1.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, </l>
		<l n="2" num="1.2" lm="2" met="2"><w n="2.1" punct="vg:2">C<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, </l>
		<l n="3" num="1.3" lm="3" met="3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="3.2" punct="vg:3">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="2">ou</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="vg">on</seg>s</w>, </l>
		<l n="4" num="1.4" lm="3" met="3"><w n="4.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="4.2">f<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w></l>
		<l n="5" num="1.5" lm="6" met="6"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="5.4" punct="vg:6">r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="vg">on</seg></w>, </l>
		<l n="6" num="1.6" lm="6" met="6"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="6.3">j<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6" punct="pv:6"><seg phoneme="a" type="vs" value="1" rule="339" place="6">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></w> ; </l>
		<l n="7" num="1.7" lm="4"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="7.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>rs</w></l>
		<l n="8" num="1.8" lm="6" met="6"><w n="8.1">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="8.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="8.3">n<seg phoneme="o" type="vs" value="1" rule="437" place="4">o</seg>s</w> <w n="8.4" punct="vg:6">l<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg">i</seg>rs</w>, </l>
		<l n="9" num="1.9" lm="6" met="6"><w n="9.1">C</w>'<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="9.3" punct="vg:3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>g<seg phoneme="e" type="vs" value="1" rule="408" place="3" punct="vg">é</seg></w>, <w n="9.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="9.5">d</w>'<w n="9.6" punct="vg:6"><seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>vr<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>, </l>
		<l n="10" num="1.10" lm="5"><w n="10.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="10.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="3">o</seg>s</w> <w n="10.3" punct="pt:5">d<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="5" punct="pt">i</seg>rs</w>. </l>
	</lg>
	<lg n="2">
		<head type="speaker">HÉLÈNE.</head>
		<l n="11" num="2.1" lm="8" met="8"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">tr<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306" place="4">a</seg>il</w> <w n="11.4">s<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>t</w> <w n="11.5" punct="vg:8"><seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="vg">é</seg></w>, </l>
		<l n="12" num="2.2" lm="8" met="8"><w n="12.1">Qu</w>'<w n="12.2"><seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rd</w>'<w n="12.3">hu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="12.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="4">en</seg></w> <w n="12.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="12.7" punct="pv:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="358" place="7">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ; </l>
		<l n="13" num="2.3" lm="8" met="8"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">em</seg>ps</w> <w n="13.3">qu</w>'<w n="13.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="13.5">d<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="13.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="13.8">f<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>l<seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
		<l n="14" num="2.4" lm="8" met="8"><w n="14.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w> <w n="14.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>rs</w> <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">mi<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="14.5" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">em</seg>pl<seg phoneme="wa" type="vs" value="1" rule="439" place="7">o</seg>y<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pt">é</seg></w>.</l>
	</lg>
</div></body></text></TEI>