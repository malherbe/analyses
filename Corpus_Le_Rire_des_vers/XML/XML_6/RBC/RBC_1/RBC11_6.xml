<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE III.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC11" modus="cp" lm_max="10" metProfile="8, 5, 4+6">
				<head type="main">LE RÉMOULEUR</head>
				<head type="tune">AIR de Turenne.</head>
				<lg n="1">
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="1.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="2" mp="C">o</seg>s</w> <w n="1.3">b<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>lʼv<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>rds</w><caesura></caesura> <w n="1.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="C">on</seg></w> <w n="1.5">rʼs<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>blʼ</w> <w n="1.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="1.7" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<seg phoneme="y" type="vs" value="1" rule="456" place="10">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg>s</w> ;</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="2.3" punct="vg:4">gu<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="2.4">gr<seg phoneme="a" type="vs" value="1" rule="339" place="5">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6" mp="P">à</seg></w> <w n="2.6" punct="vg:7">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="7" punct="vg">eu</seg></w>, <w n="2.7">n<seg phoneme="o" type="vs" value="1" rule="437" place="8" mp="C">o</seg>s</w> <w n="2.8" punct="pv:10">bl<seg phoneme="e" type="vs" value="1" rule="352" place="9" mp="M">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="pv">é</seg>s</w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="3.3">d<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="338" place="4">a</seg>y<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg></w> <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>tʼs</w> <w n="3.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="3.6" punct="pv:8">r<seg phoneme="y" type="vs" value="1" rule="456" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="4.2">r<seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rb<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4" caesura="1">è</seg>rʼs</w><caesura></caesura> <w n="4.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg></w> <w n="4.5" punct="vg:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>pl<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>c<seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="vg">é</seg>s</w>,</l>
					<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="5.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="5.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6" mp="C">e</seg>s</w> <w n="5.6">c<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="314" place="8">eau</seg>x</w> <w n="5.7" punct="pv:10">c<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="pv">é</seg>s</w> ;</l>
					<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="4"></space><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="6.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="4">ein</seg>t</w> <w n="6.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>tʼs</w> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="6.6" punct="vg:8">b<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="7.3">rʼpl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="7.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>s</w> <w n="7.5">d</w>'<w n="7.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rbr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="449" place="10" punct="pv">u</seg></w> ;</l>
					<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="4"></space><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="8.2">dʼpu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="8.3">ju<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="8.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="8.5">n</w>'<w n="8.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="8.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>s</w> <w n="8.8">p<seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg></w></l>
					<l n="9" num="1.9" lm="8" met="8"><space unit="char" quantity="4"></space><w n="9.1">R<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>st<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>l<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="9.3" punct="pt:8">r<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
<p>
LA LIBERTÉ.<lb></lb>
Allons, messieurs les auteurs, si vous ne faites pas à présent de
bonnes pièces, ce sera votre faute... Les sujets ne vous manqueront
plus.... et comme désormais c'est sous mon influence que vous allez
travailler, je vous promets de vous faire part de tous les travers, de
tous les ridicules qui s'offriront à moi... de même que des vertus
que je rencontrerai par hasard.<lb></lb>

LE RÉMOULEUR,<lb></lb>

Madame la Liberté, maintenant que je suis mon maître, voulez-vous
me prendre à votre service ?<lb></lb>

LA LIBERTÉ<lb></lb>

Volontiers, mon garçon !<lb></lb>
(On reprend en chœur :)
</p>
				<lg n="2">
					<l n="10" num="2.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="10.2" punct="pe:4">L<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="pe">é</seg></w> ! <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="10.4" punct="pe:8">L<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pe">é</seg></w> !</l>
					<l n="11" num="2.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="11.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="11.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>s</w> <w n="11.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="11.5" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>d<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="8">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="12" num="2.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="12.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="1">en</seg>t</w> <w n="12.2">d</w>'<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r</w> <w n="12.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="12.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="12.6" punct="vg:8">th<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg><seg phoneme="a" type="vs" value="1" rule="339" place="8">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="13" num="2.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="13.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="13.2">d</w>'<w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>tr<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="13.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="13.6" punct="dp:8">g<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="dp">é</seg></w> :</l>
					<l n="14" num="2.5" lm="5" met="5"><space unit="char" quantity="10"></space><w n="14.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="14.3" punct="pe:5">g<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="5" punct="pe">é</seg></w> ! <del type="repetition" reason="analysis" hand="LG">(bis.)</del></l>
					<l n="15" num="2.6" lm="5" met="5"><space unit="char" quantity="10"></space><subst type="repetition" reason="analysis" hand="LG"><del> </del><add rend="hidden"><w n="15.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="15.3" punct="pe:5">g<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="5" punct="pe">é</seg></w> !</add></subst></l>
				</lg>
			</div></body></text></TEI>