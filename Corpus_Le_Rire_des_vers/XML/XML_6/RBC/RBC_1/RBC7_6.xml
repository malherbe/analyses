<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE III.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC7" modus="sp" lm_max="8" metProfile="8, 5">
				<head type="tune">AIR de la Mansarde des Artistes.</head>
				<lg n="1">
					<head type="main">LA LIBERTÉ.</head>
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="1.2" punct="pe:4">L<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="pe">é</seg></w> ! <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="1.4" punct="pe:8">L<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pe">é</seg></w> !</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="2.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>s</w> <w n="2.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="2.5" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>d<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="8">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="1">en</seg>t</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r</w> <w n="3.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="3.5" punct="dp:8">th<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg><seg phoneme="a" type="vs" value="1" rule="339" place="8">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg>s</w> :</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="4.2">d</w>'<w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>tr<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="4.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="4.6" punct="pv:8">g<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pv">é</seg></w> ;</l>
					<l n="5" num="1.5" lm="5" met="5"><space unit="char" quantity="6"></space><w n="5.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="5.3" punct="pe:5">g<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="5" punct="pe">é</seg></w> ! <del reason="analysis" hand="LG" type="repetition">(bis.)</del></l>
					<l n="6" num="1.6" lm="5" met="5"><space unit="char" quantity="6"></space><subst reason="analysis" hand="LG" type="repetition"><del> </del><add rend="hidden"><w n="6.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="6.3" punct="pe:5">g<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="5" punct="pe">é</seg></w> !</add></subst></l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="7.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="2">en</seg>t</w> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="7.4">n<seg phoneme="o" type="vs" value="1" rule="437" place="5">o</seg>s</w> <w n="7.5">j<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</w> <w n="7.6">f<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="8">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">In</seg>tr<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>du<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="8.3" punct="pi:8">v<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pi ps">é</seg></w> ?…</l>
				</lg>
				<lg n="2">
					<head type="main">TOUS.</head>
					<l n="9" num="2.1" lm="8" met="8"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="9.2" punct="pe:4">L<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="pe">é</seg></w> ! <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="9.4" punct="pe:8">L<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pe">é</seg></w> !</l>
				</lg>
			</div></body></text></TEI>