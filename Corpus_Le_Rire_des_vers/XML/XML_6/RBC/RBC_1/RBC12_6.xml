<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VARIÉTÉS DE 1830</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="RGM" sort="1">
					<name>
						<forename>Michel-Nicolas</forename>
						<surname>BALISSON DE ROUGEMONT</surname>
					</name>
					<date from="1781" to="1840">1781-1840</date>
				</author>
				<author key="BRA" sort="2">
					<name>
						<forename>Nicolas</forename>
						<surname>BRAZIER</surname>
					</name>
					<date from="1783" to="1838">1783-1838</date>
				</author>
				<author key="COU" sort="3">
					<name>
						<forename>Frédéric</forename>
						<nameLink>de</nameLink>
						<surname>COURCY</surname>
					</name>
					<date from="1795" to="1862">1795-1862</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>401 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">RBC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les variétés de 1830</title>
						<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL">https://books.google.ch/books?vid=BL:A0021456859</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les variétés de 1830.</title>
								<author>BALISSON DE ROUGEMONT, BRAZIER ET DE COURCY</author>
								<repository>The British Library</repository>
								<idno type="URI">http://access.bl.uk/item/viewer/ark:/81055/vdc_100033600121.0x000001#?c=0</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BARBA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-16" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SCÈNE IV.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="RBC12" modus="sm" lm_max="8" metProfile="8">
				<head type="tune">AIR : Vaudeville de Haine aux femmes.</head>
				<lg n="1">
					<head type="main">COURBETTE.</head>
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="6">en</seg></w> <w n="1.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r</w> <w n="1.6" punct="vg:8">m<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="vg">oi</seg></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3" punct="vg:4">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="vg">ai</seg>s</w>, <w n="2.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="2.5">l</w>'<w n="2.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>v<seg phoneme="u" type="vs" value="1" rule="424" place="7">oû</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>s</w>-<w n="2.7" punct="pi:8">j<seg phoneme="ə" type="ef" value="1" rule="e-1" place="9" punct="pi">e</seg></w> ?</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="3.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="3.3" punct="vg:3">f<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg>ls</w>, <w n="3.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="3.5">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rt</w> <w n="3.6">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="3.7" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="434" place="7">o</seg>ll<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Tr<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="2">er</seg></w> <w n="4.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3">m<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4" punct="pv:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">em</seg>pl<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pv">oi</seg></w> ;</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">Qu<seg phoneme="wa" type="vs" value="1" rule="280" place="1">oi</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">d</w>'<w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="5.4">h<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="5.5" punct="vg:8">c<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>ct<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>s</w> ! <w n="6.2">c</w>'<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="6.5">p<seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.6" punct="vg:8">s<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="189" place="8" punct="vg">e</seg>t</w>,</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">n</w>'<w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="7.6">pu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>s</w> <w n="7.7">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="7">en</seg></w> <w n="7.8">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">J</w>'<w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">en</seg></w> <w n="8.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="8.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="8.6">s<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w>-<w n="8.7" punct="pt:8">pr<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="189" place="8" punct="pt">e</seg>t</w>.</l>
				</lg>
			</div></body></text></TEI>