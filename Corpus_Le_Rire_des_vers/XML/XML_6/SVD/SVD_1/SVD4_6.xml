<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">COMMÈRE JEANNETON</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>261 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE VII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD4" modus="sp" lm_max="5" metProfile="5, 4">
				<head type="tune">Air du Voyage de la Mariée.</head>
				<lg n="1">
					<head type="main">DELAUNAY.</head>
					<l n="1" num="1.1" lm="5" met="5"><w n="1.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="1.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="1.4" punct="pe:5">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="pe">a</seg>s</w> !</l>
					<l n="2" num="1.2" lm="4" met="4"><space unit="char" quantity="2"></space><w n="2.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="2.2">d</w>'<w n="2.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">em</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg>s</w>,</l>
					<l n="3" num="1.3" lm="4" met="4"><space unit="char" quantity="2"></space><w n="3.1" punct="vg:2">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="vg">ez</seg></w>, <w n="3.2" punct="vg:4">J<seg phoneme="a" type="vs" value="1" rule="309" place="3">ea</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="5" met="5"><w n="4.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>t</w> <w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="4.3"><seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg></w></l>
					<l n="5" num="1.5" lm="4" met="4"><space unit="char" quantity="2"></space><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="5.2">l</w>'<w n="5.3" punct="pv:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="pv">é</seg></w> ;</l>
					<l n="6" num="1.6" lm="4" met="4"><space unit="char" quantity="2"></space><w n="6.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="6.3" punct="pt:4">d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>scr<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<head type="main">JEANNETON.</head>
					<l n="7" num="2.1" lm="5" met="5"><w n="7.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">m<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="3">en</seg>t</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="7.4" punct="vg:5">d<seg phoneme="u" type="vs" value="1" rule="424" place="5" punct="vg">ou</seg>x</w>,</l>
					<l n="8" num="2.2" lm="4" met="4"><space unit="char" quantity="2"></space><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="8.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>s</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4" punct="pt:4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="pt">ou</seg>s</w>.</l>
					<l n="9" num="2.3" lm="4" met="4"><space unit="char" quantity="2"></space><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3" punct="dp:4">r<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="dp">e</seg></w> :</l>
					<l n="10" num="2.4" lm="5" met="5"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4" punct="vg:5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="4">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5" punct="vg">ai</seg>ts</w>,</l>
					<l n="11" num="2.5" lm="4" met="4"><space unit="char" quantity="2"></space><w n="11.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="2">en</seg>t</w> <w n="11.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>s</w></l>
					<l n="12" num="2.6" lm="4" met="4"><space unit="char" quantity="2"></space><w n="12.1">P<seg phoneme="ɛ" type="vs" value="1" rule="338" place="1">a</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="2">er</seg></w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="12.3" punct="pe:4">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></w> !</l>
				</lg>
				<lg n="3">
					<head type="main">DELAUNAY</head>
					<l n="13" num="3.1" lm="5" met="5"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="3">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w>-<w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w></l>
					<l n="14" num="3.2" lm="5" met="5"><w n="14.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="14.2">j</w>'<w n="14.3"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>bti<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="3">en</seg>s</w> <w n="14.4">d<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w></l>
					<l n="15" num="3.3" lm="4" met="4"><space unit="char" quantity="2"></space><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="15.2" punct="pv:4">r<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">om</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pv">e</seg></w> ;</l>
					<l n="16" num="3.4" lm="5" met="5"><w n="16.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="16.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">pr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>x</w> <w n="16.4">fl<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>tt<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>r</w></l>
					<l n="17" num="3.5" lm="5" met="5"><w n="17.1">C</w>'<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="17.3">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="17.4">b<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>r</w></l>
					<l n="18" num="3.6" lm="4" met="4"><space unit="char" quantity="2"></space><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3" punct="pt:4">s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></w>.</l>
				</lg>
				<div n="1" type="section">
					<head type="main">ENSEMBLE. |</head>
					<lg n="1">
						<head type="main">JEANNETON.</head>
						<l n="19" num="1.1" lm="5" met="5"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.4" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="vg">a</seg>s</w>,</l>
						<l n="20" num="1.2" lm="4" met="4"><space unit="char" quantity="2"></space><w n="20.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="20.2">d</w>'<w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">em</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w></l>
						<l n="21" num="1.3" lm="4" met="4"><space unit="char" quantity="2"></space><w n="21.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="21.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r</w> <w n="21.3">J<seg phoneme="a" type="vs" value="1" rule="309" place="3">ea</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
						<l n="22" num="1.4" lm="5" met="5"><w n="22.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>t</w> <w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="22.3"><seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg></w></l>
						<l n="23" num="1.5" lm="4" met="4"><space unit="char" quantity="2"></space><w n="23.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="23.2">l</w>'<w n="23.3" punct="pv:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="pv">é</seg></w> ;</l>
						<l n="24" num="1.6" lm="4" met="4"><space unit="char" quantity="2"></space><w n="24.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="24.3" punct="pt:4">d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>scr<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<head type="main">DELAUNAY.</head>
						<l n="25" num="2.1" lm="5" met="5"><w n="25.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="25.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="25.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="25.4" punct="pe:5">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="pe">a</seg>s</w> !</l>
						<l n="26" num="2.2" lm="4" met="4"><space unit="char" quantity="2"></space><w n="26.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="26.2">d</w>'<w n="26.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">em</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg>s</w>,</l>
						<l n="27" num="2.3" lm="4" met="4"><space unit="char" quantity="2"></space><w n="27.1" punct="vg:2">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="vg">ez</seg></w>, <w n="27.2" punct="vg:4">J<seg phoneme="a" type="vs" value="1" rule="309" place="3">ea</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></w>,</l>
						<l n="28" num="2.4" lm="5" met="5"><w n="28.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>t</w> <w n="28.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="28.3"><seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg></w></l>
						<l n="29" num="2.5" lm="4" met="4"><space unit="char" quantity="2"></space><w n="29.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="29.2">l</w>'<w n="29.3" punct="pv:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="pv">é</seg></w> ;</l>
						<l n="30" num="2.6" lm="4" met="4"><space unit="char" quantity="2"></space><w n="30.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="30.3" punct="pt:4">d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>scr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></w>.</l>
					</lg>
				</div>
			</div></body></text></TEI>