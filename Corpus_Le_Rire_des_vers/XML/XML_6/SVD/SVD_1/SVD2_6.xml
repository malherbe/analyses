<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">COMMÈRE JEANNETON</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>261 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE II.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD2" modus="cm" lm_max="10" metProfile="4+6">
				<head type="tune">Air : d'Yelva.</head>
				<lg n="1">
					<head type="main">DELAUNAY.</head>
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">Pu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="1.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="1.3">l</w>'<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4" caesura="1">ai</seg></w><caesura></caesura> <w n="1.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>ç<seg phoneme="y" type="vs" value="1" rule="456" place="6">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="1.7">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="8" mp="C">e</seg>t</w> <w n="1.8" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="P">a</seg>r</w> <w n="2.2">m<seg phoneme="wa" type="vs" value="1" rule="422" place="2">oi</seg></w> <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="2.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="C">i</seg>l</w> <w n="2.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="2.6" punct="pv:10">r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>ct<seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="pv">é</seg></w> ;</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1" mp="C">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="3.3">ch<seg phoneme="e" type="vs" value="1" rule="346" place="3" mp="P">ez</seg></w> <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="4" caesura="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="3.6">d</w>'<w n="3.7"><seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="3.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.9">m</w>'<w n="3.10"><seg phoneme="e" type="vs" value="1" rule="353" place="9" mp="M">e</seg>x<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" mp="M">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6">e</seg>c</w> <w n="4.5">f<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>d<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg></w></l>
					<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>rs</w><caesura></caesura> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="5.5">l</w>'<w n="5.6" punct="pt:10">h<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" mp="M">o</seg>sp<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="pt">é</seg></w>.</l>
					<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="6.2">d</w>'<w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="6.4">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="C">on</seg></w> <w n="6.6">v<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>t</w> <w n="6.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="6.8" punct="vg:10">r<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="M">om</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="7.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="7.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>t</w><caesura></caesura> <w n="7.4">g<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s</w> <w n="7.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="7">en</seg></w> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="7.7" punct="vg:10">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="189" place="10" punct="vg">e</seg>t</w>,</l>
					<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="8.2">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rd</w> <w n="8.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="8.4">dr<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>ts</w><caesura></caesura> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="P">à</seg></w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="8.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>c<seg phoneme="o" type="vs" value="1" rule="434" place="8" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9" mp="M">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
					<l n="9" num="1.9" lm="10" met="4+6"><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" mp="C">on</seg></w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.4">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4" caesura="1">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="P">à</seg></w> <w n="9.6">r<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="M">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r</w> <w n="9.7">d<seg phoneme="y" type="vs" value="1" rule="449" place="8" mp="C">u</seg></w> <w n="9.8" punct="pt:10">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="9" mp="M">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10" punct="pt">ai</seg>t</w>.</l>
				</lg>
			</div></body></text></TEI>