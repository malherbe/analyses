<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANGÉLIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIEME.</head><head type="main_subpart">SCÈNE V.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD33" modus="sp" lm_max="8" metProfile="2, 3, 8, 5, 4">
				<head type="tune">Air des Poletais.</head>
				<lg n="1">
					<head type="main">GRATIEN.</head>
					<l n="1" num="1.1" lm="2" met="2"><space unit="char" quantity="12"></space><w n="1.1">Dʼv<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>t</w> <w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w></l>
					<l n="2" num="1.2" lm="3" met="3"><space unit="char" quantity="10"></space><w n="2.1">T<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="2.2" punct="vg:3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="vg">ou</seg>s</w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="3.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t</w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6" punct="vg:8">sc<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="5" met="5"><space unit="char" quantity="6"></space><w n="4.1"><seg phoneme="y" type="vs" value="1" rule="462" place="1">U</seg>nʼ</w> <w n="4.2">d<seg phoneme="a" type="vs" value="1" rule="342" place="2">a</seg>mʼ</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="4.5">h<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
					<l n="5" num="1.5" lm="4" met="4"><space unit="char" quantity="8"></space><w n="5.1">N</w>'<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>s</w> <w n="5.4">lʼ</w> <w n="5.5" punct="vg:4">P<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg></w>,</l>
					<l n="6" num="1.6" lm="5" met="5"><space unit="char" quantity="6"></space><w n="6.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.2" punct="pt:5">G<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="pt">ou</seg></w>.</l>
				</lg>
				<lg n="2">
					<head type="main">JEANNETON.</head>
					<l n="7" num="2.1" lm="5" met="5"><space unit="char" quantity="6"></space><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>s</w> <w n="7.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4" punct="pt:5">c<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="424" place="5" punct="pt">ou</seg>x</w>.</l>
					</lg>
				<lg n="3">
					<head type="main">MÈRE GIBOU.</head>
					<l n="8" num="3.1" lm="8" met="8"><w n="8.1">Jʼ</w> <w n="8.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="1">eu</seg>x</w> <w n="8.3" punct="vg:3">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="3" punct="vg">er</seg></w>, <w n="8.4">f<seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>t</w> <w n="8.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.6">jʼ</w> <w n="8.7">m</w>'<w n="8.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="8.9" punct="vg:8">r<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="9" num="3.2" lm="5" met="5"><space unit="char" quantity="6"></space><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="9.2">di<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="9.4" punct="vg:5">t<seg phoneme="u" type="vs" value="1" rule="424" place="5" punct="vg">ou</seg>s</w>,</l>
					<l n="10" num="3.3" lm="8" met="8"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ø" type="vs" value="1" rule="404" place="3">eu</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="10.3">ch<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="10.4">ch<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w> <w n="10.5" punct="pt:8">n<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">ou</seg>s</w>.</l>
				</lg>
				<div type="section" n="1">
					<head type="main">ENSEMBLE. {</head>
					<head type="main">MAGLOIRE, <hi rend="ital">parlant à la mère Gibou</hi></head>
					<div type="subsection" n="1">
						<head type="main">REPRISE.</head>
						<lg n="1">
							<head type="main">GRATIEN.</head>
							<l n="11" num="1.1" lm="2" met="2"><space unit="char" quantity="10"></space><w n="11.1">Dʼv<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>t</w> <w n="11.2" punct="vg:2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2" punct="vg">ou</seg>s</w>, <del hand="LG" reason="analysis" type="repetition">etc. etc.</del></l>
							<l n="12" num="1.2" lm="3" met="3"><space unit="char" quantity="10"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="12.1">T<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="12.2" punct="vg:3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="vg">ou</seg>s</w>,</add></subst></l>
							<l n="13" num="1.3" lm="8" met="8"><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="13.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="13.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.6" punct="vg:8">sc<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</add></subst></l>
							<l n="14" num="1.4" lm="5" met="5"><space unit="char" quantity="6"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="14.1"><seg phoneme="y" type="vs" value="1" rule="462" place="1">U</seg>nʼ</w> <w n="14.2">d<seg phoneme="a" type="vs" value="1" rule="342" place="2">a</seg>mʼ</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="14.5">h<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></add></subst></l>
							<l n="15" num="1.5" lm="4" met="4"><space unit="char" quantity="8"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="15.1">N</w>'<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>s</w> <w n="15.4">lʼ</w> <w n="15.5" punct="vg:4">P<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg></w>, </add></subst></l>
							<l n="16" num="1.6" lm="5" met="5"><space unit="char" quantity="6"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="16.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="16.2" punct="pt:5">G<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="pt">ou</seg></w>.</add></subst></l>
						</lg>
						<lg n="2">
							<head type="main">JEANNETON</head>
							<l n="17" num="2.1" lm="2" met="2"><space unit="char" quantity="12"></space><w n="17.1">Dʼv<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>t</w> <w n="17.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w></l>
							<l n="18" num="2.2" lm="3" met="3"><space unit="char" quantity="10"></space><w n="18.1">T<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="18.2" punct="vg:3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="vg">ou</seg>s</w>,</l>
							<l n="19" num="2.3" lm="8" met="8"><w n="19.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="19.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="19.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t</w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="19.6" punct="pv:8">sc<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
							<l n="20" num="2.4" lm="5" met="5"><space unit="char" quantity="6"></space><w n="20.1">D</w>'<w n="20.2"><seg phoneme="y" type="vs" value="1" rule="462" place="1">u</seg>nʼ</w> <w n="20.3">d<seg phoneme="a" type="vs" value="1" rule="342" place="2">a</seg>mʼ</w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="20.6">h<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
							<l n="21" num="2.5" lm="8" met="8"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">n</w>'<w n="21.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg></w> <w n="21.4">n<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="21.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="21.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="21.7">n<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="21.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="21.9" punct="pt:8">g<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">oû</seg>ts</w>.</l>
						</lg>
						<lg n="3">
							<head type="main">MÈRE GIBOU.</head>
							<l n="22" num="3.1" lm="2" met="2"><space unit="char" quantity="12"></space><w n="22.1">Dʼv<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>t</w> <w n="22.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w></l>
							<l n="23" num="3.2" lm="3" met="3"><space unit="char" quantity="10"></space><w n="23.1">T<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="23.2" punct="vg:3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="vg">ou</seg>s</w>,</l>
							<l n="24" num="3.3" lm="8" met="8"><w n="24.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="24.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="24.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t</w> <w n="24.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="24.6" punct="pv:8">sc<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
							<l n="25" num="3.4" lm="5" met="5"><space unit="char" quantity="6"></space><w n="25.1">D</w>'<w n="25.2"><seg phoneme="y" type="vs" value="1" rule="462" place="1">u</seg>nʼ</w> <w n="25.3">d<seg phoneme="a" type="vs" value="1" rule="342" place="2">a</seg>mʼ</w> <w n="25.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="25.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="25.6">h<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
							<l n="26" num="3.5" lm="5" met="5"><space unit="char" quantity="6"></space><w n="26.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="26.2">m<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="26.3">G<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg></w></l>
							<l n="27" num="3.6" lm="4" met="4"><space unit="char" quantity="8"></space><w n="27.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="27.2">l</w>'<w n="27.3"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>r</w> <w n="27.4"><seg phoneme="e" type="vs" value="1" rule="188" place="3">e</seg>t</w> <w n="27.5">lʼ</w> <w n="27.6" punct="pt:4">g<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="pt">oû</seg>t</w>.</l>
						</lg>
						<lg n="4">
							<head type="main">MAGLOIRE.</head>
							<l n="28" num="4.1" lm="2" met="2"><space unit="char" quantity="12"></space><w n="28.1">Dʼv<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>t</w> <w n="28.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w></l>
							<l n="29" num="4.2" lm="3" met="3"><space unit="char" quantity="10"></space><w n="29.1">T<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="29.2" punct="vg:3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="vg">ou</seg>s</w>,</l>
							<l n="30" num="4.3" lm="8" met="8"><w n="30.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="30.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="30.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="30.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t</w> <w n="30.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="30.6" punct="pv:8">sc<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
							<l n="31" num="4.4" lm="5" met="5"><space unit="char" quantity="6"></space><w n="31.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="31.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="2">en</seg>s</w> <w n="31.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="31.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="31.5">h<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
							<l n="32" num="4.5" lm="8" met="8"><w n="32.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="32.2">n</w>'<w n="32.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="32.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="32.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="32.6">n<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="32.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="32.8" punct="pt:8">g<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">oû</seg>ts</w>.</l>
						</lg>
					</div>
				</div>
			</div></body></text></TEI>