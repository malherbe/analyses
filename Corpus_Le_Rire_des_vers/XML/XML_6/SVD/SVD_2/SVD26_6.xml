<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANGÉLIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE DEUXIEME.</head><head type="main_subpart">SCÈNE PREMIÈRE.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD26" modus="cp" lm_max="12" metProfile="5, 8, 6, 6=6">
			<head type="tune">Musique nouvelle de J. Doche.</head>
				<lg n="1">
					<head type="main">CHŒUR.</head>
					<l n="1" num="1.1" lm="5" met="5"><space unit="char" quantity="14"></space><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="1.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="3">e</seg>l</w> <w n="1.4" punct="pe:5">b<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="5" punct="pe">eu</seg>r</w> !</l>
					<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="2.5" punct="pe:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pe">eu</seg>r</w> !</l>
					<l n="3" num="1.3" lm="12" mp7="Fc" met="4+8" mp8="M" mp9="None"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M/mp">A</seg>m<seg phoneme="y" type="vs" value="1" rule="449" place="2" mp="M/mp">u</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="Lp">on</seg>s</w>-<w n="3.2" punct="vg:4">n<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="2">ou</seg>s</w>,<caesura></caesura> <w n="3.3">c<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r</w> <w n="3.4">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="Fc">e</seg></w> <w n="3.5">j<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="10">e</seg>st</w> <w n="3.7" punct="pv:12">s<seg phoneme="y" type="vs" value="1" rule="449" place="11" mp="M">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="12">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
					<l n="4" num="1.4" lm="5" met="5"><space unit="char" quantity="14"></space><w n="4.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">f<seg phoneme="œ" type="vs" value="1" rule="405" place="2">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s</w></l>
					<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>d</w> <w n="5.2">l</w>'<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>r</w> <w n="5.4">p<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>r</w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.7">g<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="5.8" punct="pv:8">fr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pv">ai</seg>s</w> ;</l>
					<l n="6" num="1.6" lm="12" mp6="Pem" met="6−6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r</w> <w n="6.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="5">è</seg>s</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem" caesura="1">e</seg></w><caesura></caesura> <w n="6.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="6.7"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="8">e</seg>st</w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>s</w> <w n="6.9">s<seg phoneme="y" type="vs" value="1" rule="449" place="11" mp="P">u</seg>r</w> <w n="6.10">l</w>'<w n="6.11" punct="pt:12">h<seg phoneme="ɛ" type="vs" value="1" rule="357" place="12">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
				<lg n="2">
					<head type="main">ANGÉLIQUE.</head>
					<l n="7" num="2.1" lm="12" met="6+6"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="P">À</seg></w> <w n="7.2" punct="vg:3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="3" punct="vg">er</seg></w>, <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="P">à</seg></w> <w n="7.4" punct="vg:6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="M">an</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="7.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>t</w> <w n="7.6"><seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg></w> <w n="7.7">n<seg phoneme="u" type="vs" value="1" rule="424" place="10" mp="C">ou</seg>s</w> <w n="7.8" punct="pv:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11" mp="M">en</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
					<l n="8" num="2.2" lm="12" met="6+6"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="8.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="C">ou</seg>s</w> <w n="8.4" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="8.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="M">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg></w> <w n="8.8">d<seg phoneme="y" type="vs" value="1" rule="449" place="10" mp="C">u</seg></w> <w n="8.9" punct="pt:12">v<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
				<lg n="3">
					<head type="main">CHŒUR.</head>
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">A</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2" mp="M">o</seg>pt<seg phoneme="e" type="vs" value="1" rule="408" place="3" punct="pe">é</seg></w> ! <w n="9.2" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" mp="M">o</seg>pt<seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="pe" caesura="1">é</seg></w> !<caesura></caesura> <w n="9.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="9.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="C">ou</seg>s</w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="10" mp="M">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11">on</seg>s</w> <w n="9.6" punct="pt:12">t<seg phoneme="u" type="vs" value="1" rule="424" place="12" punct="pt">ou</seg>s</w>.</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="10.2">v<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="10.3" punct="dp:6">c<seg phoneme="o" type="vs" value="1" rule="443" place="4" mp="M">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>c<seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="dp" caesura="1">er</seg></w> :<caesura></caesura> <w n="10.4">t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7" mp="M/mp">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="Lp">on</seg>s</w>-<w n="10.5" punct="vg:9">n<seg phoneme="u" type="vs" value="1" rule="424" place="9" punct="vg">ou</seg>s</w>, <w n="10.6">t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10" mp="M/mp">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11" mp="Lp">on</seg>s</w>-<w n="10.7" punct="pe:12">n<seg phoneme="u" type="vs" value="1" rule="424" place="12" punct="pe">ou</seg>s</w> !</l>
				</lg>
				<div n="1" type="section">
					<head type="main">ANGÉLIQUE.</head>
					<p>Écoutez, écoutez !</p>
					<div n="1" type="subsection">
						<head type="main">PREMIER COUPLET.</head>
						<lg n="1">
							<l n="11" num="1.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="11.1">S<seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="1">im</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.2">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rg<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="11.4" punct="vg:8">h<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<seg phoneme="o" type="vs" value="1" rule="314" place="8" punct="vg">eau</seg></w>,</l>
							<l n="12" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">c<seg phoneme="œ" type="vs" value="1" rule="248" place="2">œu</seg>r</w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="12.4" punct="vg:4">p<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="vg">ai</seg>x</w>, <w n="12.5">v<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="12.6" punct="dp:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">A</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
							<l n="13" num="1.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>rr<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="13.5" punct="pv:8">ch<seg phoneme="a" type="vs" value="1" rule="339" place="7">â</seg>t<seg phoneme="o" type="vs" value="1" rule="314" place="8" punct="pv">eau</seg></w> ;</l>
							<l n="14" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="14.3" punct="vg:4">j<seg phoneme="œ" type="vs" value="1" rule="406" place="4" punct="vg">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="14.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t</w> <w n="14.6" punct="pv:8">b<seg phoneme="o" type="vs" value="1" rule="314" place="8" punct="pv">eau</seg></w> ;</l>
							<l n="15" num="1.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rs</w> <w n="15.2">s<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="15.4" punct="pt:8">p<seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
							<l n="16" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="1">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="2">ô</seg>t</w> <w n="16.2"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="16.3">lu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="16.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="16.5">d</w>'<w n="16.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">ou</seg>r</w>.</l>
							<l n="17" num="1.7" lm="8" met="8"><space unit="char" quantity="8"></space><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">n</w>'<w n="17.3" punct="vg:2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2" punct="vg">ai</seg></w>, <w n="17.4">d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t</w>-<w n="17.5" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="4" punct="vg">e</seg>llʼ</w>, <w n="17.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.7">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="17.8" punct="pv:8">h<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
							<l n="18" num="1.8" lm="8" met="8"><space unit="char" quantity="8"></space><w n="18.1">B<seg phoneme="o" type="vs" value="1" rule="314" place="1">eau</seg></w> <w n="18.2" punct="vg:2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="vg">a</seg>gʼ</w>, <w n="18.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="18.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="18.6" punct="pt:8">c<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">ou</seg>r</w>.</l>
							<l n="19" num="1.9" lm="8" met="8"><space unit="char" quantity="8"></space><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="19.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">b<seg phoneme="o" type="vs" value="1" rule="314" place="3">eau</seg></w> <w n="19.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="19.5">lu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="19.6" punct="dp:8">r<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
						</lg>
						<lg n="2">
							<l n="20" num="2.1" lm="6" met="6"><space unit="char" quantity="12"></space><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="20.2">qu<seg phoneme="wa" type="vs" value="1" rule="280" place="2">oi</seg></w> <w n="20.3">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rt</w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="20.5" punct="pi:6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>d<seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="pi">eu</seg>r</w> ?</l>
							<l n="21" num="2.2" lm="6" met="6"><space unit="char" quantity="12"></space><w n="21.1">J</w>'<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="1">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="21.3">mi<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="21.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="21.5" punct="vg:6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="351" place="6">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
							<l n="22" num="2.3" lm="6" met="6"><space unit="char" quantity="12"></space><w n="22.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="22.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg></w> <w n="22.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="3">oin</seg>s</w> <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="22.5" punct="vg:6">r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="351" place="6">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
							<l n="23" num="2.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="23.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="23.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg></w> <w n="23.3">pl<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>s</w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="23.5" punct="pe:6">b<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="pe">eu</seg>r</w> !</l>
						</lg>
					</div>
					<div n="2" type="subsection">
						<head type="main">DEUXIÈME COUPLET.</head>
						<lg n="1">
							<l n="24" num="1.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="24.1"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="1">Ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="24.2">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="24.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="24.4"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="5">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="24.6">s<seg phoneme="ɛ" type="vs" value="1" rule="383" place="7">ei</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.7" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="vg">an</seg>s</w>,</l>
							<l n="25" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="25.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>ls</w> <w n="25.2">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="25.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="25.4">d<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>x</w> <w n="25.5" punct="pv:8">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
							<l n="26" num="1.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="26.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1" punct="vg">ai</seg>s</w>, <w n="26.2" punct="pe:3">h<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="pe">a</seg>s</w> ! <w n="26.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="26.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="26.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="8">en</seg>s</w></l>
							<l n="27" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="27.1">V<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="27.2">s<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="27.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="27.4" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="pt">an</seg>s</w>.</l>
							<l n="28" num="1.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="28.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="28.2">f<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>t</w> <w n="28.3">fu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg>r</w> <w n="28.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="5">oin</seg></w> <w n="28.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="28.6" punct="pt:8">v<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
							<l n="29" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="29.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>gʼ</w> <w n="29.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="29.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="29.5">l</w>'<w n="29.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ch<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg>r</w>,</l>
							<l n="30" num="1.7" lm="8" met="8"><space unit="char" quantity="8"></space><w n="30.1">Lu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="30.2">d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t</w> <w n="30.3" punct="dp:4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4" punct="dp">o</seg>rs</w> : <w n="30.4">M<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="30.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">A</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="31" num="1.8" lm="8" met="8"><space unit="char" quantity="8"></space><w n="31.1">Jʼ</w> <w n="31.2">n</w>'<w n="31.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="31.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s</w> <w n="31.5">qu</w>'<w n="31.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="31.7"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="31.8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="31.9">t</w>'<w n="31.10" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="434" place="7">o</seg>ffr<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pt">i</seg>r</w>.</l>
							<p>Prenant la main de Jules.)</p>
							<l n="32" num="1.9" lm="8" met="8"><space unit="char" quantity="8"></space><w n="32.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="32.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="32.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="32.4" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg">ou</seg>r</w>, <w n="32.5"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>llʼ</w> <w n="32.6">lu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="32.7" punct="ps:8">r<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w> …</l>
						</lg>
						<lg n="2">
							<l n="33" num="2.1" lm="6" met="6"><space unit="char" quantity="12"></space><w n="33.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">À</seg></w> <w n="33.2">qu<seg phoneme="wa" type="vs" value="1" rule="280" place="2">oi</seg></w> <w n="33.3" punct="vg:3">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" punct="vg">e</seg>rt</w>, <subst hand="LG" reason="analysis" type="repetition"><del>etc.</del><add rend="hidden"><w n="33.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="33.5" punct="pi:6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>d<seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="pi">eu</seg>r</w> ?</add></subst></l>
							<l n="34" num="2.2" lm="6" met="6"><space unit="char" quantity="12"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="34.1">J</w>'<w n="34.2"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="1">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="34.3">mi<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="34.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="34.5" punct="vg:6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="351" place="6">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</add></subst></l>
							<l n="35" num="2.3" lm="6" met="6"><space unit="char" quantity="12"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="35.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="35.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg></w> <w n="35.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="3">oin</seg>s</w> <w n="35.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="35.5" punct="vg:6">r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="351" place="6">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>, </add></subst></l>
							<l n="36" num="2.4" lm="6" met="6"><space unit="char" quantity="12"></space><subst hand="LG" reason="analysis" type="repetition"><del> </del><add rend="hidden"><w n="36.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="36.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg></w> <w n="36.3">pl<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>s</w> <w n="36.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="36.5" punct="pe:6">b<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="pe">eu</seg>r</w> !</add></subst></l>
						</lg>
					</div>
				</div>
			</div></body></text></TEI>