<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANGÉLIQUE</title>
				<title type="corpus">Le Rire des vers</title>
				<title type="medium">Édition électronique</title>
				<author key="SAI" sort="1">
					<name>
						<forename>Joseph-Xavier</forename>
						<surname>BONIFACE</surname>
						<addName type="pen_name">X.-B. SAINTINE</addName>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<author key="VIL" sort="2">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>VILLENEUVE</surname>
					</name>
					<date from="1801" to="1858">1801-1858</date>
				</author>
				<author key="DPY" sort="3">
					<name>
						<forename>Charles</forename>
						<surname>DUPEUTY</surname>
					</name>
					<date from="1798" to="1865">1798-1865</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte (Le rire de vers, Université de Bâle)</resp>
					<name id="LN">
						<forename>Lara</forename>
						<surname>Nugues</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="LG">
						<forename>Louis-Geoffrey</forename>
						<surname>Gousset</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">SVD_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Angélique et Jeanneton</title>
						<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URL"> https://books.google.ch/books?id=-TJMAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Angélique et Jeanneton</title>
								<author>SAINTINE, DUPEUTY ET VILLENEUVE</author>
								<repository>Österreichische Nationalbibliothek:</repository>
								<idno type="URI"> http://digital.onb.ac.at/OnbViewer/viewer.faces?doc=ABO_%2BZ160879706</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RIGA</publisher>
									<date when="1831">1831</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques :
					corpus "Le Rire des vers", université de Bâle et corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Le signe ʼ (UNICODE : ʼ) est utilisé pour les mots avec une élision du "e" muet interne au mot.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-07" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-18" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ACTE PREMIER.</head><head type="main_subpart">SCÈNE VIII.</head><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="SVD25" modus="sp" lm_max="7" metProfile="7, 5, 6">
			<head type="tune">Air de madame Stockhausen.</head>
				<lg n="1">
					<head type="main">GRATIEN.</head>
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">Su<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="1.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="4">oin</seg></w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="1.6" punct="vg:7">li<seg phoneme="ø" type="vs" value="1" rule="397" place="7" punct="vg">eu</seg>x</w>,</l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg>b<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="vg">ez</seg></w>, <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.4" punct="pt:7">v<seg phoneme="ø" type="vs" value="1" rule="397" place="7" punct="pt">eu</seg>x</w>.</l>
				</lg>
				<lg n="2">
					<head type="main">JEANNETON.</head>
					<l n="3" num="2.1" lm="7" met="7"><w n="3.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="3.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">an</seg>t</w>, <w n="3.3">c</w>'<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="3.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="3.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="3.7" punct="pt:7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="7" punct="pt">en</seg></w>.</l>
				</lg>
				<lg n="3">
					<head type="main">JULES.</head>
					<l n="4" num="3.1" lm="7" met="7"><w n="4.1">C</w>'<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="4.3" punct="vg:3">p<seg phoneme="o" type="vs" value="1" rule="434" place="2">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg>blʼ</w>, <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>s</w> <w n="4.5">jʼ</w> <w n="4.6">n</w>'<w n="4.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="4.8">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>s</w> <w n="4.9" punct="pt:7">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="7" punct="pt">en</seg></w>.</l>
					<l n="5" num="3.2" lm="7" met="7"><w n="5.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg></w> <w n="5.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="5.3" punct="vg:4">pl<seg phoneme="œ" type="vs" value="1" rule="406" place="4" punct="vg">eu</seg>rs</w>, <w n="5.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="5.5" punct="vg:7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="189" place="7" punct="vg">e</seg>ts</w>,</l>
					<l n="6" num="3.3" lm="7" met="7"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="6.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="6.3">s<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rʼ</w> <w n="6.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="6.5" punct="pt:7">j<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7" punct="pt">ai</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<head type="main">ANGÉLIQUE.</head>
					<l n="7" num="4.1" lm="7" met="7"><w n="7.1">F<seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>t</w>-<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="7.3">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>t</w> <w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>r</w></l>
					<l n="8" num="4.2" lm="5" met="5"><space unit="char" quantity="4"></space><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="8.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">pl<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>s</w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5" punct="pi:5">rʼv<seg phoneme="wa" type="vs" value="1" rule="419" place="5" punct="pi">oi</seg>r</w> ?</l>
					<p>Ils vont pour se tendre les mains, Gratien les sépare. )</p>
				</lg>
				<div type="section" n="1">
					<head type="main">ENSEMBLE |</head>
				<lg n="1">
					<head type="main">GRATIEN, JEANNETON.</head>
					<l n="9" num="1.1" lm="7" met="7"><w n="9.1" punct="vg:2">S<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="346" place="2" punct="vg">ez</seg></w>, <w n="9.2" punct="vg:4">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="346" place="4" punct="vg">ez</seg></w>, <w n="9.3">su<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w>-<w n="9.4" punct="vg:7">n<seg phoneme="u" type="vs" value="1" rule="424" place="7" punct="vg">ou</seg>s</w>,</l>
					<l n="10" num="1.2" lm="6" met="6"><space unit="char" quantity="2"></space><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg></w> <w n="10.2">cr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="10.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="10.4" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="vg">ou</seg>x</w>,</l>
					<l n="11" num="1.3" lm="7" met="7"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="11.2">d</w>'<w n="11.3"><seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="11.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="11.6" punct="vg:7">v<seg phoneme="u" type="vs" value="1" rule="424" place="7" punct="vg">ou</seg>s</w>,</l>
					<l n="12" num="1.4" lm="5" met="5"><space unit="char" quantity="4"></space><w n="12.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="12.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="12.3">l</w>'<w n="12.4" punct="pt:5"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424" place="5" punct="pt">ou</seg>x</w>.</l>
				</lg>
				<lg n="2">
					<head type="main">ANGÉLIQUE, JULES.</head>
					<l n="13" num="2.1" lm="7" met="7"><w n="13.1" punct="pe:1">Qu<seg phoneme="wa" type="vs" value="1" rule="280" place="1" punct="pe">oi</seg></w> ! <w n="13.2">r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="13.4">n<seg phoneme="ø" type="vs" value="1" rule="247" place="5">œu</seg>ds</w> <w n="13.5">s<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="13.6" punct="pe:7">d<seg phoneme="u" type="vs" value="1" rule="424" place="7" punct="pe">ou</seg>x</w> !</l>
					<l n="14" num="2.2" lm="6" met="6"><space unit="char" quantity="2"></space><w n="14.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>d<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="14.2">l<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="14.3" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="vg">ou</seg>x</w>,</l>
					<l n="15" num="2.3" lm="7" met="7"><w n="15.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="15.2">pl<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s</w> <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.4">b<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>r</w> <w n="15.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>r</w> <w n="15.6" punct="vg:7">n<seg phoneme="u" type="vs" value="1" rule="424" place="7" punct="vg">ou</seg>s</w>,</l>
					<l n="16" num="2.4" lm="5" met="5"><space unit="char" quantity="4"></space><w n="16.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="16.2">nʼ</w> <w n="16.3">sʼr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="16.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="16.5" punct="pt:5"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424" place="5" punct="pt">ou</seg>x</w>.</l>
					</lg>
				<lg n="3">
					<head type="main">DELAUNAY, MAGLOIRE.</head>
					<l n="17" num="3.1" lm="7" met="7"><w n="17.1">R<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">om</seg>p<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="17.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="17.3">n<seg phoneme="ø" type="vs" value="1" rule="247" place="4">œu</seg>ds</w> <w n="17.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="17.5" punct="vg:7">d<seg phoneme="u" type="vs" value="1" rule="424" place="7" punct="vg">ou</seg>x</w>,</l>
					<l n="18" num="3.2" lm="6" met="6"><space unit="char" quantity="2"></space><w n="18.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>d<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="18.2">l<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="18.3" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="vg">ou</seg>x</w>,</l>
					<l n="19" num="3.3" lm="7" met="7"><w n="19.1" punct="vg:2">H<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="vg">a</seg>s</w>, <w n="19.2">d</w>'<w n="19.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="19.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="19.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="19.6" punct="vg:7">v<seg phoneme="u" type="vs" value="1" rule="424" place="7" punct="vg">ou</seg>s</w>,</l>
					<l n="20" num="3.4" lm="5" met="5"><space unit="char" quantity="4"></space><w n="20.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="20.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="20.3">l</w>'<w n="20.4" punct="pt:5"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424" place="5" punct="pt">ou</seg>x</w>.</l>
				</lg>
			</div>
<p>
(Angelique rentre chez elle, Gratien sort avec Jules, Jeanneton et Groom.)</p>
		</div></body></text></TEI>