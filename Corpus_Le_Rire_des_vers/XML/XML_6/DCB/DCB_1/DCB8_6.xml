<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
				<title type="sub">VAUDEVILLE EN UN ACTE</title>
				<title type="corpus">Le Rire des vers</title>
				<author key="DRT" sort="1">
					<name>
						<forename>Achille</forename>
						<nameLink>d'</nameLink>
						<surname>ARTOIS</surname>
						<addname type="other">ACHILLE</addname>
					</name>
					<date from="1791" to="1868">1791-1868</date>
				</author>
				<author key="CDB" sort="2">
					<name>
						<forename>Jules</forename>
						<surname>CHABOT DE BOUIN</surname>
					</name>
					<date from="1805" to="1857">1805-1857</date>
				</author>
				<editor>Le Rire des vers, Université de Bâle</editor>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML (CRISCO, université de Caen)</resp>
					<name id="KL">
						<forename>Kedi</forename>
						<surname>LI</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Application des programmes de traitement automatique (CRISCO, université de Caen)</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>RENAULT</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>300 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DCB_1</idno>	
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE FILS DU SAVETIER, OU LES AMOURS DE TÉLÉMAQUE</title>
						<author>ACHILLE ET CHABOT DE BOUIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>GOOGLE BOOKS</publisher>
						<idno type="URL">https://books.google.ch/books ?id=y9E-AAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<repository>Princeton University Library</repository>
								<idno type="URL">https://hdl.handle.net/2027/njp.32101072323114</idno>
							</monogr>
						</biblStruct>         
					</sourceDesc>
				</biblFull> 
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">3 OCTOBRE 1832</date>
				<placeName>
					<settlement>THÉÂTRE DES VARIÉTÉS</settlement>
				</placeName>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Ce fichier .xml s'inscrit dans un projet de constitution d'un corpus de textes versifiés pour le traitement automatique des formes et structures métriques.</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties versifiées ont été prioritairement encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée.</p> 
					<p>Les apostrophes U+2019 et U+02BC ont été remplacées par l'apostrophe U+0027 majoritairement présente dans les versions océrisées des pdfs du corpus.</p>
					<p>Le trait d’union a été standardisé à l'aide du caractère U+002D.</p>
					<p>Les erreurs d'impression figurant dans le texte originel ont été corrigées lorsqu'elles étaient susceptibles d'entraver le calcul de la longueur métrique, ainsi que la reconnaissance des rimes.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" type="poem" key="DCB8" modus="sm" lm_max="8" metProfile="8">
	<head type="tune">AIR : Que d'établissemens nouveaux.</head>
	<lg n="1">
		<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="1">eu</seg></w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3" punct="pe:3">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="3" punct="pe">eu</seg></w> ! <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.6">sʼr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s</w> <w n="1.7">h<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg>x</w></l>
		<l n="2" num="1.2" lm="8" met="8"><w n="2.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="2.2">j</w>'<w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="2.4">tʼn<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="2.5" punct="pv:5"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" punct="pv">an</seg>t</w> ; <w n="2.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7" punct="pe:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>t<seg phoneme="a" type="vs" value="1" rule="306" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
	</lg>
	<lg n="2">
	<head type="speaker">TÉLÉMAQUE.</head>
		<l n="3" num="2.1" lm="8" met="8"><w n="3.1">P<seg phoneme="ɛ" type="vs" value="1" rule="409" place="1">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2" punct="vg:4">R<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ch<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg">ou</seg>x</w>, <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="280" place="6">oi</seg></w> <w n="3.4">v<seg phoneme="o" type="vs" value="1" rule="437" place="7">o</seg>s</w> <w n="3.5">y<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg>x</w></l>
		<l n="4" num="2.2" lm="8" met="8"><w n="4.1">F<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w>-<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ls</w> <w n="4.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="4">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg></w> <w n="4.4">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>tt</w>' <w n="4.5" punct="pi:8">v<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>l<seg phoneme="a" type="vs" value="1" rule="306" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
	</lg>
	<lg n="3">
	<head type="speaker">RICHOUX, voyant Télémaque manger.</head>
		<l n="5" num="3.1" lm="8" met="8"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="5.2">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="5.3" punct="vg:3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="3" punct="vg">ain</seg>s</w>, <w n="5.4" punct="vg:5">s<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>vʼti<seg phoneme="e" type="vs" value="1" rule="346" place="5" punct="vg">er</seg></w>, <w n="5.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="5.6" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="189" place="8" punct="vg">e</seg>t</w>,</l>
		<l n="6" num="3.2" lm="8" met="8"><w n="6.1">J</w>'<w n="6.2" punct="tc:1">p<seg phoneme="ø" type="vs" value="1" rule="397" place="1" punct="ti">eu</seg>x</w>—<w n="6.3">t</w>'<w n="6.4"><seg phoneme="i" type="vs" value="1" rule="496" place="2">y</seg></w> <w n="6.5">v<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>r</w> <w n="6.6">cʼt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>r</w> <w n="6.8" punct="pi:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>c<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
	</lg> 
	<lg n="4">
	<head type="speaker">TÉLÉMAQUE, mangeant.</head>
		<l n="7" num="4.1" lm="8" met="8"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="3">en</seg>s</w> <w n="7.3">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="6">en</seg>t</w> <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.5">c</w>'<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="8">e</seg>st</w></l>
		<l n="8" num="4.2" lm="8" met="8"><w n="8.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="8.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg></w> <w n="8.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="3">oin</seg>s</w> <w n="8.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>r</w> <w n="8.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.6">d</w>'<w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="8.8" punct="vg:8">s<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		<l n="9" num="4.3" lm="8" met="8"><w n="9.1">B<seg phoneme="o" type="vs" value="1" rule="314" place="1">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>p</w> <w n="9.2">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="3">oin</seg>s</w> <w n="9.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>r</w> <w n="9.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5">d</w>'<w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="9.7" punct="pt:8">s<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
	</lg> 
</div></body></text></TEI>