<div align="center">
<center><h3>Fichiers XML-TEI des textes analysés</h3></center>
</div>

Un fichier pour chaque poème ou pièce de théâtre.


▪ Les répertoires (chaque répertoire correspond à une étape du traitement) :

- XML_0 : Texte avant analyse
- XML_1 : Découpage en mots (traitement_IW)
- XML_2 : Identification des noyaux syllabiques (traitement_NS)
- XML_3 : Traitement des "e" instables (traitement_EI)
- XML_4 : Traitement des diérèses (traitement_DI)
- XML_5 : Calcul de la longueur métrique des vers (traitement_LM)
- XML_6 : Détermination du profil métrique du poème et du mètre des vers (traitement_CM)
- XML_7 : Appariement des vers en rimes, schémas rimiques (traitement_RI), identification des strophes et de la forme globale du poème (traitement_ST)
- XML_8 : Identification de la PGTC et calcul de l'extension des rimes (traitement_ER)
- XML_9 : Évaluation de la qualité des rimes (traitement_QR)
- XML_10 : Traitement de la ponctuométrie (traitement_PT)

Le répertoire XML_10 peut être vide si l'analyse ponctuométrique n'a pas été faite ;
ce qui est le cas lorsqu'il y a une distribution problématique des signes de ponctuation.

Pour les relevés métriques correspondant à ces fichiers XML-TEI, voir le répertoire
** Relevés_métriques**. Le code du répertoire de l'étape est entre parenthèse dans la liste qui précède
(ex. DI est le répertoire de fichiers CSV correspondant aux fichiers XML-TEI du répertoire XML_0)

- NS	Noyaux syllabiques
- DI	Diérèses
- LM	Longueur métrique des vers
- CM	Profils métriques et mètres des vers
- RI	Appariement des vers en rimes, schémas rimiques
- ST	Forme globale des poèmes
- ER	PGTC et extension des rimes
- QR	Qualité des rimes
- PT	Ponctuométrie
- LG	Dénombrement des strophes
- HE	Liste des vers et liste des mots analysés (Hors Étapes)

