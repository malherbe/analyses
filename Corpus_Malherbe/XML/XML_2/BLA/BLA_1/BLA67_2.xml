<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA67">
					<head type="main">ASPIRATION</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="1.4">qu</w>’<w n="1.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.6">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="1.8">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="1.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.11">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="2.2">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.6">s</w>’<w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.8">r<seg phoneme="ə" type="vi" value="1" rule="351">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
						<l n="3" num="1.3"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.2">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.6">gr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="3.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">L</w>’<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">n</w>’<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="4.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.8">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>str<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.3">qu</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="5.8">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="?" type="va" value="1" rule="162">en</seg>t</w> <w n="5.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.10"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.11">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="6" num="2.2"><w n="6.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="6.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rh<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="6.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="6.7">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="6.8">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
						<l n="7" num="2.3"><w n="7.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">gl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.4">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>, <w n="7.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.9">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="7.10">br<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="8.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">b<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="8.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.8">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.9">d</w>’<w n="8.10"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">J</w>’<w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="9.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="9.6">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.7">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.8">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.9">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.10">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1">R<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.2">d</w>’<w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.4">m<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="10.7">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="10.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> <w n="11.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="12" num="3.4"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.2">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="12.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.7">l</w>’<w n="12.8"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="307">A</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.4">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.8">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="14" num="4.2"><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="14.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="14.4">s</w>’<w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>, <w n="14.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="14.7">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.8">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3">n</w>’<w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.5">qu</w>’<w n="15.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.7">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="15.8">d</w>’<w n="15.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>pr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.10"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.11">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.12">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
						<l n="16" num="4.4"><w n="16.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="16.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.7">l</w>’<w n="16.8">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.9"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="16.10">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.11">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>