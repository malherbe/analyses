<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA42">
					<head type="main">PETIT OISEAU</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">P<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.2"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>, <w n="1.3">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.4"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.2">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="2.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="2.4">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="2.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">S<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.3">fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="3.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">l</w>’<w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="4.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="4.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.7">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="5" num="1.5"><w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.4">v<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="6" num="1.6"><w n="6.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.3">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l</w> <w n="6.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.5">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="6.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.8"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
						<l n="7" num="1.7"><w n="7.1">V<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="7.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="7.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="8" num="1.8"><w n="8.1">D</w>’<w n="8.2"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="8.3">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>-<w n="8.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="8.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="8.7">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.8">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">P<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="9.2"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>, <w n="9.3">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="9.4"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="10" num="2.2"><w n="10.1">C<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="10.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="10.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="11" num="2.3"><w n="11.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.3">cl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="11.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="12" num="2.4"><w n="12.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="12.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="vi" value="1" rule="242">e</seg>nt</w> <w n="12.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w> <w n="12.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> :</l>
						<l n="13" num="2.5"><w n="13.1">C<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="13.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="14" num="2.6"><w n="14.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.6">j</w>’<w n="14.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
						<l n="15" num="2.7"><w n="15.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.2">s</w>’<w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="15.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="15.7">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
						<l n="16" num="2.8"><w n="16.1">R<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">P<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="17.2"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>, <w n="17.3">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="17.4"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="18" num="3.2"><w n="18.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="18.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="18.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="18.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="19" num="3.3"><w n="19.1">C<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="19.4">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr</w>’<w n="19.6"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="20" num="3.4"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="20.2">pr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="20.3">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="20.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="20.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="20.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="21" num="3.5"><w n="21.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.2">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="21.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : « <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="21.6">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="21.7">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="22" num="3.6"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="22.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="22.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.4">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="22.5">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ! »</l>
						<l n="23" num="3.7"><w n="23.1">Fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="23.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="23.3">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w></l>
						<l n="24" num="3.8"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="24.2">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="24.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="24.4">n</w>’<w n="24.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="24.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="24.7">d</w>’<w n="24.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1">P<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="25.2"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>, <w n="25.3">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="25.4"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="26" num="4.2"><w n="26.1">R<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="26.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="26.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.5">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="27" num="4.3"><w n="27.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="27.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.3">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="27.4">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l</w> <w n="27.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="27.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="27.7">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="28" num="4.4"><w n="28.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="28.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="28.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
						<l n="29" num="4.5"><w n="29.1">J</w>’<w n="29.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="29.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="29.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="30" num="4.6"><w n="30.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="30.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> :</l>
						<l n="31" num="4.7"><w n="31.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="31.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="31.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.4">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="31.5">qu</w>’<w n="31.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="31.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="32" num="4.8"><w n="32.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="32.2">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="32.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="32.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>-<w n="32.5">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>