<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR982">
				<head type="main">BALLADE DU CŒUR</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="1.2">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="1.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="1.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sc<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>,</l>
					<l n="2" num="1.2"><w n="2.1">S<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">d</w>’<w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="3.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="3.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="4.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">l</w>’<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="4.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="4.6">l</w>’<w n="4.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="5" num="1.5"><w n="5.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.4">ph<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="6.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.4">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rv<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					<l n="7" num="1.7"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="8" num="1.8"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>, <w n="8.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="8.6">n</w>’<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.8">qu</w>’<w n="8.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.10">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="9.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="9.3">c</w>’<w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>,</l>
					<l n="10" num="2.2"><w n="10.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="10.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.7">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="11" num="2.3"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.4">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="11.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>.</l>
					<l n="12" num="2.4"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="12.3">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.4">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="13" num="2.5"><w n="13.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="13.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5">J<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="14" num="2.6"><w n="14.1">C</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.3">l</w>’<w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5">qu</w>’<w n="14.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="14.7">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="14.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					<l n="15" num="2.7"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="15.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="15.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="16" num="2.8"><w n="16.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>, <w n="16.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="16.6">n</w>’<w n="16.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="16.8">qu</w>’<w n="16.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.10">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">J<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="17.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3">r<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w></l>
					<l n="18" num="3.2"><w n="18.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="18.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.3">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="19" num="3.3"><w n="19.1">S</w>’<w n="19.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="19.3">st<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>pp<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="19.4">c</w>’<w n="19.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="19.8">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>.</l>
					<l n="20" num="3.4"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="20.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.3">d</w>’<w n="20.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="21" num="3.5"><w n="21.1">R<seg phoneme="e" type="vs" value="1" rule="409">é</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="21.2">l</w>’<w n="21.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="21.5">l</w>’<w n="21.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="22" num="3.6"><w n="22.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="22.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>, <w n="22.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="22.6">s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w>,</l>
					<l n="23" num="3.7"><w n="23.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="23.2">t<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="23.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.4">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.6">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="24" num="3.8"><w n="24.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="24.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>, <w n="24.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="24.6">n</w>’<w n="24.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="24.8">qu</w>’<w n="24.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.10">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w>.</l>
				</lg>
				<lg n="4">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1"><w n="25.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="25.2">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="25.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="25.5">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="25.7">m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="26" num="4.2"><w n="26.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="26.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.4"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="26.5">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="27" num="4.3"><w n="27.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> ! <w n="27.2">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="27.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.5">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="28" num="4.4"><w n="28.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="28.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>, <w n="28.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="28.6">n</w> <w n="28.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="28.8">qu</w>’<w n="28.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="28.10">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w>.</l>
				</lg>
			</div></body></text></TEI>