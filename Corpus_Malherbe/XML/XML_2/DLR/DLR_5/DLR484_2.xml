<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA MER</head><div type="poem" key="DLR484">
					<head type="main">APOSTROPHE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.3">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="1.4">p<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="1.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="1.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.7">fl<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>ts</w>, <w n="1.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.9">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.10">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.3">n</w>’<w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.5">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="2.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.8">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.9">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="2.10">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
						<l n="3" num="1.3"><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="3.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w>, <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.7">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> ! <w n="3.8">C</w>’<w n="3.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.10">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="3.11">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.12">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.4">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5">t</w>’<w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="4.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="5.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="5.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="5.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="6" num="2.2"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="6.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="6.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="6.6">d</w>’<w n="6.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.9">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="6.10"><seg phoneme="y" type="vs" value="1" rule="450">u</seg>rg<seg phoneme="?" type="va" value="1" rule="162">en</seg>t</w>.</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="7.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.7">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="7.8">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.9">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.10">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="7.11"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.12">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.13">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="8" num="2.4"><w n="8.1">C</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.5">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.7">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="8.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.9">l</w>’<w n="8.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="9.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="9.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.4">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="9.7"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.8"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w>-<w n="9.9">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.10">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.6"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.8">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.9">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="10.10">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.4">t</w>’<w n="11.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="11.6">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="11.7">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="11.8"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="11.9">f<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2">n</w>’<w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="12.4">qu</w>’<w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.6">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.9">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.5">f<seg phoneme="ɛ̃" type="vs" value="1" rule="303">aim</seg></w> <w n="13.6"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.9">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> :</l>
						<l n="14" num="4.2"><w n="14.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="14.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="14.4">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="14.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.7">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="14.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.9">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
						<l n="15" num="4.3">— <w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="15.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.8">m<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t</w> <w n="15.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.10">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
						<l n="16" num="4.4"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="16.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="16.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="16.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="16.9">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="17.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="17.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="17.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="17.7">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="17.8">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>,</l>
						<l n="18" num="5.2"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="18.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="18.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="18.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>, <w n="18.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.7">b<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.9">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.10">ph<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="19" num="5.3"><w n="19.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.3">gu<seg phoneme="e" type="vs" value="1" rule="353">e</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.5">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="19.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.7">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.8">h<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.3">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="21.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="21.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="21.8">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>squ<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="21.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.10">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> !</l>
						<l n="22" num="6.2"><w n="22.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="22.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="22.3">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="22.4">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="22.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="22.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="22.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.9">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="23" num="6.3"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.2"><seg phoneme="e" type="vs" value="1" rule="170">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="23.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="23.4">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="23.5"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="23.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.7">br<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="24" num="6.4"><w n="24.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="24.2">t</w>’<w n="24.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="24.4">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="24.5">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="24.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.8">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="24.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.11">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> !</l>
					</lg>
				</div></body></text></TEI>