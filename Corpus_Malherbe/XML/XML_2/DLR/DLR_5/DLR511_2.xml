<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA ROUTE</head><div type="poem" key="DLR511">
					<head type="main">INSCRIPTION FUNÈBRE</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.3">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="1.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.7">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rth<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="2.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="2.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ph<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="3.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">tr<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="3.6">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lcr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">J</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="4.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.4">l</w>’<w n="4.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6">d</w>’<w n="4.7"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>nn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space><w n="5.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="5.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">Sc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.5">R<seg phoneme="ɔ" type="vs" value="1" rule="441">o</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="6.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="6.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.6">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.7">d</w>’<w n="6.8">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.5">s</w>’<w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>.</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="8.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.8">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.9">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>…</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="9.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="170">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="9.7">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="9.8">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.9">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> !</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="10.2">n</w>’<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="10.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>, <w n="10.8">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.9">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="10.10">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.11">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space><w n="11.1">V<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="11.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="11.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="11.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.5">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="11.6">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.4">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="12.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w>.</l>
					</lg>
				</div></body></text></TEI>