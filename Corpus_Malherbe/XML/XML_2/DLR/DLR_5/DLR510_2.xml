<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA ROUTE</head><div type="poem" key="DLR510">
					<head type="main">DE ROME</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Qu</w>’<w n="1.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="1.3"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="1.4"><seg phoneme="y" type="vs" value="1" rule="251">eû</seg>t</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.6">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="1.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.8">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="1.10"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.11">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.12">ch<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.3">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="2.7">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.9">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="3.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8">n</w>’<w n="3.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="3.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="3.11">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.7">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.9">l</w>’<w n="4.10">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="5" num="1.5"><w n="5.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="j" type="sc" value="0" rule="475">ï</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="6" num="1.6"><w n="6.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">R<seg phoneme="ɔ" type="vs" value="1" rule="441">o</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="6.3">qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="6.4">qu</w>’<w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="6.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="6.8">qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.9">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="6.10">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.11"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="7" num="1.7"><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>, <w n="7.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="7.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.5">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="7.6">c</w>’<w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.8">l</w>’<w n="7.9"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="8" num="1.8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">c</w>’<w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.6">l</w>’<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>. <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.10">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="9" num="1.9"><w n="9.1">N<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="9.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="9.4">n</w>’<w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="9.6">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="9.8">R<seg phoneme="ɔ" type="vs" value="1" rule="441">o</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.9">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
						<l n="10" num="1.10"><w n="10.1">R<seg phoneme="ɔ" type="vs" value="1" rule="441">o</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>… <w n="10.2">S<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="10.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="10.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.8">g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ?</l>
						<l n="11" num="1.11"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="11.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="11.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="11.7">d</w>’<w n="11.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="12" num="1.12"><w n="12.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="12.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="12.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="12.6">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="12.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="12.8">n<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w></l>
						<l n="13" num="1.13">— <w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>, <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>, <w n="13.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="13.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.5">j</w>’<w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="13.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.8"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="13.9">d</w>’<w n="13.10"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="14" num="1.14"><w n="14.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>-<w n="14.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="14.6">l</w>’<w n="14.7"><seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="14.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> ?</l>
						<l n="15" num="1.15"><w n="15.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.7">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="15.8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>,</l>
						<l n="16" num="1.16"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="16.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="16.5">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.7">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.8">r<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>gl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="17" num="1.17"><w n="17.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="17.2">qu</w>’<w n="17.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="17.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bs<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="17.5">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.6">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="17.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
						<l n="18" num="1.18"><w n="18.1">D<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ff<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="18.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="18.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.6">j<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="18.7">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="18.8"><seg phoneme="o" type="vs" value="1" rule="432">o</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>