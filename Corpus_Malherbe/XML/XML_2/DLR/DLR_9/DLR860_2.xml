<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VIII</head><head type="main_part">BONHEURS</head><div type="poem" key="DLR860">
					<head type="main">POMMES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.3">p<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="10"></space><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="2.2"><seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
						<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="3.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rb<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="4" num="1.4"><w n="4.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3">p<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="4.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="5.2">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w>, <w n="5.3">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="10"></space><w n="6.1">C<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="6.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>,</l>
						<l n="7" num="2.3"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.2">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="7.4">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.4">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="8.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">B<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w>, <w n="9.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="9.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="9.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.6">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w></l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="11.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.7">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="12" num="3.4"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.2">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w> <w n="12.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="12.5">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="12.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="12.7">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="13.4">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="13.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
						<l n="15" num="4.3"><w n="15.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="15.3">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="16" num="4.4"><w n="16.1">Qu</w>’<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="16.3">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="16.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="16.5">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="16.6">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>