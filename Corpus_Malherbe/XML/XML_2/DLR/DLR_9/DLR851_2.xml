<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">L’ANGOISSE</head><div type="poem" key="DLR851">
					<head type="main">PRIÈRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>ct</w> <w n="1.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="1.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.5">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="1.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.7">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.8">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.9">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="1.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="1.11">s<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.8">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.9">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="3.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.4">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w>, <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
						<l n="4" num="1.4"><w n="4.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="4.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="4.6">qu</w>’<w n="4.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.8">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.9">cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.3">pr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="5.5">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="5.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ppl<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.8"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="5.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.10">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="6.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.7">m</w>’<w n="6.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="6.9">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="7" num="2.3"><w n="7.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <hi rend="ital"><w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></hi></l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.6">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="8.7">s</w>’<w n="8.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="8.9">s</w>’<w n="8.10"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="8.11">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.12">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="9.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> : <w n="9.7">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.9">qu</w>’<w n="9.10"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.11">s<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>-<w n="9.12">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1">S</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.4">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">h<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>r</w> <w n="10.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="10.9">n</w>’<w n="10.10"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.11">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="10.12">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>.</l>
						<l n="11" num="3.3"><w n="11.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.5">qu</w>’<w n="11.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.7">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="11.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.9">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="11.11"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.12">n</w>’<w n="11.13"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.14">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>.</l>
						<l n="12" num="3.4"><w n="12.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="12.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="12.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.3">l</w>’<w n="13.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="13.5">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.7">n</w>’<w n="13.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.9">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.10">s<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
						<l n="14" num="4.2"><w n="14.1">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="14.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.7">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="15.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.4">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="15.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="15.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.8">l</w>’<w n="15.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.10">n</w>’<w n="15.11"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.12">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="15.13"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>th<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="16" num="4.4"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="16.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="16.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="16.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="16.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.8">dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">J</w>’<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="17.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="17.4">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.6">j</w>’<w n="17.7"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="17.8">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="17.9">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.10">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="17.11">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="17.12"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.13">b<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="18" num="5.2"><w n="18.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>d<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="18.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="18.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="18.5">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
						<l n="19" num="5.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="19.2">s</w>’<w n="19.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="19.4">s</w>’<w n="19.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.6">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="19.7">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.8">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.9">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="19.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>,</l>
						<l n="20" num="5.4"><w n="20.1">T</w>’<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="20.4">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ</w>’<w n="20.5">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="20.6">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.7">n</w>’<w n="20.8"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.9">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="20.10">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">J</w>’<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="21.3">t<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="21.4">d</w>’<w n="21.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="21.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.7">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.8">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.9">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>,</l>
						<l n="22" num="6.2"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="353">E</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="22.2">d</w>’<w n="22.3"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.4">dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="22.6">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>pr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="22.8">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="23" num="6.3"><w n="23.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="23.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="23.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>, <w n="23.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.5">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="23.6">l</w>’<w n="23.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w>-<w n="23.8">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="23.9">n</w>’<w n="23.10"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="23.11">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.12">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="24" num="6.4"><w n="24.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="24.2">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ! <w n="24.3">J</w>’<w n="24.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="24.6">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="24.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="24.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="24.9">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">V<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> ! <w n="25.2">L<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="25.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>, <w n="25.4">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="25.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="26" num="7.2"><w n="26.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="26.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="26.4">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="26.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="26.6">l</w>’<w n="26.7"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>.</l>
						<l n="27" num="7.3"><w n="27.1">D<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="27.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="27.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.4">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="27.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="27.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>,</l>
						<l n="28" num="7.4"><w n="28.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.2">d</w>’<w n="28.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="28.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.5">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="28.6">d</w>’<w n="28.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dm<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ttr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="28.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.10">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>