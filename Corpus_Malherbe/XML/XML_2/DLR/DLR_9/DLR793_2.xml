<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">L’AVENUE</head><div type="poem" key="DLR793">
					<head type="main">CIMETIÈRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.3">f<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="1.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">J</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="3" num="1.3"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="3.3">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="4.3">v<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="4.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.6">l</w>’<w n="4.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w>-<w n="4.8">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.4">l</w>’<w n="5.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="6" num="2.2"><w n="6.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">n</w>’<w n="6.3"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="6.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="6.5">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="6.6">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="6.7">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
						<l n="7" num="2.3"><w n="7.1">N<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="7.2">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.4">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="9.4">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="10.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>.</l>
						<l n="11" num="3.3"><w n="11.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="11.3">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="11.4">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>ts</w>,</l>
						<l n="12" num="3.4"><w n="12.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.2">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="12.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="12.4">l</w>’<w n="12.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6">s</w>’<w n="12.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="13.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.6">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>.</l>
						<l n="14" num="4.2"><w n="14.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="14.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="15" num="4.3"><w n="15.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.2">l</w>’<w n="15.3">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.8">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="16" num="4.4"><w n="16.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="16.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> <w n="16.4">d</w>’<w n="16.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.2">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="17.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="17.4">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="18" num="5.2"><w n="18.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.2">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="18.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
						<l n="19" num="5.3"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="19.2">l</w>’<w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="19.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="19.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>, <w n="20.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="20.3">gr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">C<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="21.2">qu</w>’<w n="21.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="21.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="21.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.8">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="21.9">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w></l>
						<l n="22" num="6.2"><w n="22.1">N</w>’<w n="22.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="22.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="22.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="22.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="22.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
						<l n="23" num="6.3"><w n="23.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="23.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.3">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="24" num="6.4"><w n="24.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="24.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="24.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="24.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="24.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="24.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="25.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.3">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="25.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="25.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="26" num="7.2"><w n="26.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="26.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="26.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.4">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>,</l>
						<l n="27" num="7.3"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="27.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="27.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.4">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="27.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="27.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.7">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>,</l>
						<l n="28" num="7.4"><w n="28.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="28.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="28.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">J</w>’<w n="29.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="29.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="29.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w>, <w n="29.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
						<l n="30" num="8.2"><w n="30.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="30.2">l</w>’<w n="30.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.4"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="30.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.6">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="30.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="30.8">f<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="31" num="8.3"><w n="31.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.2">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="31.3">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="31.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="31.5">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w>,</l>
						<l n="32" num="8.4"><w n="32.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="32.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="32.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="32.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.5">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w>, <w n="33.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="33.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="33.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="33.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
						<l n="34" num="9.2"><w n="34.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="34.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="34.5">cl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="35" num="9.3"><w n="35.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="35.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>-<w n="35.4"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.7">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="36" num="9.4"><w n="36.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.3">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="36.4">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="36.5"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="36.7">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>