<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PAROLES II</head><div type="poem" key="DLR65">
					<ab type="star">*</ab>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.5">r<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="1.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.8">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="16"></space><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="341">Â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="2.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1"><w n="3.1">V<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, <w n="3.4">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.7">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="4" num="2.2"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="341">Â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="4.2"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="4.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1"><w n="5.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="5.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="5.7">qu</w>’<w n="5.8"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="5.9">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.10">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="6" num="3.2"><space unit="char" quantity="8"></space><w n="6.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="6.4">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="7" num="3.3"><space unit="char" quantity="6"></space><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="7.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.8">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					</lg>
					<lg n="4">
						<l n="8" num="4.1"><w n="8.1">F<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="8.2">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="8.3">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w> <w n="8.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="8.5">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="9" num="4.2"><space unit="char" quantity="8"></space><w n="9.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.7">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
						<l n="10" num="4.3"><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="10.2">t</w>’<w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>str<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="11" num="4.4"><space unit="char" quantity="8"></space><w n="11.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.3">m</w>’<w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="11.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?…</l>
					</lg>
				</div></body></text></TEI>