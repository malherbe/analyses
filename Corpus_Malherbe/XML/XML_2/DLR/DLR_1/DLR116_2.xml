<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VESPÉRALES</head><div type="poem" key="DLR116">
					<head type="main">L’INDIFFÉRENTE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="1.6">m</w>’<w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="1.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="2.4">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>, <w n="2.6">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="2.7">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.8">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ff<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="3.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="3.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.5">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="3.6">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sp<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="3.9"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="4.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="4.4">c<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="4.5">d</w>’<w n="4.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="4.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.9">t</w>’<w n="4.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="5.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w>, <w n="5.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.7">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="6.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="6.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.4">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5">bl<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.8">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="6.9">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w></l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">qu</w>’<w n="7.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="7.5">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.6">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="7.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.9">t</w>’<w n="7.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1">R<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="8.4">d</w>’<w n="8.5"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="8.6">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="8.7">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="9.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.4">qu</w>’<w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="9.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.8">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="10.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="10.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="10.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.9">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>,</l>
						<l n="11" num="3.3"><w n="11.1">T</w>’<w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="11.6">l</w>’<w n="11.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>gl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="12" num="3.4"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.3">g<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.6">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="12.7">t<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.8">h<seg phoneme="i" type="vs" value="1" rule="493">y</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="13.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w>, <w n="13.3"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="13.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w>, <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="14" num="4.2"><w n="14.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.3">d<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="14.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.5">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="14.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="14.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ts</w>,</l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="15.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="15.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.6">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.7">pl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="16" num="4.4"><w n="16.1">J</w>’<w n="16.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="16.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="16.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="16.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.8">r<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>.</l>
					</lg>
				</div></body></text></TEI>