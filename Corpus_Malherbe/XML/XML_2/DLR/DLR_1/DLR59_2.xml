<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PAROLES II</head><div type="poem" key="DLR59">
					<head type="main">POUR UNE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="1.5">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="2.2">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="2.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.8">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.5">qu</w>’<w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w></l>
						<l n="4" num="1.4"><w n="4.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="4.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="4.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>lt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.3">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">r<seg phoneme="i" type="vs" value="1" rule="493">y</seg>thm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.8">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="6.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.4">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="6.7">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.8">r<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="7" num="2.3"><w n="7.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="7.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="7.5">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w> <w n="7.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="7.8">ù<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="7.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.10">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="8" num="2.4"><w n="8.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.9">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="9.2">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="9.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.4">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.7">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="10" num="3.2"><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">p<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="10.6">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="10.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>,</l>
						<l n="11" num="3.3"><w n="11.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">d</w>’<w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="11.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="11.7">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.8">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w></l>
						<l n="12" num="3.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="12.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="12.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="12.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.8">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.9">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="12.10">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="13.2">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="13.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.4"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="13.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.6">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="13.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.8">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w></l>
						<l n="14" num="4.2"><w n="14.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="14.2">d</w>’<w n="14.3"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="14.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.5">l</w>’<w n="14.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="14.7">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w> <w n="14.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.9">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.10">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="14.11">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="15" num="4.3"><w n="15.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.2">g<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="15.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="15.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="355">e</seg>x<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="15.8">c<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="16" num="4.4"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.3">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="16.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.8">l</w>’<w n="16.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w>…</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="17.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="17.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="17.5">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.7">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="18" num="5.2"><w n="18.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="18.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.3">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="18.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="19" num="5.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="19.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w>, <w n="19.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="19.4">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="19.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="20" num="5.4"><w n="20.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="20.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="20.4">cl<seg phoneme="o" type="vs" value="1" rule="318">au</seg>str<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="20.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="20.6">pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="20.7">pr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="20.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.9">r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ?</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="21.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.3">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="21.4">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="21.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="21.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="21.7">l</w>’<w n="21.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.9">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>,</l>
						<l n="22" num="6.2"><w n="22.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.2">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="22.3">t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="22.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="22.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="22.7">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ct<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="23" num="6.3"><w n="23.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.2">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="23.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="23.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rb<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.9"><seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="24" num="6.4"><w n="24.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="24.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.3">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="24.6">l</w>’<w n="24.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="24.8">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="24.10">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>,</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="25.2">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="25.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="25.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="25.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="26" num="7.2"><w n="26.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.2">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="26.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.6">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.9">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.10">l<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w></l>
						<l n="27" num="7.3"><w n="27.1">T<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="27.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="27.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="27.7">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.8">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d</w></l>
						<l n="28" num="7.4"><w n="28.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="28.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.3">n</w>’<w n="28.4"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="28.5">s<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="28.6">l</w>’<w n="28.7">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.8">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
					</lg>
				</div></body></text></TEI>