<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VESPÉRALES</head><div type="poem" key="DLR114">
					<head type="main">QUATRIÈME NOCTURNE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="1.6">d</w>’<w n="1.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="i" type="vs" value="1" rule="476">ï</seg>s</w> <w n="1.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
						<l n="2" num="1.2"><w n="2.1">F<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ct<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="2.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rm<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="2.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.8">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
						<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="3.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.5">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="3.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="4.2">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>ps</w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.7">r<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="5.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="5.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="5.5">s</w>’<w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="6.4">s</w>’<w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.6">l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.8">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.9">pl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="7.3">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="7.4">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="7.5"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="7.6">d</w>’<w n="7.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">R<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ds</w> <w n="8.2">d</w>’<w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.5">d</w>’<w n="8.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>qu<seg phoneme="j" type="sc" value="0" rule="489">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t</w> <w n="9.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w>-<w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="9.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.8">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="9.9">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w>,</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>-<w n="10.3">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.5">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.7">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.8">m<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="11.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="12.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.5">s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>cl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.6">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w>.</l>
					</lg>
				</div></body></text></TEI>