<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PAROLES I</head><head type="main_subpart">TRILOGIE DE LA VIE ET DE LA MORT</head><div type="poem" key="DLR36">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sp<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="1.4">dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ps</w> <w n="1.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.6">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="1.7">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
							<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="2.2">pl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="2.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.8">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
							<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="3.3">l</w>’<w n="3.4"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="3.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="3.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.9">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
							<l n="4" num="1.4"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="4.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="4.8">p<seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="5.3">sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lpt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.7">c<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
							<l n="6" num="2.2"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>, <w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ;</l>
							<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="7.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="7.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="7.6">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8">r<seg phoneme="i" type="vs" value="1" rule="493">y</seg>thm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.10">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>,</l>
							<l n="8" num="2.4"><w n="8.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.5">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w>, <w n="9.2">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> !… <w n="9.3">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="9.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.9">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
							<l n="10" num="3.2"><w n="10.1">S<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="10.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="10.4">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.7">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.8">d</w>’<w n="10.9"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
							<l n="11" num="3.3"><w n="11.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="11.4">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="11.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.7">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="11.8">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ctr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="12.2">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="12.3">l</w>’<w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ? <w n="12.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="12.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="12.7">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="12.8">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w>,</l>
							<l n="13" num="4.2"><w n="13.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>f<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ctr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="14" num="4.3"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="14.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.4">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="14.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>, <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.8">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> ?…</l>
						</lg>
					</div></body></text></TEI>