<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ÂME DES RUES</head><div type="poem" key="DLR69">
					<head type="main">GRISAILLE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="1.3">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="1.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="1.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt</w> <w n="1.7">s</w>’<w n="1.8"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="2" num="1.2"><space unit="char" quantity="6"></space><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.5">cl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="3.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="3.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.6">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7">r<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="2.2"><space unit="char" quantity="6"></space><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3">tr<seg phoneme="o" type="vs" value="1" rule="435">o</seg>tt<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="4.4">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6">b<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1"><w n="5.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="5.5">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w> <w n="5.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="5.8">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w></l>
						<l n="6" num="3.2"><space unit="char" quantity="6"></space><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.2">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="6.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> !</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="7.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.4">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> ! <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="7.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.7">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="7.8">h<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="8" num="4.2"><space unit="char" quantity="6"></space><w n="8.1">L</w>’<w n="8.2">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="8.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.6">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1"><w n="9.1">S<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w>, <w n="9.2">b<seg phoneme="a" type="vs" value="1" rule="307">â</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.4">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="9.6">n<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="9.7">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w></l>
						<l n="10" num="5.2"><space unit="char" quantity="6"></space><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.2">s</w>’<w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.4">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.6">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.7">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.8"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> !…</l>
					</lg>
				</div></body></text></TEI>