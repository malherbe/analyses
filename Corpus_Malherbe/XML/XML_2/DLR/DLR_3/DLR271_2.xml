<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE LONG DES JARDINS ET DE L’EAU</head><div type="poem" key="DLR271">
					<head type="main">BOUFFÉE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="1.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="1.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>pr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.4">l</w>’<w n="2.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.6">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1"><w n="3.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">s</w>’<w n="3.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="3.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.8">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="3.9">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="3.10">d</w>’<w n="3.11"><seg phoneme="u" type="vs" value="1" rule="292">aoû</seg>t</w></l>
						<l n="4" num="2.2"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="4.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="4.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="4.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="4.7">cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ds</w> <w n="4.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>.</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1"><w n="5.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.2">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="6" num="3.2"><w n="6.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5">fl<seg phoneme="y" type="vs" value="1" rule="445">û</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.8">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="6.9">tr<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1"><w n="7.1">G<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="7.4">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="8" num="4.2"><w n="8.1">N<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.3">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="8.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="8.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>, <w n="8.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="8.7">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>,</l>
						<l n="9" num="4.3"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="9.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.8">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.9">f<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>…</l>
					</lg>
					<lg n="5">
						<l n="10" num="5.1"><w n="10.1">B<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="10.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w>, <w n="10.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="10.4">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="10.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.7">pr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="11" num="5.2"><w n="11.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="11.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.8">l</w>’<w n="11.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.10">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="12" num="6.1"><w n="12.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="12.5">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="12.8">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="12.9">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w></l>
						<l n="13" num="6.2"><w n="13.1">Tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="13.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="13.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.7">cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ds</w> <w n="13.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>s</w>,</l>
					</lg>
					<lg n="7">
						<l n="14" num="7.1"><w n="14.1">H<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="14.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378">en</seg>dr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="14.4">d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="14.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.6">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="15" num="7.2"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="15.3">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="15.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="15.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					</lg>
					<lg n="8">
						<l n="16" num="8.1"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="16.2">qu</w>’<w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="16.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="16.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="16.7">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>,</l>
						<l n="17" num="8.2"><w n="17.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="17.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="17.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="17.4">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="17.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.6">s</w>’<w n="17.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="17.8">n</w>’<w n="17.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="17.10">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>…</l>
					</lg>
				</div></body></text></TEI>