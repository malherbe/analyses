<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TENDRESSES</head><div type="poem" key="DLR245">
					<head type="main">LES GUETTEURS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="1.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.3">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ; <w n="1.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="1.7">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="1.8">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="2" num="1.2"><w n="2.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>, <w n="2.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.6">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.9">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="4.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="4.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6">l</w>’<w n="4.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.9">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>, <w n="5.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.5">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.9">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w>,</l>
						<l n="6" num="2.2"><w n="6.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="6.3">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="6.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="6.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.8">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="7.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.10">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.11">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w>,</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.7">h<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="ɔ" type="vs" value="1" rule="443">O</seg>r</w>, <w n="9.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="9.4">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="9.5">t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="9.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.9">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w></l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="10.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="10.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.6">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="11" num="3.3"><w n="11.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>, <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="11.3">f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="11.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.5">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="11.6">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="11.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.8">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="12" num="3.4"><w n="12.1">L<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="12.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">ph<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>sph<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="12.4">l</w>’<w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="12.7">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>…</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="13.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="13.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.7">l</w>’<w n="13.8"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.10">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
						<l n="14" num="4.2"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="14.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.5">h<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="14.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="14.7"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="14.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.10">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>,</l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="15.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>… <w n="15.4">T<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>-<w n="15.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="15.6">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="15.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="15.8">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.9">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.10">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="16" num="4.4"><w n="16.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.3">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w> ! <w n="16.4">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="16.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.6">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w> !…</l>
					</lg>
				</div></body></text></TEI>