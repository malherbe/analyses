<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DES BÊTES</head><div type="poem" key="DLR287">
					<head type="main">MATERNITÉ</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rb<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="1.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.9">tr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.6">cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.7">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.8"><seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>f</w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.3">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.7">n<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>f</w></l>
						<l n="4" num="1.4"><w n="4.1">P<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="4.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="4.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="4.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.6">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.7">j<seg phoneme="o" type="vs" value="1" rule="318">au</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.4"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> : <w n="5.5">R<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> ! <w n="5.6">R<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> !</l>
						<l n="6" num="2.2"><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="6.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.6">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>tr<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="6.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="7.2">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="7.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>dr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="8.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.5">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.9"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3">gl<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.5">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>. <w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.7">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w></l>
						<l n="10" num="3.2"><w n="10.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3">bl<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>. <w n="10.4"><seg phoneme="ø" type="vs" value="1" rule="398">Eu</seg>x</w>, <w n="10.5">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ds</w> <w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="10.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.9">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="11" num="3.3"><w n="11.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="11.2">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.5">l</w>’<w n="11.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="12" num="3.4"><w n="12.1">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="12.2">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.4"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="12.5">qu</w>’<w n="12.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="12.7">pr<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="12.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="12.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.10">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">L</w>’<w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.3">s</w>’<w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="13.7">l</w>’<w n="13.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.9">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="13.10"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.11">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="14" num="4.2"><w n="14.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="14.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="14.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="14.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.5">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.6">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w>, <w n="14.8">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="15" num="4.3"><w n="15.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.5">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.6">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.7">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="16" num="4.4"><w n="16.1">S<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="16.4">d</w>’<w n="16.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="16.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="16.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.8"><seg phoneme="ø" type="vs" value="1" rule="247">œu</seg>fs</w>.</l>
					</lg>
				</div></body></text></TEI>