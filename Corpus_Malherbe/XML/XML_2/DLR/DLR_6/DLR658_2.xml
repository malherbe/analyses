<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VIII</head><head type="main_part">DEUILS ROUGES</head><div type="poem" key="DLR658">
					<head type="main">A DES PARENTS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">M<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="1.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5">m<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="1.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.8">n</w>’<w n="1.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="1.11">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>z<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.12"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.8">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !…</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="3.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> ! <w n="3.3">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="3.5">l</w>’<w n="3.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="4.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="4.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="6" num="2.2"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.2">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.5">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.7">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="6.8">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="7.8"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="7.9">bl<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.3">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="10.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.6">l</w>’<w n="10.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.8">t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
						<l n="11" num="3.3">‒ <w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="11.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="11.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.8">fl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">F<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="13.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="13.7">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="13.8">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.9">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w></l>
						<l n="14" num="4.2"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="14.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="14.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="14.4">l</w>’<w n="14.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.6">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>, <w n="14.8"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="14.9">l</w>’<w n="14.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.11">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="14.12"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="14.13">l</w>’<w n="14.14"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.15">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="15" num="4.3"><w n="15.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="15.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="15.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="15.4">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.6">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.2">n</w>’<w n="16.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="16.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="16.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="16.6"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>ts</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="17.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.7">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.9">cl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="18" num="5.2"><w n="18.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="18.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.3">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="18.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="18.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="18.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.8">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
						<l n="19" num="5.3"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.2">cl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="19.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pt<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="19.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="19.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="19.8">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Qu</w>’<w n="20.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="20.6">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.7">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">… <w n="21.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>. <w n="21.2">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="21.3">n</w>’<w n="21.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="21.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="21.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.7">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="21.8">qu</w>’<w n="21.9"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="22" num="6.2"><w n="22.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.2">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="22.4">c</w>’<w n="22.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="22.6">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>.</l>
						<l n="23" num="6.3"><w n="23.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="23.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="23.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="23.4">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="23.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="23.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space>« <w n="24.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>. »</l>
					</lg>
				</div></body></text></TEI>