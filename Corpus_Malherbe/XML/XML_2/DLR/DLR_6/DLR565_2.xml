<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">ADMIRATIONS</head><div type="poem" key="DLR565">
					<head type="main">PAUVRE MORCEAU DE BOIS…</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="1.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="1.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.6">n</w>’<w n="1.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.8">qu</w>’<w n="1.9"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.10">b<seg phoneme="y" type="vs" value="1" rule="445">û</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Qu</w>’<w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.7">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w></l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="3.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="3.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="3.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="3.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.9">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="4.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3">p<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.6">f<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="5.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="5.3">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="5.5">l</w>’<w n="5.6">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="5.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8">l</w>’<w n="5.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.10">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="6.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.4">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="6.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> !</l>
						<l n="7" num="2.3"><w n="7.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="7.3">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="7.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="7.8">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="7.9">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="7.10">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="8.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.5">l</w>’<w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.8">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.9">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="10.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="10.4">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rge<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="10.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1">L</w>’<w n="11.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="11.4">l</w>’<w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.8">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">L</w>’<w n="12.2">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="12.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="12.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="12.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.7">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="12.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.3">qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="13.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="13.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.9">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">D<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="14.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="14.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.4">j<seg phoneme="o" type="vs" value="1" rule="318">au</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="15" num="4.3"><w n="15.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="15.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.4">s<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="15.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="15.6">d</w>’<w n="15.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.8">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="16.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="16.4">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="16.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>…</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="17.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="17.5">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="17.6"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="17.7">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.8">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.9">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.2">s<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="18.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="18.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="18.6">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
						<l n="19" num="5.3"><w n="19.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.3">qu</w>’<w n="19.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.5">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="19.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.7">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="19.8">s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="19.9">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.2">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="20.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.6">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ds</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="21.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.3">n<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="21.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.7">n</w>’<w n="21.8"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w> <w n="21.9">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="21.10">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>, <w n="21.11">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.12">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> !</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1">B<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="22.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="22.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="22.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.6">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>,</l>
						<l n="23" num="6.3"><w n="23.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.2">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="23.3">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w>, <w n="23.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lf<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="23.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="23.7">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="23.8"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">M<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="24.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="24.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="25.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="25.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.4">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="25.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="25.6">qu</w>’<w n="25.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="25.8">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="25.9">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.10">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.11">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space><w n="26.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="26.2">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ge<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.5">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="27" num="7.3"><w n="27.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="27.2">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="27.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="27.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>-<w n="27.6">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="27.7"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="27.8">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.9">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="28.2">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="28.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="28.5">l</w>’<w n="28.6">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.3">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="29.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="30" num="8.2"><space unit="char" quantity="8"></space><w n="30.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="30.2">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="30.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.4">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>.</l>
						<l n="31" num="8.3"><w n="31.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.2">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="31.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="31.4">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="31.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="31.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="31.8">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="31.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="32.2">d</w>’<w n="32.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="32.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="32.5">d</w>’<w n="32.6"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="33.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="33.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="33.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lf<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="33.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="33.7">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.8">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="34" num="9.2"><space unit="char" quantity="8"></space><w n="34.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="34.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="34.3">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rpr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
						<l n="35" num="9.3"><w n="35.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="35.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="35.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.5">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="35.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="35.8">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="35.9">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="36" num="9.4"><space unit="char" quantity="8"></space><w n="36.1">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.2">b<seg phoneme="y" type="vs" value="1" rule="445">û</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="36.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="36.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.2">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="37.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="37.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="37.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="37.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.7">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="37.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="37.9">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="37.10">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="38" num="10.2"><space unit="char" quantity="8"></space><w n="38.1">S<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="38.2">l</w>’<w n="38.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="38.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="38.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="38.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w>,</l>
						<l n="39" num="10.3"><w n="39.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="39.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="39.3">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rv<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="39.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="39.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="39.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="39.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w>,</l>
						<l n="40" num="10.4"><space unit="char" quantity="8"></space><w n="40.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="40.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="40.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="40.6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>