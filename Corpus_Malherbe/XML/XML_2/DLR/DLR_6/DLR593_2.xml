<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">V</head><head type="main_part">ARRIÈRES-SAISONS</head><div type="poem" key="DLR593">
					<head type="main">TOMBEAU</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">t</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="1.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.8">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.9">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="1.10">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="1.11">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.12">j</w>’<w n="1.13"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.4">ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="2.5">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="2.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="3.2">j</w>’<w n="3.3"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="3.4">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.5">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="3.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="3.8">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.9"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="3.10">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">M<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.4">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="4.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="5.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="5.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.8">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">n</w>’<w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rç<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="6.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.7">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.9">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
						<l n="7" num="2.3"><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.6">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.8">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.3">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="8.4">m</w>’<w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="8.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.3">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.7">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.8">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ct<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="10.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="10.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rb<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="10.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="10.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.7">rh<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.2">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="11.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.4">l</w>’<w n="11.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.7">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w>, <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.9">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="12.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="12.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="13.4">br<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="13.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.9">gu<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="14" num="4.2"><w n="14.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.2">ch<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>… <w n="14.6"><seg phoneme="w" type="sc" value="0" rule="431">Ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="14.7">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.8">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="14.9">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.10">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="15.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="15.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="15.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.8">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="15.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.10">h<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="16.2">g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rpr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="17.4">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rt</w> <w n="17.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="17.6">l</w>’<w n="17.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.8">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="18" num="5.2"><w n="18.1">M<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.3">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="18.4">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>. ‒ <w n="18.5">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> !… <w n="18.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="18.7">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.8">d</w>’<w n="18.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w>.</l>
						<l n="19" num="5.3"><w n="19.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> : ‒ <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>s</w>-<w n="19.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.4">t<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="19.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="19.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="19.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.8">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?…</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space>‒ <w n="20.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="20.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="20.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>. <w n="20.4">C</w>’<w n="20.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.6">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="20.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.8">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="20.9">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>.</l>
					</lg>
				</div></body></text></TEI>