<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRADUCTIONS <lb></lb>en vers français <lb></lb>par <lb></lb>LUCIE DELARUE-MARDRUS</head><head type="main_subpart">Six Poèmes d’Emily Brontë</head><div type="poem" key="DLR1121">
						<head type="main">La Visionnaire</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.3">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>. <w n="1.5">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="1.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.8">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>.</l>
							<l n="2" num="1.2"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="2.2">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="2.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.7">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w></l>
							<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="3.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.7">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="3.8">n<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
							<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">l</w>’<w n="4.3">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="4.4">h<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.6">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="4.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.8">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">f<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="5.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rds</w> <w n="5.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.8">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ;</l>
							<l n="6" num="2.2"><w n="6.1">N<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="6.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="6.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.7">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>.</l>
							<l n="7" num="2.3"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="7.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="8" num="2.4"><w n="8.1">L</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5">gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="8.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="9.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.4">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="9.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.8">s<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="10.4">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="10.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.6">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
							<l n="11" num="3.3"><w n="11.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="11.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.6">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rfs</w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.8">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
							<l n="12" num="3.4"><w n="12.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="12.5">s</w>’<w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.7">v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="12.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="12.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.10">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">j</w>’<w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="13.6">h<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.8">l</w>’<w n="13.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>,</l>
							<l n="14" num="4.2"><w n="14.1">F<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="14.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="14.5">s<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="14.6">gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="14.8">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>.</l>
							<l n="15" num="4.3"><w n="15.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.2">m</w>’<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ? <w n="15.4"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="15.6">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.9">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
							<l n="16" num="4.4"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">j</w>’<w n="16.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ffr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.7">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.9">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.10">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> !</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.2">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>, <w n="17.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ; <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="17.7">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.8">dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>.</l>
							<l n="18" num="5.2"><w n="18.1">Ch<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> ! <w n="18.2">L</w>’<w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.5">fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.6">d</w>’<w n="18.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>. <w n="18.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="18.10">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.11">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ?</l>
							<l n="19" num="5.3"><w n="19.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.5">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
							<l n="20" num="5.4"><w n="20.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="20.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.3">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="20.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="20.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ; <w n="20.6">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="20.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="20.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.9">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						</lg>
					</div></body></text></TEI>