<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRADUCTIONS <lb></lb>en vers français <lb></lb>par <lb></lb>LUCIE DELARUE-MARDRUS</head><head type="main_subpart">Six Poèmes d’Emily Brontë</head><div type="poem" key="DLR1122">
						<head type="main">Le Souvenir</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d</w> <w n="1.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.7">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="1.9">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
							<l n="2" num="1.2"><w n="2.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>, <w n="2.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.9">d<seg phoneme="y" type="vs" value="1" rule="445">û</seg></w> <w n="2.10">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.11">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ttr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="3" num="1.3"><w n="3.1">T</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>-<w n="3.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="3.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.6">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.9">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
							<l n="4" num="1.4"><w n="4.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="4.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.5">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.7"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ?</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="5.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="5.4">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="5.6">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.9">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
							<l n="6" num="2.2"><w n="6.1">N<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">s</w>’<w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="6.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.7">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="6.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="7" num="2.3"><w n="7.1">Pl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w>, <w n="7.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.8">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="8" num="2.4"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">br<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.5">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="8.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.7">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> ?</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d</w> <w n="9.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.6">qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>z<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.7">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="9.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.9">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="10" num="3.2"><w n="10.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w>, <w n="10.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>sf<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.6">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>…</l>
							<l n="11" num="3.3"><w n="11.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="11.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.5">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.7">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="12" num="3.4"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="12.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="12.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="12.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> !</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="13.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="13.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="14" num="4.2"><w n="14.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">t</w>’<w n="14.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="14.6">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.8">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> !</l>
							<l n="15" num="4.3"><w n="15.1">D</w>’<w n="15.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w>, <w n="15.4">d</w>’<w n="15.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="15.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w></l>
							<l n="16" num="4.4"><w n="16.1">N<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.2">t</w>’<w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="16.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="16.5">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.7">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.8">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.9">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">N<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.2">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="17.3">n</w>’<w n="17.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="17.6">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="17.7">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> :</l>
							<l n="18" num="5.2"><w n="18.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="18.2">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>, <w n="18.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="18.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="18.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.6">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="18.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.8">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="18.9">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
							<l n="19" num="5.3"><w n="19.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="19.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.3">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.5">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="19.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
							<l n="20" num="5.4"><w n="20.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="20.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.3">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="20.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="20.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="20.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="21.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="21.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="21.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="21.7">d</w>’<w n="21.8"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
							<l n="22" num="6.2"><w n="22.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="22.3">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>-<w n="22.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="22.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="22.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="22.7">br<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="23" num="6.3"><w n="23.1">N</w>’<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="23.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="23.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.5">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="23.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.9">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w></l>
							<l n="24" num="6.4"><w n="24.1">J</w>’<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="24.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="24.4">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="24.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="24.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.7">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="24.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.9">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1"><w n="25.1">J</w>’<w n="25.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="25.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="25.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="25.6">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>,</l>
							<l n="26" num="7.2"><w n="26.1">S<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>vr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="26.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="26.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.8">t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="27" num="7.3"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="27.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="27.4">d</w>’<w n="27.5">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="27.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="27.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.8">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>,</l>
							<l n="28" num="7.4"><w n="28.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="28.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="28.5">m</w>’<w n="28.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="28.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="28.8">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.9">m<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="29.2">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="29.3">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="29.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.5">n</w>’<w n="29.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="29.8">c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
							<l n="30" num="8.2"><w n="30.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="30.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="30.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="30.4">d</w>’<w n="30.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="30.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="30.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="31" num="8.3"><w n="31.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>, <w n="31.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="31.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.4">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="31.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="31.6">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="31.8">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="32" num="8.4"><w n="32.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="32.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="32.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="32.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="32.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ?</l>
						</lg>
					</div></body></text></TEI>