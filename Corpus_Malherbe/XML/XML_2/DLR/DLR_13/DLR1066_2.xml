<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE IV</head><head type="main_part">Musique</head><div type="poem" key="DLR1066">
					<head type="main">Musique</head>
					<head type="number">II</head>
					<head type="sub_2">LA FIGURE DE PROUE, Fasquelle, 1908</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.4">fr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.8">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.9">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="2.4">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.7"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="3" num="1.3"><w n="3.1">F<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w>-<w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="3.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="3.4">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="3.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8">l</w>’<w n="3.9"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="3.10">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.11">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.12">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.5">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6">qu</w>’<w n="4.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.8">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="4.9">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">N</w>’<w n="5.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>-<w n="5.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.5">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ? <w n="5.6">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.7">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="5.9">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="6" num="2.2"><w n="6.1">D</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="6.8">n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="7.2">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="7.3">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="7.6">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !… <w n="7.7">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !…</l>
						<l n="8" num="2.4"><w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>-<w n="8.4">t</w>-<w n="8.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="8.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.8">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="8.9">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.10">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="8.11">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.12">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.13"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> <w n="9.2">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> ! <w n="9.3">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w>-<w n="9.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w>, <w n="9.7">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.8">sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.9">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.3">l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.7">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="10.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>,</l>
						<l n="11" num="3.3"><w n="11.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="11.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.8">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="12" num="3.4"><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="12.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="12.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.9">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>.</l>
					</lg>
				</div></body></text></TEI>