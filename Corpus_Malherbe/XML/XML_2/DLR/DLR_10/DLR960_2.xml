<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">JACQUELINE</head><div type="poem" key="DLR960">
					<head type="number">VIII</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="1.2">Qu</w>’<w n="1.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="1.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>, <w n="1.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.7"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="1.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.10">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.4">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="3.2">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="3.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">b<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="3.6">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.2">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="4.3">d</w>’<w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.5">br<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.6">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="5" num="1.5"><space unit="char" quantity="4"></space><w n="5.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="5.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w>, <w n="5.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="6" num="1.6"><space unit="char" quantity="4"></space><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="6.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="7" num="1.7"><w n="7.1">M<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="7.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.4">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="7.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
						<l n="8" num="1.8"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="8.2">Qu</w>’<w n="8.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="8.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>, <w n="8.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.7"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="8.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.10">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="9" num="1.9"><space unit="char" quantity="4"></space><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="9.3">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>-<w n="9.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="10" num="1.10"><space unit="char" quantity="4"></space><w n="10.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="10.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">j</w>’<w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
						<l n="11" num="1.11"><space unit="char" quantity="4"></space><w n="11.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="11.5">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> ?</l>
					</lg>
				</div></body></text></TEI>