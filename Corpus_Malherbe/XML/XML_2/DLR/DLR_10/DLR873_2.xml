<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">THE SKULL</head><div type="poem" key="DLR873">
					<head type="number">III</head>
					<head type="main">DIALOGUE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">B<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="1.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">J</w>’<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.3">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">cr<seg phoneme="a" type="vs" value="1" rule="341">â</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w></l>
						<l n="3" num="1.3"><space unit="char" quantity="12"></space><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">— <w n="5.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="5.2">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="5.3">j</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="5.5">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>… !</l>
						<l n="6" num="2.2"><w n="6.1">C<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="7" num="2.3"><space unit="char" quantity="12"></space><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.2">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="7.3">r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
						<l n="8" num="2.4">— <w n="8.1">S<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="8.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w>, <w n="8.7">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>scl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="9.2">n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rfs</w>, <w n="9.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="9.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sc<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="10" num="3.2"><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>,</l>
						<l n="11" num="3.3"><space unit="char" quantity="12"></space><w n="11.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>,</l>
						<l n="12" num="3.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">fl<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="13.2">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="13.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.5">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.7">l</w>’<w n="13.8"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w></l>
						<l n="14" num="4.2"><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="14.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="14.6">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="15" num="4.3"><space unit="char" quantity="12"></space><w n="15.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.3">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="16" num="4.4"><w n="16.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w> <w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">B<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="17.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="17.5">t</w>’<w n="17.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="18" num="5.2"><w n="18.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="18.2">s<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="18.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="18.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.7">r<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
						<l n="19" num="5.3"><space unit="char" quantity="12"></space><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="20" num="5.4"><w n="20.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="20.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="21.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="21.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="22" num="6.2"><w n="22.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="22.2">c<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="22.4">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="23" num="6.3"><space unit="char" quantity="12"></space><w n="23.1">R<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>gr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
						<l n="24" num="6.4"><w n="24.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="24.3">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="24.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.5">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="25.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="25.3">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>. <w n="25.4">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="25.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.6">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="25.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.8">M<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>.</l>
						<l n="26" num="7.2"><w n="26.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.2">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="26.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="26.4">l</w>’<w n="26.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bs<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>. <w n="26.6">R<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="27" num="7.3"><space unit="char" quantity="12"></space><w n="27.1">R<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="27.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w></l>
						<l n="28" num="7.4"><w n="28.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="28.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.3">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.4">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="28.6">c<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="29.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> ! <w n="29.3">V<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="29.5">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>,</l>
						<l n="30" num="8.2"><w n="30.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ct</w> <w n="30.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="30.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.4">s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>cl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="30.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="31" num="8.3"><space unit="char" quantity="12"></space><w n="31.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> !</l>
						<l n="32" num="8.4"><w n="32.1">C</w>’<w n="32.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="32.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.4">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="32.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.7"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">— <w n="33.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="33.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="33.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="34" num="9.2"><w n="34.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="34.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="34.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="34.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.5">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="34.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.7">l</w>’<w n="34.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="35" num="9.3"><space unit="char" quantity="12"></space><w n="35.1">V<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="36" num="9.4"><w n="36.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="36.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="36.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="36.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="37.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="37.3">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="37.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="37.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w>,</l>
						<l n="38" num="10.2"><w n="38.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="38.2">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="38.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="38.4">squ<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="39" num="10.3"><space unit="char" quantity="12"></space><w n="39.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="39.2">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w></l>
						<l n="40" num="10.4"><w n="40.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="40.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="40.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="40.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="40.7">gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">J</w>’<w n="41.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="41.3">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="41.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="41.6">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="41.7">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rtr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>.</l>
						<l n="42" num="11.2"><w n="42.1">J</w>’<w n="42.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="42.3">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="42.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="42.5">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="42.6"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ss<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="42.7">st<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="43" num="11.3"><space unit="char" quantity="12"></space><w n="43.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>str<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="43.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bstr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w></l>
						<l n="44" num="11.4"><w n="44.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="44.2">j</w>’<w n="44.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="44.4">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="44.5">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="44.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="44.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="44.8">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">J</w>’<w n="45.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="45.3">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="45.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="45.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="45.6">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="45.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
						<l n="46" num="12.2"><w n="46.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="46.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="46.3">gl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="46.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>str<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="47" num="12.3"><space unit="char" quantity="12"></space><w n="47.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="47.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w>,</l>
						<l n="48" num="12.4"><w n="48.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="48.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="48.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="48.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="48.5">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>str<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="49.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="49.3">m</w>’<w n="49.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! <w n="49.5">J</w>’<w n="49.6"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="49.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="49.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w></l>
						<l n="50" num="13.2"><w n="50.1">D</w>’<w n="50.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="50.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="50.4">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rph<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="51" num="13.3"><space unit="char" quantity="12"></space>… <w n="51.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="51.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="51.3">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="52" num="13.4"><w n="52.1">M</w>’<w n="52.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="52.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> : « <w n="52.4">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="52.5">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="52.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> ! »</l>
					</lg>
				</div></body></text></TEI>