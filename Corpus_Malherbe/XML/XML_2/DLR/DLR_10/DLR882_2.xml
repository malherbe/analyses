<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR882">
				<head type="main">OUBLI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="1.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w> <w n="1.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="1.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.7">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="2.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.4">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					<l n="3" num="1.3"><w n="3.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="3.2">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="3.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="3.4">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="3.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="3.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w></l>
					<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="4.2">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="4.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="4.4">b<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="5.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="5.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="5.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
					<l n="6" num="2.2"><space unit="char" quantity="4"></space><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.3">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="7" num="2.3"><w n="7.1">L<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="7.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rv<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">n</w>’<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="7.6">p<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="8" num="2.4"><space unit="char" quantity="4"></space><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="8.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="9.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="9.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.4">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rpr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.7">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
					<l n="10" num="3.2"><space unit="char" quantity="4"></space><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="10.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> !… <w n="10.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="10.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w></l>
					<l n="11" num="3.3"><w n="11.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.3">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="11.4">d</w>’<w n="11.5"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.6">n</w>’<w n="11.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="11.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>.</l>
					<l n="12" num="3.4"><space unit="char" quantity="4"></space><w n="12.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="12.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> ; <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.4">l</w>’<w n="12.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="12.7">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="13.2">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.4">d</w>’<w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>.</l>
					<l n="14" num="4.2"><space unit="char" quantity="4"></space><w n="14.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.3">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="14.4">l</w>’<w n="14.5">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="15" num="4.3"><w n="15.1">Qu</w>’<w n="15.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="15.3">s<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="15.4">m<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w></l>
					<l n="16" num="4.4"><space unit="char" quantity="4"></space><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="16.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p</w> <w n="16.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">— <w n="17.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="17.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.5"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="17.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
					<l n="18" num="5.2"><space unit="char" quantity="4"></space><w n="18.1">Tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="18.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="18.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="18.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> <w n="18.5">pl<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>.</l>
					<l n="19" num="5.3"><w n="19.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="19.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="19.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="19.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.6">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="19.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
					<l n="20" num="5.4"><space unit="char" quantity="4"></space><w n="20.1">C</w>’<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.3"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="20.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.6"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>.</l>
				</lg>
			</div></body></text></TEI>