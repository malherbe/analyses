<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÈMES TERRESTRES</head><div type="poem" key="DLR144">
					<head type="main">PRINTEMPS DE NEIGE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301">ai</seg>t</w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.7">j<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="2" num="1.2"><w n="2.1">D</w>’<w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>, <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.6">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.7">h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.5">p<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="3.6">s</w>’<w n="3.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.3">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.7">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="5" num="2.2"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="5.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.3">sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lpt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="5.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.7">ch<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="6" num="2.3"><w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.4">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="6.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="307">a</seg>il</w> <w n="6.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w>,</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w>, <w n="7.2">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rds</w> <w n="7.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="7.5">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="7.6">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.9">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="8" num="3.2"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.2">fl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="8.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.8">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ds</w>,</l>
						<l n="9" num="3.3"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="9.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="9.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="9.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.8">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w></l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1"><w n="10.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="10.2">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.5">qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="10.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.7">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="10.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>