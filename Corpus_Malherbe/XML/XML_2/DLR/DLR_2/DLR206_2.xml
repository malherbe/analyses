<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DÉCLARATIONS</head><div type="poem" key="DLR206">
					<head type="main">TON CŒUR INTACT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="1.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ct</w> <w n="1.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="1.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.6">qu</w>’<w n="1.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.8">fr<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="1.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.10"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.11">l</w>’<w n="1.12"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">S</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="2.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">cr<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="3.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="3.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.2">j</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="4.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>ç<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w>, <w n="4.8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.10">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="5" num="2.2"><w n="5.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.4">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.6">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.9">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="6" num="2.3"><w n="6.1">J</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="6.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.4">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5">fr<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="6.6">qu</w>’<w n="6.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="6.8">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.9">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="6.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="6.11">qu</w>’<w n="6.12"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.13">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>