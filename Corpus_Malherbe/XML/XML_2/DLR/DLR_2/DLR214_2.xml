<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DÉCLARATIONS</head><div type="poem" key="DLR214">
					<head type="main">LE FLEUVE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.3">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.5">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.7">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="1.9">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.10">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="2.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="2.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="2.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.9">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="2.10">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="3.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="3.3">r<seg phoneme="i" type="vs" value="1" rule="493">y</seg>thm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">l</w>’<w n="3.6"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>, <w n="3.7">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.10">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="4.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="4.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="5.2"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.5">l</w>’<w n="5.6"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="5.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="6" num="2.2"><w n="6.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.3">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="6.4">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="6.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w>,</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="7.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="7.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">l</w>’<w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>pr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rpr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.8">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="9.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.5">l</w>’<w n="9.6"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="9.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="9.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.2">qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="10.3">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="10.4">gr<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="10.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.8">d</w>’<w n="10.9">h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">d</w>’<w n="11.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="11.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="11.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.7">t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="11.8">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.10">D<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="12" num="3.4"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="12.2">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="12.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="12.7">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">… <w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="13.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="13.4">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.7">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="14" num="4.2"><w n="14.1">N</w>’<w n="14.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.6">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="14.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="14.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
						<l n="15" num="4.3"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="15.2">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.7">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="16" num="4.4"><w n="16.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="16.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="16.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="16.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="16.6">d</w>’<w n="16.7"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="16.8">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">N</w>’<w n="17.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="17.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="17.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="17.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="17.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>qu<seg phoneme="j" type="sc" value="0" rule="489">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="18" num="5.2"><w n="18.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="18.2">d</w>’<w n="18.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="18.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="18.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>, <w n="18.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="18.10">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.11">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>,</l>
						<l n="19" num="5.3"><w n="19.1">D</w>’<w n="19.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.3">g<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="19.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="20" num="5.4"><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="20.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.7">n</w>’<w n="20.8"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="20.9">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="20.10">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> ! <w n="21.2">Ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> ! <w n="21.3">Pr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="21.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="21.5">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="21.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.8"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.9">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="22" num="6.2"><w n="22.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.3">n</w>’<w n="22.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="22.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.7">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.8">n</w>’<w n="22.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
						<l n="23" num="6.3"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="23.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.4">l</w>’<w n="23.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="23.6">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="23.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.8">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="23.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.10">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.11">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="23.12">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w></l>
						<l n="24" num="6.4"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="24.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="24.4">l</w>’<w n="24.5">h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="24.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="24.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.9">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
				</div></body></text></TEI>