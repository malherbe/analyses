<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">AU PORT</head><div type="poem" key="DLR461">
					<head type="main">PREMIÈRE OCTOBRALE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="2.2">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="493">y</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
						<l n="3" num="1.3"><w n="3.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">b<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="3.4">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="3.5">d</w>’<w n="3.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="4.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="5.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.5">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ds</w>.</l>
						<l n="6" num="2.2"><w n="6.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="6.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="7.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="7.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="8.2">pl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="8.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="8.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="8.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="8.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="8.8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="8.9">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="9.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.5">c<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.7">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w></l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w> <w n="10.2">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">cr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="11" num="3.3"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.2">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.4">b<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="11.5">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.7">n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="12" num="3.4">— <w n="12.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ! <w n="12.3">qu</w>’<w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="12.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="12.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="12.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.8">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.9">j<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.10">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.11">l</w>’<w n="12.12"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> ?</l>
					</lg>
				</div></body></text></TEI>