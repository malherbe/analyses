<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN KROUMIRIE</head><div type="poem" key="DLR406">
					<head type="main">RÉVÉLATION</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Cr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="1.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="1.5">c</w>’<w n="1.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w></l>
						<l n="2" num="1.2"><w n="2.1">S<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="2.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.6">d</w>’<w n="2.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.3">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="3.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="3.6">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7">t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>,</l>
						<l n="4" num="1.4"><w n="4.1">V<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="4.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Cr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="5.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">c</w>’<w n="5.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w></l>
						<l n="6" num="2.2"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5">r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ?</l>
						<l n="7" num="2.3"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.2">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="7.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="7.6">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w></l>
						<l n="8" num="2.4"><w n="8.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="8.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="8.5">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Cr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="9.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.5">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="9.7">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>g</w>.</l>
						<l n="10" num="3.2"><w n="10.1">C</w>’<w n="10.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="10.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">l</w>’<w n="10.5"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="10.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="11" num="3.3"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rpr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="12" num="3.4"><w n="12.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.3">qu</w>’<w n="12.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.5">n</w>’<w n="12.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.7">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="13.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>, <w n="13.3">h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="13.4">l</w>’<w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.8">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="14" num="4.2"><w n="14.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.5">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="355">e</seg>x<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="15.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.3">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="15.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="16" num="4.4"><w n="16.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="16.2">l</w>’<w n="16.3">h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.5">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ph<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>h</w> <w n="17.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! <w n="17.4">c</w>’<w n="17.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>, <w n="17.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="17.7">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w>,</l>
						<l n="18" num="5.2"><w n="18.1">M<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="18.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.3">b<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="19" num="5.3"><w n="19.1">C</w>’<w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="19.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="19.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="19.6">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="19.7">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d</w></l>
						<l n="20" num="5.4"><w n="20.1">S<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="20.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.3">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="20.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="20.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="21.2">l</w>’<w n="21.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="21.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.5">ch<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>.</l>
						<l n="22" num="6.2"><w n="22.1">C</w>’<w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="22.3">s</w>’<w n="22.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="22.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.6">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="23" num="6.3"><w n="23.1">C</w>’<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="23.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.4">g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>tt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="24" num="6.4"><w n="24.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="24.2">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="24.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="24.4">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.6">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">C</w>’<w n="25.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="25.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="25.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="25.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="25.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.8">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
						<l n="26" num="7.2"><w n="26.1">C</w>’<w n="26.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>, <w n="26.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="26.4">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="26.5">d</w>’<w n="26.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.8">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>.</l>
						<l n="27" num="7.3"><w n="27.1">L</w>’<w n="27.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>.</l>
						<l n="28" num="7.4"><w n="28.1">C</w>’<w n="28.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="28.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="28.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="28.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.7">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>ts</w> <w n="28.8">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">C</w>’<w n="29.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>, <w n="29.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="29.4">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="29.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="29.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w>.</l>
						<l n="30" num="8.2"><w n="30.1">C<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="30.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.3"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="30.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.5">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.6">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="31" num="8.3"><w n="31.1">C</w>’<w n="31.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="31.3">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="31.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.6">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="32" num="8.4"><w n="32.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="32.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.3">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sc</w> <w n="32.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="32.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="32.6">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="33.2">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="33.3">c</w>’<w n="33.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>, <w n="33.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="33.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="33.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.9">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="34" num="9.2"><w n="34.1">R<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="34.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="34.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="34.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.6">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
						<l n="35" num="9.3"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="35.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="35.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="35.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="35.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="35.7"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="36" num="9.4"><w n="36.1">B<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="36.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="36.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
					</lg>
				</div></body></text></TEI>