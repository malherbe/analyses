<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE DÉSERT</head><div type="poem" key="DLR427">
					<head type="main">FANTASIA</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.4">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.7">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="1.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="1.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.4">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="2.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.9">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="3.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.6">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1"><w n="4.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w>, <w n="4.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.5">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="4.6">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.9">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
						<l n="5" num="2.2"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="5.2">h<seg phoneme="e" type="vs" value="1" rule="169">e</seg>nn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="5.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="5.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.8"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
						<l n="6" num="2.3"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.7">m<seg phoneme="wa" type="vs" value="1" rule="192">oe</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.9"><seg phoneme="o" type="vs" value="1" rule="432">o</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1"><w n="7.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="7.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>, <w n="7.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="7.6">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="7.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.9">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="8" num="3.2"><w n="8.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.4">l</w>’<w n="8.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="8.6">l</w>’<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.8">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.9">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.10">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="9" num="3.3"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.8">cr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1"><w n="10.1">Qu</w>’<w n="10.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.3">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="10.6">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w> <w n="10.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>ct</w></l>
						<l n="11" num="4.2"><w n="11.1">L</w>’<w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.5">c<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.7">l</w>’<w n="11.8"><seg phoneme="o" type="vs" value="1" rule="435">O</seg>cc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="11.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w> ;</l>
						<l n="12" num="4.3"><w n="12.1">Qu</w>’<w n="12.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.3">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.5">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="12.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.8">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1"><w n="13.1">Qu</w>’<w n="13.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.3">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.7">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.8">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="13.9">br<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.10"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.11">r<seg phoneme="o" type="vs" value="1" rule="318">au</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="14" num="5.2"><w n="14.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">j</w>’<w n="14.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">d</w>’<w n="14.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="14.7">l</w>’<w n="14.8">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="14.9">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="o" type="vs" value="1" rule="318">au</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="15" num="5.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3">j<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="15.5">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="15.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.8">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.10">gl<seg phoneme="o" type="vs" value="1" rule="318">au</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1"><w n="16.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.5">dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="16.9">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.10">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="?" type="va" value="1" rule="34">er</seg></w>,</l>
						<l n="17" num="6.2"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="17.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t</w> <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="17.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="17.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="17.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.7">s<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="17.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>,</l>
						<l n="18" num="6.3"><w n="18.1">Cr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="18.3">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> : « <w n="18.4">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.5">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="18.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="18.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="18.8">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="18.9">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.11">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> ! »</l>
					</lg>
				</div></body></text></TEI>