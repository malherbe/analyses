<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT44">
				<head type="number">XLIV</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="1.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="1.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="1.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="1.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.8">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="2" num="1.2"><space unit="char" quantity="12"></space><w n="2.1">D</w>’<w n="2.2">h<seg phoneme="o" type="vs" value="1" rule="435">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="2.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.3">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.5">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.6">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7">n</w>’<w n="3.8"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
					<l n="4" num="1.4"><space unit="char" quantity="12"></space><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.2">j</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="4.4">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> !</l>
					<l n="5" num="1.5"><w n="5.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="5.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="5.3">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.5">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ? <w n="5.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="5.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="5.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.9">m<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="6" num="1.6"><w n="6.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> ! <w n="6.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="6.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="6.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="6.6">d</w>’<w n="6.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.8">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.9">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
					<l n="7" num="1.7"><space unit="char" quantity="12"></space><w n="7.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="7.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.5">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> !</l>
					<l n="8" num="1.8">— <w n="8.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> <w n="8.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> ! <w n="8.3">j</w>’<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="8.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="8.6">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="8.7">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="8.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.9">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="8.10">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="9" num="1.9"><w n="9.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.2"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.7">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.9">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
					<l n="10" num="1.10"><space unit="char" quantity="12"></space><w n="10.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">n</w>’<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.4">qu</w>’<w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.6">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ! »</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ A quelque chose malheur est bon.</note>
					</closer>
			</div></body></text></TEI>