<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT215">
				<head type="number">XL</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">l</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w>, <w n="1.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="1.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="1.7">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.8">l</w>’<w n="1.9">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="1.10">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="2.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>sh<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="3.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="3.3">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="3.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="3.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="3.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.10">l<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="4.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="4.5">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w>, <w n="4.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="4.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.8">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">cr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="5.5">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="5.6">cr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.7">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>, <w n="5.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.9">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="5.10"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.11">l</w>’<w n="5.12"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="6" num="2.2"><w n="6.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="6.6">r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="6.7">ch<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>.</l>
					<l n="7" num="2.3"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.2">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.5">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="7.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.8">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="7.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="8" num="2.4"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="8.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>, <w n="8.4">l</w>’<w n="8.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w> <w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="8.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.8">gr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.6">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> : <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.9">pl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>th<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="10.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="11.3">p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.5">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d</w> <w n="11.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="11.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="11.9">s</w>’<w n="11.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="12" num="3.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="12.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="12.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="12.6">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.7">qu</w>’<w n="12.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.9">n<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> « <w n="12.10"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.11">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.12">cr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>. »</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">l</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.5"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="13.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.8">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.10"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="14" num="4.2"><w n="14.1">Qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg>qu</w>’<w n="14.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="14.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.5">s</w>’<w n="14.6"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="14.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="14.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.9">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="14.10">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					<l n="15" num="4.3"><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.2">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.5">V<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.6"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="15.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.8">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="16" num="4.4"><w n="16.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dj<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="16.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="16.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.8">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Tronc, tronc, tronc, tron.</note>
					</closer>
			</div></body></text></TEI>