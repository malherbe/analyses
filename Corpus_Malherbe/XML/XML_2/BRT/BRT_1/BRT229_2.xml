<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MOTS CARRÉS</head><div type="poem" key="BRT229">
				<head type="number">IV</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="1.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="1.6">p<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.8">sc<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="2" num="1.2"><w n="2.1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="2.3">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="2.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="2.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> ! <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ff<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.9">m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>.</l>
					<l n="3" num="1.3">— <w n="3.1">D<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="3.2">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="3.3">m<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>rs</w>, <w n="3.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>sc<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="4.2">V<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="4.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.4">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> :</l>
					<l n="5" num="1.5">— <w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="5.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.5">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="5.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.8">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn</w>-<w n="5.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w>-<w n="5.10">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="6" num="1.6"><w n="6.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="6.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="6.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="6.7">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>-<w n="6.8">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> !</l>
					<l n="7" num="1.7">— <w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="7.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="7.4">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="7.6">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="7.7">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="8" num="1.8"><w n="8.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">pr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="8.4">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ph<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.7">z<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.8">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
					<l n="9" num="1.9">— <w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="9.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="9.4">b<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>, <w n="9.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.6">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.8">M<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="10" num="1.10"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="10.3">r<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="10.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">C<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>lch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xh<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.8">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.9"><seg phoneme="o" type="vs" value="1" rule="432">o</seg>s</w> !</l>
					<l n="11" num="1.11"><w n="11.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="11.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.6">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="12" num="1.12"><w n="12.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.2">c<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>, <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="12.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="12.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.8">s<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>. »</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ MAGIE<lb></lb>▪ ALISE<lb></lb>▪ GIMAT<lb></lb>▪ ISAIE<lb></lb>▪ ÉÉTÈS</note>
					</closer>
			</div></body></text></TEI>