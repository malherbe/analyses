<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT55">
				<head type="number">LV</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">G<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> ! <w n="1.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="1.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.8">d</w>’<w n="1.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">C</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.3">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="2.6">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.8">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.9">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.10">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.11">f<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> !</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="3.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.5">p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>x</w> <w n="3.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.9">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.2">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="4.4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w>, <w n="4.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="4.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="4.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.8">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> <w n="5.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>, <w n="5.3">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d</w>, <w n="5.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.5">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.8">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="6" num="1.6"><w n="6.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.2">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="6.3">s</w>’<w n="6.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="6.6">c<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="6.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.8">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.9">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.10">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.11">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
					<l n="7" num="1.7"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.2">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="7.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>, <w n="7.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.7">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="7.8"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="7.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.10">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
					<l n="8" num="1.8"><w n="8.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>… <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="451">u</seg>m</w> <w n="8.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="8.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>, <w n="8.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="8.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.10">f<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
					<l n="9" num="1.9"><w n="9.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">gl<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.9">l</w>’<w n="9.10"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="9.11">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.12">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.13"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>cr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="10" num="1.10"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="10.2">c</w>’<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.4">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="10.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>, <w n="10.6">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> ! <w n="10.7">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.8">d</w>’<w n="10.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.10">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.11">l</w>’<w n="10.12"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.13">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> ? …</l>
					<l n="11" num="1.11"><w n="11.1">Qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> ! <w n="11.2">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="11.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.8">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.9">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="12" num="1.12"><w n="12.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2">M<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>dr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="12.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="12.6">d</w>’<w n="12.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.8">t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="12.9">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> ? …</l>
					<l n="13" num="1.13"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> <w n="13.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>, <w n="13.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="13.4">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg>er<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="13.6">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="13.8">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="14" num="1.14"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.2">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.5">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="14.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.8"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.9">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> !</l>
					<l n="15" num="1.15"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="15.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="15.3">p<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="15.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="15.7">j</w>’<w n="15.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="15.9">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="16" num="1.16"><w n="16.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.5"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="16.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.9">l</w>’<w n="16.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> ! »</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Trop gratter cuit, trop parler nuit.</note>
					</closer>
			</div></body></text></TEI>