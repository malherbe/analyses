<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT16">
				<head type="number">XVI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="1.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">l</w>’<w n="1.4">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="1.6"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.10">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="2.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>, <w n="2.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>ct<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ;</l>
					<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="3.3">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c</w>, <w n="3.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.5">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="3.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3">r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.5">cl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bs</w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.9">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sh<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="5.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.5">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="5.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.9">m<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.6">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="6.7">qu</w>’<w n="6.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.9">n</w>’<w n="6.10"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.11"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
					<l n="7" num="1.7"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.2">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="7.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="7.4">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>ls</w> <w n="7.5">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.6">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="7.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8">c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="8" num="1.8"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="8.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="8.6">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="8.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					<l n="9" num="1.9"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="9.3">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.7">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.8">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="10" num="1.10"><w n="10.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="10.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="10.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">â</seg>t</w> <w n="10.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.8">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>…</l>
					<l n="11" num="1.11"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4">fr<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c</w> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.7">m<seg phoneme="wa" type="vs" value="1" rule="421">oi</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="11.8"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="11.10">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.11">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="12" num="1.12"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="12.2">n</w>’<w n="12.3"><seg phoneme="y" type="vs" value="1" rule="251">eû</seg>t</w> <w n="12.4">d</w>’<w n="12.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="12.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="12.8">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.9">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="12.10">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.11">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ L’habit ne fait pas le moine</note>
					</closer>
			</div></body></text></TEI>