<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT28">
				<head type="number">XXVIII</head>
				<lg n="1">
					<l n="1" num="1.1">« <w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="1.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="1.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6">tr<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="1.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.8">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.9"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="2" num="1.2"><w n="2.1">R<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="2.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.6">n</w>’<w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.8">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="2.9">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>.</l>
					<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.4">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.6">s<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.8">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="4.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> ! <w n="4.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.5">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6">l</w>’<w n="4.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="4.8">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> !</l>
					<l n="5" num="1.5"><w n="5.1">Qu</w>’<w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="5.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="5.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.9">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="6" num="1.6"><space unit="char" quantity="12"></space><w n="6.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="6.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w></l>
					<l n="7" num="1.7"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">f<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="7.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>, <w n="7.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="8" num="1.8"><space unit="char" quantity="12"></space><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="8.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> ? … »</l>
					<l n="9" num="1.9"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="9.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="9.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">tr<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> ! <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.7">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="9.8">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.9">l</w>’<w n="9.10"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="10" num="1.10"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="10.5">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="10.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
					<l n="11" num="1.11"><w n="11.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">l</w>’<w n="11.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>, <w n="11.5">J<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>-<w n="11.6">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l</w> <w n="11.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.9">Ch<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="12" num="1.12"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>pl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.7">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Ah ! le bon billet qu’a La Châtre !</note>
					</closer>
			</div></body></text></TEI>