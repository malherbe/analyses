<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MOTS CARRÉS</head><div type="poem" key="BRT243">
				<head type="number">XVIII</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.3">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> : <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="1.6">pr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.8">sp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="2.4">h<seg phoneme="o" type="vs" value="1" rule="435">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.7">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					<l n="3" num="1.3">— <w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="3.2">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.3">d</w>’<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>p</w> <w n="3.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w>, <w n="3.6">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.2">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="4.6">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.7">qu</w>’<w n="4.8"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="4.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.10">c<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					<l n="5" num="1.5">— <w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="5.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">Gr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="5.8">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="6" num="1.6"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="6.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>v<seg phoneme="j" type="sc" value="0" rule="479">i</seg><seg phoneme="?" type="va" value="1" rule="162">en</seg>t</w> <w n="6.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.9">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
					<l n="7" num="1.7">— <w n="7.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="7.2">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="8" num="1.8"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="8.2">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct</w> : <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.7">gr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ff<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.10">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ RAGE<lb></lb>▪ AMAN<lb></lb>▪ GANT<lb></lb>▪ ENTE</note>
					</closer>
			</div></body></text></TEI>