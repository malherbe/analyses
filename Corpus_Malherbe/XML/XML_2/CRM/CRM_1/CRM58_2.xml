<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM58">
				<head type="main">ON VOYAIT LES CHEMINS</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="1.2">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.4">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="1.5">s</w>’<w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="1.8">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="1.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.10">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
					<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ge<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="2.4">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="2.5">l</w>’<w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="421">oi</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="3" num="1.3"><w n="3.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="3.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="3.4">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="3.5">t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="3.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7">gu<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="4.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="4.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="4.6">d</w>’<w n="4.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.8">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.9">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="5.2">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="5.3">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.4">h<seg phoneme="e" type="vs" value="1" rule="169">e</seg>nn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.7">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rtr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="6" num="1.6"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>-<w n="6.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="6.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>-<w n="6.7">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>,</l>
					<l n="7" num="1.7"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="7.2">n</w>’<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>-<w n="7.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="7.6">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="7.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.9">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w></l>
					<l n="8" num="1.8"><w n="8.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">cr<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="8.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="8.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="8.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="8.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.8">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> ?</l>
					<l n="9" num="1.9"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="9.2">n</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>-<w n="9.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="9.6">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.7">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.10">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w></l>
					<l n="10" num="1.10"><w n="10.1">S<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.4">qu</w>’<w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.6">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="10.7">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w></l>
					<l n="11" num="1.11"><w n="11.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.8">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ?</l>
				</lg>
			</div></body></text></TEI>