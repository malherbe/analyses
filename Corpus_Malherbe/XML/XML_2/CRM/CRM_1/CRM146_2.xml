<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM146">
				<head type="main">LES MOISSONNEURS</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>, <w n="1.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">Br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.4">sc<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">S</w>’<w n="2.2"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="2.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="2.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.9">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="2.10">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.11">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w></l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.4">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ge<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="3.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>qs</w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1"><w n="4.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rds</w> <w n="4.4">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="4.5">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="4.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.7">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
					<l n="5" num="2.2"><w n="5.1">S<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="5.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="5.6">l</w>’<w n="5.7"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="5.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.9">b<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="6" num="2.3"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="6.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>, <w n="6.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="7.5">cr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.6">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.8">l</w>’<w n="7.9">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
					<l n="8" num="3.2"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">b<seg phoneme="ø" type="vs" value="1" rule="247">œu</seg>fs</w> <w n="8.4">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="8.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.9">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.10">b<seg phoneme="ø" type="vs" value="1" rule="400">eu</seg>gl<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>,</l>
					<l n="9" num="3.3"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rs</w> <w n="9.3">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="9.5">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="9.6">d</w>’<w n="9.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b</w> <w n="9.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.10">m<seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="10" num="3.4"><w n="10.1">Qu</w>’<w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="10.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="10.6">bl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>cs</w> <w n="10.7">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="10.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.9">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>