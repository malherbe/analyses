<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM119" rhyme="none">
				<head type="main">QUAND LES CHEVAUX RENTRENT TRÈS TARD</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="1.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w></l>
					<l n="2" num="1.2"><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.4">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
					<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="3.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="3.6">s</w>’<w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="4.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="4.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.6">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="6" num="1.6"><w n="6.1">N</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="6.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">g<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="7" num="1.7"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.5">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					<l n="8" num="1.8"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.2">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="8.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.5">bl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="8.6">j<seg phoneme="o" type="vs" value="1" rule="318">au</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="9" num="1.9"><w n="9.1">V<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="9.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="9.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="10" num="1.10"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="10.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.7">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w></l>
					<l n="11" num="1.11"><w n="11.1">D</w>’<w n="11.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.7">l</w>’<w n="11.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					<l n="12" num="1.12"><w n="12.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="12.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="12.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="12.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>,</l>
					<l n="13" num="1.13"><w n="13.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="13.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w>,</l>
					<l n="14" num="1.14"><w n="14.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="14.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.4">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="14.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="15" num="1.15"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="15.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="15.5">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="16" num="1.16"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="16.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="16.3">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="16.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.6">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>.</l>
				</lg>
			</div></body></text></TEI>