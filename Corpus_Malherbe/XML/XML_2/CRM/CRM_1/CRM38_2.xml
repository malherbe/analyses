<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM38">
				<head type="main">LA PETITE BOUTIQUE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">R<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.3">l</w>’<w n="1.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.7">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w>-<w n="2.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="2.4">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="3.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.4">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="3.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>, <w n="4.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.5">c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="5" num="1.5"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="5.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="5.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>fl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><w n="6.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="6.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="6.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>-<w n="6.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="6.6">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w></l>
					<l n="7" num="2.2"><w n="7.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>, <w n="7.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.4">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="8" num="2.3"><w n="8.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="8.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="9" num="2.4"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="9.4">d</w>’<w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc</w>-<w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w>-<w n="9.7">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w></l>
					<l n="10" num="2.5"><w n="10.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="10.4">d</w>’<w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.8">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> ?</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="11.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.3">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>-<w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="11.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="12" num="3.2"><w n="12.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>d</w>,</l>
					<l n="13" num="3.3"><w n="13.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="14" num="3.4"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="14.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="14.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="15" num="3.5"><w n="15.1">Qu</w>’<w n="15.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.3"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="15.4">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="15.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.6">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="15.7">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ?</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1"><w n="16.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="16.2">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="16.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="16.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
					<l n="17" num="4.2"><w n="17.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="18" num="4.3"><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.2">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.4">r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
					<l n="19" num="4.4"><w n="19.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.3">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="19.4">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w></l>
					<l n="20" num="4.5"><w n="20.1">Qu</w>’<w n="20.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="20.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="20.4">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="20.5">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ?</l>
				</lg>
			</div></body></text></TEI>