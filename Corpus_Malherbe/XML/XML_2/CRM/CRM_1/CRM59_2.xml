<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM59" rhyme="none">
				<head type="main">QUAND IL PLEUVAIT</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="1.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="1.3">pl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.5">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>-<w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">cr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6">t<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="3" num="1.3">« <w n="3.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="3.3">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> », <w n="3.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="3.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.6">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1"><w n="4.1">C</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.4">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.6">gr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="5" num="2.2"><w n="5.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="6" num="2.3"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="6.3">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.4">s</w>’<w n="6.5"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="6.6">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="7" num="2.4"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="7.2">gr<seg phoneme="o" type="vs" value="1" rule="434">o</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="7.3">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="7.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="8" num="3.1"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>, <w n="8.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="8.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.6">l</w>’<w n="8.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="9" num="3.2"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="9.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.5">pl<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="10" num="3.3"><w n="10.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="10.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="10.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.6">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<l n="11" num="4.1"><w n="11.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="11.2">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="11.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="11.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
					<l n="12" num="4.2"><w n="12.1">R<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>cr<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.4">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>,</l>
					<l n="13" num="4.3"><w n="13.1">M<seg phoneme="e" type="vs" value="1" rule="353">e</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.3">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="13.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
					<l n="14" num="4.4"><w n="14.1">L<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="14.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="14.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>.</l>
				</lg>
				<lg n="5">
					<l n="15" num="5.1"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.6">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="16" num="5.2"><w n="16.1">D<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="16.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> ;</l>
					<l n="17" num="5.3"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="17.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rpr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="17.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
				</lg>
				<lg n="6">
					<l n="18" num="6.1"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="18.2">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="18.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="18.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="19" num="6.2"><w n="19.1">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="19.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="19.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w>, <w n="19.4">tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="19.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="19.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.7">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w></l>
					<l n="20" num="6.3"><w n="20.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.2">l</w>’<w n="20.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>, <w n="20.4">l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="20.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="21" num="6.4"><w n="21.1">S<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.2">dr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="21.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="21.4">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="21.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
				</lg>
			</div></body></text></TEI>