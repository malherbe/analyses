<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Ïambes et poèmes</title>
				<title type="medium">Édition électronique</title>
				<author key="BRB">
					<name>
						<forename>Auguste</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1805" to="1882">1805-1882</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4203 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Iambes et poèmes</title>
						<author>Auguste BARBIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L947</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Iambes et poèmes</title>
								<author>Auguste BARBIER</author>
								<idno type="URL">https://archive.org/details/iambesetpomes03barbgoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Paul Masgana</publisher>
									<date when="1840">1840</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LAZARE</head><div type="poem" key="BRB43">
					<head type="main">CONSCIENCE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="1.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.3">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="1.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.6">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="1.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="1.8">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="1.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="1.10">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w></l>
						<l n="2" num="1.2"><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="2.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.4">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.5">d</w>’<w n="2.6">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> ?</l>
						<l n="3" num="1.3"><w n="3.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="3.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="3.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.7">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="5" num="1.5"><w n="5.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">d</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="5.4">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="5.5">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="5.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>,</l>
						<l n="6" num="1.6"><w n="6.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">h<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="6.5">l</w>’<w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>pr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="6.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.9">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> ?</l>
						<l n="7" num="1.7"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="7.2">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="7.6">h<seg phoneme="o" type="vs" value="1" rule="435">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="7.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="8" num="1.8"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="8.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">j<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.7">d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="9" num="1.9"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.5">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rpr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="9.9">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.10">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
						<l n="10" num="1.10"><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.6">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="10.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.9">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
						<l n="11" num="1.11"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.3">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="11.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5">gu<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.6">v<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="12" num="1.12"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.2">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="12.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w> <w n="12.4">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="12.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="12.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="12.7">l</w>’<w n="12.8"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="13" num="1.13"><w n="13.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.2">qu</w>’<w n="13.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.4">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>lt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="13.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.8">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.10">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w></l>
						<l n="14" num="1.14"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.4">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="14.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="14.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.9">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> !</l>
						<l n="15" num="1.15"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="15.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>sc<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! <w n="15.4"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="15.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="15.6">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="15.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.8">l</w>’<w n="15.9"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="16" num="1.16"><w n="16.1">R<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.2">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="16.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.6">c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.7">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="17" num="1.17"><w n="17.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="17.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.3">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="17.4">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="17.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> <w n="17.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.7">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="17.8">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>,</l>
						<l n="18" num="1.18"><w n="18.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.2">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="18.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.4">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="18.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="18.7">m<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>,</l>
						<l n="19" num="1.19"><w n="19.1">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="19.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="19.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="19.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="19.7">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.8">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.9">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="19.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="19.11">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="19.12">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
						<l n="20" num="1.20"><w n="20.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>sc<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="20.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="20.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="20.4">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="20.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="20.7">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="20.8">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.9">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="21" num="1.21"><w n="21.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.3">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="21.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="21.5">qu</w>’<w n="21.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="21.7">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="21.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="21.9">l</w>’<w n="21.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.11"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="22" num="1.22"><w n="22.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="22.2">n<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.3">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="22.4">l</w>’<w n="22.5"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="22.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.7">l</w>’<w n="22.8">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="23" num="1.23"><w n="23.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ</w>’<w n="23.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="23.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="23.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="23.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="23.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.8">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="23.9">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="24" num="1.24"><w n="24.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.2">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="24.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="24.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.5">bl<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="24.6">cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="25" num="1.25"><w n="25.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="25.2">d</w>’<w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="25.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="25.5">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="25.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="25.7">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="25.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w></l>
						<l n="26" num="1.26"><w n="26.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="26.2">n</w>’<w n="26.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="26.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="26.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="26.6">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>, <w n="26.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="26.8">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.10">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="26.11">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>,</l>
						<l n="27" num="1.27"><w n="27.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="27.2">l</w>’<w n="27.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="27.5">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="27.6">d</w>’<w n="27.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.8">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.9"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="28" num="1.28"><w n="28.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.3">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.4">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="28.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.7">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>