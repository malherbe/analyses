<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES276">
				<head type="main">DEUX NOMS</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">t</w>’<w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="1.5">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6">f<seg phoneme="y" type="vs" value="1" rule="445">û</seg>t</w>-<w n="1.7">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.8">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">t</w>’<w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="2.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w>, <w n="2.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.9">d</w>’<w n="2.10"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> !</l>
					<l n="3" num="1.3"><w n="3.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="3.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.5">pr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.7">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="3.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.10">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
					<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="4.3">f<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.5">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="4.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="4.7">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
					<l n="5" num="1.5"><space quantity="6" unit="char"></space><w n="5.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="5.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.7">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
					<l n="6" num="1.6"><space quantity="6" unit="char"></space><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.3">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="6.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.6">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> ! <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="7.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="7.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.8"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="8" num="2.2"><w n="8.1">Fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="8.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="8.4">v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="8.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.7">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> :</l>
					<l n="9" num="2.3"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="9.2">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="9.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="9.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.8">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
					<l n="10" num="2.4"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.3">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="10.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.8">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="10.9"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="10.10">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.11">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="11" num="2.5"><space quantity="6" unit="char"></space><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="11.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="11.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
					<l n="12" num="2.6"><space quantity="6" unit="char"></space><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="12.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="12.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="13.2">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="13.3">d</w>’<w n="13.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.7">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.8">j</w>’<w n="13.9"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="13.10">l</w>’<w n="13.11"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="14" num="3.2"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg></w> ! <w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.3">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.6">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.8">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.10">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.11">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ;</l>
					<l n="15" num="3.3"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="15.2">t<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="15.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="15.7">qu</w>’<w n="15.8"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="15.9">t</w>’<w n="15.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.11">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="15.12">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
					<l n="16" num="3.4"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="16.2">n</w>’<w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="16.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="16.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="16.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.7">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="16.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.9">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.10">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="16.11">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.12">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
					<l n="17" num="3.5"><space quantity="6" unit="char"></space><w n="17.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="17.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="17.5">l</w>’<w n="17.6"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="17.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="17.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
					<l n="18" num="3.6"><space quantity="6" unit="char"></space><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="18.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="18.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="18.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> !</l>
				</lg>
			</div></body></text></TEI>