<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES296">
				<head type="main">UN PRÉSAGE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.3">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="1.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="1.8">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.9"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="1.10">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="2.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="2.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.7">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.9">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> ?</l>
					<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="3.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.6">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d</w> <w n="3.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.9">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> :</l>
					<l n="4" num="1.4"><w n="4.1">Tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="4.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="4.4">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5">m<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="4.7">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> !</l>
					<l n="5" num="1.5"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="5.6">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="5.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="6.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !</l>
					<l n="7" num="1.7"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.3">fl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="7.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="7.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="7.8">d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
					<l n="8" num="1.8"><w n="8.1">B<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="8.2">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="8.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="9" num="1.9"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="9.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="9.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="9.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.7"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !</l>
				</lg>
				<lg n="2">
					<l n="10" num="2.1"><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="10.3">N<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="10.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="11" num="2.2"><w n="11.1">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="11.4">t</w>-<w n="11.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="11.7">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="11.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.9">l</w>’<w n="11.10"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
					<l n="12" num="2.3"><w n="12.1">L</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.7">l</w>’<w n="12.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="13" num="2.4"><w n="13.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.3">pl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w>-<w n="13.5">d<seg phoneme="ə" type="vi" value="1" rule="156">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="13.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.7">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
					<l n="14" num="2.5"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.2">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="14.3">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.5">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="15" num="2.6"><w n="15.1">L</w>’<w n="15.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="15.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="15.4">g<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="15.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="15.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
					<l n="16" num="2.7"><w n="16.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rv<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>-<w n="16.2">t</w>-<w n="16.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="16.5">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.6">n</w>’<w n="16.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="16.8">qu</w>’<w n="16.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.10">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
					<l n="17" num="2.8"><w n="17.1">L</w>’<w n="17.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="17.5">l</w>’<w n="17.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct</w> <w n="17.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.8">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="18" num="2.9"><w n="18.1">F<seg phoneme="y" type="vs" value="1" rule="445">û</seg>t</w>-<w n="18.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="18.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.7">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="18.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.9">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> !</l>
				</lg>
				<lg n="3">
					<l n="19" num="3.1"><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="19.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.7">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="20" num="3.2"><w n="20.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="20.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="20.4">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.6">j</w>’<w n="20.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="20.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="20.9">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ?</l>
					<l n="21" num="3.3"><w n="21.1">D<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="21.2">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.3">d</w>’<w n="21.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="22" num="3.4"><w n="22.1">Pr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="22.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="22.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="22.4">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.5">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="22.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="22.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
					<l n="23" num="3.5"><w n="23.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.2">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="23.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="23.4">m</w>’<w n="23.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="23.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.7">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="24" num="3.6"><w n="24.1">C</w>’<w n="24.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="24.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="24.5">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="24.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="24.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="24.8">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> :</l>
					<l n="25" num="3.7"><w n="25.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.3">s</w>’<w n="25.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="25.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="25.6">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="25.8">d</w>’<w n="25.9"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="26" num="3.8"><w n="26.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.3">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="26.4">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.6">l</w>’<w n="26.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="27" num="3.9"><w n="27.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg>s</w> <w n="27.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="27.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="27.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> !</l>
				</lg>
				<lg n="4">
					<l n="28" num="4.1"><w n="28.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="28.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="28.5">d</w>’<w n="28.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="28.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.8">l</w>’<w n="28.9"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="29" num="4.2"><w n="29.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="29.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.3">l</w>’<w n="29.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="29.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="29.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="29.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.9">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.10">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>.</l>
					<l n="30" num="4.3"><w n="30.1">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ! <w n="30.2">qu</w>’<w n="30.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="30.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="30.7">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="30.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="31" num="4.4"><w n="31.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="31.3">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.6">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> !</l>
					<l n="32" num="4.5"><w n="32.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="32.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="32.4">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="32.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="32.6">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="32.7">bl<seg phoneme="o" type="vs" value="1" rule="435">o</seg>tt<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="33" num="4.6"><w n="33.1"><seg phoneme="i" type="vs" value="1" rule="497">Y</seg></w> <w n="33.2">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="33.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="33.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w> <w n="33.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="33.7">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
					<l n="34" num="4.7"><w n="34.1">Gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="34.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="34.4">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w>, <w n="34.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="34.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
					<l n="35" num="4.8"><w n="35.1">B<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ! <w n="35.2">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ! <w n="35.3">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.4">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.5">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="35.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="35.7">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="36" num="4.9"><w n="36.1">J</w>’<w n="36.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="36.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.4">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> ; <w n="36.5">c</w>’<w n="36.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="36.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="36.8">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> : <w n="36.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>tr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> !</l>
				</lg>
			</div></body></text></TEI>