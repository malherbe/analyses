<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES216">
					<head type="main">MALHEUR À MOI</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Ah ! ce n’est pas aimer que prendre sur soi-même <lb></lb>
									De pouvoir vivre ainsi loin de l’objet qu’on aime.
								</quote>
								 <bibl><hi rend="smallcap">André Chénier.</hi></bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ! <w n="1.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="1.8">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.9">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="2.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="2.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.9"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="3.3">n</w>’<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="3.6">l</w>’<w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="3.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.9">v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="3.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.11">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="4" num="1.4"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="4.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.4">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="5" num="1.5"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="5.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="5.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="5.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.6">d</w>’<w n="5.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.8">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.9"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
						<l n="6" num="1.6"><w n="6.1">M<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.4">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> <w n="6.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
						<l n="7" num="1.7"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="7.2">v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.4">p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>x</w>, <w n="7.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="7.6">s</w>’<w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="7.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> :</l>
						<l n="8" num="1.8"><space quantity="10" unit="char"></space><w n="8.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="9.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="9.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="9.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.8">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="10" num="2.2"><w n="10.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="10.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> !</l>
						<l n="11" num="2.3"><w n="11.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.3">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="11.5">n</w>’<w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.8">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.9">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w>,</l>
						<l n="12" num="2.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">n</w>’<w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="12.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="12.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.8">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg>s</w> <w n="12.9">qu</w>’<w n="12.10"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="12.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="13" num="2.5"><w n="13.1">H<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="13.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="13.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w>, <w n="13.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.8">f<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
						<l n="14" num="2.6"><w n="14.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="14.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5">j</w>’<w n="14.6"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="14.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="15" num="2.7"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="15.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="15.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.4">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="15.5">qu</w>’<w n="15.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="15.7">n</w>’<w n="15.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="15.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="15.10">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> :</l>
						<l n="16" num="2.8"><space quantity="10" unit="char"></space><w n="16.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>str<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="17.2">d</w>’<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="17.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="17.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="17.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.7">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="18" num="3.2"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="18.2">l</w>’<w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="18.4">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="18.6">d</w>’<w n="18.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ;</l>
						<l n="19" num="3.3"><w n="19.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2">n</w>’<w n="19.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="19.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="19.6">s</w>’<w n="19.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="19.8">n</w>’<w n="19.9"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="19.10">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="19.11">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="19.12">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ;</l>
						<l n="20" num="3.4"><w n="20.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="20.2">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.4">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="20.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="20.6">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.7">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="21" num="3.5"><w n="21.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="21.2">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="21.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.4">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ! <w n="21.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="21.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>-<w n="21.7">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="21.9">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ?</l>
						<l n="22" num="3.6"><w n="22.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.2">n</w>’<w n="22.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="22.4">qu</w>’<w n="22.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="22.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="22.8">c</w>’<w n="22.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="22.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="22.11">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="22.12">qu</w>’<w n="22.13"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.14"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="23" num="3.7"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="23.2">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="23.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.4">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="23.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="23.6">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.7">n</w>’<w n="23.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="23.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="23.10">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>-<w n="23.11">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="24" num="3.8"><space quantity="10" unit="char"></space><w n="24.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="24.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="24.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
					</lg>
				</div></body></text></TEI>