<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES261">
					<head type="main">UNE ONDINE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									L’onde murmure, la vague s’élève. <lb></lb>
									La sirène l’attire par ses paroles ; <lb></lb>
									elle le charme par ses chants.
								</quote>
								 <bibl><hi rend="smallcap">Goethe.</hi></bibl>
							</cit>
						</epigraph>
					<epigraph>
						<cit>
							<quote>
								Reine de ces collines vertes, <lb></lb>
								Du sein des vagues entr’ouvertes <lb></lb>
								Une jeune Ondine apparaît <lb></lb>
								. . . . . . . . . . . . . . . . . . . . . . . . . <lb></lb>
								. . . . . . . . . . . . . . . . . . . . . . . . . <lb></lb>
								. . . . . . . . . . . . . . . . . . . . . . . . .
							</quote>
							 <bibl><hi rend="smallcap">H. de Latouche.</hi></bibl>
						</cit>
					</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ! <w n="2.2">n</w>’<w n="2.3"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="2.4">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="2.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.7">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="3.2">d</w>’<w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.5">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="4.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.5">t</w>’<w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257">eoi</seg>r</w>.</l>
						<l n="5" num="1.5"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="5.4">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="6" num="1.6"><w n="6.1">F<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">l</w>’<w n="6.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> ;</l>
						<l n="7" num="1.7"><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="8" num="1.8"><w n="8.1">D<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="8.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.6">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="9.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.4">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="9.5">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="10" num="2.2"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>tt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="10.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w></l>
						<l n="11" num="2.3"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="11.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="11.5">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="12" num="2.4"><w n="12.1">V<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="12.2">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>.</l>
						<l n="13" num="2.5"><w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="13.2">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rf<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="14" num="2.6"><w n="14.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="14.2">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="14.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ;</l>
						<l n="15" num="2.7"><w n="15.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.2">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="15.3">gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ; <w n="15.4">l</w>’<w n="15.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="16" num="2.8"><w n="16.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.5">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> !</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="17.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.3">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="18" num="3.2"><w n="18.1">Pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="18.3">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="18.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.5">l</w>’<w n="18.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w></l>
						<l n="19" num="3.3"><w n="19.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="19.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="19.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.5">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="20" num="3.4"><w n="20.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.3">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="20.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="21" num="3.5"><w n="21.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="21.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="21.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.5">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="22" num="3.6"><w n="22.1">Cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="22.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w></l>
						<l n="23" num="3.7"><w n="23.1">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> : « <w n="23.2">V<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="23.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.4">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="24" num="3.8"><w n="24.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="24.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="24.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="24.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.5">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>. »</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="25.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="25.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="25.5">d</w>’<w n="25.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="26" num="4.2"><w n="26.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="26.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.3"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="26.4">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="26.5">d</w>’<w n="26.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
						<l n="27" num="4.3"><w n="27.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="27.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.3">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> ! <w n="27.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="27.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="28" num="4.4"><w n="28.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="28.2">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="28.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="28.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="28.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="28.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
						<l n="29" num="4.5"><w n="29.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ! <w n="29.2">l</w>’<w n="29.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="29.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="29.7">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="30" num="4.6"><w n="30.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.4">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>f</w> <w n="30.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.6">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="30.7">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> ;</l>
						<l n="31" num="4.7"><w n="31.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="31.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="31.6">s<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="32" num="4.8"><w n="32.1">N</w>’<w n="32.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="32.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="32.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.6">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> !</l>
					</lg>
				</div></body></text></TEI>