<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FAMILLE</head><div type="poem" key="DES395">
					<head type="main">LA MÈRE QUI PLEURE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.6">v<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="2.2">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w></l>
						<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="3.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="3.4">d</w>’<w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.6">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
						<l n="4" num="1.4"><w n="4.1">S</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.6">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">S</w>’<w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="5.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="5.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="5.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>,</l>
						<l n="6" num="2.2"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="6.2">m</w>’<w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="6.4">r<seg phoneme="ə" type="vi" value="1" rule="351">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ?</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1"><w n="7.1">B<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="7.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="8" num="3.2"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="8.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="8.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w></l>
						<l n="9" num="3.3"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>bl<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w></l>
						<l n="10" num="3.4"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="10.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="10.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="11" num="4.1"><w n="11.1">S</w>’<w n="11.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="11.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="11.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>,</l>
						<l n="12" num="4.2"><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="12.2">m</w>’<w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.4">r<seg phoneme="ə" type="vi" value="1" rule="351">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ?</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1"><w n="13.1">Pr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt</w> <w n="13.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.4">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.5">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="14" num="5.2"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="14.2">l</w>’<w n="14.3">h<seg phoneme="i" type="vs" value="1" rule="493">y</seg>mn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.5">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
						<l n="15" num="5.3"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="15.2">s</w>’<w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>,</l>
						<l n="16" num="5.4"><w n="16.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="16.3">cr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.4">l</w>’<w n="16.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<lg n="6">
						<l n="17" num="6.1"><w n="17.1">S</w>’<w n="17.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="17.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="17.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="17.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>,</l>
						<l n="18" num="6.2"><w n="18.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="18.2">m</w>’<w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="18.4">r<seg phoneme="ə" type="vi" value="1" rule="351">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ?</l>
					</lg>
					<lg n="7">
						<l n="19" num="7.1"><w n="19.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.5">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="20" num="7.2"><w n="20.1">D<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="20.2">qu</w>’<w n="20.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="20.4"><seg phoneme="y" type="vs" value="1" rule="391">eu</seg>t</w> <w n="20.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="20.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.7">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="21" num="7.3"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="21.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="21.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="22" num="7.4"><w n="22.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="22.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="22.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<lg n="8">
						<l n="23" num="8.1"><w n="23.1">S</w>’<w n="23.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="23.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.4">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="23.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="23.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>,</l>
						<l n="24" num="8.2"><w n="24.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="24.2">m</w>’<w n="24.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="24.4">r<seg phoneme="ə" type="vi" value="1" rule="351">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ?</l>
					</lg>
					<lg n="9">
						<l n="25" num="9.1"><w n="25.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="25.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="25.6">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="26" num="9.2"><w n="26.1">Pl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="26.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="26.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.5">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
						<l n="27" num="9.3"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="27.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="27.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.4">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="28" num="9.4"><w n="28.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.2">n</w>’<w n="28.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="28.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="28.5">qu</w>’<w n="28.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="28.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="10">
						<l n="29" num="10.1"><w n="29.1">S</w>’<w n="29.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="29.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.4">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="29.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="29.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
						<l n="30" num="10.2"><w n="30.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="30.2">m</w>’<w n="30.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="30.4">r<seg phoneme="ə" type="vi" value="1" rule="351">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ?</l>
					</lg>
					<lg n="11">
						<l n="31" num="11.1"><w n="31.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="31.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.4">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="31.5">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="32" num="11.2"><w n="32.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="32.2">l</w>’<w n="32.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="32.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="32.6">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="32.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.8">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>,</l>
						<l n="33" num="11.3"><w n="33.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="33.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="33.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="33.4">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="33.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="33.6">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w></l>
						<l n="34" num="11.4"><w n="34.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="34.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="34.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="34.4">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="34.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="12">
						<l n="35" num="12.1"><w n="35.1">S</w>’<w n="35.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="35.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.4">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="35.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="35.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
						<l n="36" num="12.2"><w n="36.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="36.2">m</w>’<w n="36.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="36.4">r<seg phoneme="ə" type="vi" value="1" rule="351">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ?</l>
					</lg>
					<lg n="13">
						<l n="37" num="13.1"><w n="37.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.2">l</w>’<w n="37.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="37.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="37.5">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="37.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="37.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="38" num="13.2"><w n="38.1">S<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="38.2">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="38.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="38.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="38.5">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="38.6"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> !</l>
						<l n="39" num="13.3"><w n="39.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="39.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.3">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="39.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.5">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
						<l n="40" num="13.4"><w n="40.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="40.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="40.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="40.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="40.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<lg n="14">
						<l n="41" num="14.1"><w n="41.1">S</w>’<w n="41.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="41.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.4">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="41.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="41.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
						<l n="42" num="14.2"><w n="42.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="42.2">m</w>’<w n="42.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="42.4">r<seg phoneme="ə" type="vi" value="1" rule="351">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ?</l>
					</lg>
				</div></body></text></TEI>