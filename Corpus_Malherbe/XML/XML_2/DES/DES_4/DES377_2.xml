<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">AMOUR</head><div type="poem" key="DES377">
					<head type="main">AMOUR, DIVIN RÔDEUR</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="1.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="1.3">r<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="1.4">gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="2" num="1.2"><w n="2.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.2">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="2.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.6"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="2.7">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.8">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.10">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>qu<seg phoneme="j" type="sc" value="0" rule="489">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="3.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.5">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="3.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w>,</l>
						<l n="4" num="1.4"><w n="4.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="4.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="4.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="4.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.3">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> ! <w n="5.4">S<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> ! <w n="5.7">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.8">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="5.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> !…</l>
						<l n="6" num="2.2"><w n="6.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">n</w>’<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="6.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="6.6">d</w>’<w n="6.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="6.8">l</w>’<w n="6.9"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="6.10">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.11">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.12"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
						<l n="7" num="2.3"><w n="7.1">C</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">r<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="7.5">c</w>’<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="7.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.10">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.11">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.12">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="8.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="8.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="8.7">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.8">n</w>’<w n="8.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="8.11">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="8.12">d</w>’<w n="8.13"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> !</l>
					</lg>
				</div></body></text></TEI>