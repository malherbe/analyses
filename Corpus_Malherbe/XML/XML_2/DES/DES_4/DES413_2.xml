<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ENFANTS ET JEUNES FILLES</head><div type="poem" key="DES413">
					<head type="main">POUR ENDORMIR L’ENFANT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="1.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.3">j</w>’<w n="1.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="1.7">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
						<l n="2" num="1.2"><w n="2.1">Qu</w>’<w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>, <w n="2.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.7">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>,</l>
						<l n="3" num="1.3"><space quantity="8" unit="char"></space><w n="3.1">G<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="3.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="4" num="1.4"><space quantity="8" unit="char"></space><w n="4.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="5" num="1.5"><w n="5.1">J</w>’<w n="5.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.4">l</w>’<w n="5.5">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>…</l>
						<l n="6" num="1.6">(<w n="6.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="6.3">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="6.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="6.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>).</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301">ai</seg>s</w> <w n="7.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.5">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="8" num="2.2"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="8.2">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.3">qu</w>’<w n="8.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.5">m</w>’<w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.8">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="9" num="2.3"><space quantity="8" unit="char"></space><w n="9.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="10" num="2.4"><space quantity="8" unit="char"></space><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="11" num="2.5"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">f<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="11.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="11.7">qu</w>’<w n="11.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.9"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>…</l>
						<l n="12" num="2.6">(<w n="12.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="12.3">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="12.5">l</w>’<w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.8">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>).</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="13.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.3">j</w>’<w n="13.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="13.7">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="14" num="3.2"><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="14.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="15" num="3.3"><space quantity="8" unit="char"></space><w n="15.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.3">l</w>’<w n="15.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="16" num="3.4"><space quantity="8" unit="char"></space><w n="16.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="16.3">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="16.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="17" num="3.5"><w n="17.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">j</w>’<w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="17.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.7">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>…</l>
						<l n="18" num="3.6">(<w n="18.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="18.3">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="18.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="18.7">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>).</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="19.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="19.3">j</w>’<w n="19.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.5">l</w>’<w n="19.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="19.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="19.9">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w></l>
						<l n="20" num="4.2"><w n="20.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="20.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.3"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="20.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="20.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="20.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w></l>
						<l n="21" num="4.3"><space quantity="8" unit="char"></space><w n="21.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="21.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.4">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="22" num="4.4"><space quantity="8" unit="char"></space><w n="22.1">J</w>’<w n="22.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="22.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="22.4">l</w>’<w n="22.5">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="23" num="4.5"><w n="23.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.2">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="23.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="23.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.5">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="23.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>ts</w>…</l>
						<l n="24" num="4.6">(<w n="24.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="24.3">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="24.5">l</w>’<w n="24.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="24.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>).</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="25.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.3">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="25.4">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="25.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="25.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
						<l n="26" num="5.2"><w n="26.1">R<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="26.2">d</w>’<w n="26.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="26.4">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="26.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="26.6">l</w>’<w n="26.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
						<l n="27" num="5.3"><space quantity="8" unit="char"></space><w n="27.1">F<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="27.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3">g<seg phoneme="i" type="vs" value="1" rule="468">î</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="28" num="5.4"><space quantity="8" unit="char"></space><w n="28.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.2">cr<seg phoneme="i" type="vs" value="1" rule="468">î</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="28.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
						<l n="29" num="5.5">« <w n="29.1">D<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="29.2">l</w>’<w n="29.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="29.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="29.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> »…</l>
						<l n="30" num="5.6">(<w n="30.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="30.3">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="30.5">l</w>’<w n="30.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="30.7">qu</w>’<w n="30.8"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="30.9">v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="30.10">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>).</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="31.2">j</w>’<w n="31.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="31.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="31.5">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w> <w n="31.6">h<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="31.7">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>,</l>
						<l n="32" num="6.2"><w n="32.1">B<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="32.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="32.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="32.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="32.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="32.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.8">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w>,</l>
						<l n="33" num="6.3"><space quantity="8" unit="char"></space><w n="33.1">F<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="?" type="va" value="1" rule="34">er</seg></w> <w n="33.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="33.4">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="34" num="6.4"><space quantity="8" unit="char"></space><w n="34.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="34.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="34.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="34.4">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="35" num="6.5"><w n="35.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> : « <w n="35.3">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="35.4">M<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>rs</w>, <w n="35.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.6">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> !…»</l>
						<l n="36" num="6.6">(<w n="36.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="36.3">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="36.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="36.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="36.6">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w> <w n="36.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="36.8">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>)</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1"><w n="37.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="37.2">n</w>’<w n="37.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="37.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="37.5">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="37.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="37.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="37.8">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="38" num="7.2"><w n="38.1">N<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="38.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="38.3">r<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, <w n="38.4">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="38.5">l</w>’<w n="38.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="38.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ;</l>
						<l n="39" num="7.3"><space quantity="8" unit="char"></space><w n="39.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="39.2">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="40" num="7.4"><space quantity="8" unit="char"></space><w n="40.1">F<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="40.3">pr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="41" num="7.5"><w n="41.1">F<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="41.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="41.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="41.6">cl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="42" num="7.6"><w n="42.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="42.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="42.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="42.4">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="42.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="42.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="42.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
					</lg>
					<p>(Sur une mélodie allemande.)</p>
				</div></body></text></TEI>