<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">AMOUR</head><div type="poem" key="DES361">
					<head type="main">L’EAU DOUCE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
								L’eau qui a rencontré la mer ne retrouve <lb></lb>
								jamais sa première douceur.
								</quote>
								<bibl>
									<name>Un poète persan</name>.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">P<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ! <w n="1.4">j</w>’<w n="1.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.6">l</w>’<w n="1.7"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="1.8">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="2.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.3">j</w>’<w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="2.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.7">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> ;</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="3.2">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="3.3">j</w>’<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="3.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t</w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="4.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="4.6">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="5.2">qu</w>’<w n="5.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w></l>
						<l n="6" num="2.2"><w n="6.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>, <w n="6.2">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.5">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="7" num="2.3"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="7.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">t<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">m</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.4">j<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">N<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="9.2">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="9.3">n</w>’<w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="10" num="3.2"><w n="10.1">D</w>’<w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="10.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="10.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="11" num="3.3"><w n="11.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.3">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="11.4">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="12" num="3.4"><w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.3">r<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">L</w>’<w n="13.2"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="13.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.4">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="13.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="13.6">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="13.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="14" num="4.2"><w n="14.1">M</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="14.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5">l</w>’<w n="14.6"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="14.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.8">n<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="15" num="4.3"><w n="15.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="15.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.3">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>, <w n="15.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="15.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="16" num="4.4"><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.3">g<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.4">d<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.2">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414">ë</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="17.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.5">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.6">l<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="18" num="5.2"><w n="18.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="18.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="18.4">qu</w>’<w n="18.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="18.6">m</w>’<w n="18.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> :</l>
						<l n="19" num="5.3">« <w n="19.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="19.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="20" num="5.4"><w n="20.1">D</w>’<w n="20.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="20.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.5">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.8">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="21.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="21.4">l</w>’<w n="21.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="21.6">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
						<l n="22" num="6.2"><w n="22.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>cl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="22.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="22.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.4">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.5">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="23" num="6.3"><w n="23.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="23.2">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.3">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="23.4"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="23.5">r<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="23.6">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="24" num="6.4"><w n="24.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.2">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="24.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="24.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>. »</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">— <w n="25.1">D<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="25.2">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="25.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="25.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.5">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="26" num="7.2"><w n="26.1">S</w>’<w n="26.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>,</l>
						<l n="27" num="7.3"><w n="27.1">B<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="27.2">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="27.3">d</w>’<w n="27.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="27.5">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="27.6">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.7">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="28" num="7.4"><w n="28.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="28.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>-<w n="28.3">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="28.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.5">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.2">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="29.3">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="29.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="30" num="8.2"><w n="30.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="30.2">s</w>’<w n="30.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="30.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.5">cr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w></l>
						<l n="31" num="8.3"><w n="31.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="31.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="31.3">c<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="31.4">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="32" num="8.4"><w n="32.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>, <w n="32.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.4">l</w>’<w n="32.5">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">L</w>’<w n="33.2"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="33.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="33.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>f</w> <w n="33.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="33.7">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="34" num="9.2"><w n="34.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="34.2">n<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="34.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="34.6">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w>,</l>
						<l n="35" num="9.3"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="35.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.3">pl<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="35.5">r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w></l>
						<l n="36" num="9.4"><w n="36.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="36.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="36.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.4">n</w>’<w n="36.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="36.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="36.7">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="37.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ltr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="37.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="37.6">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w></l>
						<l n="38" num="10.2"><w n="38.1">R<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="38.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="38.3">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="38.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="38.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="38.6">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
						<l n="39" num="10.3"><w n="39.1">J</w>’<w n="39.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="39.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="39.4">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="39.5">j</w>’<w n="39.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="39.7">l</w>’<w n="39.8"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="39.9">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="40" num="10.4"><w n="40.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="40.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="40.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="40.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.6">s<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>.</l>
					</lg>
				</div></body></text></TEI>