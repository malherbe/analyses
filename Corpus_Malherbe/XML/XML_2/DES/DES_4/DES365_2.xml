<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">AMOUR</head><div type="poem" key="DES365">
					<head type="main">DERNIÈRE ENTREVUE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>, <w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="1.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="2.3">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="2.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.5">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.4">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="3.5">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="4" num="2.2"><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="4.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="4.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="4.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1"><w n="5.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.4">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="5.6">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w></l>
						<l n="6" num="3.2"><w n="6.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="6.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.4">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="6.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>.</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1"><w n="7.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="7.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="7.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="8" num="4.2"><w n="8.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="8.5">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.6">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>cl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>, <w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="9.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> :</l>
						<l n="10" num="5.2"><w n="10.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="10.3">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="10.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.5">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1"><w n="11.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.4">m</w>’<w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="12" num="6.2"><w n="12.1">L<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ! <w n="12.2">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="12.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.5">l</w>’<w n="12.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="12.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1"><w n="13.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>, <w n="13.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.8">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>,</l>
						<l n="14" num="7.2"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="14.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.6">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>.</l>
					</lg>
					<lg n="8">
						<l n="15" num="8.1"><w n="15.1">Br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="15.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.3">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.4">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="15.5">d</w>’<w n="15.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="16" num="8.2"><w n="16.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="16.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ; <w n="16.4">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="16.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="16.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="9">
						<l n="17" num="9.1"><w n="17.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="17.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rv<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="17.3">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="17.4">d</w>’<w n="17.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="17.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
						<l n="18" num="9.2"><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="18.2">n<seg phoneme="o" type="vs" value="1" rule="318">au</seg>fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.3">d</w>’<w n="18.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.5">t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="18.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>.</l>
					</lg>
					<lg n="10">
						<l n="19" num="10.1"><w n="19.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="19.2">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="19.3">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>-<w n="19.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="19.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="355">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="20" num="10.2"><w n="20.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="20.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					</lg>
					<lg n="11">
						<l n="21" num="11.1"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="21.2">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="21.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.4">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="21.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.6">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="22" num="11.2"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="22.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="22.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="22.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.6"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !</l>
					</lg>
					<lg n="12">
						<l n="23" num="12.1"><w n="23.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="23.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="23.5">n</w>’<w n="23.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="23.8">d</w>’<w n="23.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
						<l n="24" num="12.2"><w n="24.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="24.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.3">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>… <w n="24.5">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="24.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> !</l>
					</lg>
					<lg n="13">
						<l n="25" num="13.1"><w n="25.1">L<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="25.3">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="25.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="25.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> ;</l>
						<l n="26" num="13.2"><w n="26.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.2">t</w>’<w n="26.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="26.5">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> : <w n="26.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.7">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="26.8">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> ?</l>
					</lg>
					<lg n="14">
						<l n="27" num="14.1"><w n="27.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="27.2">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="27.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="27.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.7">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
						<l n="28" num="14.2"><w n="28.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.3">s</w>’<w n="28.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="28.5">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>… <w n="28.6">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.7">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="28.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !…</l>
					</lg>
					<lg n="15">
						<l n="29" num="15.1"><w n="29.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="29.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.3">n</w>’<w n="29.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="29.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="29.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="29.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>…</l>
						<l n="30" num="15.2"><w n="30.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="30.2">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="30.3"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w> <w n="30.4">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="30.5">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="30.6">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> !</l>
					</lg>
				</div></body></text></TEI>