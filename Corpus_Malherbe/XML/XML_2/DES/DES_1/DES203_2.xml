<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>ROMANCES</head><div type="poem" key="DES203">
						<head type="main">LE PRISONNIER DE GUERRE</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.2">t</w>’<w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ? <w n="1.5">R<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
							<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rds</w> <w n="2.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> !</l>
							<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="3.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">l</w>’<w n="3.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="4.2">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="4.3">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="4.4">d</w>’<w n="4.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>.</l>
							<l n="5" num="1.5"><w n="5.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="5.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="6" num="1.6"><w n="6.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="6.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.4">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ;</l>
							<l n="7" num="1.7"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="7.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="8" num="1.8"><space quantity="2" unit="char"></space><w n="8.1">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1"><w n="9.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="9.3">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="10" num="2.2"><w n="10.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="10.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> ;</l>
							<l n="11" num="2.3"><w n="11.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.3">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="12" num="2.4"><w n="12.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3"><seg phoneme="y" type="vs" value="1" rule="391">eu</seg>t</w> <w n="12.4">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.6">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>.</l>
							<l n="13" num="2.5"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.2">gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.5">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="14" num="2.6"><w n="14.1">J</w>’<w n="14.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="14.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="14.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ;</l>
							<l n="15" num="2.7"><w n="15.1">J</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="15.3">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="16" num="2.8"><space quantity="2" unit="char"></space><w n="16.1">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
						</lg>
						<lg n="3">
							<l n="17" num="3.1"><w n="17.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="17.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="17.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">bl<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
							<l n="18" num="3.2"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w>-<w n="18.2">t</w>-<w n="18.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.4">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="18.5">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="18.6">f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> !</l>
							<l n="19" num="3.3"><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="19.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="19.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="19.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="19.6">s<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
							<l n="20" num="3.4"><w n="20.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="20.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="20.3">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="20.4">d</w>’<w n="20.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>.</l>
							<l n="21" num="3.5"><w n="21.1">J</w>’<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="21.3">r<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="21.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="22" num="3.6"><w n="22.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="22.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="22.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.4">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ;</l>
							<l n="23" num="3.7"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="23.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rs</w> <w n="23.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.6">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="24" num="3.8"><space quantity="2" unit="char"></space><w n="24.1">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
						</lg>
						<lg n="4">
							<l n="25" num="4.1"><w n="25.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="26" num="4.2"><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="26.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ;</l>
							<l n="27" num="4.3"><w n="27.1">M<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="27.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="28" num="4.4"><w n="28.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.2">s</w>’<w n="28.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="28.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="28.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
							<l n="29" num="4.5"><w n="29.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="29.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="29.3">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.5">gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="30" num="4.6"><w n="30.1">F<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w>-<w n="30.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="30.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>r</w> <w n="30.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="30.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ?</l>
							<l n="31" num="4.7"><w n="31.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.2">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="31.4">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="32" num="4.8"><space quantity="2" unit="char"></space><w n="32.1">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
						</lg>
						<lg n="5">
							<l n="33" num="5.1"><w n="33.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="33.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.3">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>g<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="34" num="5.2"><w n="34.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="34.2">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.3">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="34.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
							<l n="35" num="5.3"><w n="35.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>qu<seg phoneme="j" type="sc" value="0" rule="489">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.2">s</w>’<w n="35.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="36" num="5.4"><w n="36.1">T<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="36.3">t</w>’<w n="36.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ;</l>
							<l n="37" num="5.5"><w n="37.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="37.2">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="37.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.4">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="38" num="5.6"><w n="38.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="38.2">rn</w> <w n="38.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="38.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="38.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
							<l n="39" num="5.7"><w n="39.1">N<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="39.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="39.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="39.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.6">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="40" num="5.8"><space quantity="2" unit="char"></space><w n="40.1">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
						</lg>
						<lg n="6">
							<l n="41" num="6.1"><w n="41.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="41.2">f<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="41.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="41.5">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="42" num="6.2"><w n="42.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="42.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="42.3">m<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="42.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="42.5">s</w>’<w n="42.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257">eoi</seg>r</w>,</l>
							<l n="43" num="6.3"><w n="43.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="43.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="43.3">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="43.4">j</w>’<w n="43.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="44" num="6.4"><w n="44.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="44.2">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="44.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="44.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="44.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> ;</l>
							<l n="45" num="6.5"><w n="45.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="45.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="45.3"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="45.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="45.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="45.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
							<l n="46" num="6.6"><w n="46.1">S</w>’<w n="46.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="46.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="46.4">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
							<l n="47" num="6.7"><w n="47.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="47.2">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="47.3">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="47.4">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="47.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
							<l n="48" num="6.8"><space quantity="2" unit="char"></space><w n="48.1">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="48.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="48.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
						</lg>
					</div></body></text></TEI>