<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ROMANCES</head><div type="poem" key="DES114">
						<head type="main">LA SÉPARATION</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="1.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w>, <w n="1.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.7">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ;</l>
							<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="2.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>, <w n="2.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
							<l n="3" num="1.3"><w n="3.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="3.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>, <w n="3.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.7">f<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> :</l>
							<l n="4" num="1.4"><w n="4.1">S<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="4.2">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="4.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="4.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="4.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.6">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
							<l n="5" num="1.5"><w n="5.1">S<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="5.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>… <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>, <w n="5.4">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
							<l n="6" num="1.6"><w n="6.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="6.4">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="6.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1"><w n="7.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="7.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.3">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="7.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.5">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
							<l n="8" num="2.2"><w n="8.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.6">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="9" num="2.3"><w n="9.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3">f<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="9.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
							<l n="10" num="2.4"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="10.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
							<l n="11" num="2.5"><w n="11.1">S<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="11.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>… <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>, <w n="11.4">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
							<l n="12" num="2.6"><w n="12.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="12.4">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.5">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="12.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1"><w n="13.1">R<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>-<w n="13.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rtr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
							<l n="14" num="3.2"><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="14.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
							<l n="15" num="3.3"><w n="15.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="15.2">n</w>’<w n="15.3"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="15.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="15.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.7">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>,</l>
							<l n="16" num="3.4"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="16.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="16.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> !</l>
							<l n="17" num="3.5"><w n="17.1">S<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="17.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>… <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>, <w n="17.4">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
							<l n="18" num="3.6"><w n="18.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="18.4">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.5">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="18.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
						</lg>
					</div></body></text></TEI>