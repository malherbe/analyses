<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES45">
						<head type="main">ÉLÉGIE</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.4">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="1.5">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.9">t</w>’<w n="1.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="1.11">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>.</l>
							<l n="2" num="1.2"><w n="2.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.2">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.4">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="2.6">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="2.7">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.10">t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
							<l n="3" num="1.3"><w n="3.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="3.3">m</w>’<w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="3.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="3.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.8">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> ;</l>
							<l n="4" num="1.4"><w n="4.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">s</w>’<w n="4.4"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="4.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.9">m<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
							<l n="5" num="1.5"><w n="5.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.7">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="5.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.10">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> ;</l>
							<l n="6" num="1.6"><w n="6.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">l</w>’<w n="6.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="6.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>, <w n="6.5">j</w>’<w n="6.6"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="6.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.8">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
							<l n="7" num="1.7"><w n="7.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="7.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="7.6">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.9">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
							<l n="8" num="1.8"><w n="8.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">cr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="8.3">qu</w>’<w n="8.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.5">m</w>’<w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="8.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="8.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.9">pr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.10">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>.</l>
							<l n="9" num="1.9"><w n="9.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="9.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.3">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ? <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> <w n="9.6">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>, <w n="9.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.8">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.9">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="10" num="1.10"><w n="10.1">J</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="10.3">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="10.5">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.10">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="11" num="1.11"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.7">pr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="11.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w>,</l>
							<l n="12" num="1.12"><w n="12.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="12.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="12.7">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="12.8">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>.</l>
							<l n="13" num="1.13"><w n="13.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="13.3">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="13.5">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>, <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.8"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="13.9">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.10">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>.</l>
							<l n="14" num="1.14"><w n="14.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="14.4">m<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="14.5">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="14.7">s</w>’<w n="14.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> ;</l>
							<l n="15" num="1.15"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="15.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="15.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.4">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="15.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.7">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="15.8">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.9">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>,</l>
							<l n="16" num="1.16"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.5">j</w>’<w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="16.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : <w n="16.8">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.9">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> !</l>
							<l n="17" num="1.17"><w n="17.1">D<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="17.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="17.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="17.4">r<seg phoneme="ə" type="vi" value="1" rule="351">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="17.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
							<l n="18" num="1.18"><w n="18.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="18.3">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t</w> <w n="18.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="18.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.6"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="18.7">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="18.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
							<l n="19" num="1.19"><w n="19.1">J</w>’<w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xpr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="19.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="19.5">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="19.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="19.8">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="19.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> ;</l>
							<l n="20" num="1.20"><w n="20.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.2">l</w>’<w n="20.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="20.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="20.5">m<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="20.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="20.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="20.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.9">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>.</l>
							<l n="21" num="1.21"><w n="21.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.3">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="21.5">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="21.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="21.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.9">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
							<l n="22" num="1.22"><space quantity="10" unit="char"></space><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="22.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="22.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
							<l n="23" num="1.23"><w n="23.1">D</w>’<w n="23.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="23.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="23.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="23.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
							<l n="24" num="1.24"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="24.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.3"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="24.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="24.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="24.6">s</w>’<w n="24.7"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="24.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
							<l n="25" num="1.25"><w n="25.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.2">l</w>’<w n="25.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>… <w n="25.4">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="25.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.6">n</w>’<w n="25.7"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="25.8">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="25.9">l</w>’<w n="25.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="26" num="1.26"><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="26.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.3">t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="26.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301">ai</seg>t</w> <w n="26.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="26.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
							<l n="27" num="1.27"><w n="27.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="27.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="27.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.5">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="27.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="27.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="27.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.9">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>,</l>
							<l n="28" num="1.28"><w n="28.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="28.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="28.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="28.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.7">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> ;</l>
							<l n="29" num="1.29"><w n="29.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="29.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="29.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="29.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="29.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w>, <w n="29.7">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.8">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="30" num="1.30"><w n="30.1">C</w>’<w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="30.3">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="30.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="30.5">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="30.8">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.10">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="30.11">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
							<l n="31" num="1.31"><w n="31.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="31.2">ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ! <w n="31.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="31.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ! <w n="31.5"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="31.8">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> !</l>
							<l n="32" num="1.32"><w n="32.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="32.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="32.4">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="32.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.8">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.9">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.10">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
							<l n="33" num="1.33"><w n="33.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="33.2">m</w>’<w n="33.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="33.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="33.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w>, <w n="33.7">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="33.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="33.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.10">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>,</l>
							<l n="34" num="1.34"><w n="34.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="34.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="34.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="34.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="34.6">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="34.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.8">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
					</div></body></text></TEI>