<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN62">
					<head type="main">L’Amour.</head>
					<head type="form">STANCES.</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="1.2">M<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">T<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="2.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.3">s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4">d</w>’<w n="3.5"><seg phoneme="i" type="vs" value="1" rule="497">y</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.4">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
						<l n="5" num="1.5"><space unit="char" quantity="4"></space><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.5">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> :</l>
						<l n="6" num="1.6"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.2">n</w>’<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><space unit="char" quantity="8"></space><w n="7.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="7.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="7.4">s<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="7.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w>,</l>
						<l n="8" num="2.2"><space unit="char" quantity="8"></space><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="8.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">g<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> <w n="8.4">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> ;</l>
						<l n="9" num="2.3"><space unit="char" quantity="8"></space><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="9.5">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="10" num="2.4"><space unit="char" quantity="8"></space><w n="10.1">M<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.4">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
						<l n="11" num="2.5"><space unit="char" quantity="4"></space><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.5">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> :</l>
						<l n="12" num="2.6"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.2">n</w>’<w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="12.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.6">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><space unit="char" quantity="8"></space><w n="13.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="493">y</seg></w>-<w n="13.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w></l>
						<l n="14" num="3.2"><space unit="char" quantity="8"></space><w n="14.1">R<seg phoneme="ə" type="vi" value="1" rule="351">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="14.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ;</l>
						<l n="15" num="3.3"><space unit="char" quantity="8"></space><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.4">T<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="15.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="16" num="3.4"><space unit="char" quantity="8"></space><w n="16.1">R<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="16.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="16.3">C<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="16.4">qu</w>’<w n="16.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="17" num="3.5"><space unit="char" quantity="4"></space><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="17.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.5">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> :</l>
						<l n="18" num="3.6"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="18.2">n</w>’<w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="18.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.6">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><space unit="char" quantity="8"></space><w n="19.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2">C<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="19.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="19.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="19.6">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="20" num="4.2"><space unit="char" quantity="8"></space><w n="20.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.4"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ;</l>
						<l n="21" num="4.3"><space unit="char" quantity="8"></space><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="21.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.3">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="21.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="21.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="22" num="4.4"><space unit="char" quantity="8"></space><w n="22.1">Br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="22.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="22.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="23" num="4.5"><space unit="char" quantity="4"></space><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="23.2">l</w>’<w n="23.3"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="23.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.5">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="23.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> :</l>
						<l n="24" num="4.6"><w n="24.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="24.2">n</w>’<w n="24.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="24.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.6">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><space unit="char" quantity="8"></space><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="25.2">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="25.3"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.4">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="26" num="5.2"><space unit="char" quantity="8"></space><w n="26.1">D<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="26.2"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="27" num="5.3"><space unit="char" quantity="8"></space><w n="27.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="27.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3">qu</w>’<w n="27.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="27.5">s<seg phoneme="wa" type="vs" value="1" rule="424">oy</seg></w> <w n="27.6">r<seg phoneme="ə" type="vi" value="1" rule="351">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="28" num="5.4"><space unit="char" quantity="8"></space><w n="28.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.2">C<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="28.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="28.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.5">T<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="29" num="5.5"><space unit="char" quantity="4"></space><w n="29.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="29.2">l</w>’<w n="29.3"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="29.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.5">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="29.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> :</l>
						<l n="30" num="5.6"><w n="30.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="30.2">n</w>’<w n="30.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="30.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.6">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
					</lg>
				</div></body></text></TEI>