<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE CY GIST</head><head type="sub_part">
					OU 
					Diverses Épitaphes pour toute sorte de personnes 
					de l’un et de l’autre sexe, et pour l’Auteur même, 
					quoique vivant.
				</head><div type="poem" key="BEN146">
					<head type="main">Épitaphe des plus grands Héros.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C<seg phoneme="i" type="vs" value="1" rule="493">y</seg></w> <w n="1.2">g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st</w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.4">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>qu<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.8">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="1.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="1.10">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.3">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="2.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.8">C<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">sç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="3.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="3.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="3.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="3.7">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.10">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="4.2">n</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="4.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w>-<w n="4.6">d<seg phoneme="ə" type="vi" value="1" rule="156">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.7">d</w>’<w n="4.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.9">Cr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="4.10">d</w>’<w n="4.11"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ?</l>
						<l n="5" num="1.5"><space unit="char" quantity="4"></space><w n="5.1">C<seg phoneme="i" type="vs" value="1" rule="493">y</seg></w> <w n="5.2">g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st</w> <w n="5.3">P<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="355">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.5">C<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>.</l>
						<l n="6" num="1.6"><space unit="char" quantity="4"></space><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="6.2"><seg phoneme="y" type="vs" value="1" rule="391">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="6.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="6.4">tr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>ph<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="6.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.7">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>,</l>
						<l n="7" num="1.7"><space unit="char" quantity="4"></space><w n="7.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="?" type="va" value="1" rule="34">er</seg></w> <w n="7.3">c<seg phoneme="i" type="vs" value="1" rule="493">y</seg></w> <w n="7.4">g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st</w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="7.8">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="7.9">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="8" num="1.8"><space unit="char" quantity="4"></space><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="8.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="8.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.8">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="9" num="1.9"><space unit="char" quantity="4"></space><w n="9.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>-<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="9.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> ;</l>
						<l n="10" num="1.10"><space unit="char" quantity="4"></space><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.3">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>,</l>
						<l n="11" num="1.11"><space unit="char" quantity="4"></space><w n="11.1">S</w>’<w n="11.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="11.3">n</w>’<w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="11.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.10"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> ;</l>
						<l n="12" num="1.12"><space unit="char" quantity="4"></space><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="12.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="12.4">qu<seg phoneme="wa" type="vs" value="1" rule="424">oy</seg></w> <w n="12.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.7">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="12.8">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>,</l>
						<l n="13" num="1.13"><space unit="char" quantity="4"></space><w n="13.1">Qu</w>’<w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="13.4">t</w>-<w n="13.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> ? <w n="13.6">qu</w>’<w n="13.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.9">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.10"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.11">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w></l>
						<l n="14" num="1.14"><space unit="char" quantity="4"></space><w n="14.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>, <w n="14.2">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>st</w> <w n="14.3"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="14.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>, <w n="14.5">c<seg phoneme="i" type="vs" value="1" rule="493">y</seg></w> <w n="14.6">g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st</w> <w n="14.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.8">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.9">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>fr<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> ?</l>
					</lg>
				</div></body></text></TEI>