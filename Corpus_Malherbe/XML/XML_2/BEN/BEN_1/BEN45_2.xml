<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN45">
					<head type="main">Autre à Iris.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="vi" value="1" rule="348">E</seg></w> <w n="1.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="1.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n<seg phoneme="y" type="vs" value="1" rule="463">u</seg><seg phoneme="j" type="sc" value="0" rule="475">ï</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="2.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>r</w> <w n="2.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.7">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>nh<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="3.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.7"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="4" num="1.4"><w n="4.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.8">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.9">s</w>’<w n="4.10"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xpl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.3">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="5.4">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w>-<w n="5.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.7">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="5.9">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.10">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="6.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>str<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="6.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.8">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.9">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="7" num="2.3"><w n="7.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="7.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="7.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.7">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="7.9">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ;</l>
						<l n="8" num="2.4"><w n="8.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.2">bl<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.7">fl<seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.9">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="9.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="9.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.8">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.9">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="10.3">qu</w>’<w n="10.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="10.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>, <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.7">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.8">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.10">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ;</l>
						<l n="11" num="3.3"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.3">v<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="11.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="11.8">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.9">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="12.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">m</w>’<w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>,</l>
						<l n="13" num="4.2"><w n="13.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="13.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.8">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="13.9">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.10">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.11">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="14" num="4.3"><w n="14.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">v<seg phoneme="wa" type="vs" value="1" rule="424">oy</seg></w> <w n="14.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.4">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="14.5">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w> <w n="14.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="14.7">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.8">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="14.9">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>.</l>
					</lg>
				</div></body></text></TEI>