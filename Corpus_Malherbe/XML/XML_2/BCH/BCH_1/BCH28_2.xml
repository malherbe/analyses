<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA FLEUR DES EAUX</head><div type="poem" key="BCH28">
					<head type="number">XXVII</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="1.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="1.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.7"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.9">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.10">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">t</w>’<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">d</w>’<w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.7">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.8"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>,</l>
						<l n="3" num="1.3"><w n="3.1">P<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.4">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7">l</w>’<w n="3.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="3.9">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.10">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="4.8">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct</w> <w n="4.9">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.10">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="5.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="5.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w></l>
						<l n="6" num="2.2"><w n="6.1">D</w>’<w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.7">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.9">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="7" num="2.3"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.4">l<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="7.6">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="7.7">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.9">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.10">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">m</w>’<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.6">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="8.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="8.9">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.3">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.6">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.8">l</w>’<w n="9.9"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="10" num="3.2"><w n="10.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.5">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.7">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.9">l</w>’<w n="10.10"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="11.2">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="11.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.7">s<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.9">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.10">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> :</l>
						<l n="12" num="3.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.3">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.5">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.8">d</w>’<w n="12.9"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.10">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>