<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DRAMES ET FANTAISIES</head><head type="main_subpart">Drame en trois ballades</head><div type="poem" key="CRO56">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.2">f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r</w> <w n="1.3">l</w>’<w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="1.8">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.9">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="2" num="1.2"><w n="2.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="2.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">m</w>’<w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.7">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="2.9">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w></l>
							<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="3.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="3.6">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="3.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w>.</l>
							<l n="4" num="1.4"><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.3">ch<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w> <w n="4.4">qu</w>’<w n="4.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>pl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="4.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.9">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="5.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.5">t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="5.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w>.</l>
							<l n="6" num="1.6"><w n="6.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>, <w n="6.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">m</w>’<w n="6.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="6.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.6">l</w>’<w n="6.7">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.8">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
							<l n="7" num="1.7"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.2">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.4">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ch<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.6">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></l>
							<l n="8" num="1.8"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="8.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="8.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="8.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.7">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="8.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
							<l n="9" num="1.9"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ls</w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="9.4">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="9.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
							<l n="10" num="1.10"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="10.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="11" num="2.1"><w n="11.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="11.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.5">fl<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.7">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.8">bl<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="12" num="2.2"><w n="12.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="12.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t</w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.5">r<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="12.7">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> <w n="12.8">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>.</l>
							<l n="13" num="2.3"><w n="13.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z</w> <w n="13.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.4">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="13.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="13.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="13.8"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w>,</l>
							<l n="14" num="2.4"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ls</w> <w n="14.3">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>cs</w>, <w n="14.4">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="14.5">d</w>’<w n="14.6"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="15" num="2.5"><w n="15.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="15.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w>, <w n="15.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="15.5">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="15.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="15.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.8">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>.</l>
							<l n="16" num="2.6"><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="16.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="16.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="17" num="2.7"><w n="17.1">M<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="17.3">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="17.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="17.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
							<l n="18" num="2.8"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.2">b<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.3">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="18.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="19" num="2.9"><w n="19.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="19.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> ! <w n="19.3">R<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="19.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
							<l n="20" num="2.10"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="20.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="20.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
						<lg n="3">
							<l n="21" num="3.1"><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="21.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="21.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>… <w n="21.5">D<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>. <w n="21.6">R<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="21.7">d</w>’<w n="21.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>. <w n="21.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="21.10">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="22" num="3.2"><w n="22.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="22.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.4">sc<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="22.5">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w></l>
							<l n="23" num="3.3"><w n="23.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="23.2">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="23.5">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="23.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>,</l>
							<l n="24" num="3.4"><w n="24.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="24.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="24.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.6">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="24.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="25" num="3.5"><w n="25.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="25.2">rh<seg phoneme="i" type="vs" value="1" rule="493">y</seg>thm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="25.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="25.5">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="25.6">d</w>’<w n="25.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w> <w n="25.8">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> ?</l>
							<l n="26" num="3.6"><w n="26.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="26.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.3">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.6">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="26.7">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="27" num="3.7"><w n="27.1">F<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ə" type="vi" value="1" rule="242">e</seg>nt</w> <w n="27.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.3"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="27.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="27.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.6">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="27.7">bl<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></l>
							<l n="28" num="3.8"><w n="28.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="28.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="28.3">l</w>’<w n="28.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="29" num="3.9"><w n="29.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="29.2">cr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="29.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.4"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="29.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
							<l n="30" num="3.10"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="30.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="30.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="30.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
						<lg n="4">
							<head type="form">ENVOI</head>
							<l n="31" num="4.1"><w n="31.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="31.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="31.3">l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="31.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>, <w n="31.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="31.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="32" num="4.2"><w n="32.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.2">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="32.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.4">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="32.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="32.6">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.7">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
							<l n="33" num="4.3"><w n="33.1">L</w>’<w n="33.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="33.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="33.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.5">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.6">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="34" num="4.4"><w n="34.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="34.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="34.4">m<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="34.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="34.6">fr<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> <w n="34.7">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
							<l n="35" num="4.5"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="35.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="35.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="35.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
					</div></body></text></TEI>