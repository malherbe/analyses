<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DRAMES ET FANTAISIES</head><div type="poem" key="CRO53">
					<head type="main">Insomnie</head>
					<opener>
						<salute>A Eugène Zerlaut</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="1.4">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.2">v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="2.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.5">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>,</l>
						<l n="3" num="1.3"><w n="3.1">R<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="e" type="vs" value="1" rule="383">e</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="3.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.4">cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="4.3">l</w>’<w n="4.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.7">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">C<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>cts</w>, <w n="5.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">z<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c</w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="6" num="2.2"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ts</w> <w n="6.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="6.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="6.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>,</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="7.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>, <w n="7.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="7.6">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rge<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
						<l n="8" num="2.4"><w n="8.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>. <w n="8.5">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="9.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="10" num="3.2"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="10.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg>s</w> ?</l>
						<l n="11" num="3.3"><w n="11.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="11.3">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="11.4">gr<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.7">l</w>’<w n="11.8">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="12" num="3.4"><w n="12.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg>ts</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> ! <w n="13.4">c</w>’<w n="13.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="13.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.7">fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="14" num="4.2"><w n="14.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="14.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w> <w n="14.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>-<w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="14.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.6">m<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="15.2">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="15.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.5">br<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="16" num="4.4"><w n="16.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="16.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.4">g<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>ts</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="17.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>-<w n="17.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xqu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="18" num="5.2"><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="18.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="18.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="18.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="18.5"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="18.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.7">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> ! …</l>
						<l n="19" num="5.3"><w n="19.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="19.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="20" num="5.4"><w n="20.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="20.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="20.4">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="20.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">L</w>’<w n="21.2"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="21.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="21.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="21.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.7">br<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="22" num="6.2"><w n="22.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.2">g<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="22.3">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="22.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
						<l n="23" num="6.3"><w n="23.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.2">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="23.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="23.4">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="24" num="6.4"><w n="24.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="24.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="24.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>ps</w></l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">M</w>’<w n="25.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="25.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="360">en</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>. <w n="25.4">J</w>’<w n="25.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="25.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="25.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.8">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="26" num="7.2"><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="26.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="26.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="26.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.6">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
						<l n="27" num="7.3"><w n="27.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.2">b<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="27.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="27.4"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="27.5">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="28" num="7.4"><w n="28.1">M</w>’<w n="28.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="28.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>str<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="28.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.6">pr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="28.7">v<seg phoneme="ø" type="vs" value="1" rule="248">œu</seg>x</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="29.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> <w n="29.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="29.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="29.6">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="30" num="8.2"><w n="30.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">Em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="30.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="30.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>,</l>
						<l n="31" num="8.3"><w n="31.1">J</w>’<w n="31.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="31.3">pl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="31.4">m<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="31.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="31.6">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="32" num="8.4"><w n="32.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="32.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> <w n="32.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="33.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="33.4">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="33.5">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="34" num="9.2"><w n="34.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="34.2">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="34.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="34.4">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d</w> <w n="34.5">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>,</l>
						<l n="35" num="9.3"><w n="35.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="35.2">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w> <w n="35.3">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="35.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="35.5">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="35.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="36" num="9.4"><w n="36.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="36.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="36.3">b<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="36.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>, <w n="37.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="37.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="37.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="37.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="37.6">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="38" num="10.2"><w n="38.1">J</w>’<w n="38.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="38.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="38.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="38.5">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ds</w> <w n="38.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w></l>
						<l n="39" num="10.3"><w n="39.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="39.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="39.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="39.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="39.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="39.6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="40" num="10.4"><w n="40.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="40.2">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="40.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="40.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="40.5">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="40.6">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ds</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="41.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.3">n</w>’<w n="41.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="41.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="41.6">l</w>’<w n="41.7"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="42" num="11.2"><w n="42.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="42.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="42.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="42.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="42.5">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w></l>
						<l n="43" num="11.3"><w n="43.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="43.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="43.3">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="43.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="43.5">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !…</l>
						<l n="44" num="11.4"><w n="44.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="44.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="44.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="44.4">d</w>’<w n="44.5"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="44.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> !</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">C<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>cts</w>, <w n="45.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="45.3">z<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c</w> <w n="45.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="45.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="45.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="46" num="12.2"><w n="46.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="46.2">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ts</w> <w n="46.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="46.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="46.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="46.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>,</l>
						<l n="47" num="12.3"><w n="47.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="47.2">s</w>’<w n="47.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="47.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="47.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="47.6">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rge<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="48" num="12.4"><w n="48.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="48.2">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="48.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="48.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="48.5">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="48.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="48.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="48.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>.</l>
					</lg>
				</div></body></text></TEI>