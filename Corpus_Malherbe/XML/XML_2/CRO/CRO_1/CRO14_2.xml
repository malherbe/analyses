<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS PERPÉTUELLES</head><div type="poem" key="CRO14">
					<head type="main">Chant éthiopien</head>
					<opener>
						<salute>A Émile Wroblewski</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="1.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.4">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="2" num="1.2"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="2.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="3" num="1.3"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.2">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="3.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.6">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>-<w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
						<l n="4" num="1.4"><w n="4.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="4.4">s</w>’<w n="4.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="5.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.4">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="5.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.6">fl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.7">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="5.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
						<l n="6" num="2.2"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.2">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="6.3">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="6.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w>, <w n="6.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>tr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="7.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> ?</l>
						<l n="8" num="2.4"><w n="8.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.2">c<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> — <w n="8.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="8.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> — <w n="8.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.7">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.8">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.2">c<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="9.3">qu</w>’<w n="9.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="9.5">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.8">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="9.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="10" num="3.2"><w n="10.1">Fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="10.2">d</w>’<w n="10.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>. <w n="10.4">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.5">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="10.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="10.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="11" num="3.3"><w n="11.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="11.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="11.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.7">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
						<l n="12" num="3.4"><w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, <w n="12.4">j<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.6"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="13.3">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="13.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
						<l n="14" num="4.2"><w n="14.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>-<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="14.4">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.7">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="15" num="4.3"><w n="15.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="15.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="15.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="15.7">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ;</l>
						<l n="16" num="4.4"><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> — <w n="16.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="16.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> — <w n="16.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="16.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.8">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="17.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="17.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.8">c<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="18" num="5.2"><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w> <w n="18.3">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="18.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="18.5">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="18.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.7">p<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="19" num="5.3"><w n="19.1">D<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="19.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.3">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="19.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="19.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.6"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="19.7">m</w>’<w n="19.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.9">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>,</l>
						<l n="20" num="5.4"><w n="20.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ; <w n="20.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="20.6">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.7">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="20.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.9">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>.</l>
					</lg>
				</div></body></text></TEI>