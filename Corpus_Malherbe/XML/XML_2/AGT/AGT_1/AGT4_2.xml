<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="AGT">
					<name>
						<forename>Marie</forename>
						<nameLink>d’</nameLink>
						<surname>AGOULT</surname>
						<addName type="pen_name">Daniel STERN</addName>
					</name>
					<date from="1805" to="1876">1805-1876</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>185 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AGT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ESQUISSES MORALES : PENSÉES, RÉFLEXIONS, MAXIMES</title>
						<author>Daniel STERN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://catalogue.bnf.fr/ark:/12148/cb30005117k</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ESQUISSES MORALES : PENSÉES, RÉFLEXIONS, MAXIMES</title>
							<author>Daniel STERN</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>CALMANN LÉVY, ÉDITEUR</publisher>
									<date when=" 1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1880">1880</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition ne porte que sur le chapitre consacrée aux poésies.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES</head><div type="poem" key="AGT4">
					<head type="number">IV</head>
					<head type="main">L’OLIVIER</head>
					<head type="form">SONNET</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">B<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="1.4">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="1.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="1.6">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.8">n<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.9">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
						<l n="2" num="1.2"><w n="2.1">F<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="2.3">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.6">b<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.7">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.8">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="4.2">J<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="4.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.5">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="4.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t</w> <w n="4.7">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.9">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="4.10">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>th<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="5.4">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="5.7">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
						<l n="6" num="2.2"><w n="6.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="6.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="6.4">s</w>’<w n="6.5"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.8">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="7" num="2.3"><w n="7.1">L</w>’<w n="7.2">H<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="7.3">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="7.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ppl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.8">P<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="8.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="8.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.5">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.7">l</w>’<w n="8.8">h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.9">d<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="8.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">D</w>’<w n="9.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>th<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1">T<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="10.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rg<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="10.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.5">s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w>, <w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1">D</w>’<w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5">p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>x</w>, <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="11.7">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="11.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="12.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="12.5">m</w>’<w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257">eoi</seg>r</w> <w n="12.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="13" num="4.2"><w n="13.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.2">chr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="13.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.6">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="14" num="4.3"><w n="14.1">P<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.9">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.10"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
					</lg>
				</div></body></text></TEI>