<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><div type="poem" key="AIC46">
					<head type="number">XII</head>
					<head type="main">CINCINNATUS</head>
					<opener>
						<salute><hi rend="ital">Hommage aux ouvriers de Bandol.</hi></salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="1.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>. <w n="1.5">L</w>’<w n="1.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.7">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.8">d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="2.2">l</w>’<w n="2.3">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.6">p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.7"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ppr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">D<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="4.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="4.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.5">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.2">Sc<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.4">cr<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="5.7">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ds</w> <w n="5.8">s<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">d</w>’<w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.7">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="6.8">s</w>’<w n="6.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w>, <w n="7.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.8">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>,</l>
						<l n="8" num="2.4"><w n="8.1">N<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="8.6">qu</w>’<w n="8.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="9.4">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="9.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> ;</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="10.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.4">l</w>’<w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>, <w n="10.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="10.7">p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="10.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.9">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
						<l n="11" num="3.3"><w n="11.1">N</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="11.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.7">l</w>’<w n="11.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>pp<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.9">d</w>’<w n="11.10">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">N<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>, <w n="12.3">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.4">s<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> ! <w n="12.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.6">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> !</l>
						<l n="13" num="4.2"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.3">m<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="13.4">d</w>’<w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="13.7">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.8">f<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="13.9">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>,</l>
						<l n="14" num="4.3"><w n="14.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">C<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>nn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,… <w n="14.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="14.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="14.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.8">R<seg phoneme="ɔ" type="vs" value="1" rule="441">o</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Bandol</placeName>,
							<date when="1866">27 décembre 1866</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>