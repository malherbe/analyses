<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><div type="poem" key="AIC9">
					<head type="number">VI</head>
					<head type="main">À TOI QUI VEUX MOURIR</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg>h</w> ! <w n="1.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">t</w>’<w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="1.6">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.2">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="2.4">pl<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.5">d</w>’<w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> !</l>
						<l n="3" num="1.3"><w n="3.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> : <w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>-<w n="3.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="3.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.6">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.2">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="4.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">N<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="6.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="6.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.8">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="7" num="2.3"><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="7.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="7.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1">S<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="8.4">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="9.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="9.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.8">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="10" num="3.2"><w n="10.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="10.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>,</l>
						<l n="11" num="3.3"><w n="11.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="11.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="11.4">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="11.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="12" num="3.4"><w n="12.1">Qu</w>’<w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="12.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="13.4">qu</w>’<w n="13.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.7">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="14" num="4.2"><w n="14.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="15" num="4.3"><w n="15.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="15.3">l</w>’<w n="15.4">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="15.6">l</w>’<w n="15.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="16" num="4.4"><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="17.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.6">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="17.8">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="18" num="5.2"><w n="18.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.3">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="18.4">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="18.5">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="18.6">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>,</l>
						<l n="19" num="5.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="19.2">qu</w>’<w n="19.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.7">dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="19.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.9">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="20" num="5.4"><w n="20.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>, <w n="20.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.6">l</w>’<w n="20.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1"><seg phoneme="w" type="sc" value="0" rule="431">Ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="21.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.4">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="22" num="6.2"><w n="22.1">C</w>’<w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="22.3">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="22.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>, <w n="22.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="22.6">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="22.7">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> ;</l>
						<l n="23" num="6.3"><w n="23.1">C</w>’<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="23.3">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="23.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="23.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.7">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
						<l n="24" num="6.4"><w n="24.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.2">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="24.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="24.5">qu</w>’<w n="24.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.7">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="25.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="25.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="26" num="7.2"><w n="26.1">J<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.2">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="26.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="26.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>…</l>
						<l n="27" num="7.3"><w n="27.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="27.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="27.3">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="27.4">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="27.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="28" num="7.4"><w n="28.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="28.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="28.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="28.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.6"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="29.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>-<w n="29.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="29.4">j</w>’<w n="29.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="29.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.7">t<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="30" num="8.2"><w n="30.1">J</w>’<w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="30.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="30.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="30.7">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="31" num="8.3"><w n="31.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="31.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="31.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.6">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="32" num="8.4"><w n="32.1">D<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="32.2">t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="32.3"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="32.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="32.5">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="33.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>-<w n="33.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="33.4">j</w>’<w n="33.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="33.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.7">g<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="33.9">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="34" num="9.2"><w n="34.1">J</w>’<w n="34.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="34.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="34.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="34.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="34.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ;</l>
						<l n="35" num="9.3"><w n="35.1">J</w>’<w n="35.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="35.4">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="35.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.7">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
						<l n="36" num="9.4"><w n="36.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.2">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="36.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="36.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.6">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="37.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="37.3">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w>, <w n="37.4">j</w>’<w n="37.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="37.6">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="37.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="37.8">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.9">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="38" num="10.2"><w n="38.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="38.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="38.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="38.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="38.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="38.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="38.7">m<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> ;</l>
						<l n="39" num="10.3"><w n="39.1">N<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.2">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="39.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="39.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="39.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="39.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="40" num="10.4"><w n="40.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="40.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="40.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="40.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.7">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> !</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="41.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.5">d<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="41.6">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
						<l n="42" num="11.2">« <w n="42.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="42.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="42.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="42.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="42.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> !</l>
						<l n="43" num="11.3"><w n="43.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="43.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="43.3">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> ; <w n="43.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="43.5">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="43.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="44" num="11.4"><w n="44.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="44.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="44.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="44.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="44.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="44.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="44.7">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ! »</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="45.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ! <w n="45.3">c</w>’<w n="45.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="45.5">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="45.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ff<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="46" num="12.2"><w n="46.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="46.2">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="46.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="46.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="46.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="47" num="12.3"><w n="47.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="47.2">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w>, <w n="47.3">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="47.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="48" num="12.4"><w n="48.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="48.2">cr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="48.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="48.4">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> : « <w n="48.5">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>-<w n="48.6">t</w>’<w n="48.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> ! »</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Toulon</placeName>,
							<date when="1866">21-22 juin 1866</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>