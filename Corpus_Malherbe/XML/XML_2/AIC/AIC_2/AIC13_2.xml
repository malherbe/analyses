<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><div type="poem" key="AIC13">
					<head type="number">X</head>
					<head type="main">SOLUS ERIS</head>
					<opener>
						<salute><hi rend="ital">À mon ami Alexandre M.</hi></salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> : <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.5">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="1.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rg<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="1.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="1.9">r<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="2.3">s</w>’<w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="2.7">l</w>’<w n="2.8">h<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.9">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="3.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.4">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="3.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.8">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="4.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="4.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>str<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.6"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="4.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.2">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.4">l</w>’<w n="5.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.6">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.7">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="5.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.9">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.10">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="6" num="2.2"><w n="6.1">D<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.10">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.11">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
						<l n="7" num="2.3"><w n="7.1">C<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="7.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.3">l</w>’<w n="7.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="7.5">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.8">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="7.9">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.10">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="8.2">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="8.5">qu</w>’<w n="8.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="8.7">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="8.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.9">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.2">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.4">l</w>’<w n="9.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="9.6">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="9.7">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.9">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>-<w n="9.10">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1">N<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>f</w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.3">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.4">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.6">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
						<l n="11" num="3.3"><w n="11.1">N</w>’<w n="11.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="11.5">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.6">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> : « <w n="11.8">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.9">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.10"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, »</l>
						<l n="12" num="3.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="12.7">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.10"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">S<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>. — <w n="13.2">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="13.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.6">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>. <w n="13.7">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="13.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.9">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="13.10">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.11">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.12">n<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="14" num="4.2"><w n="14.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="14.2">j</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.5">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> : <w n="14.6">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> !</l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="15.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="15.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.5">f<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.7">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="15.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.9">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="15.10">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="16" num="4.4"><w n="16.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="16.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="16.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="16.4">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>, <w n="16.5">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.6">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.7">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="16.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> !</l>
					</lg>
					<closer>
						<placeName>Toulon</placeName>.
					</closer>
				</div></body></text></TEI>