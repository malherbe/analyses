<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANTS DE PALESTINE</head><div type="poem" key="HRV47">
				<head type="main">LA COUCHE FLEURIE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="1.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.8">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>-<w n="1.9"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">S</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.3">l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>… <w n="2.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.8">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="2.10">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>…</l>
						<l n="3" num="1.3"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.2">j</w>’<w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.4">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="3.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.9">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="3.10">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.11">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="4.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.7">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.8">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J</w>’<w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.3">h<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="5.4">j<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.6">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="5.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.8">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.9">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="6" num="2.2"><w n="6.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w> <w n="6.3">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sm<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.6">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="6.7">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="6.8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="6.9">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.10">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
						<l n="7" num="2.3"><w n="7.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">l</w>’<w n="7.3">h<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="7.5">l</w>’<w n="7.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>sph<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.8">r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w></l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="8.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rpr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="8.7">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.8">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.9">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="9.5">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="9.6">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w></l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg>t</w> <w n="10.2">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="10.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.4">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="10.6">fr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> <w n="10.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.8">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.9">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>. ;</l>
						<l n="11" num="3.3"><w n="11.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="11.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.4">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="11.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>, <w n="11.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.9">r<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="12" num="3.4"><w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.4">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="12.5">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="12.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.7">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>…</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">J</w>’<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="13.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="13.6">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="13.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.9">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="14" num="4.2"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">l</w>’<w n="14.3">h<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.5">r<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="14.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.8">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w></l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="15.2">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d</w> <w n="15.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ct</w> <w n="15.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="15.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.9">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.10">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="16" num="4.4"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="16.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>…</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.2">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="17.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="17.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="17.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="17.7">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lp<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.8">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="17.9">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="18" num="5.2"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.2">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>, <w n="18.6">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>…</l>
						<l n="19" num="5.3"><w n="19.1">J</w>’<w n="19.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="19.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="19.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.8">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="19.9">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="20" num="5.4"><w n="20.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg></w>, <w n="20.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.9">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.10">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="21.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.3">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="21.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="21.7">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="21.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.10">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="22" num="6.2"><w n="22.1">Qu</w>’<w n="22.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.3">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="22.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="22.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.6">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="22.7">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="22.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.9">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>…</l>
						<l n="23" num="6.3"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="23.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.5">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sm<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="23.6"><seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="23.7">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="23.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.9"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
						<l n="24" num="6.4"><w n="24.1">C</w>’<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="24.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="24.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="24.6">p<seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="24.7">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.9">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.10">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">D<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="25.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="25.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="25.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.8">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>-<w n="25.9"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="26" num="7.2"><w n="26.1">S</w>’<w n="26.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="26.3">l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="26.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>… <w n="26.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="26.8">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="26.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="26.10">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>…</l>
						<l n="27" num="7.3"><w n="27.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="27.2">j</w>’<w n="27.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="27.4">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="27.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.6">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="27.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.9">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="27.10">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.11">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="28" num="7.4"><w n="28.1">J</w>’<w n="28.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="28.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="28.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="28.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.7">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="28.8">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>…</l>
					</lg>
				</div></body></text></TEI>