<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HYMNES, ROMANCES ET CHANSONNETTES</head><div type="poem" key="CAO64">
					<head type="main">LA CAPRICIEUSE</head>
					<head type="tune">Musique de M. Édouard Vincelette</head>
					<div type="section" n="1">
						<head type="main">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="1.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="1.5">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="2" num="1.2"><w n="2.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="2.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="2.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.5"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
							<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="3.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="4.5">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
							<l n="5" num="1.5"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.3">j</w>’<w n="5.4"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.5">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="6" num="1.6"><w n="6.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.4">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="6.5">d</w>’<w n="6.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
							<l n="7" num="1.7"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.2">pr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="7.3">l</w>’<w n="7.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
							<l n="8" num="1.8"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="8.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">cr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> : <w n="8.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> ! <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="main">II</head>
						<lg n="1">
							<l n="9" num="1.1"><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="9.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="10" num="1.2"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="10.4">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
							<l n="11" num="1.3"><w n="11.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.2">m</w>’<w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="11.4">ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="12" num="1.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">m</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> : <w n="12.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> !</l>
							<l n="13" num="1.5"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.3">j</w>’<w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.6">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="14" num="1.6"><w n="14.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="14.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="14.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>,</l>
							<l n="15" num="1.7"><w n="15.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
							<l n="16" num="1.8"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="16.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.3">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="16.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> ! <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="main">III</head>
						<lg n="1">
							<l n="17" num="1.1"><w n="17.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="17.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="18" num="1.2"><w n="18.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.2">r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w> ;</l>
							<l n="19" num="1.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="19.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="19.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="19.5">c</w>’<w n="19.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.7">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
							<l n="20" num="1.4"><w n="20.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.2">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="20.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.5">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
							<l n="21" num="1.5"><w n="21.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="21.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> : « <w n="21.4">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="22" num="1.6"><w n="22.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="22.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, »</l>
							<l n="23" num="1.7"><w n="23.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="23.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="23.3">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="23.5">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
							<l n="24" num="1.8"><w n="24.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="24.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="24.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="24.4">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ! <del reason="analysis" hand="RR" type="repetition">(bis)</del>.</l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="main">IV</head>
						<lg n="1">
							<l n="25" num="1.1"><w n="25.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="25.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
							<l n="26" num="1.2"><w n="26.1">D<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="26.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>, <w n="26.3">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
							<l n="27" num="1.3"><w n="27.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="27.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="27.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
							<l n="28" num="1.4"><w n="28.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="28.2">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="28.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.4">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="28.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
							<l n="29" num="1.5"><w n="29.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="29.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="29.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="30" num="1.6"><w n="30.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="30.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="30.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="30.5">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> :</l>
							<l n="31" num="1.7"><w n="31.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="31.2"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="31.3">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="31.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
							<l n="32" num="1.8"><w n="32.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="32.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="32.4">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.5">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ! <del reason="analysis" hand="RR" type="repetition">(bis)</del>.</l>
						</lg>
					</div>
					<closer>
						<dateline>
							<date when="1886">20 août 1886</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>