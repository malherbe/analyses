<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Fer rouge</title>
				<title type="medium">Édition électronique</title>
				<author key="GLA">
					<name>
						<forename>Albert</forename>
						<surname>GLATIGNY</surname>
					</name>
					<date from="1839" to="1873">1839-1873</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1218 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Fer rouge</title>
						<author>Albert Glatigny</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L928</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Fer rouge</title>
								<author>Albert Glatigny</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54519653</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Poulet-Malassis</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Le formatage strophique a été rétabli.</p>
				<p>Les majuscules en début de vers ont été restituées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GLA13">
				<head type="number">XIII</head>
				<head type="main">GUITARE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">d</w>’<w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301">ai</seg>t</w> <w n="2.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
					<l n="3" num="1.3"><w n="3.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>, <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">Pr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="4.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="5.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t</w> <w n="5.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> : « <w n="5.6">l</w>’<w n="5.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">Em</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="6" num="2.2"><w n="6.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.4">N<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="7.2">l</w>’<w n="7.3">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="7.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="8" num="2.4"><w n="8.1">R<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.5">L<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>. »</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.3">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="10" num="3.2"><w n="10.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">S<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>, <w n="10.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="11" num="3.3"><w n="11.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">l</w>’<w n="11.3">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="11.6">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="12" num="3.4"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="12.2">l</w>’<w n="12.3">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>, <w n="13.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.3">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="14" num="4.2"><w n="14.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.2"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>, <w n="14.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
					<l n="15" num="4.3"><w n="15.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="15.3">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="15.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="16" num="4.4"><w n="16.1">S</w>’<w n="16.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> : « <w n="16.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.5">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> ! »</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.2">n</w>’<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="17.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="17.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="17.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="17.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.8">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="18" num="5.2"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>pt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w> <w n="18.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="18.6">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rds</w>,</l>
					<l n="19" num="5.3"><w n="19.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="19.2">qu</w>’<w n="19.3"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="20" num="5.4"><w n="20.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.5">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="21.3">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="21.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="21.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.7">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="22" num="6.2"><w n="22.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.2">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="22.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.4">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="23" num="6.3"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="23.2">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="23.3">l</w>’<w n="23.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="23.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="24" num="6.4"><w n="24.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.2">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rgu<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.3">B<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="25.2">l</w>’<w n="25.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.4"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="26" num="7.2"><w n="26.1">D<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="26.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="26.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
					<l n="27" num="7.3"><w n="27.1">S<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.2">dr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.3">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="27.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="28" num="7.4"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="28.2">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="28.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="28.5">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> :</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">« <w n="29.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="29.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="29.3">J<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="29.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.5">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="30" num="8.2"><w n="30.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="30.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="30.5">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>,</l>
					<l n="31" num="8.3"><w n="31.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="31.2">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="31.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="31.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="31.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="32" num="8.4"><w n="32.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="32.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="32.3">n</w>’<w n="32.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="32.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="32.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="32.7">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> ! »</l>
				</lg>
				<closer>
					<dateline> 7 octobre.</dateline>
				</closer>
			</div></body></text></TEI>