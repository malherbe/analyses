<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE SECOND</head><div type="poem" key="FLO25">
					<head type="number">FABLE III</head>
					<head type="main">La Brebis et le Chien</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">br<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5">ch<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>, <w n="1.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.9">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="1.10"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
						<l n="2" num="1.2"><w n="2.1">S<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="2.6">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="3.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.4">br<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="3.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.8">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.9">fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></l>
						<l n="4" num="1.4"><w n="4.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="4.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="4.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="4.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.7">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.8">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="5" num="1.5"><w n="5.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="5.2">l</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5">l</w>’<w n="5.6">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w>,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="6.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="6.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="7.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.4">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="7.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.7">z<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="8.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="9.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.7">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="10" num="1.10"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.3">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.8">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.9">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="10.10">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>ps</w>,</l>
						<l n="11" num="1.11"><w n="11.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="11.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="11.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu</w>’<w n="11.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.9">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="12" num="1.12"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="12.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.4">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>.</l>
						<l n="13" num="1.13"><w n="13.1">L<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="13.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.4">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w> <w n="13.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="13.6">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.8">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space><w n="14.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="14.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nh<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="15.3"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="15.4">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>ls</w>, <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.6">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="15.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="15.8">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="15.9">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w>,</l>
						<l n="16" num="1.16"><space unit="char" quantity="8"></space><w n="16.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="16.4">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="17" num="1.17"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="17.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>, <w n="17.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="17.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.6">ch<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> : <w n="17.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.8">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>-<w n="17.9">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.10">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="17.11">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="18" num="1.18"><space unit="char" quantity="8"></space><w n="18.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="18.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space><w n="19.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="19.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.3">s<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="19.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="19.5">v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="19.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="19.7">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space><w n="20.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="20.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="20.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>