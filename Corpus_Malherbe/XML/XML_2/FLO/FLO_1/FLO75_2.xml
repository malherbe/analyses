<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><div type="poem" key="FLO75">
					<head type="number">FABLE IX</head>
					<head type="main">La Fauvette et le Rossignol</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="1.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="2.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.6">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="2.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xtr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>sp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4">r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="3.5">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>-<w n="3.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>. <w n="4.6">L</w>’<w n="4.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.8">ch<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="4.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.10">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.11">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w></l>
						<l n="5" num="1.5"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="5.2">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="5.3">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>pr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="5.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> : <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.7">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="5.8">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.9">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> ;</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">C</w>’<w n="6.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="6.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>, <w n="6.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="7.3">g<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
						<l n="8" num="1.8"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.4"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="8.5">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="8.7">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.8">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>.</l>
						<l n="9" num="1.9"><w n="9.1">D<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.2">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.5">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.6">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="9.7">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w></l>
						<l n="10" num="1.10"><w n="10.1">F<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="10.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="10.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p</w> ; <w n="10.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.6">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.8">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="11" num="1.11"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="11.2">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>. <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.7">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="12" num="1.12"><space unit="char" quantity="8"></space><w n="12.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="12.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="12.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> ;</l>
						<l n="13" num="1.13"><space unit="char" quantity="8"></space><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="14" num="1.14"><w n="14.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="14.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.7">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="15" num="1.15"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="15.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="15.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="15.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="15.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
						<l n="16" num="1.16"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppl<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>. <w n="16.4">B<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="16.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="17" num="1.17"><space unit="char" quantity="8"></space><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="17.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3">r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="17.4">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
						<l n="18" num="1.18"><space unit="char" quantity="8"></space><w n="18.1">Tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w> <w n="18.3">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w>, <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w>, <w n="18.5">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>,</l>
						<l n="19" num="1.19"><w n="19.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space><w n="20.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="20.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.3">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> ;</l>
						<l n="21" num="1.21"><space unit="char" quantity="8"></space><w n="21.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.3">g<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="21.4">fl<seg phoneme="ɛ" type="vs" value="1" rule="355">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="22" num="1.22"><w n="22.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="22.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="22.3"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="22.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="22.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="22.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.9">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>,</l>
						<l n="23" num="1.23"><w n="23.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="23.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="23.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.4">pr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="23.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="23.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="23.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="24.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="24.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="24.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.6">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>.</l>
						<l n="25" num="1.25"><w n="25.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="25.3">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="25.4">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="25.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="25.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="26" num="1.26"><w n="26.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.2">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>, <w n="26.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.4">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="26.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.7">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
						<l n="27" num="1.27"><space unit="char" quantity="8"></space><w n="27.1">N<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="27.3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="27.4">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="27.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.6">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> :</l>
						<l n="28" num="1.28"><w n="28.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="28.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sp<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>. <w n="28.4">L</w>’<w n="28.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="28.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="29" num="1.29"><space unit="char" quantity="8"></space><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="29.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="29.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ct<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="29.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
						<l n="30" num="1.30"><w n="30.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu</w>’<w n="30.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="30.3">ge<seg phoneme="ɛ" type="vs" value="1" rule="301">ai</seg></w> <w n="30.4">s</w>’<w n="30.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> : <w n="30.6">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="30.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.9">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="31" num="1.31"><space unit="char" quantity="8"></space><w n="31.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.2">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="31.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
						<l n="32" num="1.32"><space unit="char" quantity="8"></space><w n="32.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="32.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.3">r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="32.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w></l>
						<l n="33" num="1.33"><w n="33.1">L</w>’<w n="33.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="33.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="33.5">d</w>’<w n="33.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="33.8">s</w>’<w n="33.9"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xpl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="34" num="2.1"><space unit="char" quantity="8"></space><w n="34.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="34.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ffr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.4">d</w>’<w n="34.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="34.6">s<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w></l>
						<l n="35" num="2.2"><space unit="char" quantity="8"></space><w n="35.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="35.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="35.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="35.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.7">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>