<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poussières</title>
				<title type="medium">Édition électronique</title>
				<author key="BAR">
					<name>
						<forename>Jules</forename>
						<surname>BARBEY D’AUREVILLY</surname>
					</name>
					<date from="1808" to="1889">1808-1889</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1129 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poussières</title>
						<author>Barbey d’Aurevilly</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/barbeydaurevillypoussieres.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Poussières</title>
					<author>Barbey d’Aurevilly</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonose Lemerre, Éditeur</publisher>
						<date when="1897">1897</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-01" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAR23">
				<head type="main">Un Amour De Jupe</head>
				<opener>
					<epigraph>
						<cit>
							<quote>Seulement !…</quote>
						</cit>
					</epigraph>
					<salute>À la comtesse de P…</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="1.4">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.6">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="2.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">j</w>’<w n="2.5"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="2.6">m<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.7">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> :</l>
					<l n="3" num="1.3">« <w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="3.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="3.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="4" num="1.4">« <w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.3">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.4">cr<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>. »</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ! — <w n="5.4">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>z<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="6" num="2.2"><w n="6.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1">S<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="7.3">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>…</l>
					<l n="8" num="2.4"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="8.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="8.3">j</w>’<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.5">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="8.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.7">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="9.3">c</w>’<w n="9.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="10" num="3.2"><w n="10.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>…</l>
					<l n="11" num="3.3"><w n="11.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">n</w>’<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.4">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="11.5">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="11.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.8">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="12" num="3.4"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.2">m</w>’<w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t</w> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.6"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="13.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">m</w>’<w n="13.4"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.7"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="14" num="4.2"><w n="14.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
					<l n="15" num="4.3"><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="15.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.8">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="16" num="4.4"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.3"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="16.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="16.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="16.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="16.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.8">c<seg phoneme="œ" type="vs" value="1" rule="389">oeu</seg>r</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">L</w>’<w n="17.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> ?… <w n="17.5">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.6">l</w>’<w n="17.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="18" num="5.2"><w n="18.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>… <w n="18.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="18.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="18.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
					<l n="19" num="5.3"><w n="19.1">Qu</w>’<w n="19.2">h<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="19.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.5">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="20" num="5.4"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="20.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.3">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.4">cr<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="21.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="21.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.6">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t</w> <w n="21.7">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="22" num="6.2"><w n="22.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="22.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.5">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w> <w n="22.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
					<l n="23" num="6.3"><w n="23.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="23.2">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="23.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="23.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w>, <w n="23.5">l</w>’<w n="23.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="24" num="6.4"><w n="24.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="24.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.5">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">J</w>’<w n="25.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="25.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="25.4"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="25.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="26" num="7.2">(<w n="26.1"><seg phoneme="o" type="vs" value="1" rule="435">O</seg>cc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="26.2">l</w>’<w n="26.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="26.4">c</w>’<w n="26.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="26.6">l</w>’<w n="26.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
					<l n="27" num="7.3"><w n="27.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="27.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, — <w n="27.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.5">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !),</l>
					<l n="28" num="7.4"><w n="28.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.2">l</w>’<w n="28.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="28.4"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>… <w n="28.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="28.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="29.3">d</w>’<w n="29.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="29.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="30" num="8.2"><w n="30.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="30.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="30.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.4">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="30.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sm<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>,</l>
					<l n="31" num="8.3"><w n="31.1">F<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="31.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="31.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="31.4">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="32" num="8.4"><w n="32.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.2">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="32.3">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="32.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="290">aon</seg></w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="33.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="33.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="33.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?…</l>
					<l n="34" num="9.2"><w n="34.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="34.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="34.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="34.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="34.5"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> :</l>
					<l n="35" num="9.3">« <w n="35.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="35.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="35.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.4">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="35.5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="35.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="36" num="9.4">« <w n="36.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="36.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="36.3">L<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, — <w n="36.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.5">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="36.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="36.7">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ! »</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">M<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="37.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="37.4">m</w>’<w n="37.5"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="38" num="10.2"><w n="38.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w>-<w n="38.2">t</w>-<w n="38.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="38.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="38.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="38.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="38.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> :</l>
					<l n="39" num="10.3">« <w n="39.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="39.2">l</w>’<w n="39.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="39.4">qu</w>’<w n="39.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="39.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="39.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="39.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="39.9">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="40" num="10.4">« <w n="40.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="40.3">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="40.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="40.6">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !… »</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="41.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="41.4">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="42" num="11.2"><w n="42.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="42.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="42.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="42.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="42.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="43" num="11.3"><w n="43.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="43.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="43.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="43.4">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="43.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="43.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
					<l n="44" num="11.4"><w n="44.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="44.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="44.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="44.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="44.5"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="44.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>…</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="45.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="45.3">c</w>’<w n="45.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="45.5">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="45.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="45.7">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !…</l>
					<l n="46" num="12.2"><w n="46.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="46.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="46.3">cr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="46.4"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="46.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !… <w n="46.6">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="46.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
					<l n="47" num="12.3"><w n="47.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="47.2">j</w>’<w n="47.3"><seg phoneme="y" type="vs" value="1" rule="391">eu</seg>s</w> <w n="47.4">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="47.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="47.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="47.7">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?…</l>
					<l n="48" num="12.4"><w n="48.1">R<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="48.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="48.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="48.4">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="49" num="12.5"><w n="49.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="49.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="49.3">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="49.4">cr<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> !</l>
				</lg>
			</div></body></text></TEI>