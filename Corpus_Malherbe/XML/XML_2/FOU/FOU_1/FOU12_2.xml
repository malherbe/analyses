<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA NÉGRESSE BLONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="FOU">
					<name>
						<forename>Georges</forename>
						<surname>FOUREST</surname>
					</name>
					<date from="1864" to="1945">1864-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1172 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FOU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>La Négresse blonde</title>
						<author>Georges Fourest</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/La_N%C3%A9gresse_blonde_(recueil)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Négresse blonde</title>
								<author>Georges Fourest</author>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1269960g</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A MESSEIN</publisher>
									<date when="1909">1909</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1909">1909</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PETITES ÉLÉGIES FALOTES</head><div type="poem" key="FOU12">
					<head type="main">LE VIEUX SAINT</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Non ei species neque decor.
								</quote>
								<bibl>
									<name>TERTULLIEN</name>.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="10"></space><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="10"></space><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="FOU12_2">ai</seg>t</w> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="2.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> :</l>
						<l n="3" num="1.3"><space unit="char" quantity="10"></space><w n="3.1">l</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="3.3">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.5">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="1.4"><space unit="char" quantity="10"></space><w n="4.1">t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.4">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="4.5">d</w>’<w n="4.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="5" num="1.5"><space unit="char" quantity="10"></space><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w> <w n="5.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="FOU12_2">ai</seg>t</w></l>
						<l n="6" num="1.6"><space unit="char" quantity="10"></space><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="6.3">d</w>’<w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <hi rend="ital"><w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="FOU12_1">e</seg></w></hi></l>
						<l n="7" num="1.7"><space unit="char" quantity="10"></space><w n="7.1">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="7.3">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
						<l n="8" num="1.8"><space unit="char" quantity="10"></space><w n="8.1">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="8.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="8.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="8.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="9" num="1.9"><space unit="char" quantity="10"></space><w n="9.1">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.3">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="9.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="FOU12_2">ai</seg>t</w></l>
						<l n="10" num="1.10"><space unit="char" quantity="10"></space><w n="10.1">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="10.4">l</w>’<w n="10.5"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ç<seg phoneme="e" type="vs" value="1" rule="FOU12_2">ai</seg>t</w>.</l>
						<l n="11" num="1.11"><space unit="char" quantity="10"></space><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="11.2">v<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>pr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="11.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="11.4">s</w>’<w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w></l>
						<l n="12" num="1.12"><space unit="char" quantity="10"></space><w n="12.1">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.2">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>,</l>
						<l n="13" num="1.13"><space unit="char" quantity="10"></space><w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.2">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="13.3">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>, <w n="13.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.5">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="FOU12_2">ai</seg>t</w></l>
						<l n="14" num="1.14"><space unit="char" quantity="10"></space><w n="14.1">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="14.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.5">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="FOU12_2">ai</seg>t</w></l>
						<l n="15" num="1.15"><space unit="char" quantity="10"></space><w n="15.1">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.2">l</w>’<w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="15.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
						<l n="16" num="1.16"><space unit="char" quantity="10"></space><w n="16.1">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="16.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="16.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="FOU12_2">ai</seg>t</w></l>
						<l n="17" num="1.17"><space unit="char" quantity="10"></space><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="17.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="17.3">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="17.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.5">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="17.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.7">M<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w></l>
						<l n="18" num="1.18"><space unit="char" quantity="10"></space><w n="18.1">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="18.3">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="18.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.5">M<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w></l>
						<l n="19" num="1.19"><space unit="char" quantity="10"></space><w n="19.1">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.2">l</w>’<w n="19.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="19.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
						<l n="20" num="1.20"><space unit="char" quantity="10"></space><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="20.3">N<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="175">ë</seg>l</w> <w n="20.4">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="e" type="vs" value="1" rule="FOU12_2">ai</seg>t</w></l>
						<l n="21" num="1.21"><space unit="char" quantity="10"></space><w n="21.1">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.2">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="21.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="21.4">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
						<l n="22" num="1.22"><space unit="char" quantity="10"></space><w n="22.1">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414">ë</seg>f</w>, <w n="22.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="22.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="435">o</seg>tt<seg phoneme="e" type="vs" value="1" rule="FOU12_2">ai</seg>t</w></l>
						<l n="23" num="1.23"><space unit="char" quantity="10"></space><w n="23.1">J<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="23.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="23.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>-<w n="23.5">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
						<l n="24" num="1.24"><space unit="char" quantity="10"></space><w n="24.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="24.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="24.3">l</w>’<w n="24.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.5">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="25" num="1.25"><space unit="char" quantity="10"></space><w n="25.1"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="25.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="25.4">s</w>’<w n="25.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>,</l>
						<l n="26" num="1.26"><space unit="char" quantity="10"></space><w n="26.1">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="26.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="26.3">s</w>’<w n="26.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="FOU12_2">ai</seg>t</w></l>
						<l n="27" num="1.27"><space unit="char" quantity="10"></space><w n="27.1">pr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="27.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="27.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="27.4">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w></l>
						<l n="28" num="1.28"><space unit="char" quantity="10"></space><w n="28.1">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.2">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.3">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="28.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="28.5">l</w>’<w n="28.6"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ç<seg phoneme="e" type="vs" value="1" rule="FOU12_2">ai</seg>t</w> !</l>
					</lg>
					<lg n="2">
						<l n="29" num="2.1"><space unit="char" quantity="10"></space><w n="29.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="29.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.3">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="29.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="29.5">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>,</l>
						<l n="30" num="2.2"><space unit="char" quantity="10"></space><w n="30.1">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="30.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="30.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rv<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>,</l>
						<l n="31" num="2.3"><space unit="char" quantity="10"></space><w n="31.1">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="31.2">n<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>f</w> <w n="31.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="31.4">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="32" num="2.4"><space unit="char" quantity="10"></space><w n="32.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="32.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="32.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.4">pl<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.5">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="33" num="2.5"><space unit="char" quantity="10"></space><w n="33.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="33.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="33.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="33.4">qu</w>’<w n="33.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="33.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="33.7">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></l>
						<l n="34" num="2.6"><space unit="char" quantity="10"></space><w n="34.1">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="34.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.3">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="34.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="34.6">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
						<l n="35" num="2.7"><space unit="char" quantity="10"></space><w n="35.1"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="35.2">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="35.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="35.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="35.5">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="35.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
						<l n="36" num="2.8"><space unit="char" quantity="10"></space><w n="36.1"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="36.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="36.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>-<w n="36.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="36.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="36.6">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="37" num="2.9"><space unit="char" quantity="10"></space><w n="37.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="37.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> : <hi rend="ital"><w n="37.3"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="37.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="37.5">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="37.6">l<seg phoneme="e" type="vs" value="1" rule="FOU12_2">ai</seg>d</w></hi> !</l>
						<l n="38" num="2.10"><space unit="char" quantity="10"></space><w n="38.1">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="38.2">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="38.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="38.4">d</w>’<w n="38.5"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="39" num="3.1"><space unit="char" quantity="10"></space><w n="39.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="39.2">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="39.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w>, <w n="39.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.5">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.6">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w></l>
						<l n="40" num="3.2"><space unit="char" quantity="10"></space><w n="40.1">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.4">pr<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="40.5">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
						<l n="41" num="3.3"><space unit="char" quantity="10"></space><w n="41.1">l</w>’<w n="41.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>tr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="41.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="41.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="41.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="41.6">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w></l>
						<l n="42" num="3.4"><space unit="char" quantity="10"></space><w n="42.1">s</w>’<w n="42.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="42.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="42.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="42.5">v<seg phoneme="ø" type="vs" value="1" rule="248">œu</seg>x</w>, <w n="42.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="42.7">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
						<l n="43" num="3.5"><space unit="char" quantity="10"></space><w n="43.1">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="43.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="43.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="43.4">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="43.5">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w></l>
						<l n="44" num="3.6"><space unit="char" quantity="10"></space>(<w n="44.1">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="44.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="44.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="44.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="44.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="44.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="44.7">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>)</l>
						<l n="45" num="3.7"><space unit="char" quantity="10"></space><w n="45.1">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="45.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="45.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="45.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="45.5">f<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ;</l>
						<l n="46" num="3.8"><space unit="char" quantity="10"></space><w n="46.1"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="46.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="46.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="46.4">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="46.5">c</w>’<w n="46.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="46.7">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w></l>
						<l n="47" num="3.9"><space unit="char" quantity="10"></space><w n="47.1">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="47.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="47.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="47.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="47.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="48" num="3.10"><space unit="char" quantity="10"></space><w n="48.1">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="48.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="48.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="48.4">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="48.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="48.6">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="48.7">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>…</l>
					</lg>
					<lg n="4">
						<l rhyme="none" n="49" num="4.1"><w n="49.1">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="49.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="49.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="49.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="49.5">l</w>’<w n="49.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="49.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>