<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA NÉGRESSE BLONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="FOU">
					<name>
						<forename>Georges</forename>
						<surname>FOUREST</surname>
					</name>
					<date from="1864" to="1945">1864-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1172 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FOU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>La Négresse blonde</title>
						<author>Georges Fourest</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/La_N%C3%A9gresse_blonde_(recueil)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Négresse blonde</title>
								<author>Georges Fourest</author>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1269960g</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A MESSEIN</publisher>
									<date when="1909">1909</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1909">1909</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PETITES ÉLÉGIES FALOTES</head><div type="poem" key="FOU10">
					<head type="main">SARDINES À L’HUILE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Sardines à l’huile fine sans têtes et sans arêtes.
								</quote>
								<bibl>
									(Réclames des sardiniers passim.)
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="1.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>il</w> <w n="1.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5">f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="1.6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w></l>
						<l n="2" num="1.2"><w n="2.1">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="2.2">d</w>’<w n="2.3">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="2.5">p<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w></l>
						<l n="3" num="1.3"><w n="3.1">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="3.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w></l>
						<l n="4" num="1.4"><w n="4.1">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.2">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="4.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w></l>
						<l n="5" num="1.5"><w n="5.1">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ils</w> <w n="5.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="5.3">gu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w></l>
						<l n="6" num="1.6"><w n="6.1">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="6.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="6.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p</w> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.6">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> !</l>
						<l n="7" num="1.7"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="7.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="7.3">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w></l>
						<l n="8" num="1.8"><w n="8.1">c<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.2">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">Th<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="9" num="1.9"><w n="9.1">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">br<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="10" num="1.10"><w n="10.1">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.2">M<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="10.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.4">N<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
						<l n="11" num="1.11"><w n="11.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4">f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="11.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w></l>
						<l n="12" num="1.12"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.2">l</w>’<w n="12.3">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.5">p<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w></l>
						<l n="13" num="1.13"><w n="13.1">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">t<seg phoneme="o" type="vs" value="1" rule="444">o</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w></l>
						<l n="14" num="1.14"><w n="14.1">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.2">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="14.5">cl<seg phoneme="j" type="sc" value="0" rule="270">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="271">en</seg>ts</w> !</l>
						<l n="15" num="1.15"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="15.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="15.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.5">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="16" num="1.16"><w n="16.1">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.2">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="17" num="1.17"><w n="17.1">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="17.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.3">m<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="18" num="1.18"><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="18.2">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="18.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w>-<w n="18.4">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
						<l n="19" num="1.19"><w n="19.1"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="19.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.5">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="20" num="1.20"><w n="20.1">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.4">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="21" num="1.21"><w n="21.1">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.2">M<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="21.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.4">S<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="22" num="1.22"><w n="22.1"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="22.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w> <w n="22.3">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="22.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w></l>
						<l n="23" num="1.23"><w n="23.1"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w>, <w n="23.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="23.3">l</w>’<w n="23.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="24" num="1.24"><w n="24.1">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="24.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="24.3">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="24.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w></l>
						<l n="25" num="1.25"><w n="25.1">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="25.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="25.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>,</l>
						<l n="26" num="1.26"><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="26.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="26.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="26.4">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w></l>
						<l n="27" num="1.27"><w n="27.1">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="27.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="27.4">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="27.5">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> !…</l>
					</lg>
					<lg n="2">
						<l n="28" num="2.1"><w n="28.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="28.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>, <w n="28.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="28.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w>, <w n="28.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="28.6">g<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <ref target="1" type="noteAnchor">1</ref> ,</l>
						<l n="29" num="2.2"><w n="29.1">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="29.2">pr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="29.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="29.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> !…</l>
					</lg>
					<closer>
						<note type="footnote" id="1">
							Tout ce qu’il faut pour prier. <lb></lb>
							(Note de l’Auteur.)
						</note>
					</closer>
				</div></body></text></TEI>