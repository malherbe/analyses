<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2023-06-25" who="RR" type="analyse">Étape 7 de l'analyse automatique du corpus : Traitement des rimes, des strophes et de la forme globale.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN3">
		<head type="main">CANTILÈNE DES TRAINS<lb></lb>QU’ON MANQUE</head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="1.7">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
				<l n="2" num="1.2"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="2.6">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="2.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>.</l>
			</lg>
			<lg n="2">
				<l n="3" num="2.1"><w n="3.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>, <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="3.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
				<l n="4" num="2.2"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">Em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="4.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
				<l n="5" num="2.3"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="5.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w></l>
				<l n="6" num="2.4"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="6.4"><seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="6.5">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rge<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>.</l>
			</lg>
			<lg n="3">
				<l n="7" num="3.1"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>f</w>, <w n="7.2">br<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>f</w> !</l>
				<l n="8" num="3.2"><w n="8.1">W<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rp<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>fs</w> !</l>
				<l n="9" num="3.3"><w n="9.1">C<seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="9.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pl<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>…</l>
				<l n="10" num="3.4"><w n="10.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="10.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="10.7"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="10.8">j</w>’<w n="10.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.10">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>.</l>
			</lg>
			<lg n="4">
				<l n="11" num="4.1"><w n="11.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.2">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
				<l n="12" num="4.2"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="12.3">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.5">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="12.7">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
			</lg>
			<lg n="5">
				<l n="13" num="5.1"><w n="13.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">tr<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="13.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4">tr<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="13.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6">j</w>’<w n="13.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> !</l>
				<l n="14" num="5.2"><w n="14.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.2">n</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="14.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>.</l>
				<l n="15" num="5.3">(<w n="15.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>.)</l>
			</lg>
			<lg n="6">
				<l n="16" num="6.1">— <w n="16.1">M<seg phoneme="œ" type="vs" value="1" rule="151">on</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>r</w>, <w n="16.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="16.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="16.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>str<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.7">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="16.8">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> :</l>
				<l n="17" num="6.2"><w n="17.1">C</w>’<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="17.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="17.4">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
			</lg>
			<lg n="7">
				<l n="18" num="7.1"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.2">cl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w>, <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="18.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.7">cl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
				<l n="19" num="7.2"><w n="19.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.5">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ff<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="19.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="19.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.8">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="19.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.10">r<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
				<l n="20" num="7.3"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>, <w n="20.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.3">d</w>’<w n="20.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="20.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.6">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="20.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.8">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
			</lg>
			<lg n="8">
				<l n="21" num="8.1"><w n="21.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="21.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.3">tr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="21.4">s</w>’<w n="21.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="21.6">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="21.7">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="21.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="21.9">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>scr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> :</l>
				<l n="22" num="8.2"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="22.2">l</w>’<w n="22.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="22.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="22.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="22.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>.</l>
			</lg>
	</div></body></text></TEI>