<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Le Moyen âge et la Renaissance</head><div type="poem" key="HER63">
					<head type="main">Épiphanie</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w>, <w n="1.2">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lth<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>, <w n="1.3">M<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lch<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.5">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>, <w n="1.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.7">R<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="1.8">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="2" num="1.2"><w n="2.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="2.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>fs</w> <w n="2.4">d</w>’<w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="2.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="2.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.9">d</w>’<w n="2.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w></l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="3.3">d</w>’<w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="3.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="3.7">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.9">ch<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
						<l n="4" num="1.4"><w n="4.1">S</w>’<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>, <w n="4.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="4.4">qu</w>’<w n="4.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="4.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="4.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.9">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="4.10"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="5.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="5.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="5.6">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="5.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="5.8">h<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="6.2">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="6.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="6.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="6.7">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="6.9">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="6.10">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.11">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w></l>
						<l n="7" num="2.3"><w n="7.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="7.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>-<w n="7.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="7.5">l</w>’<w n="7.6">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.9"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> ;</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="8.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="8.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="8.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="8.6">r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.8">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w> <w n="9.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">l</w>’<w n="9.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.7"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="9.8">v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.9">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="9.10">J<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ph</w>,</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="10.3">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w></l>
						<l n="11" num="3.3"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.3">l</w>’<w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.6">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">C</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="12.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.4">qu</w>’<w n="12.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="12.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.7"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>st<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="12.8">C<seg phoneme="e" type="vs" value="1" rule="272">æ</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>,</l>
						<l n="13" num="4.2"><w n="13.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="13.2">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="13.3">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.4">l</w>’<w n="13.5"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>, <w n="13.6">l</w>’<w n="13.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="13.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.10">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>rrh<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="14" num="4.3"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.2">R<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="14.3">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="14.4">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>. <w n="14.5">M<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lch<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.7">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lth<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>