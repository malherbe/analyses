<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Rome et les Barbares</head><div type="poem" key="HER49">
					<head type="main">Tranquillus</head>
					<opener>
						<epigraph>
							<cit>
								<quote>C. Plinii Secundi Epist. Lib. I, Ep. XXIV</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.4">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="1.6">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w> <w n="1.7">qu</w>’<w n="1.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.9">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.10">S<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">l</w>’<w n="2.4">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.8">T<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>,</l>
						<l n="3" num="1.3"><w n="3.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="3.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="3.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="3.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.10">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="4.3">r<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>pr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.7">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.4">qu</w>’<w n="5.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="5.6">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.9">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>, <w n="5.10">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.11"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="6.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">R<seg phoneme="ɔ" type="vs" value="1" rule="441">o</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="6.5">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="6.8">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="6.9">d</w>’<w n="6.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>,</l>
						<l n="7" num="2.3"><w n="7.1">V<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="7.4">qu</w>’<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="7.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7">c<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>p</w> <w n="7.8">m<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r</w>.</l>
						<l n="8" num="2.4"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="8.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.8">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="9.2">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="9.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.5">p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>x</w> <w n="9.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.7">c</w>’<w n="9.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.9">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w></l>
						<l n="10" num="3.2"><w n="10.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.4">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.5">N<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="10.6">Cl<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="10.7">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1">M<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">r<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.5">st<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rpr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="12.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.4">f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="12.5">d</w>’<w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.7">st<seg phoneme="i" type="vs" value="1" rule="493">y</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.10">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.11"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="13" num="4.2"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.3">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="13.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w></l>
						<l n="14" num="4.3"><w n="14.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="14.4">l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="14.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.6">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="383">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="14.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.8">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>