<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RAPSODIES</head><div type="poem" key="BOR13">
						<head type="main">Isolement</head>
						<opener>
							<salute>À GÉRARD, poète.</salute>
							<epigraph>
								<cit>
									<quote>
										Les grand’s forêts renouvelées <lb></lb>
										La solitude des vallées <lb></lb>
										Clôses d’effroy tout à l’entour !
									</quote>
									<bibl>
										<name>Ronsard</name>.
									</bibl>
								</cit>
							</epigraph>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="1.4">t<seg phoneme="o" type="vs" value="1" rule="435">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="1.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="1.7">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w> <w n="1.8">cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="2" num="1.2"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="2.4">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rb<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="2.7">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="2.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.9">l</w>’<w n="2.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
							<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>, <w n="3.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.7">s</w>’<w n="3.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="4" num="1.4"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="4.2">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.3">d</w>’<w n="4.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="4.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.8">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="4.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="5.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="5.4">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="5.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="5.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="5.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="6" num="2.2"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="6.2">g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.3">d</w>’<w n="6.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.7">s</w>’<w n="6.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="6.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.10">s</w>’<w n="6.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> ;</l>
							<l n="7" num="2.3"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.3">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w></l>
							<l n="8" num="2.4"><w n="8.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.2">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="8.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="8.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.7">l</w>’<w n="8.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> ! <w n="9.2">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ! <w n="9.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
							<l n="10" num="3.2"><w n="10.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="10.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.5">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.7">s</w>’<w n="10.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="10.9"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.10">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>.</l>
							<l n="11" num="3.3"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="11.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.8">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="12" num="3.4"><w n="12.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="12.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="12.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> !</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1">— <w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">d</w>’<w n="13.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>-<w n="13.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ?… <w n="13.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.7">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="13.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.9">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?…</l>
							<l n="14" num="4.2"><w n="14.1">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="14.2">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.4">ch<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="14.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.6">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.7">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.8"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="303">aim</seg></w></l>
							<l n="15" num="4.3"><w n="15.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="15.3">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="15.4">d</w>’<w n="15.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.7">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>. —</l>
							<l n="16" num="4.4"><w n="16.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ; <w n="16.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="16.4">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="16.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="16.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.10"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.2">n</w>’<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="17.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="17.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="17.6">th<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="17.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="17.8">f<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="17.9">qu</w>’<w n="17.10"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="17.11">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.12">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="18" num="5.2"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="18.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="18.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="18.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="18.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.8">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> :</l>
							<l n="19" num="5.3"><w n="19.1">C</w>’<w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="19.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>ps</w>, <w n="19.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="19.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.7">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="19.8">gr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="19.10">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.11">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="20" num="5.4"><w n="20.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="20.2">W<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rth<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="20.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.8">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.9">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.2">n</w>’<w n="21.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="21.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="21.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.6">br<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="21.8">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="21.9">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w>, <w n="21.10">l</w>’<w n="21.11"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="21.12">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
							<l n="22" num="6.2"><w n="22.1">C</w>’<w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="22.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="22.4">c<seg phoneme="i" type="vs" value="1" rule="493">y</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> ; <w n="22.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="22.9"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="22.10">bl<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w></l>
							<l n="23" num="6.3"><w n="23.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="23.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="23.3">qu</w>’<w n="23.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="23.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.7">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>, <w n="23.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ;</l>
							<l n="24" num="6.4"><w n="24.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="24.2">qu</w>’<w n="24.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="24.4">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>fl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="24.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.6">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.7">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1"><w n="25.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="25.2">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.4">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ? — <w n="25.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="25.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="25.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.8">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="25.9">l</w>’<w n="25.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
							<l n="26" num="7.2"><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="26.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="26.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="26.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="26.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
							<l n="27" num="7.3"><w n="27.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="27.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="27.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="27.4">c<seg phoneme="i" type="vs" value="1" rule="493">y</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="27.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.6">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="27.7">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="27.8">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
							<l n="28" num="7.4"><w n="28.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="28.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.3">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="28.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>-<w n="28.6">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ?</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1"><w n="29.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="29.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.3">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="29.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.5">m<seg phoneme="wa" type="vs" value="1" rule="421">oi</seg>n<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="29.6">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="29.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="29.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.9">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
							<l n="30" num="8.2"><w n="30.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="30.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="30.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.6">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="30.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>.</l>
							<l n="31" num="8.3"><w n="31.1">S<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w>, <w n="31.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="31.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="31.4">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="31.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="i" type="vs" value="1" rule="491">i</seg>f</w>, <w n="31.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.7">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="31.8"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="32" num="8.4"><w n="32.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="32.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.3">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="32.4">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>gu<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="32.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="32.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="32.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.9">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>.</l>
						</lg>
					</div></body></text></TEI>