<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RAPSODIES</head><div type="poem" key="BOR14">
						<head type="main">La Fille du Baron</head>
						<opener>
							<salute>À THÉOPHILE GAUTIER, poète.</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ! <w n="1.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="1.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="1.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> ;</l>
							<l n="2" num="1.2"><w n="2.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.2">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> !</l>
						</lg>
						<lg n="2">
							<l n="3" num="2.1"><w n="3.1">S<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="3.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
							<l n="4" num="2.2"><w n="4.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.2">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.3">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.6">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> ;</l>
							<l n="5" num="2.3"><w n="5.1">V<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>-<w n="5.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.3">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
							<l n="6" num="2.4"><w n="6.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="6.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.4">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.6">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="6.7"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> !</l>
						</lg>
						<lg n="3">
							<l n="7" num="3.1"><w n="7.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ! <w n="7.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="7.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="7.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> ;</l>
							<l n="8" num="3.2"><w n="8.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.2">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> !</l>
						</lg>
						<lg n="4">
							<l n="9" num="4.1"><w n="9.1">P<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>-<w n="9.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.3">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="9.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
							<l n="10" num="4.2"><w n="10.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.5">d</w>’<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>lt</w>,</l>
							<l n="11" num="4.3"><w n="11.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c</w> <w n="11.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="11.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.6">tr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.7">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="12" num="4.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.5">r<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="12.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.7">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w>.</l>
						</lg>
						<lg n="5">
							<l n="13" num="5.1"><w n="13.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ! <w n="13.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="13.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="13.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> ;</l>
							<l n="14" num="5.2"><w n="14.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.2">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> !</l>
						</lg>
						<lg n="6">
							<l n="15" num="6.1"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.3">tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="15.4">ch<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="15.6">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="16" num="6.2"><w n="16.1">D<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="16.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4">Rh<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.5">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
							<l n="17" num="6.3"><w n="17.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.2">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>z<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="18" num="6.4"><w n="18.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.2">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="18.3">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.4">pr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="18.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w>.</l>
						</lg>
						<lg n="7">
							<l n="19" num="7.1"><w n="19.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ! <w n="19.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="19.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="19.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> ;</l>
							<l n="20" num="7.2"><w n="20.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.2">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="20.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> !</l>
						</lg>
						<lg n="8">
							<l n="21" num="8.1"><w n="21.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="21.2">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="21.5">h<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="22" num="8.2"><w n="22.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> ! <w n="22.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="22.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="22.7">r<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
							<l n="23" num="8.3"><w n="23.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="23.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="23.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.5">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="24" num="8.4"><w n="24.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="24.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>fr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
						</lg>
						<lg n="9">
							<l n="25" num="9.1"><w n="25.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ! <w n="25.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="25.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="25.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> ;</l>
							<l n="26" num="9.2"><w n="26.1">M<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="26.2">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="26.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="26.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> !</l>
						</lg>
						<lg n="10">
							<l n="27" num="10.1"><w n="27.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="27.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="27.3">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.4">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="27.5">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="28" num="10.2"><w n="28.1">D</w>’<w n="28.2"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>lt</w>, <w n="28.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.4">v<seg phoneme="ø" type="vs" value="1" rule="248">œu</seg>x</w> !… <w n="28.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="28.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
							<l n="29" num="10.3"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>c<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> ! <w n="29.2">qu</w>’<w n="29.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="30" num="10.4"><w n="30.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="30.2"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="30.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> !</l>
						</lg>
						<lg n="11">
							<l n="31" num="11.1"><w n="31.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ! <w n="31.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="31.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="31.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="31.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> ;</l>
							<l n="32" num="11.2"><w n="32.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="32.2">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="32.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="32.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="32.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> !</l>
						</lg>
					</div></body></text></TEI>