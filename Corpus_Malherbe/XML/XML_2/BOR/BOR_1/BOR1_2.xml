<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RAPSODIES</head><div type="poem" key="BOR1">
						<head type="main">Prologue</head>
						<opener>
							<salute>À Léon Clopet, architecte.</salute>
							<epigraph>
								<cit>
									<quote>
									« Voici, je m’en vais faire une chose nouvelle <lb></lb>
									qui viendra en avant ; et les bêtes des champs, <lb></lb>
									les dragons et les chats-huants me glorifieront.»
									</quote>
									<bibl>
										<name>La Bible</name>.
									</bibl>
								</cit>
							</epigraph>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="1.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.3">P<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>tr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="1.4"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="1.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.6">P<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="2" num="1.2"><w n="2.1">N</w>’<w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="2.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="3" num="1.3"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.2">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="3.4">l</w>’<w n="3.5"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="3.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
							<l n="4" num="1.4"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="4.2">cl<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="4.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.5">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="5" num="1.5"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.4">gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="6" num="1.6"><w n="6.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1"><w n="7.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> : — <w n="7.4">V<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="7.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.6">rh<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ps<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="8" num="2.2"><w n="8.1">V<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="8.2">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="8.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="8.4">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="8.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.6"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
							<l n="9" num="2.3"><w n="9.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="9.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.3">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="9.4">n</w>’<w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="9.7">d</w>’<w n="9.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>,</l>
							<l n="10" num="2.4"><w n="10.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="10.5">d</w>’<w n="10.6">H<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="11" num="2.5"><w n="11.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="11.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.3">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="11.4">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
							<l n="12" num="2.6"><w n="12.1">L</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="12.4">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d</w>, <w n="12.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6">s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="12.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="12.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>.</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1"><w n="13.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="13.2">n</w>’<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="13.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="14" num="3.2"><w n="14.1">V<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="14.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w>, <w n="14.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">t</w>’<w n="14.5"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="15" num="3.3"><w n="15.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w>, <w n="15.2">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="15.3">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="15.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ;</l>
							<l n="16" num="3.4"><w n="16.1">V<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="16.2">l</w>’<w n="16.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="16.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.5">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="17" num="3.5"><w n="17.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="18" num="3.6"><w n="18.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="18.2">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="18.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
						</lg>
						<lg n="4">
							<l n="19" num="4.1"><w n="19.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="19.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="19.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.5">h<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="20" num="4.2"><w n="20.1">B<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="20.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="20.4">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="21" num="4.3"><w n="21.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="21.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ;</l>
							<l n="22" num="4.4"><w n="22.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="22.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="22.3">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w>, <w n="22.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="22.5">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="22.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="23" num="4.5"><w n="23.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="23.2">m</w>’<w n="23.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="23.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="24" num="4.6"><w n="24.1">L<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="24.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="24.3">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="24.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.5">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
						</lg>
						<lg n="5">
							<l n="25" num="5.1"><w n="25.1">Qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> ! <w n="25.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.3">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.4">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.5">bl<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
							<l n="26" num="5.2"><w n="26.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="26.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="26.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="26.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="26.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="27" num="5.3"><w n="27.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="27.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t</w> <w n="27.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.4">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ?</l>
							<l n="28" num="5.4"><w n="28.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="28.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="28.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="28.4">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lf<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="29" num="5.5"><w n="29.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.2">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="29.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="29.4">s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>cl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="30" num="5.6"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="30.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.3">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
						</lg>
						<lg n="6">
							<l n="31" num="6.1"><w n="31.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="31.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="31.5">qu</w>’<w n="31.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="31.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="32" num="6.2"><w n="32.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.4">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="32.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="32.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="32.7">l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="33" num="6.3"><w n="33.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="33.2">j</w>’<w n="33.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="33.4">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="33.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rts</w> <w n="33.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.7">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
							<l n="34" num="6.4"><w n="34.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="34.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.3">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="34.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
							<l n="35" num="6.5"><w n="35.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>, <w n="35.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="35.3">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="35.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="36" num="6.6"><w n="36.1">N</w>’<w n="36.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="36.3">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="36.4">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="36.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="36.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
						</lg>
						<lg n="7">
							<l n="37" num="7.1"><w n="37.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="37.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="37.5">qu</w>’<w n="37.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="37.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="38" num="7.2"><w n="38.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="38.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="38.3">n</w>’<w n="38.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="38.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="38.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="38.7">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>st<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="39" num="7.3"><w n="39.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="39.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="39.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="39.4">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="39.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="39.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>,</l>
							<l n="40" num="7.4"><w n="40.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="40.2">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="40.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="40.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
							<l n="41" num="7.5"><w n="41.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="41.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="41.4"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="42" num="7.6"><w n="42.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="42.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="42.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rg<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="42.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
						</lg>
						<lg n="8">
							<l n="43" num="8.1"><w n="43.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="43.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="43.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="43.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="43.5">qu</w>’<w n="43.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="43.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="44" num="8.2"><w n="44.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="44.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="44.3">t<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="44.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="44.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="44.6">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="45" num="8.3"><w n="45.1">N<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="45.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="45.3">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="45.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
							<l n="46" num="8.4"><w n="46.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="46.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="46.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="46.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="46.5">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>lh<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="47" num="8.5"><w n="47.1">N<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="47.2">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="47.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="47.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="47.5">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="48" num="8.6"><w n="48.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="48.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="48.3">B<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
						</lg>
						<lg n="9">
							<l n="49" num="9.1"><w n="49.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="49.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="49.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="49.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="49.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="49.6"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
							<l n="50" num="9.2"><w n="50.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="50.2">n</w>’<w n="50.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="50.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="50.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="50.6">d</w>’<w n="50.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
							<l n="51" num="9.3"><w n="51.1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="51.2">d</w>’<w n="51.3">h<seg phoneme="i" type="vs" value="1" rule="493">y</seg>mn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="51.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="51.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="51.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
							<l n="52" num="9.4"><w n="52.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="52.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="52.3">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="52.4">d</w>’<w n="52.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="52.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="53" num="9.5"><w n="53.1">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rb<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="53.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="53.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="53.4">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="54" num="9.6"><w n="54.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="54.2">l<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="54.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="54.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="54.5">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
						</lg>
					</div></body></text></TEI>