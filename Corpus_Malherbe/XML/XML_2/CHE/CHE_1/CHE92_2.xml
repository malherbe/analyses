<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CHE92">
					<head type="number">V</head>
					<lg n="1">
						<l n="1" num="1.1"><space quantity="8" unit="char"></space><w n="1.1">V<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.2">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>j<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="1.3">d</w>’<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="2" num="1.2"><space quantity="8" unit="char"></space><w n="2.1">L</w>’<w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.4">n<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="3" num="1.3"><space quantity="8" unit="char"></space><w n="3.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="3.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="1.4"><space quantity="8" unit="char"></space><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w> <w n="4.2">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="4.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="4.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="5.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.9">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1"><space quantity="8" unit="char"></space><w n="6.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">Gr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="7" num="2.2"><space quantity="8" unit="char"></space><w n="7.1">S<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="7.4">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>,</l>
						<l n="8" num="2.3"><space quantity="8" unit="char"></space><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.2">f<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.4">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="9" num="2.4"><space quantity="8" unit="char"></space><w n="9.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.5">br<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.6">P<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="10" num="2.5"><w n="10.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.3">N<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="10.4">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.5">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>v<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="10.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="10.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>.</l>
					</lg>
				</div></body></text></TEI>