<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE SANG DE LA COUPE</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3154 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE SANG DE LA COUPE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesangdelacoupe.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Le sang de la Coupe, Trente-six Ballades joyeuses, Le Baiser.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1857">1857</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN347">
				<head type="main">La Lyre</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.2">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="1.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="1.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="1.8">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="1.9">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.10">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>.</l>
					<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.5">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="2.6">v<seg phoneme="e" type="vs" value="1" rule="383">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="2.7"><seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.10"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.3">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.5">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="3.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">L</w>’<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.4">j<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.6">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.7">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.9">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>,</l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">v<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="6.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.7">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.3">dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="7.6">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8">t<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.2">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.6">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="8.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.8">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.9">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="9.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.7">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="10" num="3.2"><w n="10.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="10.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.5"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.7">j</w>’<w n="10.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.10">r<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="11" num="3.3"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="11.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.7">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="11.8">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="12.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">m</w>’<w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>, <w n="12.5">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="12.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="13" num="4.2"><w n="13.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="13.2">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.6">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.9">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="14" num="4.3"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> : <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="14.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ; <w n="14.7">j</w>’<w n="14.8"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="14.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.10">L<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1847">Octobre 1847.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>