<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN697">
				<head type="main">Rue Lobineau</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.7">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w>-<w n="1.8">G<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
					<l n="2" num="1.2"><w n="2.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="2.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.5">n</w>’<w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="2.8">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="2.9">d</w>’<w n="2.10">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="3.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.4">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.5">pl<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="3.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="3.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="3.9">fl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="4" num="1.4"><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="4.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="4.7">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="5" num="1.5"><w n="5.1">V<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.4">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>, <w n="5.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">h<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="5.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8">g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ctr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="6.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="6.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="6.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w></l>
					<l n="7" num="1.7"><w n="7.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="7.3">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="7.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="7.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7">r<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="7.8">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="8" num="1.8"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.4">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="8.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="8.7">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="8.8">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.10">b<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.11"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.12">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.13">cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="9" num="1.9"><w n="9.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> ; <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.8">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
					<l n="10" num="1.10"><w n="10.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ; <w n="10.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="10.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="10.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.7">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.8">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="10.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.10">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ;</l>
					<l n="11" num="1.11"><w n="11.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">f<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="11.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="11.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
					<l n="12" num="1.12"><w n="12.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.2">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="12.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.7">gu<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
					<l n="13" num="1.13"><w n="13.1"><seg phoneme="w" type="sc" value="0" rule="431">Ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="13.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="13.3">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, — <w n="13.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="13.7">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="13.8">d</w>’<w n="13.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w></l>
					<l n="14" num="1.14"><w n="14.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.3">L<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="14.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.5">s<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="14.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="14.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t</w> !</l>
					<l n="15" num="1.15"><w n="15.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="15.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.4">j<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="15.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.8">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="16" num="1.16"><w n="16.1">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! <w n="16.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="16.3">L<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="476">ï</seg>s</w> ! <w n="16.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.5">Phr<seg phoneme="i" type="vs" value="1" rule="497">y</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="16.6">Cl<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="17" num="1.17"><w n="17.1"><seg phoneme="ɛ" type="vs" value="1" rule="410">È</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! <w n="17.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="17.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="17.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.5">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="17.6">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="17.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.9">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
					<l n="18" num="1.18"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="18.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="18.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="18.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="19" num="1.19"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="19.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.3">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="19.4">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="19.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="19.6">l</w>’<w n="19.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>th<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="20" num="1.20"><w n="20.1">C<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="20.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="20.3">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="20.5">d</w>’<w n="20.6">h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="20.7">c</w>’<w n="20.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.9"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.10">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
				</lg>
				<closer>
					<dateline>
						<date when="1887">Mardi, 11 janvier 1887.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>