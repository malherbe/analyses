<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN751">
				<head type="main">A Madame Léon Daudet</head>
				<head type="sub_1">le jour de son mariage</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.4">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="1.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.7">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.4">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ls</w> <w n="2.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="2.7">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.9">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="3.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.8">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.3">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="4.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="4.7">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="4.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="4.9">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="5.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>,</l>
					<l n="6" num="2.2"><w n="6.1">V<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="6.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.4">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.7">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.8">d</w>’<w n="6.9"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="7" num="2.3"><w n="7.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="7.2">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="7.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> <w n="7.4">qu</w>’<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="7.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="7.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.10"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w></l>
					<l n="8" num="2.4"><w n="8.1">Br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="8.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.9">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">T<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.3">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="9.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w> <w n="9.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="?" type="va" value="1" rule="162">en</seg>t</w>,</l>
					<l n="10" num="3.2"><w n="10.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.5">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.6">r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>, <w n="10.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> <w n="10.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.9">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="11" num="3.3"><w n="11.1">L<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="11.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="11.3">d</w>’<w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.5">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="450">u</seg>str<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.8">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.9">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
					<l n="12" num="3.4"><w n="12.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.3">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="12.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.7">Sc<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="360">en</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="13.2">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="13.3">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="13.4">H<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>, <w n="13.5">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="13.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.8">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>,</l>
					<l n="14" num="4.2"><w n="14.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>-<w n="14.3">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="14.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.7"><seg phoneme="ɔ" type="vs" value="1" rule="439">O</seg>rph<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="15" num="4.3"><w n="15.1">C<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="15.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.8">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w></l>
					<l n="16" num="4.4"><w n="16.1">Tr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="a" type="vs" value="1" rule="365">e</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="16.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.4">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="16.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="16.7">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="17.4">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="17.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.7">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="17.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w>,</l>
					<l n="18" num="5.2"><w n="18.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="18.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="18.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.7">s</w>’<w n="18.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="19" num="5.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="19.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w>, <w n="19.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="19.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="19.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.8">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>,</l>
					<l n="20" num="5.4"><w n="20.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="20.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="20.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg></w> <w n="20.6">d</w>’<w n="20.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="21.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="21.3">qu</w>’<w n="21.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="21.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="21.6">qu</w>’<w n="21.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="21.8">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="21.9">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
					<l n="22" num="6.2"><w n="22.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.2">v<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="22.3">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="22.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="22.5">l</w>’<w n="22.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="22.7">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pl<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="23" num="6.3"><w n="23.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="23.2">qu</w>’<w n="23.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="23.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="23.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="23.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.9">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="23.10">d</w>’<w n="23.11"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
					<l n="24" num="6.4"><w n="24.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="24.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="24.4">v<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="24.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.6">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="24.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.9">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">S<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="25.2">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="25.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="25.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="25.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
					<l n="26" num="7.2"><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pp<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="26.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.3">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.4">J<seg phoneme="a" type="vs" value="1" rule="310">ea</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! <w n="26.5">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.7">H<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="27" num="7.3"><w n="27.1">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="27.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.4">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="27.5">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="27.6">d</w>’<w n="27.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="27.8">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="27.9">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
					<l n="28" num="7.4"><w n="28.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="28.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.4">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="28.6">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="28.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="28.8">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="29.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="29.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="29.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="29.5">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s</w> <w n="29.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="29.7">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.8"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="29.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>,</l>
					<l n="30" num="8.2"><w n="30.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="30.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="30.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="30.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="30.5">n<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="30.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="30.8">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="31" num="8.3"><w n="31.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="31.2">tr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>ph<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="31.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="31.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="31.5">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.6">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>x<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="i" type="vs" value="1" rule="476">ï</seg></w></l>
					<l n="32" num="8.4"><w n="32.1">D</w>’<w n="32.2"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.3">b<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, — <w n="32.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="32.5">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.6">v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="32.7">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="32.8">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.9">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="32.10">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.11">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
				</lg>
			</div></body></text></TEI>