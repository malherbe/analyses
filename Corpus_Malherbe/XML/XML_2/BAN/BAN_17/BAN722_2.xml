<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN722">
				<head type="main">La Promenade</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="w" type="sc" value="0" rule="431">Ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6">R<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="3.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">P<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w>, <w n="4.2"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.4">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="5.4">B<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
					<l n="6" num="2.2"><w n="6.1">R<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="6.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">fr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.3">m<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="7.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> ;</l>
					<l n="8" num="2.4"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="8.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="8.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">R<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="9.2">n</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="9.5">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w></l>
					<l n="10" num="3.2"><w n="10.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">Gr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="11.2">l</w>’<w n="11.3">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="11.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w></l>
					<l n="12" num="3.4"><w n="12.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w> <w n="12.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.4">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.6">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.2">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="13.6">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sm<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Gr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="14.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rb<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="14.4">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">Em</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="ə" type="vi" value="1" rule="242">e</seg>nt</w> <w n="15.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="15.3">l</w>’<w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="15.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.6">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>,</l>
					<l n="16" num="4.4"><w n="16.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="16.2">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3">l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="16.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.2">z<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r</w> <w n="17.3">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="17.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>,</l>
					<l n="18" num="5.2"><w n="18.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="18.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.3">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="19" num="5.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="19.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="19.4">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="19.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.6">G<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>,</l>
					<l n="20" num="5.4"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="20.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.4">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="20.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="21.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="21.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="21.4">l</w>’<w n="21.5"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> ;</l>
					<l n="22" num="6.2"><w n="22.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.2">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>, <w n="22.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="22.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> <w n="22.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="23" num="6.3"><w n="23.1">V<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="23.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.4">s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.5">N<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w></l>
					<l n="24" num="6.4"><w n="24.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="24.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rp<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> <w n="24.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="24.5">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">L</w>’<w n="25.2"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="25.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="25.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.6">ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
					<l n="26" num="7.2"><w n="26.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="26.3">r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="26.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="26.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="26.6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="27" num="7.3"><w n="27.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>, <w n="27.2">d</w>’<w n="27.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="27.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="27.5">l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="27.7">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
					<l n="28" num="7.4"><w n="28.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.2">d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="28.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="28.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.5">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="28.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="29.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="29.3">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="29.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="29.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="29.6">d</w>’<w n="29.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="a" type="vs" value="1" rule="307">a</seg>il</w>,</l>
					<l n="30" num="8.2"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="30.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="30.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="30.4">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="30.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="30.6">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="31" num="8.3"><w n="31.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="31.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="31.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="31.4">l</w>’<w n="31.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>il</w></l>
					<l n="32" num="8.4"><w n="32.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.2">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="32.3">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="32.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="32.6">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.3">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="33.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="33.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="33.6">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> !</l>
					<l n="34" num="9.2"><w n="34.1">Gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="34.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="34.5">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.6">d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="35" num="9.3"><w n="35.1">D<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>, <w n="35.2">T<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="35.3">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>,</l>
					<l n="36" num="9.4"><w n="36.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> <w n="36.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="36.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="370">e</seg>n</w>, <w n="37.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="37.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="37.5">qu<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
					<l n="38" num="10.2"><w n="38.1">B<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="38.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="38.3">r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="38.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="38.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="38.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="39" num="10.3"><w n="39.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="39.2">J<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="39.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="39.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="39.5">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="39.7">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
					<l n="40" num="10.4"><w n="40.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w> <w n="40.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.3">gr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1879">Paris, décembre 1879.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>