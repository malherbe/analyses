<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">AMÉTHYSTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>319 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">AMÉTHYSTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleamethystes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1860-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN190">
				<head type="main">Le Charme de la voix</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="1.2">s</w>’<w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="1.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="1.5">str<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ph<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="1.6">d</w>’<w n="1.7"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="2.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="2.4"><seg phoneme="ɔ" type="vs" value="1" rule="443">O</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.5">qu</w>’<w n="2.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">l</w>’<w n="3.6"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
					<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>str<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="4.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="5" num="1.5"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="5.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.4">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="5.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.8">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="6" num="1.6"><w n="6.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.3">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">t</w>’<w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.7">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.8">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w></l>
					<l n="7" num="1.7"><w n="7.1">Qu</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">n</w>’<w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="7.6">b<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="7.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="8" num="1.8"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="8.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.2">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>,</l>
					<l n="10" num="2.2"><w n="10.1">P<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.4">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="10.5">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="11" num="2.3"><w n="11.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="11.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w></l>
					<l n="12" num="2.4"><w n="12.1">Br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="12.5">d</w>’<w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="13" num="2.5"><w n="13.1">T<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.4">rh<seg phoneme="i" type="vs" value="1" rule="493">y</seg>thm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5">s<seg phoneme="i" type="vs" value="1" rule="497">y</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="14" num="2.6"><w n="14.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="14.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.5">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="15" num="2.7"><w n="15.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pph<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>, <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.4">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="16" num="2.8"><w n="16.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="16.3">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="16.4">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1861">Février 1861.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>