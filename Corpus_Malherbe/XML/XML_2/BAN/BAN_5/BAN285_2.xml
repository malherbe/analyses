<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN285">
				<head type="number">XCIII</head>
				<head type="main">Adieu</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="w" type="sc" value="0" rule="431">Ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="1.2">j</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="1.4">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="2" num="1.2"><space quantity="8" unit="char"></space><w n="2.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="2.3">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.3">j</w>’<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="3.6">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="3.7">r<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="4" num="1.4"><space quantity="8" unit="char"></space><w n="4.1">J<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="4.2">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.6">S<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="6" num="2.2"><space quantity="8" unit="char"></space><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.4">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w></l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>pr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="8" num="2.4"><space quantity="8" unit="char"></space><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.2">qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="8.3">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w> <w n="9.4">g<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="10" num="3.2"><space quantity="8" unit="char"></space><w n="10.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="10.3">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="11.2">r<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.4">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="12" num="3.4"><space quantity="8" unit="char"></space><w n="12.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="12.3">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">J</w>’<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.4">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="14" num="4.2"><space quantity="8" unit="char"></space><w n="14.1">R<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="14.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.3">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t</w>,</l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>-<w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="15.6">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="16" num="4.4"><space quantity="8" unit="char"></space><w n="16.1">N</w>’<w n="16.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="16.3">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="16.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">J</w>’<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.4">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="17.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.7">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="18" num="5.2"><space quantity="8" unit="char"></space><w n="18.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.2">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w> <w n="18.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
					<l n="19" num="5.3"><w n="19.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.2">th<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="19.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="20" num="5.4"><space quantity="8" unit="char"></space><w n="20.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="20.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="21.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.5">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="22" num="6.2"><space quantity="8" unit="char"></space><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
					<l n="23" num="6.3"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.3">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.4">gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="24" num="6.4"><space quantity="8" unit="char"></space><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="24.2">l</w>’<w n="24.3"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="25.2">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="25.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="25.4">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="25.5">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.6">s</w>’<w n="25.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="26" num="7.2"><space quantity="8" unit="char"></space><w n="26.1">R<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="26.2">n</w>’<w n="26.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="26.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> ;</l>
					<l n="27" num="7.3"><w n="27.1">J</w>’<w n="27.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="27.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.5">pl<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="28" num="7.4"><space quantity="8" unit="char"></space><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="28.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> ;</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="29.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="29.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="29.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="29.6">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="29.7">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="30" num="8.2"><space quantity="8" unit="char"></space><w n="30.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="30.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.3">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w></l>
					<l n="31" num="8.3"><w n="31.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="31.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="31.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="31.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.6">m</w>’<w n="31.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="360">en</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="32" num="8.4"><space quantity="8" unit="char"></space><w n="32.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="32.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> ;</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="33.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="33.3">m</w>’<w n="33.4"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="33.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.6">W<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rmsp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="34" num="9.2"><space quantity="8" unit="char"></space><w n="34.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="34.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.3">G<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>,</l>
					<l n="35" num="9.3"><w n="35.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="35.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="35.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.6">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="35.7">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="36" num="9.4"><space quantity="8" unit="char"></space><w n="36.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="36.2">H<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="37.2">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="37.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="37.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="37.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="38" num="10.2"><space quantity="8" unit="char"></space><w n="38.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="38.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="38.3">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w></l>
					<l n="39" num="10.3"><w n="39.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="39.2">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="39.3">t</w>’<w n="39.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r</w>, <w n="39.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="39.7">bl<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="40" num="10.4"><space quantity="8" unit="char"></space><w n="40.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="40.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rm<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> ;</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="41.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="41.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.4">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="41.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="42" num="11.2"><space quantity="8" unit="char"></space><w n="42.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="42.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="42.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="42.4">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>,</l>
					<l n="43" num="11.3"><w n="43.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="43.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="43.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="43.4"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="43.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="43.6">gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="44" num="11.4"><space quantity="8" unit="char"></space><w n="44.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="44.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="44.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> ;</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="45.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="45.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="45.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="45.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="45.6">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="46" num="12.2"><space quantity="8" unit="char"></space><w n="46.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="46.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="46.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ils</w>,</l>
					<l n="47" num="12.3"><w n="47.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="47.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="47.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="47.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="47.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="47.6">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="48" num="12.4"><space quantity="8" unit="char"></space><w n="48.1">C<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="48.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="48.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ils</w> ;</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="49.2">f<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="49.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="49.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="49.5">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="50" num="13.2"><space quantity="8" unit="char"></space><w n="50.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="50.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="50.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
					<l n="51" num="13.3"><w n="51.1">T<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="51.2">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="51.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="51.4">l</w>’<w n="51.5"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="51.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="51.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="51.8">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="52" num="13.4"><space quantity="8" unit="char"></space><w n="52.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="52.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="52.3"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">12 mars 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>