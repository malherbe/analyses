<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN203">
				<head type="number">XI</head>
				<head type="main">Fillette</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="1.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">P<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w></l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">L<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="2.5">l</w>’<w n="2.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="3.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w></l>
					<l n="4" num="1.4"><space quantity="16" unit="char"></space><w n="4.1">L<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>str<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.5">s</w>’<w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w></l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="6.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5">qu<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="7.2">s</w>’<w n="7.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.6">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w></l>
					<l n="8" num="2.4"><space quantity="16" unit="char"></space><w n="8.1">Bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>, <w n="9.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">d<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
					<l n="10" num="3.2"><w n="10.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="10.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.4">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="11.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w></l>
					<l n="12" num="3.4"><space quantity="16" unit="char"></space><w n="12.1">L<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="13.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
					<l n="14" num="4.2"><w n="14.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ff<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.4">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
					<l n="16" num="4.4"><space quantity="16" unit="char"></space><w n="16.1">N<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="17.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
					<l n="18" num="5.2"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="18.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w> <w n="18.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.5">c<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="19" num="5.3"><w n="19.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="19.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.5">ch<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="19.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w></l>
					<l n="20" num="5.4"><space quantity="16" unit="char"></space><w n="20.1">N<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="21.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.3"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="21.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="22" num="6.2"><w n="22.1">M<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="22.2">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="23" num="6.3"><w n="23.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="23.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="23.5"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
					<l n="24" num="6.4"><space quantity="16" unit="char"></space><w n="24.1">Tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">7 décembre 1883.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>