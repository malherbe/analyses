<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN194">
				<head type="number">II</head>
				<head type="main">Lili</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">L<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.5">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="3.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="4.4">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="4.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.7">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w></l>
					<l n="6" num="2.2"><w n="6.1">S</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="6.6">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sp<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.3">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="7.6"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="7.7">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
					<l n="8" num="2.4"><w n="8.1">C</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.3">qu</w>’<w n="8.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.6">t<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="8.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="w" type="sc" value="0" rule="431">Ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="9.2">P<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="9.3">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l</w>, <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
					<l n="10" num="3.2"><w n="10.1">H<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.4">Z<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="11" num="3.3"><w n="11.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.3">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.5">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w></l>
					<l n="12" num="3.4"><w n="12.1">H<seg phoneme="o" type="vs" value="1" rule="435">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="12.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="12.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.4">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.3">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.6">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ;</l>
					<l n="14" num="4.2"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">c</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.4">v<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="14.5">qu</w>’<w n="14.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="15" num="4.3"><w n="15.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="15.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="15.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="15.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
					<l n="16" num="4.4"><w n="16.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.3">bl<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="17.3">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="17.6">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ;</l>
					<l n="18" num="5.2"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="18.2">c</w>’<w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="18.4">v<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="18.5">qu</w>’<w n="18.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="19" num="5.3"><w n="19.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="19.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="19.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="19.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
					<l n="20" num="5.4"><w n="20.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="20.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.3">bl<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">1er décembre 1883.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>