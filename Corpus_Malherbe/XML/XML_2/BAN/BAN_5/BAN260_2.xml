<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN260">
				<head type="number">LXVIII</head>
				<head type="main">Les Cartes</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="1.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="1.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5">R<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.6">D<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="2.2">s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>cl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.4">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="2.5">s<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="3.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="3.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.5">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="3.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="4" num="1.4"><w n="4.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="4.2">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="a" type="vs" value="1" rule="365">e</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="4.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>pç<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w>-<w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.4">n</w>’<w n="5.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="5.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.7">h<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> :</l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="6.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.7"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
					<l n="7" num="2.3"><w n="7.1">F<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="7.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="7.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.5">n</w>’<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="8" num="2.4"><w n="8.1">B<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.2">qu</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.4">d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.6">g<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">D<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d</w>, <w n="9.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.4">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.5">l</w>’<w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="355">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="10.2">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="10.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="10.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>,</l>
					<l n="11" num="3.3"><w n="11.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="11.3">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="11.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="12" num="3.4"><w n="12.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>qu<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.4">C<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.2">R<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="13.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.4">gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="14" num="4.2"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="353">E</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="14.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="14.4">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg>t</w> <w n="15.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="15.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="16" num="4.4"><w n="16.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="16.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rp<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>ts</w> <w n="16.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">J<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>th</w>, <w n="17.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="17.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="17.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="18" num="5.2"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="18.2">l</w>’<w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="18.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="18.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.6"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="18.7">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w></l>
					<l n="19" num="5.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="19.2">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="19.4">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="19.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="20" num="5.4"><w n="20.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.3">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="21.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="21.3">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="21.5">d</w>’<w n="21.6"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rg<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="22" num="6.2"><w n="22.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="22.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
					<l n="23" num="6.3"><w n="23.1">R<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="23.2">m<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="23.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.5">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="24" num="6.4"><w n="24.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="24.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="24.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.4">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="24.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> ;</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="25.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.3">R<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="25.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="25.5">l</w>’<w n="25.6"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="25.7">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.8">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="26" num="7.2"><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="26.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="26.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="26.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.5">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
					<l n="27" num="7.3"><w n="27.1">T<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="27.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="27.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="28" num="7.4"><w n="28.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.3">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.5">M<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">H<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="29.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.3">gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>gn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="29.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="30" num="8.2"><w n="30.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="30.2">L<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>, <w n="30.3">H<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="31" num="8.3"><w n="31.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="31.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="31.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="31.5">qu</w>’<w n="31.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="31.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="32" num="8.4"><w n="32.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="32.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.3">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="32.4">d</w>’<w n="32.5"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.6"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>g<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="33.3">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="33.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="33.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.6">P<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					<l n="34" num="9.2"><w n="34.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.2">Tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>fl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="34.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="34.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="34.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w> <w n="34.6">m<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>,</l>
					<l n="35" num="9.3"><w n="35.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="35.2"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ffr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="35.4">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="35.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
					<l n="36" num="9.4"><w n="36.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="36.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="36.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="36.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="36.5">C<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="37.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="37.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rpr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="37.5">gr<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="38.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="38.3">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="38.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="38.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="38.6">s</w>’<w n="38.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>,</l>
					<l n="39" num="10.3"><w n="39.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="39.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="39.3">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="39.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="39.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="39.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.7">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="40" num="10.4"><w n="40.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="40.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="40.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="40.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="40.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bb<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.2">J<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="41.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="41.4">m<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="41.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="41.6">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="41.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="41.8">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					<l n="42" num="11.2"><w n="42.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="42.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="42.3"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="42.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd</w> <w n="42.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="42.6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w></l>
					<l n="43" num="11.3"><w n="43.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="43.2">l</w>’<w n="43.3">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="43.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="43.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="43.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="43.7">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					<l n="44" num="11.4"><w n="44.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="44.2">v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="44.3">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="44.4">j<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="44.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="44.6">l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">17 février 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>