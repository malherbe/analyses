<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SONNAILLES ET CLOCHETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3290 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_16</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SONNAILLES ET CLOCHETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1997">1997</date>
								</imprint>
								<biblScope unit="tome">VII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Sonnailles et clochettes</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>G. Charpentier et Cie, Éditeurs</publisher>
						<date when="1890">1890</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1888">1888</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN647">
				<head type="number">XIV</head>
				<head type="main">Repos</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.4">pr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> :</l>
					<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="2.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="3.2">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="3.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="3.4">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w></l>
					<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="4.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="4.5">b<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="5.2">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.4">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>ts</w>.</l>
					<l n="6" num="2.2"><w n="6.1">H<seg phoneme="i" type="vs" value="1" rule="493">y</seg>pn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="6.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="6.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="vi" value="1" rule="242">e</seg>nt</w>-<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w>, <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.4">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="7.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>gs</w>,</l>
					<l n="8" num="2.4"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="8.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="8.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="8.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.7">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="9.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="9.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ;</l>
					<l n="10" num="3.2"><w n="10.1">L<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ct<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.3">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="11" num="3.3"><w n="11.1">D<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="11.2">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="11.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="11.5">n</w>’<w n="11.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="11.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="11.8">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></l>
					<l n="12" num="3.4"><w n="12.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.5"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.7">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">L<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="13.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.4">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w></l>
					<l n="14" num="4.2"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="14.2">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.6">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="15" num="4.3"><w n="15.1">D<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="15.2">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="15.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="15.4">n</w>’<w n="15.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="15.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.8">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w></l>
					<l n="16" num="4.4"><w n="16.1">M<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rtr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="16.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="16.3">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="16.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.6">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="17.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="17.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.7">m<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> :</l>
					<l n="18" num="5.2"><w n="18.1">C</w>’<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="18.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.8">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					<l n="19" num="5.3"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="19.2">qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="19.3">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="19.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>,</l>
					<l n="20" num="5.4"><w n="20.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ</w>’<w n="20.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="20.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.4">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="20.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="20.6">d</w>’<w n="20.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ?</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="21.2">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="21.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.5">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>,</l>
					<l n="22" num="6.2"><w n="22.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.2">bl<seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="22.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.4"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="22.5">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					<l n="23" num="6.3"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="23.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="23.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="23.4">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="23.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.6">m</w>’<w n="23.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="23.8">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> :</l>
					<l n="24" num="6.4"><w n="24.1">Pr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="24.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w> <w n="24.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="24.5">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="25.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="25.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="25.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="25.5">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="25.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="25.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>,</l>
					<l n="26" num="7.2"><w n="26.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="26.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="26.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="26.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					<l n="27" num="7.3"><w n="27.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="27.3">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="27.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="27.5">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>,</l>
					<l n="28" num="7.4"><w n="28.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="28.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="28.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="28.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="29.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="29.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w></l>
					<l n="30" num="8.2"><w n="30.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="30.3"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="30.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.5">gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="31" num="8.3"><w n="31.1">H<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="31.3">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="31.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="31.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
					<l n="32" num="8.4"><w n="32.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="32.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="32.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="32.4">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="32.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="32.6">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">M<seg phoneme="œ" type="vs" value="1" rule="151">on</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>r</w>, <w n="33.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="33.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>lt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w></l>
					<l n="34" num="9.2"><w n="34.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.2">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="34.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="34.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="35" num="9.3"><w n="35.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="35.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>,</l>
					<l n="36" num="9.4"><w n="36.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="36.2"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="36.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="36.4">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="36.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="36.6">gr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1888"> 18 août 1888.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>