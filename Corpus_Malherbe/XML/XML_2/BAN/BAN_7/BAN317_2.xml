<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">RONDELS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>312 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">RONDELS</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillerondels.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN317">
				<head type="number">II</head>
				<head type="main">La Nuit</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.2">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5">N<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>,</l>
					<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="3" num="1.3"><w n="3.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="3.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.5">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="3.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>qu<seg phoneme="j" type="sc" value="0" rule="489">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.5">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.4">s</w>’<w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>,</l>
					<l n="6" num="2.2"><w n="6.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg></w> <w n="6.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">l</w>’<w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="6.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="360">en</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="7" num="2.3"><w n="7.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.2">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">N<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>,</l>
					<l n="8" num="2.4"><w n="8.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.3">qu</w>’<w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.5">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="9.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>,</l>
					<l n="10" num="3.2"><w n="10.1">R<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="10.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="10.3">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.5">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="11" num="3.3"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.3">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="11.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.7">g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="12" num="3.4"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="12.2">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="12.3">d</w>’<w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.5">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.7">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>,</l>
					<l n="13" num="3.5"><w n="13.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="13.2">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5">N<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>.</l>
				</lg>
		</div></body></text></TEI>