<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN122">
				<head type="main">BALLADE DES CÉLÉBRITÉS <lb></lb>DU TEMPS JADIS</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>-<w n="1.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="1.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="1.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="1.5">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="476">ï</seg></w></l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="2.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ct<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w> <w n="3.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg>f<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="476">ï</seg></w> ?</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> ? <w n="4.2">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="5" num="1.5"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">m<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>-<w n="5.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="5.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.4">pl<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="6.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">D<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>,</l>
					<l n="7" num="1.7"><hi rend="ital"><w n="7.1">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">gl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> </hi><w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <hi rend="ital"><w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ? </hi></l>
					<l n="8" num="1.8"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.2"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="8.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.5">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.6">d</w>’<w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> ?</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="9.2">V<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="9.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="9.6">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
					<l n="10" num="2.2"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w>-<w n="10.2">t</w>-<w n="10.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="10.4">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.6">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
					<l n="11" num="2.3"><w n="11.1">M<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt</w> <w n="11.2">s</w>’<w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="11.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="11.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="11.6">sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ?</l>
					<l n="12" num="2.4"><w n="12.1">M<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>tz</w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>-<w n="12.3">t</w>-<w n="12.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="12.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
					<l n="13" num="2.5"><w n="13.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="13.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.4">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="13.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="14" num="2.6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.4">P<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> ?</l>
					<l n="15" num="2.7"><w n="15.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="15.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="15.3">Cl<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.5">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
					<l n="16" num="2.8"><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="16.2"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="16.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="16.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.5">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="16.6">d</w>’<w n="16.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> ?</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="17.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="17.3">R<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.5">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="17.6">h<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="476">ï</seg></w>,</l>
					<l n="18" num="3.2"><w n="18.1">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="18.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="18.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="18.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.5">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="19" num="3.3"><w n="19.1">C<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>, <w n="19.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="19.3">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="19.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
					<l n="20" num="3.4"><w n="20.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="20.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w>, <w n="20.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.6">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="21" num="3.5"><w n="21.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.2">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="21.3">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w>-<w n="21.4">L<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="22" num="3.6"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="22.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.3">Rh<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="22.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="22.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="22.6">D<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w></l>
					<l n="23" num="3.7"><w n="23.1">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="23.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.4">d</w>’<w n="23.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
					<l n="24" num="3.8"><w n="24.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="24.2"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="24.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="24.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.5">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="24.6">d</w>’<w n="24.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> ?</l>
				</lg>
					<lg n="4">
						<head type="form">ENVOI</head>
						<l n="25" num="4.1"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="25.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="26" num="4.2"><w n="26.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="26.2">s</w>’<w n="26.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="26.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="26.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="26.6">d</w>’<w n="26.7"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rv<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w></l>
						<l n="27" num="4.3"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="27.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="27.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.4">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
						<l n="28" num="4.4"><w n="28.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="28.2"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="28.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="28.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.5">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="28.6">d</w>’<w n="28.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> ?</l>
					</lg>
				<closer>
					<dateline>
						<date when="1856">novembre 1856</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>