<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TRENTE-SIX BALLADES JOYEUSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1215 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banville36ballades.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">http://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VI</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN488">
					<head type="number">XIV</head>
					<head type="main">Ballade pour une aux cheveux dorés</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C<seg phoneme="i" type="vs" value="1" rule="493">y</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="1.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="1.4">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="1.5">d</w>’<w n="1.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="y" type="vs" value="1" rule="391">Eu</seg>t</w> <w n="2.2">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>j<seg phoneme="u" type="vs" value="1" rule="429">ou</seg><seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
						<l n="3" num="1.3"><w n="3.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
						<l n="4" num="1.4"><w n="4.1">M</w>’<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="5" num="1.5"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="5.3">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w></l>
						<l n="6" num="1.6"><w n="6.1">D</w>’<w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Pr<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="7.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="7.4">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="8" num="1.8"><w n="8.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="8.2">d</w>’<w n="8.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.5">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="8.6">d</w>’<w n="8.7"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
						<l n="10" num="2.2"><w n="10.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg>t</w> <w n="10.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w></l>
						<l n="11" num="2.3"><w n="11.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.2">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="11.3">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>.</l>
						<l n="12" num="2.4"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="12.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="12.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="12.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.5">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w> <w n="12.6">d</w>’<w n="12.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
						<l n="13" num="2.5"><w n="13.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
						<l n="14" num="2.6"><w n="14.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
						<l n="15" num="2.7"><w n="15.1">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="15.2">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
						<l n="16" num="2.8"><w n="16.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="16.2">d</w>’<w n="16.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.5">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="16.6">d</w>’<w n="16.7"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="17.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="17.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.4">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="17.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="17.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="17.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
						<l n="18" num="3.2"><w n="18.1">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="18.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="18.3">d</w>’<w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
						<l n="19" num="3.3"><w n="19.1">Ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="19.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="19.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ils</w> <w n="19.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
						<l n="20" num="3.4"><w n="20.1">S<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="20.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="20.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="20.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.6">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="20.7">fr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>,</l>
						<l n="21" num="3.5"><w n="21.1"><seg phoneme="j" type="sc" value="0" rule="496">Y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="21.2">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ds</w>, <w n="21.3">qu</w>’<w n="21.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="21.5">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w></l>
						<l n="22" num="3.6"><w n="22.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.2">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="22.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="22.4">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="22.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w></l>
						<l n="23" num="3.7"><w n="23.1">D</w>’<w n="23.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="23.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rm<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>,</l>
						<l n="24" num="3.8"><w n="24.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="24.2">d</w>’<w n="24.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="24.5">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="24.6">d</w>’<w n="24.7"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>.</l>
					</lg>
					<lg n="4">
						<head type="form">Envoi</head>
						<l n="25" num="4.1"><w n="25.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="25.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.3">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! <w n="25.4"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="25.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="25.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> !</l>
						<l n="26" num="4.2"><w n="26.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.2">str<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ph<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="26.4">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="26.5"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w></l>
						<l n="27" num="4.3"><w n="27.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="27.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="27.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
						<l n="28" num="4.4"><w n="28.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="28.2">d</w>’<w n="28.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="28.5">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="28.6">d</w>’<w n="28.7"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1861">Février 1861.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>