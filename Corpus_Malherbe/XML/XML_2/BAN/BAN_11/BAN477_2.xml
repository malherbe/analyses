<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TRENTE-SIX BALLADES JOYEUSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1215 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banville36ballades.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">http://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VI</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN477">
					<head type="number">III</head>
					<head type="main">Ballade de la bonne Doctrine</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">gl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6">cr<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="2" num="1.2"><w n="2.1">R<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.4"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="2.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.7">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
						<l n="3" num="1.3"><w n="3.1">B<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.4">d</w>’<w n="3.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="412">Ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.5">bl<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w> <w n="4.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rm<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>,</l>
						<l n="5" num="1.5"><w n="5.1">F<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.6">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.7">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
						<l n="6" num="1.6"><w n="6.1">B<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="6.4">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="6.5">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="6.6">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.8">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="7" num="1.7"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="305">Ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="8" num="1.8"><w n="8.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="8.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.3">s<seg phoneme="u" type="vs" value="1" rule="292">aou</seg>l</w> <w n="8.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
						<l n="9" num="1.9"><w n="9.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="9.4">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="9.5">cl<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.8">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="10" num="1.10"><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">m<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="363">en</seg></w> <w n="10.5">d</w>’<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="10.7">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><w n="11.1">Pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.2">d</w>’<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.6">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="11.7">t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>br<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="12" num="2.2"><w n="12.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="12.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="12.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="12.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.7">d</w>’<w n="12.8"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>,</l>
						<l n="13" num="2.3"><w n="13.1">C<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.3">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="14" num="2.4"><w n="14.1">C<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>, <w n="14.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="14.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
						<l n="15" num="2.5"><w n="15.1">T<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rpr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="15.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.6">v<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>,</l>
						<l n="16" num="2.6"><w n="16.1"><seg phoneme="ɛ" type="vs" value="1" rule="412">Ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.2">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="16.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="16.4">l</w>’<w n="16.5">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.6">str<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ct<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="17" num="2.7"><w n="17.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="17.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="17.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.6">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.7">N<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="18" num="2.8"><w n="18.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="18.2">l</w>’<w n="18.3"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="18.4">s</w>’<w n="18.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="18.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.7">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="18.9">cr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> : <w n="18.10">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
						<l n="19" num="2.9"><w n="19.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="19.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="19.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="19.5">R<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="19.7">l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="20" num="2.10"><w n="20.1">C</w>’<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.4">m<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="363">en</seg></w> <w n="20.5">d</w>’<w n="20.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="20.7">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1"><w n="21.1">M<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rdr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="21.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="21.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.5">p<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="22" num="3.2"><w n="22.1"><seg phoneme="w" type="sc" value="0" rule="431">Ou</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>r</w> <w n="22.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="22.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="22.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>str<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>,</l>
						<l n="23" num="3.3"><w n="23.1">R<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="23.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="23.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="23.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="23.5">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="23.6">Ch<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>vr<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="24" num="3.4"><w n="24.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>rr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="24.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="24.4">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w> <w n="24.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.6">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>,</l>
						<l n="25" num="3.5"><w n="25.1">Ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="25.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="25.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>,</l>
						<l n="26" num="3.6"><w n="26.1">L<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="26.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.3">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="26.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.6">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.7">c<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="27" num="3.7"><w n="27.1">T<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="27.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="27.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="27.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w> <w n="27.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="27.7">l</w>’<w n="27.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="28" num="3.8"><w n="28.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="28.2">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="28.3">d</w>’<w n="28.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="28.5">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="28.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="28.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.8">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
						<l n="29" num="3.9"><w n="29.1">P<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="29.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="29.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="29.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.6">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.7">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="30" num="3.10"><w n="30.1">C</w>’<w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="30.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.4">m<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="363">en</seg></w> <w n="30.5">d</w>’<w n="30.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="30.7">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="30.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<head type="form">Envoi</head>
						<l n="31" num="4.1"><w n="31.1">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="31.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.3">f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="31.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="31.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.8">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="32" num="4.2"><w n="32.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> (<w n="32.3">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="32.4">v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="32.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.7">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !)</l>
						<l n="33" num="4.3"><w n="33.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="33.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>, <w n="33.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.5">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s</w>, <w n="33.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.7">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lb<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
						<l n="34" num="4.4"><w n="34.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="34.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.3">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>, <w n="34.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="35" num="4.5"><w n="35.1">C</w>’<w n="35.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="35.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.4">m<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="363">en</seg></w> <w n="35.5">d</w>’<w n="35.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="35.7">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="35.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1862">Janvier 1862.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>