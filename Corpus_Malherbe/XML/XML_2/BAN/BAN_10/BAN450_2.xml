<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN450">
				<head type="main">Monstre vert</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Doucement… ce n’était qu’un rêve… O <lb></lb>
								lâche conscience, comme tu me tourmentes !
							</quote>
							<bibl>
								<name>Shakspere</name>,<hi rend="ital">Richard III</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">M<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ltk<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>. <w n="1.5">Tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="1.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="1.7">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w></l>
					<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.3">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>x<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="3.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.4">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="3.6">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w></l>
					<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.2">gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="4.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">L</w>’<w n="5.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.8">M<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>.</l>
					<l n="6" num="2.2"><w n="6.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="6.4">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="6.5">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="7.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w></l>
					<l n="8" num="2.4"><w n="8.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>. <w n="8.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.5">n<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">F<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">L</w>’<w n="9.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>. <w n="9.7">R<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
					<l n="10" num="3.2"><w n="10.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ffl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>, <w n="10.2">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="11" num="3.3"><w n="11.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>, <w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="11.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.7">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rp<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w></l>
					<l n="12" num="3.4"><w n="12.1">H<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.2">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.3">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="12.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.5">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> ;</l>
					<l n="14" num="4.2"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="14.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="14.6">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="15" num="4.3"><w n="15.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="15.4">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="15.5"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>,</l>
					<l n="16" num="4.4"><w n="16.1">V<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="16.2">tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="16.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.2">G<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="17.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w>,</l>
					<l n="18" num="5.2"><w n="18.1">Ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="18.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="19" num="5.3"><w n="19.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="19.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="19.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.7">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>cs<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
					<l n="20" num="5.4"><w n="20.1">F<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> : <w n="20.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.4">n<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.5"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.2">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>, <w n="21.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="21.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
					<l n="22" num="6.2"><w n="22.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="22.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w>-<w n="22.3">d<seg phoneme="ə" type="vi" value="1" rule="156">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="22.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="22.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="23" num="6.3"><w n="23.1">L<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.3"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="23.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="23.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.6">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="23.7">s<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w>,</l>
					<l n="24" num="6.4"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="24.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="24.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="24.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="24.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">F<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="25.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="25.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>, <w n="25.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>-<w n="25.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w></l>
					<l n="26" num="7.2"><w n="26.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="26.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.3">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w> <w n="26.4">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>.</l>
					<l n="27" num="7.3"><w n="27.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="27.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="27.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.4">Str<seg phoneme="i" type="vs" value="1" rule="493">y</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>. <w n="27.5"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="27.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.7">r<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
					<l n="28" num="7.4"><w n="28.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.2">n</w>’<w n="28.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="28.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="28.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.6">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>. <w n="28.7"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="28.8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.2">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="29.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="29.5">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> !</l>
					<l n="30" num="8.2"><w n="30.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="30.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> ! <w n="30.3">C</w>’<w n="30.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="30.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.7">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="31" num="8.3"><w n="31.1">S</w>’<w n="31.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="31.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.5">M<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ltk<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>. <w n="31.6">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w></l>
					<l n="32" num="8.4"><w n="32.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="32.2">l</w>’<w n="32.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.4">F<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>s</w>-<w n="33.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="33.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="33.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="33.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="33.7">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="34.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.3">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rp<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="34.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
					<l n="35" num="9.3"><w n="35.1">D<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="35.2">t</w>-<w n="35.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>. — <w n="35.4">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ? <w n="35.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="35.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="35.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>,</l>
					<l n="36" num="9.4"><w n="36.1">L<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="36.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="36.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="36.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">S<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="37.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.3">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="37.4">n</w>’<w n="37.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="37.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="37.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w> !</l>
					<l n="38" num="10.2"><w n="38.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="38.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="38.3">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> (<w n="38.4">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="38.5">m</w>’<w n="38.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>)</l>
					<l n="39" num="10.3"><w n="39.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.2">j</w>’<w n="39.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="39.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="39.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="39.7">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w>,</l>
					<l n="40" num="10.4"><w n="40.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="40.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="40.4">t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="40.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="40.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Décembre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>