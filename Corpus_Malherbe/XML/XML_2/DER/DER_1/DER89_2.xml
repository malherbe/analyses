<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER89">
				<head type="number">LXXXIX</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="1.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="1.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.4">m<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="1.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.6">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w> <w n="1.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="1.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.9"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.10"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>qu<seg phoneme="j" type="sc" value="0" rule="489">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w></l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.5">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="2.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.7">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.9">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.10">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>,</l>
					<l n="3" num="1.3"><space quantity="12" unit="char"></space><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="3.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="3.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="3.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w></l>
					<l n="4" num="1.4"><space quantity="12" unit="char"></space><w n="4.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.2">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="4.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="354">E</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>-<w n="5.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">d</w>’<w n="5.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="5.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="6" num="2.2"><w n="6.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.4">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="6.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="7" num="2.3"><space quantity="12" unit="char"></space><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.3">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.6">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="8" num="2.4"><space quantity="12" unit="char"></space><w n="8.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg></w>-<subst hand="RR" reason="analysis" type="phonemization"><del>16</del><add rend="hidden"><w n="8.2">s<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>z<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></add></subst> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.4"><subst hand="RR" reason="analysis" type="phonemization"><del>3</del><add rend="hidden">tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</add></subst>fr<add hand="RR" reason="analysis" type="completion" rend="hidden"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</add></w>. <w n="8.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="9.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="9.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">pl<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.9">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>,</l>
					<l n="10" num="3.2"><w n="10.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="10.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.6">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.7">l</w>’<w n="10.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>cr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="11" num="3.3"><space quantity="12" unit="char"></space><w n="11.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
					<l n="12" num="3.4"><space quantity="12" unit="char"></space><w n="12.1">C<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6">l<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.2">pl<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="13.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="13.6">qu</w>’<w n="13.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.10">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="14" num="4.2"><w n="14.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="14.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.8">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="14.9">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="15" num="4.3"><space quantity="12" unit="char"></space><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="15.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t</w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.4">l</w>’<w n="15.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.7">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="16" num="4.4"><space quantity="12" unit="char"></space><w n="16.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.5">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="17.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>. <w n="17.3">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="17.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="17.8">gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="17.9">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.10">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w></l>
					<l n="18" num="5.2"><w n="18.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="18.2">l</w>’<w n="18.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="18.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.6">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ge<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="18.7">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="18.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.9">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> ;</l>
					<l n="19" num="5.3"><space quantity="12" unit="char"></space><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="19.2">qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="19.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.4">n<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.7">c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w></l>
					<l n="20" num="5.4"><space quantity="12" unit="char"></space><w n="20.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.2">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="20.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rf<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> ?</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="21.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="21.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="21.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="22" num="6.2"><w n="22.1">B<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="22.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.3">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="22.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="22.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="22.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.7">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="23" num="6.3"><space quantity="12" unit="char"></space><w n="23.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="23.2">j</w>’<w n="23.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="23.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pl<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="24" num="6.4"><space quantity="12" unit="char"></space><w n="24.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="24.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="24.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="24.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>