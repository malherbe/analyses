<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER111">
				<head type="number">CXI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="1.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>pt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="1.6">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.7">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w></l>
					<l n="2" num="1.2"><w n="2.1">S<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="3" num="1.3"><w n="3.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="3.2">j</w>’<w n="3.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="4.2">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.4">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6">b<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="5.3">n</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="5.6">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="5.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="6" num="2.2"><w n="6.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w></l>
					<l n="7" num="2.3"><w n="7.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.3">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w></l>
					<l n="8" num="2.4"><w n="8.1">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="8.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.7">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">g<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="9.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w></l>
					<l n="10" num="3.2"><w n="10.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="10.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.5">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="11" num="3.3"><w n="11.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.2">j<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="11.3">r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.5">m<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="12" num="3.4"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="12.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.5">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.6">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.2">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="13.6">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.7">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="13.8">j<seg phoneme="ə" type="ef" value="1" rule="DER111_1">e</seg></w></l>
					<l n="14" num="4.2"><w n="14.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.4">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="14.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.6">j</w>’<w n="14.7"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="14.8">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">qu</w>’<w n="15.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="15.4">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="15.5">bl<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w> <w n="15.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
					<l n="16" num="4.4"><w n="16.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.2">m<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="16.3">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="16.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="16.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.6">g<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="17.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w></l>
					<l n="18" num="5.2"><w n="18.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.2">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="18.3">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="19" num="5.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="19.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.3">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4">d</w>’<w n="19.5"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>cr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="19.6">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="20" num="5.4"><w n="20.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.3">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="20.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.3">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="21.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="21.5">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.6">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="22" num="6.2"><w n="22.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.2">p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>x</w> <w n="22.3">d</w>’<w n="22.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="22.5">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>.</l>
					<l n="23" num="6.3"><w n="23.1">Qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> ! <w n="23.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="23.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.4">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="23.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="23.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t</w></l>
					<l n="24" num="6.4"><w n="24.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="24.3">t</w>’<w n="24.4"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w> <w n="24.5">fl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="24.7">l</w>’<w n="24.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.2">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w>, <w n="25.4">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>,</l>
					<l n="26" num="7.2"><w n="26.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="26.2">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="26.3">j<seg phoneme="o" type="vs" value="1" rule="318">au</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="26.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="27" num="7.3"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="27.2">j</w>’<w n="27.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="27.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="27.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.8">V<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="28" num="7.4"><w n="28.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="28.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="28.4">d</w>’<w n="28.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="28.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>.</l>
				</lg>
			</div></body></text></TEI>