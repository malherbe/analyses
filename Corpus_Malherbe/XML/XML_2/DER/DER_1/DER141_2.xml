<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" rhyme="none" key="DER141">
				<head type="number">CXLI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">S<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="1.2">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="1.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="1.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> !</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="2.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="2.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="2.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">l</w>’<w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.8">cr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="2.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w>, <w n="2.10">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="2.11">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.12">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w>,</l>
					<l n="3" num="1.3"><w n="3.1">F<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="3.2">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> : <hi rend="ital"><w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.4">H<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="3.5">C<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="3.6">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>str<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></hi></l>
					<l n="4" num="1.4"><hi rend="ital"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="4.2">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></hi> ; <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w>, <w n="4.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="4.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.8">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>str<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="5.3">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.4"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="5.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w></l>
					<l n="6" num="1.6"><w n="6.1">D</w>’<w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="497">y</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rtr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.8">Pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w></l>
					<l n="7" num="1.7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="7.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="7.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="8" num="1.8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="8.5">n</w>’<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="8.8">qu</w>’<w n="8.9"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="9" num="1.9"><w n="9.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.9">n</w>’<w n="9.10"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="9.11">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w></l>
					<l n="10" num="1.10"><w n="10.1">Qu</w>’<w n="10.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="10.7"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="10.8">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.9">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.10">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
					<l n="11" num="1.11"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.3">m</w>’<w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w>, <w n="11.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.8">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="11.9">l</w>’<w n="11.10"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="11.11">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.12">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="12" num="1.12"><w n="12.1">D</w>’<w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.3">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg></w> <w n="12.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.9">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.10">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					<l n="13" num="1.13"><w n="13.1">Ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="13.2">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>, <w n="13.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="13.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="13.6">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.8">l</w>’<w n="13.9"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
					<l n="14" num="1.14"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">qu</w>’<w n="14.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>ill<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="14.4">j</w>’<w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.7">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.8">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>,</l>
					<l n="15" num="1.15"><w n="15.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ; <w n="15.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.3">l</w>’<w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="15.5">t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="15.7">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.8">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="16" num="1.16"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.3">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="16.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="16.6">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="16.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.9">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="17" num="1.17"><w n="17.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="17.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="17.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.5">b<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="17.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.8">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>fl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Gr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="i" type="vs" value="1" rule="493">y</seg></w>, <w n="18.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.6">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="18.7">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>.</l>
				</lg>
			</div></body></text></TEI>