<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER39">
				<head type="number">XXXIX</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.3">br<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.6">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="2.4">s</w>’<w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ff<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="2.7">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>xh<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="4.3">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="4.5">s</w>’<w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ff<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>, <w n="4.7">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>-<w n="4.8"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="5" num="1.5"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.6">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="5.7">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8">s</w>’<w n="5.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="5.10">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w></l>
					<l n="6" num="1.6"><w n="6.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.3">t<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ; <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="6.6">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="6.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> ;</l>
					<l n="7" num="1.7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="7.6">l</w>’<w n="7.7">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="7.9">l</w>’<w n="7.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="8" num="1.8"><w n="8.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="8.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="9" num="1.9"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="9.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="9.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="9.7"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="9.8">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.9">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w></l>
					<l n="10" num="1.10"><w n="10.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.4">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="10.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="10.7">c<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>