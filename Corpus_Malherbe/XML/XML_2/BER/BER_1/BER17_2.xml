<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER17">
				<head type="main">CHARLES SEPT</head>
				<head type="tune">Musique de M. B. WILHEM</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="2.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> ; <w n="2.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w>, <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> !</l>
					<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>, <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.2">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w>, <w n="4.3">l</w>’<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.7">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="5.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="5.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="6" num="1.6"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.2">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="6.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gs</w> <w n="6.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
					<l n="7" num="1.7"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.3">l</w>’<w n="7.4">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="7.6">d</w>’<w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="8" num="1.8"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="8.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="8.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.6">l</w>’<w n="8.7">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.4">d</w>’<w n="9.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="9.7"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="10" num="2.2"><w n="10.1">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.3">r<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="10.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="10.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>,</l>
					<l n="11" num="2.3"><w n="11.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="12" num="2.4"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="12.2">pr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.4">f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>.</l>
					<l n="13" num="2.5"><w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="13.2">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>, <w n="13.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.4">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="13.5">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="13.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.8">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="14" num="2.6"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="14.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="14.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="14.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.6">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
					<l n="15" num="2.7"><w n="15.1">J</w>’<w n="15.2"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="15.3">l</w>’<w n="15.4">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="15.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="15.6">d</w>’<w n="15.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="16" num="2.8"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="16.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="16.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.6">l</w>’<w n="16.7">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">S</w>’<w n="17.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="17.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="17.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="17.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="17.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="18" num="3.2"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>, <w n="18.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="18.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="18.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>.</l>
					<l n="19" num="3.3"><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ; <w n="19.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="19.4">l</w>’<w n="19.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="19.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.8">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="20" num="3.4"><w n="20.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="20.2">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="20.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>.</l>
					<l n="21" num="3.5"><w n="21.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.2">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="21.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>cr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ; <w n="21.4">j</w>’<w n="21.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="21.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.8">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="22" num="3.6"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="22.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.3">ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ffr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="22.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="22.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
					<l n="23" num="3.7"><w n="23.1">J</w>’<w n="23.2"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="23.3">l</w>’<w n="23.4">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="23.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="23.6">d</w>’<w n="23.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="24" num="3.8"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="24.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="24.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="24.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="24.6">l</w>’<w n="24.7">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">D<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="25.2">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.3">Tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="25.4">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>tr<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="26" num="4.2"><w n="26.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="26.2">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="26.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="26.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="26.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="27" num="4.3"><w n="27.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="27.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.3">l<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="27.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>gt</w> <w n="27.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="28" num="4.4"><w n="28.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="28.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
					<l n="29" num="4.5"><w n="29.1">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="29.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="29.3">d<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>vr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="29.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="29.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="30" num="4.6"><w n="30.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="30.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.3">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="30.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="30.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="30.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.7">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
					<l n="31" num="4.7"><w n="31.1">J</w>’<w n="31.2"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="31.3">l</w>’<w n="31.4">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="31.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="31.6">d</w>’<w n="31.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="32" num="4.8"><w n="32.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="32.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="32.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="32.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="32.6">l</w>’<w n="32.7">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
				</lg>
			</div></body></text></TEI>