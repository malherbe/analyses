<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER27">
				<head type="main">LA DOUBLE IVRESSE</head>
				<head type="tune">AIR : Que ne suis-je la fougère !</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.4">l</w>’<w n="1.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="2.2">N<seg phoneme="e" type="vs" value="1" rule="250">œ</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="2.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t</w> <w n="2.4">m</w>’<w n="2.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> :</l>
					<l n="3" num="1.3"><w n="3.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">cr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="3.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="3.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="4.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="4.5">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> :</l>
					<l n="5" num="1.5"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.4">Z<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="6" num="1.6"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>pr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> ;</l>
					<l n="7" num="1.7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="7.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="8" num="1.8"><w n="8.1">Fl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr</w>’<w n="8.5"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.4">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="9.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="10" num="2.2">(<w n="10.1">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.2">fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="10.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">l</w>’<w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.7">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>)</l>
					<l n="11" num="2.3"><w n="11.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="11.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="12" num="2.4"><w n="12.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.2">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.5">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w>.</l>
					<l n="13" num="2.5"><w n="13.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="13.2">qu</w>’<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.5"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="14" num="2.6"><w n="14.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
					<l n="15" num="2.7"><w n="15.1">L</w>’<w n="15.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="15.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="15.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="16" num="2.8"><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="16.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="16.4">d</w>’<w n="16.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="16.6">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">N<seg phoneme="e" type="vs" value="1" rule="250">œ</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="17.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="17.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="18" num="3.2"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="497">Y</seg></w> <w n="18.2">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="18.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.4">v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="18.5">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.6">l</w>’<w n="18.7"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
					<l n="19" num="3.3"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="19.2">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="19.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="19.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.5">r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.7">v<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
					<l n="20" num="3.4"><w n="20.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="20.3">qu</w>’<w n="20.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.5">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="20.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="20.7">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
					<l n="21" num="3.5"><w n="21.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="21.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="22" num="3.6"><w n="22.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.2">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="22.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>-<w n="22.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="22.6">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.7">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
					<l n="23" num="3.7"><w n="23.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xp<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="23.4">l</w>’<w n="23.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="24" num="3.8"><w n="24.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="24.2">l</w>’<w n="24.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.5">l</w>’<w n="24.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.3">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="25.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xtr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
					<l n="26" num="4.2"><w n="26.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="26.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="26.3">qu</w>’<w n="26.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="26.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> !</l>
					<l n="27" num="4.3"><w n="27.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.2">n</w>’<w n="27.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="27.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="27.5">N<seg phoneme="e" type="vs" value="1" rule="250">œ</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="27.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.7">j</w>’<w n="27.8"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="28" num="4.4"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="28.2">N<seg phoneme="e" type="vs" value="1" rule="250">œ</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="28.3">s</w>’<w n="28.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="28.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="28.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="28.7">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
					<l n="29" num="4.5"><w n="29.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="29.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="30" num="4.6"><w n="30.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="30.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.4">c</w>’<w n="30.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="30.6">qu</w>’<w n="30.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
					<l n="31" num="4.7"><w n="31.1">D<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>, <w n="31.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="31.3">l</w>’<w n="31.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="31.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="31.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="32" num="4.8"><w n="32.1">J</w>’<w n="32.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="32.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="32.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.5">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t</w> <w n="32.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="32.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
				</lg>
			</div></body></text></TEI>