<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER47">
				<head type="main">PRIÈRE D’UN ÉPICURIEN</head>
				<head type="sub_2">COUPLET ÉCRIT AUX CATACOMBES LE JOUR <lb></lb>OÙ S’Y RENDIRENT LES MEMBRES DU CAVEAU</head>
				<head type="tune">AIR : Ce magistrat irréprochable</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p</w> <w n="1.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="1.6">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="2.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ;</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="3.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">R<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="5.2">l</w>’<w n="5.3">h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="5.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="435">O</seg>pp<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">b<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="6.4">d</w>’<w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ;</l>
					<l n="7" num="1.7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="7.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="7.6">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="8" num="1.8"><w n="8.1">N<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="8.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
				</lg>
			</div></body></text></TEI>