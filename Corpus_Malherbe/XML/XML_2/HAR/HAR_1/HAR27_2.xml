<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La Légende des Sexes</title>
				<title type="sub">Poëmes Hystériques</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1404 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Légende des sexes, poëmes hystériques</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https ://fr.wikisource.org/wiki/La_L%C3%A9gende_des_sexes,_po%C3%ABmes_hyst%C3%A9riques</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Légende des sexes, poëmes hystériques</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https ://fr.wikisource.org/wiki/Fichier :Haraucourt_-_La_L%C3%A9gende_des_sexes,_po%C3%ABmes_hyst%C3%A9riques,_1882.djvu</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Imprimé à Bruxelles pour l’auteur</publisher>
									<date when="1882">1882</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Légende des sexes, poëmes hystériques</title>
						<author>Edmond Haraucourt</author>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/btv1b8618380c</idno>
						<imprint>
							<pubPlace>Bruxelles</pubPlace>
							<publisher>ÉDITION PRIVILE, REVUE PAR L’AUTEUR</publisher>
							<date when="1893">1893</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1882">1882</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-28" who="RR">Une correction introduite à partir de l’édition imprimée de 1893</change>
				<change when="2017-10-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La légende des sexes</head><div type="poem" key="HAR27">
					<head type="main">LE BOUCLIER</head>
					<opener>
						<salute>A Émile Goubert.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">— <w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">B<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>cl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="1.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.4">j</w>’<w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="1.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.8">r<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> !</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">V<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="2.5">G<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6">d</w>’<w n="2.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.3">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.4">j<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="3.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.7">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.5">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="4.6">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">M<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.4">f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r</w> <w n="5.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.7">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="6" num="2.2"><w n="6.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">l</w>’<w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">En</seg>n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.7">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="6.8">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.9">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.10">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="7.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.6">j</w>’<w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="7.8">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>tt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>ts</w> <w n="8.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">t</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">d</w>’<w n="9.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="9.7">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.9">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> :</l>
						<l n="10" num="3.2"><w n="10.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="10.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>ls</w> <w n="10.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.5"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="10.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.7">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="10.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.9">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.10">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>x<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">j</w>’<w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="11.4">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.6">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="11.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.9">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="11.10">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="12.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.6">bl<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">C</w>’<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="13.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="13.6">t</w>’<w n="13.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.8">dr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="13.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.10"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w></l>
						<l n="14" num="4.2"><w n="14.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="14.4"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="14.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">j</w>’<w n="15.3"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="15.4">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.7">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.8"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
				</div></body></text></TEI>