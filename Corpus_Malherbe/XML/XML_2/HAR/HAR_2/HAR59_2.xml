<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES CULTES</head><div type="poem" key="HAR59">
						<head type="main">LE SOU</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="1.3">gu<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.4">d</w>’<w n="1.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.9">r<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
							<l n="2" num="1.2"><w n="2.1">R<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="2.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="2.8">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> :</l>
							<l n="3" num="1.3"><w n="3.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>pt<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.3">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.5">pr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="4" num="1.4"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="4.3">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>, <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="4.5">qu</w>’<w n="4.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="4.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.9">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.10">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.11">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="5" num="1.5"><w n="5.1">M<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ils</w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.5">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.6">l</w>’<w n="5.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
							<l n="6" num="1.6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>, <w n="6.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="6.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.4">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="6.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w>, <w n="6.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.8">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.9">j<seg phoneme="o" type="vs" value="1" rule="318">au</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
							<l n="7" num="1.7"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="7.2">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.6">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="7.7">ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
							<l n="8" num="1.8"><w n="8.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.5">v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="8.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.10">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
							<l n="9" num="1.9"><w n="9.1">L</w>’<w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.5">v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="9.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="9.9">s</w>’<w n="9.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257">eoi</seg>r</w>,</l>
							<l n="10" num="1.10"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.5">s<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="10.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.7">l</w>’<w n="10.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.9">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="10.10"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="10.11">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>.</l>
						</lg>
					</div></body></text></TEI>