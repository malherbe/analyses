<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURELE SOIR</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><div type="poem" key="HAR137">
						<head type="main">LES BÊTES</head>
						<opener>
							<salute>À MADAME M. DE LAMER</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.6">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="1.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.9">b<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
							<l n="2" num="1.2"><w n="2.1">Qu</w>’<w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.3">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="2.5">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="2.6">br<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.8">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="2.9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>ps</w>.</l>
							<l n="3" num="1.3"><w n="3.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ; <w n="3.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="3.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7">l</w>’<w n="3.8"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="3.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.10">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w></l>
							<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.3">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="4.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="4.7"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.8">h<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.2">d</w>’<w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">bl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c</w> <w n="5.8">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="5.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.10">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="5.11">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
							<l n="6" num="2.2"><w n="6.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="6.4">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w> <w n="6.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="6.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.7">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="6.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
							<l n="7" num="2.3"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="7.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="7.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="7.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.9">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="7.10">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="7.11">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.12">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
							<l n="8" num="2.4"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>, <w n="8.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.7">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="9.2">l</w>’<w n="9.3">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="9.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="9.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.9">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
							<l n="10" num="3.2"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="10.2">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>, <w n="10.3">l</w>’<w n="10.4"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="10.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>-<w n="10.6">cl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.8">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="10.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
							<l n="11" num="3.3"><w n="11.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="11.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.3">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="11.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="11.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.6">cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="11.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.8">d</w>’<w n="11.9"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="12.2">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="12.5">gr<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="12.6">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.7">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
							<l n="13" num="4.2"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="vi" value="1" rule="242">e</seg>nt</w> <w n="13.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.4">l</w>’<w n="13.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="13.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="13.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.9">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> <w n="13.10">d</w>’<w n="13.11"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>…</l>
							<l n="14" num="4.3">— <w n="14.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.6">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> : <w n="14.7">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.8">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.9">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="14.10">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>.</l>
						</lg>
					</div></body></text></TEI>