<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURE</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><head type="main_subpart">L’AUBE</head><div type="poem" key="HAR105">
						<head type="main">ROMANCE</head>
						<lg n="1">
							<l n="1" num="1.1">— <w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.2">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="1.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="1.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="1.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="1.7">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="1.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w>,</l>
							<l n="2" num="1.2"><w n="2.1">S<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="2.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.4">l</w>’<w n="2.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>, <w n="2.8">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
							<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="3.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> : « <w n="3.3">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="3.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.8">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> ? »</l>
							<l n="4" num="1.4"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">n</w>’<w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.7">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="4.10">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">— <w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.2">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">cr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.8">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w></l>
							<l n="6" num="2.2"><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.3">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ll<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.4">d</w>’<w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="6.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="6.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.8">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.10">v<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
							<l n="7" num="2.3"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="7.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> : « <w n="7.4">V<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="7.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="7.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="7.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> ? »</l>
							<l n="8" num="2.4"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">n</w>’<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="8.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.7">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="8.9">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="8.10">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.11">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1">— <w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="9.3">cl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="9.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.6">br<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="9.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>,</l>
							<l n="10" num="3.2"><w n="10.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ils</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w> <w n="10.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.8">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
							<l n="11" num="3.3"><w n="11.1">L<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="11.2"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> : « <w n="11.4">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="11.6"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.7">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="11.8">t</w>-<w n="11.9"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> ? »</l>
							<l n="12" num="3.4"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="12.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">n</w>’<w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="12.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="12.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.7">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.8">n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="12.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="13.2">j</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="13.4">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="13.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.6">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.9">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="13.10"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="13.11"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ;</l>
							<l n="14" num="4.2"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.4">s<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="14.5">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.6">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="15" num="4.3"><w n="15.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.2">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="15.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="15.6">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="15.7">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="15.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.9">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.10">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
							<l n="16" num="4.4"><w n="16.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.2">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="16.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="16.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="16.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="16.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.8">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="16.9">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="16.10">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.11">t</w>’<w n="16.12"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						</lg>
					</div></body></text></TEI>