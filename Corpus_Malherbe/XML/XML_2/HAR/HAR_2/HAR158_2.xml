<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MER DE GLACE</head><div type="poem" key="HAR158">
					<head type="main">MER DE GLACE</head>
					<opener>
						<salute>À CYPRIEN GODEBSKI</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="1.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.7">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>, <w n="1.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.9">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w></l>
						<l n="2" num="1.2"><w n="2.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.4">bl<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.7">d</w>’<w n="2.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.9">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="2.10">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="3.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="3.6">d</w>’<w n="3.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.8">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="3.9">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w>, <w n="3.10">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.11">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.12">t<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
						<l n="4" num="2.2"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.2">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="4.3">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.5">bl<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w> <w n="4.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="4.7">l</w>’<w n="4.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="4.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.10">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="4.11">st<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="5.2">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="5.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.7">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w></l>
						<l n="6" num="3.2"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="6.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.4">h<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="6.5">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="6.7">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="6.8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> :</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.7">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="7.8">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="8" num="4.2"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="8.2">sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lpt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="8.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.5">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="9.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>v<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ls<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="9.4">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="10" num="5.2"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.2">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="10.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.4">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>cs</w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="10.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.9">d</w>’<w n="10.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1"><w n="11.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.3"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.4">d</w>’<w n="11.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="11.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.8">s</w>’<w n="11.9"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="11.10">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.11">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.12">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="12" num="6.2"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="12.2">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="12.3">gl<seg phoneme="o" type="vs" value="1" rule="318">au</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="12.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.8">br<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="12.9">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1"><w n="13.1">J<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="13.4">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="13.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.8">p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>x</w> ;</l>
						<l n="14" num="7.2"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.4">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt</w> <w n="14.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.7">l</w>’<w n="14.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="14.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>.</l>
					</lg>
					<lg n="8">
						<l n="15" num="8.1"><w n="15.1">C</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="15.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="15.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.8">f<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="15.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.10"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="16" num="8.2"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.5">r<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>cs</w> <w n="16.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="16.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.9">n<seg phoneme="o" type="vs" value="1" rule="318">au</seg>fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> !</l>
					</lg>
					<lg n="9">
						<l n="17" num="9.1"><w n="17.1">C</w>’<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="17.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="17.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.5">j<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="17.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.7">l</w>’<w n="17.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.10">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.11">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>,</l>
						<l n="18" num="9.2"><w n="18.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.2">cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="18.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.6">d</w>’<w n="18.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="18.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.9">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> !</l>
					</lg>
					<lg n="10">
						<l n="19" num="10.1"><w n="19.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rp<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="19.3">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="19.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.6">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="20" num="10.2"><w n="20.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.2">l</w>’<w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="20.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="20.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="20.7">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
					</lg>
					<lg n="11">
						<l n="21" num="11.1"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="21.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.3">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="21.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.8">fl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="22" num="11.2"><w n="22.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="22.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.3">cr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="22.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="22.5">l</w>’<w n="22.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.8">l</w>’<w n="22.9">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>…</l>
					</lg>
					<lg n="12">
						<l n="23" num="12.1">— <w n="23.1">T<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="23.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="23.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>, <w n="23.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="23.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="23.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="23.8">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="23.9">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="24" num="12.2"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="24.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="24.3">h<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="24.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="24.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="24.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.8">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="24.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="24.10">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="13">
						<l n="25" num="13.1"><w n="25.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="25.2">h<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="25.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="25.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="25.6">j</w>’<w n="25.7"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="25.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="25.9">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> :</l>
						<l n="26" num="13.2"><w n="26.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="26.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="26.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="26.6">j</w>’<w n="26.7"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="26.8">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="26.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.10">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>