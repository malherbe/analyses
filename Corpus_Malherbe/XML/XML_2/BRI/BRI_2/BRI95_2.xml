<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE CINQUIÈME</head><head type="sub_part">A ROME</head><div type="poem" key="BRI95">
					<head type="main">Les Dieux chez Anacréon</head>
					<head type="sub_1">(D’après un bas-relief de M. Guillaume)</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">P<seg phoneme="o" type="vs" value="1" rule="444">O</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">È</seg>T<seg phoneme="ə" type="vi" value="1" rule="348">E</seg>S</w>, <w n="1.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></l>
							<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="2.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> :</l>
							<l n="3" num="1.3"><w n="3.1">Qu</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="3.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="3.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ct<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						</lg>
						<lg n="2">
							<l n="4" num="2.1"><space unit="char" quantity="8"></space><w n="4.1">C<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="4.2">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="4.3">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w></l>
							<l n="5" num="2.2"><space unit="char" quantity="8"></space><w n="5.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.3">Gr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.4">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="5.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.6">G<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
							<l n="6" num="2.3"><w n="6.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="6.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="6.4">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.7">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="7" num="1.1"><space unit="char" quantity="8"></space><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.2">T<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w>, <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.6">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="7.7">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>,</l>
							<l n="8" num="1.2"><space unit="char" quantity="8"></space><w n="8.1">L</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>,</l>
							<l n="9" num="1.3"><w n="9.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.4">l</w>’<w n="9.5">h<seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>th<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xh<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="9.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
						</lg>
						<lg n="2">
							<l n="10" num="2.1"><space unit="char" quantity="8"></space><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>, <w n="10.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.4">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
							<l n="11" num="2.2"><space unit="char" quantity="8"></space><w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="11.2">ch<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="11.4">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
							<l n="12" num="2.3"><w n="12.1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t</w> <w n="12.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.5">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w> <w n="12.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="12.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.8">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>chr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> :</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1"><space unit="char" quantity="8"></space>« <w n="13.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="13.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> !</l>
							<l n="14" num="3.2"><space unit="char" quantity="8"></space><w n="14.1">C</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.3">l</w>’<w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="14.6">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="14.7">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
							<l n="15" num="3.3"><w n="15.1">L</w>’<w n="15.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.3">j<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="15.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.6">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.8">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.9">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
						<lg n="4">
							<l n="16" num="4.1"><space unit="char" quantity="8"></space>— <w n="16.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.3">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cch<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="16.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="16.5">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="383">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> !</l>
							<l n="17" num="4.2"><space unit="char" quantity="8"></space><w n="17.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="17.3">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.5">l</w>’<w n="17.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w>,</l>
							<l n="18" num="4.3"><w n="18.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w> <w n="18.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> : <w n="18.5">c</w>’<w n="18.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="18.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.8">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="18.9">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="18.10">l</w>’<w n="18.11"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
						<lg n="5">
							<l n="19" num="5.1"><space unit="char" quantity="8"></space>— <w n="19.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="19.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> !</l>
							<l n="20" num="5.2"><space unit="char" quantity="8"></space><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="20.2">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w> <w n="20.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
							<l n="21" num="5.3"><w n="21.1">J</w>’<w n="21.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="21.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.4">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="21.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="21.7">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="21.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.9">d</w>’<w n="21.10"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>. »</l>
						</lg>
						<lg n="6">
							<l n="22" num="6.1"><space unit="char" quantity="8"></space><w n="22.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.2">cl<seg phoneme="e" type="vs" value="1" rule="148">e</seg>f</w> <w n="22.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.4">br<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>z<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="22.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="22.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
							<l n="23" num="6.2"><space unit="char" quantity="8"></space><w n="23.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="23.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="23.3">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cch<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="23.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.5">l</w>’<w n="23.6"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
							<l n="24" num="6.3"><w n="24.1">Ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="24.2">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="24.3">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="24.4">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="383">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="24.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.7">P<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">III</head>
						<lg n="1">
							<l n="25" num="1.1"><space unit="char" quantity="8"></space><w n="25.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="25.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="25.4">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.6">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !</l>
							<l n="26" num="1.2"><space unit="char" quantity="8"></space><w n="26.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="26.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="26.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="26.5">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
							<l n="27" num="1.3"><w n="27.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="27.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="27.4">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="27.5">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="27.6">sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lpt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="27.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.8">Gr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="28" num="2.1"><space unit="char" quantity="8"></space><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="28.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="28.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.4">m<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.5"><seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w></l>
							<l n="29" num="2.2"><space unit="char" quantity="8"></space><w n="29.1">J</w>’<w n="29.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="29.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="29.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.5">chr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>,</l>
							<l n="30" num="2.3"><w n="30.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.2">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rp<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="30.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="30.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>fs</w>, <w n="30.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.7">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rp<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.8">d</w>’<w n="30.9"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						</lg>
					</div>
				</div></body></text></TEI>