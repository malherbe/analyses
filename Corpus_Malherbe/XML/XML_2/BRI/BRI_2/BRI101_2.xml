<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE CINQUIÈME</head><head type="sub_part">A ROME</head><div type="poem" key="BRI101">
					<head type="main">En traversant le Forum</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">S<seg phoneme="u" type="vs" value="1" rule="425">OU</seg>S</w> <w n="1.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rcs</w> <w n="1.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="1.6">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="j" type="sc" value="0" rule="475">ï</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.10">f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>,</l>
						<l n="2" num="1.2"><w n="2.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="2.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.5">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.7">l</w>’<w n="2.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>,</l>
						<l n="3" num="1.3"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.4">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="3.7">h<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.9">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="3.10">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>pr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="4.4">G<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ths</w>, <w n="4.5">H<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg>s</w>, <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w>, <w n="4.7">V<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="5" num="1.5"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="5.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="5.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.10">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w>,</l>
						<l n="6" num="1.6"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="6.2">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rb<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="6.3">fl<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rb<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="6.6">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> !</l>
						<l n="7" num="1.7"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="ɔ" type="vs" value="1" rule="439">O</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w>, <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.8">L<seg phoneme="y" type="vs" value="1" rule="450">u</seg>x<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.9"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="8" num="1.8"><w n="8.1">S</w>’<w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="8.3">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="8.5">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="9" num="1.9"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.2">L<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.5">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w>, <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="9.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>,</l>
						<l n="10" num="1.10"><w n="10.1">M<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="10.3">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="10.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.5">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="10.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="10.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> !</l>
					</lg>
				</div></body></text></TEI>