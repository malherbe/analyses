<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RELIQUIÆ</head><div type="poem" key="BUS72">
					<head type="main">CONFESSION</head>
					<div n="1" type="section">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="1.6">c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="2.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="2.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
							<l n="3" num="1.3"><w n="3.1">S<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.5">l</w>’<w n="3.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="4" num="1.4"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">l</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.7">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="5.2">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="5.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="6" num="2.2"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.2">l</w>’<w n="6.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="6.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ff<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ;</l>
							<l n="7" num="2.3"><w n="7.1">N<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="7.2">n</w>’<w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>, <w n="7.6">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="8" num="2.4"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="8.4">tr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="9.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="9.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
							<l n="10" num="3.2"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="10.2">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="10.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="10.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
							<l n="11" num="3.3"><w n="11.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="11.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>fl<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="12" num="3.4"><w n="12.1">R<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.3">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="12.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>.</l>
						</lg>
					</div>
					<div n="2" type="section">
						<head type="number">II</head>
						<lg n="1">
							<l n="13" num="1.1"><w n="13.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">l</w>’<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.5">c<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="13.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="14" num="1.2"><w n="14.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="14.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.5">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w></l>
							<l n="15" num="1.3"><w n="15.1">S</w>’<w n="15.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="15.3">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="15.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.5"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="15.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="16" num="1.4"><w n="16.1">L<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="16.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
						</lg>
						<lg n="2">
							<l n="17" num="2.1"><w n="17.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.5">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rf<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="18" num="2.2"><w n="18.1">N<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.2">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="18.5">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ;</l>
							<l n="19" num="2.3"><w n="19.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="19.2">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.3">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.6">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="20" num="2.4"><w n="20.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="20.2">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="20.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="20.4">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.5">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="20.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
						</lg>
						<lg n="3">
							<l n="21" num="3.1"><w n="21.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="21.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="22" num="3.2"><w n="22.1"><seg phoneme="i" type="vs" value="1" rule="497">Y</seg></w> <w n="22.2">gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>, <w n="22.3">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="22.4">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="22.5">s<seg phoneme="y" type="vs" value="1" rule="445">û</seg>rs</w>,</l>
							<l n="23" num="3.3"><w n="23.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="23.3">n<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="24" num="3.4"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="24.2">l</w>’<w n="24.3">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="24.6"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w>.</l>
						</lg>
					</div>
					<div n="3" type="section">
						<head type="number">III</head>
						<lg n="1">
							<l n="25" num="1.1"><w n="25.1">Qu</w>’<w n="25.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="25.3">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="25.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="25.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="25.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="26" num="1.2"><w n="26.1"><seg phoneme="ɛ" type="vs" value="1" rule="308">Ai</seg>t</w> <w n="26.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="26.3">l</w>’<w n="26.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="26.5">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
							<l n="27" num="1.3"><w n="27.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="27.4">l</w>’<w n="27.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="28" num="1.4"><w n="28.1"><seg phoneme="ɛ" type="vs" value="1" rule="306">Ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="28.2">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="28.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="28.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="28.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.7">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>.</l>
						</lg>
						<lg n="2">
							<l n="29" num="2.1"><w n="29.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.2">n</w>’<w n="29.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="29.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="29.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="29.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="29.7">c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="30" num="2.2"><w n="30.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <hi rend="ital"><w n="30.2">fl<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t</w>-<w n="30.3">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss</w></hi> <w n="30.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="30.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="30.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
							<l n="31" num="2.3"><w n="31.1">C</w>’<w n="31.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="31.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="31.4">t<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="31.5">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lg<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="31.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="31.7">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="32" num="2.4"><w n="32.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.2">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="32.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="32.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
						</lg>
						<lg n="3">
							<l n="33" num="3.1"><w n="33.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="33.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="33.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="33.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.5">t</w>’<w n="33.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="33.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dj<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="34" num="3.2"><w n="34.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="34.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="34.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="34.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>, <w n="34.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="34.7">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> ?</l>
							<l n="35" num="3.3">— <w n="35.1">J</w>’<w n="35.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.3">l</w>’<w n="35.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="35.5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="35.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.8">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="36" num="3.4"><w n="36.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="36.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="36.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="36.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="36.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> !</l>
						</lg>
					</div>
				</div></body></text></TEI>