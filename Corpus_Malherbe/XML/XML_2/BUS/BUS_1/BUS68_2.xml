<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RELIQUIÆ</head><div type="poem" key="BUS68">
					<head type="main">TROUVÉ SUR UN CARNET DE 1860</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">t</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">d</w>’<w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.7">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">j</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="3.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="3.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.7">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="5" num="1.5"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="5.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="5.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="5.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="5.6">m<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">L</w>’<w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="7" num="1.7"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="7.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="7.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.7">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">D</w>’<w n="8.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.5">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="9.2">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="9.3">l</w>’<w n="9.4"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="9.5">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="10" num="2.2"><w n="10.1">R<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="10.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rt<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="11" num="2.3"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="11.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="11.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="11.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.6">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="12" num="2.4"><space unit="char" quantity="8"></space><w n="12.1">S</w>’<w n="12.2"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.4">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="13" num="2.5"><w n="13.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.2">l</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="13.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.6">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="13.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="14" num="2.6"><space unit="char" quantity="8"></space><w n="14.1">L</w>’<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="15" num="2.7"><w n="15.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="15.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.6">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="15.7">t</w>’<w n="15.8"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="16" num="2.8"><space unit="char" quantity="8"></space><w n="16.1">J<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="16.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="16.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="17.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="17.3">c<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="17.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.6">v<seg phoneme="ø" type="vs" value="1" rule="248">œu</seg>x</w>,</l>
						<l n="18" num="3.2"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="18.2">qu</w>’<w n="18.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="18.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="18.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="18.7">s</w>’<w n="18.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="19" num="3.3"><w n="19.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="19.3">m</w>’<w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="19.6">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="20" num="3.4"><space unit="char" quantity="8"></space><w n="20.1">Qu</w>’<w n="20.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="21" num="3.5"><w n="21.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="21.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="21.6">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
						<l n="22" num="3.6"><w n="22.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="22.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="22.3">m<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w> <w n="22.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.6">tr<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="23" num="3.7"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="23.2">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="23.3">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="23.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="23.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w></l>
						<l n="24" num="3.8"><space unit="char" quantity="8"></space><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="24.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="24.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.4"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
				</div></body></text></TEI>