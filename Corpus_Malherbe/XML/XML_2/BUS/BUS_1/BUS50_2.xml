<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHOSES D’AUTREFOIS</head><div type="poem" key="BUS50">
					<head type="main">LE TIMBRE D’OR</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="1.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="1.6">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.8">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.9">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !…</l>
						<l n="2" num="1.2"><w n="2.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">d</w>’<w n="2.3"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="2.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">H<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>, <w n="2.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.7">G<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="2.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.9">N<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>,</l>
						<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="3.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4">St<seg phoneme="a" type="vs" value="1" rule="340">a</seg>dl<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="3.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6">H<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.8">D<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rv<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>,</l>
						<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">gr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">d</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="4.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.8">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="5.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> !… <w n="5.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="5.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">fl<seg phoneme="y" type="vs" value="1" rule="445">û</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="5.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="5.9">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.10">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> !</l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> <w n="6.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.4">s<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>x</w>, <w n="6.5">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="360">en</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="7" num="2.3"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="7.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="7.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="7.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">ch<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="7.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.8">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w></l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w>-<w n="8.2">t</w>-<w n="8.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="8.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>ç<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.8">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.9">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.10"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="9.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="9.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="9.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.7">l</w>’<w n="9.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>.</l>
						<l n="10" num="3.2">— <w n="10.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.3">Br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>hm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.6">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>, <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="11.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="11.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="12.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.3">r<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="12.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.5">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="12.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="12.7">v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="12.8">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.9">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
						<l n="13" num="4.2"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.3">G<seg phoneme="y" type="vs" value="1" rule="448">u</seg>zl<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.5">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.10">d</w>’<w n="13.11"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
						<l n="14" num="4.3"><w n="14.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.3">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.4">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.7">l</w>’<w n="14.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.9">v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<p>Extrait du <hi rend="ital">Tombeau de Théophile Gautier</hi>.</p>
				</div></body></text></TEI>