<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUR LES CHEMINS</head><head type="sub_part">IMPRESSIONS DE VOYAGE</head><head type="sub_part">PORTUGAL-ESPAGNE</head><div type="poem" key="BUS10">
					<head type="number">IX</head>
					<head type="main">LA TOUR DE BÉLEM</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="1.2">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sc<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> <w n="1.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">G<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">P<seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.8">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ils</w> <w n="2.9">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
						<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">r<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="3.3">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.4">M<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.8">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w>,</l>
						<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.4">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w>, <w n="4.5">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="4.6">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="4.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.9">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.3">B<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="361">e</seg>m</w> ; <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="5.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="5.7">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8">l</w>’<w n="5.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.10">s</w>’<w n="5.11"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="6.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w>, <w n="6.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.5">l</w>’<w n="6.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.7">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.9">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w>.</l>
						<l n="7" num="2.3"><w n="7.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">R<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="7.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.8">pl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="7.9">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="7.10"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.11">f<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="8.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.4">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="8.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="8.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.9">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="9.2">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="9.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="9.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="9.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="10" num="3.2"><w n="10.1">N<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="10.3">qu</w>’<w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> ! <w n="10.5">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.6">R<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="10.7">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="10.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.9">s</w>’<w n="10.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w></l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="11.2">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="11.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.4">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5">j<seg phoneme="o" type="vs" value="1" rule="318">au</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="11.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.8">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.9">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">D</w>’<w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.6">d</w>’<w n="12.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.9">s<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w></l>
						<l n="13" num="4.2"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="13.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>ill<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="13.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.8">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="14" num="4.3"><w n="14.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sc<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> <w n="14.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="14.5">l</w>’<w n="14.6">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.7">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<closer>
						<placeName>Bélem</placeName>.
					</closer>
				</div></body></text></TEI>