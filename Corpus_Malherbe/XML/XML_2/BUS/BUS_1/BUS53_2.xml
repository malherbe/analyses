<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHOSES D’AUTREFOIS</head><div type="poem" key="BUS53">
					<head type="main">LES DIEUX ONT PRONONCÉ</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.2">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.4">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> : <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.6">G<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="1.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.8">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.4">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.6">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="2.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="3.2">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="3.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">pr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">V<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w>, <w n="4.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.4">n</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="4.7">qu</w>’<w n="4.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.9">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w></l>
						<l n="5" num="1.5"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="5.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="5.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8">l</w>’<w n="5.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w></l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="6.3">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="6.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="7.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">l</w>’<w n="7.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="7.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="7.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.9">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>, <w n="7.10">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.11">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="7.12">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w>.</l>
						<l n="8" num="2.2"><w n="8.1">C</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.3">Cl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="8.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5">pr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="8.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.8">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ks</w> <w n="8.9">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>,</l>
						<l n="9" num="2.3"><space unit="char" quantity="8"></space><w n="9.1">C</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.3">K<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl</w> <w n="9.4">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>bd<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="10" num="2.4"><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.3">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.5">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="10.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>ps</w></l>
						<l n="11" num="2.5"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="11.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="11.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="11.9"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.10">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w></l>
						<l n="12" num="2.6"><space unit="char" quantity="8"></space><w n="12.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="12.5">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">C</w>’<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.3">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>scl<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="13.4">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rs</w>, <w n="13.5">N<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="13.6">c</w>’<w n="13.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.8">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w></l>
						<l n="14" num="3.2"><w n="14.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.4">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="14.5">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="14.6">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="14.7">ch<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="14.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.9">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w></l>
						<l n="15" num="3.3"><space unit="char" quantity="8"></space><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="15.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5">f<seg phoneme="y" type="vs" value="1" rule="445">û</seg>t</w> <w n="15.6">R<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="16" num="3.4"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="16.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="16.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.6">gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
						<l n="17" num="3.5"><w n="17.1">D<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t</w> <w n="17.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.3">p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>x</w> <w n="17.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="17.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="17.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.8">L<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="18" num="3.6"><space unit="char" quantity="8"></space><w n="18.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="18.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.6">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
				</div></body></text></TEI>