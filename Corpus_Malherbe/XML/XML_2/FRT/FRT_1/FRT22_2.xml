<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BOURDEAU DES NEUF PUCELLES</title>
				<title type="medium">Édition électronique</title>
				<author key="FRT">
					<name>
						<forename>Charles-Théophile</forename>
						<surname>FERET</surname>
					</name>
					<date from="1858" to="1928">1858-1928</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1111 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le Bourdeau des neuf pucelles</title>
						<author>Charles-Théophile Feret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/66781</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Bourdeau des neuf pucelles</title>
								<author>Charles-Théophile Feret</author>
								<imprint>
									<pubPlace>CAUDÉRAN-BORDEAUX</pubPlace>
									<publisher>ÉDITIONS DES CAHIERS LITTÉRAIRES</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1912">1912</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et les notes de l’éditeur n’ont pas été reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).
				</p>
				<p>Les notes de bas de page (numérotation conforme à l’édition imprimée) ont été reportées en fin de poème.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-12-09" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-12-10" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Vers pour <lb></lb>LES SERVANTES</head><div type="poem" key="FRT22">
					<head type="main">LES Servantes de Pénélope</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">F<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.3">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="1.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>x<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="?" type="va" value="1" rule="162">en</seg>t</w> <w n="2.4">d</w>’<w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="2.7">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="3.2">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="3.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.7">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="3.8">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>. <w n="3.9">Cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="4.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="4.5">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>, <w n="5.4">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt</w> <w n="6.3">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
						<l n="7" num="2.3"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="7.5">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="7.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.8">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.2">s<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="8.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.5">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="9.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.4">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="9.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6">p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="9.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.8">br<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="9.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.10">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.11">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1"><seg phoneme="y" type="vs" value="1" rule="450">U</seg>l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="10.3">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="10.4"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w>,</l>
						<l n="11" num="3.3"><w n="11.1">M<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rc</w> <w n="11.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.7">J<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.9">H<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w></l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.2">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.4">r<seg phoneme="i" type="vs" value="1" rule="480">i</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>, <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.6">s</w>’<w n="13.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="14.3">ch<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.5">Pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w></l>
						<l n="15" num="4.3"><w n="15.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.2">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="15.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>, <w n="15.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="15.7">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="15.8">fr<seg phoneme="o" type="vs" value="1" rule="435">o</seg>tt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="15.9">d</w>’<w n="15.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="16.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.2">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="17.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.4">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="17.5">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="17.6">t<seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="17.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="17.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.9">l<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">S<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rgu<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="18.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.7">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="19.2"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="19.3">r<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4">V<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="19.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.6">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.7"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ff<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">Em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.3">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="20.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="21.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="21.6">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="21.7">f<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="21.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="21.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.10">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="22.2">l</w>’<w n="22.3"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.4">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="22.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="22.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="22.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
						<l n="23" num="6.3"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.3">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="23.4">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="23.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="23.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="23.8">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">L</w>’<w n="24.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="24.3">d</w>’<w n="24.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="24.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="25.2">f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="25.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.4">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="25.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="25.6">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="25.7"><seg phoneme="ø" type="vs" value="1" rule="405">Eu</seg>r<seg phoneme="i" type="vs" value="1" rule="493">y</seg>cl<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space><w n="26.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="26.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="26.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="26.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="26.5">l</w>’<w n="26.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
						<l n="27" num="7.3"><w n="27.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="27.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.4">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="27.5">d</w>’<w n="27.6"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="27.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="27.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.9">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.10"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">M<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.4">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>