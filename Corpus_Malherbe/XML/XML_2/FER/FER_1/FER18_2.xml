<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MÉLODIES POÉTIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1014 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Mélodies poétiques</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/albertferlandmelodiespoetiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Mélodies poétiques</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://books.google.fr/books?id=TBENAAAAYAAJ</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Pierre J. Bédard, Imprimeur-relieur</publisher>
									<date when="1893">1893</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MÉLANCOLIES</head><div type="poem" key="FER18">
					<head type="main">LES CŒURS</head>
					<opener>
						<salute>À MA MÈRE</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="1.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.7">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.9">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.4">pl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="2.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="2.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.8">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>,</l>
						<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="3.4">m<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="3.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="3.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="18"></space><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">n</w>’<w n="4.3"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="4.4">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="w" type="sc" value="0" rule="431">Ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="5.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="5.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="5.5">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>v<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="5.8">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> !…</l>
						<l n="6" num="2.2"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="6.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="6.7"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="6.8">l</w>’<w n="6.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.10">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="6.11">qu</w>’<w n="6.12"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="6.13"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>,</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="7.2">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>v<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="7.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="7.4">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.7">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg>s</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="18"></space><w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="8.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="9.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="9.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="9.6">qu</w>’<w n="9.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.8"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.9">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.10">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w></l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">qu</w>’<w n="10.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="10.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>ps</w> <w n="10.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="10.9">f<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="10.10">s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="vi" value="1" rule="242">e</seg>nt</w>,</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="11.2"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="11.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="11.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="18"></space><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="242">e</seg>nt</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.9"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="14" num="4.2"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="14.3">qu</w>’<w n="14.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rç<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="14.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="14.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="14.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.9">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="14.10">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.11">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>,</l>
						<l n="15" num="4.3"><w n="15.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>, <w n="15.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="15.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="15.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>, <w n="15.6"><seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="15.7">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.8">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>v<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="15.9"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.10"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="18"></space><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="17.2">qu</w>’<w n="17.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="17.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.7">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="17.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.9">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.10">l</w>’<w n="17.11"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.12">pr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.13"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
						<l n="18" num="5.2"><w n="18.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="18.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="18.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="18.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="18.6">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="18.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="18.9">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.10">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>,</l>
						<l n="19" num="5.3"><w n="19.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.5"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="19.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="19.7">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="19.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
						<l n="20" num="5.4"><space unit="char" quantity="18"></space><w n="20.1">C</w>’<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.3">qu</w>’<w n="20.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="20.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> !…</l>
					</lg>
				</div></body></text></TEI>