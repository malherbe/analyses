<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES SONNETS DU DOCTEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="CMS">
					<name>
						<forename>Georges</forename>
						<surname>CAMUSET</surname>
					</name>
					<date from="1840" to="1885">1840-1885</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>522 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CMS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/BIUSante_80646</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Sonnets du docteur</title>
								<author>Georges Camuset</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Chez la plupart des libraires</publisher>
									<date when=" 1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
						<idno type="URL">https ://www.google.fr/books/edition/Les_sonnets_du_docteur/bsv60_A0jdUC</idno>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Chez la plupart des libraires</publisher>
							<date when=" 1884">1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poémes absents de l’édition numérique initiale ("Les Préservatifs" et "Du Signe certain de la mort") ont été ajoutés à partir du texte disponible sur Google books.</p>
				<p>Les notes qui suivent les titres dans la table des matières ont été mis en fin de poème.</p>
				<p>La lettre manuscrite de Charles Monselet n’est pas incluse dans la présente édition</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CMS22">
				<head type="main">PHTHIRIASE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">R<seg phoneme="ɔ" type="vs" value="1" rule="441">O</seg>M<seg phoneme="ə" type="vi" value="1" rule="348">E</seg></w> <w n="1.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.3">s</w>’<w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="1.6">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="1.7">d</w>’<w n="1.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.9">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="1.10">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="2.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="2.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="2.6">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="2.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.9">l</w>’<w n="2.10"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg>xt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="3.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.8">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.4">r<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.6">gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.9">pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.7">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8">f<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="6.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="6.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.8">l</w>’<w n="6.9"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="6.10">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w>.</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="ɔ" type="vs" value="1" rule="443">O</seg>r</w>, <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.3">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="7.5">d</w>’<w n="7.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.7">Phth<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="7.8">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
					<l n="8" num="2.4"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.3">pr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="8.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.8">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : « <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="9.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> !</l>
					<l n="10" num="3.2">« <w n="10.1">T<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w>-<w n="10.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="10.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="10.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="10.7"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w>, <w n="10.8">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="10.9">h<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="11" num="3.3">« <w n="11.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.7">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="11.8">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1">« <w n="12.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="12.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ccl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.7">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="12.9">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> ?</l>
					<l n="13" num="4.2">« <w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="13.2">f<seg phoneme="y" type="vs" value="1" rule="445">û</seg>t</w>-<w n="13.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="13.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="13.7">qu</w>’<w n="13.8">H<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.9">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="14" num="4.3">« <w n="14.1">N<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="14.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">m</w>’<w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.7">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! »</l>
				</lg>
			</div></body></text></TEI>