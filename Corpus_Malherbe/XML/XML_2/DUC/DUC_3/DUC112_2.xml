<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Troisième partie</head><head type="sub_part">(1871-1885)</head><div type="poem" key="DUC112">
					<head type="main">Revoyons le Code</head>
					<div type="section" n="1">
						<head type="main">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="1.3">tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
							<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.4">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
							<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ! <w n="3.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="3.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="4" num="1.4"><w n="4.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="4.2">d</w>’<w n="4.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> !</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="5.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="5.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.4">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="6" num="2.2"><w n="6.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="6.5">l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w></l>
							<l n="7" num="2.3"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.2">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="7.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.5">s</w>’<w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="8" num="2.4"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="8.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="8.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="9.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.4">h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>ct<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="10" num="3.2"><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.2">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="10.3">qu</w>’<w n="10.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.6">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.7">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>,</l>
							<l n="11" num="3.3"><w n="11.1">L</w>’<w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="11.3">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="12" num="3.4"><w n="12.1">L</w>’<w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.3">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="12.5">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> !</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">F<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ts</w> <w n="13.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.6">r<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
							<l n="14" num="4.2"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.2">ch<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w> <w n="14.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">pr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="14.6">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
							<l n="15" num="4.3"><w n="15.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.4">cr<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
							<l n="16" num="4.4"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="16.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.3">f<seg phoneme="ɛ̃" type="vs" value="1" rule="303">aim</seg></w> <w n="16.4">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.6">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="17.4">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>, <w n="17.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.6"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="17.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
							<l n="18" num="5.2"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="18.2">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="18.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.4">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="18.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="18.6">h<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
							<l n="19" num="5.3"><w n="19.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="19.2">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="19.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="19.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="19.5">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="19.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="19.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.8">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
							<l n="20" num="5.4"><w n="20.1">L</w>’<w n="20.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.3">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.5">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> !</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="main">II</head>
						<lg n="1">
							<l n="21" num="1.1"><w n="21.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="21.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="21.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="21.4">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="21.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="22" num="1.2"><w n="22.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="22.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <hi rend="ital"><w n="22.3">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w></hi>, <w n="22.4">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="22.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
							<l n="23" num="1.3"><w n="23.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="23.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="23.5">s</w>’<w n="23.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="24" num="1.4"><w n="24.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="24.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="24.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="24.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="24.6">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="25" num="2.1"><w n="25.1">C<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>-<w n="25.2">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="25.3">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="25.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.6">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
							<l n="26" num="2.2">— « <w n="26.1">D<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="26.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
							<l n="27" num="2.3"><w n="27.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> !… » <w n="27.3"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="27.4">l</w>’<w n="27.5">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.6">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="28" num="2.4"><w n="28.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="28.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="28.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="28.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
						</lg>
						<lg n="3">
							<l n="29" num="3.1"><w n="29.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>, <w n="29.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="30" num="3.2"><w n="30.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="30.2">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <hi rend="ital"><w n="30.3">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w></hi> <w n="30.4">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="30.5">l</w>’<w n="30.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>…</l>
							<l n="31" num="3.3"><w n="31.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> ! <w n="31.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.3">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
							<l n="32" num="3.4"><w n="32.1">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.2">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="32.3">j<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="32.5">r<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> !</l>
						</lg>
						<lg n="4">
							<l n="33" num="4.1"><w n="33.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="33.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.3">Tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="33.4">s</w>’<w n="33.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
							<l n="34" num="4.2"><w n="34.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="34.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="34.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="34.5">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="34.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="34.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
							<l n="35" num="4.3"><w n="35.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="35.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="35.3">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>, <w n="35.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
							<l n="36" num="4.4"><w n="36.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> : — « <w n="36.2">C<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="36.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="36.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="36.5">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ! »</l>
						</lg>
						<lg n="5">
							<l n="37" num="5.1"><w n="37.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="37.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.3">s<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="37.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="37.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>sh<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="38" num="5.2"><w n="38.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="38.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="38.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w> <w n="38.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
							<l n="39" num="5.3"><w n="39.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="39.2">l</w>’<w n="39.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="39.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="39.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="40" num="5.4"><w n="40.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="40.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="40.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.5"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
						</lg>
						<lg n="6">
							<l n="41" num="6.1"><w n="41.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="41.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="41.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.4">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="41.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.6">n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="42" num="6.2"><w n="42.1">C</w>’<w n="42.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="42.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="42.4">l<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="42.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="42.6">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
							<l n="43" num="6.3"><w n="43.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r</w> <w n="43.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="43.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="43.4">f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r</w> <w n="43.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="43.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="44" num="6.4"><w n="44.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="44.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="44.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="44.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
						</lg>
						<lg n="7">
							<l n="45" num="7.1"><w n="45.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="45.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ! <w n="45.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="45.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="45.5">C<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="46" num="7.2"><w n="46.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="46.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ph<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="46.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> ;</l>
							<l n="47" num="7.3"><w n="47.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="47.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="47.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="47.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="47.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="47.6">d</w>’<w n="47.7">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="48" num="7.4"><w n="48.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="48.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="48.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="48.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> !</l>
						</lg>
					</div>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1880">1880</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>