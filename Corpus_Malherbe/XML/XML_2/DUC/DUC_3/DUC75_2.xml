<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE PARTIE</head><head type="sub_part">(1867-1870)</head><div type="poem" key="DUC75">
					<head type="main">Assassinat de Victor Noir</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> ! — <w n="1.4">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="1.5">b<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.7">B<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.8">t<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="2.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.7">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ?</l>
						<l n="3" num="1.3"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.7">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="4.2">g<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="4.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">C<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>, <w n="4.5">Br<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="4.6">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.7">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="5" num="1.5"><w n="5.1">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rb<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>, <w n="5.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="5.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">v<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
						<l n="6" num="1.6"><w n="6.1">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>, <w n="6.2">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t</w> <w n="6.3">b<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.5">B<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">t<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.2"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="7.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="7.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="7.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="7.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>-<w n="7.8">t</w>-<w n="7.9"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="7.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dv<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ?</l>
						<l n="8" num="2.2"><w n="8.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>-<w n="8.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ? <w n="8.5">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="8.7">b<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.9">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ?</l>
						<l n="9" num="2.3"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="9.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="9.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.8">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="9.9">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="9.10">d</w>’<w n="9.11">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="10" num="2.4"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="10.2">d</w>’<w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="10.5">n</w>’<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="10.7">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.8">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="10.9">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.10">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.11">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ?</l>
						<l n="11" num="2.5"><w n="11.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ! <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.3">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="11.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="11.8">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.9">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.10">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> !</l>
						<l n="12" num="2.6"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="12.2"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="12.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="12.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="12.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="12.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.7">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>-<w n="12.8">t</w>-<w n="12.9"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="12.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dv<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ?</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="13.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.3">N<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ! <w n="13.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="13.6">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="13.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="13.9">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> !</l>
						<l n="14" num="3.2"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="14.2">V<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="14.3">B<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.5">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="14.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="14.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="14.8">h<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> !</l>
						<l n="15" num="3.3">— « <w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="15.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="15.3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ; <w n="15.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.5">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="15.6">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.8">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> ?</l>
						<l n="16" num="3.4"><w n="16.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="16.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.3">r<seg phoneme="e" type="vs" value="1" rule="160">e</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>lv<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="16.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="16.6">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="16.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="16.8">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.9">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>. »</l>
						<l n="17" num="3.5"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="17.2">N<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="17.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="17.5">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="17.6">d</w>’<w n="17.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="17.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="17.9">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> !</l>
						<l n="18" num="3.6"><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="18.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.3">N<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>…, <w n="18.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="18.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="18.6">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="18.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="18.9">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="19.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="19.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.5">dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="19.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.7">t<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="19.8">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="19.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> !</l>
						<l n="20" num="4.2"><w n="20.1">V<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="20.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="20.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="20.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="20.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.8"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
						<l n="21" num="4.3"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="21.2">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="21.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pl<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="21.4">s</w>’<w n="21.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cqu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="21.6">d</w>’<w n="21.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="21.8">m<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="22" num="4.4"><w n="22.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="22.2">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="22.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.4">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="22.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="22.7">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="22.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w>,</l>
						<l n="23" num="4.5"><w n="23.1">S<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="23.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.3">l<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="23.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="23.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
						<l n="24" num="4.6"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="24.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="24.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="24.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.5">dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="24.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.7">t<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="24.8">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="24.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ?</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="25.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="25.4">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="25.5">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="25.7">t<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="25.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="25.9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="26" num="5.2"><w n="26.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="26.2">l</w>’<w n="26.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="26.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="26.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="26.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w>.</l>
						<l n="27" num="5.3"><w n="27.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="27.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="27.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="27.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="27.6">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ; <w n="27.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.8">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.9">f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="27.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.11">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.12">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w>,</l>
						<l n="28" num="5.4"><w n="28.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="28.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="28.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="28.6">qu</w>’<w n="28.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <hi rend="ital"><w n="28.8">D<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="28.9">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></hi>,</l>
						<l n="29" num="5.5"><w n="29.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="29.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.3">r<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="29.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="29.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.8">P<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>…</l>
						<l n="30" num="5.6"><w n="30.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.2">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="30.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="30.4">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="30.5">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="30.7">t<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="30.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="30.9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1870">10 Janvier 1870 — 5 h. du soir</date>.
							Écrit <placeName>au café des Variétés</placeName>
								en apprenant <lb></lb>
								la nouvelle de l’assassinat de notre jeune ami Victor Noir.
						</dateline>
					</closer>
				</div></body></text></TEI>