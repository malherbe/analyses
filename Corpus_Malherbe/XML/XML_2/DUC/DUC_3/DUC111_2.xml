<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Troisième partie</head><head type="sub_part">(1871-1885)</head><div type="poem" key="DUC111">
					<head type="main">Les Prussiens en Alsace-Lorraine</head>
					<lg n="1">
						<head type="main">LES SOLDATS</head>
						<l n="1" num="1.1"><w n="1.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.2">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="1.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="2.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<lg n="2">
						<head type="main">UNE ALSACIENNE</head>
						<l n="3" num="2.1"><w n="3.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.2">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="3.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="3.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.5">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="4" num="2.2"><w n="4.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">d<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ;</l>
						<l n="5" num="2.3"><w n="5.1">B<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> ! <w n="5.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">n</w>’<w n="5.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="5.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.7">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<lg n="3">
						<head type="main">LES SOLDATS</head>
						<l n="6" num="3.1"><w n="6.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.2">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="6.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="7" num="3.2"><w n="7.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.2">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> ! — <w n="7.5">Qu</w>’<w n="7.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
					</lg>
					<lg n="4">
						<head type="main">UNE LORRAINE</head>
						<l n="8" num="4.1"><w n="8.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ? <w n="8.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> ? <w n="8.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="9" num="4.2"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="9.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3"><seg phoneme="y" type="vs" value="1" rule="391">eu</seg>t</w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.5">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ;</l>
						<l n="10" num="4.3"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ! <w n="10.2">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">n</w>’<w n="10.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="10.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.7">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<lg n="5">
						<head type="main">LES SOLDATS</head>
						<l n="11" num="5.1"><w n="11.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.2">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="11.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="12" num="5.2"><w n="12.1">V<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="12.4">qu</w>’<w n="12.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>… <ref type="noteAnchor" target="(1)">(1)</ref></l>
					</lg>
					<lg n="6">
						<head type="main">UNE ALSACIENNE</head>
						<l n="13" num="6.1"><w n="13.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="13.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xh<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="14" num="6.2"><w n="14.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.4">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="14.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ;</l>
						<l n="15" num="6.3"><w n="15.1">V<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> ! |<w n="15.2"><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3">n</w>’<w n="15.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="15.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.7">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					</lg>
					<lg n="7">
						<head type="main">LES SOLDATS</head>
						<l n="16" num="7.1"><w n="16.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.2">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="16.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="17" num="7.2"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="17.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="17.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="17.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.5">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="17.6">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> !</l>
					</lg>
					<lg n="8">
						<head type="main">LES ENFANTS D’ALSACE-LORRAINE</head>
						<l n="18" num="8.1"><w n="18.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="18.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="18.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w> <w n="18.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> !</l>
						<l n="19" num="8.2"><w n="19.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="19.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="19.3">l</w>’<w n="19.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="?" type="va" value="1" rule="162">en</seg>t</w> <w n="19.5">T<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="20" num="8.3"><w n="20.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="20.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="20.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="20.5">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="20.6">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1871">9 décembre 1871</date>.
						</dateline>
						<note type="footnote" id="1">
							Les pendules, pianos, tableaux, etc., provenant du <lb></lb>
							pillage de nos maisons par les soudards de Guillaume.
						</note>
					</closer>
				</div></body></text></TEI>