<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Capricieuses</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>830 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Capricieuse</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k6101950r.r=alexandre%20ducros?rk=193134;0</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Capricieuses</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Nîmes</pubPlace>
									<publisher>IMPRIMERIE TYPOGRAPHIQUE BALDY ET ROGER</publisher>
									<date when="1854">1854</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La dédicace en début d’ouvrage n’est pas reprise dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC3">
				<head type="main">RÊVERIE</head>
				<opener>
					<salute>A Mademoiselle Emma H.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="1.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="1.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="1.5">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
					<l n="2" num="1.2"><w n="2.1">Fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="2.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="3.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">cr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.6">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="4.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>, <w n="5.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="5.6">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">j</w>’<w n="6.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="6.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>,</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="7.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">l</w>’<w n="7.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.7">j<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="8.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="8.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>,</l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="ɛ" type="vs" value="1" rule="346">E</seg>l</w> <w n="11.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="11.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
					<l n="12" num="3.4"><w n="12.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="12.4">l</w>’<w n="12.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">t</w>’<w n="13.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.5">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
					<l n="14" num="4.2"><w n="14.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="14.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="14.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5">l<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="15" num="4.3"><w n="15.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>, <w n="15.2">cr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="15.6">d</w>’<w n="15.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="16" num="4.4"><w n="16.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.2">t</w>’<w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.5">t</w>’<w n="16.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="17.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> : <w n="17.5">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="18" num="5.2"><w n="18.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! <w n="18.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.4">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="18.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> !</l>
					<l n="19" num="5.3"><w n="19.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="19.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="19.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.4">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.6">d</w>’<w n="19.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.8">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
					<l n="20" num="5.4"><w n="20.1">J<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="20.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="20.3">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="20.4">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.5">l</w>’<w n="20.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="21.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="21.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.6">s</w>’<w n="21.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>,</l>
					<l n="22" num="6.2"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="22.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="22.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="22.5">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="22.6">s</w>’<w n="22.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="23" num="6.3"><w n="23.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="23.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="23.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="24" num="6.4"><w n="24.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="24.2">j</w>’<w n="24.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="24.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.5">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rp<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="24.6">d</w>’<w n="24.7"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="25.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="25.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="25.6">t</w>’<w n="25.7"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w> <w n="25.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
					<l n="26" num="7.2"><w n="26.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="26.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="26.3">n</w>’<w n="26.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="26.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.6">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> !</l>
					<l n="27" num="7.3"><w n="27.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="27.2">l</w>’<w n="27.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="27.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.6">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>,</l>
					<l n="28" num="7.4"><w n="28.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.2">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="28.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.4">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="28.6">n</w>’<w n="28.7"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="29.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ?… <w n="29.3"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="29.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="29.5"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w>-<w n="29.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> ?</l>
					<l n="30" num="8.2"><w n="30.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="30.2">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.3">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="30.4"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="30.5">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="31" num="8.3"><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="31.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="31.3">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="31.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="31.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="31.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="32" num="8.4"><w n="32.1">S</w>’<w n="32.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="32.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="32.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="32.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>, <w n="33.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.3">t</w>’<w n="33.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="33.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.6">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.8">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="34" num="9.2"><w n="34.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="34.2">d</w>’<w n="34.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="34.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="34.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="34.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="34.7">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="35.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="35.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.4">l</w>’<w n="35.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.6">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="36" num="9.4"><w n="36.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="36.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="36.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="36.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="36.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">N</w>’<w n="37.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="37.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="37.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="37.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="37.6">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="37.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
					<l n="38" num="10.2"><w n="38.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="38.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="38.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="38.4">m</w>’<w n="38.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="38.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="38.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="38.8">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w></l>
					<l n="39" num="10.3"><w n="39.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.2">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="39.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="39.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="39.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>,</l>
					<l n="40" num="10.4"><w n="40.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.2">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="40.3">qu</w>’<w n="40.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="40.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="40.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="40.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> : <w n="40.8">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.9">t</w>’<w n="40.10"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<placeName>Nice</placeName>,
						<date when="1852">janvier 1852</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>