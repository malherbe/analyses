<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC50">
				<head type="main">Ninette<ref type="noteAnchor" target="1">(1)</ref></head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="1.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="1.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.6">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w> <w n="1.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">Em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="2.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="2.6">j<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="2.7">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">g<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.4"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.8">l</w>’<w n="3.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.2">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">d</w>’<w n="4.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.7">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ds</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.3"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.5">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w> <w n="5.6">s<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="6" num="1.6"><space unit="char" quantity="10"></space><w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="6.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="6.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
					<l n="7" num="1.7"><space unit="char" quantity="10"></space><w n="7.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
					<l n="8" num="1.8"><space unit="char" quantity="16"></space><w n="8.1">N<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="9" num="1.9"><space unit="char" quantity="16"></space><w n="9.1">N<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="10" num="1.10"><space unit="char" quantity="10"></space><w n="10.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="10.3">d</w>’<w n="10.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> !</l>
				</lg>
				<lg n="2">
					<l n="11" num="2.1"><w n="11.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>-<w n="11.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.3">fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> — <w n="11.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="11.5">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! —</l>
					<l n="12" num="2.2"><w n="12.1">L</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="12.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.4">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="12.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.7">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="12.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.9">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ?</l>
					<l n="13" num="2.3"><w n="13.1">H<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="13.2">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="13.3">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="14" num="2.4"><w n="14.1">S</w>’<w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="14.3">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.5">l</w>’<w n="14.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="14.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="14.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.11">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
					<l n="15" num="2.5"><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="15.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.4">cl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.5">cr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> : « <w n="15.8">C</w>’<w n="15.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.10">f<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! »</l>
					<l n="16" num="2.6"><space unit="char" quantity="10"></space><w n="16.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.4">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
					<l n="17" num="2.7"><space unit="char" quantity="10"></space><w n="17.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="17.2">d</w>’<w n="17.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
					<l n="18" num="2.8"><space unit="char" quantity="16"></space><w n="18.1">N<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="19" num="2.9"><space unit="char" quantity="16"></space><w n="19.1">N<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="20" num="2.10"><space unit="char" quantity="10"></space><w n="20.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="20.2">d</w>’<w n="20.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> !</l>
				</lg>
				<lg n="3">
					<l n="21" num="3.1"><w n="21.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.2">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="21.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="21.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="21.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.7">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.8">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="22" num="3.2"><w n="22.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>, <w n="22.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="22.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="23" num="3.3"><w n="23.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.3">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ds</w> <w n="23.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="23.5">l</w>’<w n="23.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="23.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="24" num="3.4"><w n="24.1">D</w>’<w n="24.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.4">d</w>’<w n="24.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="24.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="24.8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="24.9">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.10">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
					<l n="25" num="3.5"><w n="25.1">R<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.3">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="25.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.6">n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="26" num="3.6"><space unit="char" quantity="10"></space><w n="26.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.2">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="26.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="26.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
					<l n="27" num="3.7"><space unit="char" quantity="10"></space><w n="27.1">Tr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.2">d</w>’<w n="27.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
					<l n="28" num="3.8"><space unit="char" quantity="16"></space><w n="28.1">N<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="29" num="3.9"><space unit="char" quantity="16"></space><w n="29.1">N<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="30" num="3.10"><space unit="char" quantity="10"></space><w n="30.1">Tr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.2">d</w>’<w n="30.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> !</l>
				</lg>
				<lg n="4">
					<l n="31" num="4.1"><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="31.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="31.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="32" num="4.2"><w n="32.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.2">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="32.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.5">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="32.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>sf<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="32.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w></l>
					<l n="33" num="4.3"><w n="33.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="33.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="33.3">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> ; <w n="33.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="33.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.6">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="34" num="4.4"><w n="34.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg>s</w> <w n="34.4">s</w>’<w n="34.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="34.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
					<l n="35" num="4.5"><w n="35.1">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="35.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.5">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.6">c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="36" num="4.6"><space unit="char" quantity="10"></space><w n="36.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="36.2">l</w>’<w n="36.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="36.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
					<l n="37" num="4.7"><space unit="char" quantity="10"></space><w n="37.1">R<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="37.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="37.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
					<l n="38" num="4.8"><space unit="char" quantity="16"></space><w n="38.1">N<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="39" num="4.9"><space unit="char" quantity="16"></space><w n="39.1">N<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="40" num="4.10"><space unit="char" quantity="10"></space><w n="40.1">R<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="40.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="40.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> !</l>
				</lg>
				<lg n="5">
					<l n="41" num="5.1"><w n="41.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.2">r<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="41.3">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="41.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.5">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="41.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="42" num="5.2"><w n="42.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="42.2">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="42.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="42.4">l</w>’<w n="42.5"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>, <w n="42.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="42.7">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="42.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="42.9">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="42.10">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>,</l>
					<l n="43" num="5.3"><w n="43.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="43.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="43.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>ill<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="43.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="43.5">l</w>’<w n="43.6">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="43.7">br<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="43.8">d</w>’<w n="43.9">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="44" num="5.4"><w n="44.1">Pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>cl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="242">e</seg>nt</w> <w n="44.2">l</w>’<w n="44.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="44.4">l</w>’<w n="44.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="44.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> !</l>
					<l n="45" num="5.5"><w n="45.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="45.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ? <w n="45.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="45.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="45.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="45.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="46" num="5.6"><space unit="char" quantity="10"></space><w n="46.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="46.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="46.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="46.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="46.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
					<l n="47" num="5.7"><space unit="char" quantity="10"></space><w n="47.1">C<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="47.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="47.3">l</w>’<w n="47.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
					<l n="48" num="5.8"><space unit="char" quantity="16"></space><w n="48.1">N<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="49" num="5.9"><space unit="char" quantity="16"></space><w n="49.1">N<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="50" num="5.10"><space unit="char" quantity="10"></space><w n="50.1">C<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="50.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="50.3">l</w>’<w n="50.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1873">1873</date>
					</dateline>
					<note type="footnote" id="1">
						Traduction, imitée d’une poésie Catalane de mon ami, <lb></lb>
						Don Victor Balaguer, Député aux Cortès, ancien Ministre <lb></lb>
						des Colonies (Espagne) et partisan dévoué du <hi rend="ital">Félibrige</hi>.
					</note>
				</closer>
			</div></body></text></TEI>