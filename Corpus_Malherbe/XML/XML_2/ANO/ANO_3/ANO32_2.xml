<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO32">
					<head type="main">HISTOIRE VÉRITABLE <lb></lb>ET REMARQUABLE D’UN ABBÉ</head>
					<head type="sub_1">
						Qui avoit donné un rendez-vous <lb></lb>
						à une femme mariée ; le mari, <lb></lb>
						instruit de ce rendez-vous, <lb></lb>
						mit à sa chaste épouse une <lb></lb>
						ceinture fort usitée en Italie
					</head>
					<head type="tune">Air : <hi rend="ital">Tarare, pon, pon.</hi></head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.4">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm</w>’ <w n="1.5">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="6"></space><w n="2.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="2.2">N<seg phoneme="o" type="vs" value="1" rule="444">o</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="3" num="1.3"><space unit="char" quantity="6"></space><w n="3.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="3.2">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">Fl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w></l>
						<l n="5" num="1.5"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="5.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.5">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>.</l>
						<l n="6" num="1.6"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="7" num="1.7"><w n="7.1">Fl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : « <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="7.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.5">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>,</l>
						<l n="8" num="1.8">» <w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="8.2">l</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bb<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="8.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>-<w n="8.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.6">l</w>’<w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space>» <w n="9.1">C<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm</w>’ <w n="9.2">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> ? »</l>
					</lg>
					<lg n="2">
						<l n="10" num="2.1">‒ « <w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.2">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c</w><ref target="1" type="noteAnchor">1</ref> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.4">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm</w>’ <w n="10.5">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, »</l>
						<l n="11" num="2.2"><space unit="char" quantity="8"></space><w n="11.1">R<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="12" num="2.3"><space unit="char" quantity="6"></space><w n="12.1">L</w>’<w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>bb<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.3">R</w>’<w n="12.4">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="13" num="2.4">« <w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.5">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w></l>
						<l n="14" num="2.5">» <w n="14.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm</w>’ <w n="14.5">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>.</l>
						<l n="15" num="2.6">» ‒ <w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>bb<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, » <w n="15.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, « <w n="15.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="16" num="2.7">» <w n="16.1">L</w>’<w n="16.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="16.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>,</l>
						<l n="17" num="2.8">» <w n="17.1">Qu</w>’<w n="17.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="17.3"><seg phoneme="e" type="vs" value="1" rule="353">E</seg>ccl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="18" num="2.9"><space unit="char" quantity="8"></space>» <w n="18.1">C<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm</w>’ <w n="18.2">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="19" num="3.1">» <w n="19.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="19.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="19.5">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm</w>’ <w n="19.6">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>,</l>
						<l n="20" num="3.2"><space unit="char" quantity="8"></space>» <w n="20.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="20.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
						<l n="21" num="3.3"><space unit="char" quantity="6"></space>» <w n="21.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="22" num="3.4">» <w n="22.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="22.3">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="22.4">m</w>’<w n="22.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w></l>
						<l n="23" num="3.5">» <w n="23.1">M<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="23.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.4">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm</w>’ <w n="23.5">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> ;</l>
						<l n="24" num="3.6">» <w n="24.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.2">c<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.3">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="25" num="3.7">» <w n="25.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.3">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.4">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="25.5">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>,</l>
						<l n="26" num="3.8">» <w n="26.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="26.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="26.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.4">g<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="27" num="3.9"><space unit="char" quantity="8"></space>» <w n="27.1">C<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm</w>’ <w n="27.2">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>. »</l>
					</lg>
					<lg n="4">
						<l n="28" num="4.1">‒ « <w n="28.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.2">n</w>’<w n="28.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="28.4">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="28.5">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="28.6">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm</w>’ <w n="28.7">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> ;</l>
						<l n="29" num="4.2"><space unit="char" quantity="8"></space>» <w n="29.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! »</l>
						<l n="30" num="4.3"><space unit="char" quantity="6"></space><w n="30.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="30.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.3">Pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="31" num="4.4">« <w n="31.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.2">ch<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="31.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>-<w n="31.5">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> !</l>
						<l n="32" num="4.5">» <w n="32.1">G<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>n<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="32.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="32.3">c<seg phoneme="œ" type="vs" value="1" rule="389">oeu</seg>r</w> <w n="32.4">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm</w>’ <w n="32.5">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> !</l>
						<l n="33" num="4.6">» <w n="33.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="33.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.3">j</w>’<w n="33.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="33.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="33.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="34" num="4.7">» <w n="34.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.2">v<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w></l>
						<l n="35" num="4.8">» <w n="35.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="35.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.4">c<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.5">d</w>’<w n="35.6"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="36" num="4.9"><space unit="char" quantity="8"></space>» <w n="36.1">C<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm</w>’ <w n="36.2">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>. »</l>
					</lg>
					<lg n="5">
						<l n="37" num="5.1">‒ « <w n="37.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.2">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.3">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm</w>’ <w n="37.4">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, »</l>
						<l n="38" num="5.2"><space unit="char" quantity="6"></space><w n="38.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="38.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="38.3">B<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="39" num="5.3"><space unit="char" quantity="6"></space>« <w n="39.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w> <w n="39.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="40" num="5.4">» <w n="40.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="40.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="40.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="40.4">c</w>’<w n="40.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="40.6">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> !</l>
						<l n="41" num="5.5">» <w n="41.1">L</w>’<w n="41.2"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>bb<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="41.3">j</w>’<w n="41.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="41.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="41.6">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm</w>’ <w n="41.7">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>. »</l>
						<l n="42" num="5.6"><w n="42.1">L</w>’<w n="42.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bb<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="42.3">r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="42.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="42.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="43" num="5.7"><w n="43.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="43.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="43.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="43.4">s</w>’<w n="43.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="43.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>,</l>
						<l n="44" num="5.8"><w n="44.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="44.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="44.3">D<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="45" num="5.9"><space unit="char" quantity="8"></space><w n="45.1">C<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm</w>’ <w n="45.2">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>.</l>
					</lg>
					<closer>
						<note type="footnote" id="1">Marchande renommée pour <lb></lb>les odeurs et les parfums.</note>
					</closer>
				</div></body></text></TEI>