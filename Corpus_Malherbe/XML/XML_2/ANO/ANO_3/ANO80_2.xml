<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO80">
					<head type="main">LA QUESTION RÉSOLUE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="1.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="1.3">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="1.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.4">v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="2.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6">bl<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="2.8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="4" num="1.4"><w n="4.1">L</w>’<w n="4.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="4.5">l</w>’<w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ;</l>
						<l n="5" num="1.5"><w n="5.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="6" num="1.6"><w n="6.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="6.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">m<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="363">en</seg></w> <w n="6.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="7" num="1.7"><w n="7.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
						<l n="8" num="1.8"><w n="8.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="8.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="8.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.5">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="9" num="1.9"><w n="9.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="9.3">qu</w>’<w n="9.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="9.5">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="9.7"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> :</l>
						<l n="10" num="1.10"><w n="10.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="10.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="10.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="10.5">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.7">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>