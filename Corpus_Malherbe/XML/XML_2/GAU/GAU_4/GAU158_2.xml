<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU158">
				<head type="main">COLÈRE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Amende-toi, vieille au regard hideux, <lb></lb>
								Ou pour ung mot villain en auras deux.
							</quote>
							<bibl>
								<hi rend="ital">Epistre à la première vieille.</hi>
							</bibl>
						</cit>
						<cit>
							<quote>
								A Montfaucon tout sec puisse-tu pendre, <lb></lb>
								Les yeux mangéz de corbeaux charongneux, <lb></lb>
								Les pieds tiréz de ces mastins hargneux <lb></lb>
								Qui vont grondant, hérissés de furie, <lb></lb>
								Quand on approche auprès de leur voirie.
							</quote>
							<bibl>
								<name>Pierre Ronsard</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">H<seg phoneme="i" type="vs" value="1" rule="493">y</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, — <w n="1.4"><seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="1.5">c</w>’<w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.7">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="1.8">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.9">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
					<l n="2" num="1.2"><space quantity="4" unit="char"></space><w n="2.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>x<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w></l>
					<l n="3" num="1.3"><w n="3.1">J<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="3.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="3.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7">cl<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="4" num="1.4"><space quantity="4" unit="char"></space><w n="4.1">D</w>’<w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="4.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="4.5">g<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>.</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="w" type="sc" value="0" rule="431">Ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, — <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.7">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="6" num="1.6"><space quantity="4" unit="char"></space><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="6.2">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> ;</l>
					<l n="7" num="1.7"><w n="7.1"><seg phoneme="w" type="sc" value="0" rule="431">Ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, — <w n="7.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="7.5">l</w>’<w n="7.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.10">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w> <w n="7.11">s</w>’<w n="7.12"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="8" num="1.8"><space quantity="4" unit="char"></space><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="8.2">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="8.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.4">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> ;</l>
					<l n="9" num="1.9"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.2">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.5">pr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="9.7">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="9.8">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="10" num="1.10"><space quantity="4" unit="char"></space><w n="10.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="10.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="10.5">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w></l>
					<l n="11" num="1.11"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="11.2">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.8">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="11.9">j<seg phoneme="o" type="vs" value="1" rule="318">au</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="12" num="1.12"><space quantity="4" unit="char"></space><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>s<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="12.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="12.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="12.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
					<l n="13" num="1.13">— <w n="13.1">S<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cb<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>th</w>, <w n="13.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.5">d</w>’<w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.7">h<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="14" num="1.14"><space quantity="4" unit="char"></space><w n="14.1">S<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rp<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="14.3">d</w>’<w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.5">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.6">d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>,</l>
					<l n="15" num="1.15"><w n="15.1"><seg phoneme="y" type="vs" value="1" rule="450">U</seg>lc<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>, <w n="15.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.4">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="16" num="1.16"><space quantity="4" unit="char"></space><w n="16.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="16.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ge<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="16.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>,</l>
					<l n="17" num="1.17"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="17.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="17.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="17.5">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.6">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>, <w n="17.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="17.8">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="17.9">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.10">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="18" num="1.18"><space quantity="4" unit="char"></space><w n="18.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="18.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="18.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></l>
					<l n="19" num="1.19"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="19.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>, <w n="19.4">cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="19.5">L<seg phoneme="o" type="vs" value="1" rule="444">o</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="20" num="1.20"><space quantity="4" unit="char"></space><w n="20.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg>t</w> <w n="20.2">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="20.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.4">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> ;</l>
					<l n="21" num="1.21"><w n="21.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="21.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="21.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.4">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="21.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="21.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="22" num="1.22"><space quantity="4" unit="char"></space><w n="22.1">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="22.3">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sp<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.5">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w>,</l>
					<l n="23" num="1.23"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="23.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="23.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.5">p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="23.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.8">pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="24" num="1.24"><space quantity="4" unit="char"></space><w n="24.1">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="?" type="va" value="1" rule="162">en</seg>t</w> <w n="24.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.3">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="24.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="24.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w>,</l>
					<l n="25" num="1.25"><w n="25.1">D</w>’<w n="25.2"><seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="25.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>f<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="25.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="25.5">g<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="25.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="25.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.8">d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="26" num="1.26"><space quantity="4" unit="char"></space><w n="26.1">L<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="26.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="26.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="26.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>,</l>
					<l n="27" num="1.27"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="27.2">pr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="27.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="27.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="27.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="27.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="27.7">sc<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="28" num="1.28"><space quantity="4" unit="char"></space><w n="28.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.3">d</w>’<w n="28.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="28.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>,</l>
					<l n="29" num="1.29"><w n="29.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.2">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="29.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="29.4">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="29.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="29.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.7">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.8">j</w>’<w n="29.9"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="30" num="1.30"><space quantity="4" unit="char"></space><w n="30.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.2">l</w>’<w n="30.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.4">n</w>’<w n="30.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.6">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="30.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="31" num="1.31"><w n="31.1">D</w>’<w n="31.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="31.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="31.4">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="31.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="31.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w>, <w n="31.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="31.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="31.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.10">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="31.11">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>-<w n="31.12">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="32" num="1.32"><space quantity="4" unit="char"></space><w n="32.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="32.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.3">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="32.4"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.5">bl<seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
				</lg>
			</div></body></text></TEI>