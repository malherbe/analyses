<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU123">
				<head type="main">STANCES</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
							La jeune fille rieuse.
							</quote>
							<bibl>
								<name>Victor Hugo.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="1.7">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.7">s</w>’<w n="2.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.9">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>,</l>
					<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4">l</w>’<w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.8">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="3.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.10">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>,</l>
					<l n="4" num="1.4"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.4">d</w>’<w n="4.5"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>, <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.8">pr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.5">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="5.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>qu<seg phoneme="j" type="sc" value="0" rule="489">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="5.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w></l>
					<l n="6" num="2.2"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="6.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="6.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">b<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="7.3">d</w>’<w n="7.4">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">sph<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="8" num="2.4"><w n="8.1">D</w>’<w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.4">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.7">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>, <w n="9.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="10" num="3.2"><w n="10.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.2">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="10.3">qu</w>’<w n="10.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="10.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="10.9">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="11" num="3.3"><w n="11.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.5">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="11.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="11.8">d</w>’<w n="11.9"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="12" num="3.4"><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bj<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="12.4">ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="13.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="13.4">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>, <w n="13.5">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="13.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.8">g<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
					<l n="14" num="4.2"><w n="14.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="14.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="14.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.8">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="15" num="4.3"><w n="15.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="15.3">n<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>f</w>, <w n="15.4">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="15.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="15.6">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="16" num="4.4"><w n="16.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w> <w n="16.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.4">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="16.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.7">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="16.8">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="16.9">d</w>’<w n="16.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="17.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="17.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="17.7">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.8">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.9">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="18" num="5.2"><w n="18.1">Br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="18.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="18.3">d</w>’<w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>, <w n="18.5">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="18.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w></l>
					<l n="19" num="5.3"><w n="19.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ds</w> <w n="19.3">d</w>’<w n="19.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="19.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.7">cr<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="19.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ;</l>
					<l n="20" num="5.4"><w n="20.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="20.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rg<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="20.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="20.5">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>-<w n="20.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.7">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="21.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="21.3">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="21.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="21.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="21.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="21.7">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="21.8">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> ;</l>
					<l n="22" num="6.2"><w n="22.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="22.2">n</w>’<w n="22.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="22.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="22.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="22.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="22.8"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.9"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="23" num="6.3"><w n="23.1">N<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="23.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="23.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="23.4">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="24" num="6.4"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="24.2">d</w>’<w n="24.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="24.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="24.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">J<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="25.3">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="25.4"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="25.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="25.6">l</w>’<w n="25.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="26" num="7.2"><w n="26.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="26.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="26.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></l>
					<l n="27" num="7.3"><w n="27.1">N<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.2">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="27.4">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="27.5">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> <w n="27.6">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="27.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.8">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="27.9">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> ;</l>
					<l n="28" num="7.4"><w n="28.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.2">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="28.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.5">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="28.6">n</w>’<w n="28.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="28.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="28.10">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="29.2">qu</w>’<w n="29.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="29.4">r<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="29.5">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="29.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="29.7">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.8">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="29.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.10">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="30" num="8.2"><w n="30.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="30.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="30.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="30.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="30.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.7">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="30.8">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.9">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="31" num="8.3"><w n="31.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.2"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.3">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="31.5">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.6">s</w>’<w n="31.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="32" num="8.4"><w n="32.1">H<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.2">d</w>’<w n="32.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="32.4">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="32.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="32.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
			</div></body></text></TEI>