<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU126">
				<head type="main">LA BASILIQUE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								The pillared arches were over their head <lb></lb>
							And beneath their feet were the bones of the dead.
							</quote>
							<bibl>
								<hi rend="ital">The lay of last minstrel.</hi>
							</bibl>
						</cit>
						<cit>
							<quote>
								On voit des figures de chevaliers à genoux sur <lb></lb>
								un tombeau, les mains jointes… les arcades obscures <lb></lb>
								de l’église couvrent de leurs ombres ceux qui reposent.
							</quote>
							<bibl>
								<name>Göerres</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="2.2">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="2.3">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="3" num="1.3"><w n="3.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.2">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="3.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="5" num="1.5"><w n="5.1">Fl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="5.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="6.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b</w> <w n="6.5">c<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w></l>
					<l n="7" num="2.2"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="7.3">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
					<l n="8" num="2.3"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="8.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="8.6">t<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w></l>
					<l n="9" num="2.4"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="9.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w></l>
					<l n="10" num="2.5"><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.2">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ds</w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1"><w n="11.1">C<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="11.2">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="11.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="12" num="3.2"><w n="12.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="12.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.4">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
					<l n="13" num="3.3"><w n="13.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>sc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="13.4">d</w>’<w n="13.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="14" num="3.4"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="14.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="14.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.5">n<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w> <w n="14.6">gr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="15" num="3.5"><w n="15.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="15.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.3">sv<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>.</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1"><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.2">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="17" num="4.2"><w n="17.1">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rb<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.3">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="17.4">d<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w></l>
					<l n="18" num="4.3"><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="18.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>fl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="19" num="4.4"><w n="19.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2">l</w>’<w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4">g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="20" num="4.5"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="20.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.4">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>ts</w>.</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1"><w n="21.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="21.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="21.4">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="22" num="5.2"><w n="22.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="22.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="22.3">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>,</l>
					<l n="23" num="5.3"><w n="23.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="23.2">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="23.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="23.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="23.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.6">d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="24" num="5.4"><w n="24.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="24.3">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lcr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="25" num="5.5"><w n="25.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="25.2">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="25.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="25.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.5">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>.</l>
				</lg>
				<lg n="6">
					<l n="26" num="6.1"><w n="26.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="26.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="26.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="27" num="6.2"><w n="27.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="27.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="27.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="27.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></l>
					<l n="28" num="6.3"><w n="28.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="28.2">v<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="28.3">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="28.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="28.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="29" num="6.4"><w n="29.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="29.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="29.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="30" num="6.5"><w n="30.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="30.3"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="30.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.5">v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>.</l>
				</lg>
				<lg n="7">
					<l n="31" num="7.1"><w n="31.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="31.2">l</w>’<w n="31.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="31.4">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.5">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="32" num="7.2"><w n="32.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="32.2">qu</w>’<w n="32.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.5">d</w>’<w n="32.6"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
					<l n="33" num="7.3"><w n="33.1">R<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="33.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="33.5">v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="34" num="7.4"><w n="34.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="34.4">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.5">s</w>’<w n="34.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="35" num="7.5"><w n="35.1">Qu</w>’<w n="35.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="35.3">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="35.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="35.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="35.6">s</w>’<w n="35.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>.</l>
				</lg>
				<lg n="8">
					<l n="36" num="8.1"><w n="36.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="36.3">pr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="36.5">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="37" num="8.2"><w n="37.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="37.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="37.3">v<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="37.4">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu</w>’<w n="37.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="37.6">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w></l>
					<l n="38" num="8.3"><w n="38.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="38.2">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="38.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="38.4">qu</w>’<w n="38.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="38.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="39" num="8.4"><w n="39.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="39.2">qu</w>’<w n="39.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="39.4">pr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="39.6">l</w>’<w n="39.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="40" num="8.5"><w n="40.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="40.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="40.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="40.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="40.6">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ;</l>
				</lg>
				<lg n="9">
					<l n="41" num="9.1"><w n="41.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="41.2">qu</w>’<w n="41.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="41.4">l</w>’<w n="41.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="41.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.7">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="42" num="9.2"><w n="42.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="42.2">g<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="42.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="42.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="42.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="43" num="9.3"><w n="43.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="43.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="43.3">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="43.4">ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="44" num="9.4"><w n="44.1">Qu</w>’<w n="44.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="44.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="44.4">cr<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="44.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="44.6">fl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="45" num="9.5"><w n="45.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="45.2">pl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="45.3">l</w>’<w n="45.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="45.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : <hi rend="ital"><w n="45.6"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="e" type="vs" value="1" rule="GAU126_1">e</seg></w></hi>.</l>
				</lg>
				<lg n="10">
					<l n="46" num="10.1"><w n="46.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="46.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="46.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="46.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="46.5">s</w>’<w n="46.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="47" num="10.2"><w n="47.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="47.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="47.3">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="47.4">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
					<l n="48" num="10.3"><w n="48.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="48.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="48.3">d</w>’<w n="48.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="48.5">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="48.6">s</w>’<w n="48.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="49" num="10.4"><w n="49.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="49.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="49.3">d</w>’<w n="49.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="49.5">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="49.6">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="50" num="10.5"><w n="50.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="50.2">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="50.3">d</w>’<w n="50.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="50.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
				</lg>
				<lg n="11">
					<l n="51" num="11.1"><w n="51.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="51.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="51.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="51.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="51.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="51.6">cl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="52" num="11.2"><w n="52.1">Cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="52.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="52.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="52.4">l</w>’<hi rend="ital"><w n="52.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>g<seg phoneme="e" type="vs" value="1" rule="10">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w></hi>;</l>
					<l n="53" num="11.3"><w n="53.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="53.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="53.3">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="53.4">s</w>’<w n="53.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ccr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="54" num="11.4"><w n="54.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="54.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="54.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="54.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="54.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="55" num="11.5"><w n="55.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="55.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="55.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="55.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="55.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="55.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> :</l>
				</lg>
				<lg n="12">
					<l n="56" num="12.1"><w n="56.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="56.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="56.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="56.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="57" num="12.2"><w n="57.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="57.2">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="57.3">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="57.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="57.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="58" num="12.3"><w n="58.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="58.2">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="58.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="58.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="58.5">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="59" num="12.4"><w n="59.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="59.2">l</w>’<w n="59.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="59.4">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="60" num="12.5"><w n="60.1">Fl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="60.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="60.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="60.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>