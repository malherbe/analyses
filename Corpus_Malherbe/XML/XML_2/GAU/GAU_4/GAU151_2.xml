<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU151">
				<head type="main">ENFANTILLAGE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Hanneton, vole, vole, vole.
							</quote>
							<bibl>
								<hi rend="ital">Ballade des petites filles.</hi>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.3">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4">pl<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="1.6">s</w>’<w n="1.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="2.5">g<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="2.6">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.8">b<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="2.9"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="2.10">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">En</seg>n<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="3.2">d</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.5">g<seg phoneme="i" type="vs" value="1" rule="468">î</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.9">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.10">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3">m<seg phoneme="wa" type="vs" value="1" rule="421">oi</seg>n<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="4.4">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w>, <w n="4.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="4.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.8">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="5.2">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, — <w n="5.3"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="5.4">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="5.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.7">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="5.8">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w>-<w n="5.9">G<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
					<l n="6" num="2.2"><w n="6.1">C<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="6.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="6.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.5">l</w>’<w n="6.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="7.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.4">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.6">g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.9">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="8" num="2.4"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="8.2">l</w>’<w n="8.3">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.6">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.8">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="8.9">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.10">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">M<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.4">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.7">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.8">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
					<l n="10" num="3.2">— <w n="10.1">D</w>’<w n="10.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">d</w>’<w n="10.5"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="10.6">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="10.7"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="10.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.10">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.11">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>,</l>
					<l n="11" num="3.3"><w n="11.1">S<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.4">sc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="11.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="11.7">d</w>’<w n="11.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> ;</l>
					<l n="12" num="3.4"><w n="12.1">D</w>’<w n="12.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.7">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="12.8">d</w>’<w n="12.9"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.10">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="12.11">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">D</w>’<w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.3">br<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="13.4">d</w>’<w n="13.5">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="13.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.8">V<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.10">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="13.11">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.12">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
					<l n="14" num="4.2">— <w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="14.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.3">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="14.5">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.6">j</w>’<w n="14.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.10">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="15" num="4.3"><w n="15.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc</w> <w n="15.4">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w>-<w n="15.5">F<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rge<seg phoneme="o" type="vs" value="1" rule="314">au</seg></w>, <w n="15.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="15.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.8">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.9">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="16" num="4.4"><w n="16.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">Em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="16.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="16.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="16.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.5">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="16.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.7">h<seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
				</lg>
			</div></body></text></TEI>