<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU124">
				<head type="main">PROMENADE NOCTURNE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Allons, la belle nuit d’été,
							</quote>
							<bibl>
								<name>Alfred de Musset</name>.
							</bibl>
						</cit>
						<cit>
							<quote>
								C’était par un beau soir, par un des soirs que rêve <lb></lb>
								Au murmure lointain d’un invisible accord <lb></lb>
								Le poète qui veille ou l’amante qui dort.
							</quote>
							<bibl>
								<name>Victor Pavie</name>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="2" num="1.2"><w n="2.1">Sc<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="2.3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.5">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.5">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="4" num="1.4"><w n="4.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.3">l</w>’<w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="4.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="4.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="5.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="5.6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="6" num="2.2"><w n="6.1">Br<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="6.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="6.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="6.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.6">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ;</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="7.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="7.3">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rb<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.6">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="8" num="2.4"><w n="8.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.5">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sm<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="9.4">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="9.5">d</w>’<w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="10" num="3.2"><w n="10.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="10.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.5">r<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="10.6">bl<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w></l>
					<l n="11" num="3.3"><w n="11.1">S</w>’<w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.5">fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="12" num="3.4"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="12.2">d<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="12.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4">l</w>’<w n="12.5"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="12.6">h<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">J<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.3">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="13.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="13.6">d</w>’<w n="13.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="14" num="4.2"><w n="14.1">N</w>’<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.3">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="14.6">d</w>’<w n="14.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>,</l>
					<l n="15" num="4.3"><w n="15.1">N<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.3">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gt</w>, <w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr</w>’<w n="15.5"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="16" num="4.4"><w n="16.1">M<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="16.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="16.4">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="16.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.7">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="16.8">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="17.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.3">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="17.4"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="17.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.6">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>-<w n="17.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="18" num="5.2"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="18.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="18.5">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="18.6">j<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r</w></l>
					<l n="19" num="5.3"><w n="19.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="20" num="5.4"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="20.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="20.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.5">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="20.6"><seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>r</w></l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="21.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="21.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="21.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.7">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="22" num="6.2"><w n="22.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="22.2">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="22.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.4">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="22.5">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
					<l n="23" num="6.3"><w n="23.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="23.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="23.3">l</w>’<w n="23.4"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="23.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="23.6">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="24" num="6.4"><w n="24.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="24.3">l</w>’<w n="24.4"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.6">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>.</l>
				</lg>
			</div></body></text></TEI>