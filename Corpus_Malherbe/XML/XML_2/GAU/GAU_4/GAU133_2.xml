<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU133">
				<head type="main">VEILLÉE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Je lis les faits joyeux du bon Pantagruel, <lb></lb>
								Je sais presque par cœur l’histoire véritable <lb></lb>
								Des quatre fils Aymon et de Robert-le-Diable.
							</quote>
							<bibl>
								<name>Grandval</name>, <hi rend="ital">le Vice puni</hi>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="1.4">cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.4">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="2.6">j<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="2.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="2.8">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w></l>
					<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="3.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w>, <w n="3.4">qu</w>’<w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.7">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="3.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">P<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="4.5">d</w>’<w n="4.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.7">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="5" num="1.5"><w n="5.1">D</w>’<w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.3">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="5.4">bl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.6">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.8">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.10">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.11">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rtr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w></l>
					<l n="6" num="1.6"><w n="6.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="6.3">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="6.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.5">gr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.7">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">S<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w>, <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="7.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.5">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="7.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.7">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="7.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.9">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="8" num="1.8"><w n="8.1">D</w>’<w n="8.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="8.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.6">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="8.7">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="8.8">v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ?</l>
					<l n="9" num="1.9"><w n="9.1">C</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.6">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> : <w n="9.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="9.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.10">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.11">ch<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
					<l n="10" num="1.10"><w n="10.1">J</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="10.3">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="10.5">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="10.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.7">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w> <w n="10.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>-<w n="10.9">q<seg phoneme="wa" type="vs" value="1" rule="159">ua</seg>rt<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>,</l>
					<l n="11" num="1.11"><w n="11.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="11.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.8">g<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>th<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="12" num="1.12"><w n="12.1">D<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="12.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="12.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="12.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="12.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="13" num="1.13"><w n="13.1">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">d</w>’<w n="13.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>, <w n="13.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.6">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="14.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="a" type="vs" value="1" rule="307">a</seg>il</w>, <w n="14.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="14.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>,</l>
					<l n="15" num="1.15"><w n="15.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="15.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>. <w n="15.6">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="15.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="15.8">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="16" num="1.16"><w n="16.1">L<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="16.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w> <w n="16.4">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="17" num="1.17"><w n="17.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.2">p<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="17.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.4">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="17.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.7">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="17.8">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="18" num="1.18"><w n="18.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="18.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.5">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> ; <w n="18.6">chr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="18.7">d</w>’<w n="18.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w> <w n="18.9">pr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="19" num="1.19"><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.2"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="19.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="19.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> ; <w n="19.5">c</w>’<w n="19.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="19.8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="19.9">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.10">l</w>’<w n="19.11">h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="20" num="1.20"><w n="20.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="20.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>z<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.4">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="20.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.7">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="20.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="21" num="1.21"><w n="21.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="21.2">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="21.3">bl<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="21.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.5">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="21.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.7">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w></l>
					<l n="22" num="1.22"><w n="22.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="22.2">m</w>’<w n="22.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ; <w n="22.4">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.5">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> : <w n="22.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="22.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.8">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.9"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="22.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
					<l n="23" num="1.23"><w n="23.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="23.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="23.5">c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="23.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.7">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="24" num="1.24"><w n="24.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="24.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="24.3">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="24.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="24.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="24.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.8">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="25" num="1.25"><w n="25.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="25.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="25.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="25.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="25.7">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="25.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.9">l</w>’<w n="25.10"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>,</l>
					<l n="26" num="1.26"><w n="26.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.2">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="26.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="26.4">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.5">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="26.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.8">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="26.9">r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
				</lg>
			</div></body></text></TEI>