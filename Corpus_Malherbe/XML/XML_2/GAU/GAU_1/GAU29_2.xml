<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU29">
				<head type="main">LES NÉRÉIDES</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>q<seg phoneme="wa" type="vs" value="1" rule="186">ua</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">B<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.3">d</w>’<w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.5">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="2.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w></l>
					<l n="3" num="1.3"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.3">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.6">qu<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="4" num="1.4">— <w n="4.1">Th<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ph<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">Kn<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="o" type="vs" value="1" rule="444">o</seg>wsk<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.6">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="6" num="2.2"><w n="6.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="6.3">gl<seg phoneme="o" type="vs" value="1" rule="318">au</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w></l>
					<l n="7" num="2.3"><w n="7.1">S<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">gr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.4">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="8" num="2.4"><w n="8.1">Tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="8.2">n<seg phoneme="ɛ̃" type="vs" value="1" rule="494">ym</seg>ph<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="8.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="8.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.5">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s</w> <w n="9.4">n<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">h<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="10" num="3.2"><w n="10.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.4">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">d</w>’<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w></l>
					<l n="11" num="3.3"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="11.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="11.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="11.5">qu</w>’<w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.7">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="12" num="3.4"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="12.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bm<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="13.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="13.3">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.4">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="13.5">c<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="14" num="4.2"><w n="14.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>cl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.2">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>l<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>, <w n="15.3">c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.4">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="16" num="4.4"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.5">fl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.7"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="17.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.3">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="17.4">l</w>’<w n="17.5">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">î</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="17.7">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="18" num="5.2"><w n="18.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="18.5">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w></l>
					<l n="19" num="5.3"><w n="19.1">L<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="19.2">g<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="19.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="19.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.5">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="19.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="19.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="20" num="5.4"><w n="20.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="20.2">d</w>’<w n="20.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="20.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="20.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="21.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="21.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="21.4">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="21.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="22" num="6.2"><w n="22.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="22.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.3">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="22.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.5">Tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="22.6">n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="23" num="6.3"><w n="23.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="23.2">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>, <w n="23.3">d</w>’<w n="23.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="24" num="6.4"><w n="24.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="24.2">l</w>’<w n="24.3"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="24.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="24.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="24.7">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w> <w n="24.8">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="25.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="25.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="25.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="25.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.6">l</w>’<w n="25.7"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="25.8">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="26" num="7.2"><w n="26.1">S<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.2">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.3">d</w>’<w n="26.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>squ<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="26.6">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
					<l n="27" num="7.3"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="27.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.4">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="27.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="27.6">qu<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="28" num="7.4"><w n="28.1">M<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="28.2">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="28.3">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="28.4">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="29.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="29.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.5">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ge<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="30" num="8.2"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="30.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.3">r<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="30.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="30.5">sq<seg phoneme="wa" type="vs" value="1" rule="187">ua</seg>m<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="30.6">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="31" num="8.3"><w n="31.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="31.2">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="31.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="31.4">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="31.5">d</w>’<w n="31.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="32" num="8.4"><w n="32.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="32.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="32.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="32.6">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ?</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="33.2">l</w>’<w n="33.3">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,— <w n="33.4">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="33.5">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="34" num="9.2"><w n="34.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.2">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="34.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.5">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, —</l>
					<l n="35" num="9.3"><w n="35.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="35.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="35.3">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="35.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="35.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="36" num="9.4"><w n="36.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.2">ch<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="36.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="36.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="37.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="37.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="37.4">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="38" num="10.2"><w n="38.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="38.2">t<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="38.3">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="38.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="38.5">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ;</l>
					<l n="39" num="10.3"><w n="39.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="39.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="39.3">f<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="39.4">l</w>’<w n="39.5"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="39.6">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="40" num="10.4"><w n="40.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="40.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="40.3">n<seg phoneme="ɛ̃" type="vs" value="1" rule="494">ym</seg>ph<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="40.4">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="40.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.6">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="41.2">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="41.4">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="41.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="41.6">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="42" num="11.2"><w n="42.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="42.2">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="42.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="42.4">l</w>’<w n="42.5"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>,</l>
					<l n="43" num="11.3"><w n="43.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="43.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="43.3">d<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ph<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>, <w n="43.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="43.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="43.6">cr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="44" num="11.4"><w n="44.1">D</w>’<w n="44.2"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="44.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="44.4">l</w>’<w n="44.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="45.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="45.3">ste<seg phoneme="a" type="vs" value="1" rule="145">a</seg>m</w>-<w n="45.4">b<seg phoneme="o" type="vs" value="1" rule="58">oa</seg>t</w> <w n="45.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="45.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="45.7">r<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="46" num="12.2"><w n="46.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="46.2">V<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lc<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="46.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="46.4">V<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
					<l n="47" num="12.3"><w n="47.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="47.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="47.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="47.4">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="48" num="12.4"><w n="48.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="48.2">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rtr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="48.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="48.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="48.5">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="49.2">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="49.3">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>th<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="50" num="13.2"><w n="50.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="50.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>b<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="50.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="50.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w>, <w n="50.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="50.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>,</l>
					<l n="51" num="13.3"><w n="51.1">Cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="51.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="51.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="51.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="51.5">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="51.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="52" num="13.4"><w n="52.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="52.2">c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lb<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="52.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="52.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rs<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
				</lg>
			</div></body></text></TEI>