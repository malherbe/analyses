<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU35">
				<head type="main">LES JOUJOUX DE LA MORTE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>il</w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="2.7">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w></l>
					<l n="3" num="1.3"><w n="3.1">Qu</w>’<w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="3.3">t<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="3.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="3.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.8">l</w>’<w n="3.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.5">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="6" num="2.2"><w n="6.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">l</w>’<w n="6.3">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
					<l n="7" num="2.3"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.2">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="7.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>, <w n="7.4">l</w>’<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="8" num="2.4"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="8.3">g<seg phoneme="i" type="vs" value="1" rule="468">î</seg>t</w> <w n="8.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="9.7">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="10" num="3.2"><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.4">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.7">b<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ;</l>
					<l n="11" num="3.3"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.3"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="11.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6">g<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="12" num="3.4"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="12.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="12.3">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>fl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">d<seg phoneme="i" type="vs" value="1" rule="467">î</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="14" num="4.2"><w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w> <w n="14.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="14.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.3">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rç<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="16" num="4.4"><w n="16.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="16.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.2">b<seg phoneme="wa" type="vs" value="1" rule="420">oî</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="17.4">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="17.6">m<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="18" num="5.2"><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="18.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="18.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.6">r<seg phoneme="ə" type="vi" value="1" rule="351">e</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w></l>
					<l n="19" num="5.3"><w n="19.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="19.2">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.3">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="19.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="19.6">fl<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="20" num="5.4"><w n="20.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="20.2">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="20.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="20.5">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">L</w>’<w n="21.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.3">ch<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>vr<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="21.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="22" num="6.2"><w n="22.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> : <hi rend="ital"><w n="22.2"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="22.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="22.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>-<w n="22.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="22.6">m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> ?</hi></l>
					<l n="23" num="6.3"><w n="23.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <hi rend="ital"><w n="23.2">Qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg>dr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.4">L<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w></hi> <w n="23.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="24" num="6.4"><w n="24.1">Tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="25.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.3">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="25.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.5">m<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="25.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.7">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="26" num="7.2"><w n="26.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <hi rend="ital"><w n="26.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.3">D<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.4"><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg></w> <w n="26.5">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="e" type="vs" value="1" rule="GAU35_1">e</seg></w></hi>,</l>
					<l n="27" num="7.3"><w n="27.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="27.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="27.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="27.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="27.7">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="28" num="7.4"><w n="28.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>xp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="28.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="28.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="29.3">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.4">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="29.6">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.7">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="30" num="8.2"><w n="30.1">P<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="30.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="31" num="8.3"><w n="31.1">J<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="31.2">d</w>’<w n="31.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="31.4">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="31.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="31.6">l</w>’<w n="31.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="32" num="8.4"><w n="32.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="32.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="32.7">cr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !</l>
				</lg>
			</div></body></text></TEI>