<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU305">
				<head type="main">FRAGMENTS</head>
				<head type="sub_1">Intercalés dans l’opéra : <hi rend="ital">Maître Wolfram</hi></head>
				<div type="section" n="1">
					<head type="number">I</head>
					<head type="main">WOLFRAM</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.6">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="2.3">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> <w n="2.5">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="2.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.7">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="3.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="3.7">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.9">m</w>’<w n="3.10"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="4.2">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="4.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> : <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="4.6">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.8">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
						<l n="5" num="1.5"><w n="5.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="5.7">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="6" num="1.6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="6.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="6.8">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.9">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="7" num="1.7"><w n="7.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="7.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="7.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.5">W<seg phoneme="i" type="vs" value="1" rule="497">i</seg>lh<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lm</w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.7">d</w>’<w n="7.8">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="8" num="1.8"><w n="8.1">S</w>’<w n="8.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.4">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="8.7">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>.</l>
					</lg>
					<p>(Montrant l’orgue.)</p>
					<lg n="2">
						<l n="9" num="2.1"><space quantity="20" unit="char"></space><w n="9.1">D<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="10" num="2.2"><space quantity="12" unit="char"></space><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="10.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="10.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="10.5">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="11" num="2.3"><space quantity="20" unit="char"></space><w n="11.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.3">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="12" num="2.4"><space quantity="12" unit="char"></space><w n="12.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> ;</l>
						<l n="13" num="2.5"><space quantity="12" unit="char"></space><w n="13.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="14" num="2.6"><space quantity="12" unit="char"></space><w n="14.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.4">s</w>’<w n="14.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="15" num="2.7"><space quantity="12" unit="char"></space><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="353">E</seg>ff<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.4">v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="16" num="2.8"><space quantity="12" unit="char"></space><w n="16.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="16.3">cr<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="16.4">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<head type="main">HÉLÈNE</head>
					<head type="form">COUPLETS</head>
					<div type="subsection" n="1">
						<head type="number">1</head>
						<lg n="1">
							<l n="17" num="1.1"><space quantity="10" unit="char"></space><w n="17.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.2">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="17.3"><seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>r</w> <w n="17.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="17.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.6">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w></l>
							<l n="18" num="1.2"><space quantity="30" unit="char"></space><w n="18.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>,</l>
							<l n="19" num="1.3"><space quantity="10" unit="char"></space><w n="19.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="19.3">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.6">l</w>’<w n="19.7"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="20" num="1.4"><space quantity="10" unit="char"></space><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.2">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="20.3">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="20.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.6">s<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w></l>
							<l n="21" num="1.5"><space quantity="30" unit="char"></space><w n="21.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="21.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
							<l n="22" num="1.6"><space quantity="10" unit="char"></space><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="22.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.3">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="22.4">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
							<l n="23" num="1.7"><space quantity="10" unit="char"></space><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="23.2">c</w>’<w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="23.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="23.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="23.7">s</w>’<w n="23.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						</lg>
					</div>
					<div type="subsection" n="2">
						<head type="number">2</head>
						<lg n="1">
							<l n="24" num="1.1"><space quantity="10" unit="char"></space><w n="24.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="24.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
							<l n="25" num="1.2"><space quantity="30" unit="char"></space><w n="25.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="25.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>,</l>
							<l n="26" num="1.3"><space quantity="10" unit="char"></space><w n="26.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.2">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="26.3">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.4"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="26.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="27" num="1.4"><space quantity="10" unit="char"></space><w n="27.1">G<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.2"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="27.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="27.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="27.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
							<l n="28" num="1.5"><space quantity="30" unit="char"></space><w n="28.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="28.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="28.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
							<l n="29" num="1.6"><space quantity="10" unit="char"></space><w n="29.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="29.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="29.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.5">v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
							<l n="30" num="1.7"><space quantity="10" unit="char"></space><w n="30.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="30.2">c</w>’<w n="30.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="30.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="30.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="30.7">s</w>’<w n="30.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						</lg>
					</div>
				</div>
				<closer>
					<dateline>
						<date when="1854">1854</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>