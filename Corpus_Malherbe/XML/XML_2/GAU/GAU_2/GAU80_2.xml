<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La comédie de la mort</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5120 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">GAU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Comédie de la mort</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>inlibroveritas.net</publisher>
						<idno type="URL">http://www.inlibroveritas.net</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Comédie de la mort</title>
						<author>Théophile Gautier</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k70716q.r=th%C3%A9ophile+gautier.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>DESESSART, ÉDITEUR</publisher>
							<date when="1838">1838</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2014-04-11" who="RR">Un vers faux dans GAU54 (Thébaïde), vers 85 a été corrigé avec l’édition Bartillat (préparée par Michel Brix)</change>
			<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GAU80">
				<head type="main">VERSAILLES</head>
				<head type="form">SONNET</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">V<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.3">n</w>’<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w> <w n="1.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="1.6">qu</w>’<w n="1.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.8">sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ctr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.10">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
					<l n="2" num="1.2"><w n="2.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">V<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="2.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="2.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>dr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="3.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="3.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="3.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ds</w> <w n="4.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.7">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="4.8">sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lpt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>, <w n="5.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
					<l n="6" num="2.2"><w n="6.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.2">n</w>’<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w> <w n="6.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.7">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.8">n</w>’<w n="6.9"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w> <w n="6.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="6.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="7.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.9">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="8" num="2.4"><w n="8.1">N<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="8.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.7">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.5">l</w>’<w n="9.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w>, <w n="9.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="10" num="3.2"><w n="10.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="10.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="10.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="10.5">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.7">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="10.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="11" num="3.3"><w n="11.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.2">gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="11.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.7">r<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="11.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="12.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="12.5">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="12.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.8">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> ;</l>
					<l n="13" num="4.2"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.2"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="13.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.5">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.7">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.8">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="13.10">t<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="14" num="4.3"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.3">n</w>’<w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="14.5">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="14.6">qu</w>’<w n="14.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.8">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.10">st<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>