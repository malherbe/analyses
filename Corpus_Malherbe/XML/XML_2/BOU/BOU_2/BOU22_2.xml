<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU22">
				<head type="main">A UNE FEMME</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> ! <w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.3">r<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>, <w n="1.5">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="1.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> : <w n="1.8">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.9">t</w>’<w n="1.10"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> ! <w n="2.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="2.5">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !… <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="2.8">qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="2.9">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ?</l>
					<l n="3" num="1.3"><w n="3.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="3.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.7">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.9">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>-<w n="3.10">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="4.3">l</w>’<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="4.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.6">n</w>’<w n="4.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.8">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.9">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.5">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w>, <w n="5.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="5.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.9">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="6" num="2.2"><w n="6.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.4">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="6.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="6.7">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.8">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="6.9">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> :</l>
					<l n="7" num="2.3"><w n="7.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">j</w>’<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.6">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="7.7">c</w>’<w n="7.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.10">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>pr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.11"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="8" num="2.4"><w n="8.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">j</w>’<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.6">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="8.7">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.8">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.9">l</w>’<w n="8.10"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="8.11">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="8.12">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.3">n</w>’<w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.5">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="9.6">qu</w>’<w n="9.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pr<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.10">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="10" num="3.2"><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="10.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="10.6">n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="10.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.8">C<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>,</l>
					<l n="11" num="3.3"><w n="11.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301">ai</seg>s</w> <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="11.5">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.7">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="11.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.10"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="12" num="3.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.5">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="12.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="12.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="12.8">d</w>’<w n="12.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.10">s</w>’<w n="12.11"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.2">n</w>’<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="13.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="13.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.8">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="13.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.10">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="13.11">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Qu</w>’<w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.3">b<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="14.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>str<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="14.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="14.8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="15.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="15.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.6">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="15.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="15.8">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="15.9">cr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="15.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.11">gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="16" num="4.4"><w n="16.1">J</w>’<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="16.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="16.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.11">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">S</w>’<w n="17.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="17.3">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="17.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.6">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>, <w n="17.7">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.8">n</w>’<w n="17.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="17.10">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="17.11">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.12"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="18" num="5.2"><w n="18.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.2">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="18.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="18.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.8">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.9">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="18.11">n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ;</l>
					<l n="19" num="5.3"><w n="19.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="19.2">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="19.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.4">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.6">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="19.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ph<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="20" num="5.4"><w n="20.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="20.2">m</w>’<w n="20.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="20.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.6">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! <w n="20.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="20.8">m</w>’<w n="20.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.10">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="20.11">d</w>’<w n="20.12"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="21.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ! <w n="21.4">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="21.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.6">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="21.7">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="22" num="6.2"><w n="22.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.2">d</w>’<w n="22.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="22.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="22.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>scr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="22.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.7">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="22.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.10">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> ;</l>
					<l n="23" num="6.3"><w n="23.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.2">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="23.4">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="23.5">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="23.6">j</w>’<w n="23.7"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="23.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="23.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.10">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="24" num="6.4"><w n="24.1">S</w>’<w n="24.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="24.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="24.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="24.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="24.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="24.9">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.10">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> !</l>
				</lg>
			</div></body></text></TEI>