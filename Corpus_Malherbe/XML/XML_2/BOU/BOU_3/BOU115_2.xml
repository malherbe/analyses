<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU115">
				<head type="number">XLVIII</head>
				<head type="main">PLUIE VENUE DU MONT KI-CHAN</head>
				<head type="sub_1">(SONG-TCHI-OUEN)</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.6">pl<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="1.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="1.9">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="2.3">s</w>’<w n="2.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="2.5">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="2.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="3.4">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="3.9">v<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="4" num="1.4"><w n="4.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>, <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="4.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="4.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="4.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.8">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="4.9">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="5.3">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="5.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ccr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.8">l</w>’<w n="5.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467">î</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="6.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>z<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">m</w>’<w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>z<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="6.8"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="6.9">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
					<l n="7" num="2.3"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="7.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.4">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="7.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.7">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="8" num="2.4"><w n="8.1">J</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="8.3">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5">l<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="8.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="8.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="9.2">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="9.4">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5">t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w>, <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="9.8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.9"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nh<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
					<l n="10" num="3.2"><w n="10.1">J</w>’<w n="10.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.4"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="10.5">f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r</w> <w n="10.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.7">l</w>’<w n="10.8"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
					<l n="11" num="3.3"><w n="11.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="11.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="12" num="3.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.3">c<seg phoneme="œ" type="vs" value="1" rule="389">oeu</seg>r</w> <w n="12.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pr<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.7">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
				</lg>
			</div></body></text></TEI>