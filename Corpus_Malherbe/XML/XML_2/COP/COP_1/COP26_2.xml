<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PROMENADES ET INTÉRIEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1057 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">COP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">PROMENADES ET INTÉRIEURS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ebooksgratuits.com</publisher>
						<idno type="URL">http://www.ebooksgratuits.com/</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Promenades et intérieurs</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Lemerre</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La date de création est celle donnée par l’Académie Française pour le recueil "Les Humbles"</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><div type="poem" key="COP26">
					<head type="main">Cheval de Renfort</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="1.3">qu</w>’<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.5">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="1.6">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.8">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="2.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="2.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.5">tr<seg phoneme="o" type="vs" value="1" rule="435">o</seg>tt<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="2.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.7">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="2.8">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rg</w> <w n="2.9">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.2">qu</w>’<w n="3.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.4">l</w>’<w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.7">fl<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.9">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="3.10"><seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>.</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.3">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="4.5">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>vr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="4.8">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rb<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
						<l n="5" num="1.5"><w n="5.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="5.8">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="6" num="1.6"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="6.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="6.3">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="6.4">d</w>’<w n="6.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.7">gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="7" num="1.7"><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w> <w n="7.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="7.4">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.6">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="7.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="7.8">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
						<l n="8" num="1.8"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="8.2">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="8.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.7">dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="9" num="1.9"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="9.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="9.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
						<l n="10" num="1.10"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.4">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="10.5">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.7"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>