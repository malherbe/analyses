<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PROMENADES ET INTÉRIEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1057 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">COP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">PROMENADES ET INTÉRIEURS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ebooksgratuits.com</publisher>
						<idno type="URL">http://www.ebooksgratuits.com/</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Promenades et intérieurs</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Lemerre</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La date de création est celle donnée par l’Académie Française pour le recueil "Les Humbles"</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><div type="poem" key="COP25">
					<head type="main">Croquis de banlieue</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="1.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.10">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="1.11">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="2.2">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>, <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="2.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.8">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
						<l n="3" num="1.3"><w n="3.1">T<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">g<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.4">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="4.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.4">l</w>’<w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.8">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.2">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="5.3">l</w>’<w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.6">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>, <w n="5.7">l</w>’<w n="5.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.9">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ç<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.10">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.11">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gt</w>.</l>
						<l n="6" num="1.6"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="6.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.7">qu</w>’<w n="6.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.9">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.10">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.6">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="7.7">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.9">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="8" num="1.8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.4">s</w>’<w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.7">d<seg phoneme="i" type="vs" value="1" rule="467">î</seg>n<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.9"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.10">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.11">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="9" num="1.9"><w n="9.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="9.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.6">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w> – <w n="9.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> ? <w n="9.9"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.10">Cl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> ! –</l>
						<l n="10" num="1.10"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="10.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>, <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="10.5">tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="10.6">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="10.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.8">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>.</l>
					</lg>
				</div></body></text></TEI>