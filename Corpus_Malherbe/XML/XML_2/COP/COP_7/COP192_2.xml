<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE RELIQUAIRE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>421 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2019</date>
				<idno type="local">COP_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE RELIQUAIRE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/francoiscoppeelereliquaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>François Coppée</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie ALEXANDRE HOUSSIAUX</publisher>
									<date when="1885">1885</date>
								</imprint>
								<biblScope unit="tome">Poésie, tome 1</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1866">1866</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-05-25" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-05-25" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP192">
				<head type="main"><hi rend="ital">ET NUNC ET SEMPER</hi></head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.2">l</w>’<w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="1.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="1.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="1.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.9">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="1.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.11">c<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="2" num="1.2"><w n="2.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="2.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="2.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.7">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.9">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ils</w>,</l>
					<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="3.2">j</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="3.4">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="3.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w>, <w n="3.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ils</w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="4.2">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="4.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.5">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ph<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.8">Ph<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.4">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>, <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.6">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="5.7">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ds</w>, <w n="5.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.9">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="5.10">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w></l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="6.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="6.3">n</w>’<w n="6.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="6.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="6.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>fl<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="6.9">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="7.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.6">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="8" num="2.4"><w n="8.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="8.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w> <w n="9.4">fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.6">qu</w>’<w n="9.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.8">ch<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="9.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.10">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rp<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="10.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="10.5">s</w>’<w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="10.8">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="10.9"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w>,</l>
					<l n="11" num="3.3"><w n="11.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.3"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="11.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg></w> <w n="11.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.7">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="11.8">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="11.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w></l>
					<l n="12" num="3.4"><w n="12.1">D<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="12.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="12.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.6">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rp<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="341">Â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.2">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.4">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="13.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.6">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="13.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.8">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
					<l n="14" num="4.2"><w n="14.1">M<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">h<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="14.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w>, <w n="14.5">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ</w>’<w n="14.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="14.7">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="14.8">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.9">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="15" num="4.3"><w n="15.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.2">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="15.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="15.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.8">l</w>’<w n="15.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="16" num="4.4"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3">j</w>’<w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.6">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="16.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="16.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.9">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.10">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> ?</l>
				</lg>
			</div></body></text></TEI>