<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CAHIER ROUGE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1198 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CAHIER ROUGE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeelecahierrouge.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 2</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP77">
				<head type="main">A un lilas</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="1.3">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>, <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.7">f<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="2.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.6">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="2.7">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.6">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">V<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="4.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="4.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="4.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="5.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="5.5">d</w>’<w n="5.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="6" num="2.2"><w n="6.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="6.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.5">c<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.7">m</w>’<w n="6.8"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.5">l</w>’<w n="7.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rb<seg phoneme="y" type="vs" value="1" rule="450">u</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="8" num="2.4"><w n="8.1">D</w>’<w n="8.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r</w> <w n="8.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.4">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.6">d</w>’<w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="360">en</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="9.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.7">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="10" num="3.2"><w n="10.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.5">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>,</l>
					<l n="11" num="3.3"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="11.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="11.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.8">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.9">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="12" num="3.4"><w n="12.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg>s</w> <w n="12.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">N<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="13.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="13.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6">j</w>’<w n="13.7"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="13.8">c<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.9">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.10"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="14" num="4.2"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.2">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>ls</w> <w n="14.3">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="14.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="14.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="14.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ?</l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="15.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="15.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.5">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.7">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="16" num="4.4"><w n="16.1">Qu</w>’<w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="16.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.6">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.7">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ?</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">— <w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="17.3">j</w>’<w n="17.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="17.5">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>, <w n="17.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="17.7">j</w>’<w n="17.8"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.10">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="18" num="5.2"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="18.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="18.4">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="18.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.6">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.7">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ;</l>
					<l n="19" num="5.3"><w n="19.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="19.2">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="19.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>, <w n="19.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="19.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.6">l</w>’<w n="19.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="20" num="5.4"><w n="20.1">Fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="20.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>, <w n="20.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="20.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="21.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="21.4">qu</w>’<w n="21.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.7">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="22" num="6.2"><w n="22.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.2">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w> <w n="22.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="22.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="22.5">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="22.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="22.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="22.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rts</w> !</l>
					<l n="23" num="6.3"><w n="23.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="23.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="23.3">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="23.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.6">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="24" num="6.4"><w n="24.1">Qu</w>’<w n="24.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.4">m</w>’<w n="24.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="24.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="24.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="24.9">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="25.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="25.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="25.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="25.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="25.7">d</w>’<w n="25.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="26" num="7.2"><w n="26.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.3">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="26.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="26.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="26.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.7">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="27" num="7.3"><w n="27.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="27.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.4"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="27.5">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="27.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="28" num="7.4"><w n="28.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="28.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="28.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.6">r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.7">d</w>’<w n="28.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="29.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.4">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="29.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="29.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="29.7">l</w>’<w n="29.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="30" num="8.2"><w n="30.1">J</w>’<w n="30.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="30.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>, <w n="30.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="30.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="30.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="30.8">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>,</l>
					<l n="31" num="8.3"><w n="31.1">D<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="31.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="31.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="31.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="31.5">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="31.7">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="31.8">d</w>’<w n="31.9"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="32" num="8.4"><w n="32.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="32.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg></w> <w n="32.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="32.5">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.7">s<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>.</l>
				</lg>
			</div></body></text></TEI>