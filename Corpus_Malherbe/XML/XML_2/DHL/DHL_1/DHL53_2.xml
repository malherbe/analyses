<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS</head><div type="poem" key="DHL53">
					<head type="main">Sur Monsieur l’abbé Testu</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">L</w>’<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>V<seg phoneme="ɑ̃" type="vs" value="1" rule="212">EN</seg>T<seg phoneme="y" type="vs" value="1" rule="450">U</seg>R<seg phoneme="ə" type="vi" value="1" rule="348">E</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.4">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="1.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="2.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> ;</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.4">d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.5">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w></l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.2">s</w>’<w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="5.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.5">s</w>’<w n="5.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.5">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="7.4">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="7.5">d</w>’<w n="7.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>.</l>
					</lg>
					<lg n="2">
						<l n="8" num="2.1"><space unit="char" quantity="8"></space><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="8.2">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="8.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="9" num="2.2"><space unit="char" quantity="8"></space><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>cl<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>,</l>
						<l n="10" num="2.3"><space unit="char" quantity="8"></space><w n="10.1">R<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">l</w>’<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="11" num="2.4"><space unit="char" quantity="8"></space><w n="11.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.2">d<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="363">en</seg></w> <w n="11.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bb<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="11.5">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>.</l>
						<l n="12" num="2.5"><space unit="char" quantity="8"></space><w n="12.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.2">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>, <w n="12.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.5">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>,</l>
						<l n="13" num="2.6"><space unit="char" quantity="8"></space><w n="13.1">S<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> : <w n="13.3">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>-<w n="13.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.6">c<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ff<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="14" num="2.7"><space unit="char" quantity="8"></space><w n="14.1">Qu</w>’<w n="14.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="14.6">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="14.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.8">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>.</l>
					</lg>
					<ab type="dot">▻◅</ab>
					<lg n="3">
						<l n="15" num="3.1"><space unit="char" quantity="10"></space><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>H</w> ! <w n="15.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="15.3">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="15.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w></l>
						<l n="16" num="3.2"><space unit="char" quantity="10"></space><w n="16.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.6">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w> ?</l>
						<l n="17" num="3.3"><w n="17.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.2">n</w>’<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="17.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="17.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="17.7">d</w>’<w n="17.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="17.9">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="17.10">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="18" num="3.4"><space unit="char" quantity="8"></space><w n="18.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="18.2">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="18.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="19" num="3.5"><space unit="char" quantity="8"></space><w n="19.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="19.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="19.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.7">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w></l>
						<l n="20" num="3.6"><space unit="char" quantity="8"></space><w n="20.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.4">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w> <w n="20.5">n</w>’<w n="20.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="20.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="20.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<ab type="dot">▻◅</ab>
					<lg n="4">
						<l n="21" num="4.1"><space unit="char" quantity="18"></space><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="21.2">L<seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="21.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
						<l n="22" num="4.2"><space unit="char" quantity="10"></space><w n="22.1"><seg phoneme="ɛ" type="vs" value="1" rule="305">Ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="22.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="22.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="23" num="4.3"><space unit="char" quantity="18"></space><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.2">l</w>’<w n="23.3"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
						<l n="24" num="4.4"><space unit="char" quantity="10"></space><w n="24.1">N</w>’<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="24.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="24.4">qu</w>’<w n="24.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="24.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="25" num="4.5"><space unit="char" quantity="18"></space><w n="25.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="25.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
						<l n="26" num="4.6"><space unit="char" quantity="10"></space><w n="26.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="26.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.4">n</w>’<w n="26.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="26.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="27" num="4.7"><space unit="char" quantity="18"></space><w n="27.1">S<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="27.2">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ;</l>
						<l n="28" num="4.8"><space unit="char" quantity="10"></space><w n="28.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="28.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="28.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.4">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="28.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
						<l n="29" num="4.9"><space unit="char" quantity="10"></space><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="29.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="29.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="29.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="29.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="30" num="4.10"><space unit="char" quantity="10"></space><w n="30.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="30.2">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="30.3"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="30.4">cr<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="31" num="4.11"><space unit="char" quantity="10"></space><w n="31.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="31.2">d</w>’<w n="31.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="31.4">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="31.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>,</l>
						<l n="32" num="4.12"><space unit="char" quantity="18"></space><w n="32.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="32.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="32.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>,</l>
						<l n="33" num="4.13"><space unit="char" quantity="10"></space><w n="33.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="33.2">l</w>’<w n="33.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="33.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="33.6">qu</w>’<w n="33.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					</lg>
					<ab type="dot">▻◅</ab>
					<lg n="5">
						<l n="34" num="5.1"><w n="34.1">L<seg phoneme="ə" type="vi" value="1" rule="348">E</seg></w> <w n="34.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="34.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="34.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="34.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="34.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="34.7">s<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="34.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="35" num="5.2"><space unit="char" quantity="8"></space><w n="35.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.3">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w>, <w n="35.5"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
						<l n="36" num="5.3"><space unit="char" quantity="8"></space><w n="36.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="36.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="36.4">t<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="37" num="5.4"><w n="37.1">L</w>’<w n="37.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.3">T<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="37.4">s</w>’<w n="37.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.7">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>.</l>
						<l n="38" num="5.5"><space unit="char" quantity="8"></space><w n="38.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="38.2">p<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="38.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="38.4"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="38.5"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>ill<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="39" num="5.6"><w n="39.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.2">l</w>’<w n="39.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="39.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="39.5">qu</w>’<w n="39.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="39.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="39.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="39.9">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="39.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w>,</l>
						<l n="40" num="5.7"><w n="40.1">N</w>’<w n="40.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="40.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.5">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="40.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="40.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="40.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
						<l n="41" num="5.8"><w n="41.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="41.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="41.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="41.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="41.5">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="41.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="41.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="42" num="5.9"><space unit="char" quantity="8"></space><w n="42.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="42.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="42.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="42.4">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="42.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="42.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="42.7">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="42.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>.</l>
						<l n="43" num="5.10"><space unit="char" quantity="8"></space><w n="43.1">F<seg phoneme="ɥi" type="vs" value="1" rule="462">U</seg><seg phoneme="j" type="sc" value="0" rule="496">Y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">ON</seg>S</w> <w n="43.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="43.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="43.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
						<l n="44" num="5.11"><w n="44.1">L</w>’<w n="44.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="44.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="44.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="44.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="44.6">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="44.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="44.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="44.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="45" num="5.12"><space unit="char" quantity="8"></space><w n="45.1">T<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="45.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="45.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="45.4">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="45.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="45.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="46" num="5.13"><space unit="char" quantity="8"></space><w n="46.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ppr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="46.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="46.3">s<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="46.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="46.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="46.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>.</l>
						<l n="47" num="5.14"><space unit="char" quantity="8"></space><w n="47.1">F<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="47.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="47.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="47.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>