<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÇA</head><div type="poem" key="CRB2">
					<head type="main">ÇA ?</head>
					<opener>
						<epigraph>
							<cit>
								<quote>What ?…</quote>
								<bibl>
									<name>SHAKESPEARE</name>, <hi rend="ital"></hi>.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> ? — <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="1.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w>, <w n="1.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6">n</w>’<w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="1.9"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ? — <w n="2.2">F<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.4">n</w>’<w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="2.6">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.7">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
						<l n="3" num="1.3"><w n="3.1">V<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ? — <w n="3.2">Tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="3.3">br<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="3.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> …</l>
						<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.3">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ? — <w n="4.4">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="4.6">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.7">n</w>’<w n="4.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.10">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="5.2">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414">ë</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ? — <w n="5.3">M<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="5.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.5">j</w>’<w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.9">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="6.2">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ? — … <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="6.4">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>, <w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.8">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.9"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.10">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !…</l>
						<l n="7" num="2.3"><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> ? — <w n="7.3">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="7.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="7.5">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="7.6">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="7.7">c</w>’<w n="7.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.9">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> !</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>lb<seg phoneme="ɔ" type="vs" value="1" rule="451">u</seg>m</w> ? — <w n="8.2">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">n</w>’<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="8.6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w>, <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.8">c</w>’<w n="8.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.10">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="8.11">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">B<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ts</w>-<w n="9.2">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> ? — <w n="9.3">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="9.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="9.5">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> ?… <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.7">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.8">n</w>’<w n="9.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="9.11">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> !</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="10.2"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ? — <w n="10.3">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">n</w>’<w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.6">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.7">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.8">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
						<l n="11" num="3.3"><w n="11.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> ? — <w n="11.2">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.4">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>, <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="11.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.7">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.8">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !…</l>
						<l n="12" num="3.4"><w n="12.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="12.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> ? — <w n="12.3">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.4">cr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>, <w n="12.6">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.7">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.8">m</w>’<w n="12.9"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">— <w n="13.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> ?… <w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="13.4">fl<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="13.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>… — <w n="13.7">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="13.8">c</w>’<w n="13.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.10">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
						<l n="14" num="4.2">— <w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w>, <w n="14.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="14.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.5">l</w>’<w n="14.6"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ?…</l>
						<l n="15" num="4.3">— <w n="15.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> … <w n="15.2">c</w>’<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.5">dr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="15.7">dr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, — <hi rend="ital"><w n="15.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.9">r<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></hi> —</l>
						<l n="16" num="4.4"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt</w> <w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>, <w n="16.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="16.5">qu</w>’<w n="16.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.7">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="16.9">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">— <w n="17.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <hi rend="ital"><w n="17.2">ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c</w></hi> <w n="17.3">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> ? — <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> <w n="17.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.6">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.7">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.9">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> !</l>
						<l n="18" num="5.2">— <w n="18.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.2">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="18.3">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> ? <w n="18.4">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.5">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w>-<w n="18.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> ? — <w n="18.7">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="18.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.9">r<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="18.10">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.11">d</w>’<w n="18.12"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> !</l>
						<l n="19" num="5.3">— <w n="19.1">Ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.3">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ttr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.6">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ? — … <w n="19.7"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="19.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="19.9"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.10">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="20" num="5.4"><w n="20.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.2">t<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>. — <w n="20.3"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="20.4">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="20.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ? — <w n="20.7">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="20.8">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">— <w n="21.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="21.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.3">n</w>’<w n="21.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="21.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="21.6">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ? — <w n="21.7"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="21.8">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="21.10">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.11">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> !</l>
						<l n="22" num="6.2">— <w n="22.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ? — <w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">Ai</seg></w>-<w n="22.3">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.4">l</w>’<w n="22.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="22.6">d</w>’<w n="22.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="22.8">m<seg phoneme="œ" type="vs" value="1" rule="151">on</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>r</w> <w n="22.9"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="22.10">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> ?</l>
						<l n="23" num="6.3"><w n="23.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="23.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.3">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ? — <w n="23.4">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.5">n</w>’<w n="23.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="23.8">qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="23.10">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.11">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
						<l n="24" num="6.4"><w n="24.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="24.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.3">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ? — <w n="24.4"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="24.5">l</w>’<w n="24.6"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="24.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.8">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="24.9">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.10">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.11">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">… <hi rend="ital"><w n="25.1">Ç<seg phoneme="a" type="vs" value="1" rule="340">A</seg></w></hi> <w n="25.2">c</w>’<w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="25.4">n<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="25.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <hi rend="ital"><w n="25.7">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></hi> ;</l>
						<l n="26" num="7.2"><w n="26.1">C</w>’<w n="26.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>, <w n="26.3"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="26.4">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.5">n</w>’<w n="26.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="26.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <hi rend="ital"><w n="26.8">ç<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w></hi> : <w n="26.9">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="26.10"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="26.11">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.12">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
						<l n="27" num="7.3">— <w n="27.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="27.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w>-<w n="27.3">d</w>’<w n="27.4"><seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ? — <w n="27.5"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="27.6">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.7">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> : <w n="27.8">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.9">n</w>’<w n="27.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="27.11"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="27.12">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="27.13">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>.</l>
						<l n="28" num="7.4">— <w n="28.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="28.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="28.3">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="28.5">h<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="28.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="28.7">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="28.8"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="28.9">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="28.10">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> ?</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">— <w n="29.1">C</w>’<w n="29.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="29.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> … <w n="29.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="29.5">j</w>’<w n="29.6"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="29.7">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="29.8">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="29.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.10">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.11">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="29.12">d</w>’<w n="29.13"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
						<l n="30" num="8.2"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="30.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="30.4">n</w>’<w n="30.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="30.7">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="30.9">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.10">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
						<l n="31" num="8.3"><w n="31.1">C</w>’<w n="31.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="31.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="31.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="31.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.6">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ccr<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c</w>, <w n="31.7">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.8"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="31.9">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w>, <w n="31.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="31.11">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>…</l>
						<l n="32" num="8.4"><w n="32.1">L</w>’<w n="32.2"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rt</w> <w n="32.3">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.4">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.5">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="32.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>. <w n="32.7">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.8">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.9">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="32.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="32.11">l</w>’<w n="32.12"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rt</w>.</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Préfecture de police</placeName>,
							<date when="1873">20 mai 1873</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>