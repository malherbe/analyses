<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SÉRÉNADE DES SÉRÉNADES</head><div type="poem" key="CRB46">
					<head type="main">CHANSON EN SI</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.2">j</w>’<w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5">F<seg phoneme="o" type="vs" value="1" rule="318">au</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="2.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>…</l>
						<l n="3" num="1.3">— <w n="3.1">T<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> : <w n="3.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
						<l n="4" num="1.4">— <w n="4.1">V<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> : <w n="4.2">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
						<l n="5" num="1.5"><space quantity="8" unit="char"></space><w n="5.1">T<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> !</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1">— <w n="6.1">Ge<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> : <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="6.3">l</w>’<w n="6.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w>…</l>
						<l n="7" num="2.2">— <w n="7.1">R<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> : <w n="7.2">f<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.4">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="7.5">tr<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w>…</l>
						<l n="8" num="2.3"><w n="8.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="8.2">j</w>’<w n="8.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.4">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="9" num="2.4"><w n="9.1">T<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">m<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
						<l n="10" num="2.5"><space quantity="8" unit="char"></space><w n="10.1">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> !</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1"><w n="11.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.2">j</w>’<w n="11.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.4">gr<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="11.5">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
						<l n="12" num="3.2"><w n="12.1">T<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2">f<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="12.4">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.5">S<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> !</l>
						<l n="13" num="3.3"><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.2">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.3">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="14" num="3.4"><w n="14.1">T<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="14.3">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
						<l n="15" num="3.5"><space quantity="8" unit="char"></space><w n="15.1">T<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>…</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1"><w n="16.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="16.2">j</w>’<w n="16.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="16.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>p<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="17" num="4.2"><w n="17.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> : « <w n="17.2">D<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="17.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.4">l</w>’<w n="17.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="17.6">l</w>’<w n="17.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="18" num="4.3"><w n="18.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="18.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.3">f<seg phoneme="ɛ̃" type="vs" value="1" rule="303">aim</seg></w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> :</l>
						<l n="19" num="4.4"><w n="19.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.2">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>-<w n="19.3">d</w>’<w n="19.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> : <w n="19.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="19.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>. »</l>
						<l n="20" num="4.5"><space quantity="8" unit="char"></space><w n="20.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="20.2">j</w>’<w n="20.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> !…</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1"><w n="21.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="21.2">j</w>’<w n="21.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.4">Fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="21.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
						<l n="22" num="5.2"><w n="22.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="22.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.3">p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="22.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w></l>
						<l n="23" num="5.3"><w n="23.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="23.2">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="23.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.4">F<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="23.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.7">P<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="24" num="5.4"><w n="24.1">L</w>’<w n="24.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="24.4">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.5">M<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
						<l n="25" num="5.5"><space quantity="8" unit="char"></space><w n="25.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> !</l>
					</lg>
					<lg n="6">
						<l n="26" num="6.1"><w n="26.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="26.2">j</w>’<w n="26.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="26.4">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="27" num="6.2"><w n="27.1">J<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="27.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>, <w n="27.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.5">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="28" num="6.3"><w n="28.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="28.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>, <w n="28.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="28.4">s<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="28.5">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
						<l n="29" num="6.4"><w n="29.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="29.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.4">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>…</l>
						<l n="30" num="6.5"><space quantity="8" unit="char"></space><w n="30.1">J<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> !</l>
					</lg>
					<lg n="7">
						<l n="31" num="7.1"><w n="31.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="31.2">j</w>’<w n="31.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="31.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="31.5">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="31.6">b<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>d<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="32" num="7.2"><w n="32.1">M<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="32.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="32.3">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="32.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>…</l>
						<l n="33" num="7.3"><w n="33.1">D</w>’<w n="33.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="33.3">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="33.4">d</w>’<w n="33.5"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="33.6">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="34" num="7.4"><w n="34.1">L</w>’<w n="34.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="34.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="35" num="7.5"><space quantity="8" unit="char"></space><w n="35.1">L</w>’<w n="35.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> !</l>
					</lg>
					<lg n="8">
						<l n="36" num="8.1"><w n="36.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="36.2">j</w>’<w n="36.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="36.4">r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>,</l>
						<l n="37" num="8.2"><w n="37.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="37.2">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="37.3">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="37.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="37.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> :</l>
						<l n="38" num="8.3"><w n="38.1">Gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="38.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="38.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="38.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="39" num="8.4"><w n="39.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>cr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.3">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="40" num="8.5"><space quantity="8" unit="char"></space><w n="40.1">Gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> !</l>
					</lg>
					<lg n="9">
						<l n="41" num="9.1"><w n="41.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="41.2">j</w>’<w n="41.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="41.4">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>… <w n="41.5"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w>, <w n="41.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="41.7">B<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="42" num="9.2"><w n="42.1">T<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="42.2">f<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="42.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="42.4">C<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>…</l>
						<l n="43" num="9.3"><w n="43.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="43.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="43.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="43.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="43.5">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w></l>
						<l n="44" num="9.4"><w n="44.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="44.2">s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="44.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="44.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="44.5">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w>…</l>
						<l n="45" num="9.5"><space quantity="8" unit="char"></space><w n="45.1">T<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="45.2">f<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>…</l>
					</lg>
					<lg n="10">
						<l n="46" num="10.1"><w n="46.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="46.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="46.3">j</w>’<w n="46.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="46.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="46.6">d<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="47" num="10.2"><w n="47.1">R<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="47.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="47.3">t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="47.4">p<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="48" num="10.3"><w n="48.1">S<seg phoneme="ə" type="vi" value="1" rule="350">E</seg>N<seg phoneme="o" type="vs" value="1" rule="444">O</seg>R<seg phoneme="a" type="vs" value="1" rule="340">A</seg></w>, <w n="48.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="48.3">j</w>’<w n="48.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="48.5">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>…</l>
						<l n="49" num="10.4"><w n="49.1">J</w>’<w n="49.2"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="49.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="49.4">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="49.5">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
						<l n="50" num="10.5"><space quantity="8" unit="char"></space> — <w n="50.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> ! —</l>
					</lg>
				</div></body></text></TEI>