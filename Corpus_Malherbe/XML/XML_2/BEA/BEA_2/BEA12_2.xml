<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES HORIZONTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Saisie du texte</resp>
					<name id="SP">
						<forename>S.</forename>
						<surname>Pestel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Relecture du texte</resp>
					<name id="AG">
						<forename>A.</forename>
						<surname>Guézou</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>404 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Horizontales</title>
						<author>Henri Beauclair</author>
					</titleStmt>
					<publicationStmt>
						<publisher>LA BIBLIOTHÈQUE ÉLECTRONIQUE DE LISIEUX</publisher>
						<pubPlace>Lisieux</pubPlace>
						<address>
							<addrLine>Place de la République</addrLine>
							<addrLine>B.P. 27216 — 14107 Lisieux cedex</addrLine>
							<addrLine>FRANCE</addrLine>
						</address>
						<idno type="URL">http://www.bmlisieux.com/archives/horizont.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Horizontales</title>
								<author>Henri Beauclair</author>
								<edition>2e édition en partie originale</edition>
								<imprint>
									<publisher>Léon Vanier, Paris</publisher>
									<date when="1886">1886</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEA12">
				<head type="number">VI</head>
				<head type="main">Gabrielle</head>
				<opener>
					<epigraph>
						<cit>
							<quote>Si je n’étais captive</quote>
							<bibl>
								<name>V. HUGO</name>, <hi rend="ital">Orientales</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">S</w>’<w n="1.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="1.3">n</w>’<w n="1.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="1.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">J</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="3.4">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.6">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">Ch<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="4.2">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="4.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ;</l>
					<l n="5" num="1.5"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="5.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="5.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="6.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.4">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="6.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="7" num="1.7"><w n="7.1">N</w>’<w n="7.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="8" num="1.8"><w n="8.1">L</w>’<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.4">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="10" num="2.2"><w n="10.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">j</w>’<w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> !</l>
					<l n="11" num="2.3"><w n="11.1">J</w>’<w n="11.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.3">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="11.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="12" num="2.4"><w n="12.1">P<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>.</l>
					<l n="13" num="2.5"><w n="13.1">Ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="13.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.4">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>lsh<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="14" num="2.6"><w n="14.1">S<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="14.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="14.4">g<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="15" num="2.7"><w n="15.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="15.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="15.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.5">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="16" num="2.8"><w n="16.1">V<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="16.2">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.4">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="17.2">j</w>’<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="18" num="3.2"><w n="18.1">D<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.3">p<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>-<w n="18.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w>-<w n="18.5">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
					<l n="19" num="3.3"><w n="19.1">J</w>’<w n="19.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.4">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="20" num="3.4"><w n="20.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.3">t<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="20.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.5">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> :</l>
					<l n="21" num="3.5"><w n="21.1">J</w>’<w n="21.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>-<w n="21.5">t<seg phoneme="i" type="vs" value="1" rule="493">y</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="22" num="3.6"><w n="22.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="22.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.3">gu<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="22.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="22.5">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="23" num="3.7"><w n="23.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="23.3">d</w>’<w n="23.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>… <w n="23.5">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>pp<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="24" num="3.8">— <w n="24.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="24.2">m</w>’<w n="24.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="24.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="24.6">l</w>’<w n="24.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="25.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="25.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="25.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="25.6">b<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="26" num="4.2"><w n="26.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="26.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="26.3">l</w>’<w n="26.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="26.5"><seg phoneme="i" type="vs" value="1" rule="468">I</seg></w>, <w n="26.6">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
					<l n="27" num="4.3"><w n="27.1">C</w>’<w n="27.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="27.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ! <w n="27.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="27.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.6">f<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="28" num="4.4"><w n="28.1"><seg phoneme="ɛ" type="vs" value="1" rule="306">Ai</seg></w>-<w n="28.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="28.4">c</w>’<w n="28.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="28.6">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> !</l>
					<l n="29" num="4.5"><w n="29.1">J</w>’<w n="29.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="29.3">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="29.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.6">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="30" num="4.6"><w n="30.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="30.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="30.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="30.5">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
					<l n="31" num="4.7">— <w n="31.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="31.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="32" num="4.8"><w n="32.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.2">l</w>’<w n="32.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="32.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.5">b<seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> !</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><w n="33.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ll<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="33.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					<l n="34" num="5.2"><w n="34.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.2">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="34.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>-<w n="34.5">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> !</l>
					<l n="35" num="5.3"><w n="35.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="35.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="35.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="35.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
					<l n="36" num="5.4"><w n="36.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="36.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="36.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="36.4">d</w>’<w n="36.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> !</l>
					<l n="37" num="5.5"><w n="37.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.2">n</w>’<w n="37.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="37.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="37.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="38" num="5.6"><w n="38.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="38.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>… <w n="38.3">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="38.4">l</w>’<w n="38.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="39" num="5.7"><w n="39.1">V<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> ! <w n="39.2">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ?</l>
					<l n="40" num="5.8"><w n="40.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.2">sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ctr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.4">R<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">Septembre 1883.</date>
						</dateline>
				</closer>
			</div></body></text></TEI>