<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES LÈVRES CLOSES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1986 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les lèvres closes</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxleslevrescloses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				<p>Le poème "la Beauté" a été ajouté à partir d’une saisie manuelle</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX29">
				<head type="main">CE SOIR</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.3">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.5">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>pl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.8">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
					<l n="2" num="1.2">— <w n="2.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="2.2">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> ! <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="2.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="2.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! — <w n="2.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.8"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="3.2">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="3.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="3.4">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="3.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="3.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.8">f<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.10">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">R<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="4.3">d<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="4.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="5.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="5.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.5">l</w>’<w n="5.6">h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="5.7">d</w>’<w n="5.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.9">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.10">qu</w>’<w n="5.11"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.12"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="6" num="2.2"><w n="6.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w> <w n="6.5">ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="6.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rg<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="6.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="6.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.9">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
					<l n="7" num="2.3"><w n="7.1">L<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="7.2"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="7.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.5">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="7.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.10"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
					<l n="8" num="2.4"><w n="8.1">L<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="8.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>ct<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="8.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.4">l</w>’<w n="8.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.7">tr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">D<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="9.3">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.4">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="9.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu</w>’<w n="9.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="9.9">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="9.10">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>.</l>
					<l n="10" num="3.2"><w n="10.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.3">qu</w>’<w n="10.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="10.5">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.8">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="10.10">qu</w>’<w n="10.11"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="10.12">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.13">fr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="11" num="3.3"><w n="11.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! <w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="11.6">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="12" num="3.4"><w n="12.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.4">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="12.7">s<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r</w> <w n="12.8">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.9">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="13.3">d</w>’<w n="13.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="13.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.8">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="13.9">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="13.10">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="14" num="4.2"><w n="14.1">C<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="14.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w>, <w n="14.7"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="14.8">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ! <w n="14.9"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.10">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.11">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w></l>
					<l n="15" num="4.3"><w n="15.1">Qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.2">j</w>’<w n="15.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="15.4"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="15.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! <w n="15.6"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="15.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! <w n="15.8"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="15.9">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> ! —</l>
					<l n="16" num="4.4"><w n="16.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.3">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="16.4">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="16.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.7">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.8">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
				</lg>
			</div></body></text></TEI>