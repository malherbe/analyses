<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU37">
					<head type="number">XXXVI</head>
					<head type="main">Le Chat</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">V<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="1.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="1.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>, <w n="1.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="1.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.7">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="1.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ;</l>
						<l n="2" num="1.2"><space quantity="8" unit="char"></space><w n="2.1">R<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.3">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ff<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="2.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>-<w n="3.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="3.4">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.7">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="3.8"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="4" num="1.4"><space quantity="8" unit="char"></space><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="4.2">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.5">d</w>’<w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.3">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w> <w n="5.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.6">l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w></l>
						<l n="6" num="2.2"><space quantity="8" unit="char"></space><w n="6.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.5">d<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="7.5">s</w>’<w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="360">en</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.8">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w></l>
						<l n="8" num="2.4"><space quantity="8" unit="char"></space><w n="8.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lp<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ctr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.4">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>. <w n="9.7">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.8">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>,</l>
						<l n="10" num="3.2"><space quantity="8" unit="char"></space><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.3">t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>, <w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">b<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1">Pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.3">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d</w>, <w n="11.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.6">f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="11.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.9">d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>,</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><space quantity="8" unit="char"></space><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="12.4">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.7">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="13" num="4.2"><w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="13.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>, <w n="13.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="13.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg></w>,</l>
						<l n="14" num="4.3"><space quantity="8" unit="char"></space><w n="14.1">N<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="14.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="14.6">br<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w>.</l>
					</lg>
				</div></body></text></TEI>