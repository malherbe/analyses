<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU8">
					<head type="number">VII</head>
					<head type="main">La Muse malade</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.3">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="1.4">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="1.5">qu</w>’<w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>-<w n="1.7">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.8">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="1.9">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.10">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ?</l>
						<l n="2" num="1.2"><w n="2.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="2.3">cr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="2.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.5">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="2.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="2.8">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ct<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.7">s</w>’<w n="3.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.9">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.11">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w></l>
						<l n="4" num="1.4"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.2">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.4">l</w>’<w n="4.5">h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="4.6">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.8">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>cc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.5">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.7">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
						<l n="6" num="2.2"><w n="6.1">T</w>’<w n="6.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>-<w n="6.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="6.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.8">l</w>’<w n="6.9"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="6.10">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.11">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="6.12"><seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ?</l>
						<l n="7" num="2.3"><w n="7.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.2">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>, <w n="7.3">d</w>’<w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>g</w> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.8">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1">T</w>’<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>-<w n="8.3">t</w>-<w n="8.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="8.5">n<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="8.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="8.8">d</w>’<w n="8.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.10">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="8.11">M<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.3">qu</w>’<w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xh<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.5">l</w>’<w n="9.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.7">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="10" num="3.2"><w n="10.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="10.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="10.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w> <w n="10.6">f<seg phoneme="y" type="vs" value="1" rule="445">û</seg>t</w> <w n="10.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="10.8">fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="11.5">chr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="11.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t</w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.8">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="11.9">rh<seg phoneme="i" type="vs" value="1" rule="493">y</seg>thm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="12.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.6">s<seg phoneme="i" type="vs" value="1" rule="493">y</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="12.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="13" num="4.2"><w n="13.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="13.2">r<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>gn<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="13.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.7">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Ph<seg phoneme="e" type="vs" value="1" rule="250">œ</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="14.5">P<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>, <w n="14.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.7">s<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="14.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.9">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>