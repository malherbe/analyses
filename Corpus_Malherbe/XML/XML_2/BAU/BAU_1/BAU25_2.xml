<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU25">
					<head type="number">XXIV</head>
					<head type="main">La Chevelure</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="1.2">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="1.3">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.4">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="1.6">l</w>’<w n="1.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="2.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>cl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ! <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg></w> <w n="2.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.6">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.7">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> !</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>xt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> ! <w n="3.2">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.3">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.4">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="3.6">l</w>’<w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>lc<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.8"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="4.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.6">ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="5" num="1.5"><w n="5.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.6">l</w>’<w n="5.7"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="5.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.10">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> !</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="7" num="2.2"><w n="7.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="7.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg>t</w>,</l>
						<l n="8" num="2.3"><w n="8.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="8.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.4">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="8.5">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="9" num="2.4"><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="9.2">d</w>’<w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="9.5">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>gu<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="9.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.8">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="10" num="2.5"><w n="10.1">L<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.2">m<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>, <w n="10.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> ! <w n="10.6">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="10.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1"><w n="11.1">J</w>’<w n="11.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="11.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="11.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="11.6">l</w>’<w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.9">l</w>’<w n="11.10">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="11.11">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="11.12">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="11.13">s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="12" num="3.2"><w n="12.1">S<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.2">p<seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="242">e</seg>nt</w> <w n="12.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="12.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.5">l</w>’<w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="12.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.8">cl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w> ;</l>
						<l n="13" num="3.3"><w n="13.1">F<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="13.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="13.3">s<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.5">h<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="13.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.7">m</w>’<w n="13.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="14" num="3.4"><w n="14.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="14.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="14.4">d</w>’<w n="14.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.8">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="15" num="3.5"><w n="15.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>, <w n="15.3">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.4">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="15.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.6">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="15.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.8">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.9">m<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ts</w> :</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1"><w n="16.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="16.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="16.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="16.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="16.7">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="16.8">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="17" num="4.2"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="17.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="17.3">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="17.4">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg></w>, <w n="17.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="17.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.10">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ;</l>
						<l n="18" num="4.3"><w n="18.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.3">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>, <w n="18.4">gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="18.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="18.6">l</w>’<w n="18.7"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="18.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="18.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.11">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="19" num="4.4"><w n="19.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="380">e</seg>nt</w> <w n="19.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="19.3">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="19.4">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="19.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="19.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="19.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.8">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="20" num="4.5"><w n="20.1">D</w>’<w n="20.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.3">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="20.4">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="20.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="20.6">fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="20.7">l</w>’<w n="20.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="20.9">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1"><w n="21.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.2">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="21.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.4">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="21.6">d</w>’<w n="21.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="22" num="5.2"><w n="22.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="22.2">c<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.3">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="22.4"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="22.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="22.6">l</w>’<w n="22.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="22.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
						<l n="23" num="5.3"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="23.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="23.5">qu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.6">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="23.7">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="23.8">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="24" num="5.4"><w n="24.1">S<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="24.3">r<seg phoneme="ə" type="vi" value="1" rule="357">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="24.4"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="24.5">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
						<l n="25" num="5.5"><w n="25.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="25.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> <w n="25.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="25.4">l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="25.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
					</lg>
					<lg n="6">
						<l n="26" num="6.1"><w n="26.1">Ch<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="26.2">bl<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w>, <w n="26.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.5">t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>br<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="26.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>,</l>
						<l n="27" num="6.2"><w n="27.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="27.2">m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="27.4">l</w>’<w n="27.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="27.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="27.7">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="27.8"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="27.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="27.10">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> ;</l>
						<l n="28" num="6.3"><w n="28.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="28.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.3">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w> <w n="28.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="28.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="28.6">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="28.7">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>ch<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="28.8">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="29" num="6.4"><w n="29.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.2">m</w>’<w n="29.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="360">en</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="29.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="a" type="vs" value="1" rule="365">e</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="29.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="29.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w></l>
						<l n="30" num="6.5"><w n="30.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.2">l</w>’<w n="30.3">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.5">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>, <w n="30.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="30.7">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sc</w> <w n="30.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="30.9">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="30.10">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
					</lg>
					<lg n="7">
						<l n="31" num="7.1"><w n="31.1">L<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> ! <w n="31.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> ! <w n="31.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="31.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="31.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.7">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="31.8">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="32" num="7.2"><w n="32.1">S<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.3">r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="32.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="32.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="32.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ph<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>,</l>
						<l n="33" num="7.3"><w n="33.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="33.2">qu</w>’<w n="33.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="33.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="33.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="33.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="33.7">n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.8">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="33.9">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="33.10">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> !</l>
						<l n="34" num="7.4"><w n="34.1">N</w>’<w n="34.2"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w>-<w n="34.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="34.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="34.5">l</w>’<w n="34.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="34.7"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="34.8">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.9">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>, <w n="34.10"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="34.11">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.12">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
						<l n="35" num="7.5"><w n="35.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="35.2">j<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.3">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="35.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w> <w n="35.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w> <w n="35.7">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="35.8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="35.9">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="35.10">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ?</l>
					</lg>
				</div></body></text></TEI>