<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL46">
				<head type="main">LE MAY.</head>
				<head type="sub_1">RONDE A DANSER.</head>
				<head type="tune">Air : Noté, N°. 26.</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">T<seg phoneme="ɔ" type="vs" value="1" rule="443">O</seg>C</w>, <w n="1.2">t<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c</w>, <w n="1.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="1.4">s</w>’<w n="1.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="1.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="2"></space><w n="2.1">M<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="2.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
					<l n="3" num="1.3"><w n="3.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.5">B<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w></l>
					<l n="4" num="1.4"><space unit="char" quantity="2"></space><w n="4.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="4.3">G<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ffl<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="5" num="1.5"><space unit="char" quantity="6"></space><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="5.2">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> ! <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="5.4">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="6.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.3">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.4">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="6.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.6">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">J<seg phoneme="ə" type="vi" value="1" rule="348">E</seg></w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.5">B<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w></l>
					<l n="8" num="2.2"><space unit="char" quantity="2"></space><w n="8.1">D<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.3">G<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ffl<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="9" num="2.3"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="9.2">B<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="9.3">c<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="9.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
					<l n="10" num="2.4"><space unit="char" quantity="2"></space><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="10.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="10.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="10.5">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="11" num="2.5"><space unit="char" quantity="6"></space><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="11.2">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> ! <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="11.4">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</l>
					<l n="12" num="2.6"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="12.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.3">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.4">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="12.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="12.6">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">UN</seg></w> <w n="13.2">B<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="13.3">c<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="13.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
					<l n="14" num="3.2"><space unit="char" quantity="2"></space><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="14.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="14.4">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="14.5">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> :</l>
					<l n="15" num="3.3"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pp<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="15.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="15.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="15.4">l<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>,</l>
					<l n="16" num="3.4"><space unit="char" quantity="2"></space><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.2">cl<seg phoneme="e" type="vs" value="1" rule="148">e</seg>f</w> <w n="16.3">n</w>’<w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="16.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="16.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="17" num="3.5"><space unit="char" quantity="6"></space><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="17.2">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> ! <subst hand="RR" reason="analysis" type="repetition"><del>etc.</del><add> <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="17.4">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</add></subst></l>
					<l n="18" num="3.6"><subst hand="RR" reason="analysis" type="repetition"><del> </del><add><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="18.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.3">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.4">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="18.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="18.6">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</add></subst></l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>PP<seg phoneme="ɥi" type="vs" value="1" rule="462">U</seg><seg phoneme="j" type="sc" value="0" rule="496">Y</seg><seg phoneme="e" type="vs" value="1" rule="347">EZ</seg></w> <w n="19.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="19.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="19.4">l<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>,</l>
					<l n="20" num="4.2"><space unit="char" quantity="2"></space><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.2">cl<seg phoneme="e" type="vs" value="1" rule="148">e</seg>f</w> <w n="20.3">n</w>’<w n="20.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="20.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="21" num="4.3"><w n="21.1">J</w>’<w n="21.2"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="21.3">j</w>’<w n="21.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>, <w n="21.5">j</w>’<w n="21.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>,</l>
					<l n="22" num="4.4"><space unit="char" quantity="2"></space><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="22.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="22.3">d</w>’<w n="22.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="22.5">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="23" num="4.5"><space unit="char" quantity="6"></space><w n="23.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="23.2">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> ! <subst hand="RR" reason="analysis" type="repetition"><del>etc.</del><add> <w n="23.3"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="23.4">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</add></subst></l>
					<l n="24" num="4.6"><subst hand="RR" reason="analysis" type="repetition"><del> </del><add><w n="24.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="24.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.3">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="24.4">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="24.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="24.6">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</add></subst></l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">J</w>’<w n="25.2"><seg phoneme="u" type="vs" value="1" rule="425">OU</seg>VR<seg phoneme="i" type="vs" value="1" rule="468">I</seg>S</w>, <w n="25.3">j</w>’<w n="25.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>, <w n="25.5">j</w>’<w n="25.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>,</l>
					<l n="26" num="5.2"><space unit="char" quantity="2"></space><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="26.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="26.3">d</w>’<w n="26.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="26.5">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="27" num="5.3"><w n="27.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="27.2">M<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="27.4">l</w>’<w n="27.5">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>,</l>
					<l n="28" num="5.4"><space unit="char" quantity="2"></space><w n="28.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="28.2">d<seg phoneme="a" type="vs" value="1" rule="145">a</seg>m</w>’<w n="28.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.4">v</w>’<w n="28.5">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="28.6">p<seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="29" num="5.5"><space unit="char" quantity="6"></space><w n="29.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="29.2">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> ! <subst hand="RR" reason="analysis" type="repetition"><del>etc.</del><add> <w n="29.3"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="29.4">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</add></subst></l>
					<l n="30" num="5.6"><subst hand="RR" reason="analysis" type="repetition"><del> </del><add><w n="30.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="30.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.3">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="30.4">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="30.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="30.6">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</add></subst></l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">QU<seg phoneme="ɑ̃" type="vs" value="1" rule="313">AN</seg>D</w> <w n="31.2">M<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="31.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="31.4">l</w>’<w n="31.5">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>,</l>
					<l n="32" num="6.2"><space unit="char" quantity="2"></space><w n="32.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="32.2">D<seg phoneme="a" type="vs" value="1" rule="145">a</seg>m</w>’ <w n="32.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.4">v</w>’<w n="32.5">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="32.6">p<seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="33" num="6.3"><w n="33.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="33.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="33.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="33.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>,</l>
					<l n="34" num="6.4"><space unit="char" quantity="2"></space><w n="34.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="34.2">j</w>’<w n="34.3"><seg phoneme="y" type="vs" value="1" rule="391">eu</seg>s</w> <w n="34.4">b<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="34.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="35" num="6.5"><space unit="char" quantity="6"></space><w n="35.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="35.2">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> ! <subst hand="RR" reason="analysis" type="repetition"><del>etc.</del><add> <w n="35.3"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="35.4">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</add></subst></l>
					<l n="36" num="6.6"><subst hand="RR" reason="analysis" type="repetition"><del> </del><add><w n="36.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="36.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.3">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="36.4">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="36.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="36.6">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</add></subst></l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1">L<seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="37.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="37.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="37.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="37.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>,</l>
					<l n="38" num="7.2"><space unit="char" quantity="2"></space><w n="38.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="38.2">j</w>’<w n="38.3"><seg phoneme="y" type="vs" value="1" rule="391">eu</seg>s</w> <w n="38.4">b<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="38.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="39" num="7.3"><w n="39.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="39.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="39.4">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>,</l>
					<l n="40" num="7.4"><space unit="char" quantity="2"></space><w n="40.1">J</w>’<w n="40.2">l</w>’<w n="40.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="40.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="40.5">b<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill</w>’ <w n="40.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="40.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="41" num="7.5"><space unit="char" quantity="6"></space><w n="41.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="41.2">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> ! <subst hand="RR" reason="analysis" type="repetition"><del>etc.</del><add> <w n="41.3"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="41.4">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</add></subst></l>
					<l n="42" num="7.6"><subst hand="RR" reason="analysis" type="repetition"><del> </del><add><w n="42.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="42.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="42.3">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="42.4">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="42.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="42.6">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</add></subst></l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="8">
					<l n="43" num="8.1"><w n="43.1">J<seg phoneme="ə" type="vi" value="1" rule="348">E</seg></w> <w n="43.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="43.3">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="43.4">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>,</l>
					<l n="44" num="8.2"><space unit="char" quantity="2"></space><w n="44.1">J</w>’<w n="44.2">l</w>’<w n="44.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="44.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="44.5">b<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill</w>’ <w n="44.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="44.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="45" num="8.3"><w n="45.1">C<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="45.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="45.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="45.4">qu</w>’<w n="45.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>-<w n="45.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="45.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> ?</l>
					<l n="46" num="8.4"><space unit="char" quantity="2"></space><w n="46.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="46.2">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="46.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="46.4">m<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="47" num="8.5"><space unit="char" quantity="6"></space><w n="47.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="47.2">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> ! <subst hand="RR" reason="analysis" type="repetition"><del>etc.</del><add> <w n="47.3"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="47.4">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</add></subst></l>
					<l n="48" num="8.6"><subst hand="RR" reason="analysis" type="repetition"><del> </del><add><w n="48.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="48.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="48.3">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="48.4">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="48.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="48.6">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</add></subst></l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="9">
					<l n="49" num="9.1"><w n="49.1">C<seg phoneme="o" type="vs" value="1" rule="444">O</seg>L<seg phoneme="ɛ̃" type="vs" value="1" rule="466">IN</seg></w>, <w n="49.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="49.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="49.4">qu</w>’<w n="49.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>-<w n="49.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="49.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> ?</l>
					<l n="50" num="9.2"><space unit="char" quantity="2"></space><w n="50.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="50.2">f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="50.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="50.4">m<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="51" num="9.3"><w n="51.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="51.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="51.3">M<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="51.4">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>,</l>
					<l n="52" num="9.4"><space unit="char" quantity="2"></space><w n="52.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="52.2">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="52.3">f<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="53" num="9.5"><space unit="char" quantity="6"></space><w n="53.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="53.2">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> ! <subst hand="RR" reason="analysis" type="repetition"><del>etc.</del><add> <w n="53.3"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="53.4">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</add></subst></l>
					<l n="54" num="9.6"><subst hand="RR" reason="analysis" type="repetition"><del> </del><add><w n="54.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="54.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="54.3">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="54.4">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="54.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="54.6">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</add></subst></l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="10">
					<l n="55" num="10.1"><w n="55.1">H<seg phoneme="e" type="vs" value="1" rule="409">É</seg>L<seg phoneme="a" type="vs" value="1" rule="340">A</seg>S</w> ! <w n="55.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="55.3">M<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="55.4">v<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>,</l>
					<l n="56" num="10.2"><space unit="char" quantity="2"></space><w n="56.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="56.2">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="56.3">f<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="57" num="10.3"><w n="57.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="57.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="57.3">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="57.4">n</w>’<w n="57.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="57.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="57.7">n<seg phoneme="e" type="vs" value="1" rule="COL46_1">et</seg></w>,</l>
					<l n="58" num="10.4"><space unit="char" quantity="2"></space><w n="58.1">J<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="58.2">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="58.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="58.4">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>.</l>
					<l n="59" num="10.5"><space unit="char" quantity="6"></space><w n="59.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="59.2">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> ! <subst hand="RR" reason="analysis" type="repetition"><del>etc.</del><add> <w n="59.3"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="59.4">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</add></subst></l>
					<l n="60" num="10.6"><subst hand="RR" reason="analysis" type="repetition"><del> </del><add><w n="60.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="60.2">l<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="60.3">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="60.4">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="60.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="60.6">M<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> !</add></subst></l>
				</lg>
			</div></body></text></TEI>