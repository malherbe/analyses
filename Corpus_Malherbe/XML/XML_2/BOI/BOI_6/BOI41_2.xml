<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODES, ÉPIGRAMMES ET AUTRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>633 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1664" to="1704">1664-1704</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI41">
				<head type="main">Vers pour mettre au bas du portrait<lb></lb> de Mlle de Lamoignon</head>
				<opener>
					<dateline>
						<date when="1687">(1687)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="1.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> <w n="1.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="1.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.7">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="2.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w></l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="3.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.3">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.6">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="3.7">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
					<l n="4" num="1.4"><w n="4.1">J<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="4.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="4.3">cl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w> <w n="4.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="4.5">n<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.7">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="4.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.9">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="5" num="1.5"><w n="5.1">F<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="5.2">r<seg phoneme="ə" type="vi" value="1" rule="351">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="5.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="5.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.7">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="5.8">s<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w> ;</l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.4">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="6.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="6.6">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="6.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="6.8">d</w>’<w n="6.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ct<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="7" num="1.7"><w n="7.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w>, <w n="7.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.5">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg>s</w>, <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="8" num="1.8"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="8.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.4">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="8.5">d<seg phoneme="ə" type="vi" value="1" rule="348">e</seg></w> <w n="8.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.8">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>