<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN23" modus="cp" lm_max="16" metProfile="8, 6=6, 4÷6, 16, (13), (11), (14)" form="suite de strophes" schema="1[aaa] 1[abbba] 1[aaabcccbdddb]">
		<head type="main">BIFUR</head>
			<lg n="1" type="regexp" rhyme="aaa">
				<l n="1" num="1.1" lm="12" mp6="F" mp8="P" met="4+8"><w n="1.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="1.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="1.3" punct="vg:4">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="1.4">tr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="1.5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>t</w> <w n="1.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="1.8">n<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w> <w n="1.9" punct="vg:12">l<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
				<l n="2" num="1.2" lm="13"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" punct="vg">ai</seg></w>, <w n="2.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="2.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="2.5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg></w> <w n="2.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="2.8" punct="vg:13">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" mp="M">o</seg>rt<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="y" type="vs" value="1" rule="453" place="13">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="14" punct="vg" mp="F">e</seg></rhyme></w>,</l>
				<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="3.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>lqu</w>’<w n="3.6" punct="pt:8"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
			</lg>
			<lg n="2" type="regexp" rhyme="a">
				<l n="4" num="2.1" lm="8" met="8"><w n="4.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w>-<w n="4.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="4.3">l</w>’<w n="4.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">im</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <hi rend="ital"><w n="4.5" punct="pi:8">b<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>f<rhyme label="a" id="2" gender="m" type="a" stanza="2"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pi">u</seg>r</rhyme></w></hi> ?</l>
			</lg>
			<lg n="3" type="regexp" rhyme="bbb">
				<l n="5" num="3.1" lm="12" met="6+6"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="5.4" punct="vg:5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5" punct="vg">oin</seg></w>, <w n="5.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>s</w><caesura></caesura> <w n="5.6" punct="vg:7">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7" punct="vg">oin</seg></w>, <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="5.8">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="9">oin</seg></w> <w n="5.9">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="5.10" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>c<rhyme label="b" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
				<l n="6" num="3.2" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>lc<rhyme label="b" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
				<l n="7" num="3.3" lm="8" met="8"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="7.4" punct="pt:8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<rhyme label="b" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
			</lg>
			<lg n="4" type="regexp" rhyme="a">
				<l n="8" num="4.1" lm="8" met="8"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">im</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <hi rend="ital"><w n="8.5" punct="pt:8">b<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>f<rhyme label="a" id="2" gender="m" type="e" stanza="2"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg>r</rhyme></w></hi>.</l>
			</lg>
			<lg n="5" type="regexp" rhyme="aaa">
				<l n="9" num="5.1" lm="10" mp4="M" mp6="Mem" met="10"><w n="9.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="9.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="9.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>vr<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="9.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="9.7" punct="pv:10">d<rhyme label="a" id="4" gender="f" type="a" stanza="3"><seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
				<l n="10" num="5.2" lm="8" met="8"><w n="10.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="10.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="10.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="10.6">r<rhyme label="a" id="4" gender="f" type="e" stanza="3"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
				<l n="11" num="5.3" lm="8" met="8"><w n="11.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="11.2">n<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>l</w> <w n="11.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="œ" type="vs" value="1" rule="389" place="4">oeu</seg>r</w> <w n="11.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="11.6" punct="pi:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>b<rhyme label="a" id="4" gender="f" type="a" stanza="3"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
			</lg>
			<lg n="6" type="regexp" rhyme="b">
				<l n="12" num="6.1" lm="8" met="8"><w n="12.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">im</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <hi rend="ital"><w n="12.4" punct="pt:8">b<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>f<rhyme label="b" id="5" gender="m" type="a" stanza="3"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg>r</rhyme></w></hi>.</l>
			</lg>
			<lg n="7" type="regexp" rhyme="ccc">
				<l n="13" num="7.1" lm="16"><w n="13.1">C</w>’<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="13.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rp<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>t<seg phoneme="y" type="vs" value="1" rule="d-3" place="5" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="13.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="11">en</seg>t</w> <w n="13.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="12" mp="C">e</seg>s</w> <w n="13.7" punct="vg:16">l<seg phoneme="o" type="vs" value="1" rule="444" place="13" mp="M">o</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="14" mp="M">o</seg>m<seg phoneme="o" type="vs" value="1" rule="444" place="15" mp="M">o</seg>t<rhyme label="c" id="6" gender="f" type="a" stanza="3"><seg phoneme="i" type="vs" value="1" rule="468" place="16">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="17" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
				<l n="14" num="7.2" lm="11"><w n="14.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="14.2" punct="vg:2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>rt</w>, <w n="14.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="14.4" punct="vg:4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></w>, <w n="14.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="14.6" punct="vg:6">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6" punct="vg">en</seg>t</w>, <w n="14.7" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="189" place="7" punct="vg">e</seg>t</w>, <w n="14.8">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d</w> <w n="14.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="14.10" punct="vg:11"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>rr<rhyme label="c" id="6" gender="f" type="e" stanza="3"><seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="vg" mp="F">e</seg></rhyme></w>,</l>
				<l n="15" num="7.3" lm="16"><w n="15.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="15.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="15.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="15.5">n</w>’<w n="15.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="15.7">gu<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="15.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="15.9" punct="vg:11">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="11" punct="vg">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="15.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="12">en</seg></w> <w n="15.11" punct="pt:16">d<seg phoneme="e" type="vs" value="1" rule="409" place="13" mp="M">é</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="14" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="15" mp="M">i</seg>t<rhyme label="c" id="6" gender="f" type="a" stanza="3"><seg phoneme="i" type="vs" value="1" rule="468" place="16">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="17" punct="pt" mp="F">e</seg></rhyme></w>.</l>
			</lg>
			<lg n="8" type="regexp" rhyme="b">
				<l n="16" num="8.1" lm="8" met="8"><w n="16.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">im</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <hi rend="ital"><w n="16.4" punct="pt:8">b<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>f<rhyme label="b" id="5" gender="m" type="e" stanza="3"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg>r</rhyme></w></hi>.</l>
			</lg>
			<lg n="9" type="regexp" rhyme="ddd">
				<l n="17" num="9.1" lm="16"><w n="17.1">H<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fm">e</seg></w>-<w n="17.2" punct="vg:4">L<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="17.3">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M/mc">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" mp="Lc">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>-<w n="17.4" punct="vg:11"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M/mc">In</seg>f<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M/mc">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="9">i</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" punct="vg" mp="F">e</seg></w>, <w n="17.5" punct="vg:13">Ni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="13" punct="vg" mp="F">e</seg></w>, <w n="17.6" punct="vg:15">C<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="14" mp="M">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="15" punct="vg">a</seg>l</w>, <w n="17.7" punct="vg:16"><rhyme label="d" id="7" gender="f" type="a" stanza="3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="16">O</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="17" punct="vg" mp="F">e</seg></rhyme></w>,</l>
				<l n="18" num="9.2" lm="12" mp6="M" met="4+4+4"><w n="18.1" punct="vg:4">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="vg" caesura="1">en</seg>ts</w>,<caesura></caesura> <w n="18.2" punct="vg:8">D<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="vg" caesura="2">en</seg>ts</w>,<caesura></caesura> <w n="18.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="18.4">c</w>’<w n="18.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="18.6" punct="pe:12">m<rhyme label="d" id="7" gender="f" type="e" stanza="3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
				<l n="19" num="9.3" lm="14"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="19.2" punct="pi:3">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="3" punct="pi">oi</seg></w> ? <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M/mp">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M/mp">e</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="Lp">on</seg>s</w>-<w n="19.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="19.5"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="19.6">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="9" mp="M">eu</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>s</w> <w n="19.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="P">u</seg>r</w> <w n="19.8">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="13" mp="F">e</seg></w> <w n="19.9" punct="pt:14">b<rhyme label="d" id="7" gender="f" type="a" stanza="3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="14">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="15" punct="pt" mp="F">e</seg></rhyme></w>.</l>
			</lg>
			<lg n="10" type="regexp" rhyme="b">
				<l n="20" num="10.1" lm="10" met="4+6"><w n="20.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="20.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="20.3" punct="vg:4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4" punct="vg" caesura="1">en</seg></w>,<caesura></caesura> <w n="20.4">l</w>’<w n="20.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5" mp="M">im</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <hi rend="ital"><w n="20.6" punct="pt:10">b<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>f<rhyme label="b" id="5" gender="m" type="a" stanza="3"><seg phoneme="y" type="vs" value="1" rule="450" place="10" punct="pt">u</seg>r</rhyme></w></hi>.</l>
			</lg>
	</div></body></text></TEI>