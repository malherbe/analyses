<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="drama" key="FRN13" modus="cp" lm_max="16" metProfile="6, 5, 1, 8, 2, 7, 3, 4, 5=8, 11, 6=6">
	<head type="main">LES PERCEPTIONS EXTÉRIEURES</head>
	<head type="form">PETIT ACTE</head>
		<div type="body">
			<div type="scene" n="1">
			<head type="number">SCÈNE I</head>
				<stage type="cast">L’AMIRAL, puis CAGLIOSTRO</stage>
			<sp n="1">
				<speaker><hi rend="smallcap">L’AMIRAL</hi></speaker> <stage>(<hi rend="ital">comptant sur ses doigts</hi>).</stage>
				<l n="1" lm="6" met="6"><w n="1.1">D<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="1.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="1.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="1.5" punct="vg:5">qu<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w></l>
				<l n="2" lm="5" met="5"><w n="2.1">Qu<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="2.3">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="2.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="2.5" punct="ps:5">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" punct="ps">e</seg>pt</w>…</l>
			</sp>
			<sp n="2">
				<speaker><hi rend="smallcap">CAGLIOSTRO</hi></speaker> <stage>(<hi rend="ital">entrant précipitamment</hi>).</stage>
				<l n="3" lm="13" met="5+8"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="497" place="1" mp="C">Y</seg></w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="Lp">a</seg></w>-<w n="3.3">t</w>-<w n="3.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="3.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" caesura="1">em</seg>ps</w><caesura></caesura> <w n="3.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="3.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="10">ez</seg></w> <w n="3.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="3.10" punct="pi:13">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" mp="M">om</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="352" place="13">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="14" punct="pi" mp="F">e</seg></w> ?</l>
				<stage>(<hi rend="ital">Il lui tire cinq coups de revolver.</hi>)</stage>
			</sp>
			<sp n="3">
				<speaker><hi rend="smallcap">L’AMIRAL</hi> (<hi rend="ital">expirant</hi>).</speaker>
				<l n="4" lm="1" met="1"><w n="4.1" punct="pe:1">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="2" punct="pe">e</seg></w> !</l>
				<stage>(<hi rend="ital">Il expire.</hi>)</stage>
			</sp>
			</div>
			<div type="scene" n="2">
			<head type="number">SCÈNE II</head>
			<sp n="4">
				<speaker><hi rend="smallcap">CAGLIOSTRO</hi></speaker> <stage>(<hi rend="ital">seul</hi>).</stage>
				<stage>(<hi rend="ital">Il s’étend sur un hamac.</hi>)</stage>
				<l n="5" lm="8" met="8"><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">Ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg></w> <w n="5.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="5.6" punct="pi:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pi">é</seg></w>?</l>
				<l n="6" lm="13" met="8+5"><w n="6.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="6.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="6.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="5">î</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" mp="F">e</seg>s</w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="6.6">m<seg phoneme="wa" type="vs" value="1" rule="420" place="8" caesura="1">oi</seg>s</w><caesura></caesura> <w n="6.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="6.8">j<seg phoneme="wa" type="vs" value="1" rule="440" place="10" mp="M">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>x</w> <w n="6.9" punct="vg:13"><seg phoneme="e" type="vs" value="1" rule="409" place="12" mp="M">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="13" punct="vg">é</seg></w>,</l>
				<l n="7" lm="8" met="8"><w n="7.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>si<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w>-<w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="7.4">qu</w>’<w n="7.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="7.6">l</w>’<w n="7.7"><seg phoneme="y" type="vs" value="1" rule="251" place="6">eû</seg>t</w> <w n="7.8" punct="pi:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pi">é</seg></w> ?</l>
				<stage>(<hi rend="ital">Il se retourne en bondissant sur le hamac.</hi>)</stage>
				<l n="8" lm="8" met="8"><w n="8.1" punct="vg:2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="8.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="8.3" punct="vg:5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="8.4">l</w>’<w n="8.5" punct="ps:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>j<seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg>l<seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
				<l n="9" lm="8" met="8"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.3">f<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="9.4">r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
				<l n="10" lm="8" met="8"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="10.3">bu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="10.4">d</w>’<w n="10.5" punct="ps:8"><seg phoneme="i" type="vs" value="1" rule="d-4" place="7">y</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
				<l n="11" lm="8" met="8"><w n="11.1" punct="vg:2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="11.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="11.3" punct="vg:5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="11.4">l</w>’<w n="11.5" punct="dp:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>j<seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg>l<seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
				<l n="12" lm="11" mp5="C" mp6="M"><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="12.2" punct="vg:2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>s</w>, <w n="12.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="12.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="12.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="12.6">cl<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s</w> <w n="12.7">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w> <w n="12.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="12.9">v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10" mp="M">e</seg>ill<seg phoneme="ø" type="vs" value="1" rule="403" place="11">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" mp="F">e</seg>s</w></l>
				<l n="13" lm="2" met="2"><w n="13.1" punct="pv:2">Fr<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l<seg phoneme="ø" type="vs" value="1" rule="403" place="2">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pv">e</seg>s</w> ;</l>
				<l n="14" lm="8" met="8"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="14.3">bu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="14.4">d</w>’<w n="14.5" punct="ps:8"><seg phoneme="i" type="vs" value="1" rule="d-4" place="7">y</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg>s</w>…</l>
				<stage>(<hi rend="ital">Un éclair sillonne la nue, de lourdes gouttes d’eau<lb></lb>
				se mettent à tomber.</hi>)</stage>
				<l n="15" lm="5" met="5"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">d</w>’<w n="15.3" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="307" place="2">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>rs</w>, <w n="15.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.5" punct="pt:5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
				<stage>(<hi rend="ital">Exit.</hi>)</stage>
			</sp>
			</div>
			<div type="scene" n="3">
			<head type="number">SCÈNE III</head>
				<stage type="cast">LA DAME AUX CAMÉLIAS, M. DE FREYCINET</stage>
			<sp n="5">
				<speaker><hi rend="smallcap">M. DE FREYCINET</hi></speaker>
				<l n="16" lm="8" met="8"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="16.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="5">en</seg>t</w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>gr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
				<l n="17" lm="12" mp6="M" mp4="C" met="8+4"><w n="17.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="17.2">f<seg phoneme="y" type="vs" value="1" rule="453" place="2" mp="M">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="17.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="17.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="17.5">c<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8" caesura="1">en</seg></w><caesura></caesura> <w n="17.7">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="17.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="17.9">t<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
				<l n="18" lm="11" mp4="M" mp6="F"><w n="18.1">N</w>’<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w>-<w n="18.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2" mp="F">e</seg></w> <w n="18.4" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>s</w>, <w n="18.5" punct="vg:6">M<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg" mp="F">e</seg></w>, <w n="18.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="18.7">c</w>’<w n="18.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="18.9"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>gr<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" mp="F">e</seg></w></l>
				<l n="19" lm="12" mp6="M" mp4="C" met="8+4"><w n="19.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="19.2">f<seg phoneme="y" type="vs" value="1" rule="453" place="2" mp="M">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="19.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="19.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="19.5">c<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8" caesura="1">en</seg></w><caesura></caesura> <w n="19.7">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="19.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="19.9" punct="ps:12">t<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></w>…</l>
			</sp>
			<sp n="6">
				<speaker><hi rend="smallcap">LA DAME AUX CAMÉLIAS</hi></speaker>
				<l n="20" lm="7" met="7"><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="20.3">f<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="20.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.6">N<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w></l>
				<l n="21" lm="2" met="2">(<w n="21.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w>-<w n="21.2" punct="pt:2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="pt">i</seg>l</w>).</l>
				<l n="22" lm="16"><w n="22.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="22.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="22.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="22.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem/mp">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M/mp">e</seg>ttr<seg phoneme="e" type="vs" value="1" rule="347" place="6" mp="Lp">ez</seg></w>-<w n="22.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="22.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg>t</w> <w n="22.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="22.8">b<seg phoneme="o" type="vs" value="1" rule="435" place="10" mp="M">o</seg>tt<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="12" mp="F">e</seg>s</w> <w n="22.9"><seg phoneme="a" type="vs" value="1" rule="342" place="13" mp="P">à</seg></w> <w n="22.10" punct="pi:16"><seg phoneme="e" type="vs" value="1" rule="409" place="14" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="15" mp="M">a</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="16">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="17" punct="pi" mp="F">e</seg>s</w> ?</l>
			</sp>
			<sp n="7">
				<speaker><hi rend="smallcap">M. DE FREYCINET</hi> (<hi rend="ital">à part</hi>).</speaker>
				<l n="23" lm="3" met="3"><w n="23.1">C</w>’<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="23.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="23.4" punct="pt:3">t<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="pt">i</seg>c</w>.</l>
				<stage>(<hi rend="ital">Il lui pince le genou : sonnerie électrique.</hi>)</stage>
			</sp>
			</div>
			<div type="scene" n="4">
			<head type="number">SCÈNE IV</head>
				<stage type="cast">LE 115<hi rend="sup">e</hi> DE LIGNE, LES PRÉCÉDENTS</stage>
			<sp n="8">
				<speaker><hi rend="smallcap">LE 115<hi rend="sup">e</hi> DE LIGNE</hi></speaker>
				<l n="24" lm="1" met="1"><w n="24.1" punct="vg:1">Gl<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="2" punct="vg">e</seg></w>,</l>
				<l n="25" lm="3" met="3"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="25.2" punct="pe:3">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pe">e</seg></w> !</l>
				<l n="26" lm="4" met="4"><w n="26.1" punct="vg:4">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></w>,</l>
				<l n="27" lm="2" met="2"><w n="27.1" punct="vg:1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg></w>, <w n="27.2" punct="pe:2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg></w> !</l>
				<stage>(<hi rend="ital">Le régiment passe.</hi>)</stage>
			</sp>
			</div>
			<div type="scene" n="5">
			<head type="number">SCÈNE V</head>
				<stage type="cast">LES PRÉCÉDENTS, moins LE RÉGIMENT, CAGLIOSTRO</stage>
			<sp n="9">
				<speaker><hi rend="smallcap">CAGLIOSTRO</hi></speaker> <stage>(<hi rend="ital">il entre en brandissant<lb></lb>
				un poignard hongrois</hi>).</stage>
				<l n="28" lm="6" met="6"><w n="28.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2" punct="vg:3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>r</w>, <w n="28.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="28.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w></l>
				<l n="29" lm="7" met="7"><w n="29.1">S</w>’<w n="29.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="29.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>t</w> <w n="29.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="29.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="29.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="29.7" punct="pt:7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="7" punct="pt">œu</seg>r</w>.</l>
			</sp>
			<sp n="10">
				<speaker><hi rend="smallcap">M. DE FREYCINET</hi></speaker> <stage>(<hi rend="ital">à part</hi>).</stage>
				<l n="30" lm="8" met="8"><w n="30.1">C</w>’<w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="30.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r</w> <w n="30.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="30.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="30.6">f<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>ch<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="30.7">h<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd</w></l>
				<l n="31" lm="10" met="10" mp4="C" mp6="F"><w n="31.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="31.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="31.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="31.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="31.5">tr<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="31.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="31.7"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="31.8" punct="pt:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg>rt</w>.</l>
				<stage>(<hi rend="ital">Haut, à Cagliostro.</hi>)</stage>
				<l n="32" lm="12" met="6+6"><w n="32.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="32.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="32.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M/mp">en</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M/mp">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="5" mp="Lp">ez</seg></w>-<w n="32.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="32.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg>t</w> <w n="32.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="32.7">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>th<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="32.8" punct="pi:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></w> ?</l>
			</sp>
			<sp n="11">
				<speaker><hi rend="smallcap">CAGLIOSTRO</hi></speaker> <stage>(<hi rend="ital">simplement, à la Dame aux Camélias</hi>).</stage>
				<l n="33" lm="4" met="4"><w n="33.1">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="33.2" punct="vg:3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>s</w>, <w n="33.3" punct="pi:4">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rth<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pi">e</seg></w> ?</l>
				<stage>(<hi rend="ital">Ils sortent.</hi>)</stage>
			</sp>
			</div>
			<div type="scene" n="6">
			<head type="number">SCÈNE VI</head>
			<sp n="12">
				<speaker>UNE VOIX</speaker>
				<l n="34" lm="1" met="1"><w n="34.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w></l>
				<l n="35" lm="1" met="1"><w n="35.1">M<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt</w></l>
				<l n="36" lm="1" met="1"><w n="36.1">T<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt</w></l>
				<l n="37" lm="1" met="1"><w n="37.1" punct="pt:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pt">A</seg></w>.</l>
			</sp>
				<stage>(<hi rend="ital">La toile s’ouvre dans le fond : apothéose représen-<lb></lb>
				tant l’admirable tableau de Millet intitulé</hi><lb></lb>
				l’Angélus. <hi rend="ital">Grandes orgues</hi> — Rideau.)</stage>
			</div>
		</div>
	</div></body></text></TEI>