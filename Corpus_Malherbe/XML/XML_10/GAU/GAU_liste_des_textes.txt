
	▪ Théophile GAUTIER [GAU]

 		Liste des recueils de poésies
 		─────────────────────────────
		▫ ÉMAUX ET CAMÉES, 1872 [GAU_1]
		▫ La comédie de la mort, 1838 [GAU_2]
		▫ ALBERTUS OU L’ÂME ET LE PÉCHÉ, 1833 [GAU_3]
		▫ POÉSIES - édition Maurice Dreyfous, 1833 [GAU_4]
		▫ POÉSIES DIVERSES, 1833-1838 - édition Maurice Dreyfous - Tome premier, 1833-1838 [GAU_5]
		▫ POÉSIES DIVERSES, 1838-1845 - édition Maurice Dreyfous - Tome second, 1838-1845 [GAU_6]
		▫ ESPAÑA - édition Maurice Dreyfous, 1845 [GAU_7]
		▫ POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES - édition Maurice Dreyfous, 1831-1872 [GAU_8]
		▫ Poésies de Th. Gautier qui ne figureront pas dans ses œuvres - Poèmes absents des précédentes éditions, 1873 [GAU_9]
