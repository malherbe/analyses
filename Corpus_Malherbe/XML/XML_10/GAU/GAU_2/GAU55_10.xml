<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La comédie de la mort</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5120 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">GAU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Comédie de la mort</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>inlibroveritas.net</publisher>
						<idno type="URL">http://www.inlibroveritas.net</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Comédie de la mort</title>
						<author>Théophile Gautier</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k70716q.r=th%C3%A9ophile+gautier.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>DESESSART, ÉDITEUR</publisher>
							<date when="1838">1838</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2014-04-11" who="RR">Un vers faux dans GAU54 (Thébaïde), vers 85 a été corrigé avec l’édition Bartillat (préparée par Michel Brix)</change>
			<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GAU55" modus="cm" lm_max="10" metProfile="4+6" form="suite périodique" schema="4(abab)">
				<head type="main">ROCAILLE</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">C<seg phoneme="o" type="vs" value="1" rule="435" place="1" mp="M/mp">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M/mp">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>s</w><caesura></caesura> <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rc</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="1.7" punct="vg:10">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rs<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="307" place="10">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="2.2" punct="vg:4">N<seg phoneme="a" type="vs" value="1" rule="343" place="3" mp="M">a</seg>ï<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="2.3"><seg phoneme="œ" type="vs" value="1" rule="286" place="5">œ</seg>il</w> <w n="2.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rt</w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8">ein</seg></w> <w n="2.7" punct="pv:10">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>fl<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pv">é</seg></rhyme></w> ;</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="3.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="3.3">h<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="3.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="3.7">r<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>c<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="307" place="10">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">D</w>’<w n="4.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="4.3">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">an</seg></w><caesura></caesura> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="4.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="4.6" punct="pt:10">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>l<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="5.3">c<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>r<seg phoneme="o" type="vs" value="1" rule="318" place="4" caesura="1">au</seg>x</w><caesura></caesura> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="5.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="P">u</seg>r</w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="5.7" punct="vg:10">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>dr<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>p<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="6" num="2.2" lm="10" met="4+6"><w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="6.2">l</w>’<w n="6.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fc">e</seg></w> <w n="6.5">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt</w> <w n="6.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="6.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="6.8" punct="pv:10">j<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="pv">on</seg>cs</rhyme></w> ;</l>
					<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3" punct="vg:4">b<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg" caesura="1">in</seg></w>,<caesura></caesura> <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="7.5">gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>n<seg phoneme="u" type="vs" value="1" rule="428" place="7">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="7.6" punct="vg:10">s<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>n<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="8" num="2.4" lm="10" met="4+6"><w n="8.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="8.3">ch<seg phoneme="œ" type="vs" value="1" rule="249" place="4" caesura="1">œu</seg>r</w><caesura></caesura> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="8.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="8.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="8.7" punct="pt:10">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>ge<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="pt">on</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="10" met="4+6"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="9.2">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3" punct="pv:4">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4" punct="pv" caesura="1">en</seg>t</w> ;<caesura></caesura> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="9.5">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" mp="M">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="9.6">N<seg phoneme="a" type="vs" value="1" rule="343" place="9" mp="M">a</seg>ï<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
					<l n="10" num="3.2" lm="10" met="4+6"><w n="10.1">S</w>’<w n="10.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="2">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="10.4">h<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">â</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.6">r<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>j<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="10.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="10.8" punct="vg:10">n<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="248" place="10" punct="vg">œu</seg>ds</rhyme></w>,</l>
					<l n="11" num="3.3" lm="10" met="4+6"><w n="11.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2">p<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="11.4">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" caesura="1">e</seg>t</w><caesura></caesura> <w n="11.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="11.6">h<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ts</w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="11.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
					<l n="12" num="3.4" lm="10" met="4+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="12.3">r<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="4" caesura="1">eau</seg>x</w><caesura></caesura> <w n="12.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="12.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="12.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="12.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="12.8" punct="pt:10">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>v<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pt">eu</seg>x</rhyme></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="10" met="4+6"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="13.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" caesura="1">en</seg>d</w><caesura></caesura> <w n="13.3">l</w>’<w n="13.4" punct="vg:7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="vg">er</seg></w>, <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="13.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="13.7">qu<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
					<l n="14" num="4.2" lm="10" met="4+6"><w n="14.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="14.2">fl<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>ts</w> <w n="14.3">d</w>’<w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="14.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="P">u</seg>r</w> <w n="14.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="14.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="14.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="14.9" punct="vg:10">su<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="10" punct="vg">i</seg>t</rhyme></w>,</l>
					<l n="15" num="4.3" lm="10" met="4+6"><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="15.2">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" caesura="1">o</seg>ff<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="15.5">tr<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="15.6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="15.8" punct="vg:10">bl<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="16" num="4.4" lm="10" met="4+6"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="16.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="16.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="16.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="16.6" punct="pt:10">br<seg phoneme="y" type="vs" value="1" rule="dc-3" place="9" mp="M">u</seg><rhyme label="b" id="8" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="10" punct="pt">i</seg>t</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>