<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU130" modus="sm" lm_max="7" metProfile="7" form="suite périodique" schema="5(abbacc)">
				<head type="main">INFIDÉLITÉ</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Bandiera d’ogni vento <lb></lb>
								Conosco que sei tu.
							</quote>
							<bibl>
								<hi rend="ital">Chanson italienne.</hi>
							</bibl>
						</cit>
						<cit>
							<quote>
								La volonté de l’ingrate est changée.
							</quote>
							<bibl>
								<name>Antoine de Baïf.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1" type="sizain" rhyme="abbacc">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="1.2">l</w>’<w n="1.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="1.5">b<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5" punct="pv:7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>ti<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="pv">er</seg></rhyme></w> ;</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ti<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="vg">er</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="7" met="7"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">b<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="4.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="4.4">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt</w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.6" punct="pv:7">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="5" num="1.5" lm="7" met="7"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.5"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.7">s<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r</rhyme></w></l>
					<l n="6" num="1.6" lm="7" met="7"><w n="6.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>mi<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="6.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="6.5" punct="pt:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="257" place="7" punct="pt">eoi</seg>r</rhyme></w>.</l>
				</lg>
				<lg n="2" type="sizain" rhyme="abbacc">
					<l n="7" num="2.1" lm="7" met="7"><w n="7.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="7.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">oû</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>m<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="8" num="2.2" lm="7" met="7"><w n="8.1">D</w>’<w n="8.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ni<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5" punct="vg:7">l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="vg">a</seg>s</rhyme></w>,</l>
					<l n="9" num="2.3" lm="7" met="7"><w n="9.1" punct="vg:1"><seg phoneme="u" type="vs" value="1" rule="426" place="1" punct="vg">Où</seg></w>, <w n="9.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ti<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="9.5" punct="vg:7">l<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="vg">a</seg>s</rhyme></w>,</l>
					<l n="10" num="2.4" lm="7" met="7"><w n="10.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" punct="vg">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg></w> <w n="10.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="10.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w>-<w n="10.5" punct="pe:7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>m<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></w> !</l>
					<l n="11" num="2.5" lm="7" met="7"><w n="11.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="11.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="11.3">gu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.5" punct="vg:7">fl<rhyme label="c" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="vg">eu</seg>rs</rhyme></w>,</l>
					<l n="12" num="2.6" lm="7" met="7"><w n="12.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ssi<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="12.3">fu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>r</w> <w n="12.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="12.5" punct="pt:7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<rhyme label="c" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="pt">eu</seg>rs</rhyme></w>.</l>
				</lg>
				<lg n="3" type="sizain" rhyme="abbacc">
					<l n="13" num="3.1" lm="7" met="7"><w n="13.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="13.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.5">r<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="14" num="3.2" lm="7" met="7"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">s<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="14.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="14.4">p<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="14.5">d</w>’<w n="14.6" punct="pv:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rg<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7" punct="pv">en</seg>t</rhyme></w> ;</l>
					<l n="15" num="3.3" lm="7" met="7"><w n="15.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="15.3">gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="u" type="vs" value="1" rule="428" place="4">ou</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="15.5">n<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ge<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>t</rhyme></w></l>
					<l n="16" num="3.4" lm="7" met="7"><w n="16.1">Tr<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="16.4" punct="pv:7">h<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>m<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="17" num="3.5" lm="7" met="7"><w n="17.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>s</w>, <w n="17.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="17.4">r<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<rhyme label="c" id="9" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg>x</rhyme></w></l>
					<l n="18" num="3.6" lm="7" met="7"><w n="18.1">B<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="18.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="18.3">pi<seg phoneme="e" type="vs" value="1" rule="241" place="4">e</seg>ds</w> <w n="18.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="18.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="18.6" punct="pt:7"><rhyme label="c" id="9" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="7" punct="pt">eau</seg>x</rhyme></w>.</l>
				</lg>
				<lg n="4" type="sizain" rhyme="abbacc">
					<l n="19" num="4.1" lm="7" met="7"><w n="19.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>s</w>, <w n="19.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="19.4" punct="vg:7">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rv<rhyme label="a" id="10" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="20" num="4.2" lm="7" met="7"><w n="20.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="20.3">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="20.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rt</w> <w n="20.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="20.6">pr<rhyme label="b" id="11" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s</rhyme></w></l>
					<l n="21" num="4.3" lm="7" met="7"><w n="21.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="21.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>ps</w> <w n="21.4" punct="vg:7">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>pr<rhyme label="b" id="11" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg>s</rhyme></w>,</l>
					<l n="22" num="4.4" lm="7" met="7"><w n="22.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="22.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="22.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="22.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>il</w> <w n="22.5">p<rhyme label="a" id="10" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="23" num="4.5" lm="7" met="7"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="23.2">m<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="23.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="23.5">mi<rhyme label="c" id="12" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>l</rhyme></w></l>
					<l n="24" num="4.6" lm="7" met="7"><w n="24.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="24.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.3">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="24.5" punct="pt:7">ci<rhyme label="c" id="12" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="7" punct="pt">e</seg>l</rhyme></w>.</l>
				</lg>
				<lg n="5" type="sizain" rhyme="abbacc">
					<l n="25" num="5.1" lm="7" met="7"><w n="25.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.2" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>s</w>, <w n="25.3">l</w>’<w n="25.4">h<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="26" num="5.2" lm="7" met="7"><w n="26.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="26.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="26.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="26.5" punct="vg:7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>j<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="vg">on</seg>s</rhyme></w>,</l>
					<l n="27" num="5.3" lm="7" met="7"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="27.3">c<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="27.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="27.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="27.6">j<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>cs</rhyme></w></l>
					<l n="28" num="5.4" lm="7" met="7"><w n="28.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">j<seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="28.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="28.4">l<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="28.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="28.6" punct="pv:7"><rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="29" num="5.5" lm="7" met="7"><w n="29.1">L</w>’<w n="29.2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r</w> <w n="29.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="29.4" punct="vg:3">p<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg>r</w>, <w n="29.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="29.6">g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="29.7" punct="ps:7">d<rhyme label="c" id="15" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="ps">ou</seg>x</rhyme></w>…</l>
					<l n="30" num="5.6" lm="7" met="7"><w n="30.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1">en</seg></w> <w n="30.2">n</w>’<w n="30.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="30.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="30.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="30.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="30.7" punct="pt:7">v<rhyme label="c" id="15" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="pt">ou</seg>s</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>