<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU125" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique" schema="abab abab cdd cee">
				<head type="main">SONNET II</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Amour tant vous hai servit <lb></lb>
								Senz pecas et senz failhimen, <lb></lb>
								Et vous sabez quant petit <lb></lb>
								Hai avut de jauzimen.
							</quote>
							<bibl>
								<name>Peyrols.</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								Ne sais tu pas que je n’eus onc <lb></lb>
								D’elle plaisir ny un seul bien.
							</quote>
							<bibl>
								<name>Marot.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1" rhyme="abab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="1.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="1.4" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="1.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="1.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="1.7">n</w>’<w n="1.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="1.9">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="10">oin</seg>t</w> <w n="1.10">d</w>’<w n="1.11"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>r</rhyme></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="2.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="2.5" punct="pv:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pv" caesura="1">er</seg></w> ;<caesura></caesura> <w n="2.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="2.8" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></w>, <w n="2.9" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>d<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>t</w> <w n="3.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="3.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="3.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="3.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="3.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="3.9" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>t<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>r</rhyme></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">Ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="2" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="4.2"><seg phoneme="y" type="vs" value="1" rule="453" place="4" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="4.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="8" mp="Lc">oi</seg></w>-<w n="4.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="4.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="4.8" punct="pt:12">bl<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" rhyme="abab">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="5.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="5.5" punct="vg:6">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="vg" caesura="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="P">ou</seg>s</w> <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="5.9">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg></w> <w n="5.10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>t<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>r</rhyme></w></l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="6.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2" mp="C">o</seg>s</w> <w n="6.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ls</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rqu<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="6.5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>t</w> <w n="6.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="6.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rd</w> <w n="6.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="6.9">fl<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="7.2" punct="vg:3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rç<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="7.3">qu</w>’<w n="7.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="7.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>t</w><caesura></caesura> <w n="7.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="7.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r</w> <w n="7.9">v<seg phoneme="y" type="vs" value="1" rule="457" place="10">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.10"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="7.11">j<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>r</rhyme></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="8.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="8.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="8.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.6"><seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="8.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="8.9" punct="pt:12"><rhyme label="b" id="4" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" rhyme="cdd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="9.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="9.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ts</w> <w n="9.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="9.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="9.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rts</w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="9.8" punct="pv:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">â</seg>l<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pv">eu</seg>r</rhyme></w> ;</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1" punct="vg:1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>r</w>, <w n="10.2">d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2" mp="P">è</seg>s</w> <w n="10.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="10.4">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg>s</w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>s</w><caesura></caesura> <w n="10.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="10.8" punct="vg:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="11.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="11.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="11.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="11.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="11.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="11.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.8" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>st<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
				</lg>
				<lg n="4" rhyme="cee">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="12.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="12.5">l</w>’<w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">â</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="12.9">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="12.10">fl<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r</rhyme></w></l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="13.2">s</w>’<w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="Lc">en</seg>tr</w>’<w n="13.4"><seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="13.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="13.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="13.7">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">em</seg>ps</w> <w n="13.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="13.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="13.10" punct="vg:12">v<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="14.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="14.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>t</w> <w n="14.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rs</w> <w n="14.7">qu</w>’<w n="14.8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="14.10" punct="pt:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>