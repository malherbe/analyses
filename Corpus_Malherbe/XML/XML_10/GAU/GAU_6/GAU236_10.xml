<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1838-1845</title>
				<title type="sub_2">Tome second</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>625 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1838-1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU236" modus="sp" lm_max="8" metProfile="8, 4" form="suite périodique" schema="3(ababcdcd)">
			<head type="main">DANS UN BAISER, L’ONDE…</head>
			<lg n="1" type="huitain" rhyme="ababcdcd">
				<l n="1" num="1.1" lm="8" met="8"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="1.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="1.3" punct="vg:4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="1.4">l</w>’<w n="1.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="1.7">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
				<l n="2" num="1.2" lm="4" met="4"><space quantity="4" unit="char"></space><w n="2.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w> <w n="2.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="2.3" punct="pv:4">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pv">eu</seg>rs</rhyme></w> ;</l>
				<l n="3" num="1.3" lm="8" met="8"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="3.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="3.5" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>v<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
				<l n="4" num="1.4" lm="4" met="4"><space quantity="4" unit="char"></space><w n="4.1">L</w>’<w n="4.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="4.5" punct="pv:4">pl<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pv">eu</seg>rs</rhyme></w> ;</l>
				<l n="5" num="1.5" lm="8" met="8"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="5.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="5.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="5.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.7">pl<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
				<l n="6" num="1.6" lm="4" met="4"><space quantity="4" unit="char"></space><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="6.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="6.3" punct="vg:4">c<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>pr<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" punct="vg">è</seg>s</rhyme></w>,</l>
				<l n="7" num="1.7" lm="8" met="8"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="7.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="7.4">t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>b<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>th<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
				<l n="8" num="1.8" lm="4" met="4"><space quantity="4" unit="char"></space><w n="8.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>gs</w> <w n="8.3" punct="pt:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>gr<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" punct="pt">e</seg>ts</rhyme></w>.</l>
			</lg>
			<lg n="2" type="huitain" rhyme="ababcdcd">
				<l n="9" num="2.1" lm="8" met="8"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="9.2">fl<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>ts</w> <w n="9.3" punct="vg:4">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>ts</w>, <w n="9.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="9.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="9.6" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>p<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
				<l n="10" num="2.2" lm="4" met="4"><space quantity="4" unit="char"></space><w n="10.1">H<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rs</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="10.3" punct="vg:4">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>r</rhyme></w>,</l>
				<l n="11" num="2.3" lm="8" met="8"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="11.2">l<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="11.3" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="11.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="11.7">c<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
				<l n="12" num="2.4" lm="4" met="4"><space quantity="4" unit="char"></space><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="12.3" punct="pt:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>l<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pt">eu</seg>r</rhyme></w>.</l>
				<l n="13" num="2.5" lm="8" met="8"><w n="13.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="13.2">d<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.3" punct="vg:4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>c</w>, <w n="13.4">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w>-<w n="13.5" punct="vg:8">S<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ph<rhyme label="c" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
				<l n="14" num="2.6" lm="4" met="4"><space quantity="4" unit="char"></space><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="14.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="14.4" punct="vg:4">bl<rhyme label="d" id="8" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg></rhyme></w>,</l>
				<l n="15" num="2.7" lm="8" met="8"><w n="15.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="15.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="15.3" punct="vg:4">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>r</w>, <w n="15.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="15.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>f<rhyme label="c" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
				<l n="16" num="2.8" lm="4" met="4"><space quantity="4" unit="char"></space><w n="16.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="16.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="16.4" punct="pt:4">Di<rhyme label="d" id="8" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="pt">eu</seg></rhyme></w>.</l>
			</lg>
			<lg n="3" type="huitain" rhyme="ababcdcd">
				<l n="17" num="3.1" lm="8" met="8"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>rbr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg></w> <w n="17.3" punct="vg:4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="vg">eau</seg></w>, <w n="17.4">c<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="17.6" punct="vg:8">r<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
				<l n="18" num="3.2" lm="4" met="4"><space quantity="4" unit="char"></space><w n="18.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="18.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg></w> <w n="18.3" punct="vg:4">r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ch<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></rhyme></w>,</l>
				<l n="19" num="3.3" lm="8" met="8"><w n="19.1" punct="vg:1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>t</w>, <w n="19.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w>-<w n="19.3" punct="vg:4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>s</w>, <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="19.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="19.6">ch<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
				<l n="20" num="3.4" lm="4" met="4"><space quantity="4" unit="char"></space><w n="20.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="20.2">s</w>’<w n="20.3" punct="ps:4"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ch<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="ps">er</seg></rhyme></w>…</l>
				<l n="21" num="3.5" lm="8" met="8"><w n="21.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="21.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="21.4" punct="vg:4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="21.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="21.6">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></w> <w n="21.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="21.8">m<rhyme label="c" id="11" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
				<l n="22" num="3.6" lm="4" met="4"><space quantity="4" unit="char"></space><w n="22.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="22.3" punct="vg:4">r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<rhyme label="d" id="12" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>d</rhyme></w>,</l>
				<l n="23" num="3.7" lm="8" met="8"><w n="23.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1">en</seg></w> <w n="23.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="23.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="23.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>x</w> <w n="23.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="23.7" punct="vg:8">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>f<rhyme label="c" id="11" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
				<l n="24" num="3.8" lm="4" met="4"><space quantity="4" unit="char"></space><w n="24.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="24.2" punct="pe:4">H<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>sp<rhyme label="d" id="12" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="pe">on</seg>t</rhyme></w> !</l>
			</lg>
			<closer>
				<dateline>
					<date when="1845">1845</date>.
				</dateline>
			</closer>
		</div></body></text></TEI>