<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU9" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="8(abab)">
				<head type="main">PREMIER SOURIRE DU PRINTEMPS</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="1.2">qu</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="1.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="1.5"><seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="1.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rv<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="2.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="2.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="2.4" punct="vg:8">h<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>ts</rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rs</w> <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="3.3" punct="vg:3">r<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>t</w>, <w n="3.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="3.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="3.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Pr<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="4.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.5" punct="pt:8">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="pt">em</seg>ps</rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="5.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="5.4" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rn<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4">en</seg>t</w> <w n="6.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="6.4" punct="vg:8">d<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>rt</rhyme></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="7.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="7.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="8.4">b<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s</w> <w n="8.5">d</w>’<w n="8.6" punct="pt:8"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pt">o</seg>r</rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rg<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="9.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.7" punct="vg:8">v<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="10.2">s</w>’<w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="10.4" punct="vg:3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg></w>, <w n="10.5">f<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>f</w> <w n="10.6" punct="vg:8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>qui<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="11.2"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-27" place="4">e</seg></w> <w n="11.3">h<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.5" punct="vg:8">c<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="493" place="8">y</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>dr<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="12.3">fr<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="12.4">l</w>’<w n="12.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>di<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></rhyme></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="13.2">n<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="13.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="13.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.6" punct="pv:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>p<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1" punct="vg:1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="14.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d</w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="14.4">j<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="14.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt</rhyme></w></l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="15.4">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.6">r<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="16.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="16.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.5">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rs</w> <w n="16.6" punct="pt:8">v<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" punct="pt">e</seg>rt</rhyme></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="17.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="17.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="17.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="17.5" punct="vg:8">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>lf<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">Qu</w>’<w n="18.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>x</w> <w n="18.3">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="18.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="18.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ffl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.6"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="18.7">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w>-<w n="18.8" punct="vg:8">v<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>x</rhyme></w>,</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="19.2">s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="19.4">pr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="19.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="19.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w>-<w n="19.7">n<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="384" place="8">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="20.3">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="20.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="20.5" punct="pt:8">b<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="21.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3" punct="vg:4">cr<seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="21.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="21.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="21.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="22.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="22.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rf</w> <w n="22.4" punct="vg:4">b<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>t</w>, <w n="22.5">l</w>’<w n="22.6"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="22.8" punct="vg:8">gu<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="vg">e</seg>t</rhyme></w>,</l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="23.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="23.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="23.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="23.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>gr<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="24.2">gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>ts</w> <w n="24.3">d</w>’<w n="24.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="24.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="24.6" punct="pt:8">m<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>gu<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pt">e</seg>t</rhyme></w>.</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="25.2">l</w>’<w n="25.3" punct="vg:3">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="25.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="25.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="25.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="25.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="25.8" punct="vg:8">c<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="œ" type="vs" value="1" rule="345" place="8">ue</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="26" num="7.2" lm="8" met="8"><w n="26.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="26.2">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="26.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="26.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="26.6">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6">ein</seg>t</w> <w n="26.7" punct="vg:8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rm<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="vg">e</seg>il</rhyme></w>,</l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="27.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="27.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="27.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="27.7">f<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="28" num="7.4" lm="8" met="8"><w n="28.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="28.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="28.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="28.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="28.5" punct="pt:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="pt">e</seg>il</rhyme></w>.</l>
				</lg>
				<lg n="8" type="quatrain" rhyme="abab">
					<l n="29" num="8.1" lm="8" met="8"><w n="29.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>s</w>, <w n="29.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="29.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="29.4">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="29.6" punct="vg:8">f<rhyme label="a" id="15" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="30" num="8.2" lm="8" met="8"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="30.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="30.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="30.4">r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="30.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="30.6" punct="vg:8">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<rhyme label="b" id="16" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>r</rhyme></w>,</l>
					<l n="31" num="8.3" lm="8" met="8"><w n="31.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="31.2">s<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>il</w> <w n="31.3">d</w>’<w n="31.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="31.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="31.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="31.7" punct="vg:8">t<rhyme label="a" id="15" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="32" num="8.4" lm="8" met="8"><w n="32.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="32.2" punct="dp:2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="dp in">i</seg>t</w> : «<w n="32.3" punct="vg:4">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="vg">em</seg>ps</w>, <w n="32.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="32.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="32.6" punct="pe:8">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<rhyme label="b" id="16" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg>r</rhyme></w> !»</l>
				</lg>
			</div></body></text></TEI>