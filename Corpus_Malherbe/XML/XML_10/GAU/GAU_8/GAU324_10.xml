<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU324" modus="cm" lm_max="10" metProfile="5+5" form="sonnet classique" schema="abba abba cdd cdc">
				<head type="main">LA FUMÉE</head>
				<head type="form">SONNET</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="1.3">f<seg phoneme="ɥi" type="vs" value="1" rule="462" place="4" mp="M">u</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>s</w><caesura></caesura> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="1.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="1.6" punct="vg:10">c<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>p<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="2.2">ch<seg phoneme="e" type="vs" value="1" rule="347" place="2" mp="P">ez</seg></w> <w n="2.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="2.6">s<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.7" punct="vg:10">gr<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="3.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="5" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="3.4">qu</w>’<w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="3.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7">ain</seg></w> <w n="3.7">l</w>’<w n="3.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="3.9"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>v<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="4.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>r</w><caesura></caesura> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="4.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="4.6" punct="pt:10">c<seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>p<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="10" met="5+5"><w n="5.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="P">a</seg>r</w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>r</w><caesura></caesura> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="5.6">l</w>’<w n="5.7">h<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>r</w> <w n="5.8" punct="vg:10">gr<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>p<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></rhyme></w>,</l>
					<l n="6" num="2.2" lm="10" met="5+5"><w n="6.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="6.3">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="6.4" punct="vg:5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5" punct="vg" caesura="1">en</seg></w>,<caesura></caesura> <w n="6.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="6.7">b<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="6.8" punct="pv:10">p<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="7" num="2.3" lm="10" met="5+5"><w n="7.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="7.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="7.3">br<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="7.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="7.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">am</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="7.7">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rps</w> <w n="7.8">s<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.9"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="7.10" punct="vg:10">m<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="8" num="2.4" lm="10" met="5+5"><w n="8.1">D</w>’<w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="8.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="8.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>gs</w> <w n="8.6">pl<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>s</w><caesura></caesura> <w n="8.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="8.8" punct="pt:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>l<seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg>pp<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" rhyme="cdd">
					<l n="9" num="3.1" lm="10" met="5+5"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="9.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="9.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" mp="M">oi</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="5" caesura="1">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="C">au</seg></w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="9.7" punct="vg:10">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rm<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="10" num="3.2" lm="10" met="5+5"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="10.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="10.3" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="10.4"><seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="C">i</seg>l</w> <w n="10.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="10.6" punct="vg:10">b<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>rge<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="vg">oi</seg>s</rhyme></w>,</l>
					<l n="11" num="3.3" lm="10" met="5+5"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="11.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="11.4">st<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" caesura="1">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="C">au</seg></w> <w n="11.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="11.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="11.8" punct="pv:10">B<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="pv">oi</seg>s</rhyme></w> ;</l>
				</lg>
				<lg n="4" rhyme="cdc">
					<l n="12" num="4.1" lm="10" met="5+5"><w n="12.1">J</w>’<w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="12.4">c<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="12.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="12.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8" mp="Lc">en</seg></w>-<w n="12.8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="9" mp="M/mc">ai</seg>m<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
					<l n="13" num="4.2" lm="10" met="5+5"><w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="13.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>t<seg phoneme="o" type="vs" value="1" rule="444" place="5" caesura="1">o</seg></w><caesura></caesura> <w n="13.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="P">a</seg>r</w> <w n="13.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="13.6" punct="vg:10">d<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="vg">oi</seg>gts</rhyme></w>,</l>
					<l n="14" num="4.3" lm="10" met="5+5"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">l</w>’<w n="14.3" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>r</w>, <w n="14.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>r</w> <w n="14.5" punct="vg:5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="vg" caesura="1">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="14.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="14.8" punct="pt:10">f<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>m<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1868">1868</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>