<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU218" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique" schema="abba abba ccd dee">
				<head type="main">SONNET</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="1.2">v<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2" mp="M">ei</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="1.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="1.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="1.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">â</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="1.8" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>c<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">J<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="2.4">d<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="2.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="2.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="2.7">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9" mp="M">im</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>z<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pv">u</seg>r</rhyme></w> ;</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="3.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="3.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="M">o</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6" caesura="1">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="3.5">d</w>’<w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="3.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>c</w> <w n="3.8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="10">en</seg></w> <w n="3.9">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="11">oin</seg>s</w> <w n="3.10">p<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r</rhyme></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="4.3">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>l</w> <w n="4.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="4.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">em</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="4.8">d</w>’<w n="4.9" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>g<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="5.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="5.3">pr<seg phoneme="y" type="vs" value="1" rule="453" place="3" mp="M">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="5.4">h<seg phoneme="y" type="vs" value="1" rule="453" place="5" mp="M">u</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="5.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>x</w> <w n="5.7">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="9" mp="M">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="5.8" punct="pv:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>cl<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="6.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="6.4">r<seg phoneme="o" type="vs" value="1" rule="435" place="4" mp="M">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>l</w><caesura></caesura> <w n="6.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="6.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="6.8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>x</w> <w n="6.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="6.10" punct="vg:12">d<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>r</rhyme></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="7.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="7.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="7.7">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="7.8">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>l</w> <w n="7.9" punct="vg:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>bsc<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>r</rhyme></w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="8.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="8.5">l<seg phoneme="y" type="vs" value="1" rule="453" place="6" caesura="1">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="8.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="8.8">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="8.9">d</w>’<w n="8.10" punct="pt:12"><seg phoneme="u" type="vs" value="1" rule="d-2" place="11" mp="M">ou</seg><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="9.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="9.3">d</w>’<w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="9.5">br<seg phoneme="y" type="vs" value="1" rule="453" place="5" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="9.6">r<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="9.7" punct="pv:12">m<seg phoneme="wa" type="vs" value="1" rule="192" place="9" mp="M">oe</seg>ll<seg phoneme="ø" type="vs" value="1" rule="403" place="10" mp="M">eu</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12" punct="pv">en</seg>t</rhyme></w> ;</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="10.4">t<seg phoneme="a" type="vs" value="1" rule="307" place="5" mp="M">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="10.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="10.6">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="10.7">n<seg phoneme="e" type="vs" value="1" rule="347" place="10">ez</seg></w> <w n="10.8" punct="pv:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rm<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pv">an</seg>t</rhyme></w> ;</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="11.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="11.5">r<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>g<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="11.7">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="11.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="11.10" punct="pv:12">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">am</seg>b<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
				</lg>
				<lg n="4" rhyme="dee">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="12.2">m<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>ts</w> <w n="12.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="12.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" caesura="1">ein</seg>s</w><caesura></caesura> <w n="12.5">d</w>’<w n="12.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="12.7">gr<seg phoneme="a" type="vs" value="1" rule="340" place="9">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="12.8" punct="vg:12">ch<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>n<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="13.3">d</w>’<w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="13.6">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="13.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="13.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="13.9">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="13.10">b<seg phoneme="o" type="vs" value="1" rule="315" place="11" mp="M">eau</seg>t<rhyme label="e" id="7" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></w></l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="14.2">ch<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="14.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>x</w><caesura></caesura> <w n="14.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="14.6">l</w>’<w n="14.7"><seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="14.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="14.9" punct="pt:12">th<rhyme label="e" id="7" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>