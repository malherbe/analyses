<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="AGT">
					<name>
						<forename>Marie</forename>
						<nameLink>d’</nameLink>
						<surname>AGOULT</surname>
						<addName type="pen_name">Daniel STERN</addName>
					</name>
					<date from="1805" to="1876">1805-1876</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>185 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AGT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ESQUISSES MORALES : PENSÉES, RÉFLEXIONS, MAXIMES</title>
						<author>Daniel STERN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://catalogue.bnf.fr/ark:/12148/cb30005117k</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ESQUISSES MORALES : PENSÉES, RÉFLEXIONS, MAXIMES</title>
							<author>Daniel STERN</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>CALMANN LÉVY, ÉDITEUR</publisher>
									<date when=" 1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1880">1880</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition ne porte que sur le chapitre consacrée aux poésies.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES</head><div type="poem" key="AGT5" modus="cm" lm_max="12" metProfile="6+6" form="sonnet non classique" schema="abba cdcd eff egg">
					<head type="number">V</head>
					<head type="main">DIEU MUET</head>
					<head type="form">SONNET</head>
					<opener>
						<salute>A mon ami Charles Dolfus.</salute>
					</opener>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="1.2">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="1.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="1.5" punct="vg:6">g<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg>s</w>,<caesura></caesura> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.7">s<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9">e</seg>il</w> <w n="1.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="1.9" punct="pv:12">tr<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>st<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="2.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="2.3">j<seg phoneme="wa" type="vs" value="1" rule="440" place="3" mp="M">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="2.4">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" mp="M">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">em</seg>ps</w><caesura></caesura> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="2.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="2.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="2.9" punct="pv:12">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>gu<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pv">eu</seg>rs</rhyme></w> ;</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">b<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="3.3">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.5" punct="vg:6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="3.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>s</w> <w n="3.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="3.10">r<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>gu<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>rs</rhyme></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">S</w>’<w n="4.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="4.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="4.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="4.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="4.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="4.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="4.9" punct="pt:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>g<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" rhyme="cdcd">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="5.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="5.3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg></w> <w n="5.4">d</w>’<w n="5.5" punct="pt:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pt" caesura="1">er</seg></w>.<caesura></caesura> <w n="5.6">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.7">d<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>il</w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="5.9">l</w>’<w n="5.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>gr<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="6.4" punct="vg:6">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>r</w>,<caesura></caesura> <w n="6.5">l</w>’<w n="6.6"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg>x</w> <w n="6.8" punct="vg:12">d<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>c<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>rs</rhyme></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="7.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="7.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="6" punct="vg" caesura="1">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="7.9">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.10"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>vr<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">N</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="8.3">qu</w>’<w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="8.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="8.6">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="8.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="8.8">s</w>’<w n="8.9"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="9">ein</seg>t</w> <w n="8.10">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="8.11">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="8.12" punct="pt:12">pl<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>rs</rhyme></w>.</l>
					</lg>
					<lg n="3" rhyme="eff">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">c</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="9.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="4">oi</seg></w> <w n="9.5" punct="vg:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>rs</w>,<caesura></caesura> <w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="9.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="9.8" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" mp="M">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<rhyme label="e" id="5" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">L</w>’<w n="10.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.3" punct="vg:3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>sc<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="10.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="10.6" punct="vg:6">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg" caesura="1">ai</seg>t</w>,<caesura></caesura> <w n="10.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="10.8">l</w>’<w n="10.9"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="10.10" punct="vg:12">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="f" id="6" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="vg">en</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="11.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="11.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="11.4">r<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="11.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l</w> <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="11.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="11.10" punct="vg:12">bi<rhyme label="f" id="6" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="12" punct="vg">en</seg></rhyme></w>,</l>
					</lg>
					<lg n="4" rhyme="egg">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="12.6" punct="vg:6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg" caesura="1">o</seg>rt</w>,<caesura></caesura> <w n="12.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="12.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="12.9">b<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="e" id="5" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="13.2">pr<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="13.4" punct="vg:6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="13.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="13.7">d</w>’<w n="13.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="13.9">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg></w> <w n="13.10">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="11" mp="M">u</seg><rhyme label="g" id="7" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12">e</seg>t</rhyme></w></l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="14.2">d<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="14.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="14.4">l</w>’<w n="14.5"><seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>tr<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="14.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="14.7">b<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="14.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="14.9" punct="pt:12">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="11" mp="M">en</seg>f<rhyme label="g" id="7" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pt">ai</seg>t</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>