<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODES, ÉPIGRAMMES ET AUTRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>633 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1664" to="1704">1664-1704</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI76" modus="cp" lm_max="12" metProfile="6+6, (8)" form="strophe unique" schema="1(abba)">
				<head type="main">ÉPIGRAMME</head>
				<head type="sub">imitée de celle de Martial</head>
				<head type="sub_1"> qui commence par : Nuper erat medicus, etc</head>
				<lg n="1" type="quatrain" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="vg:1">P<seg phoneme="o" type="vs" value="1" rule="318" place="1" punct="vg">au</seg>l</w>, <w n="1.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="1.4" punct="vg:6">m<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg" caesura="1">in</seg></w>,<caesura></caesura> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="e" type="vs" value="1" rule="353" place="7" mp="M">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg></w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="1.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="1.9" punct="vg:12">qu<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rti<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">er</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="2.2">c<seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="2.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="2.5">m<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>x</w><caesura></caesura> <w n="2.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="2.8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="2.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="2.11" punct="vg:12">gu<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="3.2">c<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="3.3" punct="vg:6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" mp="M">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.5">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>t</w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="3.7">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="10">en</seg>s</w> <w n="3.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="3.9" punct="pt:12">t<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="4" num="1.4" lm="8"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="4.2">n</w>’<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>t</w> <w n="4.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.7" punct="pt:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ti<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>