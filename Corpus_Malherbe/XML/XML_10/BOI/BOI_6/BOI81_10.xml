<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODES, ÉPIGRAMMES ET AUTRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>633 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1664" to="1704">1664-1704</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI81" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(abbacddc)">
				<head type="main">ÉPIGRAMME</head>
				<head type="sub">L’amateur d’horloges</head>
				<opener>
					<dateline>
						<date when="1704">(1704)</date>
					</dateline>
				</opener>
				<lg n="1" type="huitain" rhyme="abbacddc">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="1.2" punct="vg:2">c<seg phoneme="ɛ" type="vs" value="1" rule="352" place="2" punct="vg">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>x</w> <w n="1.6" punct="vg:8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="2.3" punct="vg:4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="2.6" punct="vg:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>dr<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="vg">an</seg>s</rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1" punct="vg:2">L<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>b<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" punct="vg">in</seg></w>, <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s</w> <w n="3.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="3.5">qu<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6" punct="vg:8"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="vg">an</seg>s</rhyme></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="435" place="1">O</seg>cc<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.3">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg>s</w> <w n="4.4" punct="pt:8">r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>c<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="5.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4" punct="vg:5">m<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></w>, <w n="5.5">s</w>’<w n="5.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="5.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="5.8" punct="vg:8">pl<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">aî</seg>t</rhyme></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w>-<w n="6.2">t</w>-<w n="6.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>cqu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s</w> <w n="6.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.6" punct="pi:8">sc<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="d" id="4" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="378" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="7.2" punct="pv:2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="pv">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="7.4">c</w>’<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="7.6">l</w>’<w n="7.7">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.9">Fr<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="8.5">l</w>’<w n="8.6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.7">qu</w>’<w n="8.8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="8.9" punct="pt:8"><rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8" punct="pt">e</seg>st</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>