<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">APPENDICE</title>
				<title type="sub_2">À L’ÉDITION DES ŒUVRES POÉTIQUES (édition Hachette, 1889)</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>125 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1654" to="1705">1654-1705</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI87" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede">
				<head type="number">II</head>
				<head type="main">Sonnet sur la mort d’une de mes parentes</head>
				<opener>
					<dateline>
						<date when="1663">(1663)</date>
					</dateline>
				</opener>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="1.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="1.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rts</w><caesura></caesura> <w n="1.5">d</w>’<w n="1.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="1.8" punct="vg:12">f<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>d<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">v<seg phoneme="wa" type="vs" value="1" rule="440" place="2" mp="M">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="2.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="2.4">d</w>’<w n="2.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">I</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="2.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="2.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="2.8">h<seg phoneme="œ" type="vs" value="1" rule="407" place="10" mp="M">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>x</w> <w n="2.9" punct="dp:12">j<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="dp">ou</seg>rs</rhyme></w> :</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:2"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">I</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s</w>, <w n="3.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3">j</w>’<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg" caesura="1">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="3.8">j</w>’<w n="3.9"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="9" mp="M">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="306" place="10">ai</seg></w> <w n="3.10" punct="vg:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>j<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>rs</rhyme></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Br<seg phoneme="y" type="vs" value="1" rule="445" place="1" mp="M">û</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="4.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="4.3">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="4.4">f<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="4.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="4.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="4.7">br<seg phoneme="y" type="vs" value="1" rule="445" place="9" mp="M">û</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="4.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="P">ou</seg>r</w> <w n="4.9" punct="pv:12"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="vg:1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" punct="vg">an</seg>d</w>, <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="P">a</seg>r</w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="5.6" punct="vg:6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="vg" caesura="1">e</seg>l</w>,<caesura></caesura> <w n="5.7"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="5.8">fi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.9">cr<seg phoneme="y" type="vs" value="1" rule="454" place="11" mp="M">u</seg><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">M</w>’<w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">en</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="6.3">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" mp="C">e</seg>t</w> <w n="6.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>bj<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="6.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="6.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="6.8" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pv">ou</seg>rs</rhyme></w> ;</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="7.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="7.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="7.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>rs</w><caesura></caesura> <w n="7.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">om</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="7.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="7.8" punct="vg:12">c<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>rs</rhyme></w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="8.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>ts</w><caesura></caesura> <w n="8.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="8.6">su<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rn<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="9.2">qu</w>’<w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="9.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="9.5">r<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="9.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>p</w><caesura></caesura> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="o" type="vs" value="1" rule="435" place="8" mp="M">o</seg>nn<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="9.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="9.9" punct="pe:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>spr<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pe">i</seg>ts</rhyme></w> !</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="10.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="10.5" punct="pe:6">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pe" caesura="1">eu</seg>rs</w> !<caesura></caesura> <w n="10.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="10.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306" place="10">ai</seg></w> <w n="10.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="10.10" punct="pe:12">cr<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pe">i</seg>s</rhyme></w> !</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="11.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="11.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="11.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="11.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="11.7">f<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>t</w> <w n="11.8" punct="pe:12">su<seg phoneme="i" type="vs" value="1" rule="491" place="11" mp="M">i</seg>v<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="4" rhyme="ede">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1" punct="vg:2"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">I</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s</w>, <w n="12.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="12.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rs</w><caesura></caesura> <w n="12.5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg>s</w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="12.7">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9">ain</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="12.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="12.9" punct="pv:12">m<rhyme label="e" id="7" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pv">oi</seg></rhyme></w> ;</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="13.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="13.3">qu</w>’<w n="13.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="13.5">tr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="13.6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="13.7">t</w>’<w n="13.8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="13.9">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="13.10">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="13.11">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="13.12" punct="vg:12">v<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="14.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="14.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="14.5">j</w>’<w n="14.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="14.7">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="14.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="14.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="14.10" punct="pt:12">t<rhyme label="e" id="7" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pt">oi</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>