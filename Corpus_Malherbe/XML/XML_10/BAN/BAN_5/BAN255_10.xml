<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN255" modus="sm" lm_max="7" metProfile="7" form="suite périodique" schema="11(abab)">
				<head type="number">LXIII</head>
				<head type="main">A l’Hiver</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">H<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>r</w> <w n="1.2" punct="vg:4">b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>z<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="1.3">h<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>r</w> <w n="1.4" punct="vg:7">ti<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="2.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="2.4">ch<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>d</w> <w n="2.5" punct="vg:7">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ffl<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1">F<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>x</w> <w n="3.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4" punct="vg:7">S<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="wa" type="vs" value="1" rule="440" place="6">o</seg>y<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="7" met="7"><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="1">E</seg>s</w>-<w n="4.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="4.3">l</w>’<w n="4.4" punct="vg:4">h<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" punct="vg">e</seg>r</w>, <w n="4.5"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="4.6">l</w>’<w n="4.7" punct="pi:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pi">é</seg></rhyme></w> ?</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="7" met="7"><w n="5.1" punct="vg:2">V<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>s</w>, <w n="5.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w>-<w n="5.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="5.4">qu</w>’<w n="5.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="5.6">s</w>’<w n="5.7">h<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="6" num="2.2" lm="7" met="7"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2" punct="vg:4">m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="6.3"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.5" punct="pi:7">v<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="pi">ai</seg>r</rhyme></w> ?</l>
					<l n="7" num="2.3" lm="7" met="7"><w n="7.1" punct="pt:1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pt">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>. <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">E</seg>xpl<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w>-<w n="7.3" punct="pt:5">t<seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="pt">oi</seg></w>. <w n="7.4" punct="pt:7">B<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
					<l n="8" num="2.4" lm="7" met="7"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="8.3" punct="pt:3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3" punct="pt">en</seg></w>. <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="4">E</seg>s</w>-<w n="8.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="8.6">l</w>’<w n="8.7" punct="pi:7">h<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>v<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" punct="pi">e</seg>r</rhyme></w> ?</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="7" met="7"><w n="9.1" punct="pt:1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="pt">on</seg></w>. <w n="9.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="vg">o</seg>rs</w>, <w n="9.3">f<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="9.5">gl<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="10" num="3.2" lm="7" met="7"><w n="10.1" punct="vg:1"><seg phoneme="u" type="vs" value="1" rule="426" place="1" punct="vg">Où</seg></w>, <w n="10.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="10.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="10.4">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ts</w> <w n="10.5" punct="vg:7">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" punct="vg">in</seg>s</rhyme></w>,</l>
					<l n="11" num="3.3" lm="7" met="7"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="11.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.4">W<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rth</w> <w n="11.5">l<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="12" num="3.4" lm="7" met="7"><w n="12.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="12.4" punct="pe:7">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" punct="pe">in</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="7" met="7"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="13.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.4">n<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="384" place="7">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="14" num="4.2" lm="7" met="7"><w n="14.1" punct="vg:1"><seg phoneme="u" type="vs" value="1" rule="426" place="1" punct="vg">Où</seg></w>, <w n="14.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="14.5" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>rc<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg></rhyme></w>,</l>
					<l n="15" num="4.3" lm="7" met="7"><w n="15.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="16" num="4.4" lm="7" met="7"><w n="16.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="16.2" punct="vg:3">d<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="16.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="16.4" punct="pe:7"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>ss<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pe">i</seg></rhyme></w> !</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="7" met="7"><w n="17.1">D<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="17.3">s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="17.4" punct="pt:7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>d<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
					<l n="18" num="5.2" lm="7" met="7"><w n="18.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">aî</seg>t</w>-<w n="18.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="18.4">d</w>’<w n="18.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="18.6">l</w>’<w n="18.7" punct="pi:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pi">é</seg></rhyme></w> ?</l>
					<l n="19" num="5.3" lm="7" met="7"><w n="19.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="19.3">S<seg phoneme="ɛ" type="vs" value="1" rule="385" place="3">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="20" num="5.4" lm="7" met="7"><w n="20.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.3" punct="pe:7">Gu<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pe">é</seg></rhyme></w> !</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="7" met="7"><w n="21.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="21.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="21.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="21.4" punct="vg:7"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>v<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="22" num="6.2" lm="7" met="7"><w n="22.1">D<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="22.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="22.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="22.5">fr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ss<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s</rhyme></w></l>
					<l n="23" num="6.3" lm="7" met="7"><w n="23.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="23.2">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="23.3">f<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="23.4" punct="vg:7">v<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="24" num="6.4" lm="7" met="7"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="24.2">qu</w>’<w n="24.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ls</w> <w n="24.4">s<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="24.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg>s</w> <w n="24.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="24.7" punct="pe:7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>s<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="pe">on</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="7" met="7"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="25.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="25.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="25.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="25.5" punct="vg:7">r<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="26" num="7.2" lm="7" met="7"><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="26.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="26.3">L<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="26.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="26.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rm<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</rhyme></w></l>
					<l n="27" num="7.3" lm="7" met="7"><w n="27.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="27.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="27.3">f<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>ts</w> <w n="27.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><rhyme label="a" id="13" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></w></l>
					<l n="28" num="7.4" lm="7" met="7"><w n="28.1">F<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="28.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="28.4" punct="pe:7"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="pe">an</seg>t</rhyme></w> !</l>
				</lg>
				<lg n="8" type="quatrain" rhyme="abab">
					<l n="29" num="8.1" lm="7" met="7"><w n="29.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="29.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="29.3" punct="pt:7">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d<rhyme label="a" id="15" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
					<l n="30" num="8.2" lm="7" met="7"><w n="30.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="30.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>squ</w>’<w n="30.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="30.4" punct="vg:5">pr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" punct="vg">en</seg>t</w>, <w n="30.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="30.6" punct="vg:7">ch<rhyme label="b" id="16" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" punct="vg">e</seg>r</rhyme></w>,</l>
					<l n="31" num="8.3" lm="7" met="7"><w n="31.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="31.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="31.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="31.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="31.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="31.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="31.7" punct="vg:7">d<rhyme label="a" id="15" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="32" num="8.4" lm="7" met="7"><w n="32.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="32.2">n</w>’<w n="32.3"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="2">e</seg>s</w> <w n="32.4">n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="32.5" punct="vg:5">p<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg">on</seg></w>, <w n="32.6">n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="32.7" punct="pt:7">ch<rhyme label="b" id="16" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="pt">ai</seg>r</rhyme></w>.</l>
				</lg>
				<lg n="9" type="quatrain" rhyme="abab">
					<l n="33" num="9.1" lm="7" met="7"><w n="33.1">J</w>’<w n="33.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="33.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="33.4">T<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<rhyme label="a" id="17" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="34" num="9.2" lm="7" met="7"><w n="34.1">D<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>t</w> <w n="34.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="34.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="34.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t<rhyme label="b" id="18" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="307" place="7">a</seg>il</rhyme></w></l>
					<l n="35" num="9.3" lm="7" met="7"><w n="35.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="35.2">g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="35.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="35.4" punct="pt:7">ch<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>ff<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<rhyme label="a" id="17" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
					<l n="36" num="9.4" lm="7" met="7"><w n="36.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="36.2">h<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="36.3" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<rhyme label="b" id="18" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="307" place="7" punct="vg">a</seg>il</rhyme></w>,</l>
				</lg>
				<lg n="10" type="quatrain" rhyme="abab">
					<l n="37" num="10.1" lm="7" met="7"><w n="37.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="37.2">h<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="37.3" punct="vg:5">cr<seg phoneme="y" type="vs" value="1" rule="454" place="4">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="vg">e</seg>l</w>, <w n="37.4" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>bs<rhyme label="a" id="19" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="38" num="10.2" lm="7" met="7"><w n="38.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="38.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="38.3">f<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="38.4">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rgn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="38.6" punct="vg:7">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ch<rhyme label="b" id="20" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="438" place="7" punct="vg">o</seg>t</rhyme></w>,</l>
					<l n="39" num="10.3" lm="7" met="7"><w n="39.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="39.2">h<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="39.3">ch<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="39.4"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="39.5" punct="vg:7">k<rhyme label="a" id="19" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="40" num="10.4" lm="7" met="7"><w n="40.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>ffl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="40.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="40.3">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>d</w> <w n="40.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="40.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="40.6" punct="vg:7">ch<rhyme label="b" id="20" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="7" punct="vg">au</seg>d</rhyme></w>,</l>
				</lg>
				<lg n="11" type="quatrain" rhyme="abab">
					<l n="41" num="11.1" lm="7" met="7"><w n="41.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="41.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="41.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="41.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="41.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="41.6">fou<rhyme label="a" id="21" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="42" num="11.2" lm="7" met="7"><w n="42.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="42.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="42.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="42.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">E</seg>scl<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<rhyme label="b" id="22" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s</rhyme></w></l>
					<l n="43" num="11.3" lm="7" met="7"><w n="43.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="43.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="43.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="43.4" punct="vg:7">g<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="u" type="vs" value="1" rule="d-2" place="6">ou</seg><rhyme label="a" id="21" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="44" num="11.4" lm="7" met="7"><w n="44.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="44.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="44.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="44.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="44.5" punct="pe:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<rhyme label="b" id="22" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="pe">on</seg>s</rhyme></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">12 février 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>