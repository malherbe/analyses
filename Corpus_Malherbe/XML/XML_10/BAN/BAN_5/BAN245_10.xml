<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN245" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="9(abab)">
				<head type="number">LIII</head>
				<head type="main">Un jeune homme</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <hi rend="ital"><w n="1.2">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="1.3">N<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w></hi> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>t</rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ds</w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ts</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.6" punct="pt:8">r<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="3.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="3.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="3.5" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg></w>, <w n="3.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.7">l</w>’<w n="3.8" punct="pe:8"><rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8" punct="pe">e</seg>st</rhyme></w> !</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="4.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="4.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="4.4">qu</w>’<w n="4.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.8">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="7">oi</seg></w> <w n="4.9" punct="pt:8">fr<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2">n</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="5.6">D<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="391" place="8">eu</seg></rhyme></w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="6.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="6.4" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>t<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="7.4">tr<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="7.5">ch<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="7.6">D<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></rhyme></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">L</w>’<w n="8.2" punct="vg:2">h<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>r</w>, <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="8.4">l</w>’<w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="8.7">l</w>’<w n="8.8" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>st<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <hi rend="ital"><w n="9.2">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="9.3">N<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w></hi> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>t</rhyme></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="10.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="2">ein</seg></w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">j<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.7" punct="pv:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>pr<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></w> ;</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">n</w>’<w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="11.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="11.6">b<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="11.8">l<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</rhyme></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">Qu</w>’<w n="12.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="12.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="5">ein</seg>s</w> <w n="12.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="12.7" punct="pt:8">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rr<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2" punct="vg:4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="13.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="13.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</rhyme></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="14.2" punct="vg:2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>f</w>, <w n="14.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.4">l</w>’<w n="14.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>fr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="14.6" punct="vg:8">n<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">d<seg phoneme="u" type="vs" value="1" rule="d-2" place="2">ou</seg><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="15.3">d</w>’<w n="15.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>t</rhyme></w>,</l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="16.2" punct="vg:2">b<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>t</w>, <w n="16.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="16.5">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>f</w> <w n="16.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="16.7" punct="pt:8">b<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="17.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="17.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="17.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="17.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="17.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><rhyme label="a" id="9" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</rhyme></w></l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg></w> <w n="18.5">r<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="18.6" punct="pv:8"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>p<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2" punct="vg:2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="vg">u</seg>s</w>, <w n="19.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="19.5">sp<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><rhyme label="a" id="9" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</rhyme></w></l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="20.3" punct="vg:4">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="20.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="20.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="20.6" punct="pt:8">p<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="21.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="21.3">g<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5">en</seg>t</w> <w n="21.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="21.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>s<rhyme label="a" id="11" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></rhyme></w></l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="22.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="22.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ls</w> <w n="22.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="22.6" punct="vg:8">L<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="b" id="12" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="23.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="23.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="23.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="23.6" punct="dp:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>s<rhyme label="a" id="11" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="dp">on</seg></rhyme></w> :</l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="24.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="24.3">cr<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="24.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="24.6" punct="pe:8">t<rhyme label="b" id="12" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="25.2" punct="vg:2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>t</w>, <w n="25.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg></w> <w n="25.4">d</w>’<w n="25.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="25.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="25.7" punct="dp:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<rhyme label="a" id="13" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="dp">in</seg></rhyme></w> :</l>
					<l n="26" num="7.2" lm="8" met="8"><w n="26.1">Di<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="26.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="26.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="26.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="26.5">d</w>’<w n="26.6" punct="pe:8"><seg phoneme="o" type="vs" value="1" rule="444" place="7">O</seg>r<rhyme label="b" id="14" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="27.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="27.3">d</w>’<w n="27.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rd</w> <w n="27.5">g<seg phoneme="u" type="vs" value="1" rule="425" place="5">oû</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="27.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="27.7" punct="dp:8">v<rhyme label="a" id="13" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="dp">in</seg></rhyme></w> :</l>
					<l n="28" num="7.4" lm="8" met="8"><w n="28.1">D</w>’<w n="28.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="28.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="28.4" punct="vg:5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg">on</seg>t</w>, <w n="28.5">s</w>’<w n="28.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="28.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="28.8" punct="pt:8">r<rhyme label="b" id="14" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="8" type="quatrain" rhyme="abab">
					<l n="29" num="8.1" lm="8" met="8"><w n="29.1">Fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="29.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="29.3">r<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="29.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="29.6" punct="vg:8">gl<seg phoneme="a" type="vs" value="1" rule="343" place="7">a</seg>ï<rhyme label="a" id="15" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>l</rhyme></w>,</l>
					<l n="30" num="8.2" lm="8" met="8"><w n="30.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="30.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="30.3">n<seg phoneme="a" type="vs" value="1" rule="343" place="3">a</seg><seg phoneme="i" type="vs" value="1" rule="477" place="4">ï</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="30.4" punct="vg:8">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>mi<rhyme label="b" id="16" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="31" num="8.3" lm="8" met="8"><w n="31.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="31.2" punct="vg:3">s<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>t</w>, <w n="31.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="31.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="31.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="343" place="7">a</seg>ï<rhyme label="a" id="15" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>l</rhyme></w>,</l>
					<l n="32" num="8.4" lm="8" met="8"><w n="32.1">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="32.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="32.3">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>ts</w> <w n="32.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="32.5" punct="pt:8">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>mi<rhyme label="b" id="16" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="9" type="quatrain" rhyme="abab">
					<l n="33" num="9.1" lm="8" met="8"><w n="33.1">S</w>’<w n="33.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="33.3">b<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="33.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="33.5">qu</w>’<w n="33.6"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="33.7">n</w>’<w n="33.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="33.9">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>t</w> <w n="33.10" punct="vg:8">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt<rhyme label="a" id="17" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></w>,</l>
					<l n="34" num="9.2" lm="8" met="8"><w n="34.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="34.2">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="34.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="34.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="34.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></w> <w n="34.6">n</w>’<w n="34.7" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>t<rhyme label="b" id="18" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="35" num="9.3" lm="8" met="8"><w n="35.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="35.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="35.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="35.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="35.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<rhyme label="a" id="17" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></rhyme></w></l>
					<l n="36" num="9.4" lm="8" met="8"><w n="36.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="36.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="36.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="36.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="36.5">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w>-<w n="36.6" punct="pt:8">D<rhyme label="b" id="18" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">1er février 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>