<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN210" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="6(abab)">
				<head type="number">XVIII</head>
				<head type="main">A l’Opéra</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="1.2">l</w>’<w n="1.3" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="444" place="2">O</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></w>, <w n="1.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="1.6" punct="vg:8">M<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="2.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="2.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="2.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="354" place="7">e</seg>x<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>ls</rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">J<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="3.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.5">ph<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>s<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="4.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="2">en</seg>s</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ff<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="4.5" punct="pv:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>bt<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pv">i</seg>ls</rhyme></w> ;</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="5.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="5.5">phr<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">V<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w> <w n="6.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="6.3" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></w>, <w n="6.4" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="353" place="6">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg>s</rhyme></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.5">l</w>’<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>xt<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="8.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="8.4">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="8.5" punct="pv:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pv">é</seg>s</rhyme></w> ;</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1" punct="vg:1">F<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="vg">o</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="9.4" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>tr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="10.2" punct="vg:3">B<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg></w>, <w n="10.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.4">fr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="10.5" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>p<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>s</rhyme></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="11.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.6" punct="dp:8">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="12.2" punct="pt:4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="pt">oi</seg></w>. <w n="12.3">N</w>’<w n="12.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="12.5" punct="pt:8">p<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="13.2">Ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">l<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>s</w> <w n="13.5">m<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="13.7">pr<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="14.6">l</w>’<w n="14.7" punct="pv:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>s<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pv">on</seg></rhyme></w> ;</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="15.3">L<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="15.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.6">r<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1" punct="dp:1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="dp">i</seg>t</w> : <w n="16.2">C</w>’<w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="16.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="16.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="16.6">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="16.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="16.8" punct="pt:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>s<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></rhyme></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>r</w>, <w n="17.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>qu<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="18.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="18.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="18.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l</w> <w n="18.5" punct="vg:8">f<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="vg">ou</seg></rhyme></w>,</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="19.2">d</w>’<w n="19.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="19.4">l</w>’<w n="19.5"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rch<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="20.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="20.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="20.5">d</w>’<w n="20.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="20.7" punct="pv:8">c<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="pv">ou</seg></rhyme></w> ;</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">P<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="21.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="21.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rs</w> <w n="21.6" punct="vg:8">c<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="22.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="22.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.4" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">E</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="a" type="vs" value="1" rule="343" place="7">a</seg><rhyme label="b" id="12" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="476" place="8" punct="vg">ï</seg>s</rhyme></w>,</l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>dm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="23.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="23.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="23.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="23.5">p<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>ss<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2" punct="vg">è</seg>s</w>, <w n="24.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="24.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="24.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="24.5">qu</w>’<w n="24.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="24.7" punct="pt:8">L<seg phoneme="a" type="vs" value="1" rule="343" place="7">a</seg><rhyme label="b" id="12" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="476" place="8" punct="pt">ï</seg>s</rhyme></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">14 décembre 1883.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>