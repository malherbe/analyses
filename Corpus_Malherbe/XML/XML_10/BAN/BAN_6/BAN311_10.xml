<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODELETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1529 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ODELETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvilleodelettes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1856">1856</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN311" modus="sp" lm_max="7" metProfile="7, 5" form="suite périodique" schema="4(ababcccb)">
				<head type="main">A Eugène Grangé</head>
				<lg n="1" type="huitain" rhyme="ababcccb">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="1.4">g<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="1.5">Th<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>sp<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</rhyme></w></l>
					<l n="2" num="1.2" lm="5" met="5"><space quantity="6" unit="char"></space><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="2.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rm<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="5">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w></l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="3.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6">l<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>s</rhyme></w></l>
					<l n="4" num="1.4" lm="5" met="5"><space quantity="6" unit="char"></space><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="4.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="4.4" punct="pt:5">bl<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>m<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="5">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></w>.</l>
					<l n="5" num="1.5" lm="7" met="7"><w n="5.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2">B<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>cch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="5.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>x</w> <w n="5.5">y<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</rhyme></w></l>
					<l n="6" num="1.6" lm="7" met="7"><w n="6.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="6.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="6.6" punct="pv:7">vi<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pv">eu</seg>x</rhyme></w> ;</l>
					<l n="7" num="1.7" lm="7" met="7"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.3" punct="pe:4">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pe">eu</seg>rs</w> ! <w n="7.4">j</w>’<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.6">mi<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</rhyme></w></l>
					<l n="8" num="1.8" lm="5" met="5"><space quantity="6" unit="char"></space><w n="8.1">L</w>’<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.5" punct="pt:5">m<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="5">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="huitain" rhyme="ababcccb">
					<l n="9" num="2.1" lm="7" met="7"><w n="9.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="9.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="9.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="9.5" punct="pe:7">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>ph<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="pe">on</seg>s</rhyme></w> !</l>
					<l n="10" num="2.2" lm="5" met="5"><space quantity="6" unit="char"></space><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="10.2">g<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="10.3" punct="vg:5">f<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<rhyme label="b" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></w>,</l>
					<l n="11" num="2.3" lm="7" met="7"><w n="11.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="11.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="11.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="11.4">j<seg phoneme="wa" type="vs" value="1" rule="440" place="4">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="11.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ff<rhyme label="a" id="4" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s</rhyme></w></l>
					<l n="12" num="2.4" lm="5" met="5"><space quantity="6" unit="char"></space><w n="12.1">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3" punct="pi:5">S<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>c<rhyme label="b" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pi">e</seg></rhyme></w> ?</l>
					<l n="13" num="2.5" lm="7" met="7"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="13.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ds</w> <w n="13.3">m<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>ts</w> <w n="13.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="353" place="5">e</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="339" place="6">a</seg>y<rhyme label="c" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></rhyme></w></l>
					<l n="14" num="2.6" lm="7" met="7"><w n="14.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">p<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="14.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="14.5">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="6">a</seg>y<rhyme label="c" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></rhyme></w></l>
					<l n="15" num="2.7" lm="7" met="7"><w n="15.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="15.2">M<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>li<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>fr<seg phoneme="ɛ" type="vs" value="1" rule="339" place="6">a</seg>y<rhyme label="c" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></rhyme></w></l>
					<l n="16" num="2.8" lm="5" met="5"><space quantity="6" unit="char"></space><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="16.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="16.3" punct="pe:5">d<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>c<rhyme label="b" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="3" type="huitain" rhyme="ababcccb">
					<l n="17" num="3.1" lm="7" met="7"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="17.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="17.3">M<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r</rhyme></w></l>
					<l n="18" num="3.2" lm="5" met="5"><space quantity="6" unit="char"></space><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="18.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="18.3">pi<seg phoneme="e" type="vs" value="1" rule="241" place="3">e</seg>d</w> <w n="18.4">d</w>’<w n="18.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>lb<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w></l>
					<l n="19" num="3.3" lm="7" met="7"><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">l<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="19.3">br<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="19.4">d</w>’<w n="19.5"><rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r</rhyme></w></l>
					<l n="20" num="3.4" lm="5" met="5"><space quantity="6" unit="char"></space><w n="20.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="20.2">si<seg phoneme="e" type="vs" value="1" rule="241" place="2">e</seg>d</w> <w n="20.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="20.4" punct="pt:5">th<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><rhyme label="b" id="8" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></w>.</l>
					<l n="21" num="3.5" lm="7" met="7"><w n="21.1">L</w>’<w n="21.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="21.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="21.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>s<rhyme label="c" id="9" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="vg">on</seg></rhyme></w>,</l>
					<l n="22" num="3.6" lm="7" met="7"><w n="22.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="22.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="22.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="22.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="22.5" punct="dp:7">m<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>ss<rhyme label="c" id="9" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="dp">on</seg></rhyme></w> :</l>
					<l n="23" num="3.7" lm="7" met="7"><w n="23.1">Qu</w>’<w n="23.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="23.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="23.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="23.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>s<rhyme label="c" id="9" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></rhyme></w></l>
					<l n="24" num="3.8" lm="5" met="5"><space quantity="6" unit="char"></space><w n="24.1">R<seg phoneme="i" type="vs" value="1" rule="d-1" place="1">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="2">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="24.3" punct="pe:5">f<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="4" type="huitain" rhyme="ababcccb">
					<l n="25" num="4.1" lm="7" met="7"><w n="25.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="25.3">C<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>d<seg phoneme="i" type="vs" value="1" rule="482" place="5">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="25.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="25.5">m<rhyme label="a" id="10" gender="m" type="a"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg>s</rhyme></w></l>
					<l n="26" num="4.2" lm="5" met="5"><space quantity="6" unit="char"></space><w n="26.1"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">Ai</seg>t</w> <w n="26.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="26.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="26.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="26.5" punct="pe:5">c<rhyme label="b" id="11" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="493" place="5">y</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></w> !</l>
					<l n="27" num="4.3" lm="7" met="7"><w n="27.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="27.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="27.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="27.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="27.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="27.6">s<rhyme label="a" id="10" gender="m" type="e"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg>s</rhyme></w></l>
					<l n="28" num="4.4" lm="5" met="5"><space quantity="6" unit="char"></space><w n="28.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="28.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="28.3" punct="vg:5">d<rhyme label="b" id="11" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></w>,</l>
					<l n="29" num="4.5" lm="7" met="7"><w n="29.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="29.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="29.3">r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="29.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="29.6" punct="pe:7">pr<rhyme label="c" id="12" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="pe">om</seg>pt</rhyme></w> !</l>
					<l n="30" num="4.6" lm="7" met="7"><w n="30.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="30.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="30.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="30.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.5" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ffr<rhyme label="c" id="12" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="vg">on</seg>t</rhyme></w>,</l>
					<l n="31" num="4.7" lm="7" met="7"><w n="31.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1">en</seg></w> <w n="31.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="31.3">si<seg phoneme="e" type="vs" value="1" rule="241" place="3">e</seg>d</w> <w n="31.4">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="31.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="31.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="31.7">fr<rhyme label="c" id="12" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</rhyme></w></l>
					<l n="32" num="4.8" lm="5" met="5"><space quantity="6" unit="char"></space><w n="32.1">Qu</w>’<w n="32.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="32.3">r<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="32.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="32.5" punct="pt:5">v<rhyme label="b" id="11" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1855">Mai 1855.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>