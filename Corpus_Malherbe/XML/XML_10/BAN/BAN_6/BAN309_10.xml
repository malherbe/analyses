<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODELETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1529 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ODELETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvilleodelettes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1856">1856</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN309" modus="sp" lm_max="6" metProfile="6, 2" form="suite périodique" schema="7(abab)">
				<head type="main">A Théophile Gautier</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="6" met="6"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="1.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="1.5" punct="vg:6">f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="6" met="6"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="3">ë</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.3"><seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</rhyme></w></l>
					<l n="3" num="1.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>n<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="6" met="6"><w n="4.1">L</w>’<w n="4.2"><seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="4.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="4.4" punct="pt:6">c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pt">eu</seg>r</rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="6" met="6"><w n="5.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="5.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="5.4">qu</w>’<w n="5.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="5.6" punct="vg:6">m<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rtr<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="6" num="2.2" lm="6" met="6"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="6.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="6.3">gr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="6.5">p<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</rhyme></w></l>
					<l n="7" num="2.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="7.1" punct="vg:2">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>pr<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg></rhyme></w>,</l>
					<l n="8" num="2.4" lm="6" met="6"><w n="8.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="8.2">m<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l</w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="8.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="8.5" punct="pt:6">d<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pt">u</seg>r</rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="6" met="6"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">tr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>il</w> <w n="9.4" punct="pe:6">c<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>mm<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></w> !</l>
					<l n="10" num="3.2" lm="6" met="6"><w n="10.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="10.2" punct="vg:3">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" punct="vg">en</seg>ds</w>, <w n="10.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.4" punct="vg:6">m<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg">oi</seg></rhyme></w>,</l>
					<l n="11" num="3.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">l</w>’<w n="11.3"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">O</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></rhyme></w></l>
					<l n="12" num="3.4" lm="6" met="6"><w n="12.1">G<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.4" punct="vg:6">l<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg">oi</seg></rhyme></w>,</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="6" met="6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2" punct="vg:2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" punct="vg">e</seg></w>, <w n="13.3">br<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="13.5" punct="vg:6">f<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="14" num="4.2" lm="6" met="6"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="14.3">rh<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.4">d</w>’<w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></rhyme></w></l>
					<l n="15" num="4.3" lm="2" met="2"><space quantity="12" unit="char"></space><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>f<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></rhyme></w></l>
					<l n="16" num="4.4" lm="6" met="6"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="16.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="16.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="16.5" punct="pt:6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" punct="pt">ein</seg></rhyme></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="6" met="6"><w n="17.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="17.2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="17.3" punct="vg:3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg></w>, <w n="17.4">f<seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="17.5">d</w>’<w n="17.6" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>xt<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="18" num="5.2" lm="6" met="6"><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="18.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="18.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="18.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ds</w> <w n="18.5">ci<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</rhyme></w></l>
					<l n="19" num="5.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="19.1" punct="vg:2">P<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>g<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg></rhyme></w>,</l>
					<l n="20" num="5.4" lm="6" met="6"><w n="20.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l</w> <w n="20.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="20.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="20.5" punct="pv:6">y<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pv">eu</seg>x</rhyme></w> ;</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="6" met="6"><w n="21.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="21.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="21.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="21.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="21.5">gr<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="22" num="6.2" lm="6" met="6"><w n="22.1">S<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="22.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="22.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="22.5">r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></rhyme></w></l>
					<l n="23" num="6.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="23.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2" punct="vg:2">R<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg></rhyme></w>,</l>
					<l n="24" num="6.4" lm="6" met="6"><w n="24.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="24.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="24.4" punct="pv:6"><seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="pv">eau</seg></rhyme></w> ;</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="6" met="6"><w n="25.1" punct="vg:2">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="25.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="25.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="25.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>s<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="384" place="6">ei</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></w></l>
					<l n="26" num="7.2" lm="6" met="6"><w n="26.1">L</w>’<w n="26.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="26.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="26.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rt</w> <w n="26.5" punct="vg:6">l<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>ri<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></rhyme></w>,</l>
					<l n="27" num="7.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="27.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="27.2">d<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg>s</rhyme></w></l>
					<l n="28" num="7.4" lm="6" met="6"><w n="28.1"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">Ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="28.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="28.4" punct="pt:6"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><rhyme label="b" id="14" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pt">er</seg></rhyme></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1856">Mai 1856.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>