<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODELETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1529 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ODELETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvilleodelettes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1856">1856</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN298" modus="sm" lm_max="7" metProfile="7" form="suite périodique" schema="8(aabccb)">
				<head type="main">A Méry</head>
				<lg n="1" type="sizain" rhyme="aabccb">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="1.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="1.5" punct="vg:7"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>t<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" punct="vg">an</seg>s</rhyme></w>,</l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1" punct="vg:2">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>qu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg></w>, <w n="2.2">l</w>’<w n="2.3" punct="vg:5"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" punct="vg">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="2.5">t<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>ps</rhyme></w></l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3">r<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="3.4" punct="vg:7">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>ss<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="7" met="7"><w n="4.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rb<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="4.2">d</w>’<w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="4.4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="6">e</seg>d</w> <w n="4.5" punct="vg:7">s<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="445" place="7" punct="vg">û</seg>r</rhyme></w>,</l>
					<l n="5" num="1.5" lm="7" met="7"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="5.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.3">pi<seg phoneme="e" type="vs" value="1" rule="241" place="4">e</seg>ds</w> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="5.5">l</w>’<w n="5.6" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>r</w>, <w n="5.7">s<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</rhyme></w></l>
					<l n="6" num="1.6" lm="7" met="7"><w n="6.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3" punct="pt:7">fr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ss<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="sizain" rhyme="aabccb">
					<l n="7" num="2.1" lm="7" met="7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="7.4">cr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="7.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.6">d</w>’<w n="7.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="7.8">b<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d</rhyme></w></l>
					<l n="8" num="2.2" lm="7" met="7"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="8.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="8.3">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>l</w> <w n="8.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<rhyme label="a" id="4" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d</rhyme></w></l>
					<l n="9" num="2.3" lm="7" met="7"><w n="9.1" punct="vg:3">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>cr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">â</seg>t</w>, <w n="9.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="9.3" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t<rhyme label="b" id="5" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="10" num="2.4" lm="7" met="7"><w n="10.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="10.3">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="10.4" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>l<rhyme label="c" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg>s</rhyme></w>,</l>
					<l n="11" num="2.5" lm="7" met="7"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>l<rhyme label="c" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s</rhyme></w></l>
					<l n="12" num="2.6" lm="7" met="7"><w n="12.1">Fou<seg phoneme="e" type="vs" value="1" rule="353" place="1">e</seg>tt<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="12.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="12.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="12.4" punct="pt:7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>l<rhyme label="b" id="5" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" type="sizain" rhyme="aabccb">
					<l n="13" num="3.1" lm="7" met="7"><w n="13.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="13.2">h<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="13.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.5" punct="vg:7">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rf<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="vg">oi</seg>s</rhyme></w>,</l>
					<l n="14" num="3.2" lm="7" met="7"><w n="14.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="14.4" punct="vg:7">v<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="vg">oi</seg>x</rhyme></w>,</l>
					<l n="15" num="3.3" lm="7" met="7"><w n="15.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="15.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="15.3">cr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="15.6">f<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="16" num="3.4" lm="7" met="7"><w n="16.1">Qu</w>’<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.3">v<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="16.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="16.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="16.6">cl<rhyme label="c" id="9" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r</rhyme></w></l>
					<l n="17" num="3.5" lm="7" met="7"><w n="17.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="17.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.4">m<rhyme label="c" id="9" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="7">e</seg>r</rhyme></w></l>
					<l n="18" num="3.6" lm="7" met="7"><w n="18.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="18.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.3">l</w>’<w n="18.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-27" place="6">e</seg></w> <w n="18.5" punct="pt:7">h<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="4" type="sizain" rhyme="aabccb">
					<l n="19" num="4.1" lm="7" met="7"><w n="19.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="19.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="19.3">qu</w>’<w n="19.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="19.5">f<seg phoneme="œ" type="vs" value="1" rule="304" place="4">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="19.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="19.7">p<rhyme label="a" id="10" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</rhyme></w></l>
					<l n="20" num="4.2" lm="7" met="7"><w n="20.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>t</w> <w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="20.4">b<rhyme label="a" id="10" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</rhyme></w></l>
					<l n="21" num="4.3" lm="7" met="7"><w n="21.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="21.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>rs</w> <w n="21.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="21.4">c<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="21.5" punct="vg:7">c<rhyme label="b" id="11" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="22" num="4.4" lm="7" met="7"><w n="22.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="22.2">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="22.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="22.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="22.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="22.6" punct="vg:7">v<rhyme label="c" id="12" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7" punct="vg">o</seg>l</rhyme></w>,</l>
					<l n="23" num="4.5" lm="7" met="7"><w n="23.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="23.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="23.4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="4">e</seg>ds</w> <w n="23.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="23.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="23.7" punct="vg:7">s<rhyme label="c" id="12" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7" punct="vg">o</seg>l</rhyme></w>,</l>
					<l n="24" num="4.6" lm="7" met="7"><w n="24.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="24.2">v<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="24.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="24.4" punct="pt:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<rhyme label="b" id="11" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="7">î</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="5" type="sizain" rhyme="aabccb">
					<l n="25" num="5.1" lm="7" met="7"><w n="25.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="25.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="25.3">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="25.4" punct="vg:7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rm<rhyme label="a" id="13" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="7" punct="vg">e</seg>ils</rhyme></w>,</l>
					<l n="26" num="5.2" lm="7" met="7"><w n="26.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="26.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="26.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="26.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="26.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<rhyme label="a" id="13" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>ils</rhyme></w></l>
					<l n="27" num="5.3" lm="7" met="7"><w n="27.1">Qu</w>’<w n="27.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="27.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="27.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="27.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="27.6">t<rhyme label="b" id="14" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="28" num="5.4" lm="7" met="7"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="28.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="28.3">l</w>’<w n="28.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>r</w> <w n="28.5" punct="vg:7">br<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<rhyme label="c" id="15" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></rhyme></w>,</l>
					<l n="29" num="5.5" lm="7" met="7"><w n="29.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="29.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301" place="4">ai</seg>t</w> <w n="29.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="29.4" punct="pt:7">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<rhyme label="c" id="15" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pt">é</seg></rhyme></w>.</l>
					<l n="30" num="5.6" lm="7" met="7"><w n="30.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="30.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="30.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt</w> <w n="30.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="30.6" punct="pt:7">p<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg><rhyme label="b" id="14" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="6" type="sizain" rhyme="aabccb">
					<l n="31" num="6.1" lm="7" met="7"><w n="31.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="31.2">tr<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="31.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="31.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="31.5" punct="pt:7">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>p<rhyme label="a" id="16" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="pt">eu</seg>r</rhyme></w>.</l>
					<l n="32" num="6.2" lm="7" met="7"><w n="32.1">B<seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg></w> <w n="32.2" punct="vg:3">m<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">er</seg></w>, <w n="32.3">s</w>’<w n="32.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="32.5">n</w>’<w n="32.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="32.7">p<rhyme label="a" id="16" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</rhyme></w></l>
					<l n="33" num="6.3" lm="7" met="7"><w n="33.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="33.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="33.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="33.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="33.5">d<rhyme label="b" id="17" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="34" num="6.4" lm="7" met="7"><w n="34.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="34.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="34.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>ds</w> <w n="34.4" punct="vg:7">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<rhyme label="c" id="18" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="7" punct="vg">ein</seg>s</rhyme></w>,</l>
					<l n="35" num="6.5" lm="7" met="7"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="35.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="35.3">s</w>’<w n="35.4"><seg phoneme="i" type="vs" value="1" rule="497" place="3">y</seg></w> <w n="35.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="35.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="35.7">r<rhyme label="c" id="18" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="7">ein</seg>s</rhyme></w></l>
					<l n="36" num="6.6" lm="7" met="7"><w n="36.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="36.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="36.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ls</w> <w n="36.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="36.5" punct="pt:7">D<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>d<rhyme label="b" id="17" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="7" type="sizain" rhyme="aabccb">
					<l n="37" num="7.1" lm="7" met="7"><w n="37.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="37.2">l</w>’<w n="37.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="37.4"><seg phoneme="a" type="vs" value="1" rule="343" place="4">a</seg><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><rhyme label="a" id="19" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7">en</seg></rhyme></w></l>
					<l n="38" num="7.2" lm="7" met="7"><w n="38.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="38.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="38.3" punct="vg:5">s<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="38.4"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="38.5">bi<rhyme label="a" id="19" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg></rhyme></w></l>
					<l n="39" num="7.3" lm="7" met="7"><w n="39.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="39.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="39.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="39.4">n<seg phoneme="y" type="vs" value="1" rule="457" place="5">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="39.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="39.6">fr<rhyme label="b" id="20" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="40" num="7.4" lm="7" met="7"><w n="40.1">Qu</w>’<w n="40.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="40.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rç<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="40.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="40.5" punct="vg:7">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">am</seg>b<rhyme label="c" id="21" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="7" punct="vg">eau</seg></rhyme></w>,</l>
					<l n="41" num="7.5" lm="7" met="7"><w n="41.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="41.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="41.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="41.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="41.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>b<rhyme label="c" id="21" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></rhyme></w></l>
					<l n="42" num="7.6" lm="7" met="7"><w n="42.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="42.2">h<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="42.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="42.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="42.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="42.6" punct="vg:7">r<rhyme label="b" id="20" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
				</lg>
				<lg n="8" type="sizain" rhyme="aabccb">
					<l n="43" num="8.1" lm="7" met="7"><w n="43.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="43.2">s<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>lph<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="43.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="43.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="43.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ge<rhyme label="a" id="22" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>t</rhyme></w></l>
					<l n="44" num="8.2" lm="7" met="7"><w n="44.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rt</w> <w n="44.2">d</w>’<w n="44.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>c<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="44.4">d</w>’<w n="44.5" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rg<rhyme label="a" id="22" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7" punct="vg">en</seg>t</rhyme></w>,</l>
					<l n="45" num="8.3" lm="7" met="7"><w n="45.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="45.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="45.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="45.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>rs</w> <w n="45.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="45.6">pl<rhyme label="b" id="23" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="46" num="8.4" lm="7" met="7"><w n="46.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="46.2">h<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="46.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="46.4">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="46.5" punct="vg:7"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<rhyme label="c" id="24" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg>s</rhyme></w>,</l>
					<l n="47" num="8.5" lm="7" met="7"><w n="47.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="47.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>v<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="47.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="47.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<rhyme label="c" id="24" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg>s</rhyme></w></l>
					<l n="48" num="8.6" lm="7" met="7"><w n="48.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="48.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="48.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="48.4" punct="pt:7">p<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>l<rhyme label="b" id="23" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1855">Mai 1855.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>