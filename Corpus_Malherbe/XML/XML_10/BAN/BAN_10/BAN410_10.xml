<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN410" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="10(abab)">
				<head type="main">La Marseillaise</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
							Les Prussiens, pour s’approprier notre <lb></lb>
							hymne national, ont fait composer des vers <lb></lb>
							allemands qu’ils chantent sur l’air de La <lb></lb>
							Marseillaise.
							</quote>
							<bibl>
								<name>Les Journaux</name>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:2">J<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="1.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="1.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="1.4">r<seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>ts</rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="2.2">l</w>’<w n="2.3">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="2.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="5">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="2.6">d</w>’<w n="2.7" punct="vg:8"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2">pl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="3.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="3.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>rs</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ds</rhyme></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.6" punct="pe:8">M<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rs<seg phoneme="e" type="vs" value="1" rule="383" place="7">e</seg>ill<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="5.2">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>f<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="5.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="5.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="5.5" punct="vg:8">pl<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="vg">e</seg>ts</rhyme></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">L<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="6.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.4" punct="vg:8">g<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rm<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="7.3" punct="dp:5"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="dp">ez</seg></w> : <w n="7.4">D<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rm<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</rhyme></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="8.4" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">B<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>sm<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rck</w> <w n="9.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="9.4" punct="dp:5">d<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="dp">i</seg>t</w> : <w n="9.5">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.7" punct="vg:8">cr<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>s</rhyme></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="10.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="10.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.7" punct="vg:8">cr<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">J</w>’<w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="11.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="11.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="11.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.7" punct="vg:8">cr<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>x</rhyme></w>,</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="12.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="12.3">l</w>’<w n="12.4">h<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.7" punct="pt:8">Pr<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="13.2" punct="pe:3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="pe">on</seg>c</w> ! <w n="13.3">l</w>’<w n="13.4">H<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="13.6">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>l</w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.8" punct="vg:8">f<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg></rhyme></w>,</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">L</w>’<w n="14.2">h<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.4">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="14.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ffr<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">V<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="15.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="15.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="15.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="15.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>l</w> <w n="15.7">bl<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></rhyme></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">N</w>’<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="16.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="16.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="16.5">cr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="16.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="16.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.8" punct="dp:7">s<seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="dp">oi</seg>t</w> : <w n="16.9" punct="pe:8">Fr<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">L</w>’<w n="17.2"><seg phoneme="œ" type="vs" value="1" rule="286" place="1">œ</seg>il</w> <w n="17.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>mm<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="17.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.5">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>r</w> <w n="17.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="17.7" punct="vg:8">m<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="vg">ain</seg></rhyme></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="18.2" punct="vg:5">g<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="4">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="18.3">l</w>’<w n="18.4"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w> <w n="19.2" punct="dp:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3" punct="dp">o</seg>r</w> : <w n="19.3" punct="pe:4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="pe">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="19.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="19.5">g<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="19.6">h<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8">ain</seg></rhyme></w></l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="20.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="20.6" punct="pe:8"><rhyme label="b" id="10" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1">Cr<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="21.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="21.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="21.5">n<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="11" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></rhyme></w></l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="22.2">gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="22.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="22.5" punct="vg:8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="6">ym</seg>b<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="b" id="12" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="23.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="23.4" punct="vg:8">R<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="11" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></w>,</l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="24.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="24.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="24.5" punct="pv:8">R<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>bl<rhyme label="b" id="12" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1" punct="vg:1"><seg phoneme="a" type="vs" value="1" rule="341" place="1" punct="vg">Â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="25.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="25.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="25.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="25.6">p<rhyme label="a" id="13" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></w></l>
					<l n="26" num="7.2" lm="8" met="8"><w n="26.1">H<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="26.3">M<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rc<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="26.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="26.5" punct="dp:8">G<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>vr<rhyme label="b" id="14" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1" punct="vg:2">T<seg phoneme="ø" type="vs" value="1" rule="405" place="1">eu</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>s</w>, <w n="27.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="27.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="27.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="27.5">p<rhyme label="a" id="13" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></w></l>
					<l n="28" num="7.4" lm="8" met="8"><w n="28.1" punct="vg:2">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg></w>, <w n="28.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="28.4">m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="28.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="28.6" punct="pt:8">p<rhyme label="b" id="14" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="8" type="quatrain" rhyme="abab">
					<l n="29" num="8.1" lm="8" met="8"><w n="29.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="29.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="29.3">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="29.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>squ</w>’<w n="29.5"><rhyme label="a" id="15" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="426" place="8">où</seg></rhyme></w></l>
					<l n="30" num="8.2" lm="8" met="8"><w n="30.1">Gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="30.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="30.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="30.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="30.6" punct="pe:8">h<rhyme label="b" id="16" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					<l n="31" num="8.3" lm="8" met="8"><w n="31.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="31.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="31.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="31.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="31.5">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="31.7">c<rhyme label="a" id="15" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg></rhyme></w></l>
					<l n="32" num="8.4" lm="8" met="8"><w n="32.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="32.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="32.3">c<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>lli<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="32.4">d</w>’<w n="32.5" punct="pv:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>scl<rhyme label="b" id="16" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
				</lg>
				<lg n="9" type="quatrain" rhyme="abab">
					<l n="33" num="9.1" lm="8" met="8"><w n="33.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="33.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="33.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="33.4">p<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="33.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="33.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>lc<rhyme label="a" id="17" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>s</rhyme></w></l>
					<l n="34" num="9.2" lm="8" met="8"><w n="34.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="34.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="34.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="34.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="34.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="34.6">p<rhyme label="b" id="18" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="35" num="9.3" lm="8" met="8"><w n="35.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="35.2">l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="35.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="35.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="35.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rc<rhyme label="a" id="17" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>s</rhyme></w></l>
					<l n="36" num="9.4" lm="8" met="8"><w n="36.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="36.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="36.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ts</w> <w n="36.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="36.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="36.6" punct="pt:8">f<rhyme label="b" id="18" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="10" type="quatrain" rhyme="abab">
					<l n="37" num="10.1" lm="8" met="8"><w n="37.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="37.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="37.3" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><rhyme label="a" id="19" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>s</rhyme></w>,</l>
					<l n="38" num="10.2" lm="8" met="8"><w n="38.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="38.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="38.4" punct="vg:4">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="38.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="38.7">l</w>’<w n="38.8" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">I</seg>d<rhyme label="b" id="20" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="39" num="10.3" lm="8" met="8"><w n="39.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="39.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="39.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="39.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="39.5">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><rhyme label="a" id="19" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>ts</rhyme></w></l>
					<l n="40" num="10.4" lm="8" met="8"><w n="40.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1">en</seg></w> <w n="40.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="40.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="40.4">l</w>’<w n="40.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="40.6" punct="pe:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd<rhyme label="b" id="20" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Octobre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>