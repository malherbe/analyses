<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN750" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="5(abab)">
				<head type="main">A Catulle Mendès</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="1.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="1.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="1.4">qu<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rti<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="1.5">L<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="2.2">j</w>’<w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <hi rend="ital"><w n="2.4">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="2.5">St<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ct<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></hi></l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="3.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="3.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="3.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt</w> <w n="3.6" punct="vg:8">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg>t<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="vg">ain</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="4.5" punct="pt:8">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">p<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="5.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.5" punct="vg:8">l<seg phoneme="u" type="vs" value="1" rule="d-2" place="7">ou</seg><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>s</rhyme></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="6.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="6.4">j<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="6.5" punct="vg:8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2" punct="vg:3">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>s</w>, <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="7.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="7.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="7">ou</seg><rhyme label="a" id="3" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>s</rhyme></w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">L<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rg</w> <w n="8.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6">ein</seg></w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.6" punct="pt:8">r<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">J</w>’<w n="9.2"><seg phoneme="i" type="vs" value="1" rule="497" place="1">y</seg></w> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="9.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</rhyme></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">D</w>’<w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>th<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>sm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.5" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3" punct="vg:4">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" punct="vg">e</seg>il</w>, <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="11.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="11.6">c<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>si<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</rhyme></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.4" punct="pt:8">B<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="13.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ti<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="13.4" punct="vg:5">r<seg phoneme="i" type="vs" value="1" rule="467" place="5" punct="vg">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">A</seg>rc<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d<rhyme label="a" id="10" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="BAN750_1" place="8">e</seg>s</rhyme></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">Am</seg>b<seg phoneme="o" type="vs" value="1" rule="444" place="2" punct="vg">o</seg></w>, <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="14.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>x</w> <w n="14.5" punct="vg:8">f<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="b" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="15.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="15.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="15.4">C<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="15.5" punct="vg:8">M<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d<rhyme label="a" id="10" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8" punct="vg">è</seg>s</rhyme></w>,</l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>ti<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="16.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="16.5" punct="pt:8">r<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="b" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="17.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="17.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="17.6" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ffr<rhyme label="a" id="8" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe">on</seg>t</rhyme></w> !</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="18.2">s</w>’<w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="18.4">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="18.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.6">z<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="493" place="6">y</seg>r</w> <w n="18.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="18.8">l</w>’<w n="18.9" punct="pi:8"><rhyme label="b" id="9" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="19.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="19.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="19.7">fr<rhyme label="a" id="8" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</rhyme></w></l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">V<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="20.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="20.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="20.4" punct="pe:8">bl<rhyme label="b" id="9" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>