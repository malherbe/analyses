<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN748" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede">
				<head type="main">Les Roses</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" mp="M">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="1.3" punct="vg:6">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="4" mp="M">a</seg>y<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="1.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="1.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="1.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="1.8">j<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>r</rhyme></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="2.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="2.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="2.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="2.5" punct="vg:6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg>t</w>,<caesura></caesura> <w n="2.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>il</w> <w n="2.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.8">l</w>’<w n="2.9" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>r<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:1">N<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">aî</seg>t</w>, <w n="3.2" punct="vg:4">tr<seg phoneme="e" type="vs" value="1" rule="353" place="2" mp="M">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="3.3" punct="vg:6">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="5" mp="M">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="3.4" punct="vg:7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="vg">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="3.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="3.7">l</w>’<w n="3.8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r</w> <w n="3.9">s<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>n<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="2">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="4.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg>s</w><caesura></caesura> <w n="4.4">m<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="4.6">l</w>’<w n="4.7" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>r</rhyme></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="5.2">S<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>lph<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="5.3" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" mp="M">in</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>s</w>,<caesura></caesura> <w n="5.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="5.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="5.7" punct="pe:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>t<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>r</rhyme></w> !</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="6.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="6.3">j<seg phoneme="wa" type="vs" value="1" rule="440" place="4" mp="M">o</seg>y<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="6.4">d</w>’<w n="6.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="6.7">f<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="9">ê</seg>t</w> <w n="6.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="6.9" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>c<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2" punct="vg:3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="7.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="7.5">c<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="7.6" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>cl<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">T<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>t<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.2">f<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">â</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="8.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg></w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="8.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="8.7" punct="vg:12">c<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>r</rhyme></w>,</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="9.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="9.3">l</w>’<w n="9.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>r</w> <w n="9.5">p<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>r</w><caesura></caesura> <w n="9.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="9.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="9.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="9.9">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="9.10" punct="vg:12">pr<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="10.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="10.4" punct="vg:6">l<seg phoneme="y" type="vs" value="1" rule="453" place="5" mp="M">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" punct="vg" caesura="1">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="10.7" punct="vg:12">j<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">Fr<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="11.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="11.4">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w><caesura></caesura> <w n="11.5"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="11.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="11.7" punct="pt:12">ci<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</rhyme></w>.</l>
				</lg>
				<lg n="4" rhyme="ede">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">B<seg phoneme="o" type="vs" value="1" rule="315" place="1" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="12.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="12.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="12.4">r<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="12.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="12.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="12.8" punct="vg:12">p<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1" punct="vg:1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">i</seg>s</w>, <w n="13.2">n</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w>-<w n="13.4">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="3" mp="F">e</seg></w> <w n="13.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="13.6">qu</w>’<w n="13.7"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="13.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6" caesura="1">e</seg>st</w><caesura></caesura> <w n="13.9">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>x</w> <w n="13.10"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="13.11">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="d" id="6" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</rhyme></w></l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="14.2">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="14.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="M">o</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="14.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="14.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="14.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="14.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="14.8" punct="pi:12">r<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg>s</rhyme></w> ?</l>
				</lg>
				<closer>
					<dateline>
						<date when="1888">Novembre 1888</date>
					</dateline>
				</closer>
			</div></body></text></TEI>