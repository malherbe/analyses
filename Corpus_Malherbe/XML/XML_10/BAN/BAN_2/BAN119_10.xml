<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN119" modus="sp" lm_max="5" metProfile="5, 2" form="suite périodique" schema="4(aabccb)">
				<head type="main">ÉCRIT SUR UN EXEMPLAIRE <lb></lb>DES <hi rend="ital">ODELETTES</hi></head>
				<lg n="1" type="sizain" rhyme="aabccb">
					<l n="1" num="1.1" lm="5" met="5"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="1.2">j</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="1.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="1.5" punct="vg:5">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>c<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="5" met="5"><w n="2.1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="2.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">n<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>l</w> <w n="2.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>c<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></rhyme></w></l>
					<l n="3" num="1.3" lm="2" met="2"><space quantity="4" unit="char"></space><w n="3.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2" punct="vg:2">r<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="5" met="5"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="4.2">fi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">l</w>’<w n="4.5"><rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</rhyme></w></l>
					<l n="5" num="1.5" lm="5" met="5"><w n="5.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="5.2">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="5.3" punct="dp:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="dp">o</seg>r</rhyme></w> :</l>
					<l n="6" num="1.6" lm="2" met="2"><space quantity="4" unit="char"></space><w n="6.1">J</w>’<w n="6.2"><seg phoneme="i" type="vs" value="1" rule="497" place="1">y</seg></w> <w n="6.3" punct="pe:2">s<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="2" type="sizain" rhyme="aabccb">
					<l n="7" num="2.1" lm="5" met="5"><w n="7.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="7.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="7.3" punct="vg:5">m<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>m<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" punct="vg">en</seg>ts</rhyme></w>,</l>
					<l n="8" num="2.2" lm="5" met="5"><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="8.3">r<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>m<rhyme label="a" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</rhyme></w></l>
					<l n="9" num="2.3" lm="2" met="2"><space quantity="4" unit="char"></space><w n="9.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">f<rhyme label="b" id="5" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></rhyme></w></l>
					<l n="10" num="2.4" lm="5" met="5"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">j<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="10.3" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>b<rhyme label="c" id="6" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>t</rhyme></w>,</l>
					<l n="11" num="2.5" lm="5" met="5"><w n="11.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="11.3">pr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="11.4">t<rhyme label="c" id="6" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</rhyme></w></l>
					<l n="12" num="2.6" lm="2" met="2"><space quantity="4" unit="char"></space><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2" punct="pe:2">m<rhyme label="b" id="5" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="3" type="sizain" rhyme="aabccb">
					<l n="13" num="3.1" lm="5" met="5"><w n="13.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="13.2">r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w>-<w n="13.3">v<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</rhyme></w></l>
					<l n="14" num="3.2" lm="5" met="5"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rts</w> <w n="14.3" punct="vg:5">j<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>x</rhyme></w>,</l>
					<l n="15" num="3.3" lm="2" met="2"><space quantity="4" unit="char"></space><w n="15.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="15.2" punct="vg:2">br<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="16" num="3.4" lm="5" met="5"><w n="16.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="16.2" punct="vg:2">p<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>rs</w>, <w n="16.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="16.4" punct="pi:5">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>cc<rhyme label="c" id="9" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="5" punct="pi">è</seg>s</rhyme></w> ?</l>
					<l n="17" num="3.5" lm="5" met="5"><w n="17.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t</w> <w n="17.3"><seg phoneme="y" type="vs" value="1" rule="391" place="4">eu</seg>t</w> <w n="17.4">s<rhyme label="c" id="9" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</rhyme></w></l>
					<l n="18" num="3.6" lm="2" met="2"><space quantity="4" unit="char"></space><w n="18.1" punct="pe:2">R<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>dr<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pe">e</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="4" type="sizain" rhyme="aabccb">
					<l n="19" num="4.1" lm="5" met="5"><w n="19.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="19.2">Qu</w>’<w n="19.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="19.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t</w> <w n="19.5" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<rhyme label="a" id="10" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" punct="vg">en</seg>t</rhyme></w>,</l>
					<l n="20" num="4.2" lm="5" met="5"><w n="20.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="20.2">M<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="20.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d<rhyme label="a" id="10" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</rhyme></w></l>
					<l n="21" num="4.3" lm="2" met="2"><space quantity="4" unit="char"></space><w n="21.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="21.2">l<rhyme label="b" id="11" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></rhyme></w></l>
					<l n="22" num="4.4" lm="5" met="5"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="22.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="22.3">bru<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="22.4">d</w>’<w n="22.5" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>f<rhyme label="c" id="12" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="5" punct="vg">e</seg>r</rhyme></w>,</l>
					<l n="23" num="4.5" lm="5" met="5"><w n="23.1">J</w>’<w n="23.2"><seg phoneme="e" type="vs" value="1" rule="353" place="1">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="339" place="2">a</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="23.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="23.4"><rhyme label="c" id="12" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r</rhyme></w></l>
					<l n="24" num="4.6" lm="2" met="2"><space quantity="4" unit="char"></space><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2" punct="pe:2">fl<rhyme label="b" id="11" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="445" place="2">û</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1858">juin 1858</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>