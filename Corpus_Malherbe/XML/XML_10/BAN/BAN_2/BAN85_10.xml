<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">OCCIDENTALES</head><div type="poem" key="BAN85" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="5(abbacdcd)">
					<head type="main">OCCIDENTALE PREMIÈRE</head>
					<head type="sub">L’OMBRE D’ÉRIC</head>
					<lg n="1" type="huitain" rhyme="abbacdcd">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="1.2">L<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="323" place="3">ay</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="1.4" punct="vg:8">fl<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="2.2">b<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="2.4">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.6">l</w>’<w n="2.7" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>r<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="3.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="3.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6">ein</seg></w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.7" punct="vg:8">fl<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="4.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="4.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>x</w> <w n="4.6" punct="pt:8">pl<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>r</rhyme></w>.</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="5.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="5.3">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>l</w> <w n="5.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="5.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="5.6" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">d</w>’<w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="6.4">l<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>s</w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="6.7" punct="pv:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pv">eu</seg>r</rhyme></w> ;</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="7.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="7.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="7.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>ts</w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="7.7" punct="vg:8">r<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="8.2">L<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="323" place="3">ay</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="8.4" punct="pt:8">fl<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="2" type="huitain" rhyme="abbacdcd">
						<l n="9" num="2.1" lm="8" met="8"><w n="9.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="9.2">L<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="323" place="3">ay</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="9.4" punct="vg:8">fl<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</rhyme></w>,</l>
						<l n="10" num="2.2" lm="8" met="8"><w n="10.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>cu<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>g</w> <w n="10.2" punct="vg:4">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>t</w>, <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="10.5" punct="vg:8">ch<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>mi<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="11" num="2.3" lm="8" met="8"><w n="11.1">L</w>’<w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="11.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="11.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nni<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="12" num="2.4" lm="8" met="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">s</w>’<w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="12.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.6">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>x</w> <w n="12.7">d</w>’<w n="12.8" punct="pt:8">h<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>nn<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>r</rhyme></w>.</l>
						<l n="13" num="2.5" lm="8" met="8"><w n="13.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="13.3">c<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="13.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="13.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.7" punct="vg:8">l<rhyme label="c" id="7" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="14" num="2.6" lm="8" met="8"><w n="14.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="1">En</seg><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="14.2">d</w>’<w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="14.4">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="d" id="8" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</rhyme></w>,</l>
						<l n="15" num="2.7" lm="8" met="8"><w n="15.1">B<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>z</w> <w n="15.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="15.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="15.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="15.5" punct="vg:8">gu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>rl<rhyme label="c" id="7" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="16" num="2.8" lm="8" met="8"><w n="16.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="16.2">L<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="323" place="3">ay</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c</w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="16.4" punct="pt:8">fl<rhyme label="d" id="8" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="3" type="huitain" rhyme="abbacdcd">
						<l n="17" num="3.1" lm="8" met="8"><w n="17.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="17.2">L<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="323" place="3">ay</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c</w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="17.4" punct="vg:8">fl<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</rhyme></w>,</l>
						<l n="18" num="3.2" lm="8" met="8"><w n="18.1">J</w>’<w n="18.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="18.3" punct="vg:4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rn<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>s</w>, <w n="18.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="18.5">d</w>’<w n="18.6"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-27" place="7">e</seg></w> <w n="18.7" punct="vg:8">h<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="19" num="3.3" lm="8" met="8"><w n="19.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="19.2">h<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.3">d</w>’<w n="19.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="19.5" punct="dp:8">H<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ss<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="BAN85_1" place="8">ay</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
						<l n="20" num="3.4" lm="8" met="8"><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">l</w>’<w n="20.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="20.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="20.5" punct="pt:8">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>r</rhyme></w>.</l>
						<l n="21" num="3.5" lm="8" met="8"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="21.2">c<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="21.3">d</w>’<w n="21.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="21.5" punct="vg:8">c<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>rb<rhyme label="c" id="11" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="22" num="3.6" lm="8" met="8"><w n="22.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="22.3">c<seg phoneme="œ" type="vs" value="1" rule="345" place="3">ue</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="22.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="22.5">l</w>’<w n="22.6">h<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>nn<rhyme label="d" id="12" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</rhyme></w></l>
						<l n="23" num="3.7" lm="8" met="8"><w n="23.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">l</w>’<w n="23.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="23.4">J<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="23.5" punct="vg:8">L<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<rhyme label="c" id="11" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="24" num="3.8" lm="8" met="8"><w n="24.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="24.2">L<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="323" place="3">ay</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c</w> <w n="24.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="24.4" punct="pt:8">fl<rhyme label="d" id="12" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="4" type="huitain" rhyme="abbacdcd">
						<l n="25" num="4.1" lm="8" met="8"><w n="25.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="25.2">L<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="323" place="3">ay</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c</w> <w n="25.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="25.4" punct="vg:8">fl<rhyme label="a" id="13" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</rhyme></w>,</l>
						<l n="26" num="4.2" lm="8" met="8"><w n="26.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="26.3">m<seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="26.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="26.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="26.6" punct="vg:8">v<rhyme label="b" id="14" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="27" num="4.3" lm="8" met="8"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="27.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="27.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>xt<rhyme label="b" id="14" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="28" num="4.4" lm="8" met="8"><w n="28.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">l</w>’<w n="28.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="28.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="28.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="28.6" punct="vg:8">c<rhyme label="a" id="13" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="vg">œu</seg>r</rhyme></w>,</l>
						<l n="29" num="4.5" lm="8" met="8"><w n="29.1">S<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="29.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="29.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="29.4">p<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="29.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>tt<rhyme label="c" id="15" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="30" num="4.6" lm="8" met="8"><w n="30.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w>-<w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="30.4">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="30.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<rhyme label="d" id="16" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</rhyme></w></l>
						<l n="31" num="4.7" lm="8" met="8"><w n="31.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="31.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="31.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="31.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="31.5">l</w>’<w n="31.6">hu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.7" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="c" id="15" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="32" num="4.8" lm="8" met="8"><w n="32.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="32.2">L<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="323" place="3">ay</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c</w> <w n="32.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="32.4" punct="pt:8">fl<rhyme label="d" id="16" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="5" type="huitain" rhyme="abbacdcd">
						<l n="33" num="5.1" lm="8" met="8"><w n="33.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="33.2">L<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="323" place="4">ay</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c</w> <w n="33.3">n</w>’<w n="33.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="33.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="33.6">fl<rhyme label="a" id="17" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</rhyme></w></l>
						<l n="34" num="5.2" lm="8" met="8"><w n="34.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="34.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="34.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w> <w n="34.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="34.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="6">um</seg>s</w> <w n="34.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="34.7">m<rhyme label="b" id="18" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>th<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="35" num="5.3" lm="8" met="8"><w n="35.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="1">En</seg><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="35.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="35.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="35.4">d</w>’<w n="35.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="b" id="18" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="36" num="5.4" lm="8" met="8"><w n="36.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="36.2">l</w>’<w n="36.3">h<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="36.4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="36.5">d</w>’<w n="36.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="36.7" punct="pt:8">r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ct<rhyme label="a" id="17" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>r</rhyme></w>.</l>
						<l n="37" num="5.5" lm="8" met="8"><w n="37.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="37.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="37.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w> <w n="37.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="37.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="37.6">p<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>mm<rhyme label="c" id="19" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="38" num="5.6" lm="8" met="8"><w n="38.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="38.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="38.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="38.4">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>l</w> <w n="38.5" punct="dp:8">s<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>ct<rhyme label="d" id="20" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="dp">eu</seg>r</rhyme></w> :</l>
						<l n="39" num="5.7" lm="8" met="8"><w n="39.1">J<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="39.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="39.3">f<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="39.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="39.5" punct="vg:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<rhyme label="c" id="19" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="40" num="5.8" lm="8" met="8"><w n="40.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="40.2">L<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="323" place="4">ay</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c</w> <w n="40.3">n</w>’<w n="40.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="40.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="40.6" punct="pe:8">fl<rhyme label="d" id="20" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pe">eu</seg>r</rhyme></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1845">novembre 1845</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>