<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SONNAILLES ET CLOCHETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3290 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_16</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SONNAILLES ET CLOCHETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1997">1997</date>
								</imprint>
								<biblScope unit="tome">VII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Sonnailles et clochettes</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>G. Charpentier et Cie, Éditeurs</publisher>
						<date when="1890">1890</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1888">1888</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN651" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="7(abab)">
				<head type="number">XVIII</head>
				<head type="main">Réplique</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2" punct="vg:3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rdr<seg phoneme="o" type="vs" value="1" rule="315" place="3" punct="vg">eau</seg></w>, <w n="1.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="1.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="1.5">pl<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t</w> <w n="1.6">d</w>’<w n="1.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rg<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="vg">en</seg>t</rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg></w> <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="2.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="2.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.5" punct="vg:8">b<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="3.3">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r</w> <w n="3.5" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ge<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="pt">an</seg>t</rhyme></w>.</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="4.2"><seg phoneme="y" type="vs" value="1" rule="251" place="2">eû</seg>t</w> <w n="4.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.6" punct="pt:8">b<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3" punct="vg:4">v<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="5.4">R<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="5.6" punct="vg:8">N<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.4">l</w>’<w n="6.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ch<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="7.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="7.5" punct="dp:7">j<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="dp">ai</seg>s</w> : <w n="7.6" punct="vg:8">N<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="8.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="8.4" punct="pt:8">f<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rch<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1" punct="vg:2">R<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="9.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="9.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">aî</seg>t</w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="9.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="9.6" punct="vg:8">P<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>dr<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8" punct="vg">o</seg></rhyme></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="10.2">d</w>’<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">E</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="10.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ls</w> <w n="10.6">d</w>’<w n="10.7" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>b<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rs</w> <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="11.4" punct="dp:6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rdr<seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="dp">eau</seg></w> : <w n="11.5" punct="vg:8">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rdr<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></rhyme></w>,</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1" punct="pe:1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1" punct="pe">oi</seg>s</w> ! <w n="12.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="12.3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="12.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="12.5">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="6">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>b<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">D</w>’<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="13.4" punct="vg:6">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="13.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="13.6" punct="vg:8">ch<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="vg">e</seg>r</rhyme></w>,</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="14.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="14.5" punct="vg:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="15.2">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="15.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="15.7">ch<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r</rhyme></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="16.6" punct="pe:8"><rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">T<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2" punct="vg:4">R<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="17.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="17.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="17.5" punct="vg:8">c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>rcu<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>t</rhyme></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">S</w>’<w n="18.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="18.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>sc<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="18.5" punct="pt:8">fr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="19.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3" punct="vg:4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rdr<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="vg">eau</seg></w>, <w n="19.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="19.5">qu</w>’<w n="19.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="19.7">f<seg phoneme="y" type="vs" value="1" rule="445" place="7">û</seg>t</w> <w n="19.8" punct="vg:8">cu<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>t</rhyme></w>,</l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">Pr<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>ç<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="20.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="20.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="20.4" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">Ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="21.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="21.3">n</w>’<w n="21.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="21.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="21.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="21.7" punct="vg:8">m<rhyme label="a" id="11" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>l</rhyme></w>,</l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w>-<w n="22.2" punct="vg:2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>l</w>, <w n="22.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="22.4">c</w>’<w n="22.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="22.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="22.7">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="22.8">m<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<rhyme label="b" id="12" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">n</w>’<w n="23.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="23.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="23.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<rhyme label="a" id="11" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>l</rhyme></w>.</l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="24.2">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="3">e</seg></w> <w n="24.3" punct="vg:5">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg">an</seg>t</w>, <w n="24.4">j<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="24.5" punct="vg:8">R<rhyme label="b" id="12" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="25.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="25.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="25.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="25.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="25.6">m<rhyme label="a" id="13" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>r</rhyme></w></l>
					<l n="26" num="7.2" lm="8" met="8"><w n="26.1">F<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="26.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="26.4" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>gr<rhyme label="b" id="14" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="454" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="27.2" punct="vg:2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="27.3" punct="vg:4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rdr<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="vg">eau</seg>x</w>, <w n="27.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="27.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.6"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="a" id="13" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>r</rhyme></w></l>
					<l n="28" num="7.4" lm="8" met="8"><w n="28.1">D</w>’<w n="28.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="28.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="28.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="28.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="28.6" punct="pe:8">gr<rhyme label="b" id="14" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="454" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></w> !</l>
				</lg>
						<closer>
							<dateline>
								<date when="1888"> 15 septembre 1888.</date>
							</dateline>
						</closer>
			</div></body></text></TEI>